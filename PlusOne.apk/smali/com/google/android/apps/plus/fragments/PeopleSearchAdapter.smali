.class public abstract Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "PeopleSearchAdapter.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/Filterable;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/common/widget/EsCompositeCursorAdapter;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/Filterable;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;"
    }
.end annotation


# static fields
.field public static final AVATAR_ONLY_PROJECTION:[Ljava/lang/String;

.field private static final CIRCLES_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_PROJECTION_WITH_PHONE:[Ljava/lang/String;

.field private static final GAIA_ID_CIRCLE_PROJECTION:[Ljava/lang/String;

.field private static final LOCAL_PROFILE_PROJECTION:[Ljava/lang/String;

.field private static final PUBLIC_PROFILE_PROJECTION:[Ljava/lang/String;


# instance fields
.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActiveLoaderCount:I

.field private mActivityId:Ljava/lang/String;

.field protected mAddToCirclesActionEnabled:Z

.field private final mCircleContentObserver:Landroid/database/DataSetObserver;

.field protected final mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private mCircleUsageType:I

.field private mCirclesError:Z

.field private mCirclesLoaded:Z

.field private final mCirclesLoaderId:I

.field private mContactsCursor:Landroid/database/Cursor;

.field private mContactsError:Z

.field private mContactsLoaded:Z

.field private final mContactsLoaderId:I

.field private mFilter:Landroid/widget/Filter;

.field private volatile mFilterLatch:Ljava/util/concurrent/CountDownLatch;

.field private mFilterNullGaiaIds:Z

.field private final mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mGaiaIdCircleCursor:Landroid/database/Cursor;

.field private final mGaiaIdLoaderId:I

.field private final mHandler:Landroid/os/Handler;

.field private mIncludePeopleInCircles:Z

.field protected mIncludePhoneNumberContacts:Z

.field private mIncludePlusPages:Z

.field private mIsMentionsAdapter:Z

.field protected mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

.field private final mLoaderManager:Landroid/support/v4/app/LoaderManager;

.field private mLocalProfileError:Z

.field private mLocalProfilesCursor:Landroid/database/Cursor;

.field private mLocalProfilesLoaded:Z

.field private final mPeopleLoaderId:I

.field private final mProfilesLoaderId:I

.field private mPublicProfileSearchEnabled:Z

.field private mPublicProfilesCursor:Landroid/database/Cursor;

.field private mPublicProfilesError:Z

.field private mPublicProfilesLoading:Z

.field private mPublicProfilesNotFound:Z

.field protected mQuery:Ljava/lang/String;

.field private mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

.field private mResultsPreserved:Z

.field private mShowPersonNameDialog:Z

.field private mShowProgressWhenEmpty:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "circle_id"

    aput-object v1, v0, v4

    const-string v1, "type"

    aput-object v1, v0, v5

    const-string v1, "circle_name"

    aput-object v1, v0, v6

    const-string v1, "contact_count"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CIRCLES_PROJECTION:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    const-string v1, "avatar"

    aput-object v1, v0, v5

    const-string v1, "interaction_sort_key"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->AVATAR_ONLY_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "person_id"

    aput-object v1, v0, v4

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "email"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->LOCAL_PROFILE_PROJECTION:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v3

    const-string v1, "lookup_key"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "email"

    aput-object v1, v0, v6

    const-string v1, "phone"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION_WITH_PHONE:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v3

    const-string v1, "packed_circle_ids"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->GAIA_ID_CIRCLE_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "gaia_id"

    aput-object v1, v0, v4

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v6

    const-string v1, "avatar"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "snippet"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->PUBLIC_PROFILE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/app/FragmentManager;
    .param p3    # Landroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/app/FragmentManager;
    .param p3    # Landroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # I

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-direct {p0, p1, v7}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    const/4 v5, -0x1

    iput v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowProgressWhenEmpty:Z

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    new-instance v5, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-direct {v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;-><init>()V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$2;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleContentObserver:Landroid/database/DataSetObserver;

    const/4 v0, 0x0

    :goto_0
    const/4 v5, 0x6

    if-ge v0, v5, :cond_0

    invoke-virtual {p0, v7, v7}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->addPartition(ZZ)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    mul-int/lit8 v5, p5, 0xa

    add-int/lit16 v1, v5, 0x400

    add-int/lit8 v2, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    add-int/lit8 v1, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    add-int/lit8 v2, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    add-int/lit8 v1, v2, 0x1

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    const-string v5, "people_search_results"

    invoke-virtual {p2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;

    if-nez v4, :cond_2

    new-instance v4, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;

    invoke-direct {v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;-><init>()V

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const-string v6, "people_search_results"

    invoke-virtual {v5, v4, v6}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->setPeopleSearchResults(Lcom/google/android/apps/plus/fragments/PeopleSearchResults;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setMyProfile(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setIncludePeopleInCircles(Z)V

    new-instance v5, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v5, p1, p3, v6, p5}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleContentObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->registerObserver(Landroid/database/DataSetObserver;)V

    return-void

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchResultsFragment;->getPeopleSearchResults()Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    move-result-object v3

    if-eqz v3, :cond_1

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getQuery()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    iput-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResultsPreserved:Z

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    :cond_0
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->getToken()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    :cond_0
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
    .param p1    # Ljava/util/concurrent/CountDownLatch;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterLatch:Ljava/util/concurrent/CountDownLatch;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private changeCursorForPeoplePartition()V
    .locals 5

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_0
.end method

.method private getWellFormedEmailAddress()Ljava/lang/String;
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/util/Rfc822Tokenizer;->tokenize(Ljava/lang/CharSequence;)[Landroid/text/util/Rfc822Token;

    move-result-object v1

    if-eqz v1, :cond_2

    array-length v3, v1

    if-lez v3, :cond_2

    const/4 v3, 0x0

    aget-object v3, v1, v3

    invoke-virtual {v3}, Landroid/text/util/Rfc822Token;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method private getWellFormedSmsAddress()Ljava/lang/String;
    .locals 6

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v5}, Landroid/telephony/PhoneNumberUtils;->isWellFormedSmsAddress(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v1, 0x1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->isDialable(C)Z

    move-result v5

    if-eqz v5, :cond_0

    const/16 v5, 0x2b

    if-ne v0, v5, :cond_2

    if-eqz v1, :cond_0

    :cond_2
    const/4 v1, 0x0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    goto :goto_0
.end method

.method private releaseLatch()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterLatch:Ljava/util/concurrent/CountDownLatch;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    :cond_0
    return-void
.end method

.method private updatePublicProfileSearchStatus()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v3, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v5, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesLoaded:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaded:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    if-eqz v1, :cond_3

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_1
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->showEmptyPeopleSearchResults()V

    :cond_2
    const/4 v1, 0x5

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesNotFound:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    if-nez v1, :cond_4

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowProgressWhenEmpty:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    :cond_5
    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method protected final continueLoadingPublicProfiles()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->hasMoreResults()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$3;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilter:Landroid/widget/Filter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$6;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilter:Landroid/widget/Filter;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilter:Landroid/widget/Filter;

    return-object v0
.end method

.method protected getItemViewType(II)I
    .locals 0
    .param p1    # I
    .param p2    # I

    return p1
.end method

.method public getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x6

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isError()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesError:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfileError:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsError:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLoaded()Z
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesLoaded:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaded:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaded:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isSearchingForFirstResult()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v0, "search_list_adapter.query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResultsPreserved:Z

    if-nez v0, :cond_0

    const-string v0, "search_list_adapter.results"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    sget-object v4, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CIRCLES_PROJECTION:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const/16 v6, 0xa

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;Ljava/lang/String;I)V

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    if-ne p1, v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->GAIA_ID_CIRCLE_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    if-ne p1, v0, :cond_3

    new-instance v0, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION_WITH_PHONE:[Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const/4 v4, 0x2

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/AndroidContactSearchLoader;-><init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->CONTACT_PROJECTION:[Ljava/lang/String;

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    if-ne p1, v0, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->LOCAL_PROFILE_PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActivityId:Ljava/lang/String;

    const/16 v9, 0xa

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    if-ne p1, v0, :cond_5

    new-instance v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->PUBLIC_PROFILE_PROJECTION:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getToken()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;IZZLjava/lang/String;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x0

    const-string v5, "add_email_dialog"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "message"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedEmailAddress()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "e:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v1, v7}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v2, v7, v1, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v7, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_0

    :cond_2
    const-string v5, "add_sms_dialog"

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "message"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedSmsAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "p:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v1, v7}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onChangeCirclesAction(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    new-instance v2, Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {v2, v7, v1, v3}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v5, v3, v7, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_0
.end method

.method public final onItemClick(I)V
    .locals 25
    .param p1    # I

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/database/Cursor;

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getPartitionForPosition(I)I

    move-result v13

    packed-switch v13, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-direct {v14, v10, v12, v0}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1, v14}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v7

    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v18

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    new-instance v5, Lcom/google/android/apps/plus/content/CircleData;

    move/from16 v0, v18

    move/from16 v1, v16

    invoke-direct {v5, v6, v0, v12, v1}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v19

    if-eqz v19, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-static {v7, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasSeenMinorPublicExtendedDialog(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v19

    if-nez v19, :cond_2

    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v19, Lcom/google/android/apps/plus/R$string;->dialog_public_or_extended_circle_for_minor:I

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v19, Lcom/google/android/apps/plus/R$string;->ok:I

    new-instance v20, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$4;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v5, v7}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$4;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;Landroid/content/Context;)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v19, Lcom/google/android/apps/plus/R$string;->cancel:I

    new-instance v20, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$5;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$5;-><init>(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    move/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v6, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    goto/16 :goto_0

    :pswitch_2
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/16 v19, 0x2

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v20, v0

    const/16 v19, 0x3

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v19, 0x4

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    const/16 v23, 0xa

    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_3

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v24, "p:"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    :cond_3
    if-nez v19, :cond_4

    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    :cond_4
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_5

    const/16 v19, 0x9

    move/from16 v0, v19

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_5

    const/16 v19, 0x0

    :cond_5
    new-instance v23, Lcom/google/android/apps/plus/content/PersonData;

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-interface {v0, v15, v11, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto/16 :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    move/from16 v19, v0

    if-eqz v19, :cond_6

    const-string v19, "add_email_dialog"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedEmailAddress()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "e:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v14, v0, v1, v9}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1, v14}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto/16 :goto_0

    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    const-string v19, "add_sms_dialog"

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->showPersonNameDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedSmsAddress()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_0

    new-instance v19, Ljava/lang/StringBuilder;

    const-string v20, "p:"

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    new-instance v14, Lcom/google/android/apps/plus/content/PersonData;

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v14, v0, v1, v15}, Lcom/google/android/apps/plus/content/PersonData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1, v14}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 13
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v7, 0x0

    const/4 v10, 0x1

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    if-ne v0, v1, :cond_4

    if-nez p2, :cond_3

    move v0, v10

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesError:Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaded:Z

    invoke-virtual {p0, v10, p2}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    if-gtz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    :cond_2
    return-void

    :cond_3
    move v0, v7

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdCircleCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onStartGaiaIdsAndCircles()V

    if-eqz p2, :cond_7

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addGaiaIdAndCircles(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onFinishGaiaIdsAndCircles()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    goto :goto_1

    :cond_8
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    if-ne v0, v1, :cond_11

    if-nez p2, :cond_e

    move v0, v10

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsError:Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_9
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onStartContacts()V

    if-eqz p2, :cond_b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_a
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v0, :cond_f

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_3
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v0, :cond_10

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-static {}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onFinishContacts()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v11, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v7

    const-string v2, "address"

    aput-object v2, v1, v10

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz p2, :cond_c

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_c

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedEmailAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v1, v2, v10

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_c
    invoke-virtual {p0, v11, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v1, v11, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v7

    const-string v2, "address"

    aput-object v2, v1, v10

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    if-eqz p2, :cond_d

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_d

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getWellFormedSmsAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    new-array v2, v11, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    aput-object v1, v2, v10

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_d
    invoke-virtual {p0, v12, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    goto/16 :goto_1

    :cond_e
    move v0, v7

    goto/16 :goto_2

    :cond_f
    move-object v5, v8

    goto/16 :goto_3

    :cond_10
    move-object v6, v8

    goto/16 :goto_4

    :cond_11
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    if-ne v0, v1, :cond_16

    if-nez p2, :cond_12

    move v7, v10

    :cond_12
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfileError:Z

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesLoaded:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_13
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLocalProfilesCursor:Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onStartLocalProfiles()V

    if-eqz p2, :cond_15

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x5

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x6

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v9, v8

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addLocalProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_14

    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->onFinishLocalProfiles()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    goto/16 :goto_1

    :cond_16
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/fragments/PublicProfileSearchLoader;->ABORTED:Landroid/database/MatrixCursor;

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v10}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    if-eq v0, p2, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_17
    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesCursor:Landroid/database/Cursor;

    if-eqz p2, :cond_18

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_19

    :cond_18
    move v0, v10

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    if-eqz v0, :cond_1a

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    goto/16 :goto_1

    :cond_19
    move v0, v7

    goto :goto_5

    :cond_1a
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setToken(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1b

    move v0, v10

    :goto_6
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setHasMoreResults(Z)V

    :goto_7
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-interface {p2, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->addPublicProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_7

    :cond_1b
    move v0, v7

    goto :goto_6

    :cond_1c
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getPublicProfileCount()I

    move-result v0

    if-nez v0, :cond_1d

    :goto_8
    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesNotFound:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursorForPeoplePartition()V

    goto/16 :goto_1

    :cond_1d
    move v10, v7

    goto :goto_8
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "search_list_adapter.query"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->isParcelable()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "search_list_adapter.results"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 5

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mGaiaIdLoaderId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "query"

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v2, v3, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v3, "add_person_dialog_listener"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;

    if-eqz v1, :cond_4

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->setAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    :cond_4
    return-void
.end method

.method public final onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public final setAddToCirclesActionEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mAddToCirclesActionEnabled:Z

    return-void
.end method

.method public final setCircleUsageType(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    return-void
.end method

.method public final setFilterNullGaiaIds(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    return-void
.end method

.method public final setIncludePeopleInCircles(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePeopleInCircles:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setIncludePeopleInCircles(Z)V

    return-void
.end method

.method public final setIncludePhoneNumberContacts(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePhoneNumberContacts:Z

    return-void
.end method

.method public final setIncludePlusPages(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIncludePlusPages:Z

    return-void
.end method

.method public final setListener(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    return-void
.end method

.method public final setMention(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActivityId:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mIsMentionsAdapter:Z

    return-void
.end method

.method public final setPublicProfileSearchEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    return-void
.end method

.method public final setQueryString(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->setQueryString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    iput v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->clearPartitions()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->releaseLatch()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mListener:Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$SearchListAdapterListener;->onSearchListAdapterStateChange(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "query"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCircleUsageType:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mCirclesLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPeopleLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFilterNullGaiaIds:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mActiveLoaderCount:I

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mContactsLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfileSearchEnabled:Z

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesError:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesNotFound:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mPublicProfilesLoading:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mLoaderManager:Landroid/support/v4/app/LoaderManager;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mProfilesLoaderId:I

    invoke-virtual {v1, v2, v0, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->updatePublicProfileSearchStatus()V

    goto/16 :goto_0
.end method

.method public final setShowPersonNameDialog(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowPersonNameDialog:Z

    return-void
.end method

.method public final setShowProgressWhenEmpty(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mShowProgressWhenEmpty:Z

    return-void
.end method

.method protected final showEmptyPeopleSearchResults()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mResults:Lcom/google/android/apps/plus/fragments/PeopleSearchResults;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->changeCursor(ILandroid/database/Cursor;)V

    :cond_0
    return-void
.end method

.method protected final showPersonNameDialog(Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v1, "add_person_dialog_listener"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;

    if-nez v8, :cond_0

    new-instance v8, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;

    invoke-direct {v8}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "add_person_dialog_listener"

    invoke-virtual {v0, v8, v1}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-virtual {v8, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter$AddEmailDialogListener;->setAdapter(Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v0, Lcom/google/android/apps/plus/R$string;->add_email_dialog_title:I

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$string;->add_email_dialog_hint:I

    invoke-virtual {v6, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x104000a

    invoke-virtual {v6, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/high16 v4, 0x1040000

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->newInstance$405ed676(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/EditFragmentDialog;

    move-result-object v7

    invoke-virtual {v7, v8, v5}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v7, v0, p1}, Lcom/google/android/apps/plus/fragments/EditFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
