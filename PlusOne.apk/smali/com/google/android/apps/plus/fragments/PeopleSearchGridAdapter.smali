.class public final Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;
.super Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;
.source "PeopleSearchGridAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

.field private mTopBottomPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/app/FragmentManager;
    .param p3    # Landroid/support/v4/app/LoaderManager;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/PeopleSearchAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 31
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/16 v30, 0x0

    const/4 v2, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    if-eqz v30, :cond_1

    if-nez p4, :cond_8

    const/16 v23, 0x1

    :goto_1
    add-int/lit8 v3, p4, 0x1

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->getCount(I)I

    move-result v5

    if-ne v3, v5, :cond_9

    const/16 v24, 0x1

    :goto_2
    if-eqz v23, :cond_a

    if-eqz v24, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mTopBottomPadding:I

    const/4 v5, 0x0

    move-object/from16 v0, v30

    invoke-virtual {v0, v3, v5}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstAndLastRow(II)V

    :cond_1
    :goto_3
    return-void

    :pswitch_1
    move-object/from16 v30, p1

    check-cast v30, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    sget v3, Lcom/google/android/apps/plus/R$id;->circle_name:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    move-object/from16 v0, v26

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->member_count:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ")"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v29

    sget v3, Lcom/google/android/apps/plus/R$id;->people_suggestion_type:I

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->people_search_circle_id:I

    move-object/from16 v0, v29

    move-object/from16 v1, v19

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->people_search_circle_name:I

    move-object/from16 v0, v29

    move-object/from16 v1, v20

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_2
    move-object/from16 v30, p1

    check-cast v30, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v5, v6}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_3
    move-object/from16 v30, p1

    check-cast v30, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowView;

    const/4 v3, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v28

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v3, 0x1

    move/from16 v0, v28

    if-ne v0, v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    invoke-virtual {v2, v5, v6, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    const/16 v3, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    const/16 v3, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    const/16 v3, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    :cond_2
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v3, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    if-nez v4, :cond_3

    if-eqz v9, :cond_6

    :cond_3
    move-object/from16 v3, p0

    :goto_5
    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez v4, :cond_4

    if-eqz v9, :cond_7

    :cond_4
    const/4 v3, 0x1

    :goto_6
    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setClickable(Z)V

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, p4

    if-ne v0, v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->continueLoadingPublicProfiles()V

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    goto :goto_6

    :pswitch_4
    move-object/from16 v30, p1

    check-cast v30, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v6, 0x1

    invoke-virtual {v2, v3, v5, v6}, Lcom/google/android/apps/plus/views/PeopleListRowView;->init(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;Lcom/google/android/apps/plus/fragments/CircleNameResolver;Z)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "e:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mQuery:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v10, v2

    invoke-virtual/range {v10 .. v18}, Lcom/google/android/apps/plus/views/PeopleListRowView;->bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setClickable(Z)V

    goto/16 :goto_0

    :pswitch_5
    const/16 v25, 0x8

    const/16 v27, 0x8

    const/16 v22, 0x8

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_1

    :goto_7
    sget v3, Lcom/google/android/apps/plus/R$id;->loading:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move/from16 v0, v25

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->not_found:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move/from16 v0, v27

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->error:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_6
    const/16 v25, 0x0

    goto :goto_7

    :pswitch_7
    const/16 v27, 0x0

    goto :goto_7

    :pswitch_8
    const/16 v22, 0x0

    goto :goto_7

    :cond_8
    const/16 v23, 0x0

    goto/16 :goto_1

    :cond_9
    const/16 v24, 0x0

    goto/16 :goto_2

    :cond_a
    if-eqz v23, :cond_b

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mTopBottomPadding:I

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsFirstRow(I)V

    goto/16 :goto_3

    :cond_b
    if-eqz v24, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mTopBottomPadding:I

    move-object/from16 v0, v30

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsLastRow(I)V

    goto/16 :goto_3

    :cond_c
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setIsBodyRow()V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method protected final getItemViewType(II)I
    .locals 1
    .param p1    # I
    .param p2    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public final getItemViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public final isCursorClosingEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final isEnabled$255f299(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sparse-switch p2, :sswitch_data_0

    sget v3, Lcom/google/android/apps/plus/R$layout;->people_list_row_as_card:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/PeopleListRowView;->setAllowAvatarClickWithoutGaiaId(Z)V

    :goto_0
    return-object v2

    :sswitch_0
    sget v3, Lcom/google/android/apps/plus/R$layout;->people_list_circle_as_card:I

    invoke-virtual {v0, v3, p5, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    :sswitch_1
    sget v3, Lcom/google/android/apps/plus/R$layout;->people_suggestions_status:I

    invoke-virtual {v0, v3, p5, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sget v6, Lcom/google/android/apps/plus/R$id;->row:I

    if-ne v2, v6, :cond_0

    sget v6, Lcom/google/android/apps/plus/R$id;->people_suggestion_type:I

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeInt(Ljava/lang/Integer;)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    sget v6, Lcom/google/android/apps/plus/R$id;->people_search_circle_id:I

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget v6, Lcom/google/android/apps/plus/R$id;->people_search_circle_name:I

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7, v0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getCirclePeopleActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    if-eqz v6, :cond_0

    instance-of v6, p1, Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v6, :cond_0

    move-object v4, p1

    check-cast v4, Lcom/google/android/apps/plus/views/PeopleListRowView;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getPersonId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getSuggestionId()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;->onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final setOnClickListener(Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mOnClickListener:Lcom/google/android/apps/plus/views/PeopleListRowView$OnClickListener;

    return-void
.end method

.method public final setTopBottomPadding(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchGridAdapter;->mTopBottomPadding:I

    return-void
.end method
