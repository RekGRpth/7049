.class public final Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PeopleSearchListLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mProjection:[Ljava/lang/String;

.field private mQueryUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Z
    .param p7    # Z
    .param p8    # Ljava/lang/String;
    .param p9    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->setUri(Landroid/net/Uri;)V

    if-eqz p7, :cond_0

    const-string v0, "gaia_id IS NOT NULL"

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->setSelection(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mProjection:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/16 v5, 0xa

    move-object v1, p4

    move v2, p5

    move v3, p6

    move-object v4, p8

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsProvider;->buildPeopleQueryUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZZLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mQueryUri:Landroid/net/Uri;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 8

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->ensurePeopleSynced(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object v4

    :cond_0
    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mQueryUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mProjection:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->getSelection()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v7, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    move-object v4, v7

    goto :goto_0
.end method
