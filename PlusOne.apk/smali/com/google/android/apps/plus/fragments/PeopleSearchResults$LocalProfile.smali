.class final Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;
.super Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;
.source "PeopleSearchResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocalProfile"
.end annotation


# instance fields
.field email:Ljava/lang/String;

.field packedCircleIds:Ljava/lang/String;

.field phoneNumber:Ljava/lang/String;

.field phoneType:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;

    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->packedCircleIds:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->email:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->phoneNumber:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->phoneType:Ljava/lang/String;

    return-void
.end method
