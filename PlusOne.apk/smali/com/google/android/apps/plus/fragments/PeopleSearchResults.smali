.class public Lcom/google/android/apps/plus/fragments/PeopleSearchResults;
.super Ljava/lang/Object;
.source "PeopleSearchResults.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;,
        Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Profile;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults;",
            ">;"
        }
    .end annotation
.end field

.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mContacts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field private mCursorValid:Z

.field private final mGaiaIdsAndCircles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mGaiaIdsAndCirclesLoaded:Z

.field private mHasMoreResults:Z

.field private mIncludePeopleInCircles:Z

.field private final mLocalProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalProfilesLoaded:Z

.field private mMyPersonId:Ljava/lang/String;

.field private mNextId:J

.field private final mPublicProfiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;",
            ">;"
        }
    .end annotation
.end field

.field private mQuery:Ljava/lang/String;

.field private mToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "lookup_key"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "matched_email"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "email"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "phone"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "phone_type"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "snippet"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->PROJECTION:[Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 11
    .param p1    # Landroid/os/Parcel;

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    iput-boolean v10, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v9

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    const/4 v7, 0x0

    :goto_2
    if-ge v7, v8, :cond_2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_0
    move v0, v10

    goto :goto_0

    :cond_1
    move v9, v10

    goto :goto_1

    :cond_2
    return-void
.end method

.method private static normalizeEmailAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_1

    const/4 p0, 0x0

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "@gmail.com"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "."

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static onFinishContacts()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final addContact(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final addGaiaIdAndCircles(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final addLocalProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p6, :cond_4

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v12, 0x0

    :goto_1
    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v12, v1, :cond_3

    const/16 v1, 0x7c

    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v12}, Ljava/lang/String;->indexOf(II)I

    move-result v14

    const/4 v1, -0x1

    if-ne v14, v1, :cond_1

    invoke-virtual/range {p6 .. p6}, Ljava/lang/String;->length()I

    move-result v14

    :cond_1
    move-object/from16 v0, p6

    invoke-virtual {v0, v12, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v11

    add-int/lit8 v1, v14, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v11, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7c

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    add-int/lit8 v12, v14, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p6

    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final addPublicProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public final getCursor()Landroid/database/Cursor;
    .locals 24

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    :goto_0
    return-object v17

    :cond_0
    new-instance v17, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v18, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->PROJECTION:[Ljava/lang/String;

    invoke-direct/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    move/from16 v17, v0

    if-nez v17, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    goto :goto_0

    :cond_2
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    move/from16 v17, v0

    if-eqz v17, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_3
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;

    iget-object v9, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->gaiaId:Ljava/lang/String;

    iget-object v13, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->email:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->personId:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    aput-object v9, v18, v19

    const/16 v19, 0x4

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x5

    iget v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->profileType:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->avatarUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x7

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->packedCircleIds:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x8

    aput-object v13, v18, v19

    const/16 v19, 0x9

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xa

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->phoneNumber:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0xb

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->phoneType:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0xc

    const/16 v20, 0x0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    invoke-virtual {v10, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$LocalProfile;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    if-eqz v13, :cond_3

    invoke-static {v13}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->normalizeEmailAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_5
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_6

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    iget-object v9, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->gaiaId:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->personId:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    aput-object v9, v18, v19

    const/16 v19, 0x4

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x5

    iget v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->profileType:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->avatarUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x7

    aput-object v3, v18, v19

    const/16 v19, 0x8

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x9

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xa

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xb

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xc

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->snippet:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    invoke-virtual {v10, v9}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v17

    if-nez v17, :cond_b

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_7
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_b

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    iget-object v5, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->email:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->normalizeEmailAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    invoke-virtual {v12, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    if-eqz v7, :cond_a

    const/16 v17, 0x4

    aget-object v8, v7, v17

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_8

    invoke-virtual {v8, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    :cond_8
    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_7

    const/16 v17, 0x1

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->personId:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    const/16 v17, 0x2

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->lookupKey:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    const/16 v17, 0x4

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    const/16 v17, 0xa

    aget-object v17, v7, v17

    if-nez v17, :cond_9

    const/16 v17, 0xa

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneNumber:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    :cond_9
    const/16 v17, 0xb

    aget-object v17, v7, v17

    if-nez v17, :cond_7

    const/16 v17, 0xb

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneType:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v7, v17

    goto :goto_3

    :cond_a
    const/16 v17, 0xd

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x1

    add-long v20, v20, v18

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->personId:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x2

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->lookupKey:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x3

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x4

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0x5

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x6

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x7

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x8

    const/16 v18, 0x0

    aput-object v18, v16, v17

    const/16 v17, 0x9

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->email:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0xa

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneNumber:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0xb

    iget-object v0, v4, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$Contact;->phoneType:Ljava/lang/String;

    move-object/from16 v18, v0

    aput-object v18, v16, v17

    const/16 v17, 0xc

    const/16 v18, 0x0

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-virtual {v12, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_c
    :goto_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_d

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    iget-object v9, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->gaiaId:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    const/16 v18, 0xd

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    add-long v22, v22, v20

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mNextId:J

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x1

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->personId:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x2

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x3

    aput-object v9, v18, v19

    const/16 v19, 0x4

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x5

    iget v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->profileType:I

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    const/16 v19, 0x6

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->avatarUrl:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    const/16 v19, 0x7

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x8

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0x9

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xa

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xb

    const/16 v20, 0x0

    aput-object v20, v18, v19

    const/16 v19, 0xc

    iget-object v0, v15, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->snippet:Ljava/lang/String;

    move-object/from16 v20, v0

    aput-object v20, v18, v19

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v17, v0

    goto/16 :goto_0
.end method

.method public final getPublicProfileCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public final getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    return-object v0
.end method

.method public final hasMoreResults()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    return v0
.end method

.method public final isParcelable()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x3e8

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onFinishGaiaIdsAndCircles()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCirclesLoaded:Z

    return-void
.end method

.method public final onFinishLocalProfiles()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    return-void
.end method

.method public final onStartContacts()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mContacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    return-void
.end method

.method public final onStartGaiaIdsAndCircles()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mGaiaIdsAndCircles:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    return-void
.end method

.method public final onStartLocalProfiles()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    return-void
.end method

.method public final setHasMoreResults(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    return-void
.end method

.method public final setIncludePeopleInCircles(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    return-void
.end method

.method public final setMyProfile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    return-void
.end method

.method public final setQueryString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mLocalProfilesLoaded:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mCursorValid:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public final setToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mMyPersonId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mToken:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mHasMoreResults:Z

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mIncludePeopleInCircles:Z

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleSearchResults;->mPublicProfiles:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;

    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->personId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->gaiaId:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->name:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->profileType:I

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->avatarUrl:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/google/android/apps/plus/fragments/PeopleSearchResults$PublicProfile;->snippet:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_1

    :cond_2
    return-void
.end method
