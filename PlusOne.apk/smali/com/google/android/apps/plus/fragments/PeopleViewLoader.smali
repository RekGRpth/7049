.class public final Lcom/google/android/apps/plus/fragments/PeopleViewLoader;
.super Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;
.source "PeopleViewLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader",
        "<",
        "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAllRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataRequestParameter;",
            ">;"
        }
    .end annotation
.end field

.field private mAllResponses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;"
        }
    .end annotation
.end field

.field private mCacheExpiredTimeout:J

.field private mData:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

.field mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private mResponseMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/api/services/plusi/model/PeopleViewDataResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/api/services/plusi/model/DataRequestParameter;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mResponseMap:Ljava/util/HashMap;

    new-instance v1, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v1, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p5, :cond_0

    invoke-virtual {v0, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const-wide/32 v1, 0x7fffffff

    invoke-direct {p0, p2, v1, v2, v0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->init(Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataRequestParameter;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mResponseMap:Ljava/util/HashMap;

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    const-wide/32 v0, 0x7fffffff

    invoke-direct {p0, p2, v0, v1, p5}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->init(Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;)V

    return-void
.end method

.method private esLoadInBackground()Lcom/google/api/services/plusi/model/PeopleViewDataResponse;
    .locals 44

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mData:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    move-object/from16 v33, v0

    :cond_1
    return-object v33

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    new-instance v43, Ljava/util/ArrayList;

    invoke-direct/range {v43 .. v43}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v29

    const/16 v26, 0x0

    :goto_0
    move/from16 v0, v26

    move/from16 v1, v29

    if-ge v0, v1, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    move/from16 v0, v26

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/api/services/plusi/model/DataRequestParameter;

    invoke-static/range {v35 .. v35}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->isPreview(Lcom/google/api/services/plusi/model/DataRequestParameter;)Z

    move-result v3

    move-object/from16 v0, v35

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getFilename(Lcom/google/api/services/plusi/model/DataRequestParameter;Z)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mResponseMap:Ljava/util/HashMap;

    move-object/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    if-nez v39, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mCacheExpiredTimeout:J

    move-object/from16 v0, v24

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/apps/plus/content/SuggestionsFileCache;->getCachedJson(Landroid/content/Context;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v21

    if-eqz v21, :cond_5

    invoke-static {}, Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mResponseMap:Ljava/util/HashMap;

    move-object/from16 v0, v24

    move-object/from16 v1, v39

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    :goto_1
    if-eqz v39, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    move-object/from16 v0, v39

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v26, v26, 0x1

    goto :goto_0

    :cond_5
    move-object/from16 v0, v43

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    new-instance v31, Landroid/os/ConditionVariable;

    invoke-direct/range {v31 .. v31}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v32, Ljava/util/ArrayList;

    invoke-direct/range {v32 .. v32}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_11

    const/4 v7, 0x0

    const/4 v13, 0x0

    new-instance v20, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/apps/plus/fragments/PeopleViewLoader$1;

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v6, v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader$1;-><init>(Lcom/google/android/apps/plus/fragments/PeopleViewLoader;Landroid/os/ConditionVariable;)V

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v43 .. v43}, Ljava/util/ArrayList;->size()I

    move-result v29

    const/16 v26, 0x0

    :goto_2
    move/from16 v0, v26

    move/from16 v1, v29

    if-ge v0, v1, :cond_e

    move-object/from16 v0, v43

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/api/services/plusi/model/DataRequestParameter;

    const/4 v2, 0x0

    const-string v3, "FRIEND_ADDS"

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "YOU_MAY_KNOW"

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_7
    if-nez v7, :cond_8

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/google/android/apps/plus/api/PeopleViewOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/plus/api/PeopleViewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;)V

    :cond_8
    move-object/from16 v0, v35

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    if-eqz v2, :cond_9

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v26, v26, 0x1

    goto :goto_2

    :cond_a
    const-string v3, "ORGANIZATION"

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    const-string v3, "SCHOOL"

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    :cond_b
    if-nez v13, :cond_c

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/google/android/apps/plus/api/PeopleViewOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v8, v2

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/plus/api/PeopleViewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;)V

    :cond_c
    move-object/from16 v0, v35

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_d
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/api/PeopleViewOperation;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v14, v2

    invoke-direct/range {v14 .. v19}, Lcom/google/android/apps/plus/api/PeopleViewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;)V

    goto :goto_3

    :cond_e
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->startThreaded()V

    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->getGaiaIdToPackedCircleIdsMap(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v25

    invoke-virtual/range {v31 .. v31}, Landroid/os/ConditionVariable;->block()V

    if-eqz v20, :cond_1d

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v22

    const/16 v26, 0x0

    :goto_5
    move/from16 v0, v26

    move/from16 v1, v22

    if-ge v0, v1, :cond_1d

    move-object/from16 v0, v32

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v30

    check-cast v30, Lcom/google/android/apps/plus/api/PeopleViewOperation;

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/api/PeopleViewOperation;->hasError()Z

    move-result v3

    if-nez v3, :cond_1c

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/api/PeopleViewOperation;->getRequest()Lcom/google/api/services/plusi/model/PeopleViewDataRequest;

    move-result-object v3

    iget-object v0, v3, Lcom/google/api/services/plusi/model/PeopleViewDataRequest;->parameter:Ljava/util/List;

    move-object/from16 v37, v0

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/api/PeopleViewOperation;->getResponse()Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    move-result-object v3

    iget-object v0, v3, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    move-object/from16 v42, v0

    invoke-interface/range {v37 .. v37}, Ljava/util/List;->size()I

    move-result v36

    const/16 v34, 0x0

    :goto_6
    move/from16 v0, v34

    move/from16 v1, v36

    if-ge v0, v1, :cond_1c

    move-object/from16 v0, v37

    move/from16 v1, v34

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v35

    check-cast v35, Lcom/google/api/services/plusi/model/DataRequestParameter;

    new-instance v41, Ljava/util/ArrayList;

    invoke-direct/range {v41 .. v41}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "ORGANIZATION"

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "SCHOOL"

    move-object/from16 v0, v35

    iget-object v4, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    :cond_f
    move-object/from16 v0, v35

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    if-eqz v3, :cond_12

    const/16 v28, 0x0

    :goto_7
    if-nez v42, :cond_13

    const/16 v40, 0x0

    :goto_8
    const/16 v38, 0x0

    :goto_9
    move/from16 v0, v38

    move/from16 v1, v40

    if-ge v0, v1, :cond_1a

    move-object/from16 v0, v42

    move/from16 v1, v38

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/google/api/services/plusi/model/ListResponse;

    move-object/from16 v0, v35

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_10

    if-eqz v28, :cond_14

    move-object/from16 v0, v41

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    :goto_a
    add-int/lit8 v38, v38, 0x1

    goto :goto_9

    :cond_11
    const/16 v20, 0x0

    invoke-virtual/range {v31 .. v31}, Landroid/os/ConditionVariable;->open()V

    goto/16 :goto_4

    :cond_12
    const/16 v28, 0x1

    goto :goto_7

    :cond_13
    invoke-interface/range {v42 .. v42}, Ljava/util/List;->size()I

    move-result v40

    goto :goto_8

    :cond_14
    move-object/from16 v0, v35

    iget-object v3, v0, Lcom/google/api/services/plusi/model/DataRequestParameter;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    if-eqz v3, :cond_15

    if-nez v4, :cond_16

    :cond_15
    const/4 v3, 0x0

    :goto_b
    if-eqz v3, :cond_10

    move-object/from16 v0, v41

    move-object/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_16
    iget-object v5, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    iget-object v6, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    invoke-static {v5, v6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_17

    const/4 v3, 0x0

    goto :goto_b

    :cond_17
    iget-object v5, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    if-eqz v5, :cond_18

    iget-object v5, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    if-eqz v5, :cond_18

    iget-object v5, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    iget-object v6, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->startYear:Ljava/lang/Integer;

    if-eq v5, v6, :cond_18

    const/4 v3, 0x0

    goto :goto_b

    :cond_18
    iget-object v5, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    if-eqz v5, :cond_19

    iget-object v5, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    if-eqz v5, :cond_19

    iget-object v3, v3, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->endYear:Ljava/lang/Integer;

    if-eq v3, v4, :cond_19

    const/4 v3, 0x0

    goto :goto_b

    :cond_19
    const/4 v3, 0x1

    goto :goto_b

    :cond_1a
    invoke-interface/range {v41 .. v41}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1b

    new-instance v23, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    invoke-direct/range {v23 .. v23}, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;-><init>()V

    move-object/from16 v0, v41

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    invoke-static/range {v35 .. v35}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->isPreview(Lcom/google/api/services/plusi/model/DataRequestParameter;)Z

    move-result v3

    move-object/from16 v0, v35

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getFilename(Lcom/google/api/services/plusi/model/DataRequestParameter;Z)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;->getInstance()Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Lcom/google/api/services/plusi/model/PeopleViewDataResponseJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-static {v3, v0, v4}, Lcom/google/android/apps/plus/content/SuggestionsFileCache;->saveJson(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mResponseMap:Ljava/util/HashMap;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v3, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    move-object/from16 v0, v23

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1b
    add-int/lit8 v34, v34, 0x1

    goto/16 :goto_6

    :cond_1c
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_5

    :cond_1d
    new-instance v33, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    invoke-direct/range {v33 .. v33}, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v33

    iput-object v3, v0, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v27

    :goto_c
    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface/range {v27 .. v27}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    move-object/from16 v0, v39

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    move-object/from16 v0, v25

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->createMembershipsInListResponse(Ljava/util/List;Ljava/util/HashMap;)V

    move-object/from16 v0, v33

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    move-object/from16 v0, v39

    iget-object v4, v0, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;->listResponse:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_c
.end method

.method private static getFilename(Lcom/google/api/services/plusi/model/DataRequestParameter;Z)Ljava/lang/String;
    .locals 4
    .param p0    # Lcom/google/api/services/plusi/model/DataRequestParameter;
    .param p1    # Z

    const/4 v0, 0x0

    const-string v1, "SCHOOL"

    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ORGANIZATION"

    iget-object v2, p0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataRequestParameter;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataRequestParameter;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    iget-object v0, v1, Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;->name:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lcom/google/api/services/plusi/model/DataRequestParameter;->listType:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsMediaCache;->buildShortFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p1, :cond_3

    const-string v1, "-preview"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_3
    const-string v1, ""

    goto :goto_0
.end method

.method private init(Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "J",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataRequestParameter;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-wide p2, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mCacheExpiredTimeout:J

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    return-void
.end method

.method private static isPreview(Lcom/google/api/services/plusi/model/DataRequestParameter;)Z
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/DataRequestParameter;

    iget-object v0, p0, Lcom/google/api/services/plusi/model/DataRequestParameter;->maxResults:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->isReset()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mData:Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic esLoadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->esLoadInBackground()Lcom/google/api/services/plusi/model/PeopleViewDataResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final onReset()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->onReset()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->cancelLoad()Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllRequests:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mAllResponses:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserverRegistered:Z

    :cond_2
    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserverRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CIRCLES_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->mObserverRegistered:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->forceLoad()V

    return-void
.end method

.method protected final onStopLoading()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PeopleViewLoader;->cancelLoad()Z

    return-void
.end method
