.class public interface abstract Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;
.super Ljava/lang/Object;
.source "PhotoOneUpCallbacks.java"


# virtual methods
.method public abstract addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V
.end method

.method public abstract addScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V
.end method

.method public abstract deletePhoto(Lcom/google/android/apps/plus/api/MediaRef;)V
.end method

.method public abstract getFullScreen()Z
.end method

.method public abstract isFragmentActive(Landroid/support/v4/app/Fragment;)Z
.end method

.method public abstract onFragmentVisible(Landroid/support/v4/app/Fragment;)V
.end method

.method public abstract onPhotoRemoved$1349ef()V
.end method

.method public abstract removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V
.end method

.method public abstract removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V
.end method

.method public abstract toggleFullScreen()V
.end method
