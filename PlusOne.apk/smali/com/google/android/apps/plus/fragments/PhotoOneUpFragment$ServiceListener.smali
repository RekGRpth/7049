.class final Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "PhotoOneUpFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    return v0
.end method

.method private handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, p1, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2202(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    sget v0, Lcom/google/android/apps/plus/R$string;->operation_failed:I

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2700(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :sswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->reshare_post_error:I

    goto :goto_1

    :sswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->report_photo_error:I

    goto :goto_1

    :sswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_post_error:I

    goto :goto_1

    :sswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_delete_error:I

    goto :goto_1

    :sswitch_4
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_moderate_error:I

    goto :goto_1

    :sswitch_5
    sget v0, Lcom/google/android/apps/plus/R$string;->comment_edit_error:I

    goto :goto_1

    :sswitch_6
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_tag_approve_error:I

    goto :goto_1

    :sswitch_7
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_tag_deny_error:I

    goto :goto_1

    :sswitch_8
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_tag_deny_error:I

    goto :goto_1

    :sswitch_9
    sget v0, Lcom/google/android/apps/plus/R$string;->set_profile_photo_error:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_2
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_1
        0x12 -> :sswitch_0
        0x15 -> :sswitch_9
        0x20 -> :sswitch_2
        0x21 -> :sswitch_3
        0x22 -> :sswitch_4
        0x25 -> :sswitch_5
        0x30 -> :sswitch_8
        0x31 -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onGetPhoto$4894d499(IJ)V
    .locals 2
    .param p1    # I
    .param p2    # J

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1402(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    goto :goto_0
.end method

.method public final onGetPhotoSettings$6e3d3b8d(IZ)V
    .locals 2
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1402(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1702(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    goto :goto_0
.end method

.method public final onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # J
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->onPhotoRemoved$1349ef()V

    :cond_0
    return-void
.end method

.method public final onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2202(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    if-eqz p2, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$string;->plusone_error:I

    :goto_1
    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$string;->delete_plusone_error:I

    goto :goto_1
.end method

.method public final onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # Z
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;

    move-result-object v1

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->plusone_error:I

    :goto_0
    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->delete_plusone_error:I

    goto :goto_0
.end method

.method public final onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->removeFlaggedComment(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->addFlaggedComment(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method

.method public final onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 6
    .param p1    # I
    .param p2    # Ljava/io/File;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2202(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    if-eqz p6, :cond_5

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "StreamOneUp"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string v3, "StreamOneUp"

    const-string v4, "Could not download image"

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    if-eqz p3, :cond_4

    sget v2, Lcom/google/android/apps/plus/R$id;->photo_view_download_full_failed_dialog:I

    :goto_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v3, "tag"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Landroid/support/v4/app/FragmentActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    :cond_3
    const-string v3, "StreamOneUp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not download image: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p6}, Lcom/google/android/apps/plus/service/ServiceResult;->getErrorCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    sget v2, Lcom/google/android/apps/plus/R$id;->photo_view_download_nonfull_failed_dialog:I

    goto :goto_2

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-static {v3, v0, p2, p4, p5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$string;->download_photo_success:I

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method public final onTagSuggestionApprovalComplete$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->this$0:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->access$2400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->onPhotoRemoved$1349ef()V

    :cond_0
    return-void
.end method

.method public final onUploadProfilePhotoComplete(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z

    return-void
.end method
