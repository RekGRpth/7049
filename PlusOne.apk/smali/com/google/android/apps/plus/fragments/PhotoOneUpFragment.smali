.class public Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "PhotoOneUpFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;
.implements Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;
.implements Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;
.implements Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;
.implements Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;
.implements Lcom/google/android/apps/plus/views/OneUpListener;
.implements Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$MyTextWatcher;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;",
        "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;",
        "Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;",
        "Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;",
        "Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;",
        "Lcom/google/android/apps/plus/views/OneUpListener;",
        "Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;"
    }
.end annotation


# static fields
.field private static sActionBarHeight:I

.field private static sCommentBackgroundPaint:Landroid/graphics/Paint;

.field private static sMaxWidth:I

.field private static sResourcesLoaded:Z


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

.field private mAlbumName:Ljava/lang/String;

.field private mAllowPlusOne:Z

.field private mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

.field private mAuthkey:Ljava/lang/String;

.field private mAutoPlay:Z

.field private mAutoRefreshDone:Z

.field private mBackgroundDesiredHeight:I

.field private mBackgroundDesiredWidth:I

.field private mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

.field private mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

.field private mCommentButton:Landroid/view/View;

.field private mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

.field private mDisableComments:Z

.field private mDownloadable:Ljava/lang/Boolean;

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

.field private mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mFooterLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mFullScreen:Z

.field private mIsPlaceholder:Z

.field private mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mListLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mListParent:Landroid/view/View;

.field private mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

.field private mOperationType:I

.field private mPendingBytes:[B

.field private mPendingRequestId:Ljava/lang/Integer;

.field private mReadProcessed:Z

.field private mRefreshRequestId:Ljava/lang/Integer;

.field private mScale:F

.field private final mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

.field private mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

.field private mTagLayout:Landroid/view/View;

.field private mTagLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTitle:Ljava/lang/String;

.field private mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

.field private mUpdateActionBar:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "pouf_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1702(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
    .param p1    # Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    return-void
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Landroid/content/Context;Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/io/File;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    :try_start_0
    const-string v0, "download"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    invoke-virtual {p2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Ljava/io/File;->length()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object v2, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v8}, Landroid/app/DownloadManager;->addCompletedDownload(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;JZ)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "StreamOneUp"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "StreamOneUp"

    const-string v2, "Could not add photo to the Downloads application"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    return v0
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sActionBarHeight:I

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    return-object v0
.end method

.method private adjustActionBarMargins(Lcom/google/android/apps/plus/views/HostActionBar;Z)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;
    .param p2    # Z

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v3, v2, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getHeight()I

    move-result v6

    neg-int v4, v6

    :goto_0
    iget v0, v2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v2, v1, v4, v3, v0}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentOffset(I)V

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz p2, :cond_2

    :goto_1
    invoke-virtual {v6, v5}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    :cond_0
    return-void

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    const/4 v5, 0x2

    goto :goto_1
.end method

.method private doReportComment(Ljava/lang/String;ZZ)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    const-string v4, "extra_comment_id"

    invoke-static {v4, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_REPORT_ABUSE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    if-eqz p3, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_undo_report_dialog_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz p3, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_undo_report_dialog_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_1
    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v2, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, p0, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "comment_id"

    invoke-virtual {v4, v5, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "delete"

    invoke-virtual {v4, v5, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "is_undo"

    invoke-virtual {v4, v5, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "pouf_report_comment"

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_report_dialog_title:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_report_dialog_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private showProgressDialog(I)V
    .locals 1
    .param p1    # I

    sget v0, Lcom/google/android/apps/plus/R$string;->post_operation_pending:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    return-void
.end method

.method private showProgressDialog(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    iput p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, p2, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "pouf_pending"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private updateChrome()V
    .locals 9

    const/16 v6, 0x8

    const/4 v3, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_8

    move v2, v3

    :goto_0
    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v2, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_9

    move v2, v4

    :goto_1
    invoke-virtual {v7, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_a

    move v2, v3

    :goto_2
    invoke-virtual {v7, v2}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v2, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_b

    move v2, v4

    :goto_3
    invoke-virtual {v7, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hasTags()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v7, :cond_c

    :goto_4
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_d

    move v2, v4

    :goto_5
    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v7

    invoke-static {v2, v7, v8}, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->shouldShowTags(Landroid/content/Context;J)Z

    move-result v1

    if-eqz v1, :cond_e

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->showTagShape()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-direct {p0, v0, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->adjustActionBarMargins(Lcom/google/android/apps/plus/views/HostActionBar;Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v3, :cond_4

    move v5, v4

    :cond_4
    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_10

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->getHeight()I

    move-result v2

    neg-int v2, v2

    :goto_7
    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentOffset(I)V

    :cond_5
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_6

    move v4, v6

    :cond_6
    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/HostActionBar;->setVisibility(I)V

    :cond_7
    return-void

    :cond_8
    move v2, v4

    goto/16 :goto_0

    :cond_9
    move v2, v5

    goto/16 :goto_1

    :cond_a
    move v2, v4

    goto/16 :goto_2

    :cond_b
    move v2, v5

    goto :goto_3

    :cond_c
    move v3, v4

    goto :goto_4

    :cond_d
    move v2, v5

    goto :goto_5

    :cond_e
    if-nez v1, :cond_f

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->hideTagShape()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    goto :goto_6

    :cond_f
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->hideTagShape()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :cond_10
    move v2, v4

    goto :goto_7
.end method

.method private updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->setLoading(Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public final doDownload(Landroid/content/Context;Z)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Z

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_2

    const/4 v0, -0x1

    :goto_1
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v0, v0, v2, v5, v5}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getResizedUrl(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    :goto_2
    if-eqz v1, :cond_4

    const-string v4, "StreamOneUp"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "StreamOneUp"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Downloading image from: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAlbumName:Ljava/lang/String;

    invoke-static {p1, v4, v1, p2, v5}, Lcom/google/android/apps/plus/service/EsService;->savePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v4, 0x13

    sget v5, Lcom/google/android/apps/plus/R$string;->download_photo_pending:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    const/16 v0, 0x800

    goto :goto_1

    :cond_3
    invoke-static {v0, v2}, Lcom/google/android/apps/plus/phone/ImageProxyUtil;->setImageUrlSize(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->download_photo_error:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {p1, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final enableImageTransforms(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->enableImageTransforms(Z)V

    return-void
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onActionButtonClicked(I)V
    .locals 4
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    const-string v0, "data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onAppInviteButtonClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedDeepLink;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/DbEmbedDeepLink;

    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->getFullScreen()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Activity must implement PhotoOneUpCallbacks"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "extra_gaia_id"

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_AUTHOR:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v0, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 23
    .param p1    # Landroid/view/View;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v18

    sget v3, Lcom/google/android/apps/plus/R$id;->footer_post_button:I

    move/from16 v0, v18

    if-ne v0, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_POST_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/api/ApiUtils;->buildPostableString$6d7f0b14(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAuthkey:Ljava/lang/String;

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/plus/service/EsService;->createPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v3, 0x20

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$id;->background:I

    move/from16 v0, v18

    if-ne v0, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideo()Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideoReady()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVideoData()[B

    move-result-object v13

    invoke-static/range {v8 .. v13}, Lcom/google/android/apps/plus/phone/Intents;->getVideoViewActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)Landroid/content/Intent;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    sget v3, Lcom/google/android/apps/plus/R$string;->photo_view_video_not_ready:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/4 v4, 0x1

    move-object/from16 v0, v22

    invoke-static {v3, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isPanorama()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/phone/Intents;->getViewPanoramaActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;)Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->toggleFullScreen()V

    goto/16 :goto_0

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$id;->tag_approve:I

    move/from16 v0, v18

    if-ne v0, v3, :cond_8

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_shape_id:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_is_suggestion:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_7

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_gaiaid:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-static/range {v8 .. v14}, Lcom/google/android/apps/plus/service/EsService;->suggestedTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    :goto_1
    const/16 v3, 0x31

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    const/16 v16, 0x1

    invoke-static/range {v11 .. v16}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_1

    :cond_8
    sget v3, Lcom/google/android/apps/plus/R$id;->tag_deny:I

    move/from16 v0, v18

    if-ne v0, v3, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_shape_id:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Long;

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_is_suggestion:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_9

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_gaiaid:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static/range {v8 .. v14}, Lcom/google/android/apps/plus/service/EsService;->suggestedTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    :goto_2
    const/16 v3, 0x32

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    const/16 v16, 0x0

    invoke-static/range {v11 .. v16}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    goto :goto_2
.end method

.method public final onCommentButtonClicked()V
    .locals 0

    return-void
.end method

.method public final onCommentClicked(Lcom/google/android/apps/plus/views/StreamOneUpCommentView;)V
    .locals 13
    .param p1    # Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    const/4 v11, 0x5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getAuthorId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v2

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v3

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v11}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v11}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getPlusOneByMe()Z

    move-result v7

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->isFlagged()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v7, :cond_3

    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_plusminus:I

    :goto_0
    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v9, 0x26

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v2, :cond_4

    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_edit:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v9, 0x25

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    if-nez v3, :cond_1

    if-eqz v2, :cond_2

    :cond_1
    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_delete:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v9, 0x21

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v5, v9, [Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_options_title:I

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v9, 0x0

    invoke-virtual {v0, p0, v9}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "comment_action"

    invoke-virtual {v9, v10, v4}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "comment_id"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "comment_content"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->getCommentContent()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "plus_one_by_me"

    invoke-virtual {v9, v10, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v9

    const-string v10, "photo_id"

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v11

    invoke-virtual {v9, v10, v11, v12}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v9

    const-string v10, "pouf_delete_comment"

    invoke-virtual {v0, v9, v10}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void

    :cond_3
    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_plusone:I

    goto/16 :goto_0

    :cond_4
    if-eqz v1, :cond_5

    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_undo_report:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v9, 0x23

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_5
    sget v9, Lcom/google/android/apps/plus/R$string;->stream_one_up_comment_option_report:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_6

    const/16 v9, 0x24

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    const/16 v9, 0x22

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v3, -0x1

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v2, "photo_width"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundDesiredWidth:I

    const-string v2, "photo_height"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundDesiredHeight:I

    const-string v2, "allow_plusone"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAllowPlusOne:Z

    const-string v2, "disable_photo_comments"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDisableComments:Z

    if-eqz p1, :cond_3

    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "pending_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v2, "refresh_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "refresh_request_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    :cond_1
    const-string v2, "audience_data"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AudienceData;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    const-string v2, "flagged_comments"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    const-string v2, "operation_type"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    const-string v2, "read_processed"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mReadProcessed:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoPlay:Z

    const-string v2, "photo_ref"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    const-string v2, "is_placeholder"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mIsPlaceholder:Z

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mUpdateActionBar:Z

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$2;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$3;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-void

    :cond_3
    const-string v2, "photo_ref"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    const-string v2, "is_placeholder"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mIsPlaceholder:Z

    const-string v2, "refresh"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "force_load_id"

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    :cond_5
    const-string v2, "auth_key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAuthkey:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    sparse-switch p1, :sswitch_data_0

    move-object v0, v6

    :goto_0
    return-object v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDisableComments:Z

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :sswitch_1
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_SHAPES_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    new-instance v2, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/plus/views/PhotoTagScroller$PhotoShapeQuery;->PROJECTION:[Ljava/lang/String;

    const-string v8, "shape_id"

    move-object v7, v6

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1efab34d -> :sswitch_0
        0x1fd2f7ba -> :sswitch_1
    .end sparse-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v9, 0x1

    const/4 v2, 0x0

    const/4 v10, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    sget-boolean v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sResourcesLoaded:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v0, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_max_width:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sMaxWidth:I

    sget v0, Lcom/google/android/apps/plus/R$dimen;->host_action_bar_height:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sActionBarHeight:I

    sput-boolean v9, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sResourcesLoaded:Z

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sCommentBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sCommentBackgroundPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$layout;->photo_one_up_fragment:I

    invoke-virtual {p1, v0, p2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    sget v0, Lcom/google/android/apps/plus/R$id;->list_parent:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->list_expander:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x102000a

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/StreamOneUpListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sCommentBackgroundPaint:Landroid/graphics/Paint;

    move-object v3, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    sget v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sMaxWidth:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setMaxWidth(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setItemsCanFocus(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v0, :cond_2

    move-object v0, v2

    :goto_0
    sget v3, Lcom/google/android/apps/plus/R$id;->stage:I

    invoke-virtual {v8, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-nez v0, :cond_3

    if-eqz v3, :cond_1

    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    :goto_1
    sget v0, Lcom/google/android/apps/plus/R$id;->one_up_tag_layout:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->one_up_tag_list:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setHeaderView(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->setExternalOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->footer:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    sget v0, Lcom/google/android/apps/plus/R$id;->footer_text:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setLayoutListener(Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications$LayoutListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    sget v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->sMaxWidth:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->setMaxWidth(I)V

    new-instance v6, Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v6, v1, v0, v3}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/CircleNameResolver;->initLoader()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, p0, v3, v2, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->footer_post_button:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v9

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setEnabled(Z)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$MyTextWatcher;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-direct {v0, v3, v10}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$MyTextWatcher;-><init>(Landroid/view/View;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    new-instance v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$4;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;)V

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->touch_handler:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setBackground(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setScrollView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setTagLayout(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->setActionBar(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v3, 0x1efab34d

    invoke-virtual {v0, v3, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-direct {v0, v2, v10, v10}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;-><init>(Landroid/view/View;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-direct {v0, v2, v10, v9}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;-><init>(Landroid/view/View;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateChrome()V

    return-object v8

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    if-nez v3, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$id;->stage_media:I

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->background:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mIsPlaceholder:Z

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->init(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnImageListener(Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->enableImageTransforms(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->list_expander:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    invoke-virtual {v3}, Landroid/view/View;->invalidate()V

    goto/16 :goto_1

    :cond_4
    move v0, v10

    goto/16 :goto_2
.end method

.method public final onDestroyView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->destroy()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->destroy()V

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDestroyView()V

    return-void
.end method

.method public final onDetach()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDetach()V

    return-void
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 16
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    const-string v1, "comment_action"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v14

    if-nez v14, :cond_1

    const-string v1, "StreamOneUp"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "StreamOneUp"

    const-string v2, "No actions for comment option dialog"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v1

    move/from16 v0, p1

    if-lt v0, v1, :cond_2

    const-string v1, "StreamOneUp"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "StreamOneUp"

    const-string v2, "Option selected outside the action list"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v1, "comment_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v1, "comment_content"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "plus_one_by_me"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    const-string v1, "photo_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v1, "extra_comment_id"

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_DELETE_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->menu_delete_comment:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->comment_delete_question:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->ok:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v7, Lcom/google/android/apps/plus/R$string;->cancel:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v2, v3, v7}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "pouf_delete_comment"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const/4 v7, 0x0

    if-nez v15, :cond_3

    const/4 v8, 0x1

    :goto_1
    invoke-static/range {v1 .. v8}, Lcom/google/android/apps/plus/service/EsService;->plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I

    goto/16 :goto_0

    :cond_3
    const/4 v8, 0x0

    goto :goto_1

    :pswitch_2
    const-string v1, "extra_comment_id"

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_EDIT_COMMENT:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v13

    move-object v10, v6

    invoke-static/range {v7 .. v13}, Lcom/google/android/apps/plus/phone/Intents;->getEditCommentActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_3
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :pswitch_4
    const/4 v1, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :pswitch_5
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doReportComment(Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x21
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    const-string v0, "pouf_report_photo"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1, v6, v7, v2}, Lcom/google/android/apps/plus/service/EsService;->reportPhotoAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x11

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "pouf_delete_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-string v5, "comment_id"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v1, v3, v5}, Lcom/google/android/apps/plus/service/EsService;->deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto :goto_0

    :cond_2
    const-string v0, "pouf_report_comment"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v3, "comment_id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v8, "delete"

    invoke-virtual {p1, v8, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string v8, "is_undo"

    invoke-virtual {p1, v8, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->reportPhotoComment$3486cdbb(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;ZZ)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x22

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto :goto_0

    :cond_3
    const-string v0, "pouf_delete_tag"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v0, 0x30

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(I)V

    goto/16 :goto_0
.end method

.method public final onFullScreenChanged$25decb5(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x2

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->showTagShape()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mUpdateActionBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/HostActionBar;->getHeight()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentOffset(I)V

    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mUpdateActionBar:Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v3, :cond_8

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooterAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->animate(Z)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->hideTagShape()V

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v1, v2

    goto :goto_2
.end method

.method public final onImageLoadFinished$46d55081()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final onImageScaled(F)V
    .locals 2
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mScale:F

    const/high16 v1, 0x3f800000

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->toggleFullScreen()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onInterceptMoveLeft$2548a39()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVisualScale()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mScale:F

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mScale:F

    const/high16 v2, 0x3f800000

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->getTargetView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onInterceptMoveRight$2548a39()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVisualScale()F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mScale:F

    iget v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mScale:F

    const/high16 v2, 0x3f800000

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTouchHandler:Lcom/google/android/apps/plus/views/OneUpTouchHandler;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OneUpTouchHandler;->getTargetView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onJoinHangout(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/DbEmbedHangout;

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 13
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x7

    const/16 v6, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_1b

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1b

    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v2, v9

    :goto_1
    if-eqz v2, :cond_9

    move v3, v8

    :goto_2
    if-eqz v3, :cond_1a

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    const-string v4, "FINAL"

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v8

    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setVideoBlob([B)V

    const/16 v2, 0x12

    invoke-interface {p2, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_b

    move-object v2, v9

    :goto_4
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    invoke-interface {p2, v5}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_d

    move-object v2, v9

    :goto_5
    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAlbumName:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "PLACEHOLDER"

    invoke-static {v5, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_3

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    :cond_3
    const-string v0, "profile"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoRefreshDone:Z

    if-nez v0, :cond_e

    move v10, v8

    :goto_6
    iput-boolean v8, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoRefreshDone:Z

    const/16 v0, 0xa

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDisableComments:Z

    if-nez v2, :cond_f

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_f

    move v11, v8

    :goto_7
    if-nez v12, :cond_5

    const/16 v0, 0x9

    invoke-interface {p2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x9

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/16 v0, 0x15

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_4

    move v1, v8

    :cond_4
    if-eqz v3, :cond_10

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_8
    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getDisplayName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v1, v12}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->init(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    iput-boolean v12, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mIsPlaceholder:Z

    :cond_5
    move v1, v11

    move v0, v10

    :goto_9
    if-nez v0, :cond_13

    if-eqz p2, :cond_13

    :cond_6
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_12

    :goto_a
    if-eqz v8, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->changeCursor(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->setFlaggedComments(Ljava/util/HashSet;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setEnabled(Z)V

    if-eqz v1, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->compose_comment_hint:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(I)V

    :goto_b
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const v1, 0x1fd2f7ba

    invoke-virtual {v0, v1, v9, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_0

    :cond_8
    const/16 v0, 0xe

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_1

    :cond_9
    move v3, v1

    goto/16 :goto_2

    :cond_a
    move v0, v1

    goto/16 :goto_3

    :cond_b
    const/16 v2, 0x12

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v8, :cond_c

    move v2, v8

    :goto_c
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_4

    :cond_c
    move v2, v1

    goto :goto_c

    :cond_d
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    :cond_e
    move v10, v1

    goto/16 :goto_6

    :cond_f
    move v11, v1

    goto/16 :goto_7

    :cond_10
    if-eqz v1, :cond_11

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto/16 :goto_8

    :cond_11
    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto/16 :goto_8

    :cond_12
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_6

    :cond_13
    move v8, v0

    goto :goto_a

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCommentText:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->compose_comment_not_allowed_hint:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setHint(I)V

    goto :goto_b

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    sget v3, Lcom/google/android/apps/plus/R$id;->one_up_tags:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v4, v5, p2, v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->bind(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->hasTags()Z

    move-result v0

    if-eqz v0, :cond_19

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-direct {v0, p0, v3, v8, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$5;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Landroid/view/View;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v0, :cond_16

    move v0, v1

    :goto_d
    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayoutListener:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/plus/util/PlusOneViewUtils;->shouldShowTags(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_17

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-nez v3, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->showTagShape()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_15
    :goto_e
    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    goto/16 :goto_0

    :cond_16
    const/4 v0, 0x2

    goto :goto_d

    :cond_17
    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->hideTagShape()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    goto :goto_e

    :cond_18
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->hideTagShape()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_e

    :cond_19
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagLayout:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    goto :goto_e

    :cond_1a
    move v0, v1

    goto/16 :goto_3

    :cond_1b
    move v0, v1

    goto/16 :goto_9

    nop

    :sswitch_data_0
    .sparse-switch
        0x1efab34d -> :sswitch_0
        0x1fd2f7ba -> :sswitch_1
    .end sparse-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onLocationClick$75c560e7(Lcom/google/android/apps/plus/content/DbLocation;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/DbLocation;

    return-void
.end method

.method public final onMeasured(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpAdapter;->setContainerHeight(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFooter:Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/LinearLayoutWithLayoutNotifications;->getMeasuredHeight()I

    move-result v0

    new-instance v1, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$6;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;I)V

    invoke-static {v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final onMediaClick(Lcom/google/android/apps/plus/api/MediaRef;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 8
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v4, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v4, Lcom/google/android/apps/plus/R$id;->feedback:I

    if-ne v0, v4, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    move v1, v2

    goto :goto_0

    :cond_2
    sget v4, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    if-ne v0, v4, :cond_3

    sget v3, Lcom/google/android/apps/plus/R$string;->menu_report_photo:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->report_photo_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v3

    invoke-virtual {v3, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v4, "pouf_report_photo"

    invoke-virtual {v3, v1, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v1, v2

    goto :goto_0

    :cond_3
    sget v4, Lcom/google/android/apps/plus/R$id;->share:I

    if-ne v0, v4, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, v1}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    move v1, v2

    goto :goto_0

    :cond_4
    sget v4, Lcom/google/android/apps/plus/R$id;->set_profile_photo:I

    if-ne v0, v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {v1, v4, v3, v5, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPhotoPickerIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v1, v2

    goto :goto_0

    :cond_5
    sget v4, Lcom/google/android/apps/plus/R$id;->set_wallpaper:I

    if-ne v0, v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-nez v4, :cond_7

    :goto_1
    if-eqz v3, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->set_wallpaper_photo_success:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->set_wallpaper_photo_error:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x14

    sget v7, Lcom/google/android/apps/plus/R$string;->set_wallpaper_photo_pending:I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;

    invoke-direct {v6, p0, v5, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;-><init>(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;Ljava/lang/String;Ljava/lang/String;)V

    new-array v4, v2, [Landroid/graphics/Bitmap;

    aput-object v3, v4, v1

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$7;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_6
    move v1, v2

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_1

    :cond_8
    sget v3, Lcom/google/android/apps/plus/R$id;->delete:I

    if-ne v0, v3, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-interface {v1, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->deletePhoto(Lcom/google/android/apps/plus/api/MediaRef;)V

    move v1, v2

    goto/16 :goto_0

    :cond_9
    sget v3, Lcom/google/android/apps/plus/R$id;->download:I

    if-ne v0, v3, :cond_a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doDownload(Landroid/content/Context;Z)V

    move v1, v2

    goto/16 :goto_0

    :cond_a
    sget v3, Lcom/google/android/apps/plus/R$id;->refresh:I

    if-ne v0, v3, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->refresh()V

    move v1, v2

    goto/16 :goto_0

    :cond_b
    sget v3, Lcom/google/android/apps/plus/R$id;->remove_tag:I

    if-ne v0, v3, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$string;->menu_remove_tag:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->remove_tag_question:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v3

    invoke-virtual {v3, p0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v4, "pouf_delete_tag"

    invoke-virtual {v3, v1, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    move v1, v2

    goto/16 :goto_0
.end method

.method public final onPause()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPause()V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->onStop()V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStop()V

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->removeScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->removeMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onPlaceClick(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    invoke-static {v1, v2, p1, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final onPlusOne(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/DbPlusOneData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    invoke-static {v2, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsService;->isPhotoPlusOnePending(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v6, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object v3, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->photoPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JZ)I

    :cond_1
    return-void

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_actionbar_reshare:I

    sget v1, Lcom/google/android/apps/plus/R$string;->from_your_phone_initiate_share:I

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-direct {v0, p1, v2, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;-><init>(Landroid/view/View;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mUpdateActionBar:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->adjustActionBarMargins(Lcom/google/android/apps/plus/views/HostActionBar;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentState(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mActionBarAnimator:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->getHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->setCurrentOffset(I)V

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setVisibility(I)V

    :goto_0
    return-void

    :cond_3
    invoke-virtual {p1, v3}, Lcom/google/android/apps/plus/views/HostActionBar;->setVisibility(I)V

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 17
    .param p1    # Landroid/view/Menu;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    move-object/from16 v0, p0

    invoke-interface {v15, v0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v15

    if-nez v15, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    if-nez v15, :cond_5

    const/4 v13, 0x0

    :goto_1
    if-eqz v13, :cond_6

    const/4 v14, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    :goto_3
    const-wide/16 v15, 0x0

    cmp-long v15, v9, v15

    if-nez v15, :cond_8

    invoke-static {v12}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_8

    const/4 v4, 0x1

    :goto_4
    const-wide/16 v15, 0x0

    cmp-long v15, v9, v15

    if-eqz v15, :cond_9

    invoke-static {v12}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v15

    if-nez v15, :cond_9

    const/4 v5, 0x1

    :goto_5
    const-wide/16 v15, 0x0

    cmp-long v15, v9, v15

    if-nez v15, :cond_a

    if-eqz v12, :cond_a

    const/4 v7, 0x1

    :goto_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v15, v8}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v15

    if-nez v15, :cond_1

    if-nez v8, :cond_b

    invoke-static {v12}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v15

    if-eqz v15, :cond_b

    :cond_1
    const/4 v6, 0x1

    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v15

    const-string v16, "stream_id"

    invoke-virtual/range {v15 .. v16}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v15, "camerasync"

    invoke-virtual {v15, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v7, :cond_2

    if-eqz v5, :cond_d

    if-nez v6, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    if-eqz v15, :cond_c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mDownloadable:Ljava/lang/Boolean;

    invoke-virtual {v15}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-eqz v15, :cond_c

    const/4 v15, 0x1

    :goto_8
    if-eqz v15, :cond_d

    :cond_2
    const/4 v2, 0x1

    :goto_9
    if-eqz v6, :cond_e

    if-nez v5, :cond_3

    if-eqz v4, :cond_e

    :cond_3
    const/4 v1, 0x1

    :goto_a
    sget v15, Lcom/google/android/apps/plus/R$id;->share:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->set_profile_photo:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v16

    if-nez v6, :cond_4

    if-eqz v14, :cond_f

    :cond_4
    const/4 v15, 0x1

    :goto_b
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->set_wallpaper:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->delete:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->download:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->report_abuse:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v16

    if-nez v6, :cond_10

    if-eqz v5, :cond_10

    const/4 v15, 0x1

    :goto_c
    move-object/from16 v0, v16

    invoke-interface {v0, v15}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->remove_tag:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    invoke-interface {v15, v14}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v15, Lcom/google/android/apps/plus/R$id;->feedback:I

    move-object/from16 v0, p1

    invoke-interface {v0, v15}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v15

    const/16 v16, 0x1

    invoke-interface/range {v15 .. v16}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTagScroll:Lcom/google/android/apps/plus/views/PhotoTagScroller;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->getMyApprovedShapeId()Ljava/lang/Long;

    move-result-object v13

    goto/16 :goto_1

    :cond_6
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_5

    :cond_a
    const/4 v7, 0x0

    goto/16 :goto_6

    :cond_b
    const/4 v6, 0x0

    goto/16 :goto_7

    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_8

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_e
    const/4 v1, 0x0

    goto/16 :goto_a

    :cond_f
    const/4 v15, 0x0

    goto :goto_b

    :cond_10
    const/4 v15, 0x0

    goto :goto_c
.end method

.method public final onReshareClicked()V
    .locals 0

    return-void
.end method

.method public final onResume()V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onResume()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->onStart()V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    :goto_0
    if-ltz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mListView:Lcom/google/android/apps/plus/views/StreamOneUpListView;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Lcom/google/android/apps/plus/views/OneUpBaseView;

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/views/OneUpBaseView;->onStart()V

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v4, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->addScreenListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnScreenListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v4, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->addMenuItemListener(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$OnMenuItemListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mServiceListener:Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->handleServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)Z
    invoke-static {v4, v5, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;->access$1100(Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment$ServiceListener;ILcom/google/android/apps/plus/service/ServiceResult;)Z

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->invalidateActionBar()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v4, :cond_8

    const-string v4, "StreamOneUp"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "StreamOneUp"

    const-string v5, "Both a pending profile image and an existing request"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_1
    iput-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v4}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->getFullScreen()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    :cond_7
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFullScreen:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mUpdateActionBar:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateChrome()V

    return-void

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingBytes:[B

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5, v1}, Lcom/google/android/apps/plus/service/EsService;->uploadProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    const/16 v4, 0x15

    sget v5, Lcom/google/android/apps/plus/R$string;->set_profile_photo_pending:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->showProgressDialog(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    const-string v1, "pending_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    const-string v1, "refresh_request_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_2

    const-string v1, "audience_data"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAudienceData:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    const-string v1, "flagged_comments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    :cond_3
    const-string v1, "photo_ref"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "operation_type"

    iget v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mOperationType:I

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v1, "read_processed"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mReadProcessed:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "is_placeholder"

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mIsPlaceholder:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "auto_play_music"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAutoPlay:Z

    return-void
.end method

.method public final onSkyjamBuyClick(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onSkyjamListenClick(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onSourceAppContentClick(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 9
    .param p1    # Landroid/text/style/URLSpan;

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    const-string v6, "https://plus.google.com/s/%23"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "#"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/16 v7, 0x1d

    invoke-virtual {v5, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v6, v4}, Lcom/google/android/apps/plus/phone/Intents;->getPostSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/phone/Intents;->isProfileUrl(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "pid="

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/phone/Intents;->getParameter(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "extra_gaia_id"

    invoke-static {v6, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    sget-object v6, Lcom/google/android/apps/plus/analytics/OzActions;->ONE_UP_SELECT_PERSON:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v6, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v8, 0x0

    invoke-static {v6, v7, v5, v8}, Lcom/google/android/apps/plus/phone/Intents;->viewContent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSquareClick(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public final onViewActivated()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->isFragmentActive(Landroid/support/v4/app/Fragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mCallback:Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;

    invoke-interface {v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpCallbacks;->onFragmentVisible(Landroid/support/v4/app/Fragment;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->doAnimate(Z)V

    :cond_1
    return-void
.end method

.method public final recordNavigationAction()V
    .locals 0

    return-void
.end method

.method public final refresh()V
    .locals 6

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getPhotoSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->updateProgressIndicator(Lcom/google/android/apps/plus/views/HostActionBar;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mBackgroundRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mAuthkey:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mRefreshRequestId:Ljava/lang/Integer;

    goto :goto_1
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->mTitle:Ljava/lang/String;

    return-void
.end method
