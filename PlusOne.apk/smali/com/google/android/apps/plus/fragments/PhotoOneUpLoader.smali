.class public final Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "PhotoOneUpLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$LeftoverQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentLoadingQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentCountQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mDisableComments:Z

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mOwnerId:Ljava/lang/String;

.field private final mPhotoId:J

.field private final mPhotoUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z

    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-wide p3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mDisableComments:Z

    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 24

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    :goto_0
    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mDisableComments:Z

    if-nez v3, :cond_1

    if-eqz v19, :cond_6

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_6

    const/16 v3, 0xc

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    const/4 v3, -0x1

    move-object/from16 v0, v19

    invoke-interface {v0, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_1
    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentCountQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x2

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_7

    move-object v11, v9

    :cond_0
    :goto_2
    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "create_time"

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_8

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v13

    :goto_3
    if-eq v10, v13, :cond_1

    new-instance v14, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoCommentLoadingQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v14, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    move-object v3, v14

    check-cast v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const v5, 0x7ffffffd

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    :cond_1
    new-instance v16, Landroid/database/MatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$LeftoverQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const v5, 0x7ffffffc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v3

    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    new-instance v20, Landroid/database/MergeCursor;

    const/4 v3, 0x5

    new-array v3, v3, [Landroid/database/Cursor;

    const/4 v5, 0x0

    aput-object v19, v3, v5

    const/4 v5, 0x1

    aput-object v11, v3, v5

    const/4 v5, 0x2

    aput-object v12, v3, v5

    const/4 v5, 0x3

    aput-object v14, v3, v5

    const/4 v5, 0x4

    aput-object v16, v3, v5

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v20

    :cond_2
    new-instance v19, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoUrl:Ljava/lang/String;

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoData(Landroid/content/Context;Landroid/net/Uri;)Lcom/google/api/services/plusi/model/DataVideo;

    move-result-object v22

    if-eqz v22, :cond_3

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v23

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v17

    :goto_5
    if-eqz v17, :cond_5

    const/4 v15, 0x0

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v17

    invoke-static {v3, v5, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->getUserName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    sget-object v3, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    array-length v3, v3

    new-array v0, v3, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/4 v3, 0x0

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v21, v3

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v21, v3

    const/4 v3, 0x2

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v21, v3

    const/4 v3, 0x3

    aput-object v17, v21, v3

    const/4 v3, 0x4

    aput-object v18, v21, v3

    const/4 v3, 0x6

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v21, v3

    const/4 v3, 0x7

    const/4 v5, 0x0

    aput-object v5, v21, v3

    const/16 v3, 0x8

    const/4 v5, 0x0

    aput-object v5, v21, v3

    const/16 v3, 0x9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoUrl:Ljava/lang/String;

    aput-object v5, v21, v3

    const/16 v3, 0xa

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v21, v3

    const/16 v3, 0xb

    const/4 v5, 0x0

    aput-object v5, v21, v3

    const/16 v3, 0xc

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v21, v3

    const/16 v3, 0xd

    const/4 v5, 0x0

    aput-object v5, v21, v3

    const/16 v3, 0xe

    aput-object v23, v21, v3

    const/16 v3, 0xf

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v21, v3

    const/16 v3, 0x10

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v21, v3

    const/16 v3, 0x11

    const-string v5, "ORIGINAL"

    aput-object v5, v21, v3

    const/16 v3, 0x12

    aput-object v15, v21, v3

    const/16 v3, 0x13

    const/4 v5, 0x0

    aput-object v5, v21, v3

    const/16 v3, 0x14

    const/4 v5, 0x0

    aput-object v5, v21, v3

    move-object/from16 v3, v19

    check-cast v3, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_3
    const/16 v23, 0x0

    goto/16 :goto_4

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mOwnerId:Ljava/lang/String;

    move-object/from16 v17, v0

    goto/16 :goto_5

    :cond_5
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    goto/16 :goto_6

    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_1

    :cond_7
    if-eqz v9, :cond_0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_3
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final onAbandon()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onReset()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->cancelLoad()Z

    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onReset()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->onAbandon()V

    return-void
.end method

.method protected final onStartLoading()V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onStartLoading()V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_COMMENTS_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mPhotoId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v2, v1, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v2, v0, v6, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/PhotoOneUpLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onStopLoading()V
    .locals 0

    return-void
.end method
