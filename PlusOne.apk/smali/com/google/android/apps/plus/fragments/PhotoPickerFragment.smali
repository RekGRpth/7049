.class public Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "PhotoPickerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;


# static fields
.field private static sPhotoSize:Ljava/lang/Integer;


# instance fields
.field private mCoverPhotoCoordinates:Landroid/graphics/RectF;

.field private mCropMode:I

.field private mFullBleedPhotoOffset:Ljava/lang/Integer;

.field private mIntent:Landroid/content/Intent;

.field private mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isPhotoBound()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onAttach(Landroid/app/Activity;)V

    sget-object v2, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->sPhotoSize:Ljava/lang/Integer;

    if-nez v2, :cond_0

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v2, "window"

    invoke-virtual {p1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->sPhotoSize:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    sget v11, Lcom/google/android/apps/plus/R$id;->cancel_button:I

    if-ne v3, v11, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Landroid/app/Activity;->setResult(I)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v11, Lcom/google/android/apps/plus/R$id;->accept_button:I

    if-ne v3, v11, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v11

    if-eqz v11, :cond_8

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    :goto_1
    if-eqz v8, :cond_2

    const-string v11, "photo_url"

    invoke-virtual {v9, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v6

    const-wide/16 v11, 0x0

    cmp-long v11, v6, v11

    if-eqz v11, :cond_3

    const-string v11, "photo_id"

    invoke-virtual {v9, v11, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :cond_3
    iget v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    if-eqz v11, :cond_7

    const-string v11, "photo_picker_crop_mode"

    iget v12, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    invoke-virtual {v9, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-wide/16 v11, 0x0

    cmp-long v11, v6, v11

    if-eqz v11, :cond_4

    iget v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_5

    :cond_4
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getCroppedPhoto()Landroid/graphics/Bitmap;

    move-result-object v2

    const/16 v11, 0x5a

    const/4 v12, 0x0

    invoke-static {v2, v11, v12}, Lcom/google/android/apps/plus/util/ImageUtils;->compressBitmap(Landroid/graphics/Bitmap;IZ)[B

    move-result-object v4

    const-string v11, "data"

    invoke-virtual {v9, v11, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_5
    iget v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_9

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v11, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getCoverPhotoCoordinates(Landroid/graphics/RectF;)V

    const-string v11, "coordinates"

    invoke-virtual {v9, v11, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_6
    :goto_2
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    const-string v11, "115239603441691718952"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    const-string v11, "is_gallery_photo"

    const/4 v12, 0x1

    invoke-virtual {v9, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_7
    const/4 v11, -0x1

    invoke-virtual {v0, v11, v9}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    :cond_8
    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    :cond_9
    iget v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_6

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getFullBleedTopOffset()I

    move-result v10

    const-string v11, "top_offset"

    invoke-virtual {v9, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.apps.plus.PhotoViewFragment.INTENT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "mediarefs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "photo_picker_crop_mode"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "top_offset"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "top_offset"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mFullBleedPhotoOffset:Ljava/lang/Integer;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "coordinates"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    const-string v1, "coordinates"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    :cond_2
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    sget v4, Lcom/google/android/apps/plus/R$layout;->photo_picker_fragment:I

    invoke-super {p0, p1, p2, p3, v4}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->photo_header_view:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iput-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->init(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->enableImageTransforms(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setCropMode(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mFullBleedPhotoOffset:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setCropModeFullBleedOffset(Ljava/lang/Integer;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setCropModeCoverCoordinates(Landroid/graphics/RectF;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mPhotoView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnImageListener(Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->showEmptyViewProgress(Landroid/view/View;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->cancel_button:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget v4, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mCropMode:I

    if-eqz v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->photo_picker_save:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    sget v4, Lcom/google/android/apps/plus/R$id;->accept_button:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ActionButton;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ActionButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$style;->AlbumView_BottomActionBar_ActionButton_Disabled:I

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/plus/views/ActionButton;->setTextAppearance(Landroid/content/Context;I)V

    return-object v3

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$string;->photo_picker_select:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final onImageLoadFinished$46d55081()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->accept_button:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ActionButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ActionButton;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$style;->AlbumView_BottomActionBar_ActionButton_Disabled:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/views/ActionButton;->setTextAppearance(Landroid/content/Context;I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->photo_network_error:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->setupEmptyView(Landroid/view/View;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->showEmptyView(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ActionButton;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$style;->AlbumView_BottomActionBar_ActionButton:I

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/plus/views/ActionButton;->setTextAppearance(Landroid/content/Context;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onImageScaled(F)V
    .locals 0
    .param p1    # F

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.apps.plus.PhotoViewFragment.INTENT"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method
