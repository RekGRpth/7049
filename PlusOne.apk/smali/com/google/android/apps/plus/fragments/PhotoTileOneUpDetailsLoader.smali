.class public final Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "PhotoTileOneUpDetailsLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader$PhotoCommentQuery;,
        Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader$PhotoAuthorQuery;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 13

    const/4 v12, 0x3

    const/4 v6, 0x5

    const/4 v11, 0x2

    const/4 v7, 0x1

    const/4 v8, 0x0

    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader$PhotoAuthorQuery;->PROJECTION:[Ljava/lang/String;

    array-length v5, v5

    new-array v3, v5, [Ljava/lang/Object;

    new-instance v4, Landroid/database/MatrixCursor;

    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader$PhotoAuthorQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v4, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v11

    const-string v5, "Rick Ross"

    aput-object v5, v3, v12

    const/4 v5, 0x4

    const-string v9, "https://lh3.googleusercontent.com/-B5zM7UR_iIg/AAAAAAAAAAI/AAAAAAAApFM/CFAaQk_km0g/photo.jpg"

    aput-object v9, v3, v5

    const-string v5, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."

    aput-object v5, v3, v6

    const/4 v5, 0x6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v3, v5

    invoke-virtual {v4, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader$PhotoCommentQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v5}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    const/16 v5, 0x14

    if-ge v1, v5, :cond_4

    sget-object v5, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader$PhotoCommentQuery;->PROJECTION:[Ljava/lang/String;

    array-length v5, v5

    new-array v3, v5, [Ljava/lang/Object;

    add-int/lit8 v5, v1, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v12

    const/4 v9, 0x4

    rem-int/lit8 v5, v1, 0x2

    if-nez v5, :cond_0

    const-string v5, "John Doe"

    :goto_1
    aput-object v5, v3, v9

    const-string v5, "https://lh3.googleusercontent.com/-B5zM7UR_iIg/AAAAAAAAAAI/AAAAAAAApFM/CFAaQk_km0g/photo.jpg"

    aput-object v5, v3, v6

    const/4 v5, 0x6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v3, v5

    const/4 v9, 0x7

    rem-int/lit8 v5, v1, 0x3

    if-nez v5, :cond_1

    const-string v5, "This photo is cool"

    :goto_2
    aput-object v5, v3, v9

    const/16 v9, 0x9

    rem-int/lit8 v5, v1, 0x5

    if-nez v5, :cond_2

    move v5, v6

    :goto_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v9

    const/16 v9, 0xa

    rem-int/lit8 v5, v1, 0x4

    if-nez v5, :cond_3

    move v5, v7

    :goto_4
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v9

    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v5, "Jennifer Doe"

    goto :goto_1

    :cond_1
    const-string v5, "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."

    goto :goto_2

    :cond_2
    const/16 v5, 0xe8

    goto :goto_3

    :cond_3
    move v5, v8

    goto :goto_4

    :cond_4
    new-instance v2, Landroid/database/MergeCursor;

    new-array v5, v11, [Landroid/database/Cursor;

    aput-object v4, v5, v8

    aput-object v0, v5, v7

    invoke-direct {v2, v5}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v2
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpDetailsLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
