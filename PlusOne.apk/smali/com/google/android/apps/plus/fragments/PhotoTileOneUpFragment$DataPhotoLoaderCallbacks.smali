.class final Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;
.super Ljava/lang/Object;
.source "PhotoTileOneUpFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataPhotoLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/api/services/plusi/model/DataPhoto;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation

    if-nez p2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    const-string v3, "view_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "tile_id"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "account"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v3, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getSafeContext()Landroid/content/Context;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->access$100(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v2, v1, v0}, Lcom/google/android/apps/plus/phone/SinglePhotoTileLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 7
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v1, 0x0

    check-cast p2, Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v2, p2, Lcom/google/api/services/plusi/model/DataPhoto;->plusOne:Lcom/google/api/services/plusi/model/DataPlusOne;

    if-eqz v2, :cond_0

    iget-object v0, v2, Lcom/google/api/services/plusi/model/DataPlusOne;->globalCount:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mPlusOneButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->access$200(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/google/api/services/plusi/model/DataPlusOne;->isPlusonedByViewer:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mPlusOneButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->access$200(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/widget/Button;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {v3, v4, v1}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;B)V

    invoke-virtual {v0, v6, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mPlusOneButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->access$200(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/widget/Button;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$color;->transparent_black_title_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setBackgroundColor(I)V

    goto :goto_2
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
