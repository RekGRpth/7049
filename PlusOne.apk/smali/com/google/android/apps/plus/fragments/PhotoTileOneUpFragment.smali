.class public Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "PhotoTileOneUpFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;
.implements Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;
.implements Lcom/google/android/apps/plus/views/TileOneUpListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$PhotoDetailLoaderCallbacks;,
        Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;
    }
.end annotation


# static fields
.field private static sCommentListOpaqueScrollPosition:I

.field private static sTranslucentLayerScrollPosition:I


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;

.field private mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

.field private mDetailsParent:Landroid/view/View;

.field private mPlusOneButton:Landroid/widget/Button;

.field private mTranslucentLayer:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mPlusOneButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;)Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;

    return-object v0
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_detail_list_opaque_scroll_position:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->sCommentListOpaqueScrollPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->tile_photo_one_up_translucent_layer_scroll_position:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->sTranslucentLayerScrollPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment$DataPhotoLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;B)V

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v2

    sget v9, Lcom/google/android/apps/plus/R$layout;->photo_tile_one_up_fragment:I

    invoke-virtual {p1, v9, p2, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    sget v9, Lcom/google/android/apps/plus/R$id;->plus_one:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/Button;

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mPlusOneButton:Landroid/widget/Button;

    sget v9, Lcom/google/android/apps/plus/R$id;->list_parent:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mDetailsParent:Landroid/view/View;

    sget v9, Lcom/google/android/apps/plus/R$id;->translucent_layer:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mTranslucentLayer:Landroid/view/View;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mDetailsParent:Landroid/view/View;

    sget v10, Lcom/google/android/apps/plus/R$id;->list_expander:I

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v4, v11}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAlwaysExpanded(Z)V

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setOnScrollChangedListener(Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;)V

    const v9, 0x102000a

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/TileOneUpDetailsListView;

    new-instance v9, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;

    invoke-direct {v9, v2, v1, p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/TileOneUpListener;)V

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/PhotoTileOneUpAdapter;

    invoke-virtual {v3, v9}, Lcom/google/android/apps/plus/views/TileOneUpDetailsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const/4 v5, 0x0

    if-eqz v0, :cond_0

    const-string v9, "photo_ref"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "photo_ref"

    invoke-virtual {v0, v9}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    if-nez v5, :cond_1

    :goto_0
    sget v9, Lcom/google/android/apps/plus/R$id;->stage:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-nez v1, :cond_2

    const/16 v9, 0x8

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    sget v9, Lcom/google/android/apps/plus/R$id;->touch_handler:I

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->setBackground(Landroid/view/View;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mDetailsParent:Landroid/view/View;

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/TileOneUpTouchHandler;->setScrollView(Landroid/view/View;)V

    return-object v8

    :cond_1
    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    sget v9, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v11}, Landroid/view/View;->setVisibility(I)V

    sget v9, Lcom/google/android/apps/plus/R$id;->background:I

    invoke-virtual {v6, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/views/PhotoView;

    iput-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v9, v5, v11}, Lcom/google/android/apps/plus/views/PhotoView;->init(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v9, p0}, Lcom/google/android/apps/plus/views/PhotoView;->setOnImageListener(Lcom/google/android/apps/plus/views/PhotoView$OnImageListener;)V

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/google/android/apps/plus/views/PhotoView;->enableImageTransforms(Z)V

    goto :goto_1
.end method

.method public final onDestroyView()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onDestroyView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoView;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    :cond_0
    return-void
.end method

.method public final onImageLoadFinished$4344180e()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoView;->onStop()V

    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoView;

    invoke-static {}, Lcom/google/android/apps/plus/views/PhotoView;->onStart()V

    :cond_0
    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 0
    .param p1    # Landroid/text/style/URLSpan;

    return-void
.end method

.method public final onVerticalScrollChanged(I)V
    .locals 4
    .param p1    # I

    const/high16 v1, 0x3f800000

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mDetailsParent:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->sCommentListOpaqueScrollPosition:I

    if-le p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->mTranslucentLayer:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->sTranslucentLayerScrollPosition:I

    if-le p1, v2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    return-void

    :cond_0
    int-to-float v0, p1

    sget v3, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->sCommentListOpaqueScrollPosition:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    goto :goto_0

    :cond_1
    int-to-float v1, p1

    sget v2, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;->sTranslucentLayerScrollPosition:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto :goto_1
.end method
