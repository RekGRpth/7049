.class final Lcom/google/android/apps/plus/fragments/PostFragment$13;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$13;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$13;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$13;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$13;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getPostLinkIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "link_url"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$13;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$13;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v2, 0x6

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V

    return-void
.end method
