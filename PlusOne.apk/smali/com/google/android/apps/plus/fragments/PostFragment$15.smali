.class final Lcom/google/android/apps/plus/fragments/PostFragment$15;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;->updatePreviewContainer(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

.field final synthetic val$activity:Landroid/app/Activity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/phone/Intents;->getChooseEmotiShareObjectIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->val$activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_INSERT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->val$activity:Landroid/app/Activity;

    invoke-static {v4}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;
    invoke-static {v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$15;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v2, 0x5

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V
    invoke-static {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V

    return-void
.end method
