.class final Lcom/google/android/apps/plus/fragments/PostFragment$17;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/google/android/apps/plus/api/MediaRef;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;>;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/MediaRefLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/MediaRefLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 5
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v0, 0x0

    check-cast p2, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    move v2, v0

    :goto_0
    if-nez p2, :cond_1

    move v1, v0

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3402(Lcom/google/android/apps/plus/fragments/PostFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3502(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->addToMediaGallery(Lcom/google/android/apps/plus/api/MediaRef;)V
    invoke-static {v4, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3600(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/api/MediaRef;)V

    goto :goto_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachments:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_1

    :cond_2
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$17;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->post_invalid_photos_unsupported:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_3
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;>;)V"
        }
    .end annotation

    return-void
.end method
