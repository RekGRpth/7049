.class final Lcom/google/android/apps/plus/fragments/PostFragment$2;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private toggleAclOverlay()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->showAclOverlay()V

    goto :goto_0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 15
    .param p1    # Landroid/view/View;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v4

    sget v12, Lcom/google/android/apps/plus/R$id;->audience_button:I

    if-ne v4, v12, :cond_3

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/AudienceData;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment$2;->toggleAclOverlay()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$400(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v12

    if-lez v12, :cond_2

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchSquarePicker()V

    goto :goto_0

    :cond_2
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->launchAclPicker()V

    goto :goto_0

    :cond_3
    sget v12, Lcom/google/android/apps/plus/R$id;->chevron_icon:I

    if-ne v4, v12, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/PostFragment$2;->toggleAclOverlay()V

    goto :goto_0

    :cond_4
    sget v12, Lcom/google/android/apps/plus/R$id;->empty_media:I

    if-eq v4, v12, :cond_5

    sget v12, Lcom/google/android/apps/plus/R$id;->choose_media:I

    if-ne v4, v12, :cond_6

    :cond_5
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$600(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    goto :goto_0

    :cond_6
    sget v12, Lcom/google/android/apps/plus/R$id;->empty_link:I

    if-ne v4, v12, :cond_7

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    invoke-static {v0, v12}, Lcom/google/android/apps/plus/phone/Intents;->getPostLinkIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v6

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v13, 0x6

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V
    invoke-static {v12, v6, v13}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V

    goto :goto_0

    :cond_7
    sget v12, Lcom/google/android/apps/plus/R$id;->empty_emotishare:I

    if-ne v4, v12, :cond_8

    invoke-static {}, Lcom/google/android/apps/plus/util/ResourceRedirector;->getInstance()Lcom/google/android/apps/plus/util/ResourceRedirector;

    sget-object v12, Lcom/google/android/apps/plus/util/Property;->ENABLE_EMOTISHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v12

    if-eqz v12, :cond_0

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->EMOTISHARE_INSERT_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v12, v13, v11, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mEmotiShare:Lcom/google/android/apps/plus/content/DbEmotishareMetadata;
    invoke-static {v13}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$900(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbEmotishareMetadata;

    move-result-object v13

    invoke-static {v0, v12, v13}, Lcom/google/android/apps/plus/phone/Intents;->getChooseEmotiShareObjectIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbEmotishareMetadata;)Landroid/content/Intent;

    move-result-object v6

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v13, 0x5

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V
    invoke-static {v12, v6, v13}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_8
    sget v12, Lcom/google/android/apps/plus/R$id;->location_view:I

    if-ne v4, v12, :cond_a

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v12, v13, v11, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1000(Lcom/google/android/apps/plus/fragments/PostFragment;)Z

    move-result v8

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v12

    if-eqz v12, :cond_9

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/content/DbLocation;->hasCoordinates()Z

    move-result v12

    if-eqz v12, :cond_9

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v7

    :goto_1
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    invoke-static {v0, v12, v8, v7}, Lcom/google/android/apps/plus/phone/Intents;->getChooseLocationIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLcom/google/android/apps/plus/content/DbLocation;)Landroid/content/Intent;

    move-result-object v6

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v13, 0x3

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V
    invoke-static {v12, v6, v13}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_9
    const/4 v7, 0x0

    goto :goto_1

    :cond_a
    sget v12, Lcom/google/android/apps/plus/R$id;->remove_location:I

    if-ne v4, v12, :cond_b

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    sget-object v13, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_CLICKED_LOCATION:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v12, v13, v11, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v13, 0x1

    invoke-static {v12, v13}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1202(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/fragments/PostFragment;->setLocationChecked(Z)V

    goto/16 :goto_0

    :cond_b
    sget v12, Lcom/google/android/apps/plus/R$id;->mention_scroll_view:I

    if-eq v4, v12, :cond_c

    sget v12, Lcom/google/android/apps/plus/R$id;->compose_text:I

    if-ne v4, v12, :cond_d

    :cond_c
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAclDropDown:Lcom/google/android/apps/plus/views/AclDropDown;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/views/AclDropDown;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/AclDropDown;->hideAclOverlay()V

    goto/16 :goto_0

    :cond_d
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoComposeActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v1

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    new-array v10, v12, [Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAttachmentRefs:Ljava/util/ArrayList;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1300(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/util/ArrayList;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v9, :cond_0

    const/4 v5, 0x0

    const/4 v3, 0x0

    :goto_2
    array-length v12, v10

    if-ge v3, v12, :cond_f

    aget-object v12, v10, v3

    invoke-virtual {v9, v12}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    move v5, v3

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_f
    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v12}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v12

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoIndex(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setMediaRefs([Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    iget-object v12, p0, Lcom/google/android/apps/plus/fragments/PostFragment$2;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v13

    const/4 v14, 0x4

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->launchActivity(Landroid/content/Intent;I)V
    invoke-static {v12, v13, v14}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$800(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/content/Intent;I)V

    goto/16 :goto_0
.end method
