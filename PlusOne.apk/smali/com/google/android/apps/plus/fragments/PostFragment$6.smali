.class final Lcom/google/android/apps/plus/fragments/PostFragment$6;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 2
    .param p1    # Landroid/location/Location;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->removeLocationListener()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1900(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1100(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # setter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mProviderLocation:Landroid/location/Location;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2002(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/location/Location;)Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->getCityLevelLocationPreference()Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2100(Lcom/google/android/apps/plus/fragments/PostFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/LocationController;->getCityLevelLocation(Landroid/location/Location;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    :goto_1
    # setter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLocation:Lcom/google/android/apps/plus/content/DbLocation;
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$1102(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/content/DbLocation;)Lcom/google/android/apps/plus/content/DbLocation;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->updatePostUI()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$6;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getView()Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->updateLocation(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2200(Lcom/google/android/apps/plus/fragments/PostFragment;Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/plus/phone/LocationController;->getStreetLevelLocation(Landroid/location/Location;)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v0

    goto :goto_1
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    return-void
.end method
