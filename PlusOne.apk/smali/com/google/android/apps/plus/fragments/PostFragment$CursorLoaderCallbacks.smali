.class final Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/PostFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CursorLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/PostFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/PostFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mApiaryApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2300(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsApiProvider;->makePreviewUri(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->setUri(Landroid/net/Uri;)V

    new-instance v1, Lcom/google/android/apps/plus/content/PreviewRequestData;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mUrl:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2400(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mCallToAction:Lcom/google/android/apps/plus/api/CallToActionData;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2500(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/api/CallToActionData;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/content/PreviewRequestData;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/CallToActionData;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/PreviewRequestData;->toSelectionArg()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->setSelectionArgs([Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 7
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v2, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    check-cast p1, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2602(Lcom/google/android/apps/plus/fragments/PostFragment;Z)Z

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;->isCachedData()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/fragments/PreviewCursorLoader;->setCachedData(Z)V

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v0, "com.google.circles.platform.result.extra.ERROR_CODE"

    const/16 v1, 0xc8

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "com.google.circles.platform.result.extra.ERROR_MESSAGE"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "Ok"

    move-object v1, v0

    :goto_1
    const-string v0, "com.google.android.apps.content.EXTRA_ACTIVITY"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_3

    array-length v3, v0

    if-lez v3, :cond_3

    aget-object v0, v0, v5

    check-cast v0, Lcom/google/android/apps/plus/api/ApiaryActivity;

    :goto_2
    new-instance v3, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v3, v4, v1, v2}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_SHOWN:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v4}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {v2}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->getExtrasForLogging()Landroid/os/Bundle;
    invoke-static {v6}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$300(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/os/Bundle;

    move-result-object v6

    invoke-static {v2, v4, v1, v5, v6}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V
    invoke-static {v1, v3, v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2700(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_PREVIEW_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/PostFragment;->handlePreviewResult(Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V
    invoke-static {v0, v2, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2700(Lcom/google/android/apps/plus/fragments/PostFragment;Lcom/google/android/apps/plus/service/ServiceResult;Lcom/google/android/apps/plus/api/ApiaryActivity;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2800(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/PostFragment$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mLoadingView:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$2800(Lcom/google/android/apps/plus/fragments/PostFragment;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
