.class final Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;
.super Ljava/lang/Object;
.source "PostFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->add(Lcom/google/android/apps/plus/api/MediaRef;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;->this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;->this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/PostFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->newPhotosActivityIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;->this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$700(Lcom/google/android/apps/plus/fragments/PostFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;->this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mTargetAlbumId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3700(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setAlbumId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;->this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    iget-object v2, v2, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/PostFragment;->mAlbumOwnerId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->access$3800(Lcom/google/android/apps/plus/fragments/PostFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setPhotoPickerMode(Ljava/lang/Integer;)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->setHideShareAction(Z)Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery$1;->this$1:Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;

    iget-object v1, v1, Lcom/google/android/apps/plus/fragments/PostFragment$MediaGallery;->this$0:Lcom/google/android/apps/plus/fragments/PostFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotosIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/PostFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
