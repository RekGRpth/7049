.class final Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;
.super Ljava/lang/Object;
.source "ProfileEditFragment.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocationCheckboxWatcher"
.end annotation


# instance fields
.field private final mOriginalValue:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Z)V
    .locals 0
    .param p2    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->mOriginalValue:Z

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getItemCount()I
    invoke-static {v6}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1600(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v6, v6, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mContentView:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eq v4, v3, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;

    iget v6, v5, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ItemViewIds;->current:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-boolean v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->mOriginalValue:Z

    if-ne v6, p2, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->removeChangedField(Landroid/view/View;)V
    invoke-static {v6, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    :goto_1
    return-void

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$LocationCheckboxWatcher;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->addChangedField(Landroid/view/View;)V
    invoke-static {v6, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$1500(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Landroid/view/View;)V

    goto :goto_1
.end method
