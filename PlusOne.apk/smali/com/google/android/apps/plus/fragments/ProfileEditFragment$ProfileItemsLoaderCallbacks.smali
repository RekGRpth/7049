.class final Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;
.super Ljava/lang/Object;
.source "ProfileEditFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ProfileEditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProfileItemsLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$000(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v3}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$000(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 5
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    check-cast p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEditMode:I
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$100(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->rosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mSharingRosterData:Lcom/google/api/services/plusi/model/SharingRosterData;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$602(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/SharingRosterData;)Lcom/google/api/services/plusi/model/SharingRosterData;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$702(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # invokes: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateViewsWithItemData()V
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$800(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->updateView(Landroid/view/View;)V

    return-void

    :pswitch_0
    if-eqz p2, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v2, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/User;->employments:Lcom/google/api/services/plusi/model/Employments;

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$302(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Employments;)Lcom/google/api/services/plusi/model/Employments;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$202(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Employments;)Lcom/google/api/services/plusi/model/Employments;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$200(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/api/services/plusi/model/Employments;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    new-instance v2, Lcom/google/api/services/plusi/model/Employments;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/Employments;-><init>()V

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEmployments:Lcom/google/api/services/plusi/model/Employments;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$302(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Employments;)Lcom/google/api/services/plusi/model/Employments;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEmployments:Lcom/google/api/services/plusi/model/Employments;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$202(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Employments;)Lcom/google/api/services/plusi/model/Employments;

    goto :goto_0

    :pswitch_1
    if-eqz p2, :cond_3

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v2, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/User;->educations:Lcom/google/api/services/plusi/model/Educations;

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$502(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Educations;)Lcom/google/api/services/plusi/model/Educations;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$402(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Educations;)Lcom/google/api/services/plusi/model/Educations;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$400(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;)Lcom/google/api/services/plusi/model/Educations;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileEditFragment$ProfileItemsLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    new-instance v2, Lcom/google/api/services/plusi/model/Educations;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/Educations;-><init>()V

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mEducations:Lcom/google/api/services/plusi/model/Educations;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$502(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Educations;)Lcom/google/api/services/plusi/model/Educations;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->mOriginalEducations:Lcom/google/api/services/plusi/model/Educations;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->access$402(Lcom/google/android/apps/plus/fragments/ProfileEditFragment;Lcom/google/api/services/plusi/model/Educations;)Lcom/google/api/services/plusi/model/Educations;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
