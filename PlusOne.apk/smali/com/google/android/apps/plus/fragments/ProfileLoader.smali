.class public Lcom/google/android/apps/plus/fragments/ProfileLoader;
.super Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;
.source "ProfileLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

.field private final mFullProfileNeeded:Z

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z

.field private final mPersonId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mPersonId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mFullProfileNeeded:Z

    return-void
.end method


# virtual methods
.method public deliverResult(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->isReset()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->deliverResult(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V

    return-void
.end method

.method public final bridge synthetic esLoadInBackground()Ljava/lang/Object;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mPersonId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mFullProfileNeeded:Z

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->getProfileAndContactData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    move-result-object v0

    return-object v0
.end method

.method protected onAbandon()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onReset()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsAsyncTaskLoader;->onReset()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserverRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mPersonId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mObserverRegistered:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/ProfileLoader;->mData:Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->forceLoad()V

    :cond_1
    return-void
.end method
