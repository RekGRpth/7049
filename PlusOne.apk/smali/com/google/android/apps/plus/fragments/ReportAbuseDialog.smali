.class public Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ReportAbuseDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final ABUSE_TYPES:[Ljava/lang/String;

.field private static final ABUSE_TYPE_LABELS:[I


# instance fields
.field private mAbuseType:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "FAKE_USER"

    aput-object v1, v0, v3

    const-string v1, "HATE"

    aput-object v1, v0, v4

    const-string v1, "IMPERSONATION"

    aput-object v1, v0, v5

    const-string v1, "PORN"

    aput-object v1, v0, v6

    const-string v1, "SPAM"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "COPYRIGHT"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->ABUSE_TYPES:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [I

    sget v1, Lcom/google/android/apps/plus/R$string;->report_abuse_reason_fake_profile:I

    aput v1, v0, v3

    sget v1, Lcom/google/android/apps/plus/R$string;->report_abuse_reason_hate_speech_or_violence:I

    aput v1, v0, v4

    sget v1, Lcom/google/android/apps/plus/R$string;->report_abuse_reason_impersonation:I

    aput v1, v0, v5

    sget v1, Lcom/google/android/apps/plus/R$string;->report_abuse_reason_nudity:I

    aput v1, v0, v6

    sget v1, Lcom/google/android/apps/plus/R$string;->report_abuse_reason_spam:I

    aput v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/plus/R$string;->report_abuse_reason_copyright:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->ABUSE_TYPE_LABELS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    if-ltz p2, :cond_0

    iput p2, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget v0, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;

    sget-object v1, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->ABUSE_TYPES:[Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedProfileFragment;->reportAbuse(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1    # Landroid/os/Bundle;

    if-eqz p1, :cond_0

    const-string v4, "abuse_type"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->report_user_dialog_title:I

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v4, 0x104000a

    invoke-virtual {v2, v4, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v4, 0x1040000

    invoke-virtual {v2, v4, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    sget-object v4, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->ABUSE_TYPE_LABELS:[I

    array-length v4, v4

    new-array v0, v4, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_0
    sget-object v4, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->ABUSE_TYPE_LABELS:[I

    array-length v4, v4

    if-ge v3, v4, :cond_1

    sget-object v4, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->ABUSE_TYPE_LABELS:[I

    aget v4, v4, v3

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    iget v4, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    invoke-virtual {v2, v0, v4, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "abuse_type"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/ReportAbuseDialog;->mAbuseType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
