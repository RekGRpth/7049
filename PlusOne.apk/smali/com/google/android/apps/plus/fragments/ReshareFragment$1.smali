.class final Lcom/google/android/apps/plus/fragments/ReshareFragment$1;
.super Ljava/lang/Object;
.source "ReshareFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/ReshareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final mentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/ReshareFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->mentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 10
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->mentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-virtual {v7, p1, v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v8}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getThreshold()I

    move-result v8

    add-int/2addr v7, v8

    if-gt v7, v0, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$dimen;->plus_mention_suggestion_min_space:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v3, v7

    const/4 v7, 0x2

    new-array v2, v7, [I

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getLocationOnScreen([I)V

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->getView()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    const/4 v7, 0x1

    aget v4, v2, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getCursorYPosition()I

    move-result v7

    add-int v1, v4, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v7, v5, v1

    if-ge v7, v3, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v7}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$200(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Landroid/widget/ScrollView;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/ReshareFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/ReshareFragment;->mEditor:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    invoke-static {v9}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->access$100(Lcom/google/android/apps/plus/fragments/ReshareFragment;)Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getCursorYTop()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method
