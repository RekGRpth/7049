.class public Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;
.super Landroid/support/v4/app/Fragment;
.source "SearchResults.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/SearchResults;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SearchResultsFragment"
.end annotation


# instance fields
.field private mResults:Lcom/google/android/apps/plus/fragments/SearchResults;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;->setRetainInstance(Z)V

    return-void
.end method


# virtual methods
.method public final getSearchResults()Lcom/google/android/apps/plus/fragments/SearchResults;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    return-object v0
.end method

.method public final setSearchResults(Lcom/google/android/apps/plus/fragments/SearchResults;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/SearchResults;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;->mResults:Lcom/google/android/apps/plus/fragments/SearchResults;

    return-void
.end method
