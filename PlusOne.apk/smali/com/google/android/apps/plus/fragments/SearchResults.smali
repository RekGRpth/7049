.class public Lcom/google/android/apps/plus/fragments/SearchResults;
.super Ljava/lang/Object;
.source "SearchResults.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/SearchResults$SearchResultsFragment;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/fragments/SearchResults;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContinuationToken:Ljava/lang/String;

.field private mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

.field private mIdColumn:I

.field private mMaxParcelableSize:I

.field private mQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/SearchResults$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/SearchResults$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/SearchResults;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 9
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mQuery:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    array-length v0, v3

    new-instance v7, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {v7, v3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    new-array v4, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v5, :cond_1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    const/4 v7, 0x0

    aput-object v7, v4, v2

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v4, v2

    goto :goto_2

    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    aput-object v7, v4, v2

    goto :goto_2

    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v2

    goto :goto_2

    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v7

    aput-object v7, v4, v2

    goto :goto_2

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v7, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;

    const/16 v0, 0x1f4

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/fragments/SearchResults;-><init>([Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>([Ljava/lang/String;I)V
    .locals 3
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/16 v1, 0x1f4

    iput v1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mMaxParcelableSize:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mIdColumn:I

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    const-string v1, "_id"

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mIdColumn:I

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final addResults(Lcom/google/android/apps/plus/phone/EsMatrixCursor;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getColumnCount()I

    move-result v0

    const/4 v5, -0x1

    invoke-virtual {p1, v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->moveToPosition(I)Z

    new-array v4, v0, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v2

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getType(I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    const/4 v5, 0x0

    aput-object v5, v4, v1

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_0
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getLong(I)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_2

    :pswitch_1
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getDouble(I)D

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_2

    :pswitch_2
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_2

    :pswitch_3
    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getBlob(I)[B

    move-result-object v5

    aput-object v5, v4, v1

    goto :goto_2

    :cond_0
    iget v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mIdColumn:I

    if-lez v5, :cond_1

    iget v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mIdColumn:I

    add-int/lit8 v3, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    move v2, v3

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getContinuationToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v0

    return v0
.end method

.method public final getCursor()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    return-object v0
.end method

.method public final getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mQuery:Ljava/lang/String;

    return-object v0
.end method

.method public final hasMoreResults()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mContinuationToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isParcelable()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mMaxParcelableSize:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setContinuationToken(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mContinuationToken:Ljava/lang/String;

    return-void
.end method

.method public final setQueryString(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mQuery:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mQuery:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mContinuationToken:Ljava/lang/String;

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 7
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mQuery:Ljava/lang/String;

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getCount()I

    move-result v3

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v5

    array-length v0, v5

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->moveToPosition(I)Z

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getType(I)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    packed-switch v4, :pswitch_data_0

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :pswitch_0
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {p1, v5, v6}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_2

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getDouble(I)D

    move-result-wide v5

    invoke-virtual {p1, v5, v6}, Landroid/os/Parcel;->writeDouble(D)V

    goto :goto_2

    :pswitch_2
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_3
    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SearchResults;->mCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->getBlob(I)[B

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
