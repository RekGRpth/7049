.class final Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "SelectSquareCategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGetSquareComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mNewerReqId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    iget-object v0, v0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    iput-object v2, v0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mNewerReqId:Ljava/lang/Integer;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    # getter for: Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mLoaderError:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->access$000(Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    sget v2, Lcom/google/android/apps/plus/R$string;->people_list_error:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->updateSpinner()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;->this$0:Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    invoke-virtual {v0, v3, v2, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_1
.end method
