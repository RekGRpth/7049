.class final Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;
.super Ljava/lang/Object;
.source "SelectSquareCategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SquareStreamInfo"
.end annotation


# instance fields
.field private final mStreamId:Ljava/lang/String;

.field private final mStreamName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->mStreamId:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->mStreamName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getStreamId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->mStreamId:Ljava/lang/String;

    return-object v0
.end method

.method public final getStreamName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->mStreamName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->mStreamName:Ljava/lang/String;

    return-object v0
.end method
