.class public Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "SelectSquareCategoryFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<[",
        "Lcom/google/android/apps/plus/content/DbSquareStream;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mLoaderError:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSquareId:Ljava/lang/String;

.field private mSquareName:Ljava/lang/String;

.field private mSquareStreamLoaderActive:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareStreamLoaderActive:Z

    new-instance v0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mLoaderError:Z

    return v0
.end method

.method private isLoading()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateView(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x0

    const/16 v3, 0x8

    const v2, 0x102000a

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->server_error:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mLoaderError:Z

    if-eqz v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->showContent(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->isLoading()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->showEmptyViewProgress(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$string;->no_squares:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->showEmptyView(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->showContent(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final isEmpty()Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->isLoading()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isProgressIndicatorVisible()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->isProgressIndicatorVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareStreamLoaderActive:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x1090003

    invoke-direct {v0, p1, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareId:Ljava/lang/String;

    const-string v1, "square_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<[",
            "Lcom/google/android/apps/plus/content/DbSquareStream;",
            ">;"
        }
    .end annotation

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v3, 0x0

    sget v1, Lcom/google/android/apps/plus/R$layout;->edit_audience_fragment:I

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->getStreamId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->getStreamName()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/plus/content/SquareTargetData;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareName:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v1, v2}, Lcom/google/android/apps/plus/content/SquareTargetData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/SquareTargetData;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "audience"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v3, -0x1

    invoke-virtual {v1, v3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 7
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    check-cast p2, [Lcom/google/android/apps/plus/content/DbSquareStream;

    if-nez p2, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mLoaderError:Z

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :pswitch_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareStreamLoaderActive:Z

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->isDataStale()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->refresh()V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mLoaderError:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    array-length v5, p2

    if-eq v5, v0, :cond_2

    move v3, v1

    :goto_2
    if-nez v3, :cond_5

    move v4, v2

    :goto_3
    if-ge v4, v5, :cond_5

    aget-object v0, p2, v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;->getStreamId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_4
    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    :goto_5
    if-ge v2, v5, :cond_4

    aget-object v0, p2, v2

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAdapter:Landroid/widget/ArrayAdapter;

    new-instance v3, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->getStreamId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment$SquareStreamInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->updateView(Landroid/view/View;)V

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<[",
            "Lcom/google/android/apps/plus/content/DbSquareStream;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->refresh:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->refresh()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->updateView(Landroid/view/View;)V

    return-void
.end method

.method public final refresh()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mNewerReqId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getSquare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->mNewerReqId:Ljava/lang/Integer;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SelectSquareCategoryFragment;->updateSpinner()V

    return-void
.end method
