.class final Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$1;
.super Landroid/widget/ArrayAdapter;
.source "SimpleAudiencePickerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;Landroid/content/Context;ILjava/util/List;)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$1;->this$0:Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public final getItemViewType(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/plus/views/CircleListItemView;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$1;->this$0:Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;

    # getter for: Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mThemeContext:Landroid/view/ContextThemeWrapper;
    invoke-static {v1}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->access$000(Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;)Landroid/view/ContextThemeWrapper;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/CircleListItemView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CircleListItemView;->setCheckBoxVisible(Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CircleListItemView;->setMemberCountVisible(Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/CircleListItemView;->updateContentDescription()V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$1;->getItem(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/views/CircleListItemView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->getType()I

    move-result v2

    invoke-virtual {v6}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->getName()Ljava/lang/String;

    move-result-object v3

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/CircleListItemView;->setCircle(Ljava/lang/String;ILjava/lang/String;IZ)V

    return-object v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
