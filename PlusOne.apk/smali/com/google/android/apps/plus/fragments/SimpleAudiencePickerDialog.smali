.class public Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;
.super Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;
.source "SimpleAudiencePickerDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;
    }
.end annotation


# instance fields
.field private mOptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mThemeContext:Landroid/view/ContextThemeWrapper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;)Landroid/view/ContextThemeWrapper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mThemeContext:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    new-instance v1, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "domain_name"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "domain_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "has_public_circle"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/google/android/apps/plus/R$style;->CircleSubscriptionList:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mThemeContext:Landroid/view/ContextThemeWrapper;

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v8, "domain_name"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "domain_id"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v8, "has_public_circle"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mThemeContext:Landroid/view/ContextThemeWrapper;

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v8, Lcom/google/android/apps/plus/R$layout;->simple_audience_picker_dialog:I

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    const-string v10, "1f"

    const/4 v11, 0x7

    sget v12, Lcom/google/android/apps/plus/R$string;->acl_extended_network:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v11, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    const/16 v10, 0x8

    invoke-direct {v9, v4, v10, v5}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v6, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    const-string v10, "0"

    const/16 v11, 0x9

    sget v12, Lcom/google/android/apps/plus/R$string;->acl_public:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v11, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    const-string v10, "1c"

    const/4 v11, 0x5

    sget v12, Lcom/google/android/apps/plus/R$string;->acl_your_circles:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v11, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    const-string v10, "v.private"

    const/16 v11, 0x65

    sget v12, Lcom/google/android/apps/plus/R$string;->acl_private:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v11, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    new-instance v9, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    const-string v10, "v.custom"

    const/4 v11, -0x3

    sget v12, Lcom/google/android/apps/plus/R$string;->post_create_custom_acl:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, v10, v11, v12}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v8, Lcom/google/android/apps/plus/R$id;->list:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v8, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$1;

    iget-object v9, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mThemeContext:Landroid/view/ContextThemeWrapper;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mOptions:Ljava/util/ArrayList;

    invoke-direct {v8, p0, v9, v10, v11}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$1;-><init>(Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v3, v8}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->mThemeContext:Landroid/view/ContextThemeWrapper;

    invoke-direct {v1, v8}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v8, Lcom/google/android/apps/plus/R$string;->profile_edit_item_visibility:I

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const/high16 v8, 0x1040000

    invoke-virtual {v1, v8, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v8, 0x1

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->getType()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog$CircleInfo;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileEditFragment;->onSetSimpleAudience(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SimpleAudiencePickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method
