.class public final Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;
.super Landroid/support/v4/content/AsyncTaskLoader;
.source "SquareCategoryLoader.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/content/AsyncTaskLoader",
        "<[",
        "Lcom/google/android/apps/plus/content/DbSquareStream;",
        ">;"
    }
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mData:[Lcom/google/android/apps/plus/content/DbSquareStream;

.field private mIsDataStale:Z

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "square_streams"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "last_sync"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mSquareId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, [Lcom/google/android/apps/plus/content/DbSquareStream;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->isReset()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mData:[Lcom/google/android/apps/plus/content/DbSquareStream;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/content/AsyncTaskLoader;->deliverResult(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final isDataStale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mIsDataStale:Z

    return v0
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 11

    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mSquareId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v7, 0x0

    cmp-long v0, v4, v7

    if-lez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-long/2addr v0, v4

    const-wide/32 v3, 0xdbba0

    cmp-long v0, v0, v3

    if-lez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mIsDataStale:Z

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->deserialize([B)[Lcom/google/android/apps/plus/content/DbSquareStream;

    move-result-object v3

    :cond_0
    :goto_1
    return-object v3

    :cond_1
    move v0, v10

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mSquareId:Ljava/lang/String;

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;->start()V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbSquareStream;->deserialize([B)[Lcom/google/android/apps/plus/content/DbSquareStream;

    move-result-object v3

    goto :goto_1
.end method

.method protected final onStartLoading()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->mData:[Lcom/google/android/apps/plus/content/DbSquareStream;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareCategoryLoader;->forceLoad()V

    :cond_0
    return-void
.end method
