.class public final Lcom/google/android/apps/plus/fragments/SquareListLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "SquareListLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mIsDataStale:Z

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->SQUARES_URI:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/fragments/SquareListLoader;->setUri(Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mProjection:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 11

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareListLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareListLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/content/EsSquaresData;->queryLastSquaresSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v7

    const-wide/32 v9, 0xdbba0

    cmp-long v2, v4, v9

    if-lez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mIsDataStale:Z

    const-wide/16 v4, 0x0

    cmp-long v2, v7, v4

    if-gtz v2, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/api/GetSquaresOperation;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/GetSquaresOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSquaresOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetSquaresOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return-object v3

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareListLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mProjection:[Ljava/lang/String;

    const-string v5, "square_name"

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsSquaresData;->getJoinedSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v6, v2}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_2
    move-object v3, v6

    goto :goto_1
.end method

.method public final isDataStale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SquareListLoader;->mIsDataStale:Z

    return v0
.end method
