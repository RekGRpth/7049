.class public final Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;
.super Lcom/google/android/apps/plus/fragments/BaseSearchLoader;
.source "SquareMemberSearchLoader.java"


# instance fields
.field private final mMinQueryLength:I

.field private final mProjection:[Ljava/lang/String;

.field private final mSquareId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # [Ljava/lang/String;
    .param p7    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mSquareId:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mProjection:[Ljava/lang/String;

    iput p7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mMinQueryLength:I

    return-void
.end method

.method private loadInBackground()Lcom/google/android/apps/plus/fragments/SearchLoaderResults;
    .locals 15

    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mQuery:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mMinQueryLength:I

    if-ge v1, v2, :cond_1

    :cond_0
    new-instance v7, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    invoke-direct {v7}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;-><init>()V

    :goto_0
    return-object v7

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mSquareId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mQuery:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mContinuationToken:Ljava/lang/String;

    const/16 v6, 0x32

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->isAborted()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->ABORTED:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    move-object v7, v1

    goto :goto_0

    :cond_2
    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "SquareMemberSearch"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->logError(Ljava/lang/String;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    iput-object v7, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    throw v1

    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->getSquareMemberList()Ljava/util/List;

    move-result-object v13

    new-instance v9, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {v9, v8}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    move v1, v10

    move v2, v11

    move v3, v11

    move v4, v11

    move v5, v11

    :goto_1
    array-length v6, v8

    if-ge v1, v6, :cond_9

    aget-object v6, v8, v1

    const-string v12, "_id"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    move v5, v1

    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    const-string v12, "gaia_id"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    move v4, v1

    goto :goto_2

    :cond_6
    const-string v12, "name"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    move v3, v1

    goto :goto_2

    :cond_7
    const-string v12, "avatar"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    move v11, v1

    goto :goto_2

    :cond_8
    const-string v12, "membership_status"

    invoke-virtual {v12, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v2, v1

    goto :goto_2

    :cond_9
    if-eqz v13, :cond_e

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    move v6, v1

    :goto_3
    array-length v1, v8

    new-array v14, v1, [Ljava/lang/Object;

    move v12, v10

    :goto_4
    if-ge v12, v6, :cond_f

    invoke-static {v14, v7}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/SquareMember;

    if-ltz v5, :cond_10

    add-int/lit8 v8, v10, 0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v14, v5

    :goto_5
    if-ltz v4, :cond_a

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareMember;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v10, v14, v4

    :cond_a
    if-ltz v3, :cond_b

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareMember;->displayName:Ljava/lang/String;

    aput-object v10, v14, v3

    :cond_b
    if-ltz v11, :cond_c

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareMember;->photoUrl:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAvatarData;->compressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v14, v11

    :cond_c
    if-ltz v2, :cond_d

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SquareMember;->membershipStatus:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsSquaresData;->getMembershipStatus(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v14, v2

    :cond_d
    invoke-virtual {v9, v14}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v10, v12, 0x1

    move v12, v10

    move v10, v8

    goto :goto_4

    :cond_e
    move v6, v10

    goto :goto_3

    :cond_f
    new-instance v7, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareMemberSearchOperation;->getNextContinuationToken()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v7, v9, v1, v2}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;-><init>(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_10
    move v8, v10

    goto :goto_5
.end method


# virtual methods
.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SquareMemberSearchLoader;->loadInBackground()Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    move-result-object v0

    return-object v0
.end method
