.class public final Lcom/google/android/apps/plus/fragments/SquareMembersLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "SquareMembersLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mContinuationToken:Ljava/lang/String;

.field private final mIsAdmin:Z

.field private mIsDataStale:Z

.field private final mMemberListType:I

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private final mProjection:[Ljava/lang/String;

.field private final mSquareId:Ljava/lang/String;

.field private mTotalMembers:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZI[Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # I
    .param p6    # [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    iput-object p2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mSquareId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mIsAdmin:Z

    iput p5, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mMemberListType:I

    iput-object p6, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mProjection:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 13

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mSquareId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/content/EsSquaresData;->queryLastSquareMemberSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v10

    const-wide/32 v4, 0xdbba0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mIsDataStale:Z

    const-wide/16 v2, 0x0

    cmp-long v2, v10, v2

    if-gtz v2, :cond_3

    new-instance v0, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mSquareId:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mIsAdmin:Z

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    const/4 v5, 0x0

    const/16 v6, 0x1f4

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;ILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v9, 0x0

    :cond_0
    :goto_2
    return-object v9

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mSquareId:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mMemberListType:I

    iget-object v6, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mProjection:[Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/plus/content/EsSquaresData;->getSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    if-eqz v9, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-interface {v9, v2}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mSquareId:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mMemberListType:I

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsSquaresData;->getSquareMemberCountAndToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mContinuationToken:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mTotalMembers:I

    goto :goto_2
.end method

.method public final getContinuationToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method public final getTotalMembers()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mTotalMembers:I

    return v0
.end method

.method public final isDataStale()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/SquareMembersLoader;->mIsDataStale:Z

    return v0
.end method
