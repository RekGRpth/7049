.class public final Lcom/google/android/apps/plus/fragments/SquareSearchLoader;
.super Lcom/google/android/apps/plus/fragments/BaseSearchLoader;
.source "SquareSearchLoader.java"


# instance fields
.field private final mMinQueryLength:I

.field private final mProjection:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/BaseSearchLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    iput-object p5, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mProjection:[Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mMinQueryLength:I

    return-void
.end method

.method private loadInBackground()Lcom/google/android/apps/plus/fragments/SearchLoaderResults;
    .locals 15

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mQuery:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mMinQueryLength:I

    if-ge v1, v2, :cond_1

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;-><init>()V

    :goto_0
    return-object v1

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mQuery:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mContinuationToken:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->isAborted()Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->ABORTED:Lcom/google/android/apps/plus/fragments/SearchLoaderResults;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "SquareSearch"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->logError(Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mOperation:Lcom/google/android/apps/plus/network/HttpOperation;

    throw v1

    :cond_3
    iget-object v10, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mProjection:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->getSquareSearchResults()Ljava/util/List;

    move-result-object v13

    const/4 v11, 0x0

    const/4 v8, -0x1

    const/4 v6, -0x1

    const/4 v5, -0x1

    const/4 v4, -0x1

    const/4 v3, -0x1

    const/4 v2, -0x1

    new-instance v7, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {v7, v10}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_1
    array-length v9, v10

    if-ge v1, v9, :cond_a

    aget-object v9, v10, v1

    const-string v12, "_id"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    move v8, v1

    :cond_4
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    const-string v12, "square_id"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    move v6, v1

    goto :goto_2

    :cond_6
    const-string v12, "square_name"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    move v5, v1

    goto :goto_2

    :cond_7
    const-string v12, "photo_url"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    move v4, v1

    goto :goto_2

    :cond_8
    const-string v12, "post_visibility"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    move v3, v1

    goto :goto_2

    :cond_9
    const-string v12, "member_count"

    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    move v2, v1

    goto :goto_2

    :cond_a
    if-eqz v13, :cond_11

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v1

    move v9, v1

    :goto_3
    array-length v1, v10

    new-array v14, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    move v12, v1

    :goto_4
    if-ge v12, v9, :cond_13

    const/4 v1, 0x0

    invoke-static {v14, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v13, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/SquareResult;

    if-ltz v8, :cond_b

    add-int/lit8 v10, v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v14, v8

    move v11, v10

    :cond_b
    if-ltz v6, :cond_c

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareResult;->squareId:Lcom/google/api/services/plusi/model/SquareId;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/SquareId;->obfuscatedGaiaId:Ljava/lang/String;

    aput-object v10, v14, v6

    :cond_c
    if-ltz v5, :cond_d

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareResult;->displayName:Ljava/lang/String;

    aput-object v10, v14, v5

    :cond_d
    if-ltz v4, :cond_e

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareResult;->photoUrl:Ljava/lang/String;

    aput-object v10, v14, v4

    :cond_e
    if-ltz v3, :cond_f

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareResult;->privatePosts:Ljava/lang/Boolean;

    if-eqz v10, :cond_f

    iget-object v10, v1, Lcom/google/api/services/plusi/model/SquareResult;->privatePosts:Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_12

    const/4 v10, 0x1

    :goto_5
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v14, v3

    :cond_f
    if-ltz v2, :cond_10

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SquareResult;->memberCount:Ljava/lang/Long;

    aput-object v1, v14, v2

    :cond_10
    invoke-virtual {v7, v14}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto :goto_4

    :cond_11
    const/4 v1, 0x0

    move v9, v1

    goto :goto_3

    :cond_12
    const/4 v10, 0x0

    goto :goto_5

    :cond_13
    new-instance v1, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/SquareSearchQueryOperation;->getContinuationToken()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v7, v2, v3}, Lcom/google/android/apps/plus/fragments/SearchLoaderResults;-><init>(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/SquareSearchLoader;->loadInBackground()Lcom/google/android/apps/plus/fragments/SearchLoaderResults;

    move-result-object v0

    return-object v0
.end method
