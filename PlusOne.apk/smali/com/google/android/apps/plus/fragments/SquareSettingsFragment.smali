.class public Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;
.super Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;
.source "SquareSettingsFragment.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# static fields
.field private static sSquareVelocityOptionValues:[I

.field private static sSquareVelocityOptions:[Ljava/lang/String;


# instance fields
.field private mLeaveSquareRequestId:Ljava/lang/Integer;

.field private final mLeaveSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mSquareId:Ljava/lang/String;

.field private mSquareVisibility:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->handleLeaveSquareCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method private handleLeaveSquareCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 7
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->dismissProgressDialog()V

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v1, :cond_5

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_4

    iget-object v4, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x1080027

    invoke-static {v4, v1, v5, v3, v6}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_2
    move v0, v2

    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->finishActivity(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$string;->square_leave_error:I

    invoke-static {v1, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_2

    :cond_5
    move-object v1, v3

    goto :goto_1
.end method


# virtual methods
.method protected final createBaseExtrasForLogging()Landroid/os/Bundle;
    .locals 2

    const-string v0, "extra_square_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected final getVolumeControTypeForLogging()I
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;->SQUARE:Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData$VolumeControlType;->value()I

    move-result v0

    return v0
.end method

.method protected final getVolumeSettingId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareId:Ljava/lang/String;

    return-object v0
.end method

.method protected final getVolumeSettingType()Ljava/lang/String;
    .locals 1

    const-string v0, "SQUARES"

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->leave_square_section:I

    if-ne v0, v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareVisibility:I

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$string;->square_confirm_leave_public:I

    :goto_0
    sget v2, Lcom/google/android/apps/plus/R$string;->square_confirm_leave_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->square_dialog_leave_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v1, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "leave_square"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$string;->square_confirm_leave_private:I

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onClick(Landroid/view/View;)V

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x3

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v1, "leave_square_req_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "leave_square_req_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->sSquareVelocityOptions:[Ljava/lang/String;

    if-nez v1, :cond_1

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_more:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_standard:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/apps/plus/R$string;->circle_settings_amount_fewer:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->sSquareVelocityOptions:[Ljava/lang/String;

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    sput-object v1, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->sSquareVelocityOptionValues:[I

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->sSquareVelocityOptions:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mVelocityOptions:[Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->sSquareVelocityOptionValues:[I

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mVelocityValues:[I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "square_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareId:Ljava/lang/String;

    const-string v1, "square_visibility"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareVisibility:I

    return-void

    :array_0
    .array-data 4
        0x3
        0x2
        0x1
    .end array-data
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    sget v4, Lcom/google/android/apps/plus/R$id;->from_this_circle:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$string;->circle_settings_from_this_community:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    sget v4, Lcom/google/android/apps/plus/R$id;->subscription_section_title:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$string;->circle_settings_community_subscription_section:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    sget v4, Lcom/google/android/apps/plus/R$id;->square_membership_section:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    sget v4, Lcom/google/android/apps/plus/R$id;->leave_square_section:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v2
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "leave_square"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareId:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "LEAVE"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$string;->square_leaving:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->showProgressDialog(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->SQUARE_LEAVE:Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_LANDING:Lcom/google/android/apps/plus/analytics/OzViews;

    const-string v4, "extra_square_id"

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mSquareId:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public final onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->mLeaveSquareRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/fragments/SquareSettingsFragment;->handleLeaveSquareCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_0
    return-void
.end method
