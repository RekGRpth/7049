.class public final Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "StreamOneUpAdapter.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/SettableItemAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$LoadingQuery;,
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$CommentQuery;,
        Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$ActivityQuery;
    }
.end annotation


# instance fields
.field private mActivityPosition:I

.field private mFlaggedComments:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHeights:Landroid/util/SparseIntArray;

.field private mLeftoverPosition:I

.field private mLoading:Z

.field private final mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

.field private final mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/apps/plus/views/OneUpListener;Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Lcom/google/android/apps/plus/views/OneUpListener;
    .param p4    # Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    iput-object p3, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    return-void
.end method


# virtual methods
.method public final addFlaggedComment(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v4, 0x1

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    move-object v3, p1

    check-cast v3, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mShouldAutoPlayInformer:Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->setShouldAutoPlayInformer(Lcom/google/android/apps/plus/views/OneUpSkyjamView$ShouldAutoPlayInformer;)V

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->bind(Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_1
    const/4 v4, 0x5

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    move-object v3, p1

    check-cast v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    invoke-static {p2}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setCustomBackground(Landroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->setOneUpClickListener(Lcom/google/android/apps/plus/views/OneUpListener;)V

    invoke-virtual {v3, p3, v1}, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;->bind(Landroid/database/Cursor;Z)V

    goto :goto_0

    :pswitch_2
    move-object v3, p1

    check-cast v3, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/plus/views/StreamOneUpCommentCountView;->bind(Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_3
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLoading:Z

    if-eqz v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$id;->loading_spinner:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$color;->riviera_reshare_background:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p1, v4}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    goto :goto_0

    :cond_0
    sget v4, Lcom/google/android/apps/plus/R$id;->loading_spinner:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final getAclText()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    if-gez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getActivityAuthorId()Ljava/lang/String;
    .locals 3

    iget v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getCount()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    iget v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_activity_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_comment_count_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_comment_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_3
    sget v2, Lcom/google/android/apps/plus/R$layout;->stream_one_up_loading_view:I

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final removeFlaggedComment(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public final setFlaggedComments(Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mFlaggedComments:Ljava/util/HashSet;

    return-void
.end method

.method public final setItemHeight(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    goto :goto_0
.end method

.method public final setLoading(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLoading:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLoading:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    if-eqz p1, :cond_3

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mActivityPosition:I

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mHeights:Landroid/util/SparseIntArray;

    iput v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter;->mLeftoverPosition:I

    goto :goto_1
.end method
