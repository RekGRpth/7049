.class public final Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;
.super Landroid/support/v4/content/CursorLoader;
.source "StreamOneUpLoader.java"


# instance fields
.field private final mActivityUri:Landroid/net/Uri;

.field private final mCommentsUri:Landroid/net/Uri;

.field private mNeedToRefreshComments:Z

.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v1, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-static {p2, p3}, Lcom/google/android/apps/plus/content/EsProvider;->buildActivityViewUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mActivityUri:Landroid/net/Uri;

    sget-object v1, Lcom/google/android/apps/plus/content/EsProvider;->COMMENTS_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-static {v0, p2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri$Builder;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri$Builder;

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final loadInBackground()Landroid/database/Cursor;
    .locals 14

    const/4 v12, 0x1

    const/4 v13, 0x0

    const/4 v11, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mActivityUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$ActivityQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v9, v6

    :goto_0
    return-object v9

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$CommentQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "created ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x7

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eq v1, v2, :cond_2

    move v1, v12

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mNeedToRefreshComments:Z

    new-instance v8, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/fragments/StreamOneUpAdapter$LoadingQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v8}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    const v2, 0x7ffffffc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    new-instance v9, Landroid/database/MergeCursor;

    const/4 v1, 0x3

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object v6, v1, v2

    const/4 v2, 0x1

    aput-object v7, v1, v2

    const/4 v2, 0x2

    aput-object v8, v1, v2

    invoke-direct {v9, v1}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_2
    move v1, v13

    goto :goto_1

    :catch_0
    move-exception v10

    const-string v1, "StreamOnUpLoader"

    const-string v2, "loadInBackground failed"

    invoke-static {v1, v2, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v9, v11

    goto :goto_0
.end method

.method public final bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final needToRefreshComments()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mNeedToRefreshComments:Z

    return v0
.end method

.method protected final onAbandon()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onReset()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->cancelLoad()Z

    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onReset()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->onAbandon()V

    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/support/v4/content/CursorLoader;->onStartLoading()V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mActivityUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mCommentsUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/StreamOneUpLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onStopLoading()V
    .locals 0

    return-void
.end method
