.class final Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;
.super Landroid/os/AsyncTask;
.source "UploadStatisticsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RescanMediaTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mFragment:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;->mFragment:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;->mFragment:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->processAllMedia()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->MEDIA_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_0
    return-object v2
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$RescanMediaTask;->mFragment:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "dialog_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method
