.class final Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "UploadStatisticsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UploadSatisticsLoader"
.end annotation


# instance fields
.field private final mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">.Force",
            "LoadContentObserver;"
        }
    .end annotation
.end field

.field private mObserverRegistered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    new-instance v0, Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-direct {v0, p0}, Landroid/support/v4/content/Loader$ForceLoadContentObserver;-><init>(Landroid/support/v4/content/Loader;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 27

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->MEDIA_URI:Landroid/net/Uri;

    sget-object v3, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "media_id DESC, upload_reason ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    new-instance v8, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v8, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsQuery;->PROJECTION:[Ljava/lang/String;

    array-length v1, v1

    new-array v0, v1, [Ljava/lang/Object;

    move-object/from16 v26, v0

    :goto_0
    if-eqz v13, :cond_4

    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    const/4 v1, 0x3

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v1, 0x4

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v23

    const/4 v1, 0x5

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v22

    const/4 v1, 0x6

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v24

    const/4 v1, 0x7

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const/16 v1, 0x8

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    const/4 v1, 0x0

    aget-object v1, v26, v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    aget-object v1, v26, v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    const/4 v1, 0x5

    aget-object v1, v26, v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v14, 0x1

    :goto_1
    const-wide/16 v1, -0x1

    cmp-long v1, v17, v1

    if-eqz v1, :cond_0

    cmp-long v1, v20, v17

    if-nez v1, :cond_0

    if-nez v14, :cond_1

    :cond_0
    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    const/4 v1, 0x0

    move-object/from16 v0, v26

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    const/4 v1, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v26, v1

    const/4 v1, 0x1

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v26, v1

    const/4 v1, 0x2

    aput-object v19, v26, v1

    const/4 v1, 0x3

    aput-object v7, v26, v1

    const/4 v1, 0x4

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v26, v1

    const/4 v1, 0x5

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v26, v1

    const/4 v1, 0x6

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v26, v1

    const/4 v1, 0x7

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v26, v1

    const/16 v1, 0x8

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v26, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    if-eqz v13, :cond_2

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1

    :cond_3
    const/4 v14, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :try_start_1
    aget-object v1, v26, v1

    if-eqz v1, :cond_5

    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    if-eqz v13, :cond_6

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_6
    return-object v8
.end method

.method protected final onAbandon()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserverRegistered:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method

.method protected final onStartLoading()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->onStartLoading()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserverRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->MEDIA_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserver:Landroid/support/v4/content/Loader$ForceLoadContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadSatisticsLoader;->mObserverRegistered:Z

    :cond_0
    return-void
.end method
