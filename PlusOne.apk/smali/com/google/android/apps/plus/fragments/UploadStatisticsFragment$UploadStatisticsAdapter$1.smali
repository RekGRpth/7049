.class final Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter$1;
.super Ljava/lang/Object;
.source "UploadStatisticsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter$1;->this$0:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->tag_media_url:I

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "image/*"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter$1;->this$0:Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    # getter for: Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->access$000(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
