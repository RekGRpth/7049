.class final Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "UploadStatisticsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "UploadStatisticsAdapter"
.end annotation


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 45
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    sget v3, Lcom/google/android/apps/plus/R$id;->thumb:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    check-cast v37, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;

    sget v3, Lcom/google/android/apps/plus/R$id;->state:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v35

    check-cast v35, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->desc:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->date:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->progress:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    check-cast v24, Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->shade:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v29

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    const/4 v3, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v27

    const/4 v3, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v26

    const/4 v3, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v41

    const/4 v3, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const/16 v3, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    div-int/lit8 v3, v27, 0x64

    mul-int/lit8 v32, v3, 0x64

    rem-int/lit8 v36, v27, 0x64

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v28

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v28

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getMediaId(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_4

    const/16 v19, 0x1

    :goto_0
    if-eqz v26, :cond_5

    move/from16 v40, v26

    :goto_1
    sget v3, Lcom/google/android/apps/plus/R$id;->tag_media_url:I

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_media_id:I

    invoke-static/range {v21 .. v22}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_upload_reason:I

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_row_id:I

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_2
    if-eqz v19, :cond_a

    sget v34, Lcom/google/android/apps/plus/R$string;->media_state:I

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    invoke-static/range {v23 .. v23}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->tag_media_url:I

    move-object/from16 v0, v37

    move-object/from16 v1, v23

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setTag(ILjava/lang/Object;)V

    new-instance v3, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter$1;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter$1;-><init>(Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;)V

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v3, 0x12c

    move/from16 v0, v32

    if-ne v0, v3, :cond_7

    const/high16 v3, -0x10000

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundColor(I)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x2

    new-array v5, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    div-int/lit8 v3, v32, 0x64

    mul-int/lit8 v3, v3, 0x64

    sparse-switch v3, :sswitch_data_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_unknown:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_4
    aput-object v3, v5, v6

    const/4 v6, 0x1

    sparse-switch v26, :sswitch_data_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_reason_unknown:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_5
    aput-object v3, v5, v6

    move/from16 v0, v34

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v35

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    rem-int/lit8 v3, v36, 0x64

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_unknown:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_6
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v3, 0x64

    move/from16 v0, v32

    if-ne v0, v3, :cond_c

    const/16 v39, 0x1

    :goto_7
    const/16 v3, 0xc8

    move/from16 v0, v32

    if-ne v0, v3, :cond_d

    const/16 v38, 0x1

    :goto_8
    if-nez v39, :cond_0

    if-eqz v38, :cond_e

    :cond_0
    const/16 v20, 0x1

    :goto_9
    if-eqz v19, :cond_f

    if-eqz v20, :cond_f

    const-wide/16 v3, 0x0

    cmp-long v3, v11, v3

    if-eqz v3, :cond_f

    const/16 v25, 0x1

    :goto_a
    if-eqz v25, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v5, Lcom/google/android/apps/plus/R$string;->media_progress:I

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    cmp-long v3, v11, v8

    if-eqz v3, :cond_1

    const-wide/16 v8, 0x0

    cmp-long v3, v13, v8

    if-nez v3, :cond_10

    :cond_1
    const/4 v3, 0x0

    :goto_b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    if-eqz v25, :cond_11

    const/4 v3, 0x0

    :goto_c
    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v19, :cond_12

    const-wide/16 v3, 0x0

    cmp-long v3, v41, v3

    if-lez v3, :cond_12

    const/16 v16, 0x1

    :goto_d
    if-eqz v16, :cond_3

    const-string v18, "MMM dd, yyyy h:mmaa"

    move-object/from16 v0, v18

    move-wide/from16 v1, v41

    invoke-static {v0, v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    if-eqz v16, :cond_13

    const/4 v3, 0x0

    :goto_e
    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v19, :cond_14

    const/16 v3, 0x8

    :goto_f
    move-object/from16 v0, v31

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_4
    const/16 v19, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v40, 0xa

    goto/16 :goto_1

    :cond_6
    sget v3, Lcom/google/android/apps/plus/R$id;->tag_row_id:I

    invoke-static/range {v29 .. v30}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_2

    :cond_7
    const/16 v3, 0x190

    move/from16 v0, v32

    if-ne v0, v3, :cond_8

    const v3, -0xff0100

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundColor(I)V

    goto/16 :goto_3

    :cond_8
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_9

    const/4 v3, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :cond_9
    const/4 v3, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :cond_a
    sget v34, Lcom/google/android/apps/plus/R$string;->media_state_removed:I

    const/4 v3, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    const/4 v3, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :sswitch_0
    rem-int/lit8 v3, v32, 0x64

    const/4 v7, 0x1

    if-ne v3, v7, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_uploading:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_pending:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_queued:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_failed:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_uploaded:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_state_not_uploaded:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_reason_dont_upload:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :sswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_reason_instant_share:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_reason_instant_upload:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_reason_manual:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/plus/R$string;->media_reason_upload_all:I

    invoke-virtual {v3, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_ok:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_in_progress:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_no_wifi:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_roaming:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_no_power:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_upsync_disabled:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_downsync_disabled:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_background_disabled:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_yielded:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_user_auth:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_no_storage:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_no_network:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_network_exception:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_no_quota:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_fail_user_auth:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_fail_no_storage:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_invalid_metadata:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_duplicate:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_no_fingerprint:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_is_disabled:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_google_exif:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_skipped:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mContext:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/plus/R$string;->media_status_cancelled:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_6

    :cond_c
    const/16 v39, 0x0

    goto/16 :goto_7

    :cond_d
    const/16 v38, 0x0

    goto/16 :goto_8

    :cond_e
    const/16 v20, 0x0

    goto/16 :goto_9

    :cond_f
    const/16 v25, 0x0

    goto/16 :goto_a

    :cond_10
    long-to-float v3, v13

    long-to-float v8, v11

    div-float/2addr v3, v8

    float-to-double v8, v3

    const-wide/high16 v43, 0x4059000000000000L

    mul-double v8, v8, v43

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v3, v8

    const/16 v8, 0x64

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto/16 :goto_b

    :cond_11
    const/16 v3, 0x8

    goto/16 :goto_c

    :cond_12
    const/16 v16, 0x0

    goto/16 :goto_d

    :cond_13
    const/16 v3, 0x8

    goto/16 :goto_e

    :cond_14
    const/4 v3, 0x0

    goto/16 :goto_f

    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_5
        0xa -> :sswitch_8
        0x14 -> :sswitch_6
        0x1e -> :sswitch_7
        0x28 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment$UploadStatisticsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->upload_statistics_media_row:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
