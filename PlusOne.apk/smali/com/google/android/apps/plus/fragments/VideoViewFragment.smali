.class public Lcom/google/android/apps/plus/fragments/VideoViewFragment;
.super Lcom/google/android/apps/plus/fragments/EsFragment;
.source "VideoViewFragment.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnInfoListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/EsFragment;",
        "Landroid/media/MediaPlayer$OnErrorListener;",
        "Landroid/media/MediaPlayer$OnInfoListener;",
        "Landroid/media/MediaPlayer$OnPreparedListener;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final PROJECTION:[Ljava/lang/String;

.field private static final sPlayableTypes:Landroid/util/SparseBooleanArray;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAuthkey:Ljava/lang/String;

.field private mError:Z

.field private final mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mIntent:Landroid/content/Intent;

.field private mIsWiFiConnection:Z

.field private mLoading:Z

.field private mOwnerId:Ljava/lang/String;

.field private mPerformedRefetch:Z

.field private mPhotoId:J

.field private mPlayOnResume:Z

.field private mPlayerView:Landroid/widget/VideoView;

.field private mPreviousOrientation:I

.field private mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

.field private mVideoPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->PROJECTION:[Ljava/lang/String;

    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x16

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    sget-object v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    const/16 v1, 0x24

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseBooleanArray;->put(IZ)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    new-instance v0, Lcom/google/android/apps/plus/fragments/VideoViewFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment$1;-><init>(Lcom/google/android/apps/plus/fragments/VideoViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/fragments/VideoViewFragment;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/fragments/VideoViewFragment;

    iget-wide v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    return-wide v0
.end method

.method private startPlayback()V
    .locals 10

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const-string v1, "READY"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FINAL"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_0
    iget-wide v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    const-wide/16 v7, 0x0

    cmp-long v1, v1, v7

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataVideo;->stream:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataVideoStream;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    move-object v0, v3

    :goto_0
    if-eqz v0, :cond_9

    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DataVideo;->stream:Ljava/util/List;

    if-nez v1, :cond_2

    move-object v0, v3

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v2, v3

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DataVideoStream;

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->height:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    sget-object v4, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->sPlayableTypes:Landroid/util/SparseBooleanArray;

    iget-object v9, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->formatId:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v4, v9}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_c

    iget-object v4, v1, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    const/16 v4, 0x280

    if-gt v8, v4, :cond_6

    move v4, v5

    :goto_3
    if-eqz v2, :cond_5

    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    if-eqz v9, :cond_3

    if-gtz v8, :cond_5

    :cond_3
    iget-boolean v9, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    if-nez v9, :cond_4

    if-eqz v4, :cond_4

    if-gtz v8, :cond_5

    :cond_4
    iget-boolean v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    if-nez v4, :cond_c

    if-gez v8, :cond_c

    :cond_5
    :goto_4
    move-object v2, v1

    goto :goto_2

    :cond_6
    move v4, v6

    goto :goto_3

    :cond_7
    if-nez v2, :cond_8

    move-object v0, v3

    goto :goto_0

    :cond_8
    iget-object v3, v2, Lcom/google/api/services/plusi/model/DataVideoStream;->url:Ljava/lang/String;

    move-object v0, v3

    goto :goto_0

    :cond_9
    iput-boolean v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->video_no_stream:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    goto :goto_1

    :cond_a
    const-string v1, "PENDING"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->video_not_ready:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    goto :goto_1

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->no_video:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    goto/16 :goto_1

    :cond_c
    move-object v1, v2

    goto :goto_4
.end method

.method private updateView(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v1, :cond_1

    const-string v1, "READY"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "FINAL"

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->showEmptyViewProgress(Landroid/view/View;)V

    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    if-nez v1, :cond_3

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->showContent(Landroid/view/View;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->showEmptyView(Landroid/view/View;)V

    goto :goto_1
.end method


# virtual methods
.method protected final isEmpty()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v3, :cond_3

    const-string v3, "READY"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "FINAL"

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    if-nez v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    if-eqz v3, :cond_2

    :cond_1
    move v0, v2

    :cond_2
    return v0

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v3, v1, Landroid/widget/MediaController;

    if-eqz v3, :cond_0

    move-object v2, v1

    check-cast v2, Landroid/widget/MediaController;

    invoke-virtual {v2}, Landroid/widget/MediaController;->isShowing()Z

    move-result v3

    if-nez v3, :cond_0

    :try_start_0
    invoke-virtual {v2}, Landroid/widget/MediaController;->show()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v3

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$string;->video_no_stream:I

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.google.android.apps.plus.VideoViewFragment.INTENT"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v2, "com.google.android.apps.plus.VideoViewFragment.POSITION"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoPosition:I

    const-string v2, "com.google.android.apps.plus.VideoViewFragment.PLAY_ON_RESUME"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    const-string v2, "com.google.android.apps.plus.VideoViewFragment.PREVIOUS_ORIENTATION"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v1, v2, Landroid/content/res/Configuration;->orientation:I

    iget v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    if-eq v2, v1, :cond_1

    iput v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "photo_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "owner_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "data"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "data"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataVideo;

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "auth_key"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    const-string v3, "auth_key"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAuthkey:Ljava/lang/String;

    :cond_3
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->PHOTO_BY_PHOTO_ID_URI:Landroid/net/Uri;

    iget-wide v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    invoke-static {v0, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v4, -0x1

    sget v3, Lcom/google/android/apps/plus/R$layout;->video_view_fragment:I

    invoke-super {p0, p1, p2, p3, v3}, Lcom/google/android/apps/plus/fragments/EsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v2

    new-instance v1, Landroid/widget/MediaController;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->videolayout:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/MediaController;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->videoplayer:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/VideoView;

    iput-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v3, v1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v3, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v3, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-nez v3, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    sget v3, Lcom/google/android/apps/plus/R$string;->no_video:I

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    return-object v2
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 8
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v7, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPerformedRefetch:Z

    if-nez v0, :cond_1

    if-ne p2, v7, :cond_1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPerformedRefetch:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mOwnerId:Ljava/lang/String;

    iget-wide v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPhotoId:J

    iget-object v5, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mAuthkey:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;)I

    :cond_0
    :goto_0
    return v7

    :cond_1
    iput-boolean v7, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$string;->video_no_stream:I

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->setupEmptyView(Landroid/view/View;I)V

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onInfo(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    sparse-switch p2, :sswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    :cond_0
    return v2

    :sswitch_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    goto :goto_0

    :sswitch_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    goto :goto_0

    :sswitch_2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mError:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x64 -> :sswitch_2
        0xc8 -> :sswitch_2
        0x2bd -> :sswitch_0
        0x2be -> :sswitch_1
    .end sparse-switch
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    iput-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->startPlayback()V

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    goto :goto_0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mLoading:Z

    invoke-virtual {p1, p0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoPosition:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    iget v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoPosition:I

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->seekTo(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->start()V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragment;->onResume()V

    iget-object v4, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mEsListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v4}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-string v5, "connectivity"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    if-ne v4, v2, :cond_1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIsWiFiConnection:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mVideoData:Lcom/google/api/services/plusi/model/DataVideo;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->startPlayback()V

    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->updateView(Landroid/view/View;)V

    return-void

    :cond_1
    move v2, v3

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.apps.plus.VideoViewFragment.INTENT"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "com.google.android.apps.plus.VideoViewFragment.POSITION"

    iget-object v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v1}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "com.google.android.apps.plus.VideoViewFragment.PLAY_ON_RESUME"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPlayOnResume:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v0, "com.google.android.apps.plus.VideoViewFragment.PREVIOUS_ORIENTATION"

    iget v1, p0, Lcom/google/android/apps/plus/fragments/VideoViewFragment;->mPreviousOrientation:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
