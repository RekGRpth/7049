.class public final Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;
.super Ljava/lang/Object;
.source "Cameras.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/Cameras;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CameraProperties"
.end annotation


# static fields
.field public static final FROYO_CAMERA_PROPERTIES:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;


# instance fields
.field private final frontFacing:Z

.field private final orientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    const/4 v1, 0x0

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;-><init>(ZI)V

    sput-object v0, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->FROYO_CAMERA_PROPERTIES:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    return-void
.end method

.method public constructor <init>(ZI)V
    .locals 0
    .param p1    # Z
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->frontFacing:Z

    iput p2, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->orientation:I

    return-void
.end method


# virtual methods
.method public final getOrientation()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->orientation:I

    return v0
.end method

.method public final isFrontFacing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->frontFacing:Z

    return v0
.end method
