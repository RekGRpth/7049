.class public final Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;
.super Ljava/lang/Object;
.source "Cameras.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/Cameras;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CameraResult"
.end annotation


# instance fields
.field private final camera:Landroid/hardware/Camera;

.field private final properties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;


# direct methods
.method public constructor <init>(Landroid/hardware/Camera;Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)V
    .locals 0
    .param p1    # Landroid/hardware/Camera;
    .param p2    # Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->camera:Landroid/hardware/Camera;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->properties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    return-void
.end method


# virtual methods
.method public final getCamera()Landroid/hardware/Camera;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->camera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public final getProperties()Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->properties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    return-object v0
.end method
