.class final Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;
.super Ljava/lang/Object;
.source "Cameras.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/Cameras;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SelectedCameras"
.end annotation


# instance fields
.field public final frontFacingCameraId:I

.field public final frontFacingCameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

.field public final rearFacingCameraId:I

.field public final rearFacingCameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;


# direct methods
.method public constructor <init>(ILcom/google/android/apps/plus/hangout/Cameras$CameraProperties;ILcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->rearFacingCameraId:I

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->rearFacingCameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    iput p3, p0, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->frontFacingCameraId:I

    iput-object p4, p0, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->frontFacingCameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    return-void
.end method
