.class final Lcom/google/android/apps/plus/hangout/Cameras;
.super Ljava/lang/Object;
.source "Cameras.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;,
        Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;,
        Lcom/google/android/apps/plus/hangout/Cameras$CameraType;,
        Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;
    }
.end annotation


# static fields
.field private static cameraGetCameraInfo:Ljava/lang/reflect/Method;

.field private static cameraGetNumberOfCameras:Ljava/lang/reflect/Method;

.field private static cameraInfoClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static cameraInfoFacing:Ljava/lang/reflect/Field;

.field private static cameraInfoFrontFacingConstant:I

.field private static cameraInfoOrientation:Ljava/lang/reflect/Field;

.field private static cameraOpen:Ljava/lang/reflect/Method;

.field private static gingerbreadCameraApiSupported:Z

.field private static selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    :try_start_0
    const-string v1, "android.hardware.Camera$CameraInfo"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoClass:Ljava/lang/Class;

    const-class v1, Landroid/hardware/Camera;

    const-string v2, "open"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraOpen:Ljava/lang/reflect/Method;

    const-class v1, Landroid/hardware/Camera;

    const-string v2, "getNumberOfCameras"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraGetNumberOfCameras:Ljava/lang/reflect/Method;

    const-class v1, Landroid/hardware/Camera;

    const-string v2, "getCameraInfo"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoClass:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraGetCameraInfo:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoClass:Ljava/lang/Class;

    const-string v2, "facing"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoFacing:Ljava/lang/reflect/Field;

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoClass:Ljava/lang/Class;

    const-string v2, "orientation"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoOrientation:Ljava/lang/reflect/Field;

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoClass:Ljava/lang/Class;

    const-string v2, "CAMERA_FACING_FRONT"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoFrontFacingConstant:I

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    sget-boolean v1, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadSelectCameras()Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_0
.end method

.method private static gingerbreadGetCameraProperties(I)Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;
    .locals 10
    .param p0    # I

    const/4 v2, 0x1

    const/4 v4, 0x0

    sget-boolean v5, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-nez v5, :cond_0

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Gingerbread camera API not supported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    :try_start_0
    sget-object v5, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoClass:Ljava/lang/Class;

    invoke-virtual {v5}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    sget-object v5, Lcom/google/android/apps/plus/hangout/Cameras;->cameraGetCameraInfo:Ljava/lang/reflect/Method;

    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v5, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoFacing:Ljava/lang/reflect/Field;

    invoke-virtual {v5, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoFrontFacingConstant:I

    if-ne v5, v6, :cond_1

    :goto_0
    sget-object v4, Lcom/google/android/apps/plus/hangout/Cameras;->cameraInfoOrientation:Ljava/lang/reflect/Field;

    invoke-virtual {v4, v0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v3

    new-instance v4, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;-><init>(ZI)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v4

    :cond_1
    move v2, v4

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :catch_1
    move-exception v1

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    :catch_2
    move-exception v1

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method private static gingerbreadGetNumberOfCameras()I
    .locals 4

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Gingerbread camera API not supported"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraGetNumberOfCameras:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static gingerbreadOpenCamera(I)Landroid/hardware/Camera;
    .locals 6
    .param p0    # I

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Gingerbread camera API not supported"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->cameraOpen:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static gingerbreadSelectCameras()Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;
    .locals 8

    const/4 v7, -0x1

    sget-boolean v6, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-nez v6, :cond_0

    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Gingerbread camera API not supported"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    const/4 v0, -0x1

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadGetNumberOfCameras()I

    move-result v6

    if-ge v2, v6, :cond_3

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadGetCameraProperties(I)Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->isFrontFacing()Z

    move-result v6

    if-eqz v6, :cond_2

    if-ne v0, v7, :cond_1

    move v0, v2

    move-object v1, v3

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-ne v4, v7, :cond_1

    move v4, v2

    move-object v5, v3

    goto :goto_1

    :cond_3
    new-instance v6, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    invoke-direct {v6, v4, v5, v0, v1}, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;-><init>(ILcom/google/android/apps/plus/hangout/Cameras$CameraProperties;ILcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)V

    return-object v6
.end method

.method public static isAnyCameraAvailable()Z
    .locals 1

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFrontFacingCameraAvailable()Z
    .locals 3

    const/4 v0, 0x0

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    iget v1, v1, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->frontFacingCameraId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public static isRearFacingCameraAvailable()Z
    .locals 3

    const/4 v0, 0x1

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    iget v1, v1, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->rearFacingCameraId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static open(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    sget-boolean v2, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadCameraApiSupported:Z

    if-eqz v2, :cond_5

    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    if-ne p0, v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    if-ne p0, v2, :cond_2

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Requested camera type not available"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    if-ne p0, v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    iget v0, v2, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->frontFacingCameraId:I

    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    iget-object v1, v2, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->frontFacingCameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    :goto_0
    new-instance v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Cameras;->gingerbreadOpenCamera(I)Landroid/hardware/Camera;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;-><init>(Landroid/hardware/Camera;Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)V

    :goto_1
    return-object v2

    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    if-ne p0, v2, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    iget v0, v2, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->rearFacingCameraId:I

    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras;->selectedCameras:Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;

    iget-object v1, v2, Lcom/google/android/apps/plus/hangout/Cameras$SelectedCameras;->rearFacingCameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Unknown camera type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    if-ne p0, v2, :cond_6

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Requested camera type not available"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    new-instance v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->FROYO_CAMERA_PROPERTIES:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;-><init>(Landroid/hardware/Camera;Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)V

    goto :goto_1
.end method
