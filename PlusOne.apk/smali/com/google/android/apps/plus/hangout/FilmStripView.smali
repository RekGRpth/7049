.class public Lcom/google/android/apps/plus/hangout/FilmStripView;
.super Landroid/widget/LinearLayout;
.source "FilmStripView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;
    }
.end annotation


# instance fields
.field private final eventHandler:Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;

.field private hangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private isResumed:Z

.field private final size:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/FilmStripView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->eventHandler:Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->micro_kind_max_dimension:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->size:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/FilmStripView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/FilmStripView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->isResumed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/FilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/FilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/FilmStripView;->addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method private addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    new-instance v1, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, p1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->size:I

    iget v3, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->size:I

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/FilmStripView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->hangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;->onResume()V

    return-void
.end method


# virtual methods
.method public final onPause()V
    .locals 6

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->isResumed:Z

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$ParticipantVideoView;->onPause()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->eventHandler:Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    return-void
.end method

.method public final onResume(Lcom/google/android/apps/plus/hangout/SelfVideoView;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->isResumed:Z

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v4, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->hangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMembersOrderedByEntry()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->size:I

    iget v5, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->size:I

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/FilmStripView;->addView(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/FilmStripView;->addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->eventHandler:Lcom/google/android/apps/plus/hangout/FilmStripView$EventHandler;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    return-void
.end method

.method public setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/FilmStripView;->hangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-void
.end method
