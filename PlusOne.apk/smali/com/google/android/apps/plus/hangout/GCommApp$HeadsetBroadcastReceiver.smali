.class final Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "GCommApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeadsetBroadcastReceiver"
.end annotation


# instance fields
.field private headsetPluggedIn:Z

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/GCommApp;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->headsetPluggedIn:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;-><init>(Lcom/google/android/apps/plus/hangout/GCommApp;)V

    return-void
.end method


# virtual methods
.method final isHeadsetPluggedIn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->headsetPluggedIn:Z

    return v0
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v0, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "state"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->headsetPluggedIn:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->audioFocus:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$400(Lcom/google/android/apps/plus/hangout/GCommApp;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->audioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$500(Lcom/google/android/apps/plus/hangout/GCommApp;)Landroid/media/AudioManager;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp$HeadsetBroadcastReceiver;->headsetPluggedIn:Z

    if-nez v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method
