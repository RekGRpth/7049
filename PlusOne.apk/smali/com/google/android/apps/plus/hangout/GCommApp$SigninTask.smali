.class final Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;
.super Landroid/os/AsyncTask;
.source "GCommApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SigninTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final context:Landroid/content/Context;

.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/GCommApp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/plus/hangout/GCommApp;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->context:Landroid/content/Context;

    return-void
.end method

.method private varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 5
    .param p1    # [Ljava/lang/Void;

    sget-boolean v2, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    array-length v2, p1

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$600(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "webupdates"

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/network/AuthData;->getAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v3, 0x0

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$702(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;)Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$800(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->signoutAndDisconnect()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    const-string v0, "Got null auth token. Raising authenticatioin error message."

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->context:Landroid/content/Context;

    const/4 v1, -0x1

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUTHENTICATION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$602(Lcom/google/android/apps/plus/hangout/GCommApp;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsAccount;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$800(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/GCommApp$SigninTask;->this$0:Lcom/google/android/apps/plus/hangout/GCommApp;

    # getter for: Lcom/google/android/apps/plus/hangout/GCommApp;->account:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->access$600(Lcom/google/android/apps/plus/hangout/GCommApp;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->connectAndSignin(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    goto :goto_0
.end method
