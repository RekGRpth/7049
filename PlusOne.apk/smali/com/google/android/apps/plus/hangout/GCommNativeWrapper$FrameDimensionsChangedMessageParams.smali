.class public Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;
.super Ljava/lang/Object;
.source "GCommNativeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FrameDimensionsChangedMessageParams"
.end annotation


# instance fields
.field private final dimensions:Lcom/google/android/apps/plus/hangout/RectangleDimensions;

.field private final requestID:I


# direct methods
.method public constructor <init>(ILcom/google/android/apps/plus/hangout/RectangleDimensions;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->requestID:I

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->dimensions:Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    return-void
.end method


# virtual methods
.method public final getDimensions()Lcom/google/android/apps/plus/hangout/RectangleDimensions;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->dimensions:Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    return-object v0
.end method

.method public final getRequestID()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$FrameDimensionsChangedMessageParams;->requestID:I

    return v0
.end method
