.class public Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;
.super Ljava/lang/Object;
.source "GCommNativeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "VideoSourceChangedMessageParams"
.end annotation


# instance fields
.field private final requestID:I

.field private final source:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private final videoAvailable:Z


# direct methods
.method public constructor <init>(ILcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->requestID:I

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->source:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-boolean p3, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->videoAvailable:Z

    return-void
.end method


# virtual methods
.method public final getRequestID()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->requestID:I

    return v0
.end method

.method public final getSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->source:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public final isVideoAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$VideoSourceChangedMessageParams;->videoAvailable:Z

    return v0
.end method
