.class final Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "HangoutPhoneTile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventHandler"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method private notifyListeners()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->listeners:Ljava/util/List;

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;->onParticipantPresenceChanged()V

    goto :goto_0
.end method


# virtual methods
.method public final onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$1200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Ljava/lang/Boolean;)V

    :cond_1
    return-void
.end method

.method public final onCallgrokLogUploadCompleted$4f708078()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->checkAndDismissCallgrokLogUploadProgressDialog()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$1100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    return-void
.end method

.method public final onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    const-string v0, "HangoutPhoneTile$EventHandler.onError(%s) %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    aput-object p0, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUTHENTICATION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    if-ne p1, v0, :cond_2

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$500(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v0, v1, p1, v3}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordErrorExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-static {v0, p1, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$600(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    goto :goto_0
.end method

.method public final onHangoutCreated(Lcom/google/android/apps/plus/service/Hangout$Info;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutCreated(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iput-object p1, v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutPhoneTile.onHangoutCreated: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->greenRoomParticipants:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHoaConsented:Z

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->enterHangout(Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/List;Z)V

    return-void
.end method

.method public final onHangoutWaitTimeout(Lcom/google/android/apps/plus/service/Hangout$Info;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onHangoutWaitTimeout(Lcom/google/android/apps/plus/service/Hangout$Info;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$900(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$1000(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_no_one_joined:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->READY_TO_LAUNCH_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->HANGOUT_ON_AIR:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$700(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showHoaNotification(Landroid/widget/Button;)V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->OUTDATED_CLIENT:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    if-ne p1, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$800(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$14;->$SwitchMap$com$google$android$apps$plus$hangout$GCommNativeWrapper$MeetingEnterError:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_unknown_error:I

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "HangoutPhoneTile.onMeetingEnterError: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_timeout_error:I

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_blocked_error:I

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_blocking_error:I

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_max_users_error:I

    goto :goto_1

    :pswitch_4
    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_server_error:I

    goto :goto_1

    :pswitch_5
    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_green_room_info_error:I

    goto :goto_1

    :pswitch_6
    const/4 v0, 0x1

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_hangout_over:I

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final onMeetingExited(Z)V
    .locals 6
    .param p1    # Z

    const/4 v5, 0x1

    const-string v1, "HangoutPhoneTile$EventHandler.onMeetingExited: this=%s, tile=%s clientInitiated=%s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    aput-object v3, v2, v5

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingExited(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v1, v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/StringUtils;->getDomain(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "google.com"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->uploadCallgrokLog()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_log_upload_title:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_log_upload_message:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getEsFragmentActivity()Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "log_upload"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2, v5}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordNormalExit(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;Z)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_exited:I

    invoke-virtual {v1, v2, v5}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_1
.end method

.method public final onMeetingMediaStarted()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMediaStarted()V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "com.google.android.apps.plus.hangout.HangoutTile"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "filmStrip_"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    if-eqz v1, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_FILM_STRIP:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    :goto_1
    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v3, v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->getHangoutNotificationIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/GCommService;->showHangoutNotification(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getHangoutTileActivity()Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->onMeetingMediaStarted()V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    goto :goto_1
.end method

.method public final onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->notifyListeners()V

    return-void
.end method

.method public final onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->notifyListeners()V

    return-void
.end method

.method public final onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->notifyListeners()V

    return-void
.end method

.method public final onMucEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMucEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->sendInvites()V

    return-void
.end method

.method public final onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$1200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Ljava/lang/Boolean;)V

    :cond_0
    return-void
.end method

.method public final onSignedIn(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedIn(Ljava/lang/String;)V

    const-string v3, "HangoutPhoneTile$EventHandler.onSignedIn: this=%s, tile=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v2

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;

    move-result-object v3

    if-nez v3, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->skipGreenRoom:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ENTERING_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v3, v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez v3, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    const-string v3, "hangout_ring_invitees"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->createHangout(Z)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v4, v4, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v5, v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->MissedCall:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v5, v6, :cond_3

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->greenRoomParticipants:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-boolean v5, v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHoaConsented:Z

    invoke-virtual {v3, v4, v1, v2, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->enterHangout(Lcom/google/android/apps/plus/service/Hangout$Info;ZLjava/util/List;Z)V

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->READY_TO_LAUNCH_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_0
.end method

.method public final onSignedOut()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedOut()V

    const-string v0, "HangoutPhoneTile$EventHandler.onSignedOut"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_signin_timeout_error:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_0
.end method

.method public final onSigninTimeOutError()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSigninTimeOutError()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutPhoneTile$EventHandler.onSigninTimeOutError: this="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_signin_timeout_error:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_0
.end method

.method public final onVideoMuteChanged(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    # invokes: Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->access$1300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Ljava/lang/Boolean;)V

    return-void
.end method
