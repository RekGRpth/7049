.class public Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
.super Lcom/google/android/apps/plus/hangout/HangoutTile;
.source "HangoutPhoneTile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$14;,
        Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;,
        Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$OverlayMenuSlideOutAnimationListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private mEmptyHangoutMessageView:Landroid/widget/TextView;

.field private final mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

.field private mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

.field private mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

.field private mHadConnectedParticipant:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHangoutLaunchJoinPanel:Landroid/view/ViewGroup;

.field private mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

.field private mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

.field private mHangoutSwitchMenuButton:Landroid/widget/ImageButton;

.field private mInnerActionBarEnabled:Z

.field private mInstructionsView:Landroid/view/View;

.field private mInstructionsViewFadeOutRunnable:Ljava/lang/Runnable;

.field private mInviteParticipantsMenuButton:Landroid/widget/ImageButton;

.field private mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

.field private mIsHangoutLite:Z

.field private mIsTileStarted:Z

.field private mJoinButton:Landroid/widget/Button;

.field private mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

.field private mMessageContainer:Landroid/view/View;

.field private mMessageView:Landroid/widget/TextView;

.field private mNeedToToastForInvite:Z

.field private mParticipantsView:Landroid/view/View;

.field private mRootView:Landroid/view/ViewGroup;

.field private mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

.field private mSelfVideoViewContainer:Landroid/widget/FrameLayout;

.field private mShowOverlayMenu:Z

.field private mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

.field private mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

.field private mSwitchCameraMenuItem:Landroid/widget/ImageButton;

.field private mTitleBarView:Landroid/view/View;

.field private mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

.field private mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

.field private mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

.field private mTopMenuView:Landroid/view/View;

.field private mTouchSensorView:Landroid/view/View;

.field private mUpButton:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHandler:Landroid/os/Handler;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsHangoutLite:Z

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInnerActionBarEnabled:Z

    const-string v0, "HangoutPhoneTile(): this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    aput-object p1, v1, v3

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/SelfVideoView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->checkAndDismissCallgrokLogUploadProgressDialog()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Landroid/view/View;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .param p1    # Landroid/view/View;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HangoutPhoneTile onExit with state:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Setting userRequestedMeetingExit to true"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    const-string v0, "Did not set userRequestedMeetingExit"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->fadeOutInstructionsView()V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mShowOverlayMenu:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mShowOverlayMenu:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsHangoutLite:Z

    return v0
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHadConnectedParticipant:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/ViewGroup;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    const/4 v4, 0x1

    const-string v0, "HangoutLaunchActivity#handleAuthenticationError: state=%s appState=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    aput-object v3, v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_authentication_error:I

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    .param p2    # Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_outdated_client_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_enter_upgrade:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x1080027

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v1

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$13;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$13;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v2, "error"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    return-object v0
.end method

.method private addSelfVideoViewToRootView()V
    .locals 4

    const/4 v3, -0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    if-ne v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private checkAndDismissCallgrokLogUploadProgressDialog()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "log_upload"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private fadeOutInstructionsView()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$anim;->fade_out:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/hangout/HideViewAnimationListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/hangout/HideViewAnimationListener;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    const/4 v0, 0x1

    const/16 v10, 0x8

    const/4 v8, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting state to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/hangout/ToastsView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/hangout/FilmStripView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->addSelfVideoViewToRootView()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    sget-object v2, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;)V

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$14;->$SwitchMap$com$google$android$apps$plus$hangout$HangoutTile$State:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->skipGreenRoom:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->removeAllParticipants()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->greenRoomParticipants:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->greenRoomParticipants:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->addParticipants(Ljava/util/ArrayList;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$10;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$10;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/views/ParticipantsGalleryView;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_launch_signing_in:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v1, v8}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    invoke-static {}, Lcom/google/android/apps/plus/hangout/StressMode;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v0, v8

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->fadeOutInstructionsView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_launch_joining:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v1, v10}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    invoke-virtual {v1, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    invoke-virtual {v1, v10}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->setVisibility(I)V

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->$assertionsDisabled:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIsHangoutLite()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsHangoutLite:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsHangoutLite:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteParticipantsMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v8}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-ne v1, v2, :cond_a

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsHangoutLite:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mParticipantsView:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setVisibility(I)V

    :cond_7
    sget-boolean v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->$assertionsDisabled:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eq v1, v2, :cond_8

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$anim;->slide_in_down_self:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$anim;->slide_out_up_self:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$anim;->slide_in_up_self:I

    invoke-static {v1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$anim;->slide_out_down_self:I

    invoke-static {v1, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    sget v1, Lcom/google/android/apps/plus/R$id;->overlay_menu:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_9

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mShowOverlayMenu:Z

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTouchSensorView:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$11;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$11;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Landroid/view/animation/Animation;Landroid/view/animation/Animation;Landroid/view/View;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->hangout_overlay_menu_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setExtraBottomOffset(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->addSelfVideoViewToRootView()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/hangout/FilmStripView;->setVisibility(I)V

    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateOverlayMenuAndMessageViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$12;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$12;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setVisibleViewOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsTileStarted:Z

    if-eqz v0, :cond_0

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-ne p1, v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->onPause()V

    goto/16 :goto_0

    :cond_9
    move v0, v8

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mParticipantsView:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTouchSensorView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/hangout/FilmStripView;->setVisibility(I)V

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->onPause()V

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/FilmStripView;->onResume(Lcom/google/android/apps/plus/hangout/SelfVideoView;)V

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->getRequestId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setMainVideoRequestId(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->startCapturing()V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-ne p1, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->onResume()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    .param p2    # Z

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$14;->$SwitchMap$com$google$android$apps$plus$hangout$GCommNativeWrapper$Error:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_authentication_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_fatal_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_fatal_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_network_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_audio_video_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_5
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_unknown_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private updateAudioMuteMenuButton(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_audio_unmute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_audio_unmute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_audio_mute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_audio_mute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateAudioMuteMenuButton(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->hasAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private updateOverlayMenuAndMessageViews()V
    .locals 7

    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHadConnectedParticipant:Z

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHadConnectedParticipant:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHadConnectedParticipant:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mShowOverlayMenu:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHasSomeConnectedParticipant()Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->INSET:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    :goto_3
    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->requestLayout()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v1

    if-le v1, v4, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHasSomeConnectedParticipant()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v5, "audience"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v5

    if-nez v5, :cond_6

    const-string v5, "audience"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v5

    if-lez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setInvitees(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setVisibility(I)V

    move v1, v4

    :goto_4
    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    move v1, v3

    goto :goto_4

    :cond_7
    sget-object v1, Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/SelfVideoView$LayoutMode;

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v1

    sget-object v5, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v1, v5, :cond_9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_already_ended:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getWaitingMessage(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v1

    sget-object v5, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-eq v1, v5, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRingInvitees()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_c
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v1

    if-ne v1, v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_no_one_joined:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private updateVideoMuteMenuButton(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_video_unmute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_video_unmute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_video_mute:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_menu_video_mute:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    if-nez v1, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateVideoMuteMenuButton(Z)V

    goto :goto_1
.end method


# virtual methods
.method public final isTileStarted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsTileStarted:Z

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mNeedToToastForInvite:Z

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const-string v0, "HangoutPhoneTile.onCreate: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->values()[Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v0

    const-string v1, "HangoutTile_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 4

    const-string v0, "HangoutPhoneTile.onPause: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    return-void
.end method

.method public final onResume()V
    .locals 4

    const-string v0, "HangoutPhoneTile.onResume: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    :goto_0
    const-string v1, "HangoutPhoneTile.onSaveInstanceState: this=%s context=%s eventHandler=%s stateToSave:%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "HangoutTile_state"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ordinal()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    const-string v0, "HangoutPhoneTile.onStart: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    return-void
.end method

.method public final onStop()V
    .locals 4

    const-string v0, "HangoutPhoneTile.onStop: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->stoppingHangoutActivity()V

    return-void
.end method

.method public final onTilePause()V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "HangoutPhoneTile.onTilePause: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->onPause()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->dismissAvatarMenuDialog()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->onPause()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->checkAndDismissCallgrokLogUploadProgressDialog()V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsTileStarted:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mState:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_FILM_STRIP:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->onPause()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final onTileResume()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const-string v3, "HangoutPhoneTile.onTileResume: this=%s context=%s eventHandler=%s hangoutInfo=%s"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->getErrorMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_1
    iput-boolean v7, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mIsTileStarted:Z

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->START:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->onResume()V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->onResume()V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$RoomType;->UNKNOWN:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    if-ne v3, v4, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_not_supported_type:I

    invoke-virtual {p0, v3, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_0

    :cond_2
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mNeedToToastForInvite:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_invites_sent:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    iput-boolean v8, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mNeedToToastForInvite:Z

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->hasAudioFocus()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v8}, Landroid/app/Activity;->setVolumeControlStream(I)V

    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "com.google.android.apps.plus.hangout.HangoutTile"

    invoke-virtual {v3, v4, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v3, "filmStrip_"

    invoke-interface {v1, v3, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_FILM_STRIP:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    :goto_2
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v4, Lcom/google/android/apps/plus/R$string;->hangout_audiofocus_loss_warning:I

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    goto :goto_1

    :cond_6
    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING_WITH_SELF_VIDEO_INSET:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mStateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/ExitHistory;->exitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v3}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    const-string v3, "Stopping hangout tile. Exit from hangout already reported."

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/ExitHistory;->exitedNormally(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v3

    if-eqz v3, :cond_9

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_exited:I

    invoke-virtual {p0, v3, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordExitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/hangout/ExitHistory;->getError(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    move-result-object v0

    if-eqz v0, :cond_a

    invoke-direct {p0, v0, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    goto :goto_3

    :cond_a
    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_exit_generic:I

    invoke-virtual {p0, v3, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto :goto_3

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v3

    if-eqz v3, :cond_c

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_launch_already_in_hangout:I

    invoke-virtual {p0, v3, v7}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->showError(IZ)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v3, v4, :cond_d

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getUserJid()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;->onSignedIn(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNING_IN:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_0
.end method

.method public final onTileStart()V
    .locals 10

    const/16 v5, 0x8

    const/4 v9, 0x0

    const/4 v4, 0x0

    const-string v3, "HangoutPhoneTile.onTileStart: this=%s context=%s eventHandler=%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p0, v6, v4

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v8, v6, v7

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v6, "layout_inflater"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/plus/R$layout;->hangout_tile:I

    invoke-virtual {v2, v3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->addView(Landroid/view/View;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_tile_root_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_top_menu:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->title_bar:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTitleBarView:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTitleBarView:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInnerActionBarEnabled:Z

    if-eqz v3, :cond_3

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_participants_info:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mParticipantsView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->touch_sensor_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTouchSensorView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->self_video_container:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    new-instance v3, Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6, v9}, Lcom/google/android/apps/plus/hangout/SelfVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->up:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mUpButton:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mUpButton:Landroid/view/View;

    new-instance v6, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$1;

    invoke-direct {v6, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->invite_participants:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteParticipantsMenuButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteParticipantsMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteParticipantsMenuButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$2;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_common_toggle_audio_mute:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateAudioMuteMenuButton(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$3;

    invoke-direct {v5, p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$3;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;Lcom/google/android/apps/plus/hangout/GCommApp;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_common_toggle_video_mute:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateVideoMuteMenuButton(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$4;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$4;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_common_hangout_switch:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSwitchMenuButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSwitchMenuButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_SWITCH:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSwitchMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutSwitchMenuButton:Landroid/widget/ImageButton;

    new-instance v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$5;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$5;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_common_switch_camera:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSwitchCameraMenuItem:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSwitchCameraMenuItem:Landroid/widget/ImageButton;

    new-instance v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$6;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$6;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_menu_common_exit:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$7;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$7;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSwitchCameraMenuItem:Landroid/widget/ImageButton;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_green_room_instructions:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$8;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$8;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_launch_join_panel:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_launch_join_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    new-instance v5, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$9;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$9;-><init>(Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->green_room_participants_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    sget v3, Lcom/google/android/apps/plus/R$id;->toasts_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/ToastsView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v3, Lcom/google/android/apps/plus/R$id;->main_video:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->film_strip:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/FilmStripView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/plus/hangout/FilmStripView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->hangout_participants_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v3, p0}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/R$id;->message_container:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->invitees_view:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/HangoutInviteesView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    sget v5, Lcom/google/android/apps/plus/R$id;->empty_message:I

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    invoke-virtual {v3, v5, v6, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    return-void

    :cond_3
    move v3, v5

    goto/16 :goto_0
.end method

.method public final onTileStop()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const-string v0, "HangoutPhoneTile.onTileStop: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v5

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEventHandler:Lcom/google/android/apps/plus/hangout/HangoutPhoneTile$EventHandler;

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTouchSensorView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->removeAllViews()V

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mRootView:Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTopMenuView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTitleBarView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mParticipantsView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTouchSensorView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleAudioMuteMenuButton:Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToggleVideoMuteMenuButton:Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSwitchCameraMenuItem:Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoViewContainer:Landroid/widget/FrameLayout;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mSelfVideoView:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mGreenRoomParticipantsGalleryView:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInstructionsView:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mJoinButton:Landroid/widget/Button;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/FilmStripView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageView:Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMessageContainer:Landroid/view/View;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mEmptyHangoutMessageView:Landroid/widget/TextView;

    return-void
.end method

.method public final setInnerActionBarEnabled(Z)Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;
    .locals 2
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mInnerActionBarEnabled:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTitleBarView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mTitleBarView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-object p0
.end method

.method public setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mHangoutParticipantsGalleryView:Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutParticipantsGalleryView;->setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V

    :cond_0
    return-void
.end method

.method public final transfer()V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    const-string v1, "Transfer hangout"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    const-string v2, "TRANSFER"

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_transfer_sent:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    return-void
.end method

.method public final updateMainVideoStreaming()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutPhoneTile;->mMainVideoView:Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/IncomingVideoView$MainVideoView;->updateVideoStreaming()V

    return-void
.end method
