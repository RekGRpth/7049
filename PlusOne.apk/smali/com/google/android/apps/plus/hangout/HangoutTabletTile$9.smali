.class final Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;
.super Ljava/lang/Object;
.source "HangoutTabletTile.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private onTarget(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getHeight()I

    move-result v1

    div-int/lit8 v0, v1, 0x4

    if-ltz p1, :cond_0

    add-int/lit8 v3, p1, 0x0

    if-gt v3, v2, :cond_0

    if-ltz p2, :cond_0

    add-int v3, p2, v0

    if-gt v3, v1, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/DragEvent;

    const v4, 0x3f59999a

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/high16 v3, 0x3f800000

    invoke-virtual {p2}, Landroid/view/DragEvent;->getLocalState()Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    instance-of v7, v1, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    if-eqz v7, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    :cond_0
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    move v3, v5

    :goto_0
    return v3

    :pswitch_0
    if-nez v2, :cond_1

    move v3, v5

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$color;->hangout_drag_drop_off_target:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setBackgroundColor(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v5

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setAlpha(F)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->invalidate()V

    move v3, v6

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v7

    float-to-int v7, v7

    invoke-direct {p0, v5, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->onTarget(II)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$color;->hangout_drag_drop_on_target:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setBackgroundColor(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v5

    if-nez v0, :cond_4

    :goto_3
    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setAlpha(F)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->invalidate()V

    move v3, v6

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$color;->hangout_drag_drop_off_target:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3

    :pswitch_2
    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->onTarget(II)Z

    move-result v3

    goto/16 :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setBackgroundColor(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setAlpha(F)V

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->invalidate()V

    invoke-virtual {p2}, Landroid/view/DragEvent;->getResult()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;->this$0:Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    # getter for: Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->requestPinVideo(Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;)V

    :cond_5
    move v3, v6

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
