.class public Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
.super Lcom/google/android/apps/plus/hangout/HangoutTile;
.source "HangoutTabletTile.java"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;,
        Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private bradyLayoutContainer:Landroid/widget/RelativeLayout;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

.field private hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

.field private instructionsView:Landroid/view/View;

.field private instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

.field private isRegistered:Z

.field private isTileStarted:Z

.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarDismissalTimer:Landroid/os/CountDownTimer;

.field private mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

.field private mCenterStageContainer:Landroid/widget/RelativeLayout;

.field private mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

.field private mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

.field private mEnableStageIcons:Z

.field private mFilmStripAnimOut:Landroid/view/animation/Animation;

.field private mFilmStripContainer:Landroid/view/View;

.field private mFilmStripIsPaused:Z

.field private mFilmStripPauseTimer:Landroid/os/CountDownTimer;

.field private mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

.field private final mHandler:Landroid/os/Handler;

.field private mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

.field private mInset:Landroid/view/View;

.field private mInsetVideo:Landroid/widget/FrameLayout;

.field private mInviteesContainer:Landroid/view/View;

.field private mInviteesMessageView:Landroid/widget/TextView;

.field private mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

.field private mIsAudioEnabled:Z

.field private mIsAudioMuted:Z

.field private mIsHangoutLite:Z

.field private mIsVideoMuted:Z

.field private mJoinButton:Landroid/widget/Button;

.field private mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

.field private mMessageContainer:Landroid/view/View;

.field private mMessageView:Landroid/widget/TextView;

.field private mNeedToToastForInvite:Z

.field private mSlideInUp:Landroid/view/animation/Animation;

.field private mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

.field private mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

.field private progressBar:Landroid/widget/ProgressBar;

.field private progressText:Landroid/widget/TextView;

.field private stageLayoutContainer:Landroid/widget/RelativeLayout;

.field private state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

.field private stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHandler:Landroid/os/Handler;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    const-string v0, "HangoutTabletTile(): this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    aput-object p1, v1, v3

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v1

    if-ne v1, p1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateMainVideoStreaming()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->checkAndDismissCallgrokLogUploadProgressDialog()V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mEnableStageIcons:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->getCurrentVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showAudioMutedStatus()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->hideAudioMutedStatus()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Ljava/lang/Boolean;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V

    goto :goto_0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Z

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTile$State;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->fadeOutInstructionsView()V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->pauseFilmStrip()V

    return-void
.end method

.method static synthetic access$2300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->resumeFilmStrip()V

    return-void
.end method

.method static synthetic access$2700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mEnableStageIcons:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getSelectedVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->hidePinnedStatus()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->isAudioMuted(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showAudioMutedStatus()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->showPinnedStatus()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->hideAudioMutedStatus()V

    goto :goto_1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    const/4 v4, 0x1

    const-string v0, "HangoutLaunchActivity#handleAuthenticationError: state=%s appState=%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    aput-object v3, v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isSigningIn()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_authentication_error:I

    invoke-virtual {p0, v0, v4}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNIN_ERROR:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    .param p2    # Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_enter_outdated_client_error:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_enter_upgrade:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x1080027

    invoke-static {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setCancelable(Z)V

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$11;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$11;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "error"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    return-object v0
.end method

.method private addVideoToCenterStage(Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_0
.end method

.method private checkAndDismissCallgrokLogUploadProgressDialog()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "log_upload"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_0
    return-void
.end method

.method private fadeOutInstructionsView()V
    .locals 3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$anim;->fade_out:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/hangout/HideViewAnimationListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/hangout/HideViewAnimationListener;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private hideActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->hide()V

    goto :goto_0
.end method

.method private hideFilmStrip()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripAnimOut:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private pauseFilmStrip()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onPause()V

    :cond_0
    return-void
.end method

.method private resumeFilmStrip()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripIsPaused:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onResume()V

    :cond_0
    return-void
.end method

.method private setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$HangoutTabletTile$StageViewMode:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown stage layout mode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->addVideoToCenterStage(Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :goto_1
    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->addVideoToCenterStage(Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    if-eq v0, v2, :cond_3

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->invalidate()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    const/4 v1, 0x0

    const/16 v6, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Setting state to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/plus/hangout/ToastsView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->bradyLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v2, v3, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_ONLY:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$HangoutTile$State:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_0

    :pswitch_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->skipGreenRoom:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_launch_signing_in:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-static {}, Lcom/google/android/apps/plus/hangout/StressMode;->isEnabled()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v1, 0x1

    :cond_3
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->fadeOutInstructionsView()V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_launch_joining:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    invoke-virtual {v2, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    sget-boolean v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v2

    if-nez v2, :cond_5

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v2, v3, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stageLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->bradyLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    :cond_6
    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateHangoutViews()V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->onResume()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->resumeFilmStrip()V

    goto/16 :goto_1

    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stageLayoutContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_2

    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_0

    goto/16 :goto_1

    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v2, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->onResume()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->resumeFilmStrip()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->onResume()V

    goto/16 :goto_1

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->show()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0
.end method

.method private showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;
    .param p2    # Z

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$12;->$SwitchMap$com$google$android$apps$plus$hangout$GCommNativeWrapper$Error:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_authentication_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_fatal_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_fatal_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_network_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_audio_video_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :pswitch_5
    sget v0, Lcom/google/android/apps/plus/R$string;->hangout_unknown_error:I

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private showFilmStrip()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mSlideInUp:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method private updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V
    .locals 5
    .param p1    # Ljava/lang/Boolean;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    if-nez v3, :cond_1

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->hasAudioFocus()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_2
    const/4 v0, 0x1

    :goto_2
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    if-eq v3, v1, :cond_3

    const/4 v2, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    if-eq v3, v0, :cond_4

    const/4 v2, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    :cond_4
    if-eqz v2, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->invalidateOptionsMenu()V

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v4, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v3, v4, :cond_8

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->showAudioMutedStatus()V

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->hideAudioMutedStatus()V

    goto :goto_1

    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_1
.end method

.method private updateHangoutViews()V
    .locals 12

    const/16 v6, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getIsHangoutLite()Z

    move-result v7

    iput-boolean v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->invalidateOptionsMenu()V

    if-eqz v4, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez v7, :cond_2

    :cond_0
    sget-object v7, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_ONLY:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMemberCount()I

    move-result v2

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v0

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v7

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Creation:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v7, v10, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRingInvitees()Z

    move-result v7

    if-eqz v7, :cond_4

    move v5, v8

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v7

    if-eqz v7, :cond_5

    move v1, v8

    :goto_2
    if-eqz v0, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_3
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v7}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHasSomeConnectedParticipant()Z

    move-result v7

    if-eqz v7, :cond_8

    sget-object v7, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_AND_REMOTE:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v5, v9

    goto :goto_1

    :cond_5
    move v1, v9

    goto :goto_2

    :cond_6
    if-eqz v5, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    invoke-virtual {v7}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v10, "audience"

    invoke-virtual {v7, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getHadSomeConnectedParticipantInPast()Z

    move-result v10

    if-nez v10, :cond_7

    const-string v10, "audience"

    invoke-virtual {v7, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v11

    invoke-virtual {v10, v7, v11}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setInvitees(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->getAvatarCount()I

    move-result v7

    if-lez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {v7, v9}, Lcom/google/android/apps/plus/views/HangoutInviteesView;->setVisibility(I)V

    move v5, v8

    :goto_4
    if-eqz v5, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_7
    move v5, v9

    goto :goto_4

    :cond_8
    sget-object v7, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_LOCAL_ONLY:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setStageViewMode(Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v7

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-ne v7, v10, :cond_9

    if-ne v2, v8, :cond_1

    if-nez v0, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v10, Lcom/google/android/apps/plus/R$string;->hangout_already_ended:I

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_9
    if-eqz v0, :cond_b

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    move-result-object v7

    sget-object v10, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Ring:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    if-eq v7, v10, :cond_a

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRingInvitees()Z

    move-result v7

    if-eqz v7, :cond_b

    :cond_a
    if-ne v2, v8, :cond_1

    if-nez v0, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v10, Lcom/google/android/apps/plus/R$string;->hangout_no_one_joined:I

    invoke-virtual {v8, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getWaitingMessage(Z)Ljava/lang/String;

    move-result-object v3

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v7

    if-eqz v7, :cond_c

    move v6, v9

    :cond_c
    if-eqz v1, :cond_1

    if-eqz v5, :cond_d

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesMessageView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_d
    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method private updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/Boolean;

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v0

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    if-nez v1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->invalidateOptionsMenu()V

    goto :goto_1
.end method


# virtual methods
.method public final hideChild(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideFilmStrip()V

    :cond_0
    return-void
.end method

.method public final isTileStarted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    return v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mNeedToToastForInvite:Z

    :cond_0
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    :cond_0
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildCount()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideFilmStrip()V

    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    const-wide/16 v11, 0x7530

    const-wide/16 v2, 0x1388

    const-wide/16 v9, 0x1f4

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string v0, "HangoutTabletTile.onCreate: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v4

    aput-object v4, v1, v8

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->values()[Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    move-result-object v0

    const-string v1, "HangoutTile_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_STAGE_STATUS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mEnableStageIcons:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    sget v0, Lcom/google/android/apps/plus/R$layout;->hangout_tablet_tile:I

    invoke-virtual {v6, v0, p0, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_INVALID:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    sget v0, Lcom/google/android/apps/plus/R$id;->stage_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stageLayoutContainer:Landroid/widget/RelativeLayout;

    sget v0, Lcom/google/android/apps/plus/R$id;->brady_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->bradyLayoutContainer:Landroid/widget/RelativeLayout;

    sget v0, Lcom/google/android/apps/plus/R$id;->inset:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInset:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->inset_video_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    new-instance v0, Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/plus/hangout/LocalVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->filmstrip_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripContainer:Landroid/view/View;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateAudioMuteMenuButtonState(Ljava/lang/Boolean;)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->updateVideoMuteMenuButtonState(Ljava/lang/Boolean;)V

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$id;->hangout_green_room_instructions:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsView:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$1;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    sget v0, Lcom/google/android/apps/plus/R$id;->hangout_launch_join_panel:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutLaunchJoinPanel:Landroid/view/ViewGroup;

    sget v0, Lcom/google/android/apps/plus/R$id;->hangout_launch_join_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mJoinButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$2;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->hangout_launch_progress_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressBar:Landroid/widget/ProgressBar;

    sget v0, Lcom/google/android/apps/plus/R$id;->hangout_launch_progress_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->progressText:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->toasts_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/ToastsView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v0, Lcom/google/android/apps/plus/R$id;->center_stage_video_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    new-instance v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->film_strip:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->message_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageContainer:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mMessageView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->invitees_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesContainer:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->invitees_message:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesMessageView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->invitees_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/HangoutInviteesView;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInviteesView:Lcom/google/android/apps/plus/views/HangoutInviteesView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$anim;->slide_in_up_self:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mSlideInUp:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$anim;->slide_out_down_self:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripAnimOut:Landroid/view/animation/Animation;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$3;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$3;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    new-instance v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$4;

    move-object v1, p0

    move-wide v2, v11

    move-wide v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$4;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripPauseTimer:Landroid/os/CountDownTimer;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBar:Landroid/app/ActionBar;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$5;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$5;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->addOnMenuVisibilityListener(Landroid/app/ActionBar$OnMenuVisibilityListener;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripAnimOut:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$6;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mSlideInUp:Landroid/view/animation/Animation;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$7;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$7;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mInsetVideo:Landroid/widget/FrameLayout;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$8;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$8;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageContainer:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$CenterStageTouchListener;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$9;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$anim;->fade_out:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$anim;->fade_in:I

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {v1, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    new-instance v3, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$10;-><init>(Lcom/google/android/apps/plus/hangout/HangoutTabletTile;Landroid/view/animation/Animation;Landroid/view/animation/Animation;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->setVideoChangeListener(Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;)V

    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->hangout_exit_menu:I

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->hangout_camera_switch_menu:I

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->hangout_audio_toggle_menu:I

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->hangout_video_toggle_menu:I

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->hangout_invite_people_menu:I

    invoke-virtual {p2, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_video_toggle_menu_item:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isAnyCameraAvailable()Z

    move-result v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_menu_switch_camera:I

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isFrontFacingCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Cameras;->isRearFacingCameraAvailable()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_exit_menu_item:I

    if-ne v1, v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "HangoutTabletTile onExit with state:"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Setting userRequestedMeetingExit to true"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->exitMeeting()V

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v2}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    const-string v2, "Did not set userRequestedMeetingExit"

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_menu_switch_camera:I

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0xc9

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    goto :goto_0

    :cond_3
    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_audio_toggle_menu_item:I

    if-ne v1, v2, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v3

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->setAudioMute(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :cond_5
    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_video_toggle_menu_item:I

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v4, 0xca

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendEmptyMessage(Landroid/content/Context;I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    goto :goto_0

    :cond_6
    sget v2, Lcom/google/android/apps/plus/R$id;->hangout_invite_menu_item:I

    if-ne v1, v2, :cond_7

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hideActionBar()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->inviteMoreParticipants()V

    goto :goto_0

    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v3

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    const-string v0, "HangoutTabletTile.onPause: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    return-void
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    sget v5, Lcom/google/android/apps/plus/R$id;->hangout_video_toggle_menu_item:I

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsVideoMuted:Z

    if-eqz v5, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_video_unmute:I

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_menu_video_unmute:I

    :goto_0
    invoke-interface {v4, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    :cond_0
    sget v5, Lcom/google/android/apps/plus/R$id;->hangout_audio_toggle_menu_item:I

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->isAudioMute()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioMuted:Z

    if-eqz v5, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_audio_unmute:I

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_menu_audio_unmute:I

    :goto_1
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsAudioEnabled:Z

    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    :cond_1
    sget v5, Lcom/google/android/apps/plus/R$id;->hangout_invite_menu_item:I

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mIsHangoutLite:Z

    if-nez v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    return-void

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_video_mute:I

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_menu_video_mute:I

    goto :goto_0

    :cond_3
    sget v1, Lcom/google/android/apps/plus/R$drawable;->hangout_ic_menu_audio_mute:I

    sget v3, Lcom/google/android/apps/plus/R$string;->hangout_menu_audio_mute:I

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public final onResume()V
    .locals 4

    const-string v0, "HangoutTabletTile.onResume: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    :goto_0
    const-string v1, "HangoutTabletTile.onSaveInstanceState: this=%s context=%s eventHandler=%s stateToSave:%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "HangoutTile_state"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->ordinal()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    goto :goto_0
.end method

.method public final onStart()V
    .locals 4

    const-string v0, "HangoutTabletTile.onStart: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActivity:Lcom/google/android/apps/plus/fragments/EsFragmentActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    return-void
.end method

.method public final onStop()V
    .locals 4

    const-string v0, "HangoutTabletTile.onStop: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->stoppingHangoutActivity()V

    return-void
.end method

.method public final onTilePause()V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "HangoutTabletTile.onTilePause: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sget-object v1, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->onPause()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->pauseFilmStrip()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mLocalVideoView:Lcom/google/android/apps/plus/hangout/LocalVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/LocalVideoView;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->dismissParticipantMenuDialog()V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;->STAGE_MODE_INVALID:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCurrentStageMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$StageViewMode;

    :goto_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->checkAndDismissCallgrokLogUploadProgressDialog()V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->instructionsViewFadeOutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_BRADY_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    goto :goto_2
.end method

.method public final onTileResume()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v1, "HangoutTabletTile.onTileResume: this=%s context=%s eventHandler=%s hangoutInfo=%s"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    sget-boolean v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showActionBar()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mHangoutSupportStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->getErrorMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_1
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isTileStarted:Z

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->START:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$Info;->getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/Hangout$RoomType;->UNKNOWN:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    if-ne v1, v2, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_not_supported_type:I

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mNeedToToastForInvite:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_invites_sent:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    iput-boolean v6, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mNeedToToastForInvite:Z

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->hasAudioFocus()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1, v6}, Landroid/app/Activity;->setVolumeControlStream(I)V

    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInHangout(Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->IN_MEETING:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangoutWithMedia()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v2, Lcom/google/android/apps/plus/R$string;->hangout_audiofocus_loss_warning:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->stateBeforeStop:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->exitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;

    invoke-interface {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile$HangoutTileActivity;->stopHangoutTile()V

    const-string v1, "Stopping hangout tile. Exit from hangout already reported."

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->exitedNormally(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_exited:I

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->recordExitReported(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->hangoutInfo:Lcom/google/android/apps/plus/service/Hangout$Info;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/ExitHistory;->getError(Landroid/content/Context;Lcom/google/android/apps/plus/service/Hangout$Info;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;Z)V

    goto :goto_2

    :cond_9
    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_exit_generic:I

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto :goto_2

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v1

    if-eqz v1, :cond_b

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_launch_already_in_hangout:I

    invoke-virtual {p0, v1, v5}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showError(IZ)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getCurrentState()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;->SIGNED_IN:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$GCommAppState;

    if-ne v1, v2, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getUserJid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;->onSignedIn(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->disconnect()V

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->SIGNING_IN:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->setState(Lcom/google/android/apps/plus/hangout/HangoutTile$State;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    goto/16 :goto_0
.end method

.method public final onTileStart()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "HangoutTabletTile.onTileStart: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "setHangoutInfo must be called before switching to HangoutTabletTile"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    return-void
.end method

.method public final onTileStop()V
    .locals 5

    const/4 v4, 0x0

    const-string v0, "HangoutTabletTile.onTileStop: this=%s context=%s eventHandler=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v4

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mActionBarDismissalTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->isRegistered:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->eventHandler:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$EventHandler;

    invoke-virtual {v0, v1, v2, v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    return-void
.end method

.method public setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final showChild(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mFilmStripView:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->showFilmStrip()V

    :cond_0
    return-void
.end method

.method public final transfer()V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;)V

    const-string v1, "Transfer hangout"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    const-string v2, "TRANSFER"

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->inviteToMeeting(Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;ZZ)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mToastsView:Lcom/google/android/apps/plus/hangout/ToastsView;

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_transfer_sent:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(I)V

    return-void
.end method

.method public final updateMainVideoStreaming()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mViewMode:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    sget-object v1, Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;->MODE_STAGE_VIEW:Lcom/google/android/apps/plus/hangout/HangoutTabletTile$ViewMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->state:Lcom/google/android/apps/plus/hangout/HangoutTile$State;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile$State;->isInMeeting()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/HangoutTabletTile;->mCenterStageVideo:Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;->updateVideoStreaming()V

    :cond_0
    return-void
.end method
