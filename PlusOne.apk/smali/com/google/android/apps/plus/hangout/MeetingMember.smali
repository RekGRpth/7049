.class public final Lcom/google/android/apps/plus/hangout/MeetingMember;
.super Ljava/lang/Object;
.source "MeetingMember.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/MeetingMember$SortByEntryOrder;,
        Lcom/google/android/apps/plus/hangout/MeetingMember$Status;
    }
.end annotation


# static fields
.field private static BIG_NASTY_GAIA_ID_PREFIX:Ljava/lang/String; = null

.field private static isAnonymousMuc:Z = false

.field private static final serialVersionUID:J = 0x17d491ee6a8eca90L


# instance fields
.field private currentStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

.field private final entryOrder:I

.field private final gaiaId:Ljava/lang/String;

.field private isMediaBlocked:Z

.field private final isSelf:Z

.field private final isSelfProfile:Z

.field private isVideoPaused:Z

.field private final memberMucJid:Ljava/lang/String;

.field private final nickName:Ljava/lang/String;

.field private previousStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

.field private vCard:Lcom/google/android/apps/plus/hangout/VCard;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isAnonymousMuc:Z

    const-string v0, "g:"

    sput-object v0, Lcom/google/android/apps/plus/hangout/MeetingMember;->BIG_NASTY_GAIA_ID_PREFIX:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z
    .param p6    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->memberMucJid:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->nickName:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->DISCONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->previousStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    sget-object v0, Lcom/google/android/apps/plus/hangout/MeetingMember$Status;->DISCONNECTED:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->currentStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->gaiaId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->entryOrder:I

    iput-boolean p5, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf:Z

    iput-boolean p6, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelfProfile:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/MeetingMember;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->entryOrder:I

    return v0
.end method


# virtual methods
.method public final getCurrentStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->currentStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/hangout/MeetingMember;->BIG_NASTY_GAIA_ID_PREFIX:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->gaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getMucJid()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->memberMucJid:Ljava/lang/String;

    return-object v0
.end method

.method final getName(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->vCard:Lcom/google/android/apps/plus/hangout/VCard;

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isAnonymousMuc:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->hangout_anonymous_person:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->nickName:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->vCard:Lcom/google/android/apps/plus/hangout/VCard;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VCard;->getFullName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPreviousStatus()Lcom/google/android/apps/plus/hangout/MeetingMember$Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->previousStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    return-object v0
.end method

.method public final getVCard()Lcom/google/android/apps/plus/hangout/VCard;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->vCard:Lcom/google/android/apps/plus/hangout/VCard;

    return-object v0
.end method

.method public final isMediaBlocked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked:Z

    return v0
.end method

.method public final isSelf()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf:Z

    return v0
.end method

.method public final isSelfProfile()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelfProfile:Z

    return v0
.end method

.method public final isVideoPaused()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused:Z

    return v0
.end method

.method public final setCurrentStatus(Lcom/google/android/apps/plus/hangout/MeetingMember$Status;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->currentStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->currentStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->previousStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->currentStatus:Lcom/google/android/apps/plus/hangout/MeetingMember$Status;

    goto :goto_0
.end method

.method public final setMediaBlocked(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked:Z

    return-void
.end method

.method final setVCard(Lcom/google/android/apps/plus/hangout/VCard;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/VCard;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->vCard:Lcom/google/android/apps/plus/hangout/VCard;

    return-void
.end method

.method public final setVideoPaused(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused:Z

    return-void
.end method
