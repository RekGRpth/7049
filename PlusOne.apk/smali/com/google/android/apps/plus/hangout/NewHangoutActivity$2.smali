.class final Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;
.super Ljava/lang/Object;
.source "NewHangoutActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$300(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Landroid/widget/Button;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$300(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Landroid/widget/Button;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$200(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/fragments/AudienceFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->isAudienceEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$100(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    # getter for: Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$200(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/fragments/AudienceFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->isAudienceEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$100(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$100(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->hangout_ring_toggle_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    neg-float v1, v1

    invoke-direct {v0, v4, v4, v1, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->access$100(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;->this$0:Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    sget v2, Lcom/google/android/apps/plus/R$id;->list_layout_parent:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method
