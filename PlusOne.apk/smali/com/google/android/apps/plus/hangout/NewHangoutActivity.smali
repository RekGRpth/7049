.class public Lcom/google/android/apps/plus/hangout/NewHangoutActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NewHangoutActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

.field private mHangoutButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    const/4 v0, 0x0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Lcom/google/android/apps/plus/fragments/AudienceFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/NewHangoutActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->HANGOUT_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 4
    .param p1    # Landroid/support/v4/app/Fragment;

    const/4 v3, 0x0

    const/4 v2, 0x1

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/AudienceFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/AudienceFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setCirclesUsageType(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setIncludePhoneOnlyContacts(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setIncludePlusPages(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setPublicProfileSearchEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setShowSuggestedPeople(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setFilterNullGaiaIds(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    new-instance v1, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$2;-><init>(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->new_hangout_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v0, Lcom/google/android/apps/plus/R$id;->start_hangout_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mHangoutButton:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity$1;-><init>(Lcom/google/android/apps/plus/hangout/NewHangoutActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->showTitlebar(Z)V

    sget v0, Lcom/google/android/apps/plus/R$string;->new_hangout_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->setTitlebarTitle(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v0, 0x7f0a003e

    if-ne p1, v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->hangout_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1    # Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v3, 0x102002c

    if-ne v1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    :goto_0
    return v2

    :cond_0
    sget v3, Lcom/google/android/apps/plus/R$id;->feedback:I

    if-ne v1, v3, :cond_1

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_FEEDBACK:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/phone/GoogleFeedback;->launch(Landroid/app/Activity;)V

    goto :goto_0

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v1, v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->url_param_help_hangouts:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 5

    const/4 v4, 0x1

    const-string v1, "NewHangoutActivity.onStart: this=%s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->signinUser(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->startingHangoutActivity(Lcom/google/android/apps/plus/fragments/EsFragmentActivity;)V

    return-void
.end method

.method protected onStop()V
    .locals 3

    const-string v0, "NewHangoutActivity.onStop: this=%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStop()V

    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->stoppingHangoutActivity()V

    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/NewHangoutActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
