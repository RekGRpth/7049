.class public abstract Lcom/google/android/apps/plus/hangout/RemoteVideoView;
.super Lcom/google/android/apps/plus/hangout/HangoutVideoView;
.source "RemoteVideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$CenterStageVideoView;,
        Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;
    }
.end annotation


# instance fields
.field private currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

.field private final eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

.field protected incomingVideoContainerHeight:I

.field protected incomingVideoContainerWidth:I

.field protected incomingVideoFrameHeight:I

.field protected incomingVideoFrameWidth:I

.field private isRegistered:Z

.field protected mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private mHaveSetVideoParams:Z

.field protected mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

.field private final mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

.field protected requestID:I

.field private showingUnknownAvatar:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameWidth:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameHeight:I

    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->NONE:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    iput v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    new-instance v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/RemoteVideoView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    new-instance v0, Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setVideoSurface(Landroid/view/View;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;->FIT:Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setLayoutMode(Lcom/google/android/apps/plus/hangout/HangoutVideoView$LayoutMode;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/VideoTextureView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/hangout/RemoteVideoView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/RemoteVideoView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/RemoteVideoView;)Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/RemoteVideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    return-object v0
.end method


# virtual methods
.method public final getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final getCurrentVideoSource()Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mCurrentVideoSource:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method public final isVideoShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->isDecoding()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure$3b4dfe4b(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x2

    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoFrameHeight:I

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->layoutVideo(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/RelativeLayout$LayoutParams;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mHaveSetVideoParams:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerWidth:I

    iget v1, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerHeight:I

    iget v1, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-le v0, v2, :cond_1

    :cond_0
    iget v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerWidth:I

    iget v0, v6, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iput v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerHeight:I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mHaveSetVideoParams:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    iget v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->incomingVideoContainerHeight:I

    sget-object v4, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;->AUTO_ZOOM:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;

    const/16 v5, 0xf

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->setIncomingVideoParameters(IIILcom/google/android/apps/plus/hangout/GCommNativeWrapper$ScalingMode;I)V

    :cond_1
    return-void
.end method

.method public final onPause()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->onPause()V

    iget v1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopIncomingVideo(I)V

    iput v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->requestID:I

    :cond_1
    return-void
.end method

.method public final onResume()V
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->eventHandler:Lcom/google/android/apps/plus/hangout/RemoteVideoView$EventHandler;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->isRegistered:Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->startVideo()V

    return-void
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setAlpha(F)V

    goto :goto_0
.end method

.method protected final setIncomingContent(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isMediaBlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isVideoPaused()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V

    goto :goto_0
.end method

.method protected final setIncomingContent(Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    const/16 v4, 0x8

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showVideoSurface()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->getSnapshotView()Landroid/widget/ImageView;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->AVATAR:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_5

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mVideoSurface:Lcom/google/android/apps/plus/hangout/VideoTextureView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->AVATAR:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-eq p1, v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_6

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showAvatar()V

    :goto_3
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->BLOCKED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showBlocked()V

    :goto_4
    sget-object v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;->VIDEO_PAUSED:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    if-ne p1, v2, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showPaused()V

    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideLogo()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->showingUnknownAvatar:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->currentContent:Lcom/google/android/apps/plus/hangout/RemoteVideoView$IncomingContentType;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideVideoSurface()V

    goto :goto_1

    :cond_4
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_5
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideAvatar()V

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hideBlocked()V

    goto :goto_4

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->hidePaused()V

    goto :goto_5
.end method

.method public setVideoChangeListener(Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/RemoteVideoView;->mListener:Lcom/google/android/apps/plus/hangout/RemoteVideoView$VideoChangeListener;

    return-void
.end method

.method protected abstract startVideo()V
.end method
