.class public Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;
.super Landroid/widget/LinearLayout;
.source "RingHangoutToggleWidget.java"


# instance fields
.field private icon:Landroid/widget/ImageView;

.field private label:Landroid/widget/TextView;

.field private ringInvitees:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->ringInvitees:Z

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->setOrientation(I)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->icon:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->icon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_ring_on:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setClickable(Z)V

    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->ring_on_hangout_label:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    const/16 v1, 0x14

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->icon:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget$1;-><init>(Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public final getRingInvitees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->ringInvitees:Z

    return v0
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->setRingInvitees(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->ring_on_hangout_label:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->setRingInvitees(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->ring_disabled_hangout_label:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public setRingInvitees(Z)V
    .locals 2
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->ringInvitees:Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->ring_on_hangout_label:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->icon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_ring_on:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->label:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$string;->ring_off_hangout_label:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/RingHangoutToggleWidget;->icon:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_ring_off:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
