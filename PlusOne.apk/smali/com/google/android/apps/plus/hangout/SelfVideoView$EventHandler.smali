.class final Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "SelfVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/SelfVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/SelfVideoView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/SelfVideoView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/SelfVideoView;)V

    return-void
.end method


# virtual methods
.method public final onCameraPreviewFrameDimensionsChanged(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onCameraPreviewFrameDimensionsChanged(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # setter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameWidth:I
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$602(Lcom/google/android/apps/plus/hangout/SelfVideoView;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # setter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->selfFrameHeight:I
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$702(Lcom/google/android/apps/plus/hangout/SelfVideoView;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->layout(II)V

    return-void
.end method

.method public final onCameraSwitchRequested()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    if-ne v0, v2, :cond_2

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->RearFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setLastUsedCameraType(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # invokes: Lcom/google/android/apps/plus/hangout/SelfVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    invoke-static {v2, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$000(Lcom/google/android/apps/plus/hangout/SelfVideoView;Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    :cond_1
    return-void

    :cond_2
    sget-object v1, Lcom/google/android/apps/plus/hangout/Cameras$CameraType;->FrontFacing:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    goto :goto_0
.end method

.method public final onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    sget-object v0, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;->AUDIO_VIDEO_SESSION:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$100(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/VideoCapturer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$802(Lcom/google/android/apps/plus/hangout/SelfVideoView;Z)Z

    :cond_0
    return-void
.end method

.method public final onOutgoingVideoStarted()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onOutgoingVideoStarted()V

    const-string v0, "Outgoing video started"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # operator-- for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$210(Lcom/google/android/apps/plus/hangout/SelfVideoView;)I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$300(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/HangoutTile;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$300(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/HangoutTile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->isTileStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->numPendingStartOutgoingVideoRequests:I
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$200(Lcom/google/android/apps/plus/hangout/SelfVideoView;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->surfaceView:Landroid/view/SurfaceView;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$400(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Landroid/view/SurfaceView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$100(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/VideoCapturer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->selectedCameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$500(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    :cond_0
    return-void
.end method

.method public final onVideoMuteToggleRequested()V
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->isOutgoingVideoMute()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/GCommApp;->getLastUsedCameraType()Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    move-result-object v4

    # invokes: Lcom/google/android/apps/plus/hangout/SelfVideoView;->restartOutgoingVideo(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    invoke-static {v1, v4}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$000(Lcom/google/android/apps/plus/hangout/SelfVideoView;Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v4

    if-nez v0, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setOutgoingVideoMute(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v4, 0xcb

    if-nez v0, :cond_2

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v4, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    # getter for: Lcom/google/android/apps/plus/hangout/SelfVideoView;->videoCapturer:Lcom/google/android/apps/plus/hangout/VideoCapturer;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->access$100(Lcom/google/android/apps/plus/hangout/SelfVideoView;)Lcom/google/android/apps/plus/hangout/VideoCapturer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stop()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/SelfVideoView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/SelfVideoView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/SelfVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->stopOutgoingVideo()V

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method
