.class final Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "StressMode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/StressMode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/StressMode;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/hangout/StressMode;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public final onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$Error;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$200(Lcom/google/android/apps/plus/hangout/StressMode;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->launchGreenRoomRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$300(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingEnterError(Lcom/google/android/apps/plus/hangout/GCommNativeWrapper$MeetingEnterError;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->meetingEnterRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$000(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onMeetingExited(Z)V
    .locals 4
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingExited(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$200(Lcom/google/android/apps/plus/hangout/StressMode;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->launchGreenRoomRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$300(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onMeetingMediaStarted()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMediaStarted()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->exitMeetingRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$400(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onSignedIn(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedIn(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->meetingEnterRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$000(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onSignedOut()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSignedOut()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$200(Lcom/google/android/apps/plus/hangout/StressMode;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->launchGreenRoomRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$300(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final onSigninTimeOutError()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onSigninTimeOutError()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/StressMode;->access$100(Lcom/google/android/apps/plus/hangout/StressMode;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/StressMode$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/StressMode;

    # getter for: Lcom/google/android/apps/plus/hangout/StressMode;->launchGreenRoomRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/StressMode;->access$300(Lcom/google/android/apps/plus/hangout/StressMode;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
