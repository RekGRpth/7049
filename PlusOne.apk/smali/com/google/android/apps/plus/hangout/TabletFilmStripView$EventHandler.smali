.class final Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "TabletFilmStripView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)V

    return-void
.end method


# virtual methods
.method public final onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onAudioMuteStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$500(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    return-void
.end method

.method public final onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$300(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$300(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/HangoutTile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutTile;->updateMainVideoStreaming()V

    return-void
.end method

.method public final onMeetingMemberEntered(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$000(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$100(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    return-void
.end method

.method public final onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # getter for: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$000(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->getMember()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    check-cast v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    const/4 v3, 0x1

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$200(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;Z)V

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$500(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    return-void
.end method

.method public final onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVideoPauseStateChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    # invokes: Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$300(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method public final onVolumeChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;I)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVolumeChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->access$600(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;I)V

    return-void
.end method
