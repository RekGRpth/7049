.class public Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
.super Landroid/widget/LinearLayout;
.source "TabletFilmStripView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;,
        Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;,
        Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;
    }
.end annotation


# instance fields
.field private mContextMenuDialog:Landroid/app/Dialog;

.field private mCurrentOrientation:I

.field private mDismissMenuTimer:Landroid/os/CountDownTimer;

.field private final mEventHandler:Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;

.field private final mFilmStripMargin:I

.field private mGCommAppInstance:Lcom/google/android/apps/plus/hangout/GCommApp;

.field private mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

.field private final mMeetingMembersByVideoView:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/hangout/HangoutVideoView;",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            ">;"
        }
    .end annotation
.end field

.field private mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

.field private mShouldShowStatusIcons:Z

.field private mShouldShowStatusIconsMockValue:Z

.field private mShouldShowStatusIconsMockValueIsSet:Z

.field private final mTimersByMeetingMember:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            "Landroid/os/CountDownTimer;",
            ">;"
        }
    .end annotation
.end field

.field private final mVideoViewsByMeetingMember:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/plus/hangout/MeetingMember;",
            "Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;",
            ">;"
        }
    .end annotation
.end field

.field private msResumed:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const-wide/16 v2, 0x1388

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mEventHandler:Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mMeetingMembersByVideoView:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mTimersByMeetingMember:Ljava/util/HashMap;

    iput v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mCurrentOrientation:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v0, Lcom/google/android/apps/plus/R$dimen;->hangout_filmstrip_margin:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mFilmStripMargin:I

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_HANGOUT_FILMSTRIP_STATUS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mShouldShowStatusIcons:Z

    new-instance v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$1;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$1;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;JJ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mDismissMenuTimer:Landroid/os/CountDownTimer;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/MeetingMember;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->unpinVideo()V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/GCommApp;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Landroid/os/CountDownTimer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mDismissMenuTimer:Landroid/os/CountDownTimer;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Landroid/app/Dialog;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mContextMenuDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;
    .param p2    # Z

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->removeParticipantVideo(Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;)Lcom/google/android/apps/plus/hangout/HangoutTile;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_4

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mShouldShowStatusIconsMockValueIsSet:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mShouldShowStatusIconsMockValue:Z

    :goto_1
    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->showAudioMutedStatus()V

    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setVolume(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->hideVolumeBar()V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mShouldShowStatusIcons:Z

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->hideAudioMutedStatus()V

    goto :goto_0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;I)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->hideAudioMutedStatus()V

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->setVolume(I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->isVideoShowing()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->showVolumeBar()V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mTimersByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->hideVolumeBar()V

    goto :goto_1
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutVideoView;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onVideoClicked(Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/TabletFilmStripView;
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutVideoView;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->unpinVideo()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->onVideoClicked(Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0
.end method

.method private addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    const-wide/16 v2, 0x7d0

    const/4 v5, -0x2

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v9, 0x1

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    if-eqz v7, :cond_1

    invoke-direct {p0, v7, v9}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->removeParticipantVideo(Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;Z)V

    :cond_1
    new-instance v6, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v6, v1, v4, p1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_2

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mFilmStripMargin:I

    iput v1, v8, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v6, v8}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;

    invoke-direct {v1, p0, v6}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$TouchListener;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/HangoutVideoView;)V

    invoke-virtual {v6, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v6, p1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v6, v1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->onResume()V

    new-instance v0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$2;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$2;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;JJLcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mTimersByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v9, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mMeetingMembersByVideoView:Ljava/util/HashMap;

    invoke-virtual {v1, v6, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_0

    :cond_4
    const/4 v9, 0x0

    goto :goto_1
.end method

.method private getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mGCommAppInstance:Lcom/google/android/apps/plus/hangout/GCommApp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mGCommAppInstance:Lcom/google/android/apps/plus/hangout/GCommApp;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    goto :goto_0
.end method

.method private onVideoClicked(Lcom/google/android/apps/plus/hangout/HangoutVideoView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutVideoView;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    const/4 v1, 0x0

    const/4 v5, 0x1

    new-instance v3, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;

    invoke-direct {v3, p0, p2}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView$ParticipantContextMenuHelper;-><init>(Lcom/google/android/apps/plus/hangout/TabletFilmStripView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    move-object v0, p1

    move-object v2, v1

    move-object v4, v3

    move v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/util/QuickActions;->show(Landroid/view/View;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/View$OnCreateContextMenuListener;Landroid/view/MenuItem$OnMenuItemClickListener;ZZ)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mContextMenuDialog:Landroid/app/Dialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mDismissMenuTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    return-void
.end method

.method private pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->updateMainVideoStreaming()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method private removeParticipantVideo(Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->onPause()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mMeetingMembersByVideoView:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mTimersByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->unpinVideo()V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->removeView(Landroid/view/View;)V

    return-void
.end method

.method private unpinVideo()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/GCommApp;->setSelectedVideoSource(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/HangoutTile;->updateMainVideoStreaming()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    return-void
.end method

.method private updateStatusOverlay(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mPinnedVideoMember:Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->showPinnedStatus()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->hidePinnedStatus()V

    goto :goto_0
.end method


# virtual methods
.method public final dismissParticipantMenuDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mDismissMenuTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mContextMenuDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mContextMenuDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mContextMenuDialog:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public final isAudioMuted(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mVideoViewsByMeetingMember:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/HangoutVideoView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/HangoutVideoView;->isAudioMuteStatusShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mCurrentOrientation:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mCurrentOrientation:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->dismissParticipantMenuDialog()V

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mEventHandler:Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/apps/plus/hangout/GCommApp;->unregisterForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    :goto_1
    if-ltz v1, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->removeParticipantVideo(Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;Z)V

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->removeAllViews()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->dismissParticipantMenuDialog()V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 7

    const/4 v6, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z

    iput v6, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mCurrentOrientation:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;->getMeetingMembersOrderedByEntry()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->addParticipantVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getGCommAppInstance()Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mEventHandler:Lcom/google/android/apps/plus/hangout/TabletFilmStripView$EventHandler;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/apps/plus/hangout/GCommApp;->registerForEvents(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommEventHandler;Z)V

    goto :goto_0
.end method

.method public final requestPinVideo(Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->msResumed:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/RemoteVideoView$ParticipantVideoView;->getMember()Lcom/google/android/apps/plus/hangout/MeetingMember;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/MeetingMember;->isSelf()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->pinVideo(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    :cond_0
    return-void
.end method

.method public setHangoutTile(Lcom/google/android/apps/plus/hangout/HangoutTile;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/HangoutTile;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mHangoutTile:Lcom/google/android/apps/plus/hangout/HangoutTile;

    return-void
.end method

.method public setMockGCommAppInstanceForTesting(Lcom/google/android/apps/plus/hangout/GCommApp;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/GCommApp;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mGCommAppInstance:Lcom/google/android/apps/plus/hangout/GCommApp;

    return-void
.end method

.method public setMockShouldShowStatusIconsForTesting(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mShouldShowStatusIconsMockValueIsSet:Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/TabletFilmStripView;->mShouldShowStatusIconsMockValue:Z

    return-void
.end method
