.class final Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;
.super Lcom/google/android/apps/plus/hangout/GCommEventHandler;
.source "ToastsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/ToastsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/hangout/ToastsView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/hangout/ToastsView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/hangout/ToastsView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;)V

    return-void
.end method


# virtual methods
.method public final onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p3    # Z

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMediaBlock(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/google/android/apps/plus/hangout/ToastsView$MediaBlockToast;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V

    return-void
.end method

.method public final onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onMeetingMemberExited(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V

    return-void
.end method

.method public final onMeetingMemberPresenceConnectionStatusChanged(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->shouldShowToastForMember(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V

    :cond_0
    return-void
.end method

.method public final onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;
    .param p2    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onRemoteMute(Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/ToastsView$RemoteMuteToast;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/apps/plus/hangout/ToastsView$RemoteMuteToast;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V

    return-void
.end method

.method public final onVCardResponse(Lcom/google/android/apps/plus/hangout/MeetingMember;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/hangout/MeetingMember;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/hangout/GCommEventHandler;->onVCardResponse(Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/ToastsView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->shouldShowToastForMember(Lcom/google/android/apps/plus/hangout/MeetingMember;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    new-instance v1, Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/ToastsView$EventHandler;->this$0:Lcom/google/android/apps/plus/hangout/ToastsView;

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/plus/hangout/ToastsView$MeetingMemberToast;-><init>(Lcom/google/android/apps/plus/hangout/ToastsView;Lcom/google/android/apps/plus/hangout/MeetingMember;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/ToastsView;->addToast(Lcom/google/android/apps/plus/hangout/ToastsView$ToastInfo;)V

    :cond_0
    return-void
.end method
