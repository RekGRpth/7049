.class public final Lcom/google/android/apps/plus/hangout/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static debuggable:Z

.field private static version:Ljava/lang/String;

.field private static versionName:Ljava/lang/String;


# direct methods
.method public static fitContentInContainer(DII)Lcom/google/android/apps/plus/hangout/RectangleDimensions;
    .locals 8
    .param p0    # D
    .param p2    # I
    .param p3    # I

    int-to-double v4, p2

    int-to-double v6, p3

    div-double v0, v4, v6

    cmpg-double v4, p0, v0

    if-gez v4, :cond_1

    int-to-double v4, p3

    mul-double/2addr v4, p0

    double-to-int v3, v4

    move v2, p3

    :goto_0
    if-gtz v2, :cond_0

    const/4 v2, 0x1

    :cond_0
    new-instance v4, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    invoke-direct {v4, v3, v2}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    return-object v4

    :cond_1
    move v3, p2

    int-to-double v4, p2

    const-wide/high16 v6, -0x4010000000000000L

    invoke-static {p0, p1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-int v2, v4

    goto :goto_0
.end method

.method public static getVersion()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/hangout/Utils;->version:Ljava/lang/String;

    return-object v0
.end method

.method static initialize(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x80

    :try_start_0
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v5, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x2

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    sput-boolean v5, Lcom/google/android/apps/plus/hangout/Utils;->debuggable:Z

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget-object v5, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sput-object v5, Lcom/google/android/apps/plus/hangout/Utils;->versionName:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v2, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "-"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/google/android/apps/plus/hangout/Utils;->version:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    move v5, v6

    goto :goto_0

    :catch_0
    move-exception v1

    sput-boolean v6, Lcom/google/android/apps/plus/hangout/Utils;->debuggable:Z

    const-string v5, "Error reading version"

    sput-object v5, Lcom/google/android/apps/plus/hangout/Utils;->versionName:Ljava/lang/String;

    goto :goto_1
.end method

.method public static isAppInstalled(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v0, p0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isOnMainThread(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
