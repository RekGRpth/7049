.class public final Lcom/google/android/apps/plus/hangout/VCard;
.super Ljava/lang/Object;
.source "VCard.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x17d491ee6a8e7c70L


# instance fields
.field private avatarData:[B

.field private avatarHash:Ljava/lang/String;

.field private cellPhoneNumber:Ljava/lang/String;

.field private fullName:Ljava/lang/String;

.field private homePhoneNumber:Ljava/lang/String;

.field private isAvatarModified:Z

.field private workPhoneNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # [B
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VCard;->fullName:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/hangout/VCard;->isAvatarModified:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/VCard;->avatarData:[B

    iput-object p4, p0, Lcom/google/android/apps/plus/hangout/VCard;->avatarHash:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/hangout/VCard;->homePhoneNumber:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/hangout/VCard;->workPhoneNumber:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/hangout/VCard;->cellPhoneNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getAvatarData()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->avatarData:[B

    return-object v0
.end method

.method public final getAvatarHash()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->avatarHash:Ljava/lang/String;

    return-object v0
.end method

.method public final getCellPhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->cellPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getFullName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->fullName:Ljava/lang/String;

    return-object v0
.end method

.method public final getHomePhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->homePhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final getIsAvatarModified()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->isAvatarModified:Z

    return v0
.end method

.method public final getWorkPhoneNumber()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VCard;->workPhoneNumber:Ljava/lang/String;

    return-object v0
.end method
