.class public final Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;
.super Lcom/google/android/apps/plus/hangout/VideoCapturer;
.source "VideoCapturer.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/hangout/VideoCapturer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TextureViewVideoCapturer"
.end annotation


# instance fields
.field protected final textureView:Landroid/view/TextureView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Landroid/view/TextureView;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .param p3    # Landroid/view/TextureView;
    .param p4    # Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/apps/plus/hangout/VideoCapturer;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->textureView:Landroid/view/TextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->textureView:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method


# virtual methods
.method public final onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->isSurfaceReady:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->startCapturingWhenSurfaceReady:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->isCapturing:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->startCapturing()Z

    :cond_0
    return-void
.end method

.method public final onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->isSurfaceReady:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->stopCapturing()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v0, 0x1

    return v0
.end method

.method public final onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    return-void
.end method

.method public final onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;

    return-void
.end method
