.class public Lcom/google/android/apps/plus/hangout/VideoCapturer;
.super Ljava/lang/Object;
.source "VideoCapturer.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/VideoCapturer$TextureViewVideoCapturer;,
        Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private volatile camera:Landroid/hardware/Camera;

.field private cameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

.field private cameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

.field private final context:Landroid/content/Context;

.field private volatile deviceRotation:I

.field private flashLightEnabled:Z

.field private volatile frameRotationBeforeDisplaying:I

.field private volatile frameRotationBeforeSending:I

.field private final holder:Landroid/view/SurfaceHolder;

.field private final host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

.field protected volatile isCapturing:Z

.field protected isSurfaceReady:Z

.field private final nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

.field private volatile previewFrameHeight:I

.field private volatile previewFrameWidth:I

.field protected startCapturingWhenSurfaceReady:Z

.field private supportsFlashLight:Z

.field protected surfaceTexture:Landroid/graphics/SurfaceTexture;

.field private final windowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Landroid/view/SurfaceHolder;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .param p3    # Landroid/view/SurfaceHolder;
    .param p4    # Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->holder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->holder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->holder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->windowManager:Landroid/view/WindowManager;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->deviceRotation:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .param p3    # Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->holder:Landroid/view/SurfaceHolder;

    iput-object p3, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->windowManager:Landroid/view/WindowManager;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->deviceRotation:I

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/VideoCapturer;)Z
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoCapturer;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->deviceRotation:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/VideoCapturer;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoCapturer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->configureCamera()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/VideoCapturer;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoCapturer;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeSending:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/VideoCapturer;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoCapturer;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->nativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/VideoCapturer;)Landroid/hardware/Camera;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoCapturer;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    return-object v0
.end method

.method private configureCamera()V
    .locals 18

    const-string v13, "*** configureCamera"

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isSurfaceReady:Z

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v13, :cond_2

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->surfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-static {v13, v11}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getBestMatchPreviewSize(Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;

    move-result-object v1

    const/16 v13, 0x11

    invoke-virtual {v11, v13}, Landroid/hardware/Camera$Parameters;->setPreviewFormat(I)V

    iget v13, v1, Landroid/hardware/Camera$Size;->width:I

    iget v14, v1, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v11, v13, v14}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    const-string v13, "Setting camera preview size to %d x %d"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget v0, v1, Landroid/hardware/Camera$Size;->width:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    iget v0, v1, Landroid/hardware/Camera$Size;->height:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight:Z

    invoke-virtual {v11}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v12

    if-eqz v12, :cond_0

    const-string v13, "torch"

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight:Z

    :cond_0
    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Camera flash light in torch mode supports: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight:Z

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight:Z

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled:Z

    if-eqz v13, :cond_1

    const-string v13, "torch"

    invoke-virtual {v11, v13}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    const-string v13, "Turning on flash light in torch mode"

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13, v11}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    const/16 v13, 0x11

    invoke-static {v13}, Landroid/graphics/ImageFormat;->getBitsPerPixel(I)I

    move-result v13

    int-to-long v4, v13

    iget v13, v1, Landroid/hardware/Camera$Size;->width:I

    iget v14, v1, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v13, v14

    int-to-long v13, v13

    mul-long v2, v13, v4

    const-wide/16 v13, 0x7

    add-long/2addr v13, v2

    const-wide/16 v15, 0x8

    div-long/2addr v13, v15

    long-to-int v6, v13

    const-string v13, "BitsPerPixel: %d BitsPerFrame: %d BufferSize: %d"

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    const/4 v8, 0x0

    :goto_1
    const/4 v13, 0x5

    if-ge v8, v13, :cond_5

    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    add-int/lit16 v14, v6, 0x400

    new-array v14, v14, [B

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->addCallbackBuffer([B)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    :try_start_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->holder:Landroid/view/SurfaceHolder;

    if-eqz v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->holder:Landroid/view/SurfaceHolder;

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v13

    const-string v13, "setPreviewDisplay failed.  Ignoring, but video capture is disabled"

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13}, Landroid/hardware/Camera;->release()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    :cond_3
    :goto_2
    return-void

    :cond_4
    :try_start_3
    const-string v13, "No surface for camera preview."

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :catch_1
    move-exception v7

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "camera.addCallbackBuffer failed.  Video capture is disabled: "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v13}, Landroid/hardware/Camera;->release()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    new-instance v14, Lcom/google/android/apps/plus/hangout/VideoCapturer$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$1;-><init>(Lcom/google/android/apps/plus/hangout/VideoCapturer;)V

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->windowManager:Landroid/view/WindowManager;

    invoke-interface {v13}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Display;->getRotation()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->deviceRotation:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->deviceRotation:I

    packed-switch v13, :pswitch_data_0

    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Invalid value of deviceOrientation"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    :pswitch_0
    const/4 v13, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-static {v14}, Lcom/google/android/apps/plus/hangout/Compatibility;->getCameraOrientation(Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-virtual {v14}, Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;->isFrontFacing()Z

    move-result v14

    if-eqz v14, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    sget-object v14, Lcom/google/android/apps/plus/util/Property;->HANGOUT_CAMERA_MIRRORED:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v14}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v14

    const-string v16, "FALSE"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_7

    const/4 v14, 0x1

    :goto_4
    if-eqz v14, :cond_8

    add-int v14, v13, v15

    rem-int/lit16 v14, v14, 0x168

    rsub-int v14, v14, 0x168

    rem-int/lit16 v14, v14, 0x168

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeSending:I

    :goto_5
    add-int v14, v13, v15

    rem-int/lit16 v14, v14, 0x168

    rsub-int v14, v14, 0x168

    rem-int/lit16 v14, v14, 0x168

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeDisplaying:I

    :goto_6
    const-string v14, "Device orientation is %d; camera orientation is %d"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v16, v17

    const/4 v13, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v16, v13

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v13, "frameRotationBeforeDisplaying is %d; frameRotationBeforeSending is %d"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeDisplaying:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeSending:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeDisplaying:I

    invoke-virtual {v13, v14}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameWidth:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameHeight:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeDisplaying:I

    sparse-switch v13, :sswitch_data_0

    :goto_7
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameWidth:I

    if-ne v13, v10, :cond_6

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameHeight:I

    if-eq v13, v9, :cond_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->context:Landroid/content/Context;

    const/16 v14, 0xcc

    new-instance v15, Lcom/google/android/apps/plus/hangout/RectangleDimensions;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameWidth:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameHeight:I

    move/from16 v17, v0

    invoke-direct/range {v15 .. v17}, Lcom/google/android/apps/plus/hangout/RectangleDimensions;-><init>(II)V

    invoke-static {v13, v14, v15}, Lcom/google/android/apps/plus/hangout/GCommApp;->sendObjectMessage(Landroid/content/Context;ILjava/lang/Object;)V

    goto/16 :goto_2

    :pswitch_1
    const/16 v13, 0x5a

    goto/16 :goto_3

    :pswitch_2
    const/16 v13, 0xb4

    goto/16 :goto_3

    :pswitch_3
    const/16 v13, 0x10e

    goto/16 :goto_3

    :cond_7
    const/4 v14, 0x0

    goto/16 :goto_4

    :cond_8
    add-int v14, v15, v13

    rem-int/lit16 v14, v14, 0x168

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeSending:I

    goto/16 :goto_5

    :cond_9
    sub-int v14, v15, v13

    add-int/lit16 v14, v14, 0x168

    rem-int/lit16 v14, v14, 0x168

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeSending:I

    sub-int v14, v15, v13

    add-int/lit16 v14, v14, 0x168

    rem-int/lit16 v14, v14, 0x168

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->frameRotationBeforeDisplaying:I

    goto/16 :goto_6

    :sswitch_0
    iget v13, v1, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameWidth:I

    iget v13, v1, Landroid/hardware/Camera$Size;->height:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameHeight:I

    goto :goto_7

    :sswitch_1
    iget v13, v1, Landroid/hardware/Camera$Size;->height:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameWidth:I

    iget v13, v1, Landroid/hardware/Camera$Size;->width:I

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameHeight:I

    goto :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_0
        0x10e -> :sswitch_1
    .end sparse-switch
.end method

.method private static getBestMatchPreviewSize(Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;
    .locals 13
    .param p0    # Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;
    .param p1    # Landroid/hardware/Camera$Parameters;

    const/4 v9, 0x0

    const/4 v0, 0x0

    invoke-static {p1, p0}, Lcom/google/android/apps/plus/hangout/Compatibility;->getSupportedPreviewSizes(Landroid/hardware/Camera$Parameters;Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;)Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/hardware/Camera$Size;

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getMisMatchArea(Landroid/hardware/Camera$Size;)J

    move-result-wide v6

    const-string v10, "Supported camera preview size: %d x %d x %d. mismatch area=%d"

    const/4 v8, 0x4

    new-array v11, v8, [Ljava/lang/Object;

    iget v8, v5, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v11, v9

    const/4 v8, 0x1

    iget v12, v5, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v11, v8

    const/4 v12, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getPreviewFrameRate()I

    move-result v8

    :goto_1
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v11, v12

    const/4 v8, 0x3

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v11, v8

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez v0, :cond_2

    move-object v0, v5

    goto :goto_0

    :cond_1
    move v8, v9

    goto :goto_1

    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getMisMatchArea(Landroid/hardware/Camera$Size;)J

    move-result-wide v1

    cmp-long v8, v6, v1

    if-gez v8, :cond_0

    move-object v0, v5

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private static getMisMatchArea(Landroid/hardware/Camera$Size;)J
    .locals 7
    .param p0    # Landroid/hardware/Camera$Size;

    const-wide/16 v5, 0x1e0

    const-wide/16 v3, 0x168

    const/16 v2, 0x1e0

    const/16 v1, 0x168

    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    if-gt v0, v2, :cond_0

    iget v0, p0, Landroid/hardware/Camera$Size;->height:I

    if-gt v0, v1, :cond_0

    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    rsub-int v0, v0, 0x1e0

    int-to-long v0, v0

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    rsub-int v2, v2, 0x168

    int-to-long v2, v2

    mul-long/2addr v2, v5

    add-long/2addr v0, v2

    :goto_0
    return-wide v0

    :cond_0
    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    if-le v0, v2, :cond_1

    iget v0, p0, Landroid/hardware/Camera$Size;->height:I

    if-le v0, v1, :cond_1

    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    add-int/lit16 v0, v0, -0x1e0

    int-to-long v0, v0

    mul-long/2addr v0, v3

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    add-int/lit16 v2, v2, -0x168

    int-to-long v2, v2

    iget v4, p0, Landroid/hardware/Camera$Size;->width:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    goto :goto_0

    :cond_1
    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    if-le v0, v2, :cond_3

    sget-boolean v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->$assertionsDisabled:Z

    if-nez v0, :cond_2

    iget v0, p0, Landroid/hardware/Camera$Size;->height:I

    if-le v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    add-int/lit16 v0, v0, -0x1e0

    int-to-long v0, v0

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    int-to-long v2, v2

    mul-long/2addr v0, v2

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    rsub-int v2, v2, 0x168

    int-to-long v2, v2

    mul-long/2addr v2, v5

    add-long/2addr v0, v2

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->$assertionsDisabled:Z

    if-nez v0, :cond_5

    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    if-gt v0, v2, :cond_4

    iget v0, p0, Landroid/hardware/Camera$Size;->height:I

    if-gt v0, v1, :cond_5

    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    iget v0, p0, Landroid/hardware/Camera$Size;->width:I

    rsub-int v0, v0, 0x1e0

    int-to-long v0, v0

    mul-long/2addr v0, v3

    iget v2, p0, Landroid/hardware/Camera$Size;->height:I

    add-int/lit16 v2, v2, -0x168

    int-to-long v2, v2

    iget v4, p0, Landroid/hardware/Camera$Size;->width:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public static getSizeOfCapturedFrames(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Landroid/hardware/Camera$Size;
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/hangout/Cameras;->open(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->getCamera()Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->getProperties()Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    move-result-object v1

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v5

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->getBestMatchPreviewSize(Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;

    move-result-object v4

    if-eqz v4, :cond_0

    const-string v5, "Size of captured frames %d x %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v8, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/hangout/Log;->info(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    :goto_0
    return-object v4

    :catch_0
    move-exception v3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error opening camera: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final flashLightEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled:Z

    return v0
.end method

.method public final isCapturing()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    return v0
.end method

.method public final start(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->startCapturingWhenSurfaceReady:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isSurfaceReady:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->startCapturing()Z

    :cond_0
    return-void
.end method

.method protected final startCapturing()Z
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    if-eqz v4, :cond_0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    return v2

    :cond_0
    :try_start_1
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraType:Lcom/google/android/apps/plus/hangout/Cameras$CameraType;

    invoke-static {v4}, Lcom/google/android/apps/plus/hangout/Cameras;->open(Lcom/google/android/apps/plus/hangout/Cameras$CameraType;)Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->getCamera()Landroid/hardware/Camera;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/Cameras$CameraResult;->getProperties()Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->cameraProperties:Lcom/google/android/apps/plus/hangout/Cameras$CameraProperties;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->configureCamera()V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v4}, Landroid/hardware/Camera;->startPreview()V

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    invoke-interface {v4}, Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;->onCapturingStateChanged$1385ff()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v2}, Landroid/hardware/Camera;->release()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    invoke-interface {v2}, Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;->onCapturingStateChanged$1385ff()V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    invoke-interface {v2, v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;->onCameraOpenError(Ljava/lang/RuntimeException;)V

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v2, v3

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final stop()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stopCapturing()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->startCapturingWhenSurfaceReady:Z

    return-void
.end method

.method protected final stopCapturing()V
    .locals 2

    const-string v0, "*** stopCapturing"

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->host:Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    invoke-interface {v0}, Lcom/google/android/apps/plus/hangout/VideoCapturer$Host;->onCapturingStateChanged$1385ff()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameWidth:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->previewFrameHeight:I

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final supportsFlashLight()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight:Z

    return v0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isSurfaceReady:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->startCapturingWhenSurfaceReady:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isCapturing:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->startCapturing()Z

    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->isSurfaceReady:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoCapturer;->stopCapturing()V

    return-void
.end method

.method public final toggleFlashLightEnabled()V
    .locals 2

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->supportsFlashLight:Z

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->flashLightEnabled:Z

    if-eqz v1, :cond_2

    const-string v1, "torch"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    const-string v1, "Turning on flash light in torch mode"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/VideoCapturer;->camera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const-string v1, "off"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    const-string v1, "Turning off flash light in torch mode"

    invoke-static {v1}, Lcom/google/android/apps/plus/hangout/Log;->debug(Ljava/lang/String;)V

    goto :goto_2
.end method
