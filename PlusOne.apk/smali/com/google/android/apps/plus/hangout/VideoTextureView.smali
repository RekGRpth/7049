.class public Lcom/google/android/apps/plus/hangout/VideoTextureView;
.super Lcom/google/android/apps/plus/views/GLTextureView;
.source "VideoTextureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;
    }
.end annotation


# instance fields
.field private final mGcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

.field private volatile mIsDecoding:Z

.field private final mRenderer:Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;

.field private volatile mRequestID:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/GLTextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mGcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->setEGLContextClientVersion(I)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;-><init>(Lcom/google/android/apps/plus/hangout/VideoTextureView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRenderer:Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRenderer:Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->setRenderer(Lcom/google/android/apps/plus/views/GLTextureView$Renderer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->setRenderMode(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoTextureView;->onPause()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/VideoTextureView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoTextureView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/VideoTextureView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mGcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/hangout/VideoTextureView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoTextureView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mIsDecoding:Z

    return p1
.end method


# virtual methods
.method public final isDecoding()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mIsDecoding:Z

    return v0
.end method

.method public final setRequestID(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRequestID:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoTextureView;->mRenderer:Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/VideoTextureView$Renderer;->reinitialize()V

    :cond_0
    return-void
.end method
