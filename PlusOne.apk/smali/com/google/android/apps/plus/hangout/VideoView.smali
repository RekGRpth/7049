.class public Lcom/google/android/apps/plus/hangout/VideoView;
.super Landroid/opengl/GLSurfaceView;
.source "VideoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/VideoView$Renderer;
    }
.end annotation


# instance fields
.field private final gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

.field private volatile reinitializeRenderer:Z

.field private volatile requestID:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommNativeWrapper()Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/VideoView;->setEGLContextClientVersion(I)V

    new-instance v0, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/hangout/VideoView$Renderer;-><init>(Lcom/google/android/apps/plus/hangout/VideoView;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/VideoView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/hangout/VideoView;->setRenderMode(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/VideoView;->onPause()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/VideoView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->reinitializeRenderer:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/hangout/VideoView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoView;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->reinitializeRenderer:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/VideoView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/VideoView;)Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/VideoView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->gcommNativeWrapper:Lcom/google/android/apps/plus/hangout/GCommNativeWrapper;

    return-object v0
.end method


# virtual methods
.method public setRequestID(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/hangout/VideoView;->requestID:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/VideoView;->reinitializeRenderer:Z

    :cond_0
    return-void
.end method
