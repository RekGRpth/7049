.class public final Lcom/google/android/apps/plus/hangout/crash/CrashReport;
.super Ljava/lang/Object;
.source "CrashReport.java"


# static fields
.field private static final LOG_TAGS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private crashProcessingError:Ljava/lang/String;

.field private final isTestCrash:Z

.field private final params:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private reportText:Ljava/lang/String;

.field private signature:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "GoogleMeeting"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "gcomm_native"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "libjingle"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "DEBUG"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->LOG_TAGS:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v0, "uninitialized non-null value"

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->crashProcessingError:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->isTestCrash:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/crash/CrashReport;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/crash/CrashReport;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->reportText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/crash/CrashReport;)Ljava/util/Map;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/crash/CrashReport;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/crash/CrashReport;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/crash/CrashReport;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->crashProcessingError:Ljava/lang/String;

    return-object v0
.end method

.method private static appendNonHangoutLog(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public static computeJavaCrashSignature(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 13
    .param p0    # Ljava/lang/Throwable;

    const/16 v12, 0xa

    new-array v3, v12, [I

    const/4 v4, -0x1

    invoke-virtual {p0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    array-length v11, v5

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v6

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v6, :cond_0

    add-int/lit8 v4, v4, 0x1

    array-length v11, v3

    rem-int v9, v4, v11

    aget v11, v3, v9

    aget-object v12, v5, v7

    invoke-virtual {v12}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v12

    xor-int/2addr v11, v12

    aput v11, v3, v9

    add-int/lit8 v4, v4, 0x1

    array-length v11, v3

    rem-int v9, v4, v11

    aget v11, v3, v9

    aget-object v12, v5, v7

    invoke-virtual {v12}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v12

    xor-int/2addr v11, v12

    aput v11, v3, v9

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v3

    array-length v10, v3

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_1

    aget v2, v0, v8

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11
.end method

.method private getSystemLogs()Z
    .locals 10

    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "logcat"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "-d"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "-v"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-string v8, "threadtime"

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {p0, v2, v5}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->processLogs(Ljava/lang/StringBuilder;Ljava/io/InputStream;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->reportText:Ljava/lang/String;

    const-string v5, "Logs successfully processed"

    iput-object v5, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->crashProcessingError:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Process;->destroy()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    const-string v5, "Error getting system logs: %s\n%s"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->crashProcessingError:Ljava/lang/String;

    move v3, v4

    goto :goto_0
.end method

.method private processLogs(Ljava/lang/StringBuilder;Ljava/io/InputStream;)V
    .locals 12
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v10, 0x4

    const/4 v11, 0x1

    new-instance v0, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, p2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/4 v6, 0x0

    const-string v9, "[\\p{Digit}-]+ \\p{Space}+[\\p{Digit}\\.:]+ \\p{Space}+[\\p{Digit}]+ \\p{Space}+[\\p{Digit}]+ \\p{Space}+\\p{Upper} \\p{Space}+([^:]+):((.*))"

    invoke-static {v9, v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v4

    const-string v9, "\\p{Space}+\\# [\\p{Digit}]+ \\p{Space}+pc \\p{Space}+(([\\p{XDigit}]{8})) \\p{Space}+[\\p{Alnum}/\\._-}]*libgcomm_jni\\.so"

    invoke-static {v9, v10}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v4, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v9

    if-nez v9, :cond_1

    invoke-static {p1, v3}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->appendNonHangoutLog(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v5, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    sget-object v9, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->LOG_TAGS:Ljava/util/List;

    invoke-interface {v9, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v9

    if-gez v9, :cond_2

    invoke-static {p1, v3}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->appendNonHangoutLog(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "\n"

    invoke-virtual {p1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v9, "DEBUG"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const-string v9, "*** *** *** *** ***"

    invoke-virtual {v2, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    const/4 v6, 0x1

    const-string v9, ""

    iput-object v9, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    :cond_3
    if-eqz v6, :cond_0

    invoke-virtual {v8, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    const/16 v10, 0x50

    if-ge v9, v10, :cond_0

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ","

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v7, v11}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    return-void
.end method


# virtual methods
.method public final generateReport(Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->getSystemLogs()Z

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "prod"

    const-string v2, "Google_Plus_Android"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v2, "ver"

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->isTestCrash:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Test-"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/apps/plus/hangout/Utils;->getVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "sig"

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "sig2"

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->signature:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "should_process"

    const-string v2, "F"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_board"

    sget-object v2, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_brand"

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_device"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_id"

    sget-object v2, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_manufacturer"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_model"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_product"

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "build_type"

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "version_codename"

    sget-object v2, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "version_incremental"

    sget-object v2, Landroid/os/Build$VERSION;->INCREMENTAL:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "version_release"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "version_sdk_int"

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->isTestCrash:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->params:Ljava/util/Map;

    const-string v1, "testing"

    const-string v2, "true"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/crash/CrashReport;->reportText:Ljava/lang/String;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/hangout/Utils;->getVersion()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final send(Landroid/app/Activity;Z)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Z

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/apps/plus/hangout/crash/CrashReport$1;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/plus/hangout/crash/CrashReport$1;-><init>(Lcom/google/android/apps/plus/hangout/crash/CrashReport;Landroid/app/Activity;Z)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
