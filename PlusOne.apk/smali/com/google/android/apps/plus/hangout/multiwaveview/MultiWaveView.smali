.class public Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
.super Landroid/view/View;
.source "MultiWaveView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;
    }
.end annotation


# instance fields
.field private mActiveTarget:I

.field private mAnimatingTargets:Z

.field private mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

.field private mChevronAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;",
            ">;"
        }
    .end annotation
.end field

.field private mChevronDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDirectionDescriptionsResourceId:I

.field private mDragging:Z

.field private mFeedbackCount:I

.field private mGrabbedState:I

.field private mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

.field private mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

.field private mHitRadius:F

.field private mHorizontalOffset:F

.field private mNewTargetResources:I

.field private mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

.field private mOuterRadius:F

.field private mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

.field private mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mResetListener:Landroid/animation/Animator$AnimatorListener;

.field private mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSnapMargin:F

.field private mTapRadius:F

.field private mTargetAnimations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetDescriptionsResourceId:I

.field private mTargetDrawables:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;",
            ">;"
        }
    .end annotation
.end field

.field private mTargetResourceId:I

.field private mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

.field private mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field private mVerticalOffset:F

.field private mVibrationDuration:I

.field private mVibrator:Landroid/os/Vibrator;

.field private mWaveCenterX:F

.field private mWaveCenterY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 18
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct/range {p0 .. p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    const/4 v15, 0x3

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    const/4 v15, -0x1

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    sget v15, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v16, 0xb

    move/from16 v0, v16

    if-lt v15, v0, :cond_0

    sget-object v15, Lcom/google/android/apps/plus/hangout/multiwaveview/Ease$Quad;->easeOut:Landroid/animation/TimeInterpolator;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$1;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$2;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$3;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$4;-><init>(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    sget-object v15, Lcom/google/android/apps/plus/R$styleable;->MultiWaveView:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v15}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    const/16 v15, 0xe

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    const/16 v15, 0xd

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    const/16 v15, 0x9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    const/16 v15, 0xb

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    const/16 v15, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    const/16 v15, 0xc

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const/16 v16, 0x3

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v16

    invoke-direct/range {v15 .. v16}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    int-to-float v15, v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    const-string v15, "window"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/WindowManager;

    new-instance v11, Landroid/util/DisplayMetrics;

    invoke-direct {v11}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    invoke-virtual {v15, v11}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v15, v11, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenWidth:I

    iget v15, v11, Landroid/util/DisplayMetrics;->heightPixels:I

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenHeight:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mScreenHeight:I

    move/from16 v16, v0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v15

    int-to-float v15, v15

    const v16, 0x3f666666

    mul-float v15, v15, v16

    const/high16 v16, 0x40000000

    div-float v16, v15, v16

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-lez v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v15

    div-int/lit8 v15, v15, 0x2

    :goto_0
    int-to-float v15, v15

    sub-float v15, v16, v15

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    const/high16 v16, 0x40000000

    mul-float v15, v15, v16

    float-to-int v7, v15

    const/16 v15, 0x8

    invoke-virtual {v2, v15}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    check-cast v15, Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v15}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v15

    check-cast v15, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v15, v7, v7}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const/4 v15, 0x4

    new-array v6, v15, [I

    fill-array-data v6, :array_0

    move-object v3, v6

    array-length v10, v6

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v10, :cond_4

    aget v4, v3, v9

    invoke-virtual {v2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    const/4 v8, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    if-ge v8, v15, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    if-eqz v5, :cond_2

    new-instance v15, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-direct {v15, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_1
    const/4 v15, 0x0

    goto :goto_0

    :cond_2
    const/4 v15, 0x0

    goto :goto_3

    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_4
    new-instance v12, Landroid/util/TypedValue;

    invoke-direct {v12}, Landroid/util/TypedValue;-><init>()V

    const/4 v15, 0x0

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v15

    if-eqz v15, :cond_5

    iget v15, v12, Landroid/util/TypedValue;->resourceId:I

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    if-eqz v15, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v15

    if-nez v15, :cond_7

    :cond_6
    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "Must specify at least one target drawable"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_7
    const/4 v15, 0x1

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v15

    if-eqz v15, :cond_9

    iget v13, v12, Landroid/util/TypedValue;->resourceId:I

    if-nez v13, :cond_8

    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "Must specify target descriptions"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setTargetDescriptionsResourceId(I)V

    :cond_9
    const/4 v15, 0x2

    invoke-virtual {v2, v15, v12}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    move-result v15

    if-eqz v15, :cond_b

    iget v13, v12, Landroid/util/TypedValue;->resourceId:I

    if-nez v13, :cond_a

    new-instance v15, Ljava/lang/IllegalStateException;

    const-string v16, "Must specify direction descriptions"

    invoke-direct/range {v15 .. v16}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v15

    :cond_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setDirectionDescriptionsResourceId(I)V

    :cond_b
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    if-lez v15, :cond_c

    const/4 v15, 0x1

    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setVibrateEnabled(Z)V

    return-void

    :cond_c
    const/4 v15, 0x0

    goto :goto_4

    nop

    :array_0
    .array-data 4
        0x4
        0x5
        0x6
        0x7
    .end array-data
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)F
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)F
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;IFF)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .param p1    # I
    .param p2    # F
    .param p3    # F

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mNewTargetResources:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;I)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .param p1    # I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mNewTargetResources:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;I)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    return-void
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    return v0
.end method

.method private announceText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->sendAccessibilityEvent(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private deactivateTargets()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    return-void
.end method

.method private getTargetDescription(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->loadDescriptions(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    const-string v0, "MultiWaveView"

    const-string v1, "The number of target drawables must be equal to the number of target descriptions."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private handleMove(Landroid/view/MotionEvent;)V
    .locals 30
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    move/from16 v28, v0

    if-nez v28, :cond_0

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->trySwitchToFirstTouchState(Landroid/view/MotionEvent;)Z

    :goto_0
    return-void

    :cond_0
    const/4 v4, -0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v10

    const/4 v13, 0x0

    :goto_1
    add-int/lit8 v28, v10, 0x1

    move/from16 v0, v28

    if-ge v13, v0, :cond_c

    if-ge v13, v10, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v26

    :goto_2
    if-ge v13, v10, :cond_3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v27

    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    move/from16 v28, v0

    sub-float v24, v26, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    move/from16 v28, v0

    sub-float v25, v27, v28

    mul-float v28, v24, v24

    mul-float v29, v25, v25

    add-float v28, v28, v29

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move/from16 v28, v0

    cmpl-float v28, v23, v28

    if-lez v28, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move/from16 v28, v0

    div-float v18, v28, v23

    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    move/from16 v28, v0

    mul-float v29, v24, v18

    add-float v14, v28, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    move/from16 v28, v0

    mul-float v29, v25, v18

    add-float v15, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_5

    const/16 v19, 0x1

    :goto_5
    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    move/from16 v29, v0

    sub-float v20, v28, v29

    cmpl-float v28, v23, v20

    if-lez v28, :cond_1

    const/4 v4, 0x0

    move/from16 v26, v14

    move/from16 v27, v15

    :cond_1
    :goto_6
    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v4, v0, :cond_b

    const/16 v28, 0x3

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    if-eqz v19, :cond_9

    move/from16 v16, v14

    :goto_7
    if-eqz v19, :cond_a

    move/from16 v17, v15

    :goto_8
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v28, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    invoke-virtual {v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->hasState$25e2147()Z

    :goto_9
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v26

    goto/16 :goto_2

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v27

    goto/16 :goto_3

    :cond_4
    const/high16 v18, 0x3f800000

    goto/16 :goto_4

    :cond_5
    const/16 v19, 0x0

    goto :goto_5

    :cond_6
    const v5, 0x7f7fffff

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    move/from16 v29, v0

    mul-float v11, v28, v29

    const/4 v12, 0x0

    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/util/ArrayList;->size()I

    move-result v28

    move/from16 v0, v28

    if-ge v12, v0, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v28

    sub-float v8, v14, v28

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v28

    sub-float v9, v15, v28

    mul-float v28, v8, v8

    mul-float v29, v9, v9

    add-float v7, v28, v29

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->isValid()Z

    move-result v28

    if-eqz v28, :cond_7

    cmpg-float v28, v7, v11

    if-gez v28, :cond_7

    cmpg-float v28, v7, v5

    if-gez v28, :cond_7

    move v4, v12

    move v5, v7

    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_a

    :cond_8
    move/from16 v26, v14

    move/from16 v27, v15

    goto/16 :goto_6

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v16

    goto/16 :goto_7

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v17

    goto/16 :goto_8

    :cond_b
    const/16 v28, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v28, v0

    const/high16 v29, 0x3f800000

    invoke-virtual/range {v28 .. v29}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto/16 :goto_9

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->invalidateGlobalRegion(Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-eq v0, v4, :cond_e

    const/16 v28, -0x1

    move/from16 v0, v28

    if-eq v4, v0, :cond_e

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->vibrate()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    move-object/from16 v28, v0

    if-eqz v28, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    move-object/from16 v28, v0

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v28

    const-string v29, "accessibility"

    invoke-virtual/range {v28 .. v29}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v28

    if-eqz v28, :cond_e

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getTargetDescription(I)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->announceText(Ljava/lang/String;)V

    :cond_e
    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    goto/16 :goto_0
.end method

.method private hideChevrons()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private hideTargets(Z)V
    .locals 14
    .param p1    # Z

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    const/4 v10, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    if-eqz p1, :cond_3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_3

    if-eqz p1, :cond_1

    const/16 v0, 0x4b0

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    int-to-long v5, v0

    const/4 v7, 0x6

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "alpha"

    aput-object v8, v7, v3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    aput-object v8, v7, v11

    const-string v8, "delay"

    aput-object v8, v7, v12

    const/16 v8, 0xc8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v13

    const/4 v8, 0x4

    const-string v9, "onUpdate"

    aput-object v9, v7, v8

    const/4 v8, 0x5

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v9, v7, v8

    invoke-static {v2, v5, v6, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move v0, v3

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    int-to-long v6, v0

    const/16 v8, 0x8

    new-array v8, v8, [Ljava/lang/Object;

    const-string v9, "alpha"

    aput-object v9, v8, v3

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v8, v11

    const-string v3, "delay"

    aput-object v3, v8, v12

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v13

    const/4 v3, 0x4

    const-string v9, "onUpdate"

    aput-object v9, v8, v3

    const/4 v3, 0x5

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v9, v8, v3

    const/4 v3, 0x6

    const-string v9, "onComplete"

    aput-object v9, v8, v3

    const/4 v3, 0x7

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetUpdateListener:Landroid/animation/Animator$AnimatorListener;

    aput-object v9, v8, v3

    invoke-static {v5, v6, v7, v8}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    return-void

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v3, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    invoke-virtual {v2, v10}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v3, v10}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_2
.end method

.method private hideUnselected(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    if-eq v0, p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    return-void
.end method

.method private internalSetTargetResources(I)V
    .locals 10
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v1

    const/4 v5, 0x0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_1

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    new-instance v8, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-direct {v8, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    if-le v8, v5, :cond_0

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getHeight()I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    int-to-float v8, v8

    const v9, 0x3f666666

    mul-float/2addr v8, v9

    const/high16 v9, 0x40000000

    div-float/2addr v8, v9

    iput v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    iget v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    float-to-int v8, v8

    mul-int/lit8 v2, v8, 0x2

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v8, v2, v2}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    new-instance v8, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget-object v9, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetResourceId:I

    iput-object v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->updateTargetPositions()V

    return-void
.end method

.method private loadDescriptions(I)Ljava/util/ArrayList;
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->length()I

    move-result v2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-object v4
.end method

.method private moveHandleTo$483d6f3f(FF)V
    .locals 1
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    return-void
.end method

.method private static resolveMeasured(II)I
    .locals 3
    .param p0    # I
    .param p1    # I

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p0}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    move v0, v1

    :goto_0
    return v0

    :sswitch_0
    move v0, p1

    goto :goto_0

    :sswitch_1
    invoke-static {v1, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method

.method private setGrabbedState(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mGrabbedState:I

    if-eq p1, v0, :cond_1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->vibrate()V

    :cond_0
    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mGrabbedState:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mGrabbedState:I

    :cond_1
    return-void
.end method

.method private stopChevronAnimation()V
    .locals 4

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->end()V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private stopHandleAnimation()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    iget-object v0, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    goto :goto_0
.end method

.method private stopTargetAnimation()V
    .locals 4

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    iget-object v2, v0, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->animator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->end()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    :cond_1
    return-void
.end method

.method private switchToState$48676aae(I)V
    .locals 10
    .param p1    # I

    const/4 v5, 0x0

    const/16 v9, 0xb

    const/high16 v6, 0x3f800000

    const/4 v2, 0x1

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->deactivateTargets()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopHandleAnimation()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->deactivateTargets()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetAnimations:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    :cond_1
    iput-boolean v3, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v5, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setGrabbedState(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "accessibility"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_2
    if-ge v4, v6, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getTargetDescription(I)Ljava/lang/String;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_3
    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->loadDescriptions(I)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-eq v1, v8, :cond_6

    const-string v1, "MultiWaveView"

    const-string v8, "The number of target drawables must be euqal to the number of direction descriptions."

    invoke-static {v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_3
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    new-array v8, v2, [Ljava/lang/Object;

    aput-object v7, v8, v3

    invoke-static {v1, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->announceText(Ljava/lang/String;)V

    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_3

    :pswitch_3
    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    const/4 v1, -0x1

    if-eq v7, v1, :cond_8

    move v1, v2

    :goto_4
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v1, :cond_9

    move v4, v5

    :goto_5
    invoke-virtual {v8, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideUnselected(I)V

    const-string v1, "MultiWaveView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "Finish with target hit = "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mActiveTarget:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->vibrate()V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    invoke-interface {v4, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;->onTrigger$5359dc9a(I)V

    :cond_7
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const-wide/16 v4, 0x0

    const/16 v7, 0xe

    new-array v7, v7, [Ljava/lang/Object;

    const-string v8, "ease"

    aput-object v8, v7, v3

    sget-object v8, Lcom/google/android/apps/plus/hangout/multiwaveview/Ease$Quart;->easeOut:Landroid/animation/TimeInterpolator;

    aput-object v8, v7, v2

    const/4 v2, 0x2

    const-string v8, "delay"

    aput-object v8, v7, v2

    const/4 v2, 0x3

    const/16 v8, 0x4b0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x4

    const-string v8, "alpha"

    aput-object v8, v7, v2

    const/4 v2, 0x5

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v2

    const/4 v2, 0x6

    const-string v6, "x"

    aput-object v6, v7, v2

    const/4 v2, 0x7

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v2

    const/16 v2, 0x8

    const-string v6, "y"

    aput-object v6, v7, v2

    const/16 v2, 0x9

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v7, v2

    const/16 v2, 0xa

    const-string v6, "onUpdate"

    aput-object v6, v7, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v2, v7, v9

    const/16 v2, 0xc

    const-string v6, "onComplete"

    aput-object v6, v7, v2

    const/16 v2, 0xd

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    aput-object v6, v7, v2

    invoke-static {v1, v4, v5, v7}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    :goto_6
    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setGrabbedState(I)V

    goto/16 :goto_0

    :cond_8
    move v1, v3

    goto/16 :goto_4

    :cond_9
    move v4, v6

    goto/16 :goto_5

    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto :goto_6

    :cond_b
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v9, :cond_d

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const-wide/16 v7, 0x12c

    const/16 v1, 0xe

    new-array v5, v1, [Ljava/lang/Object;

    const-string v1, "ease"

    aput-object v1, v5, v3

    sget-object v1, Lcom/google/android/apps/plus/hangout/multiwaveview/Ease$Quart;->easeOut:Landroid/animation/TimeInterpolator;

    aput-object v1, v5, v2

    const/4 v1, 0x2

    const-string v2, "delay"

    aput-object v2, v5, v1

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x4

    const-string v2, "alpha"

    aput-object v2, v5, v1

    const/4 v1, 0x5

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x6

    const-string v2, "x"

    aput-object v2, v5, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    const/16 v1, 0x8

    const-string v2, "y"

    aput-object v2, v5, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v5, v1

    const/16 v1, 0xa

    const-string v2, "onUpdate"

    aput-object v2, v5, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v1, v5, v9

    const/16 v1, 0xc

    const-string v2, "onComplete"

    aput-object v2, v5, v1

    const/16 v2, 0xd

    iget-boolean v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListenerWithPing:Landroid/animation/Animator$AnimatorListener;

    :goto_7
    aput-object v1, v5, v2

    invoke-static {v4, v7, v8, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleAnimation:Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    goto/16 :goto_6

    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mResetListener:Landroid/animation/Animator$AnimatorListener;

    goto :goto_7

    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v2, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private trySwitchToFirstTouchState(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    sub-float v0, v2, v4

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    sub-float v1, v3, v4

    mul-float v4, v0, v0

    mul-float v6, v1, v1

    add-float/2addr v6, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v7, "accessibility"

    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v4}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x3fa66666

    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    mul-float/2addr v4, v7

    :goto_0
    mul-float/2addr v4, v4

    cmpg-float v4, v6, v4

    if-gtz v4, :cond_1

    const-string v4, "MultiWaveView"

    const-string v6, "** Handle HIT"

    invoke-static {v4, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    move v4, v5

    :goto_1
    return v4

    :cond_0
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private updateTargetPositions()V
    .locals 10

    const/4 v2, 0x0

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const-wide v6, -0x3fe6de04abbbd2e8L

    int-to-double v8, v2

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    int-to-double v8, v8

    div-double v0, v6, v8

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    add-float v4, v6, v7

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    iget v7, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    add-float v5, v6, v7

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private vibrate()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Landroid/os/Vibrator;->vibrate(J)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected getSuggestedMinimumHeight()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :cond_0
    add-int/2addr v0, v1

    return v0
.end method

.method protected getSuggestedMinimumWidth()I
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    :cond_0
    add-int/2addr v0, v1

    return v0
.end method

.method final invalidateGlobalRegion(Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;)V
    .locals 9
    .param p1    # Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v1

    new-instance v0, Landroid/graphics/RectF;

    int-to-float v4, v3

    int-to-float v5, v1

    invoke-direct {v0, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v4

    div-int/lit8 v5, v3, 0x2

    int-to-float v5, v5

    sub-float/2addr v4, v5

    invoke-virtual {p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v5

    div-int/lit8 v6, v1, 0x2

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    move-object v2, p0

    :goto_0
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    instance-of v4, v4, Landroid/view/View;

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :cond_0
    iget v4, v0, Landroid/graphics/RectF;->left:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    iget v5, v0, Landroid/graphics/RectF;->top:F

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-int v5, v5

    iget v6, v0, Landroid/graphics/RectF;->right:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    float-to-double v7, v7

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v7, v7

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v1, :cond_2

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "accessibility"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    return v2

    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    :pswitch_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->setAction(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    sub-int v3, p4, p2

    sub-int v0, p5, p3

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float v1, v4, v5

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    add-float v2, v4, v5

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    cmpl-float v4, v1, v4

    if-nez v4, :cond_0

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    cmpl-float v4, v2, v4

    if-eqz v4, :cond_5

    :cond_0
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_4

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_4

    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_1

    const/high16 v4, 0x3f000000

    mul-float v5, v1, v1

    mul-float v6, v2, v2

    add-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v5

    double-to-float v5, v5

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    :cond_1
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDrawables:Ljava/util/ArrayList;

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000

    div-float/2addr v4, v5

    iput v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    :cond_2
    iget v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    cmpl-float v4, v4, v7

    if-nez v4, :cond_3

    const/4 v4, 0x1

    const/high16 v5, 0x41a00000

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideChevrons()V

    invoke-direct {p0, v8}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->moveHandleTo$483d6f3f(FF)V

    :cond_4
    iput v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    iput v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v4, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->updateTargetPositions()V

    :cond_5
    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Outer Radius = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HitRadius = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHitRadius:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SnapMargin = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mSnapMargin:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "FeedbackCount = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "VibrationDuration = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrationDuration:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TapRadius = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTapRadius:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WaveCenterX = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "WaveCenterY = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "HorizontalOffset = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHorizontalOffset:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "MultiWaveView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "VerticalOffset = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVerticalOffset:F

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/high16 v8, 0x40000000

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getSuggestedMinimumWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->resolveMeasured(II)I

    move-result v4

    invoke-static {p2, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->resolveMeasured(II)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingLeft()I

    move-result v5

    sub-int v5, v4, v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingTop()I

    move-result v6

    sub-int v6, v3, v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    const v6, 0x3f666666

    mul-float/2addr v5, v6

    div-float/2addr v5, v8

    iput v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    iget v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    mul-float/2addr v5, v8

    float-to-int v0, v5

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    check-cast v5, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v5, v0, v0}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    new-instance v5, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getResources()Landroid/content/res/Resources;

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRingDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v5, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRing:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    invoke-virtual {p0, v4, v3}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->invalidate()V

    if-eqz v1, :cond_2

    const/4 v2, 0x1

    :goto_1
    return v2

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->trySwitchToFirstTouchState(Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->ping()V

    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDragging:Z

    if-eqz v2, :cond_1

    const-string v2, "MultiWaveView"

    const-string v3, "** Handle RELEASE"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v2, 0x4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->switchToState$48676aae(I)V

    const/4 v1, 0x1

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->handleMove(Landroid/view/MotionEvent;)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final ping()V
    .locals 14

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopChevronAnimation()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3ecccccd

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOuterRadius:F

    const v2, 0x3f666666

    mul-float/2addr v1, v2

    const/4 v2, 0x4

    new-array v3, v2, [[F

    const/4 v2, 0x0

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    sub-float/2addr v6, v0

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v4, v5

    aput-object v4, v3, v2

    const/4 v2, 0x1

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    add-float/2addr v6, v0

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v4, v5

    aput-object v4, v3, v2

    const/4 v2, 0x2

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    sub-float/2addr v6, v0

    aput v6, v4, v5

    aput-object v4, v3, v2

    const/4 v2, 0x3

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    add-float/2addr v0, v6

    aput v0, v4, v5

    aput-object v4, v3, v2

    const/4 v0, 0x4

    new-array v4, v0, [[F

    const/4 v0, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    sub-float/2addr v6, v1

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v2, v5

    aput-object v2, v4, v0

    const/4 v0, 0x1

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    add-float/2addr v6, v1

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    aput v6, v2, v5

    aput-object v2, v4, v0

    const/4 v0, 0x2

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    sub-float/2addr v6, v1

    aput v6, v2, v5

    aput-object v2, v4, v0

    const/4 v0, 0x3

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    aput v6, v2, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    add-float/2addr v1, v6

    aput v1, v2, v5

    aput-object v2, v4, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x4

    if-ge v2, v0, :cond_2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    if-ge v1, v0, :cond_1

    mul-int/lit16 v5, v1, 0xa0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronDrawables:Ljava/util/ArrayList;

    iget v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mFeedbackCount:I

    mul-int/2addr v6, v2

    add-int/2addr v6, v1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    if-eqz v0, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimations:Ljava/util/ArrayList;

    const-wide/16 v7, 0x352

    const/16 v9, 0x10

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "ease"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mChevronAnimationInterpolator:Landroid/animation/TimeInterpolator;

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string v11, "delay"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v9, v10

    const/4 v5, 0x4

    const-string v10, "x"

    aput-object v10, v9, v5

    const/4 v5, 0x5

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aget-object v12, v3, v2

    const/4 v13, 0x0

    aget v12, v12, v13

    aput v12, v10, v11

    const/4 v11, 0x1

    aget-object v12, v4, v2

    const/4 v13, 0x0

    aget v12, v12, v13

    aput v12, v10, v11

    aput-object v10, v9, v5

    const/4 v5, 0x6

    const-string v10, "y"

    aput-object v10, v9, v5

    const/4 v5, 0x7

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    aget-object v12, v3, v2

    const/4 v13, 0x1

    aget v12, v12, v13

    aput v12, v10, v11

    const/4 v11, 0x1

    aget-object v12, v4, v2

    const/4 v13, 0x1

    aget v12, v12, v13

    aput v12, v10, v11

    aput-object v10, v9, v5

    const/16 v5, 0x8

    const-string v10, "alpha"

    aput-object v10, v9, v5

    const/16 v5, 0x9

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_0

    aput-object v10, v9, v5

    const/16 v5, 0xa

    const-string v10, "scaleX"

    aput-object v10, v9, v5

    const/16 v5, 0xb

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_1

    aput-object v10, v9, v5

    const/16 v5, 0xc

    const-string v10, "scaleY"

    aput-object v10, v9, v5

    const/16 v5, 0xd

    const/4 v10, 0x2

    new-array v10, v10, [F

    fill-array-data v10, :array_2

    aput-object v10, v9, v5

    const/16 v5, 0xe

    const-string v10, "onUpdate"

    aput-object v10, v9, v5

    const/16 v5, 0xf

    iget-object v10, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    aput-object v10, v9, v5

    invoke-static {v0, v7, v8, v9}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->to(Ljava/lang/Object;J[Ljava/lang/Object;)Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_2
    return-void

    :array_0
    .array-data 4
        0x3f800000
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x3f000000
        0x40000000
    .end array-data

    :array_2
    .array-data 4
        0x3f000000
        0x40000000
    .end array-data
.end method

.method public final reset(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopChevronAnimation()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopHandleAnimation()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->stopTargetAnimation()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideChevrons()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->hideTargets(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterX:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mWaveCenterY:F

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    sget-object v1, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mHandleDrawable:Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;

    const/high16 v1, 0x3f800000

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/hangout/multiwaveview/Tweener;->reset()V

    :cond_0
    return-void
.end method

.method public setDirectionDescriptionsResourceId(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptionsResourceId:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mDirectionDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public setOnTriggerListener(Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mOnTriggerListener:Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView$OnTriggerListener;

    return-void
.end method

.method public setTargetDescriptionsResourceId(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptionsResourceId:I

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mTargetDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public setTargetResources(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mAnimatingTargets:Z

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mNewTargetResources:I

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->internalSetTargetResources(I)V

    goto :goto_0
.end method

.method public setVibrateEnabled(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/MultiWaveView;->mVibrator:Landroid/os/Vibrator;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
