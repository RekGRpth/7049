.class public Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;
.super Ljava/lang/Object;
.source "TargetDrawable.java"


# static fields
.field public static final STATE_ACTIVE:[I

.field public static final STATE_FOCUSED:[I

.field public static final STATE_INACTIVE:[I


# instance fields
.field private mAlpha:F

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mScaleX:F

.field private mScaleY:F

.field private mTranslationX:F

.field private mTranslationY:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x2

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_ACTIVE:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101009e

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_FOCUSED:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x101009e
        0x10100a2
    .end array-data

    :array_1
    .array-data 4
        0x101009e
        0x101009c
    .end array-data
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/high16 v0, 0x3f800000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationX:F

    iput v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationY:F

    iput v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleX:F

    iput v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleY:F

    iput v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mAlpha:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getScaleX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setScaleX(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getScaleY()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setScaleY(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getX()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setX(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getY()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setY(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getAlpha()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setAlpha(F)V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->STATE_INACTIVE:[I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->setState([I)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v2, -0x41000000

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationX:F

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationY:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleX:F

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleY:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mAlpha:F

    const/high16 v2, 0x437f0000

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public getAlpha()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mAlpha:F

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScaleX()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleX:F

    return v0
.end method

.method public getScaleY()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleY:F

    return v0
.end method

.method public getWidth()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationX:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationY:F

    return v0
.end method

.method public final hasState$25e2147()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v0, :cond_0

    :cond_0
    return v1
.end method

.method public final isValid()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mAlpha:F

    return-void
.end method

.method public setScaleX(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleX:F

    return-void
.end method

.method public setScaleY(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mScaleY:F

    return-void
.end method

.method public setState([I)V
    .locals 2
    .param p1    # [I

    iget-object v1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v1, v1, Landroid/graphics/drawable/StateListDrawable;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    invoke-virtual {v0}, Landroid/graphics/drawable/StateListDrawable;->invalidateSelf()V

    :cond_0
    return-void
.end method

.method public setX(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationX:F

    return-void
.end method

.method public setY(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/hangout/multiwaveview/TargetDrawable;->mTranslationY:F

    return-void
.end method
