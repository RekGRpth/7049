.class final Lcom/google/android/apps/plus/iu/FingerprintHelper;
.super Ljava/lang/Object;
.source "FingerprintHelper.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/plus/iu/FingerprintHelper;


# instance fields
.field private final mCachedFingerprintUri:Landroid/net/Uri;

.field private final mFingerprintUri:Landroid/net/Uri;

.field private final mRecalculateFingerprintUri:Landroid/net/Uri;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mResolver:Landroid/content/ContentResolver;

    invoke-static {p1}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getFingerprintUri()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mFingerprintUri:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/picasastore/PicasaStoreFacade;->getFingerprintUri(ZZ)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mCachedFingerprintUri:Landroid/net/Uri;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getFingerprintUri(ZZ)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mRecalculateFingerprintUri:Landroid/net/Uri;

    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/FingerprintHelper;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/FingerprintHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->sInstance:Lcom/google/android/apps/plus/iu/FingerprintHelper;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/FingerprintHelper;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/FingerprintHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->sInstance:Lcom/google/android/apps/plus/iu/FingerprintHelper;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->sInstance:Lcom/google/android/apps/plus/iu/FingerprintHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getFingerprint(Landroid/net/Uri;Ljava/lang/String;)Lcom/android/gallery3d/common/Fingerprint;
    .locals 8
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mResolver:Landroid/content/ContentResolver;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    aput-object p2, v2, v4

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/gallery3d/common/Fingerprint;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/gallery3d/common/Fingerprint;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v3, v0

    :goto_0
    return-object v3

    :cond_0
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_1
    const-string v0, "FingerprintHelper"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FingerprintHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot get fingerprint for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method


# virtual methods
.method public final getCachedFingerprint(Ljava/lang/String;)Lcom/android/gallery3d/common/Fingerprint;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mCachedFingerprintUri:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->getFingerprint(Landroid/net/Uri;Ljava/lang/String;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized getFingerprint(Ljava/lang/String;Z)Lcom/android/gallery3d/common/Fingerprint;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mRecalculateFingerprintUri:Landroid/net/Uri;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->getFingerprint(Landroid/net/Uri;Ljava/lang/String;)Lcom/android/gallery3d/common/Fingerprint;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final invalidate(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/FingerprintHelper;->mFingerprintUri:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method
