.class final Lcom/google/android/apps/plus/iu/GDataUploader;
.super Ljava/lang/Object;
.source "GDataUploader.java"

# interfaces
.implements Lcom/google/android/apps/plus/iu/Uploader;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;,
        Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;
    }
.end annotation


# static fields
.field private static final RE_RANGE_HEADER:Ljava/util/regex/Pattern;


# instance fields
.field private mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

.field private mContext:Landroid/content/Context;

.field private mHttpClient:Lorg/apache/http/client/HttpClient;

.field private mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

.field private mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

.field private final mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "bytes=(\\d+)-(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/GDataUploader;->RE_RANGE_HEADER:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/apps/plus/network/UserAgent;->from(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/iu/HttpUtils;->createHttpClient(Landroid/content/Context;Ljava/lang/String;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    new-instance v0, Lcom/google/android/apps/plus/iu/Authorizer;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/iu/Authorizer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    return-void
.end method

.method private static consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V
    .locals 2
    .param p0    # Lorg/apache/http/HttpEntity;

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->isStreaming()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    .locals 9
    .param p1    # Lorg/apache/http/client/methods/HttpUriRequest;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    const/4 v8, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementNetworkOpDuration(J)V

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0x191

    if-eq v4, v5, :cond_0

    const/16 v5, 0x193

    if-ne v4, v5, :cond_6

    :cond_0
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    const-string v6, "lh2"

    invoke-virtual {v5, p2, v6, p3}, Lcom/google/android/apps/plus/iu/Authorizer;->getFreshAuthToken(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    if-nez p3, :cond_4

    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    const-string v6, "null auth token"

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    :catch_0
    move-exception v0

    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "iu.UploadsManager"

    const-string v6, "authentication canceled"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :catch_1
    move-exception v0

    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "iu.UploadsManager"

    const-string v6, "authentication failed"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    throw v0

    :catch_2
    move-exception v0

    const-string v5, "iu.UploadsManager"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    :cond_4
    const-string v5, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "GoogleLogin auth="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v5, v6}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "iu.UploadsManager"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "iu.UploadsManager"

    const-string v6, "executeWithAuthRetry: attempt #2"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v5, p1}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long/2addr v5, v2

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementNetworkOpDuration(J)V

    :cond_6
    return-object v1
.end method

.method private getAuthToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    const/4 v3, 0x4

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    const-string v2, "lh2"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/apps/plus/iu/Authorizer;->getAuthToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "iu.UploadsManager"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "iu.UploadsManager"

    const-string v2, "authentication canceled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    const-string v1, "iu.UploadsManager"

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "iu.UploadsManager"

    const-string v2, "authentication failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    throw v0

    :catch_2
    move-exception v0

    const-string v1, "iu.UploadsManager"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "iu.UploadsManager"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    new-instance v1, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-direct {v1, v0}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;
    .locals 5
    .param p0    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lorg/apache/http/entity/BufferedHttpEntity;

    invoke-interface {p0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/entity/BufferedHttpEntity;-><init>(Lorg/apache/http/HttpEntity;)V

    invoke-virtual {v0}, Lorg/apache/http/entity/BufferedHttpEntity;->getContentLength()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private getMediaRecordEntry(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMediaRecordId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "could not find the media record for the uploaded task; "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-wide v1, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoId:J

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadId(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->photoUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v1

    iget-wide v2, p2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->timestamp:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadTime(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setBytesUploaded(J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v1

    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    return-object v0
.end method

.method private static isIncompeteStatusCode(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x134

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isSuccessStatusCode(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0xc8

    if-eq p0, v0, :cond_0

    const/16 v0, 0xc9

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static parseHeaders(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 9
    .param p0    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v7, "\r\n"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move-object v0, v5

    array-length v3, v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    const-string v7, ":"

    invoke-virtual {v4, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    const/4 v7, 0x0

    aget-object v7, v6, v7

    const/4 v8, 0x1

    aget-object v8, v6, v8

    invoke-virtual {v1, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private static parseQuotaResponse(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;
    .locals 4
    .param p0    # Lorg/apache/http/HttpEntity;

    if-nez p0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;-><init>(B)V

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v1, v2, v0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    :cond_1
    :goto_1
    iget-object v2, v0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    goto :goto_0

    :catch_0
    move-exception v2

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    :catch_3
    move-exception v2

    goto :goto_1

    :catch_4
    move-exception v2

    if-eqz v1, :cond_1

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_1

    :catch_5
    move-exception v2

    goto :goto_1

    :catchall_0
    move-exception v2

    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7

    :cond_2
    :goto_2
    throw v2

    :catch_6
    move-exception v2

    goto :goto_1

    :catch_7
    move-exception v3

    goto :goto_2
.end method

.method private static parseRangeHeaderEndByte(Ljava/lang/String;)J
    .locals 5
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/iu/GDataUploader;->RE_RANGE_HEADER:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    :goto_0
    return-wide v1

    :cond_0
    const-wide/16 v1, -0x1

    goto :goto_0
.end method

.method private static parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    .locals 4
    .param p0    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;
        }
    .end annotation

    if-nez p0, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v3, "null HttpEntity in response"

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;-><init>(B)V

    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    :try_start_0
    sget-object v2, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    invoke-static {v1, v2, v0}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->validateResult()V

    return-object v0

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v2
.end method

.method private resetUpload()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    return-void
.end method

.method private resume(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 11
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v8

    new-instance v6, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v6, v8}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    const-string v8, "Content-Range"

    const-string v9, "bytes */*"

    invoke-virtual {v6, v8, v9}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v6, p2, p3}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V

    iget-object v8, v2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    iget-object v9, v2, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    invoke-static {v8, p2, v9}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateQuotaSettings(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;)V

    :cond_0
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "iu.UploadsManager"

    const-string v9, "nothing to resume; upload already complete"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    iget-object v9, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-direct {p0, v8, v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->getMediaRecordEntry(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    :goto_0
    return-object v8

    :cond_2
    :try_start_1
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->isIncompeteStatusCode(I)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "range"

    invoke-interface {v7, v8}, Lorg/apache/http/HttpResponse;->containsHeader(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "range"

    invoke-interface {v7, v8}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseRangeHeaderEndByte(Ljava/lang/String;)J

    move-result-wide v3

    const-wide/16 v8, 0x0

    cmp-long v8, v3, v8

    if-gez v8, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/GDataUploader;->resetUpload()V

    new-instance v8, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "negative range offset: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v8

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    throw v8

    :cond_3
    :try_start_2
    invoke-virtual {p1, v3, v4}, Ljava/io/InputStream;->skip(J)J

    const/high16 v8, 0x40000

    invoke-virtual {p1, v8}, Ljava/io/InputStream;->mark(I)V

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v8, v3, v4}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/iu/GDataUploader;->uploadChunks(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v8

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    :cond_4
    const/16 v8, 0x191

    if-ne v0, v8, :cond_5

    :try_start_3
    new-instance v8, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/GDataUploader;->resetUpload()V

    new-instance v8, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "unexpected resume response: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method private start(Ljava/io/InputStream;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 10
    .param p1    # Ljava/io/InputStream;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;
        }
    .end annotation

    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    const-string v6, "\r\n\r\n"

    invoke-virtual {p3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    if-lez v7, :cond_0

    const/4 v5, 0x0

    invoke-virtual {p3, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    move-object p3, v6

    :goto_0
    invoke-static {p3}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseHeaders(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    move-object v7, v5

    goto :goto_0

    :cond_1
    if-eqz v7, :cond_2

    new-instance v5, Lorg/apache/http/entity/StringEntity;

    invoke-direct {v5, v7}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    invoke-direct {p0, v3, p4, p5}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v5

    if-eqz v5, :cond_4

    if-eqz v1, :cond_3

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/GDataUploader;->throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V

    :cond_3
    const-string v5, "Location"

    invoke-interface {v4, v5}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-interface {v2}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    const-wide/16 v6, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    invoke-direct {p0, p1, p4, p5}, Lcom/google/android/apps/plus/iu/GDataUploader;->uploadChunks(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    return-object v5

    :cond_4
    const/16 v5, 0x190

    if-ne v0, v5, :cond_5

    :try_start_1
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "upload failed (bad payload, file too large) "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v5

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    throw v5

    :cond_5
    const/16 v5, 0x191

    if-ne v0, v5, :cond_6

    :try_start_2
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_6
    const/16 v5, 0x1f4

    if-lt v0, v5, :cond_7

    const/16 v5, 0x258

    if-ge v0, v5, :cond_7

    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "upload transient error:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_7
    new-instance v5, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private static throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;
        }
    .end annotation

    if-eqz p0, :cond_0

    const-string v0, "LimitQuota"

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->errorCode:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private uploadChunks(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 32
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;,
            Lorg/xml/sax/SAXException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;
        }
    .end annotation

    const/high16 v24, 0x40000

    move/from16 v0, v24

    new-array v6, v0, [B

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v26

    cmp-long v24, v24, v26

    if-gez v24, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-interface/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;->onProgress(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->isUploading()Z

    move-result v24

    if-nez v24, :cond_1

    const/16 v24, 0x0

    :goto_1
    return-object v24

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesUploaded()J

    move-result-wide v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v24

    sub-long v24, v24, v17

    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v16, v0

    const/high16 v24, 0x40000

    move/from16 v0, v16

    move/from16 v1, v24

    if-gt v0, v1, :cond_3

    const/4 v15, 0x1

    :goto_2
    if-nez v15, :cond_2

    const/high16 v16, 0x40000

    :cond_2
    const/high16 v24, 0x40000

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->mark(I)V

    const/16 v20, 0x0

    :goto_3
    move/from16 v0, v20

    move/from16 v1, v16

    if-ge v0, v1, :cond_4

    add-int/lit8 v24, v20, 0x0

    sub-int v25, v16, v20

    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v24

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_4

    add-int v20, v20, v24

    goto :goto_3

    :cond_3
    const/4 v15, 0x0

    goto :goto_2

    :cond_4
    if-nez v15, :cond_5

    const/high16 v24, 0x40000

    move/from16 v0, v20

    move/from16 v1, v24

    if-eq v0, v1, :cond_7

    :cond_5
    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v7, v0, [J

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-static {v0, v7}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getFingerprint()Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v12, v0}, Lcom/android/gallery3d/common/Fingerprint;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getContentUri()Landroid/net/Uri;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setFingerprint(Lcom/android/gallery3d/common/Fingerprint;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setUploadUrl(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    const-wide/16 v25, 0x0

    invoke-virtual/range {v24 .. v26}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aget-wide v25, v7, v25

    invoke-virtual/range {v24 .. v26}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesTotal(J)V

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v24, v0

    if-eqz v24, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-interface/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;->onFileChanged(Lcom/google/android/apps/plus/iu/UploadTaskEntry;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_6
    :goto_4
    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "UPLOAD_SIZE_DATA_MISMATCH: fingerprint changed; uri="

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ",uploadUrl="

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;-><init>(Ljava/lang/String;)V

    throw v24

    :catch_0
    move-exception v10

    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;

    move-object/from16 v0, v24

    invoke-direct {v0, v10}, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;-><init>(Ljava/lang/Throwable;)V

    throw v24

    :catch_1
    move-exception v14

    const-string v24, "iu.UploadsManager"

    const/16 v25, 0x3

    invoke-static/range {v24 .. v25}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v24

    if-eqz v24, :cond_6

    const-string v24, "iu.UploadsManager"

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "error calling back on file change:"

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v25

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_7
    const-string v24, "iu.UploadsManager"

    const/16 v25, 0x4

    invoke-static/range {v24 .. v25}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v24

    if-eqz v24, :cond_8

    const-string v24, "iu.UploadsManager"

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "--- UPLOAD task: "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v26, v0

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getMimeType()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v26

    new-instance v21, Lorg/apache/http/client/methods/HttpPut;

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v28, v0

    add-long v28, v28, v17

    const-wide/16 v30, 0x1

    sub-long v28, v28, v30

    const-string v24, "Content-Range"

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "bytes "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "-"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "/"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v24, "Content-Type"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpPut;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v24, Ljava/io/ByteArrayInputStream;

    const/16 v25, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v16

    invoke-direct {v0, v6, v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    new-instance v25, Lorg/apache/http/entity/InputStreamEntity;

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v26, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/http/entity/InputStreamEntity;-><init>(Ljava/io/InputStream;J)V

    const/16 v24, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/InputStreamEntity;->setContentType(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    const/4 v11, 0x0

    :try_start_2
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v11

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v8

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v24

    if-eqz v24, :cond_b

    invoke-static {v11}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseResult(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/apps/plus/iu/GDataUploader;->throwIfQuotaError(Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)V

    iget-object v0, v13, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    move-object/from16 v24, v0

    if-eqz v24, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    move-object/from16 v24, v0

    iget-object v0, v13, Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;->iuStats:Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    move-object/from16 v1, p2

    move-object/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/iu/InstantUploadProvider;->updateQuotaSettings(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;)V

    :cond_9
    const-string v24, "iu.UploadsManager"

    const/16 v25, 0x3

    invoke-static/range {v24 .. v25}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v24

    if-eqz v24, :cond_a

    const-string v24, "iu.UploadsManager"

    const-string v25, "UPLOAD_SUCCESS"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v25

    invoke-virtual/range {v24 .. v26}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V

    const-wide/16 v24, 0x1

    invoke-static/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/MetricsUtils;->incrementNetworkOpCount(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v1, v13}, Lcom/google/android/apps/plus/iu/GDataUploader;->getMediaRecordEntry(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/GDataUploader$GDataResponse;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v24

    invoke-static {v11}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_1

    :cond_b
    :try_start_3
    invoke-static {v8}, Lcom/google/android/apps/plus/iu/GDataUploader;->isIncompeteStatusCode(I)Z

    move-result v24

    if-eqz v24, :cond_f

    const-string v24, "range"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v19

    if-eqz v19, :cond_c

    invoke-interface/range {v19 .. v19}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseRangeHeaderEndByte(Ljava/lang/String;)J

    move-result-wide v4

    :goto_5
    const-wide/16 v24, 0x0

    cmp-long v24, v4, v24

    if-gez v24, :cond_d

    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v25, "malformed or missing range header for subsequent upload"

    invoke-direct/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v24
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v24

    invoke-static {v11}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    throw v24

    :cond_c
    const-wide/16 v4, -0x1

    goto :goto_5

    :cond_d
    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v24, v0

    add-long v24, v24, v17

    cmp-long v24, v4, v24

    if-gez v24, :cond_e

    :try_start_4
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->reset()V

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Ljava/io/InputStream;->skip(J)J

    :cond_e
    move-wide/from16 v17, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setBytesUploaded(J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    invoke-static {v11}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    goto/16 :goto_0

    :cond_f
    const/16 v24, 0x190

    move/from16 v0, v24

    if-ne v8, v0, :cond_10

    :try_start_5
    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "upload failed (bad payload, file too large) "

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v24

    :cond_10
    const/16 v24, 0x1f4

    move/from16 v0, v24

    if-lt v8, v0, :cond_11

    const/16 v24, 0x258

    move/from16 v0, v24

    if-ge v8, v0, :cond_11

    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$RestartException;

    new-instance v25, Ljava/lang/StringBuilder;

    const-string v26, "upload transient error"

    invoke-direct/range {v25 .. v26}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$RestartException;-><init>(Ljava/lang/String;)V

    throw v24

    :cond_11
    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    invoke-interface/range {v22 .. v22}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v24
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_12
    new-instance v24, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v25, "upload is done but no server confirmation"

    invoke-direct/range {v24 .. v25}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;)V

    throw v24
.end method


# virtual methods
.method public final close()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mHttpClient:Lorg/apache/http/client/HttpClient;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mAuthorizer:Lcom/google/android/apps/plus/iu/Authorizer;

    return-void
.end method

.method final getQuota(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v8, 0x5

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/GDataUploader;->getAuthToken(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    new-instance v3, Lorg/apache/http/client/methods/HttpGet;

    const-string v7, "https://picasaweb.google.com/data/quotainfo"

    invoke-direct {v3, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    const-string v7, "User-Agent"

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/apps/plus/network/UserAgent;->from(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "GData-Version"

    const-string v8, "3.0"

    invoke-interface {v3, v7, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_0

    const-string v7, "Authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "GoogleLogin auth="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 v2, 0x0

    :try_start_1
    invoke-direct {p0, v3, p1, v0}, Lcom/google/android/apps/plus/iu/GDataUploader;->executeWithAuthRetry(Lorg/apache/http/client/methods/HttpUriRequest;Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/GDataUploader;->getEntity(Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpEntity;

    move-result-object v2

    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    invoke-static {v7}, Lcom/google/android/apps/plus/iu/GDataUploader;->isSuccessStatusCode(I)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->parseQuotaResponse(Lorg/apache/http/HttpEntity;)Lcom/google/android/apps/plus/iu/GDataUploader$GDataQuota;
    :try_end_1
    .catch Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    :cond_1
    :goto_0
    return-object v6

    :catch_0
    move-exception v1

    const-string v7, "iu.UploadsManager"

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "iu.UploadsManager"

    const-string v8, "Couldn\'t get auth token"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v1

    const-string v7, "iu.UploadsManager"

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "iu.UploadsManager"

    const-string v8, "Couldn\'t get auth token"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    :try_start_2
    const-string v7, "iu.UploadsManager"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    const-string v7, "iu.UploadsManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Couldn\'t get quota info "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    :catch_2
    move-exception v1

    :try_start_3
    const-string v7, "iu.UploadsManager"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "iu.UploadsManager"

    const-string v8, "Unauthorized to get quota"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_4
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    :catch_3
    move-exception v1

    :try_start_4
    const-string v7, "iu.UploadsManager"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "iu.UploadsManager"

    const-string v8, "IOException getting quota"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_5
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    goto :goto_0

    :catchall_0
    move-exception v6

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/GDataUploader;->consumeEntityQuietly(Lorg/apache/http/HttpEntity;)V

    throw v6
.end method

.method public final upload(Lcom/google/android/apps/plus/iu/UploadTaskEntry;Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;Z)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 15
    .param p1    # Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .param p2    # Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;
    .param p3    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/apps/plus/iu/Uploader$UploadException;,
            Lcom/google/android/apps/plus/iu/Uploader$RestartException;,
            Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileChangedException;,
            Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException;,
            Lcom/google/android/apps/plus/iu/Uploader$UnauthorizedException;,
            Lcom/google/android/apps/plus/iu/Uploader$PicasaQuotaException;
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getBytesTotal()J

    move-result-wide v11

    const-wide/16 v13, 0x0

    cmp-long v1, v11, v13

    if-gtz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException;

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v5, "Zero length file can\'t be uploaded"

    invoke-direct {v3, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/iu/Uploader$MediaFileUnavailableException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mListener:Lcom/google/android/apps/plus/iu/Uploader$UploadProgressListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/iu/GDataUploader;->getAuthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getRequestTemplate()Ljava/lang/String;

    move-result-object v4

    if-nez v6, :cond_3

    const-string v1, "Authorization: GoogleLogin auth=%=_auth_token_=%\r\n"

    const-string v3, ""

    invoke-virtual {v4, v1, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    const/4 v9, 0x0

    :try_start_0
    const-string v1, "iu.UploadsManager"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v3, "iu.UploadsManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p3, :cond_4

    const-string v1, ""

    :goto_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "upload full size; task: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-nez p3, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/iu/UploadUtils;->shouldResizeImage(Landroid/content/Context;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v10, 0x1

    :goto_2
    if-eqz v10, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/iu/UploadUtils;->downSampleImage(Landroid/content/Context;Lcom/google/android/apps/plus/iu/UploadTaskEntry;)Ljava/lang/String;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v8

    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUploadUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getUrl()Landroid/net/Uri;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/iu/GDataUploader;->start(Ljava/io/InputStream;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    :goto_3
    return-object v1

    :cond_3
    const-string v1, "%=_auth_token_=%"

    invoke-virtual {v4, v1, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_4
    :try_start_2
    const-string v1, "Don\'t "
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :cond_5
    const/4 v10, 0x0

    goto :goto_2

    :cond_6
    :try_start_3
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/GDataUploader;->mUploadTask:Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->getAccount()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v1, v6}, Lcom/google/android/apps/plus/iu/GDataUploader;->resume(Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_3
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_3

    :catch_0
    move-exception v7

    move-object v2, v9

    :goto_4
    :try_start_4
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v7}, Lorg/apache/http/client/ClientProtocolException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v1

    :goto_5
    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v1

    :catch_1
    move-exception v7

    move-object v2, v9

    :goto_6
    :try_start_5
    new-instance v1, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;

    invoke-direct {v1, v7}, Lcom/google/android/apps/plus/iu/Uploader$LocalIoException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v7

    move-object v2, v9

    :goto_7
    const-string v1, "iu.UploadsManager"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "iu.UploadsManager"

    const-string v3, "error in parsing response"

    invoke-static {v1, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_7
    new-instance v1, Lcom/google/android/apps/plus/iu/Uploader$UploadException;

    const-string v3, "error in parsing response"

    invoke-direct {v1, v3, v7}, Lcom/google/android/apps/plus/iu/Uploader$UploadException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :catchall_1
    move-exception v1

    move-object v2, v9

    goto :goto_5

    :catch_3
    move-exception v7

    goto :goto_7

    :catch_4
    move-exception v7

    goto :goto_6

    :catch_5
    move-exception v7

    goto :goto_4
.end method
