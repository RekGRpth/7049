.class public final Lcom/google/android/apps/plus/iu/InstantUploadFacade;
.super Ljava/lang/Object;
.source "InstantUploadFacade.java"


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field public static final INSTANT_UPLOAD_URI:Landroid/net/Uri;

.field public static final MEDIA_URI:Landroid/net/Uri;

.field public static final PHOTOS_URI:Landroid/net/Uri;

.field public static final SETTINGS_URI:Landroid/net/Uri;

.field public static final UPLOADS_URI:Landroid/net/Uri;

.field public static final UPLOAD_ALL_URI:Landroid/net/Uri;

.field private static sNetworkReceiver:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "content://com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->BASE_URI:Landroid/net/Uri;

    const-string v1, "uploads"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->BASE_URI:Landroid/net/Uri;

    const-string v1, "upload_all"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOAD_ALL_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->BASE_URI:Landroid/net/Uri;

    const-string v1, "iu"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->INSTANT_UPLOAD_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->BASE_URI:Landroid/net/Uri;

    const-string v1, "settings"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->BASE_URI:Landroid/net/Uri;

    const-string v1, "photos"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->PHOTOS_URI:Landroid/net/Uri;

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->BASE_URI:Landroid/net/Uri;

    const-string v1, "media"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->MEDIA_URI:Landroid/net/Uri;

    return-void
.end method

.method public static broadcastOperationReport(Landroid/content/Context;Ljava/lang/String;JJIJJ)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .param p4    # J
    .param p6    # I
    .param p7    # J
    .param p9    # J

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->sNetworkReceiver:Ljava/lang/Class;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->sNetworkReceiver:Ljava/lang/Class;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.plus.iu.op_report"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "op_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "total_time"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "net_duration"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "transaction_count"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "sent_bytes"

    invoke-virtual {v0, v1, p7, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "received_bytes"

    invoke-virtual {v0, v1, p9, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static getUploadUri(J)Landroid/net/Uri;
    .locals 2
    .param p0    # J

    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static isOutOfQuota(II)Z
    .locals 2
    .param p0    # I
    .param p1    # I

    sub-int v0, p0, p1

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static requestUploadSync(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V

    return-void
.end method

.method public static setNetworkReceiver(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    sput-object p0, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->sNetworkReceiver:Ljava/lang/Class;

    return-void
.end method

.method public static stateToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const-string v0, "unknown"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "SYNC_STATE_IDLE"

    goto :goto_0

    :pswitch_1
    const-string v0, "SYNC_STATE_SYNCING"

    goto :goto_0

    :pswitch_2
    const-string v0, "SYNC_STATE_REJECT_ON_WIFI"

    goto :goto_0

    :pswitch_3
    const-string v0, "SYNC_STATE_REJECT_ON_ROAMING"

    goto :goto_0

    :pswitch_4
    const-string v0, "SYNC_STATE_REJECT_ON_POWER"

    goto :goto_0

    :pswitch_5
    const-string v0, "SYNC_STATE_REJECT_ON_USER_AUTH"

    goto :goto_0

    :pswitch_6
    const-string v0, "SYNC_STATE_REJECT_ON_AUTO_SYNC"

    goto :goto_0

    :pswitch_7
    const-string v0, "SYNC_STATE_REJECT_ON_DISABLED_DOWNSYNC"

    goto :goto_0

    :pswitch_8
    const-string v0, "SYNC_STATE_REJECT_ON_BACKGROUND_DATA"

    goto :goto_0

    :pswitch_9
    const-string v0, "SYNC_STATE_STOP_ON_QUOTA_REACHED"

    goto :goto_0

    :pswitch_a
    const-string v0, "SYNC_STATE_STOP_ON_USER_AUTH"

    goto :goto_0

    :pswitch_b
    const-string v0, "SYNC_STATE_WAIT_ON_SDCARD"

    goto :goto_0

    :pswitch_c
    const-string v0, "SYNC_STATE_STOP_ON_SDCARD"

    goto :goto_0

    :pswitch_d
    const-string v0, "SYNC_STATE_YIELD"

    goto :goto_0

    :pswitch_e
    const-string v0, "SYNC_STATE_STOP_ON_NETWORK"

    goto :goto_0

    :pswitch_f
    const-string v0, "SYNC_STATE_STOP_ON_IOE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
