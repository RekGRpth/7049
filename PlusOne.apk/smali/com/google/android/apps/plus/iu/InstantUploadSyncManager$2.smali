.class final Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;
.super Landroid/os/Handler;
.source "InstantUploadSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Landroid/os/Looper;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unknown message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    new-instance v2, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    # getter for: Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$200(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProvider:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;
    invoke-static {v1, v2}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$102(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;)Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

    :goto_0
    return-void

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$300(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$400(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/Boolean;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    # invokes: Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$500(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V

    goto :goto_0

    :pswitch_4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    # getter for: Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    invoke-static {v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$600(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->syncAccounts()V

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-static {v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$700(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "iu.SyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MSG_UPDATE_PICASA_ACCOUNTS: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method
