.class final Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;
.super Ljava/lang/Object;
.source "InstantUploadSyncManager.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetNextSyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/android/apps/plus/iu/SyncTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;->mAccountName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final bridge synthetic call()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    # getter for: Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$000(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;->this$0:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;->mAccountName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->access$800(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/String;Z)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v0

    return-object v0
.end method
