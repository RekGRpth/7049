.class final Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;
.super Ljava/lang/Object;
.source "InstantUploadSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "InstantUploadSyncSession"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field private mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

.field private mSyncCancelled:Z

.field public final result:Landroid/content/SyncResult;


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/content/SyncResult;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/SyncResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->account:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->result:Landroid/content/SyncResult;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/content/SyncResult;B)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/content/SyncResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;-><init>(Ljava/lang/String;Landroid/content/SyncResult;)V

    return-void
.end method


# virtual methods
.method public final declared-synchronized cancelSync()V
    .locals 2

    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mSyncCancelled:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/SyncTask;->cancelSync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final declared-synchronized getCurrentTask()Lcom/google/android/apps/plus/iu/SyncTask;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized isSyncCancelled()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mSyncCancelled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized setCurrentTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/iu/SyncTask;

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mSyncCancelled:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->mCurrentTask:Lcom/google/android/apps/plus/iu/SyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
