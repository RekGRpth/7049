.class public final Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
.super Ljava/lang/Object;
.source "InstantUploadSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;,
        Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;
    }
.end annotation


# static fields
.field private static final PROJECTION_ENABLE_ACCOUNT:[Ljava/lang/String;

.field private static final UPLOAD_TASK_CLEANUP_DELETE_WHERE:Ljava/lang/String;

.field private static sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;


# instance fields
.field private mBackgroundData:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

.field private mHasWifiConnectivity:Z

.field private final mInvalidAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsPlugged:Z

.field private mIsRoaming:Z

.field private mProvider:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

.field private final mSyncHandler:Landroid/os/Handler;

.field private final mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto_upload_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "auto_upload_account_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "instant_share_eventid"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "instant_share_starttime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "instant_share_endtime"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->PROJECTION_ENABLE_ACCOUNT:[Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "media_record_id NOT IN ( SELECT _id FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v1}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == ? AND ( "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(upload_state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / 100) * 100 == "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "upload_state / 100) * 100"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " == 200"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->UPLOAD_TASK_CLEANUP_DELETE_WHERE:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    iput-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "picasa-sync-manager"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$2;-><init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$1;-><init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    return-void
.end method

.method private acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/iu/SyncTask;

    const/4 v6, 0x6

    const/4 v1, 0x0

    const/4 v5, 0x3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isBackgroundSync()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because master auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    :goto_0
    return v1

    :cond_1
    new-instance v2, Landroid/accounts/Account;

    iget-object v3, p1, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v4, "com.google"

    invoke-direct {v2, v3, v4}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v2, v3}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p1, v6}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iget-object v4, p1, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "iu.SyncManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "iu.SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "reject "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for invalid account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_5
    monitor-exit v2

    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    if-nez v2, :cond_7

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isBackgroundSync()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for disabled background data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_7
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    if-nez v2, :cond_9

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isSyncOnBattery()Z

    move-result v2

    if-nez v2, :cond_9

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on battery"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_9
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    if-nez v2, :cond_d

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isSyncOnWifiOnly()Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for non-wifi connection"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_b
    iget-boolean v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    if-eqz v2, :cond_d

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/SyncTask;->isSyncOnRoaming()Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    invoke-virtual {p1, v5}, Lcom/google/android/apps/plus/iu/SyncTask;->onRejected(I)V

    goto/16 :goto_0

    :cond_d
    const/4 v1, 0x1

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;)Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    .param p1    # Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProvider:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .locals 8
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    const/4 v7, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    const-string v1, "iu.SyncManager"

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "iu.SyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "active network: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_5

    move v1, v2

    :goto_1
    iget-boolean v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    if-eq v1, v5, :cond_7

    iput-boolean v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mHasWifiConnectivity:Z

    move v1, v2

    :goto_2
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    :cond_1
    iget-boolean v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    if-eq v3, v4, :cond_2

    iput-boolean v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsRoaming:Z

    move v1, v2

    :cond_2
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    const-string v3, "iu.SyncManager"

    invoke-static {v3, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "iu.SyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "background data: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    if-eq v3, v0, :cond_6

    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mBackgroundData:Z

    :goto_3
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->enqueueMediaRecords(Landroid/content/Context;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V

    :cond_4
    return-void

    :pswitch_1
    move v1, v3

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1

    :cond_6
    move v2, v1

    goto :goto_3

    :cond_7
    move v1, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/Boolean;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    .param p1    # Ljava/lang/Boolean;

    const/4 v4, 0x0

    const/4 v3, 0x5

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    if-nez p1, :cond_3

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "iu.SyncManager"

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "there is no battery info yet"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "plugged"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v0, :cond_2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    :cond_2
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    :cond_3
    const-string v0, "iu.SyncManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "iu.SyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "battery info: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mIsPlugged:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    const/16 v1, 0xa

    invoke-static {v0, v4, v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->enqueueMediaRecords(Landroid/content/Context;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasksInternal()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)Lcom/google/android/apps/plus/iu/PicasaSyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;)V
    .locals 12
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    const/4 v8, 0x6

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->PROJECTION_ENABLE_ACCOUNT:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, "iu.SyncManager"

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncManager"

    const-string v1, "failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "iu.SyncManager"

    const-string v1, "failed to query system settings"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_7

    move v1, v6

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/4 v0, 0x2

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x3

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    const/4 v5, 0x4

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    if-eqz v0, :cond_8

    cmp-long v0, v3, v8

    if-ltz v0, :cond_8

    cmp-long v0, v3, v10

    if-gtz v0, :cond_8

    move v0, v6

    :goto_2
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_4

    if-eqz v0, :cond_6

    :cond_4
    if-eqz v3, :cond_6

    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, v3, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "iu.SyncManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "iu.SyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "remove sync account: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->SETTINGS_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_7
    move v1, v7

    goto :goto_1

    :cond_8
    move v0, v7

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/String;Z)Lcom/google/android/apps/plus/iu/SyncTask;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->nextSyncTaskInternal(Ljava/lang/String;Z)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static createSession(Ljava/lang/String;Landroid/content/SyncResult;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/content/SyncResult;

    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;-><init>(Ljava/lang/String;Landroid/content/SyncResult;B)V

    return-object v0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->sInstance:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getNextSyncTask(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;
    .locals 5
    .param p1    # Ljava/lang/String;

    const-string v3, "InstantUploadSyncManager.getNextSyncTask"

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v2

    new-instance v1, Ljava/util/concurrent/FutureTask;

    new-instance v3, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;

    invoke-direct {v3, p0, p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$GetNextSyncTask;-><init>(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;Ljava/lang/String;)V

    invoke-direct {v1, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/iu/SyncTask;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    :try_start_1
    const-string v3, "iu.SyncManager"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "iu.SyncManager"

    const-string v4, "fail to get next task"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    const/4 v3, 0x0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/MetricsUtils;->end(I)V

    throw v3
.end method

.method private isValidAccount(Landroid/accounts/Account;)Z
    .locals 7
    .param p1    # Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v5, "com.google"

    invoke-virtual {v3, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_1
    return v5

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private nextSyncTaskInternal(Ljava/lang/String;Z)Lcom/google/android/apps/plus/iu/SyncTask;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v1, 0x0

    const/4 v3, 0x4

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProvider:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->getNextTask(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "iu.SyncManager"

    const-string v3, "NEXT; no task"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    if-eqz p2, :cond_4

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NEXT; not accepted; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/SyncTask;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v0, v1

    goto :goto_0

    :cond_4
    if-eqz p1, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v2, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "iu.SyncManager"

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "iu.SyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "NEXT; wrong account; task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/SyncTask;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", account: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method private updateTasksInternal()V
    .locals 12

    const/4 v11, 0x1

    const/4 v10, 0x4

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    :goto_0
    if-nez v4, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v6, 0x0

    invoke-direct {p0, v6, v11}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->nextSyncTaskInternal(Ljava/lang/String;Z)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v0, Landroid/accounts/Account;

    iget-object v6, v5, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v7, "com.google"

    invoke-direct {v0, v6, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_3

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v6, "ignore_settings"

    invoke-virtual {v2, v6, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "iu.SyncManager"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "iu.SyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "request sync for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v6, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v6, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_3
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v7, "iu.SyncManager"

    const/4 v8, 0x5

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "iu.SyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "account: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has been removed ?!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v7

    :try_start_0
    iget-object v8, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v8, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_5
    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->getCurrentTask()Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v1

    if-nez v1, :cond_6

    const-string v6, "iu.SyncManager"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "iu.SyncManager"

    const-string v7, "stop session; no current task"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    move-result v6

    if-nez v6, :cond_8

    const-string v6, "iu.SyncManager"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_7

    const-string v6, "iu.SyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cancel task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; task rejected"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/SyncTask;->cancelSync()V

    goto/16 :goto_1

    :cond_8
    iget-object v6, v4, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->account:Ljava/lang/String;

    invoke-direct {p0, v6, v11}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->nextSyncTaskInternal(Ljava/lang/String;Z)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v6, v3, Lcom/google/android/apps/plus/iu/SyncTask;->mPriority:I

    iget v7, v1, Lcom/google/android/apps/plus/iu/SyncTask;->mPriority:I

    if-ge v6, v7, :cond_0

    const-string v6, "iu.SyncManager"

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "iu.SyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cancel task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; higher priority task: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/SyncTask;->cancelSync()V

    goto/16 :goto_1
.end method


# virtual methods
.method public final declared-synchronized onAccountActivated(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/UserEntry;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHelper:Lcom/google/android/apps/plus/iu/PicasaSyncHelper;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/plus/iu/UserEntry;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/iu/UserEntry;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/iu/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2, v0, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final declared-synchronized onAccountDeactivated(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    sget-object v2, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "upload_account == ?"

    invoke-virtual {v0, v2, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final onBatteryStateChanged(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    if-eqz p1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final onEnvironmentChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final performSync(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;)V
    .locals 12
    .param p1    # Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    const-wide/16 v10, 0x0

    const/4 v9, 0x0

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    iput-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-enter p0

    :try_start_1
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mProvider:Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;

    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager$UploadTaskProvider;->onSyncStart()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_0
    :try_start_2
    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->account:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getNextSyncTask(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/SyncTask;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->acceptSyncTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "iu.SyncManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "iu.SyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SYNC; not accepted; task: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/SyncTask;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    iget-object v5, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->account:Ljava/lang/String;

    iget v6, v3, Lcom/google/android/apps/plus/iu/SyncTask;->mPriority:I

    shr-int/lit8 v6, v6, 0x1

    add-int/lit8 v6, v6, -0x1

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->enqueueMediaRecords(Landroid/content/Context;Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    :goto_0
    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    sget-object v4, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v4}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v4

    const-string v7, "upload_account NOT NULL AND upload_account != ?"

    invoke-virtual {v5, v4, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v4, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v4}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->UPLOAD_TASK_CLEANUP_DELETE_WHERE:Ljava/lang/String;

    invoke-virtual {v5, v4, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->isSyncCancelled()Z

    move-result v4

    if-nez v4, :cond_3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->updateTasks(J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :cond_3
    iput-object v9, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_4
    :try_start_3
    invoke-virtual {p1, v3}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->setCurrentTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    if-eqz v3, :cond_2

    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->account:Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/apps/plus/iu/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v4

    if-eqz v4, :cond_2

    :try_start_4
    const-string v4, "iu.SyncManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "iu.SyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SYNC; start task: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/iu/SyncTask;->performSync(Landroid/content/SyncResult;)V

    const-string v4, "iu.SyncManager"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "iu.SyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "SYNC; complete; result: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :cond_6
    const/4 v4, 0x0

    :try_start_5
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->setCurrentTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    :goto_1
    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->result:Landroid/content/SyncResult;

    iget-object v4, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v4, v4, v10

    if-gtz v4, :cond_7

    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->result:Landroid/content/SyncResult;

    iget-object v4, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    cmp-long v4, v4, v10

    if-lez v4, :cond_0

    :cond_7
    const-string v4, "iu.SyncManager"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_8

    const-string v4, "iu.SyncManager"

    const-string v5, "stop sync session due to io error"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->cancelSync()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v4

    iput-object v9, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mCurrentSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    throw v4

    :catch_0
    move-exception v0

    :try_start_6
    const-string v4, "iu.SyncManager"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "iu.SyncManager"

    const-string v5, "SYNC; fail"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_9
    iget-object v4, p1, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->result:Landroid/content/SyncResult;

    iget-object v4, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v5, v4, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v7, 0x1

    add-long/2addr v5, v7

    iput-wide v5, v4, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    const/4 v4, 0x0

    :try_start_7
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->setCurrentTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_1

    :catch_1
    move-exception v2

    :try_start_8
    const-string v4, "iu.SyncManager"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "iu.SyncManager"

    const-string v5, "SYNC; fail"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :cond_a
    const/4 v4, 0x0

    :try_start_9
    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->setCurrentTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    goto :goto_1

    :catchall_3
    move-exception v4

    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->setCurrentTask(Lcom/google/android/apps/plus/iu/SyncTask;)Z

    throw v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2
.end method

.method public final requestAccountSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final updateTasks(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
