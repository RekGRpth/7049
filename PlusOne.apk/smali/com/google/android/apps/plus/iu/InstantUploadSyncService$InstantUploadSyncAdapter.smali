.class final Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "InstantUploadSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/InstantUploadSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InstantUploadSyncAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static declared-synchronized carryOverSyncAutomaticallyForAllAccounts(Landroid/content/Context;)V
    .locals 11
    .param p0    # Landroid/content/Context;

    const/4 v9, 0x2

    const-class v8, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;

    monitor-enter v8

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    new-instance v1, Landroid/content/ComponentName;

    const-class v7, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$CarryOverDummyReceiver;

    invoke-direct {v1, p0, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v6, v1}, Landroid/content/pm/PackageManager;->getComponentEnabledSetting(Landroid/content/ComponentName;)I

    move-result v7

    if-eq v7, v9, :cond_4

    const/4 v7, 0x2

    const/4 v9, 0x1

    invoke-virtual {v6, v1, v7, v9}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v7

    const-string v9, "com.google"

    invoke-virtual {v7, v9}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    monitor-exit v8

    return-void

    :cond_1
    :try_start_1
    array-length v7, v2

    add-int/lit8 v3, v7, -0x1

    :goto_1
    if-ltz v3, :cond_0

    aget-object v0, v2, v3

    const-string v7, "com.google.android.apps.plus.content.EsGooglePhotoProvider"

    invoke-static {v0, v7}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    const-string v7, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v7}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v5

    const-string v7, "iu.SyncService"

    const/4 v9, 0x3

    invoke-static {v7, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "iu.SyncService"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Carry over sync; plus? "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", photo? "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; acct: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v5, :cond_3

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v7, v4}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->activateAccount(Landroid/content/Context;Ljava/lang/String;Z)V

    :goto_2
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_3
    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v7}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->deactivateAccount(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    :cond_4
    :try_start_2
    const-string v7, "iu.SyncService"

    const/4 v9, 0x3

    invoke-static {v7, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "iu.SyncService"

    const-string v9, "leave auto sync alone"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 10
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/ContentProviderClient;
    .param p5    # Landroid/content/SyncResult;

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz p2, :cond_2

    const-string v5, "initialize"

    const/4 v6, 0x0

    invoke-virtual {p2, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->carryOverSyncAutomaticallyForAllAccounts(Landroid/content/Context;)V

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v6

    const-string v7, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    if-eqz v2, :cond_3

    const/4 v5, 0x1

    :goto_2
    invoke-static {v6, v7, v5}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    :cond_4
    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/NewMediaTracker;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->processNewMedia()I

    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;

    move-result-object v4

    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_6

    const-string v5, "iu.SyncService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "iu.SyncService"

    const-string v6, "sync is cancelled"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_6
    :try_start_1
    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5, p5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->createSession(Ljava/lang/String;Landroid/content/SyncResult;)Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v5, "iu.SyncService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v5, "iu.SyncService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "start to perform sync on "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :try_start_2
    iget-object v5, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager;->performSync(Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const-string v5, "iu.SyncService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "iu.SyncService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sync finished - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->isSyncCancelled()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :catch_0
    move-exception v1

    :try_start_3
    const-string v5, "iu.SyncService"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "iu.SyncService"

    const-string v6, "performSync error"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_8
    iget-object v5, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v5, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v5, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const-string v5, "iu.SyncService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "iu.SyncService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sync finished - "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->isSyncCancelled()Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :catchall_1
    move-exception v5

    const-string v6, "iu.SyncService"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_9

    const-string v6, "iu.SyncService"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sync finished - "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->isSyncCancelled()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    throw v5
.end method

.method public final declared-synchronized onSyncCanceled()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "iu.SyncService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "iu.SyncService"

    const-string v1, "receive cancel request"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/InstantUploadSyncService$InstantUploadSyncAdapter;->mSession:Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/InstantUploadSyncManager$InstantUploadSyncSession;->cancelSync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
