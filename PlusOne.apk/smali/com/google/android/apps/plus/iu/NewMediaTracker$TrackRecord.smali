.class final Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;
.super Ljava/lang/Object;
.source "NewMediaTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/NewMediaTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TrackRecord"
.end annotation


# instance fields
.field final mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

.field mLastMediaId:J


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mLastMediaId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
