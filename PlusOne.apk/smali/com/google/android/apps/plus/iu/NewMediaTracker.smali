.class public final Lcom/google/android/apps/plus/iu/NewMediaTracker;
.super Ljava/lang/Object;
.source "NewMediaTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;,
        Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;
    }
.end annotation


# static fields
.field private static final ALL_CONFIGS:[Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

.field private static final COUNT_MEDIA_BY_ACCOUNT_AND_REASON:Ljava/lang/String;

.field private static final EXIF_TAGS:[Ljava/lang/String;

.field private static final MEDIA_RECORD_TABLE:Ljava/lang/String;

.field private static final PROJECTION_ID:[Ljava/lang/String;

.field private static final PROJECTION_MAX_ID:[Ljava/lang/String;

.field private static final SELECT_MEDIA_NOT_UPLOADED_BY_ACCOUNT:Ljava/lang/String;

.field private static final UPLOAD_ACCOUNT_INDEX:I

.field private static final UPLOAD_REASON_INDEX:I

.field private static sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mPreferences:Landroid/content/SharedPreferences;

.field private mStopProcessing:Z

.field private final mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

.field private final mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v3, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "FNumber"

    aput-object v1, v0, v5

    const-string v1, "DateTime"

    aput-object v1, v0, v6

    const-string v1, "ExposureTime"

    aput-object v1, v0, v7

    const-string v1, "Flash"

    aput-object v1, v0, v8

    const-string v1, "FocalLength"

    aput-object v1, v0, v3

    const/4 v1, 0x5

    const-string v2, "GPSAltitude"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "GPSAltitudeRef"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "GPSDateStamp"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "GPSLatitude"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "GPSLatitudeRef"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "GPSLongitude"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "GPSLongitudeRef"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "GPSProcessingMethod"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "GPSTimeStamp"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "ImageLength"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "ImageWidth"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "ISOSpeedRatings"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Make"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Model"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Orientation"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->EXIF_TAGS:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "MAX(_id)"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->PROJECTION_MAX_ID:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->PROJECTION_ID:[Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const-string v1, "upload_reason"

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->UPLOAD_REASON_INDEX:I

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    const-string v1, "upload_account"

    invoke-virtual {v0, v1}, Lcom/android/gallery3d/common/EntrySchema;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->UPLOAD_ACCOUNT_INDEX:I

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0}, Lcom/android/gallery3d/common/EntrySchema;->getTableName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "upload_account IS NULL AND media_id NOT IN ( SELECT media_id FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = ? )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->SELECT_MEDIA_NOT_UPLOADED_BY_ACCOUNT:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "SELECT COUNT(*) FROM "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE upload_account = ? AND ( (upload_state / 100) * 100 == 100 OR (upload_state / 100) * 100 == 200 ) AND upload_reason = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->COUNT_MEDIA_BY_ACCOUNT_AND_REASON:Ljava/lang/String;

    new-array v0, v3, [Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    new-instance v1, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    const-string v2, "photo"

    sget-object v3, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "external"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    const-string v2, "photo"

    const-string v3, "phoneStorage"

    invoke-static {v3}, Landroid/provider/MediaStore$Images$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "phoneStorage"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    const-string v2, "video"

    sget-object v3, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v4, "external"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    const-string v2, "video"

    const-string v3, "phoneStorage"

    invoke-static {v3}, Landroid/provider/MediaStore$Video$Media;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "phoneStorage"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;-><init>(Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    aput-object v1, v0, v8

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->ALL_CONFIGS:[Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mPreferences:Landroid/content/SharedPreferences;

    invoke-static {p1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    sget-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->ALL_CONFIGS:[Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->loadStates()V

    return-void
.end method

.method public static declared-synchronized clearPreferences(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->clearPreferencesInternal(Landroid/content/SharedPreferences;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mStopProcessing:Z

    sget-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    invoke-direct {v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->resetPreferencesInternal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static clearPreferencesInternal(Landroid/content/SharedPreferences;)V
    .locals 4
    .param p0    # Landroid/content/SharedPreferences;

    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "media_scanner."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public static enqueueMediaRecords(Landroid/content/Context;Ljava/lang/String;I)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-static {p0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-result-object v1

    if-nez p1, :cond_3

    if-gez p2, :cond_2

    const-string v7, "upload_account NOT NULL AND (upload_state / 100) * 100 == 200"

    const/4 v6, 0x0

    :goto_0
    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x4

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "SELECT COUNT(*) FROM "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v9, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " WHERE "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    invoke-static {v8, v0, v6}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "num queued entries: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v5, Landroid/content/ContentValues;

    const/4 v8, 0x1

    invoke-direct {v5, v8}, Landroid/content/ContentValues;-><init>(I)V

    const-string v8, "upload_state"

    const/16 v9, 0x64

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v8

    sget-object v9, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    invoke-virtual {v8, v9, v5, v7, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    const-string v8, "iu.UploadsManager"

    const/4 v9, 0x4

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_1

    const-string v8, "iu.UploadsManager"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "num updated entries: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-lez v4, :cond_5

    const/4 v8, 0x1

    :goto_1
    return v8

    :cond_2
    const-string v7, "upload_account NOT NULL AND upload_reason <= ? AND (upload_state / 100) * 100 == 200"

    const/4 v8, 0x1

    new-array v6, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    goto/16 :goto_0

    :cond_3
    if-gez p2, :cond_4

    const-string v7, "upload_account = ? AND (upload_state / 100) * 100 == 200"

    const/4 v8, 0x1

    new-array v6, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v6, v8

    goto/16 :goto_0

    :cond_4
    const-string v7, "upload_account = ? AND upload_reason <= ? AND (upload_state / 100) * 100 == 200"

    const/4 v8, 0x2

    new-array v6, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object p1, v6, v8

    const/4 v8, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v8

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/NewMediaTracker;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->sMediaTracker:Lcom/google/android/apps/plus/iu/NewMediaTracker;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getMediaTime(Landroid/content/ContentResolver;Landroid/net/Uri;)J
    .locals 10
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "datetaken"

    aput-object v5, v4, v8

    invoke-static {p0, p1, v4, v6, v7}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v4, v2, v6

    if-lez v4, :cond_0

    :goto_0
    return-wide v2

    :cond_0
    new-array v4, v9, [Ljava/lang/String;

    const-string v5, "date_added"

    aput-object v5, v4, v8

    invoke-static {p0, p1, v4, v6, v7}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v4, v0, v6

    if-lez v4, :cond_1

    move-wide v2, v0

    goto :goto_0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    goto :goto_0
.end method

.method private static getNextMediaId(Landroid/content/ContentResolver;Landroid/net/Uri;J)J
    .locals 9
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;
    .param p2    # J

    const-wide/16 v7, -0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->PROJECTION_ID:[Ljava/lang/String;

    const-string v3, "_id > ? AND _data LIKE \'%/DCIM/%\'"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    const-string v5, "_id"

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-wide v7

    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :goto_1
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-wide v7, v0

    goto :goto_0

    :cond_1
    move-wide v0, v7

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J
    .locals 9
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # J

    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-wide v0

    :cond_1
    move-wide v0, v7

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-wide v0, v7

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :goto_0
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v7

    goto :goto_0

    :catch_0
    move-exception v0

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    goto :goto_1

    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private static hasExif(Landroid/media/ExifInterface;)Z
    .locals 2
    .param p0    # Landroid/media/ExifInterface;

    sget-object v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;->EXIF_TAGS:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-lez v0, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;->EXIF_TAGS:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static hasGoogleUploadExif(Landroid/content/ContentResolver;Landroid/net/Uri;)Z
    .locals 8
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-array v6, v5, [Ljava/lang/String;

    const-string v7, "_data"

    aput-object v7, v6, v4

    invoke-static {p0, p1, v6}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "jpg"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "jpeg"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-direct {v0, v2}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    const-string v6, "Software"

    invoke-virtual {v0, v6}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v6, "Google"

    invoke-virtual {v3, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-ltz v6, :cond_0

    move v4, v5

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private static isStorageAvailable(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;)Z
    .locals 9
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    const/4 v8, 0x0

    const/4 v6, 0x0

    :try_start_0
    iget-object v1, p1, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mMediaStoreUri:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->PROJECTION_MAX_ID:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_1
    return v0

    :cond_0
    move v0, v8

    goto :goto_0

    :cond_1
    move v0, v8

    goto :goto_0

    :cond_2
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_2
    move v0, v8

    goto :goto_1

    :catch_0
    move-exception v7

    :try_start_1
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "iu.UploadsManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "exception loading config: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private loadStates()V
    .locals 7

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    array-length v3, v3

    add-int/lit8 v1, v3, -0x1

    :goto_0
    if-ltz v1, :cond_0

    sget-object v3, Lcom/google/android/apps/plus/iu/NewMediaTracker;->ALL_CONFIGS:[Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    aget-object v0, v3, v1

    new-instance v2, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;-><init>(Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mPreferences:Landroid/content/SharedPreferences;

    iget-object v4, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mKeyLastMediaId:Ljava/lang/String;

    const-wide/16 v5, 0x0

    invoke-interface {v3, v4, v5, v6}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mLastMediaId:J

    iget-object v3, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    aput-object v2, v3, v1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private performSanityChecks(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/UploadSettings;Landroid/net/Uri;Z)Z
    .locals 20
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Lcom/google/android/apps/plus/iu/UploadSettings;
    .param p3    # Landroid/net/Uri;
    .param p4    # Z

    const-string v13, "MMM dd, yyyy h:mmaa"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "datetaken"

    aput-object v19, v17, v18

    const-wide/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, v17

    move-wide/from16 v3, v18

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v5

    const-wide/16 v17, 0x0

    cmp-long v17, v5, v17

    if-lez v17, :cond_2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getEventEndTime()J

    move-result-wide v17

    cmp-long v17, v5, v17

    if-gtz v17, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getEventStartTime()J

    move-result-wide v17

    cmp-long v17, v5, v17

    if-gez v17, :cond_2

    :cond_0
    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1

    new-instance v17, Ljava/util/Date;

    move-object/from16 v0, v17

    invoke-direct {v0, v5, v6}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v17

    invoke-static {v13, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v7

    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: bad taken time; taken: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/16 v17, 0x0

    :goto_0
    return v17

    :cond_2
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const-string v19, "_data"

    aput-object v19, v17, v18

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_5

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->lastModified()J

    move-result-wide v14

    const-wide/16 v17, 0x0

    cmp-long v17, v14, v17

    if-lez v17, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getEventEndTime()J

    move-result-wide v17

    cmp-long v17, v14, v17

    if-gtz v17, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/iu/UploadSettings;->getEventStartTime()J

    move-result-wide v17

    cmp-long v17, v14, v17

    if-gez v17, :cond_5

    :cond_3
    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_4

    new-instance v17, Ljava/util/Date;

    move-object/from16 v0, v17

    invoke-direct {v0, v14, v15}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v17

    invoke-static {v13, v0}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Date;)Ljava/lang/CharSequence;

    move-result-object v16

    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_4

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: bad modify time; modified: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/16 v17, 0x0

    goto :goto_0

    :cond_5
    if-eqz v11, :cond_6

    const-string v17, "cache/com.google.android.googlephotos"

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_6

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: file from cache directory; path: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x0

    goto/16 :goto_0

    :cond_6
    if-eqz p4, :cond_c

    if-eqz v11, :cond_c

    invoke-static {v11}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v17, "jpg"

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_8

    const-string v17, "jpeg"

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_8

    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_7

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " is not a jpeg"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const/16 v17, 0x0

    goto/16 :goto_0

    :cond_8
    :try_start_0
    new-instance v8, Landroid/media/ExifInterface;

    invoke-direct {v8, v11}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->hasExif(Landroid/media/ExifInterface;)Z

    move-result v17

    if-nez v17, :cond_a

    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_9

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " does not contain any EXIF data"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const/16 v17, 0x0

    goto/16 :goto_0

    :cond_a
    const-string v17, "Software"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_c

    const-string v17, "Google"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v17

    if-ltz v17, :cond_c

    const-string v17, "iu.UploadsManager"

    const/16 v18, 0x4

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_b

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " has GOOGLE_EXIF_TAG set"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_b
    const/16 v17, 0x0

    goto/16 :goto_0

    :catch_0
    move-exception v17

    const-string v17, "iu.UploadsManager"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "FAIL: could get EXIF for file: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v17, 0x0

    goto/16 :goto_0

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/util/GservicesConfig;->isInstantShareEnabled(Landroid/content/Context;)Z

    move-result v17

    if-eqz v17, :cond_10

    if-nez p4, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v17

    const-string v18, "plusone:enable_instant_share_video"

    const/16 v19, 0x1

    invoke-static/range {v17 .. v19}, Lcom/google/android/gsf/EsGservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v17

    if-nez v17, :cond_d

    sget-object v17, Lcom/google/android/apps/plus/util/Property;->ENABLE_INSTANT_SHARE_VIDEO:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v17

    if-eqz v17, :cond_f

    :cond_d
    const/16 v17, 0x1

    :goto_1
    if-eqz v17, :cond_10

    :cond_e
    const/16 v17, 0x1

    goto/16 :goto_0

    :cond_f
    const/16 v17, 0x0

    goto :goto_1

    :cond_10
    const/16 v17, 0x0

    goto/16 :goto_0
.end method

.method private declared-synchronized resetPreferencesInternal()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mPreferences:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->clearPreferencesInternal(Landroid/content/SharedPreferences;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->loadStates()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mStopProcessing:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private saveStates()V
    .locals 9

    iget-object v6, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    iget-object v1, v5, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    iget-object v6, v1, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mKeyLastMediaId:Ljava/lang/String;

    iget-wide v7, v5, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mLastMediaId:J

    invoke-interface {v2, v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method


# virtual methods
.method final cancelUpload(Ljava/lang/String;I)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v3, "upload_account = ? AND ( (upload_state / 100) * 100 == 100 OR (upload_state / 100) * 100 == 200 ) AND upload_reason = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v4, v6

    const/4 v6, 0x1

    const/16 v7, 0x28

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v9

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    iget-object v1, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-wide v2, v9, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->id:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/gallery3d/common/EntrySchema;->deleteWithId(Landroid/database/sqlite/SQLiteDatabase;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public final getNextMediaRecord(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    if-nez p1, :cond_0

    const-string v3, "upload_account NOT NULL AND (upload_state / 100) * 100 == 100"

    const/4 v4, 0x0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v2

    const-string v7, "upload_reason ASC, upload_state ASC, is_image DESC, retry_end_time ASC"

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v8}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v5

    :cond_0
    const-string v3, "upload_account = ? AND (upload_state / 100) * 100 == 100"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    goto :goto_0

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method final getNextUpload(Ljava/util/HashSet;Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;)Lcom/google/android/apps/plus/iu/UploadTaskEntry;
    .locals 19
    .param p2    # Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/gallery3d/common/Fingerprint;",
            ">;",
            "Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;",
            ")",
            "Lcom/google/android/apps/plus/iu/UploadTaskEntry;"
        }
    .end annotation

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v3}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v3

    const-string v4, "upload_account NOT NULL AND (upload_state / 100) * 100 == 100"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v8, "upload_reason ASC, upload_state ASC, is_image DESC, retry_end_time ASC"

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/GservicesConfig;->isInstantShareEnabled(Landroid/content/Context;)Z

    move-result v13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->get(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/FingerprintHelper;

    move-result-object v12

    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_d

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/iu/PicasaSyncHelper$SyncContext;->syncInterrupted()Z

    move-result v3

    if-nez v3, :cond_d

    :cond_0
    invoke-static {v11}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getMediaUrl()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v10, 0x0

    sget v3, Lcom/google/android/apps/plus/iu/NewMediaTracker;->UPLOAD_REASON_INDEX:I

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    sget v3, Lcom/google/android/apps/plus/iu/NewMediaTracker;->UPLOAD_ACCOUNT_INDEX:I

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x14

    move/from16 v0, v17

    if-ne v0, v3, :cond_2

    if-nez v13, :cond_a

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP record; instant share disabled; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/16 v3, 0x12c

    const/16 v4, 0x24

    invoke-virtual {v14, v3, v4}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v3, v4, v14}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_2
    const/16 v3, 0x28

    move/from16 v0, v17

    if-ne v0, v3, :cond_8

    :try_start_1
    invoke-virtual {v12, v15}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->getCachedFingerprint(Ljava/lang/String;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v9

    if-nez v9, :cond_3

    const/4 v3, 0x1

    invoke-virtual {v12, v15, v3}, Lcom/google/android/apps/plus/iu/FingerprintHelper;->getFingerprint(Ljava/lang/String;Z)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v9

    move-object v10, v9

    :cond_3
    if-nez v9, :cond_6

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP record; no fingerprint; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x5

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "no fingerprint; skip upload for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/16 v3, 0x12c

    const/16 v4, 0x23

    invoke-virtual {v14, v3, v4}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v3, v4, v14}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto/16 :goto_0

    :cond_6
    if-eqz p1, :cond_a

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP record; duplicate upload; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    const/16 v3, 0x190

    const/16 v4, 0x22

    invoke-virtual {v14, v3, v4}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v3, v4, v14}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v14}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->isImage()Z

    move-result v3

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->hasGoogleUploadExif(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ SKIP record; has google exif; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const/16 v3, 0x12c

    const/16 v4, 0x25

    invoke-virtual {v14, v3, v4}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(II)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    invoke-virtual {v3, v4, v14}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto/16 :goto_0

    :cond_a
    iget-wide v3, v14, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->id:J

    invoke-virtual {v14}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getAlbumId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v14}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->getEventId()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/plus/iu/UploadRequestHelper;->createUploadTask(Ljava/lang/String;Landroid/net/Uri;JLjava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/UploadTaskEntry;

    move-result-object v16

    if-eqz v10, :cond_b

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/iu/UploadTaskEntry;->setFingerprint(Lcom/android/gallery3d/common/Fingerprint;)V

    :cond_b
    const-string v3, "iu.UploadsManager"

    const/4 v4, 0x4

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_c

    const-string v3, "iu.UploadsManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "+++ START; upload started; "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_c
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v16

    :cond_d
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    const/16 v16, 0x0

    goto :goto_1
.end method

.method final getUploadProgress(Ljava/lang/String;I)I
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v0, 0x0

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->COUNT_MEDIA_BY_ACCOUNT_AND_REASON:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object p1, v3, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method final getUploadTotal$505cff29(I)I
    .locals 12
    .param p1    # I

    const/4 v1, 0x1

    const/4 v11, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    new-array v3, v1, [Ljava/lang/String;

    const-string v4, "COUNT(*)"

    aput-object v4, v3, v11

    const-string v4, "upload_account IS NULL"

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    move-object v9, v5

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return v0

    :cond_0
    move v0, v11

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final declared-synchronized processAllMedia()I
    .locals 30

    monitor-enter p0

    :try_start_0
    new-instance v25, Landroid/content/ContentValues;

    invoke-direct/range {v25 .. v25}, Landroid/content/ContentValues;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const/16 v19, 0x0

    const-wide/16 v17, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    const-string v26, "iu.UploadsManager"

    const/16 v27, 0x4

    invoke-static/range {v26 .. v27}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v26

    if-eqz v26, :cond_0

    const-string v26, "iu.UploadsManager"

    const-string v27, "START; processing all media"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    array-length v0, v0

    move/from16 v26, v0

    add-int/lit8 v13, v26, -0x1

    :goto_0
    if-ltz v13, :cond_a

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mStopProcessing:Z

    move/from16 v26, v0

    if-nez v26, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    move-object/from16 v26, v0

    aget-object v20, v26, v13

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    move-object/from16 v0, v21

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->isStorageAvailable(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;)Z

    move-result v26

    if-nez v26, :cond_2

    const-string v26, "iu.UploadsManager"

    const/16 v27, 0x4

    invoke-static/range {v26 .. v27}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v26

    if-eqz v26, :cond_1

    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "SKIP; no storage for config: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    add-int/lit8 v13, v13, -0x1

    goto :goto_0

    :cond_2
    const-wide/16 v15, -0x1

    :cond_3
    iget-object v0, v7, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mMediaStoreUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v26

    move-wide v2, v15

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getNextMediaId(Landroid/content/ContentResolver;Landroid/net/Uri;J)J

    move-result-wide v17

    const-wide/16 v26, -0x1

    cmp-long v26, v17, v26

    if-nez v26, :cond_5

    const-string v26, "iu.UploadsManager"

    const/16 v27, 0x4

    invoke-static/range {v26 .. v27}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v26

    if-eqz v26, :cond_4

    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "DONE; no more media of type: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move-object/from16 v0, v20

    iput-wide v15, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mLastMediaId:J

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->saveStates()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v26

    monitor-exit p0

    throw v26

    :cond_5
    move-wide/from16 v15, v17

    :try_start_1
    move-wide/from16 v0, v17

    invoke-static {v9, v0, v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromMediaId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v26

    if-eqz v26, :cond_7

    const-string v26, "iu.UploadsManager"

    const/16 v27, 0x4

    invoke-static/range {v26 .. v27}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v26

    if-eqz v26, :cond_6

    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "SKIP; existing media id: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_2
    const-wide/16 v26, -0x1

    cmp-long v26, v17, v26

    if-eqz v26, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mStopProcessing:Z

    move/from16 v26, v0

    if-eqz v26, :cond_3

    goto :goto_1

    :cond_7
    const-string v26, "iu.UploadsManager"

    const/16 v27, 0x4

    invoke-static/range {v26 .. v27}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v26

    if-eqz v26, :cond_8

    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "NEW; add media id: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v0, v7, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mMediaStoreUri:Landroid/net/Uri;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v26

    invoke-static/range {v17 .. v18}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v24

    const-string v26, "photo"

    move-object/from16 v0, v26

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->matchesMediaType(Ljava/lang/String;)Z

    move-result v14

    invoke-virtual/range {v25 .. v25}, Landroid/content/ContentValues;->clear()V

    const-string v26, "album_id"

    invoke-virtual/range {v25 .. v26}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v26, "event_id"

    invoke-virtual/range {v25 .. v26}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v26, "upload_account"

    invoke-virtual/range {v25 .. v26}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v26, "is_image"

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v26, "media_id"

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v26, "media_time"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getMediaTime(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v27

    invoke-static/range {v27 .. v28}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const-string v28, "_data"

    aput-object v28, v26, v27

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_9

    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v11

    :cond_9
    const-string v26, "media_hash"

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v27

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const-string v28, "_size"

    aput-object v28, v26, v27

    const-wide/16 v27, 0x0

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    move-object/from16 v2, v26

    move-wide/from16 v3, v27

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v5

    const-string v26, "bytes_total"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v26, 0x1

    :try_start_2
    move/from16 v0, v26

    new-array v8, v0, [J

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-static {v0, v8}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v12

    const-string v26, "fingerprint"

    invoke-virtual {v12}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_3
    :try_start_3
    const-string v26, "media_url"

    invoke-virtual/range {v24 .. v24}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v26, "upload_reason"

    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v26, "upload_state"

    const/16 v27, 0x1f4

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v27

    invoke-virtual/range {v25 .. v27}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v26, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2

    :catch_0
    move-exception v10

    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "WARNING; no fingerprint for media id: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-wide/from16 v1, v17

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v26, "fingerprint"

    invoke-virtual/range {v25 .. v26}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    const-string v26, "iu.UploadsManager"

    const/16 v27, 0x4

    invoke-static/range {v26 .. v27}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v26

    if-eqz v26, :cond_b

    const-string v26, "iu.UploadsManager"

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "END; processing all media; "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    sub-long v28, v28, v22

    invoke-virtual/range {v27 .. v29}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, " ms"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_b
    monitor-exit p0

    return v19
.end method

.method public final declared-synchronized processNewMedia()I
    .locals 36

    monitor-enter p0

    :try_start_0
    new-instance v31, Landroid/content/ContentValues;

    invoke-direct/range {v31 .. v31}, Landroid/content/ContentValues;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-static/range {v32 .. v32}, Lcom/google/android/apps/plus/iu/UploadSettings;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    const/16 v22, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v27

    new-instance v19, Ljava/lang/StringBuffer;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuffer;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v32, v0

    const-string v33, "media_scanner.has_run"

    const/16 v34, 0x0

    invoke-interface/range {v32 .. v34}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_0

    const-string v32, "iu.UploadsManager"

    const-string v33, "Start processing new media"

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    array-length v0, v0

    move/from16 v32, v0

    add-int/lit8 v15, v32, -0x1

    :goto_0
    if-ltz v15, :cond_e

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mStopProcessing:Z

    move/from16 v32, v0

    if-nez v32, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    move-object/from16 v32, v0

    aget-object v24, v32, v15

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    move-object/from16 v32, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->isStorageAvailable(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;)Z

    move-result v32

    if-eqz v32, :cond_2

    move-object/from16 v0, v24

    iget-object v7, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mConfig:Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;

    :cond_1
    iget-object v0, v7, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mMediaStoreUri:Landroid/net/Uri;

    move-object/from16 v32, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mLastMediaId:J

    move-wide/from16 v33, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v32

    move-wide/from16 v2, v33

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getNextMediaId(Landroid/content/ContentResolver;Landroid/net/Uri;J)J

    move-result-wide v20

    const-wide/16 v32, -0x1

    cmp-long v32, v20, v32

    if-nez v32, :cond_3

    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_2

    const-string v32, "iu.UploadsManager"

    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "no new media; type: "

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v7, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mMediaType:Ljava/lang/String;

    move-object/from16 v34, v0

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    add-int/lit8 v15, v15, -0x1

    goto :goto_0

    :cond_3
    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_4

    const/16 v32, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->setLength(I)V

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "NEW; media_id: "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    move-wide/from16 v0, v20

    move-object/from16 v2, v24

    iput-wide v0, v2, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->mLastMediaId:J

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->saveStates()V

    iget-object v0, v7, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->mMediaStoreUri:Landroid/net/Uri;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v32

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v32 .. v33}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v30

    const-string v32, "photo"

    move-object/from16 v0, v32

    invoke-virtual {v7, v0}, Lcom/google/android/apps/plus/iu/NewMediaTracker$Config;->matchesMediaType(Ljava/lang/String;)Z

    move-result v18

    invoke-virtual/range {v31 .. v31}, Landroid/content/ContentValues;->clear()V

    move-wide/from16 v0, v20

    invoke-static {v9, v0, v1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromMediaId(Landroid/database/sqlite/SQLiteDatabase;J)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v23

    if-eqz v23, :cond_6

    const-string v32, "iu.UploadsManager"

    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "!!! update existing media id: "

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    :goto_2
    const-wide/16 v32, -0x1

    cmp-long v32, v20, v32

    if-eqz v32, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mStopProcessing:Z

    move/from16 v32, v0

    if-eqz v32, :cond_1

    goto/16 :goto_1

    :cond_6
    const-string v32, "album_id"

    invoke-virtual/range {v31 .. v32}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v32, "event_id"

    invoke-virtual/range {v31 .. v32}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v32, "upload_account"

    invoke-virtual/range {v31 .. v32}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v32, "is_image"

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v32, "media_id"

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v32, "media_time"

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getMediaTime(Landroid/content/ContentResolver;Landroid/net/Uri;)J

    move-result-wide v33

    invoke-static/range {v33 .. v34}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const-string v34, "_data"

    aput-object v34, v32, v33

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    move-object/from16 v2, v32

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalString(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    if-nez v12, :cond_7

    invoke-virtual/range {v30 .. v30}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v12

    :cond_7
    const-string v32, "media_hash"

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v33

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/16 v32, 0x1

    move/from16 v0, v32

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    const-string v34, "_size"

    aput-object v34, v32, v33

    const-wide/16 v33, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    move-object/from16 v2, v32

    move-wide/from16 v3, v33

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->getOptionalLong(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;J)J

    move-result-wide v5

    const-string v32, "bytes_total"

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v32, 0x1

    :try_start_1
    move/from16 v0, v32

    new-array v8, v0, [J

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v32

    move-object/from16 v0, v32

    invoke-static {v0, v8}, Lcom/android/gallery3d/common/Fingerprint;->fromInputStream(Ljava/io/InputStream;[J)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v13

    const-string v32, "fingerprint"

    invoke-virtual {v13}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_3
    :try_start_2
    const-string v32, "media_url"

    invoke-virtual/range {v30 .. v30}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v32, "upload_reason"

    const/16 v33, 0x0

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v32, "upload_state"

    const/16 v33, 0x1f4

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v32, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v33

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v0, v9, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    add-int/lit8 v22, v22, 0x1

    if-eqz v14, :cond_9

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings()V

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadSettings;->getSyncAccount()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadSettings;->getEventId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/iu/UploadSettings;->getAutoUploadEnabled()Z

    move-result v32

    if-eqz v32, :cond_a

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_a

    const/16 v17, 0x1

    :goto_4
    if-eqz v11, :cond_b

    invoke-static/range {v29 .. v29}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v30

    move/from16 v4, v18

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->performSanityChecks(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/iu/UploadSettings;Landroid/net/Uri;Z)Z

    move-result v32

    if-eqz v32, :cond_b

    const/16 v16, 0x1

    :goto_5
    if-eqz v16, :cond_c

    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_8

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, ", IS: "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_8
    const-string v32, "_id"

    invoke-virtual/range {v31 .. v32}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v32, "event_id"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v32, "upload_account"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v32, "upload_reason"

    const/16 v33, 0x14

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v32, "upload_state"

    const/16 v33, 0x64

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v32, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v33

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v0, v9, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    :cond_9
    :goto_6
    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_5

    const-string v32, "iu.UploadsManager"

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2

    :catchall_0
    move-exception v32

    monitor-exit p0

    throw v32

    :catch_0
    move-exception v10

    :try_start_3
    const-string v32, "iu.UploadsManager"

    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "!!! could not get fingerprint; media id: "

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v32, "fingerprint"

    invoke-virtual/range {v31 .. v32}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_4

    :cond_b
    const/16 v16, 0x0

    goto/16 :goto_5

    :cond_c
    if-eqz v17, :cond_9

    move-object/from16 v0, v25

    move-object/from16 v1, v30

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/iu/NewMediaTracker;->hasGoogleUploadExif(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v32

    if-nez v32, :cond_9

    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_d

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, ", IU: "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    move-object/from16 v0, v19

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_d
    const-string v32, "_id"

    invoke-virtual/range {v31 .. v32}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v32, "upload_account"

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    move-object/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v32, "upload_reason"

    const/16 v33, 0x1e

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v32, "upload_state"

    const/16 v33, 0x64

    invoke-static/range {v33 .. v33}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    invoke-virtual/range {v31 .. v33}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v32, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->createNew(Landroid/content/ContentValues;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v33

    move-object/from16 v0, v32

    move-object/from16 v1, v33

    invoke-virtual {v0, v9, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J

    goto/16 :goto_6

    :cond_e
    const-string v32, "iu.UploadsManager"

    const/16 v33, 0x4

    invoke-static/range {v32 .. v33}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v32

    if-eqz v32, :cond_f

    const-string v32, "iu.UploadsManager"

    new-instance v33, Ljava/lang/StringBuilder;

    const-string v34, "End processing new media; "

    invoke-direct/range {v33 .. v34}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v34

    sub-long v34, v34, v27

    invoke-virtual/range {v33 .. v35}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v33

    const-string v34, " ms"

    invoke-virtual/range {v33 .. v34}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v33

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v32 .. v33}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mPreferences:Landroid/content/SharedPreferences;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v32

    const-string v33, "media_scanner.has_run"

    const/16 v34, 0x1

    invoke-interface/range {v32 .. v34}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v32

    invoke-interface/range {v32 .. v32}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return v22
.end method

.method final startUpload(Ljava/lang/String;I)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v3}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/iu/NewMediaTracker;->SELECT_MEDIA_NOT_UPLOADED_BY_ACCOUNT:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v10}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v12

    const-wide/16 v0, 0x0

    iput-wide v0, v12, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->id:J

    invoke-virtual {v12, p1}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadAccount(Ljava/lang/String;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    const/16 v0, 0x28

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setUploadReason(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    const/16 v0, 0x64

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0, v11, v12}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mUploadsDbHelper:Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/iu/UploadsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/plus/iu/NewMediaTracker;->MEDIA_RECORD_TABLE:Ljava/lang/String;

    sget-object v3, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v3}, Lcom/android/gallery3d/common/EntrySchema;->getProjection()[Ljava/lang/String;

    move-result-object v3

    const-string v4, "upload_account = ? AND (upload_state / 100) * 100 == 300"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v0 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :goto_1
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v10}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->fromCursor(Landroid/database/Cursor;)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    move-result-object v12

    const/16 v0, 0x64

    invoke-virtual {v12, v0}, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->setState(I)Lcom/google/android/apps/plus/iu/MediaRecordEntry;

    sget-object v0, Lcom/google/android/apps/plus/iu/MediaRecordEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v0, v11, v12}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NewMediaTracker:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/plus/iu/NewMediaTracker;->mTrackRecords:[Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    const-string v5, ";"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/iu/NewMediaTracker$TrackRecord;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method
