.class final Lcom/google/android/apps/plus/iu/PhotoCollectorJson;
.super Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser;
.source "PhotoCollectorJson.java"


# static fields
.field private static final sMediaContentFieldMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPhotoEntryFieldMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/16 v6, 0xa

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->sPhotoEntryFieldMap:Ljava/util/Map;

    sget-object v2, Lcom/google/android/apps/plus/iu/PhotoEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    sget-object v0, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->sPhotoEntryFieldMap:Ljava/util/Map;

    const-string v3, "gphoto$id"

    const-string v4, "_id"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "gphoto$albumid"

    const-string v4, "album_id"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "gphoto$timestamp"

    const-string v4, "date_taken"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "gphoto$width"

    const-string v4, "width"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "gphoto$height"

    const-string v4, "height"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "gphoto$size"

    const-string v4, "size"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "title"

    const-string v4, "title"

    invoke-virtual {v2, v4}, Lcom/android/gallery3d/common/EntrySchema;->getColumn(Ljava/lang/String;)Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->newObjectField(Lcom/android/gallery3d/common/EntrySchema$ColumnInfo;)Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "published"

    new-instance v4, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    const-string v5, "date_published"

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "updated"

    new-instance v4, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    const-string v5, "date_updated"

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "app$edited"

    new-instance v4, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    const-string v5, "date_edited"

    invoke-direct {v4, v5, v6}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "gphoto$streamId"

    new-instance v4, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    const/16 v5, 0xd

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;-><init>(I)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v3, "media$group"

    new-instance v4, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$NestedObjectField;

    invoke-direct {v4, v1}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$NestedObjectField;-><init>(Ljava/util/Map;)V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "media$content"

    new-instance v4, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    const/16 v5, 0xe

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;-><init>(I)V

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->sMediaContentFieldMap:Ljava/util/Map;

    const-string v4, "type"

    new-instance v5, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;

    const-string v6, "type"

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser;-><init>(Lcom/google/android/apps/plus/iu/PicasaApi$EntryHandler;)V

    return-void
.end method

.method private parseStreamIds(Lcom/google/android/apps/plus/json/JsonReader;Landroid/content/ContentValues;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/json/JsonReader;
    .param p2    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->beginArray()V

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "$t"

    invoke-virtual {p0, p1, v3}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->parseObject(Lcom/google/android/apps/plus/json/JsonReader;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->endArray()V

    invoke-static {v2}, Lcom/android/gallery3d/common/Fingerprint;->extractFingerprint(Ljava/util/List;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v3, "fingerprint"

    invoke-virtual {v0}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    const-string v3, "fingerprint_hash"

    invoke-virtual {v0}, Lcom/android/gallery3d/common/Fingerprint;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_2
    return-void
.end method


# virtual methods
.method protected final getEntryFieldMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->sPhotoEntryFieldMap:Ljava/util/Map;

    return-object v0
.end method

.method protected final handleComplexValue(Lcom/google/android/apps/plus/json/JsonReader;ILandroid/content/ContentValues;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/JsonReader;
    .param p2    # I
    .param p3    # Landroid/content/ContentValues;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    packed-switch p2, :pswitch_data_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->skipValue()V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->beginArray()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    sget-object v1, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->sMediaContentFieldMap:Ljava/util/Map;

    invoke-virtual {p0, p1, v1, v0}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->parseObject(Lcom/google/android/apps/plus/json/JsonReader;Ljava/util/Map;Landroid/content/ContentValues;)V

    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "content_type"

    invoke-virtual {p3, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/json/JsonReader;->endArray()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/plus/iu/PhotoCollectorJson;->parseStreamIds(Lcom/google/android/apps/plus/json/JsonReader;Landroid/content/ContentValues;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
