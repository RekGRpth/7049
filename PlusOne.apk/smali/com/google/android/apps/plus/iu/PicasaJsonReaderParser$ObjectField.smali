.class public Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;
.super Ljava/lang/Object;
.source "PicasaJsonReaderParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ObjectField"
.end annotation


# instance fields
.field final columnName:Ljava/lang/String;

.field final type:I


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;->columnName:Ljava/lang/String;

    iput p1, p0, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;->type:I

    const/16 v0, 0xa

    if-le p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;->columnName:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/apps/plus/iu/PicasaJsonReaderParser$ObjectField;->type:I

    const/16 v0, 0xa

    if-gt p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
