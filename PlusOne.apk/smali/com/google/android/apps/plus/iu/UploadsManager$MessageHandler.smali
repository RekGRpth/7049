.class final Lcom/google/android/apps/plus/iu/UploadsManager$MessageHandler;
.super Landroid/os/Handler;
.source "UploadsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/iu/UploadsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageHandler"
.end annotation


# direct methods
.method constructor <init>(Landroid/os/Looper;)V
    .locals 0
    .param p1    # Landroid/os/Looper;

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Landroid/database/Cursor;

    if-eqz v0, :cond_1

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings(Landroid/database/Cursor;)V

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->mSettings:Lcom/google/android/apps/plus/iu/UploadSettings;
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$1300(Lcom/google/android/apps/plus/iu/UploadsManager;)Lcom/google/android/apps/plus/iu/UploadSettings;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/iu/UploadSettings;->reloadSettings(Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_2
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->cancelTaskInternal(J)V
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2400(Lcom/google/android/apps/plus/iu/UploadsManager;J)V

    goto :goto_0

    :pswitch_3
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->sendUploadAllProgressBroadcast()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2500(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    goto :goto_0

    :pswitch_4
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->uploadExistingPhotosInternal(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2600(Lcom/google/android/apps/plus/iu/UploadsManager;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->cancelUploadExistingPhotosInternal(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2700(Lcom/google/android/apps/plus/iu/UploadsManager;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_6
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->onFsIdChangedInternal()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$000(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    goto :goto_0

    :pswitch_7
    const-string v0, "iu.UploadsManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "iu.UploadsManager"

    const-string v1, "Try to reset UploadsManager again!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    # getter for: Lcom/google/android/apps/plus/iu/UploadsManager;->sInstance:Lcom/google/android/apps/plus/iu/UploadsManager;
    invoke-static {}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$100()Lcom/google/android/apps/plus/iu/UploadsManager;

    move-result-object v0

    # invokes: Lcom/google/android/apps/plus/iu/UploadsManager;->reset()V
    invoke-static {v0}, Lcom/google/android/apps/plus/iu/UploadsManager;->access$2800(Lcom/google/android/apps/plus/iu/UploadsManager;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
