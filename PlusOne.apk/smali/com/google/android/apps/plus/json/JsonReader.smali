.class public final Lcom/google/android/apps/plus/json/JsonReader;
.super Ljava/lang/Object;
.source "JsonReader.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/json/JsonReader$1;
    }
.end annotation


# instance fields
.field private final buffer:[C

.field private final in:Ljava/io/Reader;

.field private lenient:Z

.field private limit:I

.field private name:Ljava/lang/String;

.field private pos:I

.field private skipping:Z

.field private final stack:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/json/JsonScope;",
            ">;"
        }
    .end annotation
.end field

.field private token:Lcom/google/android/apps/plus/json/JsonToken;

.field private value:Ljava/lang/String;

.field private valueLength:I

.field private valuePos:I


# direct methods
.method public constructor <init>(Ljava/io/Reader;)V
    .locals 2
    .param p1    # Ljava/io/Reader;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->lenient:Z

    const/16 v0, 0x400

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    sget-object v0, Lcom/google/android/apps/plus/json/JsonScope;->EMPTY_DOCUMENT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->push(Lcom/google/android/apps/plus/json/JsonScope;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "in == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/json/JsonReader;->in:Ljava/io/Reader;

    return-void
.end method

.method private advance()Lcom/google/android/apps/plus/json/JsonToken;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->name:Ljava/lang/String;

    return-object v0
.end method

.method private static decodeNumber([CII)Lcom/google/android/apps/plus/json/JsonToken;
    .locals 6
    .param p0    # [C
    .param p1    # I
    .param p2    # I

    const/16 v5, 0x2d

    const/16 v4, 0x39

    const/16 v3, 0x30

    move v1, p1

    aget-char v0, p0, p1

    if-ne v0, v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    :cond_0
    if-ne v0, v3, :cond_2

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    :cond_1
    const/16 v2, 0x2e

    if-ne v0, v2, :cond_4

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    :goto_0
    if-lt v0, v3, :cond_4

    if-gt v0, v4, :cond_4

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    goto :goto_0

    :cond_2
    const/16 v2, 0x31

    if-lt v0, v2, :cond_3

    if-gt v0, v4, :cond_3

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    :goto_1
    if-lt v0, v3, :cond_1

    if-gt v0, v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    :goto_2
    return-object v2

    :cond_4
    const/16 v2, 0x65

    if-eq v0, v2, :cond_5

    const/16 v2, 0x45

    if-ne v0, v2, :cond_9

    :cond_5
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    const/16 v2, 0x2b

    if-eq v0, v2, :cond_6

    if-ne v0, v5, :cond_7

    :cond_6
    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    :cond_7
    if-lt v0, v3, :cond_8

    if-gt v0, v4, :cond_8

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    :goto_3
    if-lt v0, v3, :cond_9

    if-gt v0, v4, :cond_9

    add-int/lit8 v1, v1, 0x1

    aget-char v0, p0, v1

    goto :goto_3

    :cond_8
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_2

    :cond_9
    add-int v2, p1, p2

    if-ne v1, v2, :cond_a

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_2

    :cond_a
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_2
.end method

.method private expect(Lcom/google/android/apps/plus/json/JsonToken;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/json/JsonToken;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v0, p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return-void
.end method

.method private fillBuffer(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    invoke-static {v2, v3, v4, v1, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->in:Ljava/io/Reader;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget-object v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    array-length v5, v5

    iget v6, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Ljava/io/Reader;->read([CII)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-lt v2, p1, :cond_0

    const/4 v1, 0x1

    :cond_1
    return v1

    :cond_2
    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    goto :goto_0
.end method

.method private getSnippet()Ljava/lang/CharSequence;
    .locals 6

    const/16 v5, 0x14

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v1

    invoke-virtual {v2, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v3, v4

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-virtual {v2, v3, v4, v0}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    return-object v2
.end method

.method private nextInArray(Z)Lcom/google/android/apps/plus/json/JsonToken;
    .locals 1
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/json/JsonScope;->NONEMPTY_ARRAY:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    const-string v0, "Unterminated array"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_0

    :sswitch_2
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :sswitch_3
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_0

    :cond_1
    :sswitch_4
    const-string v0, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2c -> :sswitch_4
        0x3b -> :sswitch_4
        0x5d -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_2
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method private nextInObject(Z)Lcom/google/android/apps/plus/json/JsonToken;
    .locals 2
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v1

    sparse-switch v1, :sswitch_data_1

    const-string v1, "Unterminated object"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->pop()Lcom/google/android/apps/plus/json/JsonScope;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_0

    :sswitch_2
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :sswitch_3
    int-to-char v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString(C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->name:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->DANGLING_NAME:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->NAME:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7d
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x2c -> :sswitch_0
        0x3b -> :sswitch_0
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private nextLiteral(Z)Ljava/lang/String;
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v3, -0x1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    iput v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    const/4 v1, 0x0

    :cond_0
    :goto_0
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v3, v1

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ge v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v4, v1

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :sswitch_0
    const-string v3, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    array-length v3, v3

    if-ge v1, v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    aput-char v5, v3, v4

    :goto_1
    :sswitch_1
    if-eqz p1, :cond_4

    if-nez v0, :cond_4

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    const/4 v2, 0x0

    :goto_2
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    return-object v2

    :cond_2
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_1

    :cond_4
    iget-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    if-eqz v3, :cond_5

    const-string v2, "skipped!"

    goto :goto_2

    :cond_5
    if-nez v0, :cond_6

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-direct {v2, v3, v4, v1}, Ljava/lang/String;-><init>([CII)V

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-virtual {v0, v3, v4, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xa -> :sswitch_1
        0xc -> :sswitch_1
        0xd -> :sswitch_1
        0x20 -> :sswitch_1
        0x23 -> :sswitch_0
        0x2c -> :sswitch_1
        0x2f -> :sswitch_0
        0x3a -> :sswitch_1
        0x3b -> :sswitch_0
        0x3d -> :sswitch_0
        0x5b -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
        0x7b -> :sswitch_1
        0x7d -> :sswitch_1
    .end sparse-switch
.end method

.method private nextNonWhitespace()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x1

    :sswitch_0
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-lt v1, v2, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    aget-char v0, v1, v2

    sparse-switch v0, :sswitch_data_0

    :cond_1
    return v0

    :sswitch_1
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ne v1, v2, :cond_2

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_2
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :sswitch_2
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :cond_3
    new-instance v1, Ljava/io/EOFException;

    const-string v2, "End of input"

    invoke-direct {v1, v2}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x20 -> :sswitch_0
        0x23 -> :sswitch_2
        0x2f -> :sswitch_1
    .end sparse-switch
.end method

.method private nextString(C)Ljava/lang/String;
    .locals 8
    .param p1    # C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v0, 0x0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    :cond_1
    :goto_0
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ge v3, v4, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    aget-char v1, v3, v4

    if-ne v1, p1, :cond_4

    iget-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    if-eqz v3, :cond_2

    const-string v3, "skipped!"

    :goto_1
    return-object v3

    :cond_2
    if-nez v0, :cond_3

    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v5, v2

    add-int/lit8 v5, v5, -0x1

    invoke-direct {v3, v4, v2, v5}, Ljava/lang/String;-><init>([CII)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_4
    const/16 v3, 0x5c

    if-ne v1, v3, :cond_1

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v2

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-ne v3, v4, :cond_6

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "Unterminated escape sequence"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    aget-char v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    :goto_2
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    goto :goto_0

    :sswitch_0
    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v3, v3, 0x4

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->limit:I

    if-le v3, v4, :cond_7

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "Unterminated escape sequence"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    :cond_7
    new-instance v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    invoke-direct {v3, v4, v5, v7}, Ljava/lang/String;-><init>([CII)V

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    int-to-char v3, v3

    goto :goto_2

    :sswitch_1
    const/16 v3, 0x9

    goto :goto_2

    :sswitch_2
    const/16 v3, 0x8

    goto :goto_2

    :sswitch_3
    const/16 v3, 0xa

    goto :goto_2

    :sswitch_4
    const/16 v3, 0xd

    goto :goto_2

    :sswitch_5
    const/16 v3, 0xc

    goto :goto_2

    :cond_8
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    sub-int/2addr v4, v2

    invoke-virtual {v0, v3, v2, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/json/JsonReader;->fillBuffer(I)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "Unterminated string"

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v3

    throw v3

    :sswitch_data_0
    .sparse-switch
        0x62 -> :sswitch_2
        0x66 -> :sswitch_5
        0x6e -> :sswitch_3
        0x72 -> :sswitch_4
        0x74 -> :sswitch_1
        0x75 -> :sswitch_0
    .end sparse-switch
.end method

.method private nextValue()Lcom/google/android/apps/plus/json/JsonToken;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v8, 0x55

    const/16 v7, 0x45

    const/4 v6, 0x4

    const/16 v5, 0x6c

    const/16 v4, 0x4c

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->pos:I

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->nextLiteral(Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    if-nez v1, :cond_0

    const-string v1, "Expected literal value"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :sswitch_0
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->EMPTY_OBJECT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->push(Lcom/google/android/apps/plus/json/JsonScope;)V

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    :goto_0
    return-object v1

    :sswitch_1
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->EMPTY_ARRAY:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->push(Lcom/google/android/apps/plus/json/JsonScope;)V

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_0

    :sswitch_2
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :sswitch_3
    int-to-char v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->nextString(C)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-ne v1, v2, :cond_12

    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :cond_1
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    if-ne v1, v6, :cond_6

    const/16 v1, 0x6e

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_2

    const/16 v1, 0x4e

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_6

    :cond_2
    const/16 v1, 0x75

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x1

    aget-char v1, v1, v2

    if-ne v8, v1, :cond_6

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-eq v5, v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-ne v4, v1, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x3

    aget-char v1, v1, v2

    if-eq v5, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x3

    aget-char v1, v1, v2

    if-ne v4, v1, :cond_6

    :cond_5
    const-string v1, "null"

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->NULL:Lcom/google/android/apps/plus/json/JsonToken;

    goto :goto_1

    :cond_6
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    if-ne v1, v6, :cond_b

    const/16 v1, 0x74

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_7

    const/16 v1, 0x54

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_b

    :cond_7
    const/16 v1, 0x72

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_8

    const/16 v1, 0x52

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_b

    :cond_8
    const/16 v1, 0x75

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x2

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-ne v8, v1, :cond_b

    :cond_9
    const/16 v1, 0x65

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x3

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x3

    aget-char v1, v1, v2

    if-ne v7, v1, :cond_b

    :cond_a
    const-string v1, "true"

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BOOLEAN:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_1

    :cond_b
    iget v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_11

    const/16 v1, 0x66

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_c

    const/16 v1, 0x46

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_11

    :cond_c
    const/16 v1, 0x61

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_d

    const/16 v1, 0x41

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x1

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_11

    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-eq v5, v1, :cond_e

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x2

    aget-char v1, v1, v2

    if-ne v4, v1, :cond_11

    :cond_e
    const/16 v1, 0x73

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x3

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_f

    const/16 v1, 0x53

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x3

    aget-char v2, v2, v3

    if-ne v1, v2, :cond_11

    :cond_f
    const/16 v1, 0x65

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v3, v3, 0x4

    aget-char v2, v2, v3

    if-eq v1, v2, :cond_10

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    add-int/lit8 v2, v2, 0x4

    aget-char v1, v1, v2

    if-ne v7, v1, :cond_11

    :cond_10
    const-string v1, "false"

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->BOOLEAN:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_1

    :cond_11
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    iget v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    iput-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->buffer:[C

    iget v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->valuePos:I

    iget v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->valueLength:I

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/json/JsonReader;->decodeNumber([CII)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v1

    goto/16 :goto_1

    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_3
        0x27 -> :sswitch_2
        0x5b -> :sswitch_1
        0x7b -> :sswitch_0
    .end sparse-switch
.end method

.method private pop()Lcom/google/android/apps/plus/json/JsonScope;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/json/JsonScope;

    return-object v0
.end method

.method private push(Lcom/google/android/apps/plus/json/JsonScope;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/json/JsonScope;

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/json/JsonScope;

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private syntaxError(Ljava/lang/String;)Ljava/io/IOException;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/json/MalformedJsonException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " near "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->getSnippet()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/json/MalformedJsonException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final beginArray()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    return-void
.end method

.method public final beginObject()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    return-void
.end method

.method public final close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->CLOSED:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->in:Ljava/io/Reader;

    invoke-virtual {v0}, Ljava/io/Reader;->close()V

    return-void
.end method

.method public final endArray()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    return-void
.end method

.method public final endObject()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/json/JsonReader;->expect(Lcom/google/android/apps/plus/json/JsonToken;)V

    return-void
.end method

.method public final hasNext()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v1, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nextBoolean()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BOOLEAN:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a boolean but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    const-string v2, "true"

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final nextDouble()D
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v3, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v3, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expected a double but was "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return-wide v0
.end method

.method public final nextInt()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v4, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v4, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expected an int but was "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return v2

    :catch_0
    move-exception v3

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-int v2, v0

    int-to-double v3, v2

    cmpl-double v3, v3, v0

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/NumberFormatException;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public final nextLong()J
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v5, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v5, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v4, v5, :cond_0

    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Expected a long but was "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return-wide v2

    :catch_0
    move-exception v4

    iget-object v4, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-long v2, v0

    long-to-double v4, v2

    cmpl-double v4, v4, v0

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/NumberFormatException;

    iget-object v5, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public final nextName()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NAME:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a name but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->name:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return-object v0
.end method

.method public final nextString()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->STRING:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NUMBER:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->NULL:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected a string but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/json/JsonReader;->peek()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->value:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    return-object v0
.end method

.method public final peek()Lcom/google/android/apps/plus/json/JsonToken;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/json/JsonReader$1;->$SwitchMap$com$google$android$apps$plus$json$JsonScope:[I

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->stack:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/json/JsonScope;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/json/JsonScope;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :pswitch_0
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->NONEMPTY_DOCUMENT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Expected JSON document to start with \'[\' or \'{\' but was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_1
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextInArray(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->nextInArray(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/json/JsonReader;->nextInObject(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextNonWhitespace()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    :pswitch_5
    const-string v1, "Expected \':\'"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :pswitch_6
    const-string v1, "Use JsonReader.setLenient(true) to accept malformed JSON"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1

    :pswitch_7
    sget-object v1, Lcom/google/android/apps/plus/json/JsonScope;->NONEMPTY_OBJECT:Lcom/google/android/apps/plus/json/JsonScope;

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->replaceTop(Lcom/google/android/apps/plus/json/JsonScope;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/json/JsonReader;->nextInObject(Z)Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->nextValue()Lcom/google/android/apps/plus/json/JsonToken;

    const-string v1, "Expected EOF"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/json/JsonReader;->syntaxError(Ljava/lang/String;)Ljava/io/IOException;

    move-result-object v1

    throw v1
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v1

    sget-object v0, Lcom/google/android/apps/plus/json/JsonToken;->END_DOCUMENT:Lcom/google/android/apps/plus/json/JsonToken;

    iput-object v0, p0, Lcom/google/android/apps/plus/json/JsonReader;->token:Lcom/google/android/apps/plus/json/JsonToken;

    goto/16 :goto_0

    :pswitch_a
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "JsonReader is closed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3a
        :pswitch_7
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final skipValue()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    const/4 v0, 0x0

    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->advance()Lcom/google/android/apps/plus/json/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->BEGIN_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    :goto_0
    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    return-void

    :cond_3
    :try_start_1
    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->END_ARRAY:Lcom/google/android/apps/plus/json/JsonToken;

    if-eq v1, v2, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/json/JsonToken;->END_OBJECT:Lcom/google/android/apps/plus/json/JsonToken;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-ne v1, v2, :cond_2

    :cond_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v2

    iput-boolean v3, p0, Lcom/google/android/apps/plus/json/JsonReader;->skipping:Z

    throw v2
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " near "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/JsonReader;->getSnippet()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
