.class final Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;
.super Lcom/google/android/maps/ItemizedOverlay;
.source "LocationsMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/locations/LocationsMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactOverlay"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/maps/ItemizedOverlay",
        "<",
        "Lcom/google/android/maps/OverlayItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final mItem:Lcom/google/android/maps/OverlayItem;

.field final synthetic this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p2    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    invoke-static {p2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/ItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Lcom/google/android/maps/OverlayItem;

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;
    invoke-static {p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$000(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;
    invoke-static {p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$100(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$200(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$200(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/OverlayItem;-><init>(Lcom/google/android/maps/GeoPoint;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;->mItem:Lcom/google/android/maps/OverlayItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;->populate()V

    return-void
.end method


# virtual methods
.method protected final createItem(I)Lcom/google/android/maps/OverlayItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;->mItem:Lcom/google/android/maps/OverlayItem;

    return-object v0
.end method

.method public final size()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
