.class final Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;
.super Lcom/google/android/apps/plus/fragments/ProfileLoader;
.source "LocationsMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/locations/LocationsMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyProfileLoader"
.end annotation


# instance fields
.field private final mLoaderPersonId:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    # getter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$400(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    iput-object p2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->mLoaderPersonId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final deliverResult(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    const/4 v0, 0x0

    const-wide v5, 0x412e848000000000L

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->isStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->deliverResult(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V

    iget-object v1, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profile:Lcom/google/api/services/plusi/model/SimpleProfile;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/SimpleProfile;->user:Lcom/google/api/services/plusi/model/User;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/User;->deviceLocations:Lcom/google/api/services/plusi/model/DeviceLocations;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DeviceLocations;->deviceLocation:Ljava/util/List;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/DeviceLocation;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v3, p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    # setter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$202(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->mLoaderPersonId:Ljava/lang/String;

    # setter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$702(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/DeviceLocation;->lat:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    # setter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;
    invoke-static {v2, v3}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$002(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/DeviceLocation;->lng:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    mul-double/2addr v3, v5

    double-to-int v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;
    invoke-static {v2, v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$102(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    # invokes: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->resetLocation()V
    invoke-static {v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$500(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->this$0:Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    # invokes: Lcom/google/android/apps/plus/locations/LocationsMapActivity;->onLocationLoadError()V
    invoke-static {v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->access$600(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)V

    goto :goto_0
.end method

.method public final bridge synthetic deliverResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->deliverResult(Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;)V

    return-void
.end method

.method public final onAbandon()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/ProfileLoader;->onAbandon()V

    return-void
.end method
