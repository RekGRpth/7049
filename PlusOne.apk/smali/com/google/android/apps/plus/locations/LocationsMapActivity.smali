.class public Lcom/google/android/apps/plus/locations/LocationsMapActivity;
.super Lcom/google/android/maps/MapActivity;
.source "LocationsMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;,
        Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mLastRequestId:Ljava/lang/Integer;

.field private mLatE6:Ljava/lang/Integer;

.field private mLngE6:Ljava/lang/Integer;

.field private mMapView:Lcom/google/android/maps/MapView;

.field private mName:Ljava/lang/String;

.field private mOverlay:Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

.field private mPersonId:Ljava/lang/String;

.field private mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

.field private mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/maps/MapActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/locations/LocationsMapActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$1;-><init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/locations/LocationsMapActivity;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->resetLocation()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->onLocationLoadError()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->prepareActionBar(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/locations/LocationsMapActivity;
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x1

    invoke-static {p0, v0, p1, v1}, Lcom/google/android/apps/plus/service/EsService;->getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->updateProgressIndicator()V

    return-void
.end method

.method private handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->onLocationLoadError()V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->updateProgressIndicator()V

    goto :goto_0
.end method

.method private onLocationLoadError()V
    .locals 2

    sget v0, Lcom/google/android/apps/plus/R$string;->locations_map_cannot_load_message:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private prepareActionBar(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->locations_map_default_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->updateRefreshButtonIcon(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    new-instance v2, Lcom/google/android/apps/plus/locations/LocationsMapActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$2;-><init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V

    move-object v0, p0

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    new-instance v2, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$3;-><init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    return-void
.end method

.method private resetLocation()V
    .locals 6

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getOverlays()Ljava/util/List;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mOverlay:Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mOverlay:Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mOverlay:Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_location_active:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

    invoke-direct {v3, p0, v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;-><init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Landroid/graphics/drawable/Drawable;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mOverlay:Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mOverlay:Lcom/google/android/apps/plus/locations/LocationsMapActivity$ContactOverlay;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-instance v5, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v5, v3, v4}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    iget-object v3, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getController()Lcom/google/android/maps/MapController;

    move-result-object v3

    const/16 v4, 0xe

    invoke-virtual {v3, v4}, Lcom/google/android/maps/MapController;->setZoom(I)I

    invoke-virtual {v3, v5}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;)V

    return-void
.end method

.method private updateProgressIndicator()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_0
.end method


# virtual methods
.method protected isRouteDisplayed()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onCreate(Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/R$layout;->locations_map_activity:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v2, Lcom/google/android/apps/plus/R$id;->title_bar:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/HostActionBar;

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    if-eqz p1, :cond_2

    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "person_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "lat"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "lng"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "name"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;

    const-string v2, "person_id"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    const-string v2, "lat"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    const-string v2, "lng"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    move v2, v4

    :goto_0
    if-nez v2, :cond_0

    if-eqz v1, :cond_3

    const-string v2, "display_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "person_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "lat"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "lng"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "display_name"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "person_id"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "lat"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "lng"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    move v2, v4

    :goto_1
    if-eqz v2, :cond_4

    :cond_0
    move v0, v4

    :goto_2
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;

    :goto_3
    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->prepareActionBar(Ljava/lang/String;)V

    sget v2, Lcom/google/android/apps/plus/R$id;->map_view:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/maps/MapView;

    iput-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2, v3}, Lcom/google/android/maps/MapView;->setTraffic(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2, v3}, Lcom/google/android/maps/MapView;->setStreetView(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2, v3}, Lcom/google/android/maps/MapView;->setSatellite(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2, v4}, Lcom/google/android/maps/MapView;->setBuiltInZoomControls(Z)V

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->resetLocation()V

    :cond_1
    return-void

    :cond_2
    move v2, v3

    goto/16 :goto_0

    :cond_3
    move v2, v3

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->onAbandon()V

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->reset()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->stopLoading()V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/maps/MapActivity;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;-><init>(Lcom/google/android/apps/plus/locations/LocationsMapActivity;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mProfileLoader:Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/locations/LocationsMapActivity$MyProfileLoader;->startLoading()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->handleProfileServiceCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->resetLocation()V

    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->onLocationLoadError()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/maps/MapActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "name"

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "person_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mPersonId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "lat"

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLatE6:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "lng"

    iget-object v1, p0, Lcom/google/android/apps/plus/locations/LocationsMapActivity;->mLngE6:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
