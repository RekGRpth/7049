.class public final Lcom/google/android/apps/plus/network/ClientVersion;
.super Ljava/lang/Object;
.source "ClientVersion.java"


# static fields
.field private static sCachedValue:Ljava/lang/Integer;


# direct methods
.method public static declared-synchronized from(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/apps/plus/network/ClientVersion;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/network/ClientVersion;->sCachedValue:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/network/ClientVersion;->getVersionCode(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/network/ClientVersion;->sCachedValue:Ljava/lang/Integer;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/network/ClientVersion;->sCachedValue:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getVersionCode(Landroid/content/Context;)I
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v3

    :catch_0
    move-exception v4

    goto :goto_0
.end method
