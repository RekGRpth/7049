.class final Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;
.super Ljava/io/FilterOutputStream;
.source "HttpTransaction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/network/HttpTransaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CountingOutputStream"
.end annotation


# instance fields
.field private final mChunk:J

.field private final mLength:J

.field private mNext:J

.field private final mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

.field private mTransferred:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/network/HttpTransaction;Ljava/io/OutputStream;J)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/network/HttpTransaction;
    .param p2    # Ljava/io/OutputStream;
    .param p3    # J

    invoke-direct {p0, p2}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    const-wide/16 v0, 0x2

    mul-long/2addr v0, p3

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    const-wide/16 v2, 0x5

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    return-void
.end method


# virtual methods
.method public final write(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1}, Ljava/io/FilterOutputStream;->write(I)V

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->access$000(Lcom/google/android/apps/plus/network/HttpTransaction;)Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    :cond_0
    return-void
.end method

.method public final write([BII)V
    .locals 4
    .param p1    # [B
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterOutputStream;->write([BII)V

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    iget-object v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransaction:Lcom/google/android/apps/plus/network/HttpTransaction;

    # getter for: Lcom/google/android/apps/plus/network/HttpTransaction;->mListener:Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/network/HttpTransaction;->access$000(Lcom/google/android/apps/plus/network/HttpTransaction;)Lcom/google/android/apps/plus/network/HttpTransaction$HttpTransactionListener;

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mTransferred:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mLength:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    iget-wide v2, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mChunk:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/plus/network/HttpTransaction$CountingOutputStream;->mNext:J

    :cond_0
    return-void
.end method
