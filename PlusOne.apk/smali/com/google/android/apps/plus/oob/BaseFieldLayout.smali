.class public abstract Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.super Landroid/widget/LinearLayout;
.source "BaseFieldLayout.java"


# instance fields
.field protected mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

.field protected mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

.field private mInputId:I

.field private mLabelId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 5
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    sget v2, Lcom/google/android/apps/plus/R$id;->label:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iput p2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mLabelId:I

    iget v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mLabelId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    add-int/lit8 p2, p2, 0x1

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->input:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iput p2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mInputId:I

    iget v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mInputId:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setId(I)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iput-object p3, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_2

    instance-of v3, v2, Landroid/widget/TextView;

    if-eqz v3, :cond_2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$style;->SignupErrorAppearance:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_2
    return-void
.end method

.method public final getActionType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->type:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getField()Lcom/google/api/services/plusi/model/OutOfBoxField;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    return-object v0
.end method

.method public final getInputView()Landroid/view/View;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mInputId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getLabelView()Landroid/widget/TextView;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mLabelId:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public final getServerBooleanValue()Ljava/lang/Boolean;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getServerDateValue()Lcom/google/api/services/plusi/model/MobileCoarseDate;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getServerImageType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->image:Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxImageField;->type:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getServerStringValue()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->stringValue:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getServerValue()Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
.end method

.method public setActionEnabled(Z)V
    .locals 0
    .param p1    # Z

    return-void
.end method

.method public final shouldPreventCompletionAction()Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-eqz v1, :cond_0

    const-string v1, "HIDDEN"

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
