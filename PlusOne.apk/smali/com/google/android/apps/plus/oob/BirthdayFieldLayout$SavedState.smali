.class public Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "BirthdayFieldLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final day:I

.field public final month:I

.field public final year:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->year:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->month:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->day:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;III)V
    .locals 0
    .param p1    # Landroid/os/Parcelable;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    iput p2, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->year:I

    iput p3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->month:I

    iput p4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->day:I

    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    iget v0, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->year:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->month:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->day:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
