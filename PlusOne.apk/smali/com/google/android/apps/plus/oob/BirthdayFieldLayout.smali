.class public Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "BirthdayFieldLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;
    }
.end annotation


# instance fields
.field private mInput:Landroid/widget/DatePicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 10
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    const/4 v7, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v6

    iget-object v2, v6, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v3

    iget-object v6, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, v2, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->style:Lcom/google/api/services/plusi/model/TextStyle;

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/oob/OobTextStyler;->applyStyle(Landroid/widget/TextView;Lcom/google/api/services/plusi/model/TextStyle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/DatePicker;

    iput-object v6, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getServerDateValue()Lcom/google/api/services/plusi/model/MobileCoarseDate;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    if-eqz v6, :cond_0

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_0
    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    add-int/lit8 v4, v6, -0x1

    :goto_1
    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    if-eqz v6, :cond_2

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v6, v5, v4, v1}, Landroid/widget/DatePicker;->updateDate(III)V

    :goto_3
    return-void

    :cond_0
    move v5, v7

    goto :goto_0

    :cond_1
    move v4, v7

    goto :goto_1

    :cond_2
    move v1, v7

    goto :goto_2

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    const/16 v8, 0x7b2

    const/4 v9, 0x1

    invoke-virtual {v6, v8, v7, v9}, Landroid/widget/DatePicker;->updateDate(III)V

    goto :goto_3
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v1

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-static {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    new-instance v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    new-instance v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/MobileCoarseDate;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;->year:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;->month:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/MobileCoarseDate;->day:Ljava/lang/Integer;

    iput-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->dateValue:Lcom/google/api/services/plusi/model/MobileCoarseDate;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    iget v2, v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->year:I

    iget v3, v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->month:I

    iget v4, v0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;->day:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/DatePicker;->updateDate(III)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    invoke-super {p0}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getMonth()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout;->mInput:Landroid/widget/DatePicker;

    invoke-virtual {v4}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v0

    new-instance v4, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;

    invoke-direct {v4, v2, v3, v1, v0}, Lcom/google/android/apps/plus/oob/BirthdayFieldLayout$SavedState;-><init>(Landroid/os/Parcelable;III)V

    return-object v4
.end method
