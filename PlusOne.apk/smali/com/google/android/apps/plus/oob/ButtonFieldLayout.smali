.class public Lcom/google/android/apps/plus/oob/ButtonFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "ButtonFieldLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mAction:Lcom/google/api/services/plusi/model/OutOfBoxAction;

.field private mInput:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v0

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iput-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mAction:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mInput:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mAction:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxAction;->text:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mInput:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mAction:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxAction;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mInput:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    iget-object v1, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mAction:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/oob/ActionCallback;->onAction(Lcom/google/api/services/plusi/model/OutOfBoxAction;)V

    return-void
.end method

.method public setActionEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/ButtonFieldLayout;->mInput:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method
