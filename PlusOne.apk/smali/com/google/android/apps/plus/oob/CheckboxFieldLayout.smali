.class public Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "CheckboxFieldLayout.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private mInput:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getInputView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v2

    iget-object v0, v2, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    iget-object v3, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->style:Lcom/google/api/services/plusi/model/TextStyle;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/oob/OobTextStyler;->applyStyle(Landroid/widget/TextView;Lcom/google/api/services/plusi/model/TextStyle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getServerBooleanValue()Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    if-eqz p3, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_1
    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mField:Lcom/google/api/services/plusi/model/OutOfBoxField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v1

    iget-object v1, v1, Lcom/google/api/services/plusi/model/OutOfBoxField;->input:Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-static {v1}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    move-result-object v0

    new-instance v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iget-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->value:Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;

    iget-object v2, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mInput:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/google/api/services/plusi/model/OutOfBoxFieldValue;->boolValue:Ljava/lang/Boolean;

    return-object v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/CheckboxFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-interface {v0}, Lcom/google/android/apps/plus/oob/ActionCallback;->onInputChanged$7c32a9fe()V

    return-void
.end method
