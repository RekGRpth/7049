.class public final Lcom/google/android/apps/plus/oob/OutOfBoxMessages;
.super Ljava/lang/Object;
.source "OutOfBoxMessages.java"


# direct methods
.method public static areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    if-nez p0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static copyWithoutValue(Lcom/google/api/services/plusi/model/OutOfBoxInputField;)Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 2
    .param p0    # Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxInputField;-><init>()V

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->type:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->label:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->mandatory:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->hasError:Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->valueOption:Ljava/util/List;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->valueOption:Ljava/util/List;

    iget-object v1, p0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->helpText:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/api/services/plusi/model/OutOfBoxInputField;->helpText:Ljava/lang/String;

    goto :goto_0
.end method

.method public static createOutOfBoxRequest(Ljava/lang/String;)Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;
    .locals 1
    .param p0    # Ljava/lang/String;

    new-instance v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    iput-object p0, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    return-object v0
.end method
