.class public Lcom/google/android/apps/plus/oob/TextFieldLayout;
.super Lcom/google/android/apps/plus/oob/BaseFieldLayout;
.source "TextFieldLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/oob/ActionTagHandler$Callback;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/OutOfBoxField;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/oob/BaseFieldLayout;->bindToField(Lcom/google/api/services/plusi/model/OutOfBoxField;ILcom/google/android/apps/plus/oob/ActionCallback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextFieldLayout;->getLabelView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextFieldLayout;->getField()Lcom/google/api/services/plusi/model/OutOfBoxField;

    move-result-object v4

    iget-object v4, v4, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    iget-object v3, v4, Lcom/google/api/services/plusi/model/OutOfBoxTextField;->text:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/oob/ActionTagHandler;->findActionIds(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/oob/TextFieldLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$color;->signup_action_link_color:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/apps/plus/oob/ActionTagHandler;

    invoke-direct {v5, v0, v2, p0}, Lcom/google/android/apps/plus/oob/ActionTagHandler;-><init>(Ljava/util/List;ILcom/google/android/apps/plus/oob/ActionTagHandler$Callback;)V

    invoke-static {v3, v4, v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v4

    sget-object v5, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/OutOfBoxField;->text:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/OutOfBoxTextField;->style:Lcom/google/api/services/plusi/model/TextStyle;

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/oob/OobTextStyler;->applyStyle(Landroid/widget/TextView;Lcom/google/api/services/plusi/model/TextStyle;)V

    return-void
.end method

.method public final isEmpty()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final newFieldFromInput()Lcom/google/api/services/plusi/model/OutOfBoxInputField;
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final onActionId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/TextFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/oob/TextFieldLayout;->mActionCallback:Lcom/google/android/apps/plus/oob/ActionCallback;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/oob/ActionCallback;->onActionId(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
