.class public final Lcom/google/android/apps/plus/phone/AlbumViewLoader;
.super Lcom/google/android/apps/plus/phone/PhotoCursorLoader;
.source "AlbumViewLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/AlbumViewLoader$PhotoQuery;
    }
.end annotation


# instance fields
.field private mExcludedCount:I

.field private mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # [Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x2

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v12, p8

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/String;)V

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 13

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->getLoaderUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->setUri(Landroid/net/Uri;)V

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v11, v11

    iput v11, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedCount:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v11, v11

    mul-int/lit8 v11, v11, 0x2

    invoke-direct {v10, v11}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    iget-object v11, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v11, v11

    if-ge v4, v11, :cond_2

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedMedia:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v8, v11, v4

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-virtual {v8}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    if-eqz v3, :cond_0

    const-string v11, " OR "

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v11, "(owner_id = ? AND photo_id = ?)"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-lez v11, :cond_3

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "SELECT photo_id FROM photo WHERE "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "photo_id NOT IN ("

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ")"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->setSelection(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    new-array v11, v11, [Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Ljava/lang/String;

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->setSelectionArgs([Ljava/lang/String;)V

    :cond_3
    sget-object v11, Lcom/google/android/apps/plus/phone/AlbumViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->setProjection([Ljava/lang/String;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/PhotoCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method public final getExcludedCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/AlbumViewLoader;->mExcludedCount:I

    return v0
.end method
