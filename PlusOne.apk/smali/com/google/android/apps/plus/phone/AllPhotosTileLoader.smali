.class public final Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "AllPhotosTileLoader.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/PageProducer;


# static fields
.field private static final TILES_PROJECTION:[Ljava/lang/String;

.field private static final TOKEN_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mViewId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "tile_id"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "collection_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "subtitle"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "image_url"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "color"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "plusone_count"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "comment_count"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "view_order"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->TILES_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "resume_token"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->TOKEN_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/apps/plus/content/EsTileData;->getViewNotification(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mViewId:Ljava/lang/String;

    return-void
.end method

.method private getStartFromNextCursor(Landroid/database/Cursor;I)I
    .locals 7
    .param p1    # Landroid/database/Cursor;
    .param p2    # I

    const/4 v2, -0x1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "start_order"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    :goto_0
    if-ne v1, v2, :cond_1

    :goto_1
    return v2

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const/4 v2, -0x1

    :try_start_0
    const-string v3, "SELECT view_order FROM all_tiles WHERE view_id = ? AND view_order < ?  ORDER BY view_order DESC  LIMIT ?, 1"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mViewId:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v3

    long-to-int v2, v3

    goto :goto_1

    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 12

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v10, Lcom/google/android/apps/plus/phone/PageableCursor;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/phone/PageableCursor;-><init>(Lcom/google/android/apps/plus/phone/PageProducer;)V

    const-string v1, "SELECT count(*) FROM all_tiles WHERE view_id = ?"

    new-array v2, v4, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mViewId:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {v0, v1, v2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v10, v1}, Lcom/google/android/apps/plus/phone/PageableCursor;->setCursorCount(I)V

    invoke-virtual {p0, v10, v7, v4}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->loadPage(Lcom/google/android/apps/plus/phone/PageableCursor;IZ)Z

    const-string v1, "tile_requests"

    sget-object v2, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->TOKEN_PROJECTION:[Ljava/lang/String;

    const-string v3, "view_id = ?"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mViewId:Ljava/lang/String;

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v11

    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    if-eqz v11, :cond_1

    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    const-string v1, "resume_token"

    invoke-virtual {v9, v1, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/phone/PageableCursor;->setExtras(Landroid/os/Bundle;)V

    return-object v10

    :cond_0
    move-object v11, v5

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_1
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public final loadPage(Lcom/google/android/apps/plus/phone/PageableCursor;IZ)Z
    .locals 18
    .param p1    # Lcom/google/android/apps/plus/phone/PageableCursor;
    .param p2    # I
    .param p3    # Z

    const-string v4, "Pageable"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Pageable"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#loadPage; page # "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getDatabaseHelper(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/EsDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/EsDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/plus/phone/PageableCursor;->getPageSize()I

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v16, -0x1

    if-lez p2, :cond_2

    add-int/lit8 v4, p2, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursor(I)Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "end_order"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v16

    :goto_0
    const/4 v4, -0x1

    move/from16 v0, v16

    if-ne v0, v4, :cond_1

    add-int/lit8 v4, p2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursor(I)Landroid/database/Cursor;

    move-result-object v4

    const/16 v5, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->getStartFromNextCursor(Landroid/database/Cursor;I)I

    move-result v16

    :cond_1
    const/4 v4, -0x1

    move/from16 v0, v16

    if-ne v0, v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    mul-int/lit8 v5, p2, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :cond_2
    const/4 v4, 0x2

    new-array v7, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->mViewId:Ljava/lang/String;

    aput-object v5, v7, v4

    const/4 v4, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v4

    const-string v4, "all_tiles"

    sget-object v5, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;->TILES_PROJECTION:[Ljava/lang/String;

    const-string v6, "view_id = ? AND view_order > ?"

    const/4 v8, 0x0

    const/4 v9, 0x0

    const-string v10, "view_order ASC"

    invoke-virtual/range {v3 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_4

    const/16 v4, 0xa

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    :goto_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToLast()Z

    move-result v4

    if-eqz v4, :cond_5

    const/16 v4, 0xa

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    :goto_2
    const/4 v4, -0x1

    invoke-interface {v12, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    new-instance v15, Landroid/os/Bundle;

    invoke-direct {v15}, Landroid/os/Bundle;-><init>()V

    const-string v4, "start_order"

    move/from16 v0, v17

    invoke-virtual {v15, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "end_order"

    invoke-virtual {v15, v4, v14}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v13, Lcom/google/android/apps/plus/phone/EsCursor;

    invoke-direct {v13, v12}, Lcom/google/android/apps/plus/phone/EsCursor;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v13, v15}, Lcom/google/android/apps/plus/phone/EsCursor;->setExtras(Landroid/os/Bundle;)V

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v13, v2}, Lcom/google/android/apps/plus/phone/PageableCursor;->setCursor(ILandroid/database/Cursor;Z)V

    if-eqz v12, :cond_6

    const/4 v4, 0x1

    :goto_3
    return v4

    :cond_3
    const/16 v16, -0x1

    goto/16 :goto_0

    :cond_4
    const/16 v17, -0x1

    goto :goto_1

    :cond_5
    const/4 v14, -0x1

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    goto :goto_3
.end method

.method public final requestPage(Lcom/google/android/apps/plus/phone/PageableCursor;IZ)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/phone/PageableCursor;
    .param p2    # I
    .param p3    # Z

    const/4 v3, 0x0

    const-string v0, "Pageable"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Pageable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#requestPage; page # "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader$1;

    invoke-direct {v0, p0, p1, p2, v3}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader$1;-><init>(Lcom/google/android/apps/plus/phone/AllPhotosTileLoader;Lcom/google/android/apps/plus/phone/PageableCursor;IZ)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Void;

    const/4 v2, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/AllPhotosTileLoader$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
