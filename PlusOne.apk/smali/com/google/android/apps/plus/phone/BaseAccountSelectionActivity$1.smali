.class final Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;
.super Ljava/lang/Object;
.source "BaseAccountSelectionActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final addAccount(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    iget-object v9, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    new-instance v0, Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v7, -0x1

    move-object v1, p1

    move-object v3, v2

    move v5, v4

    move v6, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZI)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getUpgradeOrigin()Ljava/lang/String;

    move-result-object v1

    invoke-static {v9, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->addAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    # setter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;
    invoke-static {v8, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$102(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(I)V

    return-void
.end method
