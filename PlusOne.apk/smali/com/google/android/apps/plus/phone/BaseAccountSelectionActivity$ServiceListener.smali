.class final Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;
.super Lcom/google/android/apps/plus/service/EsServiceListener;
.source "BaseAccountSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/EsServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    return-void
.end method


# virtual methods
.method public final onAccountAdded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # getter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$100(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # getter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$100(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->dismissDialog(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # getter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$200(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # getter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$200(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    :cond_0
    :goto_0
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # invokes: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$400(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;ILcom/google/android/apps/plus/content/EsAccount;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$102(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$302(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Z)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # invokes: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V
    invoke-static {v0, p3}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$500(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_1
.end method

.method public final onAccountUpgraded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # getter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$600(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    # getter for: Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$600(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->access$602(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->dismissDialog(I)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleUpgradeSuccess(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;->this$0:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleUpgradeFailure()V

    goto :goto_0
.end method
