.class public abstract Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "BaseAccountSelectionActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;,
        Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;
    }
.end annotation


# instance fields
.field private mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

.field private mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

.field private mAddAccountPendingRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mShowOnAttach:Z

.field private mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$ServiceListener;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    new-instance v0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$1;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Lcom/google/android/apps/plus/fragments/AccountsListFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mShowOnAttach:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;ILcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method private chooseAccountManually()V
    .locals 10

    const/4 v3, 0x1

    const/4 v9, 0x0

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v1, "allowSkip"

    invoke-virtual {v7, v1, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v1, "introMessage"

    sget v2, Lcom/google/android/apps/plus/R$string;->create_account_prompt:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    new-array v2, v3, [Ljava/lang/String;

    const-string v1, "com.google"

    aput-object v1, v2, v9

    const-string v5, "webupdates"

    move-object v1, v0

    move-object v4, v0

    move-object v6, v0

    invoke-static/range {v0 .. v7}, Landroid/accounts/AccountManager;->newChooseAccountIntent(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    :goto_0
    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    move-object v8, v0

    goto :goto_0
.end method

.method private handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x0

    instance-of v3, v2, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/api/OzServerException;->getUserErrorMessage(Landroid/content/Context;)Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;

    sget v3, Lcom/google/android/apps/plus/R$string;->signup_title_no_connection:I

    sget v4, Lcom/google/android/apps/plus/R$string;->signup_error_network:I

    invoke-direct {v1, p0, v3, v4}, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;-><init>(Landroid/content/Context;II)V

    :cond_1
    iget-object v3, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "error_title"

    iget-object v4, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->title:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    const-string v3, "error_message"

    iget-object v4, v1, Lcom/google/android/apps/plus/api/OzServerException$ErrorMessage;->message:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    return-void
.end method

.method private handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeIncompleteOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/EsService;->removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v1

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V

    goto :goto_0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getUpgradeOrigin()Ljava/lang/String;
    .locals 1

    const-string v0, "DEFAULT"

    return-object v0
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final handleUpgradeFailure()V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "error_title"

    sget v2, Lcom/google/android/apps/plus/R$string;->signup_title_no_connection:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "error_message"

    sget v2, Lcom/google/android/apps/plus/R$string;->signup_error_network:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(ILandroid/os/Bundle;)Z

    return-void
.end method

.method public final handleUpgradeSuccess(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getStreamActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->finish()V

    return-void
.end method

.method protected abstract onAccountSet(Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    if-eqz p3, :cond_0

    const-string v1, "authAccount"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;->addAccount(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->setAccountsAdder(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mShowOnAttach:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mShowOnAttach:Z

    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showAccountList()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 0
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showAccountList()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "aa_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "aa_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    :goto_0
    const-string v0, "ua_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ua_reqid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    goto :goto_0

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    goto :goto_1
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v2, 0x0

    const/4 v5, 0x0

    if-nez p2, :cond_0

    move-object v3, v2

    :goto_0
    if-nez p2, :cond_1

    move-object v1, v2

    :goto_1
    sparse-switch p1, :sswitch_data_0

    :goto_2
    return-object v2

    :cond_0
    const-string v4, "error_title"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    const-string v4, "error_message"

    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :sswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v0, v4, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_2

    :sswitch_1
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->signup_signing_in:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_2

    :sswitch_2
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/apps/plus/R$string;->signup_upgrading:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_2

    :sswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/apps/plus/R$string;->ok:I

    new-instance v5, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$2;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v4, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$3;-><init>(Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0xa -> :sswitch_1
        0xb -> :sswitch_2
    .end sparse-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setIntent(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v5, 0x0

    const/16 v4, 0xe

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_3

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->dismissDialog(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v1

    if-eqz v1, :cond_7

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleResponse(ILcom/google/android/apps/plus/content/EsAccount;)V

    :cond_2
    :goto_0
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_5

    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->dismissDialog(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-nez v1, :cond_8

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleUpgradeSuccess(Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_4
    :goto_1
    iput-object v5, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    :cond_5
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v4, :cond_6

    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->overridePendingTransition(II)V

    :cond_6
    return-void

    :cond_7
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleError(Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->handleUpgradeFailure()V

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "aa_reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "ua_reqid"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method

.method protected final showAccountList()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsListFragment:Lcom/google/android/apps/plus/fragments/AccountsListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AccountsListFragment;->showList()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->chooseAccountManually()V

    goto :goto_0
.end method

.method protected final showAccountSelectionOrUpgradeAccount(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/16 v3, 0xe

    const/16 v2, 0xb

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccountUnsafe(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isAccountUpgradeRequired(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mUpdateAccountIdPendingRequestId:Ljava/lang/Integer;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v3, :cond_3

    sget v1, Lcom/google/android/apps/plus/R$layout;->account_selection_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setContentView(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v2, :cond_2

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->showTitlebar(Z)V

    sget v1, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->setTitlebarTitle(Ljava/lang/String;)V

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$id;->info_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->signup_select_account_title:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->info_desc:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$string;->signup_select_account_desc:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAddAccountPendingRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    if-nez p1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasLoggedInThePast(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AccountsUtil;->getNumAccounts(Landroid/content/Context;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_6

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->hasVisitedOob(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0, v4}, Lcom/google/android/apps/plus/content/EsAccountsData;->setHasVisitedOob(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->finish()V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->mAccountsAdder:Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v3, "com.google"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v3, v1

    if-lez v3, :cond_5

    aget-object v1, v1, v4

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    :goto_1
    invoke-interface {v2, v1}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity$AccountsAdder;->addAccount(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/BaseAccountSelectionActivity;->chooseAccountManually()V

    goto :goto_0
.end method
