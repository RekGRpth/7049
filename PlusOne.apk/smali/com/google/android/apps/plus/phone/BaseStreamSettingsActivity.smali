.class public Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "BaseStreamSettingsActivity.java"


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mFragment:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CIRCLE_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;->mFragment:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;->mFragment:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;->mFragment:Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/BaseStreamSettingsFragment;->onDiscard()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/BaseStreamSettingsActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method
