.class public Lcom/google/android/apps/plus/phone/CameraAlbumLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "CameraAlbumLoader.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/Pageable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/CameraAlbumLoader$PhotoQuery;,
        Lcom/google/android/apps/plus/phone/CameraAlbumLoader$CorrectedMediaStoreColumn;
    }
.end annotation


# static fields
.field protected static final sImageStoreUri:[Landroid/net/Uri;

.field protected static final sMediaStoreUri:[Landroid/net/Uri;


# instance fields
.field private mExcludedCount:I

.field private mExcludedUris:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHasMore:Z

.field private mHideVideos:Z

.field private mLoadLimit:I

.field private mPageable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    const-string v1, "\'0\' as width"

    aput-object v1, v0, v5

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_IMAGES_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_VIDEO_URI:Landroid/net/Uri;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    new-array v0, v4, [Landroid/net/Uri;

    sget-object v1, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/plus/util/MediaStoreUtils;->PHONE_STORAGE_IMAGES_URI:Landroid/net/Uri;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sImageStoreUri:[Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;-><init>(Landroid/content/Context;ZI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/api/MediaRef;Z)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # [Lcom/google/android/apps/plus/api/MediaRef;
    .param p4    # Z

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    if-nez p3, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    :cond_0
    if-eqz p3, :cond_3

    array-length v0, p3

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashSet;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    :cond_1
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    aget-object v2, p3, v0

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iput-boolean p4, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHideVideos:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ZI)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Z
    .param p3    # I

    const/16 v0, 0x10

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    iput v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v1, :cond_0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method protected buildMatrixCursor(Landroid/content/Context;[Landroid/database/Cursor;[Landroid/net/Uri;)Landroid/database/Cursor;
    .locals 21
    .param p1    # Landroid/content/Context;
    .param p2    # [Landroid/database/Cursor;
    .param p3    # [Landroid/net/Uri;

    new-instance v10, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v18, Lcom/google/android/apps/plus/phone/AlbumViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedCount:I

    :goto_0
    const-wide/16 v12, -0x1

    const/4 v4, -0x1

    const/4 v7, 0x0

    :goto_1
    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v7, v0, :cond_2

    aget-object v2, p2, v7

    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v18

    if-nez v18, :cond_0

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v18

    if-eqz v18, :cond_1

    const-wide/16 v5, 0x0

    :goto_2
    cmp-long v18, v5, v12

    if-lez v18, :cond_0

    move-wide v12, v5

    move v4, v7

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    goto :goto_2

    :cond_2
    const/16 v18, -0x1

    move/from16 v0, v18

    if-eq v4, v0, :cond_6

    aget-object v3, p2, v4

    const/16 v18, 0x0

    :try_start_0
    move/from16 v0, v18

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    aget-object v18, p3, v4

    move-object/from16 v0, v18

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v15

    const/16 v18, 0x3

    move/from16 v0, v18

    invoke-interface {v3, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v18

    if-eqz v18, :cond_4

    const/16 v17, -0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedUris:Ljava/util/HashSet;

    move-object/from16 v18, v0

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    :cond_3
    const/16 v18, 0x2

    move/from16 v0, v18

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->toVideoDataBytes(Landroid/content/Context;Landroid/net/Uri;)[B

    move-result-object v16

    sget-object v18, Lcom/google/android/apps/plus/phone/AlbumViewLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    new-array v11, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v11, v18

    const/16 v18, 0x1

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0x2

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0x3

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0x4

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0x5

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0x6

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0x7

    aput-object v14, v11, v18

    const/16 v18, 0x8

    const-wide/16 v19, 0x0

    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v11, v18

    const/16 v18, 0x9

    invoke-virtual {v15}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    aput-object v19, v11, v18

    const/16 v18, 0xa

    const/16 v19, 0x0

    aput-object v19, v11, v18

    const/16 v18, 0xb

    aput-object v16, v11, v18

    const/16 v18, 0xd

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v19

    aput-object v19, v11, v18

    const/16 v18, 0xe

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v11, v18

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_4
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    goto/16 :goto_0

    :cond_4
    const/16 v18, 0x3

    :try_start_1
    move/from16 v0, v18

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    goto/16 :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedCount:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedCount:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception v18

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    throw v18

    :cond_6
    return-object v10
.end method

.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 17

    const/4 v12, 0x0

    const-string v13, "IUAlbumLoader"

    const/4 v14, 0x2

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_0

    new-instance v13, Lcom/google/android/apps/plus/util/StopWatch;

    invoke-direct {v13}, Lcom/google/android/apps/plus/util/StopWatch;-><init>()V

    invoke-virtual {v13}, Lcom/google/android/apps/plus/util/StopWatch;->start()Lcom/google/android/apps/plus/util/StopWatch;

    move-result-object v12

    const-string v13, "IUAlbumLoader"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "esLoadInBackground: BEGIN thread="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHideVideos:Z

    if-eqz v13, :cond_2

    sget-object v9, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sImageStoreUri:[Landroid/net/Uri;

    :goto_0
    array-length v13, v9

    new-array v4, v13, [Landroid/database/Cursor;

    :try_start_0
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    const/4 v5, 0x0

    :goto_1
    array-length v13, v9

    if-ge v5, v13, :cond_5

    aget-object v13, v9, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->setUri(Landroid/net/Uri;)V

    sget-object v13, Lcom/google/android/apps/plus/phone/CameraAlbumLoader$PhotoQuery;->PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->setProjection([Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v13, :cond_3

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "corrected_date_taken desc LIMIT 0, "

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->setSortOrder(Ljava/lang/String;)V

    invoke-super/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/EsCursorLoader;->esLoadInBackground()Landroid/database/Cursor;

    move-result-object v13

    aput-object v13, v4, v5

    aget-object v13, v4, v5

    if-eqz v13, :cond_4

    aget-object v13, v4, v5

    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v3

    aget-object v13, v4, v5

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_3
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    if-nez v13, :cond_1

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v13, :cond_1

    if-ne v3, v8, :cond_1

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    sget-object v9, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->sMediaStoreUri:[Landroid/net/Uri;

    goto :goto_0

    :cond_3
    :try_start_1
    const-string v11, "corrected_date_taken desc"

    goto :goto_2

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->getContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v4, v9}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->buildMatrixCursor(Landroid/content/Context;[Landroid/database/Cursor;[Landroid/net/Uri;)Landroid/database/Cursor;

    move-result-object v10

    const-string v13, "IUAlbumLoader"

    const/4 v14, 0x2

    invoke-static {v13, v14}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v13

    if-eqz v13, :cond_6

    const-string v13, "IUAlbumLoader"

    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "esLoadInBackground: END totalRows="

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", msec="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v12}, Lcom/google/android/apps/plus/util/StopWatch;->getElapsedMsec()J

    move-result-wide v15

    invoke-virtual/range {v14 .. v16}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", thread="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    move-object v1, v4

    array-length v7, v4

    const/4 v6, 0x0

    :goto_4
    if-ge v6, v7, :cond_a

    aget-object v2, v1, v6

    if-eqz v2, :cond_7

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :catchall_0
    move-exception v13

    move-object v1, v4

    array-length v7, v4

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v7, :cond_9

    aget-object v2, v1, v6

    if-eqz v2, :cond_8

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    :cond_9
    throw v13

    :cond_a
    return-object v10
.end method

.method public final getCurrentPage()I
    .locals 2

    const/4 v0, -0x1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    if-eq v1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    div-int/lit8 v0, v0, 0x10

    :cond_0
    return v0
.end method

.method public final getExcludedCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mExcludedCount:I

    return v0
.end method

.method public final hasMore()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mPageable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mHasMore:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isDataSourceLoading()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final loadMore()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    add-int/lit8 v0, v0, 0x30

    iput v0, p0, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->mLoadLimit:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CameraAlbumLoader;->onContentChanged()V

    :cond_0
    return-void
.end method

.method public final setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;

    return-void
.end method
