.class public Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "CirclesMembershipActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

.field private mPositiveButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private getPersonId()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "person_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isEmptySelectionAllowed()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "empty_selection_allowed"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONTACTS_CIRCLELIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setCircleUsageType(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setNewCircleEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setPersonId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->setOnCircleSelectionListener(Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment$OnCircleSelectionListener;)V

    :cond_0
    return-void
.end method

.method public final onCircleSelectionChange()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->isEmptySelectionAllowed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getSelectedCircleIds()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->ok:I

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getOriginalCircleIds()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mFragment:Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/CirclesMultipleSelectFragment;->getSelectedCircleIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "person_id"

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getPersonId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "display_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "display_name"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "suggestion_id"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "suggestion_id"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "original_circle_ids"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "selected_circle_ids"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v1, Lcom/google/android/apps/plus/R$id;->cancel:I

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->circle_selection_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setContentView(I)V

    sget v0, Lcom/google/android/apps/plus/R$string;->add_to_circles_dialog_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->setTitle(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->ok:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->isEmptySelectionAllowed()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->mPositiveButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->cancel:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/CirclesMembershipActivity;->finish()V

    :cond_0
    return-void
.end method
