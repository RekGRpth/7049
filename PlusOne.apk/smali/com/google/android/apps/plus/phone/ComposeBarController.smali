.class public final Lcom/google/android/apps/plus/phone/ComposeBarController;
.super Ljava/lang/Object;
.source "ComposeBarController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;
.implements Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;
    }
.end annotation


# static fields
.field private static sActionBarHeight:I

.field private static sOverlayDrawable:Landroid/graphics/drawable/Drawable;

.field private static sRecentImagesDefaultPadding:I

.field private static sRecentImagesDimension:I

.field private static sSelectorDrawable:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mAlwaysHide:Z

.field private mComposeBarListener:Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

.field private mCumulativeTouchSlop:I

.field private mCurrentOffset:F

.field private mCurrentTouchDelta:I

.field private mFloatingBarView:Landroid/view/View;

.field private mLandscape:Z

.field private mRecentImageRefs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentImageViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/ImageResourceView;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentImagesMargin:I

.field private mRecentImagesThatFitOnScreen:I

.field private final mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/view/View;ZLcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

    const/16 v7, 0xa

    const/4 v6, 0x0

    const/4 v9, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    iput v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    new-instance v5, Lcom/google/android/apps/plus/phone/ComposeBarController$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/phone/ComposeBarController$1;-><init>(Lcom/google/android/apps/plus/phone/ComposeBarController;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    iput-boolean v6, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mComposeBarListener:Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    check-cast v5, Lcom/google/android/apps/plus/views/ComposeBarView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/ComposeBarView;->setOnComposeBarMeasuredListener(Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;)V

    sget v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$drawable;->recent_images_border:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    sput-object v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sOverlayDrawable:Landroid/graphics/drawable/Drawable;

    sget v5, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    sput-object v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sSelectorDrawable:Landroid/graphics/drawable/Drawable;

    sget v5, Lcom/google/android/apps/plus/R$dimen;->compose_bar_recent_images_dimension:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sput v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    sget v5, Lcom/google/android/apps/plus/R$dimen;->compose_bar_recent_images_default_padding:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sput v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDefaultPadding:I

    sget v5, Lcom/google/android/apps/plus/R$dimen;->host_action_bar_height:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sput v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sActionBarHeight:I

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCumulativeTouchSlop:I

    const/16 v1, 0x9

    :goto_0
    if-ltz v1, :cond_1

    new-instance v2, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    sget v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    sget v6, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDefaultPadding:I

    sget v6, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDefaultPadding:I

    sget v7, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDefaultPadding:I

    sget v8, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDefaultPadding:I

    invoke-virtual {v2, v5, v6, v7, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setPadding(IIII)V

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    invoke-virtual {v2, v9}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    invoke-virtual {v2, v9}, Lcom/google/android/apps/plus/views/ImageResourceView;->setFadeIn(Z)V

    invoke-virtual {v2, v9}, Lcom/google/android/apps/plus/views/ImageResourceView;->setClickable(Z)V

    sget-object v5, Lcom/google/android/apps/plus/phone/ComposeBarController;->sSelectorDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/ComposeBarController;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ComposeBarController;

    iget v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/ComposeBarController;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    return p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/phone/ComposeBarController;F)F
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/ComposeBarController;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ComposeBarController;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/ComposeBarController;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ComposeBarController;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/ComposeBarController;Z)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/ComposeBarController;
    .param p1    # Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setFloatingBarVisible(Z)V

    return-void
.end method

.method private dismissRecentImages(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setRecentImageRefs(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mComposeBarListener:Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mComposeBarListener:Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;->onDismissRecentImages(Z)V

    :cond_0
    return-void
.end method

.method private getRecentImagesLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    sget v1, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    sget v2, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    const/high16 v3, 0x3f800000

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesMargin:I

    iget v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesMargin:I

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    :goto_0
    return-object v0

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesMargin:I

    iget v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesMargin:I

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method

.method private setFloatingBarVisible(Z)V
    .locals 4
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-ne v1, p1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_post:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_photos:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_location:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_custom:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_image_bar_share:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->compose_image_bar_close:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_3
    if-ltz v0, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setClickable(Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_3
    const/16 v2, 0x8

    goto :goto_2

    :cond_4
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestLayout()V

    goto :goto_1
.end method

.method private shouldShowRecentImages()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startAnimation(FI)V
    .locals 3
    .param p1    # F
    .param p2    # I

    const/4 v2, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    invoke-direct {v0, v1, p1, v2, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    :goto_0
    const-wide/16 v1, 0xc8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void

    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    invoke-direct {v0, v2, v2, v1, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    goto :goto_0
.end method

.method private updateBarView()V
    .locals 12

    const/16 v7, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v11, Lcom/google/android/apps/plus/R$id;->compose_image_bar:I

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    if-nez v10, :cond_1

    :cond_0
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->shouldShowRecentImages()Z

    move-result v6

    if-eqz v6, :cond_2

    move v7, v8

    :cond_2
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    sget v7, Lcom/google/android/apps/plus/R$id;->compose_image_container:I

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v10, Lcom/google/android/apps/plus/R$id;->compose_image_bar_close:I

    invoke-virtual {v7, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v6, :cond_3

    move-object v7, p0

    :goto_1
    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz v6, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iget v10, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    invoke-static {v7, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    invoke-virtual {v4, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v7, Lcom/google/android/apps/plus/phone/ComposeBarController;->sOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOverlay(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->getRecentImagesLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    invoke-virtual {v1, v4, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move-object v7, v9

    goto :goto_1

    :cond_4
    move v2, v5

    :goto_3
    iget v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    if-ge v2, v7, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    sget v7, Lcom/google/android/apps/plus/R$drawable;->empty_recent_image:I

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/ImageResourceView;->setBackgroundResource(I)V

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSelected(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->getRecentImagesLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v7

    invoke-virtual {v1, v4, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    iget v10, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    invoke-static {v7, v10}, Ljava/lang/Math;->max(II)I

    move-result v2

    :goto_4
    const/16 v7, 0xa

    if-ge v2, v7, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setBackgroundResource(I)V

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v8}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSelected(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual {v3}, Landroid/view/View;->invalidate()V

    goto/16 :goto_0
.end method

.method private updateShareButton()V
    .locals 7

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v1, 0x1

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    sget v6, Lcom/google/android/apps/plus/R$id;->compose_image_bar_share:I

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    if-eqz v1, :cond_4

    sget v4, Lcom/google/android/apps/plus/R$color;->compose_bar_share_button_enabled:I

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    invoke-virtual {v2}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setTextColor(I)V

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    sget v4, Lcom/google/android/apps/plus/R$color;->compose_bar_share_button_disabled:I

    goto :goto_2
.end method


# virtual methods
.method public final forceHide()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mAlwaysHide:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iput v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setFloatingBarVisible(Z)V

    return-void
.end method

.method public final forceShow()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mAlwaysHide:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setFloatingBarVisible(Z)V

    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v6, Lcom/google/android/apps/plus/R$id;->compose_image_bar_close:I

    if-ne v1, v6, :cond_0

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/ComposeBarController;->dismissRecentImages(Z)V

    :goto_0
    return-void

    :cond_0
    sget v6, Lcom/google/android/apps/plus/R$id;->compose_image_bar_share:I

    if-ne v1, v6, :cond_4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageViews:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mComposeBarListener:Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mComposeBarListener:Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;

    invoke-interface {v4, v2}, Lcom/google/android/apps/plus/phone/ComposeBarController$ComposeBarListener;->onShareRecentImages(Ljava/util/ArrayList;)V

    :cond_3
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/phone/ComposeBarController;->dismissRecentImages(Z)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v6

    if-nez v6, :cond_5

    :goto_2
    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateShareButton()V

    goto :goto_0

    :cond_5
    move v4, v5

    goto :goto_2
.end method

.method public final onComposeBarMeasured(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/phone/ComposeBarController;->sActionBarHeight:I

    sub-int v0, p2, v2

    :goto_0
    sget v2, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDefaultPadding:I

    sget v3, Lcom/google/android/apps/plus/phone/ComposeBarController;->sRecentImagesDimension:I

    add-int v1, v2, v3

    const/16 v2, 0xa

    div-int v3, v0, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    iget v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    if-lez v2, :cond_1

    iget v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    mul-int/2addr v2, v1

    sub-int v2, v0, v2

    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesThatFitOnScreen:I

    mul-int/lit8 v3, v3, 0x2

    div-int/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesMargin:I

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateBarView()V

    return-void

    :cond_0
    move v0, p1

    goto :goto_0

    :cond_1
    iput v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImagesMargin:I

    goto :goto_1
.end method

.method public final onScroll(Lcom/google/android/apps/plus/views/StreamGridView;II)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p2    # I
    .param p3    # I

    const/16 v5, 0xc8

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mAlwaysHide:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    if-gez p3, :cond_2

    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    if-gtz v3, :cond_3

    :cond_2
    if-lez p3, :cond_4

    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    if-gez v3, :cond_4

    :cond_3
    iput v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    :cond_4
    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    add-int/2addr v3, p3

    iput v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    if-lez v3, :cond_7

    move v0, v1

    :goto_1
    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    if-nez v3, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->shouldShowRecentImages()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/ComposeBarController;->dismissRecentImages(Z)V

    :cond_5
    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    iget v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCumulativeTouchSlop:I

    neg-int v4, v4

    if-le v3, v4, :cond_6

    iget v3, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    iget v4, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCumulativeTouchSlop:I

    if-lt v3, v4, :cond_0

    :cond_6
    if-eqz v0, :cond_8

    iget v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    iput v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setFloatingBarVisible(Z)V

    const/4 v1, 0x0

    invoke-direct {p0, v1, v5}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setFloatingBarVisible(Z)V

    goto :goto_0

    :cond_8
    iget v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->clearAnimation()V

    iput v2, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/ComposeBarController;->setFloatingBarVisible(Z)V

    goto :goto_0

    :pswitch_4
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mState:I

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mLandscape:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1, v5}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    goto :goto_0

    :cond_9
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1, v5}, Lcom/google/android/apps/plus/phone/ComposeBarController;->startAnimation(FI)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mFloatingBarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentOffset:F

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public final onTouchModeChanged(Lcom/google/android/apps/plus/views/StreamGridView;I)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p2    # I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mAlwaysHide:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mCurrentTouchDelta:I

    goto :goto_0
.end method

.method public final setRecentImageRefs(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ComposeBarController;->mRecentImageRefs:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateBarView()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/ComposeBarController;->updateShareButton()V

    return-void
.end method
