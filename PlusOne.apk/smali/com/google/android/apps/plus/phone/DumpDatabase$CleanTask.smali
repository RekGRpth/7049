.class final Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;
.super Landroid/os/AsyncTask;
.source "DumpDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/DumpDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CleanTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/DumpDatabase;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs doInBackground$10299ca()Ljava/lang/Void;
    .locals 12

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v3

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "es"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".db"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v4

    :goto_0
    :try_start_0
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v7, v0, v8}, Lcom/google/android/apps/plus/content/EsProvider;->cleanupData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    const-string v7, "DumpDatabase"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Clean complete; orig size: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", copy size: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$000(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/app/ProgressDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v7, 0x0

    return-object v7

    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_0

    :catchall_0
    move-exception v7

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v6

    const-string v8, "DumpDatabase"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Clean complete; orig size: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", copy size: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    throw v7
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/DumpDatabase$CleanTask;->doInBackground$10299ca()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
