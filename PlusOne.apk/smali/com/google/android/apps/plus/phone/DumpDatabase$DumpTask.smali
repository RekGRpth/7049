.class final Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;
.super Landroid/os/AsyncTask;
.source "DumpDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/DumpDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DumpTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mFromDbName:[Ljava/lang/String;

.field private mOriginalSize:[J

.field private mToDbName:[Ljava/lang/String;

.field private mTotalBytes:J

.field final synthetic this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/DumpDatabase;)V
    .locals 2

    const/4 v1, 0x4

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mToDbName:[Ljava/lang/String;

    new-array v0, v1, [J

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mOriginalSize:[J

    return-void
.end method

.method private doDump(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 15
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v1, 0x0

    new-instance v10, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-direct {v10, v11, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_0
    iget-object v11, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    :try_start_0
    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v11}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v2, Ljava/io/BufferedInputStream;

    new-instance v11, Ljava/io/FileInputStream;

    invoke-direct {v11, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v11}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/16 v11, 0x4000

    :try_start_2
    new-array v5, v11, [B

    :goto_0
    invoke-virtual {v2, v5}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_3

    const/4 v11, 0x0

    invoke-virtual {v4, v5, v11, v6}, Ljava/io/BufferedOutputStream;->write([BII)V

    iget-wide v11, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mTotalBytes:J

    int-to-long v13, v6

    add-long/2addr v11, v13

    iput-wide v11, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mTotalBytes:J

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$200(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/os/Handler;

    move-result-object v11

    invoke-virtual {v11}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v9

    iget-wide v11, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mTotalBytes:J

    long-to-int v11, v11

    iput v11, v9, Landroid/os/Message;->arg1:I

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$200(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/os/Handler;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catch_0
    move-exception v7

    move-object v1, v2

    move-object v3, v4

    :goto_1
    :try_start_3
    const-string v11, "DumpDatabase"

    const-string v12, "Exception copying database; destination may not be complete."

    invoke-static {v11, v12, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v3, :cond_1

    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_1
    :goto_2
    if-eqz v1, :cond_2

    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    :cond_2
    :goto_3
    return-object v10

    :cond_3
    :try_start_6
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :goto_4
    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    move-object v1, v2

    move-object v3, v4

    goto :goto_3

    :catch_1
    move-exception v11

    move-object v1, v2

    move-object v3, v4

    goto :goto_3

    :catchall_0
    move-exception v11

    :goto_5
    if-eqz v3, :cond_4

    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_4
    :goto_6
    if-eqz v1, :cond_5

    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    :cond_5
    :goto_7
    throw v11

    :catch_2
    move-exception v11

    goto :goto_4

    :catch_3
    move-exception v11

    goto :goto_2

    :catch_4
    move-exception v11

    goto :goto_3

    :catch_5
    move-exception v12

    goto :goto_6

    :catch_6
    move-exception v12

    goto :goto_7

    :catchall_1
    move-exception v11

    move-object v3, v4

    goto :goto_5

    :catchall_2
    move-exception v11

    move-object v1, v2

    move-object v3, v4

    goto :goto_5

    :catch_7
    move-exception v7

    goto :goto_1

    :catch_8
    move-exception v7

    move-object v3, v4

    goto :goto_1
.end method


# virtual methods
.method protected final bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # [Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mOriginalSize:[J

    aget-wide v1, v1, v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mToDbName:[Ljava/lang/String;

    aget-object v4, v4, v0

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-nez v5, :cond_0

    const-string v1, "DumpDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Could not find database: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, v3, v4}, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->doDump(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    const-string v4, "DumpDatabase"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Dump complete; orig size: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", copy size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$000(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final onPreExecute()V
    .locals 14

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getIndex()I

    move-result v4

    const-wide/16 v5, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "es"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".db"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    const-string v8, "picasa.db"

    aput-object v8, v7, v11

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    const-string v8, "iu.picasa.db"

    aput-object v8, v7, v12

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    const-string v8, "iu.upload.db"

    aput-object v8, v7, v13

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mToDbName:[Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "es"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "_dump.bin"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mToDbName:[Ljava/lang/String;

    const-string v8, "picasa_dump.bin"

    aput-object v8, v7, v11

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mToDbName:[Ljava/lang/String;

    const-string v8, "iu.picasa_dump.bin"

    aput-object v8, v7, v12

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mToDbName:[Ljava/lang/String;

    const-string v8, "iu.upload_dump.bin"

    aput-object v8, v7, v13

    const/4 v2, 0x0

    :goto_0
    const/4 v7, 0x4

    if-ge v2, v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mFromDbName:[Ljava/lang/String;

    aget-object v1, v7, v2

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$100(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mOriginalSize:[J

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v8

    aput-wide v8, v7, v2

    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->mOriginalSize:[J

    aget-wide v7, v7, v2

    add-long/2addr v5, v7

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/DumpDatabase$DumpTask;->this$0:Lcom/google/android/apps/plus/phone/DumpDatabase;

    # getter for: Lcom/google/android/apps/plus/phone/DumpDatabase;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v7}, Lcom/google/android/apps/plus/phone/DumpDatabase;->access$000(Lcom/google/android/apps/plus/phone/DumpDatabase;)Landroid/app/ProgressDialog;

    move-result-object v7

    long-to-int v8, v5

    invoke-virtual {v7, v8}, Landroid/app/ProgressDialog;->setMax(I)V

    return-void
.end method
