.class public Lcom/google/android/apps/plus/phone/EditAudienceActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EditAudienceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;


# instance fields
.field private mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

.field private mPositiveButton:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    if-nez p1, :cond_2

    const/4 v4, -0x1

    if-ne p2, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v4, :cond_1

    const-string v4, "person_id"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v4, "person_data"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/PersonData;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addSelectedPerson(Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V

    :cond_0
    const-string v4, "circle_id"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v4, "circle_data"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/CircleData;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->addSelectedCircle(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 5
    .param p1    # Landroid/support/v4/app/Fragment;

    const/4 v4, 0x1

    const/4 v3, 0x0

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setOnSelectionChangeListener(Lcom/google/android/apps/plus/fragments/EditAudienceFragment$OnAudienceChangeListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setCircleSelectionEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "circle_usage_type"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setCircleUsageType(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "search_plus_pages_enabled"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setIncludePlusPages(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "filter_null_gaia_ids"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setFilterNullGaiaIds(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience_is_read_only"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setIncomingAudienceIsReadOnly(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "empty_selection_allowed"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAllowEmptyAudience(Z)V

    :cond_0
    return-void
.end method

.method public final onAudienceChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isSelectionValid()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v2, Lcom/google/android/apps/plus/R$id;->ok:I

    if-ne v0, v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "audience"

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->cancel:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$layout;->edit_audience_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->showTitlebar(ZZ)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->setTitlebarTitle(Ljava/lang/String;)V

    sget v1, Lcom/google/android/apps/plus/R$menu;->edit_audience_menu:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->createTitlebarButtons(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->ok:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->isSelectionValid()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mPositiveButton:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->cancel:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->edit_audience_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x102002c

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    :goto_0
    return v1

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->search:I

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->onSearchRequested()Z

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    sget v0, Lcom/google/android/apps/plus/R$id;->search:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/Menu;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method protected onResume()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->isIntentAccountActive()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->hasAudience()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "audience"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->mEditAudienceFragment:Lcom/google/android/apps/plus/fragments/EditAudienceFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/EditAudienceFragment;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->finish()V

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 10

    const/4 v2, 0x1

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_phones_enabled"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_plus_pages_enabled"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_pub_profiles_enabled"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "circle_usage_type"

    const/4 v7, -0x1

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v7, "filter_null_gaia_ids"

    invoke-virtual {v0, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    move-object v0, p0

    move v7, v2

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getPeopleSearchActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZIZZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v9}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return v2
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EditAudienceActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
