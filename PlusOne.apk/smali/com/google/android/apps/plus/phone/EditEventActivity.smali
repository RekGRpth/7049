.class public Lcom/google/android/apps/plus/phone/EditEventActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EditEventActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;


# instance fields
.field private mAuthKey:Ljava/lang/String;

.field private mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

.field private mEventId:Ljava/lang/String;

.field private mOwnerId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/EditEventActivity;)Lcom/google/android/apps/plus/fragments/EditEventFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/EditEventActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    return-object v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->save:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 4
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEventId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mOwnerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mAuthKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->editEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setOnEventChangedListener(Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onDiscard()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "event_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mEventId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "owner_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "auth_key"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EditEventActivity;->mAuthKey:Ljava/lang/String;

    sget v2, Lcom/google/android/apps/plus/R$layout;->new_event_activity:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditEventActivity;->setContentView(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->cancel_button:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/phone/EditEventActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/EditEventActivity$1;-><init>(Lcom/google/android/apps/plus/phone/EditEventActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->share_button:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EditEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageTextButton;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$string;->save:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageTextButton;->setText(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/apps/plus/phone/EditEventActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/EditEventActivity$2;-><init>(Lcom/google/android/apps/plus/phone/EditEventActivity;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public final onEventClosed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    return-void
.end method

.method public final onEventSaved(Lcom/google/api/services/plusi/model/PlusEvent;)V
    .locals 0
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EditEventActivity;->onBackPressed()V

    return-void
.end method
