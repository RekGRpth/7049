.class public abstract Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;
.super Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;
.source "EsCursorPagerAdapter.java"


# instance fields
.field mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mDataValid:Z

.field private mItemPosition:Landroid/util/SparseIntArray;

.field private mObjectRowMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRowIDColumn:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/app/FragmentManager;
    .param p3    # Landroid/database/Cursor;

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mObjectRowMap:Ljava/util/HashMap;

    if-eqz p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-object p3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mRowIDColumn:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private moveCursorTo(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setItemPosition()V
    .locals 5

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mItemPosition:Landroid/util/SparseIntArray;

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/SparseIntArray;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-direct {v0, v3}, Landroid/util/SparseIntArray;-><init>(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    const/4 v4, -0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    iget v4, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mRowIDColumn:I

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/util/SparseIntArray;->append(II)V

    goto :goto_1

    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mItemPosition:Landroid/util/SparseIntArray;

    goto :goto_0
.end method


# virtual methods
.method public final destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mObjectRowMap:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->destroyItem(Landroid/view/View;ILjava/lang/Object;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCursor()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->moveCursorTo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getItem$2282a066(Landroid/database/Cursor;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getItem$2282a066(Landroid/database/Cursor;)Landroid/support/v4/app/Fragment;
.end method

.method public final getItemPosition(Ljava/lang/Object;)I
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v0, -0x2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mObjectRowMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mItemPosition:Landroid/util/SparseIntArray;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mItemPosition:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    goto :goto_0
.end method

.method public final instantiateItem(Landroid/view/View;I)Ljava/lang/Object;
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when the cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->moveCursorTo(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    iget v3, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mRowIDColumn:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->instantiateItem(Landroid/view/View;I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mObjectRowMap:Ljava/util/HashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final isDataValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    return v0
.end method

.method protected final makeFragmentName(II)Ljava/lang/String;
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->moveCursorTo(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:espager:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    iget v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mRowIDColumn:I

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->makeFragmentName(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 5
    .param p1    # Landroid/database/Cursor;

    const/4 v2, -0x1

    const-string v1, "EsCursorPagerAdapter"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v3, "EsCursorPagerAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "swapCursor old="

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "; new="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p1, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    if-ne p1, v1, :cond_3

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz p1, :cond_4

    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mRowIDColumn:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    :goto_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->setItemPosition()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->notifyDataSetChanged()V

    goto :goto_2

    :cond_4
    iput v2, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mRowIDColumn:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->mDataValid:Z

    goto :goto_3
.end method
