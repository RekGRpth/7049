.class final Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;
.super Landroid/support/v4/util/LruCache;
.source "EsFragmentPagerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FragmentCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/support/v4/app/Fragment;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;->this$0:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Landroid/support/v4/util/LruCache;-><init>(I)V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic entryRemoved(ZLjava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/Object;
    .param p3    # Ljava/lang/Object;
    .param p4    # Ljava/lang/Object;

    check-cast p3, Landroid/support/v4/app/Fragment;

    check-cast p4, Landroid/support/v4/app/Fragment;

    if-nez p1, :cond_0

    if-eqz p4, :cond_1

    if-eq p3, p4, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter$FragmentCache;->this$0:Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;

    # getter for: Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->mCurTransaction:Landroid/support/v4/app/FragmentTransaction;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;->access$000(Lcom/google/android/apps/plus/phone/EsFragmentPagerAdapter;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    :cond_1
    return-void
.end method
