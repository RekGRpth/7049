.class public final Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;
.super Ljava/lang/Object;
.source "EsMatrixCursor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/EsMatrixCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RowBuilder"
.end annotation


# instance fields
.field private final endIndex:I

.field private index:I

.field final synthetic this$0:Lcom/google/android/apps/plus/phone/EsMatrixCursor;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/EsMatrixCursor;II)V
    .locals 0
    .param p2    # I
    .param p3    # I

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->this$0:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->index:I

    iput p3, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->endIndex:I

    return-void
.end method


# virtual methods
.method public final add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;
    .locals 3
    .param p1    # Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->index:I

    iget v1, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->endIndex:I

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "No more columns left."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->this$0:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    # getter for: Lcom/google/android/apps/plus/phone/EsMatrixCursor;->data:[Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->access$000(Lcom/google/android/apps/plus/phone/EsMatrixCursor;)[Ljava/lang/Object;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->index:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->index:I

    aput-object p1, v0, v1

    return-object p0
.end method
