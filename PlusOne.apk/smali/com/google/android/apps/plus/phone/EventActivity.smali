.class public Lcom/google/android/apps/plus/phone/EventActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "EventActivity.java"


# instance fields
.field private mCurrentSpinnerIndex:I

.field private mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    new-instance v1, Landroid/widget/ArrayAdapter;

    sget v2, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v1, p0, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    const v2, 0x1090009

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    sget v2, Lcom/google/android/apps/plus/R$string;->event_tab_event_text:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mPrimarySpinnerAdapter:Landroid/widget/ArrayAdapter;

    sget v2, Lcom/google/android/apps/plus/R$string;->event_tab_photos_text:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "notif_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 5
    .param p1    # I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onPrimarySpinnerSelectionChange(I)V

    iget v3, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    if-ne v3, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    const/4 v2, 0x0

    :cond_2
    :goto_1
    if-eqz v2, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventActivity;->replaceFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :pswitch_0
    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedEventFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedEventFragment;-><init>()V

    goto :goto_1

    :pswitch_1
    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->EVENTS_VIEW_PHOTOS_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/EventActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    new-instance v2, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {v2}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "event_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v3, "event_id"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "spinnerIndex"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "spinnerIndex"

    iget v1, p0, Lcom/google/android/apps/plus/phone/EventActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
