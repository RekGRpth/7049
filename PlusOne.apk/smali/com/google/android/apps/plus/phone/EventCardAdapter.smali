.class public final Lcom/google/android/apps/plus/phone/EventCardAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "EventCardAdapter.java"


# static fields
.field private static sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field protected mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;Lcom/google/android/apps/plus/views/ColumnGridView;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/database/Cursor;
    .param p4    # Landroid/view/View$OnClickListener;
    .param p5    # Lcom/google/android/apps/plus/views/ItemClickListener;
    .param p6    # Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    sget-object v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    :cond_0
    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    sget-object v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v2, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v3, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p6, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    new-instance v0, Lcom/google/android/apps/plus/phone/EventCardAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/EventCardAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/EventCardAdapter;)V

    invoke-virtual {p6, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    check-cast p1, Landroid/widget/TextView;

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$string;->events_section_upcoming:I

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->events_section_past:I

    goto :goto_1

    :pswitch_1
    check-cast p1, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    const/4 v0, 0x3

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    sget-object v1, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->init$51b687e2(ILandroid/view/View$OnClickListener;Lcom/google/android/apps/plus/views/ItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventCardAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->bindData$5ae0320(Lcom/google/api/services/plusi/model/PlusEvent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 3
    .param p1    # I

    const/4 v1, 0x1

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/EventCardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getItemViewType(I)I

    move-result v1

    :goto_0
    :pswitch_0
    return v1

    :pswitch_1
    const/4 v1, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v9, 0x2

    const/4 v8, -0x2

    const/4 v7, -0x3

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-object v2

    :pswitch_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x9

    invoke-static {p1, v3, v4, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextView(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v2

    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->getColumnCount()I

    move-result v3

    invoke-direct {v1, v9, v7, v3, v6}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    iput v8, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :pswitch_1
    new-instance v2, Lcom/google/android/apps/plus/views/EventDestinationCardView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/EventDestinationCardView;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    invoke-direct {v1, v9, v7, v6, v6}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(IIII)V

    sget-object v3, Lcom/google/android/apps/plus/phone/EventCardAdapter;->sMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v3, :cond_0

    iput v8, v1, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    :cond_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
