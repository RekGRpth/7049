.class public final Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;
.super Lcom/android/common/widget/EsCompositeCursorAdapter;
.source "EventDetailsActivityAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;
    }
.end annotation


# static fields
.field private static sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field private mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

.field private mResolvedPeople:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;",
            ">;"
        }
    .end annotation
.end field

.field private mViewUseListener:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;

.field private mWrapContent:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/views/ColumnGridView;Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/views/ColumnGridView;
    .param p3    # Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;
    .param p4    # Lcom/google/android/apps/plus/views/EventActionListener;

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/common/widget/EsCompositeCursorAdapter;-><init>(Landroid/content/Context;B)V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->addPartition(ZZ)V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->addPartition(ZZ)V

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    sget-object v3, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-nez v3, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    :cond_0
    sget-object v3, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v3, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mWrapContent:Z

    invoke-virtual {p2, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v0, :cond_2

    :goto_0
    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setItemMargin(I)V

    sget-object v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v0, v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v1, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v1, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v2, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v2, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    sget-object v3, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->sScreenMetrics:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    iget v3, v3, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setPadding(IIII)V

    new-instance v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$1;-><init>(Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;)V

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method protected final bindView(Landroid/view/View;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)V
    .locals 16
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    :try_start_0
    move-object/from16 v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;

    move-object v10, v0

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v12

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v13

    const/4 v14, 0x0

    if-eqz v13, :cond_2

    invoke-static {v13}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v14

    :cond_2
    if-eqz v11, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual {v10, v11, v14, v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/DbPlusOneData;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v2, "EventDetailsAdapter"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "EventDetailsAdapter"

    const-string v3, "[bindView] stale cursor"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_1
    const/4 v2, 0x1

    :try_start_1
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    sparse-switch v15, :sswitch_data_0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mViewUseListener:Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;

    move/from16 v0, p4

    invoke-interface {v2, v0}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter$ViewUseListener;->onViewUsed(I)V

    goto :goto_0

    :sswitch_0
    move-object/from16 v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;

    move-object v1, v0

    new-instance v3, Lcom/google/android/apps/plus/views/EventUpdate;

    invoke-direct {v3}, Lcom/google/android/apps/plus/views/EventUpdate;-><init>()V

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/plus/views/EventUpdate;->gaiaId:Ljava/lang/String;

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/plus/views/EventUpdate;->ownerName:Ljava/lang/String;

    const/4 v2, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/apps/plus/views/EventUpdate;->timestamp:J

    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/android/apps/plus/views/EventUpdate;->comment:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mWrapContent:Z

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v1, v3, v4, v2}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->bind(Lcom/google/android/apps/plus/views/EventUpdate;Lcom/google/android/apps/plus/views/EventActionListener;Z)V

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :sswitch_1
    move-object/from16 v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;

    move-object v1, v0

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v4, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    const/4 v4, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;->bind(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;[BLcom/google/android/apps/plus/views/EventActionListener;Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_2
    move-object/from16 v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;

    move-object v1, v0

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    sget-object v5, Lcom/google/android/apps/plus/content/EsEventData;->EVENT_COALESCED_FRAME_JSON:Lcom/google/android/apps/plus/json/EsJson;

    const/4 v6, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/json/EsJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;

    iget-object v5, v5, Lcom/google/android/apps/plus/content/EsEventData$EventCoalescedFrame;->people:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mResolvedPeople:Ljava/util/HashMap;

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move v8, v6

    :goto_3
    if-ltz v8, :cond_5

    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;

    iget-object v7, v6, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    if-eqz v7, :cond_4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mResolvedPeople:Ljava/util/HashMap;

    iget-object v9, v6, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->gaiaId:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;

    if-eqz v7, :cond_4

    iget-object v7, v7, Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;->name:Ljava/lang/String;

    iput-object v7, v6, Lcom/google/android/apps/plus/content/EsEventData$EventPerson;->name:Ljava/lang/String;

    :cond_4
    add-int/lit8 v6, v8, -0x1

    move v8, v6

    goto :goto_3

    :cond_5
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;->bind(IJLjava/util/List;Lcom/google/android/apps/plus/views/EventActionListener;)V
    :try_end_1
    .catch Landroid/database/StaleDataException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public final changeActivityCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeCursor(ILandroid/database/Cursor;)V

    return-void
.end method

.method public final changeInfoCursor(Landroid/database/Cursor;Lcom/google/android/apps/plus/fragments/EventActiveState;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/fragments/EventActiveState;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->changeCursor(ILandroid/database/Cursor;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    return-void
.end method

.method protected final getItemViewType(II)I
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x1

    const/4 v2, 0x4

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return v2

    :pswitch_0
    const/4 v2, 0x0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->getCursor(I)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const/4 v2, 0x3

    goto :goto_0

    :sswitch_1
    const/4 v2, 0x2

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x5

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final isWrapContentEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mWrapContent:Z

    return v0
.end method

.method protected final newView(Landroid/content/Context;ILandroid/database/Cursor;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # I
    .param p5    # Landroid/view/ViewGroup;

    const/4 v1, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mWrapContent:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Landroid/database/StaleDataException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_1
    :goto_1
    sparse-switch v2, :sswitch_data_0

    :goto_2
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v3, "EventDetailsAdapter"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "EventDetailsAdapter"

    const-string v4, "[newView] stale cursor"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :sswitch_0
    new-instance v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;-><init>(Landroid/content/Context;)V

    goto :goto_2

    :sswitch_1
    new-instance v1, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventActivityPhotoCardLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    goto :goto_2

    :sswitch_2
    new-instance v1, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventActivityFrameCardLayout;-><init>(Landroid/content/Context;)V

    goto :goto_2

    :sswitch_3
    new-instance v1, Lcom/google/android/apps/plus/views/CardViewLayout;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_0
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public final setResolvedPeople(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/content/EsEventData$ResolvedPerson;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/EventDetailsActivityAdapter;->mResolvedPeople:Ljava/util/HashMap;

    return-void
.end method
