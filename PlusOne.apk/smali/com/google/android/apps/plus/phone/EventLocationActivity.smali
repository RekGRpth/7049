.class public Lcom/google/android/apps/plus/phone/EventLocationActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "EventLocationActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;


# instance fields
.field private mInitialQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->LOCATION_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v1, p1, Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/fragments/EventLocationFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->setOnLocationSelectedListener(Lcom/google/android/apps/plus/fragments/EventLocationFragment$OnLocationSelectedListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->mInitialQuery:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->mInitialQuery:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EventLocationFragment;->setInitialQueryString(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/PlaceJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/Place;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/EventLocationActivity;->mInitialQuery:Ljava/lang/String;

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$layout;->event_location_activity:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setContentView(I)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->showTitlebar(Z)V

    sget v2, Lcom/google/android/apps/plus/R$string;->event_location_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setTitlebarTitle(Ljava/lang/String;)V

    return-void
.end method

.method public final onLocationSelected(Lcom/google/api/services/plusi/model/Place;)V
    .locals 3
    .param p1    # Lcom/google/api/services/plusi/model/Place;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-eqz p1, :cond_0

    const-string v1, "location"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceJson;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/google/api/services/plusi/model/PlaceJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->finish()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->onBackPressed()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EventLocationActivity;->onBackPressed()V

    return-void
.end method
