.class abstract Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;
.super Ljava/lang/Object;
.source "FIFEUtil.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "AbstractIterator"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field next:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->NOT_READY:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract computeNext()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final hasNext()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    sget-object v3, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->FAILED:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    if-ne v2, v3, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/phone/FIFEUtil$1;->$SwitchMap$com$google$android$apps$plus$phone$FIFEUtil$Splitter$AbstractIterator$State:[I

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->FAILED:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->computeNext()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->next:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    sget-object v3, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->DONE:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    if-eq v2, v3, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->READY:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    move v0, v1

    :cond_1
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;->NOT_READY:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator$State;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;->next:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
