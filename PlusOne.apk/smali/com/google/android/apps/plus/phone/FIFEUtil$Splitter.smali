.class final Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;
.super Ljava/lang/Object;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/FIFEUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Splitter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$AbstractIterator;,
        Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$SplittingIterator;,
        Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;
    }
.end annotation


# instance fields
.field private final omitEmptyStrings:Z

.field private final strategy:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;-><init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;Z)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;Z)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->strategy:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->omitEmptyStrings:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;)Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->strategy:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->omitEmptyStrings:Z

    return v0
.end method

.method public static on(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;
    .locals 2
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "separator may not be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    new-instance v1, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$1;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;-><init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;)V

    return-object v0
.end method


# virtual methods
.method public final omitEmptyStrings()Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;
    .locals 3

    new-instance v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;->strategy:Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;-><init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$Strategy;Z)V

    return-object v0
.end method

.method public final split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter$2;-><init>(Lcom/google/android/apps/plus/phone/FIFEUtil$Splitter;Ljava/lang/CharSequence;)V

    return-object v0
.end method
