.class final Lcom/google/android/apps/plus/phone/HomeActivity$2;
.super Ljava/lang/Object;
.source "HomeActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

.field final synthetic val$finishOnOk:Z


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/HomeActivity;Z)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->val$finishOnOk:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->val$finishOnOk:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    # getter for: Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->access$100(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/HostLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->isNavigationBarVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    # getter for: Lcom/google/android/apps/plus/phone/HomeActivity;->mHostLayout:Lcom/google/android/apps/plus/views/HostLayout;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/HomeActivity;->access$100(Lcom/google/android/apps/plus/phone/HomeActivity;)Lcom/google/android/apps/plus/views/HostLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HomeActivity$2;->this$0:Lcom/google/android/apps/plus/phone/HomeActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/HomeActivity;->access$202(Lcom/google/android/apps/plus/phone/HomeActivity;Landroid/os/Bundle;)Landroid/os/Bundle;

    goto :goto_0
.end method
