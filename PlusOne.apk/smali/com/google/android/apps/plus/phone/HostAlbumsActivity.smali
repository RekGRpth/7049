.class public Lcom/google/android/apps/plus/phone/HostAlbumsActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostAlbumsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedAlbumsFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method
