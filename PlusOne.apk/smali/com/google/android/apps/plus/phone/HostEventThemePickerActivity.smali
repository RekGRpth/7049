.class public Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostEventThemePickerActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;-><init>(I)V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->EVENT_THEMES:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;

    invoke-virtual {p1, p0}, Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment;->setOnThemeSelectedListener(Lcom/google/android/apps/plus/fragments/HostedEventThemeListFragment$OnThemeSelectedListener;)V

    :cond_0
    return-void
.end method

.method public final onThemeSelected(ILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "theme_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "theme_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostEventThemePickerActivity;->finish()V

    return-void
.end method
