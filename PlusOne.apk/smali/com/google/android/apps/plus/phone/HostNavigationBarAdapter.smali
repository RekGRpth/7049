.class public final Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "HostNavigationBarAdapter.java"


# static fields
.field private static final DESTINATIONS_PROJECTION:[Ljava/lang/String;


# instance fields
.field private mDestinationRows:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mDestinationsCursor:Landroid/database/MatrixCursor;

.field private mInflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "icon"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "text"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationRows:Ljava/util/ArrayList;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public final addDestination(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->addDestination(IILjava/lang/CharSequence;Ljava/lang/String;)V

    return-void
.end method

.method public final addDestination(IILjava/lang/CharSequence;Ljava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Ljava/lang/String;

    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const/4 v1, 0x3

    aput-object p4, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationRows:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationsCursor:Landroid/database/MatrixCursor;

    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    return-void
.end method

.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/16 v8, 0x8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v6, 0x2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v6, Lcom/google/android/apps/plus/R$id;->icon:I

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget v6, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    sget v6, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final getDestinationId(I)I
    .locals 2
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mInflater:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->host_navigation_item:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final removeAllDestinations()V
    .locals 2

    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->DESTINATIONS_PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationsCursor:Landroid/database/MatrixCursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationRows:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public final showDestinations()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->mDestinationsCursor:Landroid/database/MatrixCursor;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostNavigationBarAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method
