.class public Lcom/google/android/apps/plus/phone/HostPhotosActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostPhotosActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mCurrentAlbumId:Ljava/lang/String;

.field private mCurrentAlbumType:Ljava/lang/String;

.field private mCurrentSpinnerIndex:I

.field private mCurrentStreamId:Ljava/lang/String;

.field private mHideAlbumSpinner:Z

.field private mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

.field private mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->shouldHideParentActionBarItems()Z

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "notif_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-static {p0, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    iput v4, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "album_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentAlbumId:Ljava/lang/String;

    const-string v2, "stream_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentStreamId:Ljava/lang/String;

    const-string v2, "album_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentAlbumType:Ljava/lang/String;

    const-string v2, "hide_album_spinner"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHideAlbumSpinner:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHideAlbumSpinner:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    invoke-virtual {v2, v5, v6, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    new-instance v2, Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;

    invoke-direct {v2, p0, v6}, Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;

    :cond_1
    return-void

    :cond_2
    const-string v2, "current_spinner_index"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    const-string v0, "person_id"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "photos_show_camera_album"

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string v0, "photos_home"

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    const-string v0, "hide_photos_of_me"

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;

    const/4 v4, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/phone/PhotosHomeGridLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 9
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v8, 0x7

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v2, 0x0

    check-cast p2, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    if-nez p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->onActionBarInvalidated()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-interface {p2, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_4

    move-object v1, v2

    :goto_1
    invoke-interface {p2, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_5

    move-object v3, v2

    :goto_2
    invoke-interface {p2, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object v4, v2

    :goto_3
    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentAlbumId:Ljava/lang/String;

    invoke-static {v1, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentStreamId:Ljava/lang/String;

    invoke-static {v3, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentAlbumType:Ljava/lang/String;

    invoke-static {v4, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    :cond_3
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->onActionBarInvalidated()V

    goto :goto_0

    :cond_4
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_6
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 10
    .param p1    # I

    iget v8, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    if-ne v8, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;

    invoke-virtual {v8, p1}, Lcom/google/android/apps/plus/phone/HostPhotosActivity$AlbumSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    if-eqz v1, :cond_0

    const/16 v8, 0x8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    sget v9, Lcom/google/android/apps/plus/R$string;->photos_home_unknown_label:I

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_1
    const/4 v8, 0x5

    invoke-interface {v1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_3

    const/4 v0, 0x0

    :goto_2
    const/4 v8, 0x4

    invoke-interface {v1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v7, 0x0

    :goto_3
    const/4 v8, 0x6

    invoke-interface {v1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_5

    const/4 v3, 0x0

    :goto_4
    const/4 v8, 0x7

    invoke-interface {v1, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v5, 0x0

    :goto_5
    const-string v8, "photos_of_me"

    invoke-static {v7, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_7

    const/4 v4, 0x0

    :goto_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v8, "album_id"

    invoke-virtual {v2, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "album_name"

    invoke-virtual {v2, v8, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "owner_id"

    invoke-virtual {v2, v8, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "stream_id"

    invoke-virtual {v2, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "album_type"

    invoke-virtual {v2, v8, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v8, "photos_of_user_id"

    invoke-virtual {v2, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->setIntent(Landroid/content/Intent;)V

    iput p1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentAlbumId:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentStreamId:Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentAlbumType:Ljava/lang/String;

    new-instance v8, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {v8}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V

    iput-object v8, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->replaceFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :cond_2
    const/16 v8, 0x8

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    :cond_3
    const/4 v8, 0x5

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    const/4 v8, 0x4

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    :cond_5
    const/4 v8, 0x6

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4

    :cond_6
    const/4 v8, 0x7

    invoke-interface {v1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v4

    goto :goto_6
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "current_spinner_index"

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
