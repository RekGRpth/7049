.class public Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostPhotosShareActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;

.field private static sRowId:J


# instance fields
.field private mCurrentSpinnerIndex:I

.field private mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

.field private mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;

.field private mSpinnerCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "owner_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "stream_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method

.method private static writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/phone/EsMatrixCursor;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->newRow()Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v0

    sget-wide v1, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->sRowId:J

    const-wide/16 v3, 0x1

    add-long/2addr v3, v1

    sput-wide v3, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->sRowId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Lcom/google/android/apps/plus/phone/EsMatrixCursor$RowBuilder;

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTOS_LIST:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected final onAttachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;->shouldHideParentActionBarItems()Z

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;

    iget v2, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v1, "notif_id"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1, v8}, Lcom/google/android/apps/plus/service/EsService;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;

    :cond_0
    const-string v1, "album_type"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v1, "camera_photos"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    :goto_1
    new-instance v0, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mSpinnerCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->photos_home_local_label:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mSpinnerCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const-string v1, "camera_photos"

    move-object v3, v2

    move-object v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->photos_home_instant_upload_label:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mSpinnerCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    const-string v1, "from_my_phone"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "camerasync"

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->writeMatrix(Lcom/google/android/apps/plus/phone/EsMatrixCursor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mSpinnerCursor:Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;

    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const-string v1, "current_spinner_index"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    goto :goto_1
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 12
    .param p1    # I

    const/4 v8, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    if-ne v7, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mPrimarySpinnerAdapter:Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;

    invoke-virtual {v7, p1}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity$AlbumSpinnerAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    invoke-interface {v0, v8}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$string;->photos_home_unknown_label:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-interface {v0, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v5, v6

    :goto_2
    invoke-interface {v0, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v2, v6

    :goto_3
    invoke-interface {v0, v11}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-eqz v7, :cond_5

    move-object v3, v6

    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v6, "album_name"

    invoke-virtual {v1, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "owner_id"

    invoke-virtual {v1, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "stream_id"

    invoke-virtual {v1, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "album_type"

    invoke-virtual {v1, v6, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->setIntent(Landroid/content/Intent;)V

    iput p1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    new-instance v6, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-direct {v6}, Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;-><init>()V

    iput-object v6, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mHostedFragment:Lcom/google/android/apps/plus/fragments/HostedPhotosFragment;

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->replaceFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :cond_2
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_3
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :cond_4
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    :cond_5
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "current_spinner_index"

    iget v1, p0, Lcom/google/android/apps/plus/phone/HostPhotosShareActivity;->mCurrentSpinnerIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
