.class public Lcom/google/android/apps/plus/phone/HostPostLinkActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "HostPostLinkActivity.java"


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/HostPostLinkActivity;)Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/HostPostLinkActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    return-object v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->COMPOSE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v5, Lcom/google/android/apps/plus/R$layout;->post_link_activity:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->setContentView(I)V

    new-instance v3, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;

    invoke-direct {v3}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v5, "account"

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "link_url"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "link_url"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "link_url"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/plus/fragments/HostedPostLinkFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->post_container:I

    const-string v6, "link_tag"

    invoke-virtual {v4, v5, v3, v6}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    sget v5, Lcom/google/android/apps/plus/R$id;->cancel_button:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v5, Lcom/google/android/apps/plus/phone/HostPostLinkActivity$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity$1;-><init>(Lcom/google/android/apps/plus/phone/HostPostLinkActivity;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    sget v5, Lcom/google/android/apps/plus/R$id;->done_button:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v5, Lcom/google/android/apps/plus/phone/HostPostLinkActivity$2;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/phone/HostPostLinkActivity$2;-><init>(Lcom/google/android/apps/plus/phone/HostPostLinkActivity;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    return-void
.end method
