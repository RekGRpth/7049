.class public Lcom/google/android/apps/plus/phone/HostSquareMemberSearchActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "HostSquareMemberSearchActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final bridge synthetic createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberSearchFragment;-><init>()V

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->host_dialog_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method
