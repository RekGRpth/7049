.class public Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;
.super Lcom/google/android/apps/plus/fragments/HostedEsFragment;
.source "HostedEventInviteeListFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/fragments/HostedEsFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AbsListView$RecyclerListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

.field private mAuthKey:Ljava/lang/String;

.field private mDeleteAddRequestId:Ljava/lang/Integer;

.field private mEventId:Ljava/lang/String;

.field private mGetEventRequestId:Ljava/lang/Integer;

.field private mGetInviteesRequestId:Ljava/lang/Integer;

.field private mInviteMoreRequestId:Ljava/lang/Integer;

.field protected mListView:Landroid/widget/ListView;

.field private final mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

.field private mOwnerId:Ljava/lang/String;

.field private mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mRefreshNeeded:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$1;-><init>(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleGetEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$string;->event_inviting_more:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAuthKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->invitePeopleToEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method private handleGetEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->updateRefreshStatus()V

    return-void
.end method

.method private handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->refresh(I)V

    goto :goto_0
.end method

.method private refresh(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    if-eq p1, v4, :cond_0

    if-nez p1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    :cond_1
    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAuthKey:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->getEventInvitees(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->updateRefreshStatus()V

    return-void
.end method

.method private showProgressDialog(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method private updateRefreshStatus()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showProgressIndicator()V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->hideProgressIndicator()V

    goto :goto_0
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final handleAddRemoveGuestCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 4
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq p1, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->transient_server_error:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final handleGetEventInviteesCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 3
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    :cond_2
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->updateRefreshStatus()V

    goto :goto_0
.end method

.method protected final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActionButtonClicked(I)V
    .locals 9
    .param p1    # I

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_OPENED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->event_invite_activity_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0xb

    const/4 v7, 0x1

    move v6, v5

    move v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/plus/phone/Intents;->getEditAudienceActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;IZZZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v1, -0x1

    if-eq p2, v1, :cond_0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_CANCEL_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :cond_0
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->INVITE_WIDGET_ADD_PEOPLE_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    const-string v1, "audience"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v1, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$3;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$3;-><init>(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1
    .param p1    # Landroid/app/Activity;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onAttach(Landroid/app/Activity;)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->setOnActionListener(Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter$OnActionListener;)V

    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    if-eqz p1, :cond_6

    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    :cond_0
    const-string v0, "ownerid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ownerid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    :cond_1
    const-string v0, "invitemoreid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "invitemoreid"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    :cond_2
    const-string v0, "inviteesreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "inviteesreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    :cond_3
    const-string v0, "eventreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "eventreq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    :cond_4
    const-string v0, "eventaddremovereq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "eventaddremovereq"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    :cond_5
    const-string v0, "inviteesrefreshneeded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "inviteesrefreshneeded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->setViewerGaiaId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->setEventOwnerId(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->refresh()V

    :cond_7
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 6
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v1, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$2;

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->EVENTS_ALL_URI:Landroid/net/Uri;

    invoke-direct {v1, p0, v0, v2, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment$2;-><init>(Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;Landroid/content/Context;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0

    :pswitch_1
    new-instance v1, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/fragments/EventInviteeListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget v1, Lcom/google/android/apps/plus/R$layout;->people_list_fragment:I

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    invoke-virtual {v1, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v4, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    instance-of v1, p2, Lcom/google/android/apps/plus/views/PeopleListItemView;

    if-eqz v1, :cond_0

    check-cast p2, Lcom/google/android/apps/plus/views/PeopleListItemView;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/PeopleListItemView;->getGaiaId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityByGaiaIdIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->authKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAuthKey:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->invalidateActionBar()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAdapter:Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/fragments/EventInviteeListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_0
    return-void
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPause()V

    return-void
.end method

.method protected final onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->canInviteOthers(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_rsvp_invite_more:I

    sget v2, Lcom/google/android/apps/plus/R$string;->event_button_invite_more_label:I

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostActionBar;->showActionButton(III)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mPlusEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->updateRefreshStatus()V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onPrepareActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    return-void
.end method

.method public final onReInviteInvitee(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_reinviting_invitee:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAuthKey:Ljava/lang/String;

    const/4 v4, 0x0

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->manageEventGuest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    return-void
.end method

.method public final onRemoveInvitee(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    sget v0, Lcom/google/android/apps/plus/R$string;->event_removing_invitee:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->showProgressDialog(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAuthKey:Ljava/lang/String;

    const/4 v4, 0x1

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/EsService;->manageEventGuest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    return-void
.end method

.method public final onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onResume()V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleInviteMoreComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleGetEventInviteesCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleGetEventCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->handleAddRemoveGuestCallback(ILcom/google/android/apps/plus/service/ServiceResult;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->refresh()V

    :cond_4
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "id"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "ownerid"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "invitemoreid"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mInviteMoreRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    const-string v0, "inviteesreq"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetInviteesRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    const-string v0, "eventreq"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mGetEventRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    const-string v0, "eventaddremovereq"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mDeleteAddRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    const-string v0, "inviteesrefreshneeded"

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mRefreshNeeded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected final onSetArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->onSetArguments(Landroid/os/Bundle;)V

    const-string v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mEventId:Ljava/lang/String;

    const-string v0, "owner_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mOwnerId:Ljava/lang/String;

    const-string v0, "auth_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->mAuthKey:Ljava/lang/String;

    return-void
.end method

.method public final refresh()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/HostedEsFragment;->refresh()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/HostedEventInviteeListFragment;->refresh(I)V

    return-void
.end method
