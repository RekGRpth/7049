.class final Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;
.super Ljava/lang/Object;
.source "LocationController.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/LocationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LocalLocationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/LocationController;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/phone/LocationController;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/LocationController;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;-><init>(Lcom/google/android/apps/plus/phone/LocationController;)V

    return-void
.end method

.method private triggerLocationListener()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$500(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$500(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-ne v0, p0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$600(Lcom/google/android/apps/plus/phone/LocationController;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$700(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->areSameLocations(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    const-string v0, "LocationController"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v1, "LocationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "----> onLocationChanged: triggering location change because "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocationAcquisitionTimer:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$600(Lcom/google/android/apps/plus/phone/LocationController;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_6

    const-string v0, "only this location listener was registered"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mReverseGeo:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$800(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$900(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)V

    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    # setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSentLocation:Landroid/location/Location;
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$702(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLastSuccessfulLocationListener:Landroid/location/LocationListener;
    invoke-static {v0, p0}, Lcom/google/android/apps/plus/phone/LocationController;->access$502(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/LocationListener;)Landroid/location/LocationListener;

    return-void

    :cond_6
    const-string v0, "a previous location listener was successful"

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/location/LocationListener;->onLocationChanged(Landroid/location/Location;)V

    goto :goto_1
.end method


# virtual methods
.method public final onLocationChanged(Landroid/location/Location;)V
    .locals 6
    .param p1    # Landroid/location/Location;

    const/4 v5, 0x3

    const-string v1, "LocationController"

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "LocationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "====> onLocationChanged: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from provider: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v2

    invoke-static {v1, p1, v2}, Lcom/google/android/apps/plus/phone/LocationController;->access$100(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;Landroid/location/Location;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->triggerLocationListener()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "LocationController"

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "LocationController"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "----> onLocationChanged: new location: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # setter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1, p1}, Lcom/google/android/apps/plus/phone/LocationController;->access$002(Lcom/google/android/apps/plus/phone/LocationController;Landroid/location/Location;)Landroid/location/Location;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mDisplayDebugToast:Z
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$200(Lcom/google/android/apps/plus/phone/LocationController;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mNetworkListener:Landroid/location/LocationListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$300(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v1

    if-ne p0, v1, :cond_5

    const-string v0, "net: "

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mLocation:Landroid/location/Location;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/LocationController;->access$000(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "location_source"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->triggerLocationListener()V

    goto :goto_0

    :cond_5
    const-string v0, "gps: "

    goto :goto_1
.end method

.method public final onProviderDisabled(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderDisabled(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onProviderEnabled(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/location/LocationListener;->onProviderEnabled(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/LocationController$LocalLocationListener;->this$0:Lcom/google/android/apps/plus/phone/LocationController;

    # getter for: Lcom/google/android/apps/plus/phone/LocationController;->mListener:Landroid/location/LocationListener;
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/LocationController;->access$400(Lcom/google/android/apps/plus/phone/LocationController;)Landroid/location/LocationListener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Landroid/location/LocationListener;->onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V

    :cond_0
    return-void
.end method
