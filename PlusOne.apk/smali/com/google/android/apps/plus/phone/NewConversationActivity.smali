.class public Lcom/google/android/apps/plus/phone/NewConversationActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NewConversationActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/InsertCameraPhotoDialogDisplayer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;
    }
.end annotation


# static fields
.field private static sInstanceCount:I


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

.field private mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

.field private mCreateConversationRequestId:Ljava/lang/Integer;

.field private final mRTCServiceListener:Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/phone/NewConversationActivity;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mRTCServiceListener:Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/NewConversationActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/NewConversationActivity;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/NewConversationActivity;Ljava/lang/String;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/phone/NewConversationActivity;
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v1, v0, p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->createConversation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/phone/NewConversationActivity;)Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/NewConversationActivity;)Lcom/google/android/apps/plus/fragments/AudienceFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewConversationActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    return-object v0
.end method

.method public static hasInstance()Z
    .locals 1

    sget v0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateAllowSendMessage()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->isAudienceEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->setAllowSendMessage(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CONVERSATION_START_NEW:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final hideInsertCameraPhotoDialog()V
    .locals 1

    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->dismissDialog(I)V

    return-void
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 4
    .param p1    # Landroid/support/v4/app/Fragment;

    const/4 v3, 0x0

    const/4 v2, 0x1

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->updateAllowSendMessage()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->allowSendingImages(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mComposeMessageFragment:Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;

    new-instance v1, Lcom/google/android/apps/plus/phone/NewConversationActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity$1;-><init>(Lcom/google/android/apps/plus/phone/NewConversationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/ComposeMessageFragment;->setListener(Lcom/google/android/apps/plus/fragments/ComposeMessageFragment$Listener;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/AudienceFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/AudienceFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setCirclesUsageType(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setIncludePhoneOnlyContacts(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setIncludePlusPages(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setPublicProfileSearchEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setShowSuggestedPeople(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->updateAllowSendMessage()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAudienceFragment:Lcom/google/android/apps/plus/fragments/AudienceFragment;

    new-instance v1, Lcom/google/android/apps/plus/phone/NewConversationActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity$2;-><init>(Lcom/google/android/apps/plus/phone/NewConversationActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/AudienceFragment;->setAudienceChangedCallback(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_ABORT_NEW:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->new_conversation_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->showTitlebar(Z)V

    sget v0, Lcom/google/android/apps/plus/R$string;->new_huddle_label:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->setTitlebarTitle(Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "requestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "requestId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    :cond_1
    :goto_0
    sget v0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    if-le v0, v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NewConversationActivity onCreate instanceCount out of sync: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const v0, 0x7f0a003e

    if-ne p1, v0, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ImageUtils;->createInsertCameraPhotoDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onDestroy()V

    sget v0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "NewConversationActivity onDestroy instanceCount out of sync: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/hangout/Log;->error(Ljava/lang/String;)V

    const/4 v0, 0x0

    sput v0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->sInstanceCount:I

    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mRTCServiceListener:Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->isIntentAccountActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mRTCServiceListener:Lcom/google/android/apps/plus/phone/NewConversationActivity$RTCServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->connectAndStayConnected(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->finish()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "requestId"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mCreateConversationRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onStart()V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewConversationActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method public final showInsertCameraPhotoDialog()V
    .locals 1

    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewConversationActivity;->showDialog(I)V

    return-void
.end method
