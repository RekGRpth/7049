.class public Lcom/google/android/apps/plus/phone/NewEventActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "NewEventActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;


# instance fields
.field private mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/NewEventActivity;)Lcom/google/android/apps/plus/fragments/EditEventFragment;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewEventActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    return-object v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method protected final getTitleButton3Text$9aa72f6()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->invite:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->CREATE_EVENT:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->createEvent()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->setOnEventChangedListener(Lcom/google/android/apps/plus/fragments/EditEventFragment$OnEditEventListener;)V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewEventActivity;->mEditEventFragment:Lcom/google/android/apps/plus/fragments/EditEventFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->onDiscard()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v2, Lcom/google/android/apps/plus/R$layout;->new_event_activity:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NewEventActivity;->setContentView(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->cancel_button:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NewEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/phone/NewEventActivity$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/NewEventActivity$1;-><init>(Lcom/google/android/apps/plus/phone/NewEventActivity;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->share_button:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/NewEventActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v2, Lcom/google/android/apps/plus/phone/NewEventActivity$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/phone/NewEventActivity$2;-><init>(Lcom/google/android/apps/plus/phone/NewEventActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    return-void
.end method

.method public final onEventClosed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    return-void
.end method

.method public final onEventSaved(Lcom/google/api/services/plusi/model/PlusEvent;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz p1, :cond_3

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->description:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_DESCRIPTION_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_0
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_ENDTIME_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_1
    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/EventOptions;->hangout:Ljava/lang/Boolean;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_HANGOUT_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->EVENT_WITH_IMAGE_CREATED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/NewEventActivity;->onBackPressed()V

    return-void
.end method
