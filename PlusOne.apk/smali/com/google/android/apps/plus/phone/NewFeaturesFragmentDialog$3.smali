.class final Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;
.super Ljava/lang/Object;
.source "NewFeaturesFragmentDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->this$0:Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->this$0:Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    # getter for: Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->CONTACTS_SYNC_ENABLED:Z
    invoke-static {}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->access$200()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->this$0:Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    # getter for: Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsSyncChoice:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->access$000(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->this$0:Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    # getter for: Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->access$100(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncPreference(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-static {v0, v3, v1, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordImproveSuggestionsPreferenceChange(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLcom/google/android/apps/plus/analytics/OzViews;)V

    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/service/EsService;->disableWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/service/EsService;->enableAndPerformWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    goto :goto_0
.end method
