.class public Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "NewFeaturesFragmentDialog.java"


# static fields
.field private static final CONTACTS_SYNC_ENABLED:Z


# instance fields
.field private mContactsStatsSyncChoice:Landroid/widget/CheckBox;

.field private mContactsSyncChoice:Landroid/widget/CheckBox;

.field private mContactsSyncView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->CONTACTS_SYNC_ENABLED:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsSyncChoice:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->CONTACTS_SYNC_ENABLED:Z

    return v0
.end method


# virtual methods
.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 18
    .param p1    # Landroid/os/Bundle;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v15

    const-string v16, "account"

    invoke-virtual/range {v15 .. v16}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v15

    sget v16, Lcom/google/android/apps/plus/R$layout;->whats_new_dialog:I

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    sget v15, Lcom/google/android/apps/plus/R$id;->contacts_sync_view:I

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsSyncView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsSyncView:Landroid/view/View;

    move-object/from16 v16, v0

    sget-boolean v15, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->CONTACTS_SYNC_ENABLED:Z

    if-eqz v15, :cond_0

    const/4 v15, 0x0

    :goto_0
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/view/View;->setVisibility(I)V

    sget v15, Lcom/google/android/apps/plus/R$id;->contacts_sync_checkbox:I

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsSyncChoice:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsSyncChoice:Landroid/widget/CheckBox;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/CheckBox;->setChecked(Z)V

    sget v15, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox:I

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->mContactsStatsSyncChoice:Landroid/widget/CheckBox;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/CheckBox;->setChecked(Z)V

    sget v15, Lcom/google/android/apps/plus/R$id;->contacts_sync_checkbox_title:I

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    new-instance v15, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$1;-><init>(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;)V

    invoke-virtual {v5, v15}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v15, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox_title:I

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {v6}, Lcom/google/android/apps/plus/util/AndroidUtils;->hasTelephony(Landroid/content/Context;)Z

    move-result v15

    if-eqz v15, :cond_1

    sget v15, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_phone_summary:I

    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v15, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$2;-><init>(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;)V

    invoke-virtual {v4, v15}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v15, Lcom/google/android/apps/plus/R$id;->contacts_stats_sync_checkbox_link:I

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    sget v15, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_learn_more:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v12, Landroid/text/SpannableString;

    invoke-direct {v12, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    sget v16, Lcom/google/android/apps/plus/R$string;->url_param_help_stats_sync:I

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v11

    invoke-static {v12, v11, v13}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v15

    invoke-virtual {v3, v15}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v2, v14}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v10, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;

    move-object/from16 v0, p0

    invoke-direct {v10, v0, v1}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog$3;-><init>(Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;Lcom/google/android/apps/plus/content/EsAccount;)V

    const v15, 0x104000a

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/phone/NewFeaturesFragmentDialog;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v15, 0x0

    invoke-virtual {v2, v15}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v15

    return-object v15

    :cond_0
    const/16 v15, 0x8

    goto/16 :goto_0

    :cond_1
    sget v15, Lcom/google/android/apps/plus/R$string;->contacts_stats_sync_preference_enabled_tablet_summary:I

    goto :goto_1
.end method
