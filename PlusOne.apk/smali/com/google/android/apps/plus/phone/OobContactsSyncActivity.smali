.class public Lcom/google/android/apps/plus/phone/OobContactsSyncActivity;
.super Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.source "OobContactsSyncActivity.java"


# instance fields
.field private mSyncFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_IMPROVE_CONTACTS_VIEW:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/OobContactsSyncActivity;->mSyncFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    :cond_0
    return-void
.end method

.method public final onContinuePressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobContactsSyncActivity;->mSyncFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobContactsSyncActivity;->mSyncFragment:Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobContactsSyncFragment;->commit()Z

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinuePressed()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->oob_contacts_sync_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobContactsSyncActivity;->setContentView(I)V

    return-void
.end method
