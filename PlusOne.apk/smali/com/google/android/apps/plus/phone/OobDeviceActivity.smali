.class public abstract Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "OobDeviceActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mIsLastOobStep:Z

.field private mOobActionBar:Lcom/google/android/apps/plus/views/ClientOobActionBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mIsLastOobStep:Z

    return-void
.end method


# virtual methods
.method protected getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getOobActionBar()Lcom/google/android/apps/plus/views/ClientOobActionBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mOobActionBar:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    return-object v0
.end method

.method public getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final isLastOobStep()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mIsLastOobStep:Z

    return v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->finish()V

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->overridePendingTransition(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/Intents;->isInitialOobIntent$755b117a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onBackPressed()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinuePressed()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1020019
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onContinue()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "plus_pages"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AccountSettingsData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {p0, v3, v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getNextOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/google/android/apps/plus/content/EsAccountsData;->setOobComplete(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->finish()V

    goto :goto_0
.end method

.method public onContinuePressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->setHasVisitedOob(Landroid/content/Context;Z)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "plus_pages"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/AccountSettingsData;

    sget v3, Lcom/google/android/apps/plus/R$id;->oob_action_bar:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/ClientOobActionBar;

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mOobActionBar:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mOobActionBar:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    if-eqz v3, :cond_0

    invoke-static {p0, v0, v2, v1}, Lcom/google/android/apps/plus/phone/Intents;->isLastOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Landroid/content/Intent;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mIsLastOobStep:Z

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mOobActionBar:Lcom/google/android/apps/plus/views/ClientOobActionBar;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->mIsLastOobStep:Z

    if-eqz v3, :cond_1

    sget v3, Lcom/google/android/apps/plus/R$string;->signup_done:I

    :goto_0
    invoke-virtual {v4, v3, p0}, Lcom/google/android/apps/plus/views/ClientOobActionBar;->enableRightButton(ILandroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :cond_1
    sget v3, Lcom/google/android/apps/plus/R$string;->signup_continue:I

    goto :goto_0
.end method
