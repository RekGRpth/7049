.class public Lcom/google/android/apps/plus/phone/OobInstantUploadActivity;
.super Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.source "OobInstantUploadActivity.java"


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->OOB_CAMERA_SYNC:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/OobInstantUploadActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;

    :cond_0
    return-void
.end method

.method public final onContinuePressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobInstantUploadActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobInstantUploadActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobInstantUploadFragment;->commit()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinuePressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->oob_instant_upload_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobInstantUploadActivity;->setContentView(I)V

    return-void
.end method
