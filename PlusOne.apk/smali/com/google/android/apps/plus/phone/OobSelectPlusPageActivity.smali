.class public Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;
.super Lcom/google/android/apps/plus/phone/OobDeviceActivity;
.source "OobSelectPlusPageActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mContinueButton:Landroid/widget/Button;

.field private mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onActivityResult(IILandroid/content/Intent;)V

    return-void

    :pswitch_0
    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    :cond_0
    return-void
.end method

.method public final onContinue()V
    .locals 1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onContinue()V

    return-void
.end method

.method public final onContinuePressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->activateAccount()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->oob_select_plus_page_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setContentView(I)V

    if-eqz p1, :cond_0

    const-string v0, "active_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_0
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onPostCreate(Landroid/os/Bundle;)V

    const v0, 0x102001a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mContinueButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mFragment:Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/OobSelectPlusPageFragment;->isAccountSelected()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->setContinueButtonEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/OobDeviceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    const-string v0, "active_account"

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public final setContinueButtonEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mContinueButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/OobSelectPlusPageActivity;->mContinueButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method
