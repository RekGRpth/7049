.class public Lcom/google/android/apps/plus/phone/OutOfBoxActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "OutOfBoxActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v6, Lcom/google/android/apps/plus/R$layout;->out_of_box_activity:I

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->setContentView(I)V

    const/4 v6, 0x1

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->setHasVisitedOob(Landroid/content/Context;Z)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "network_oob"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v1

    :goto_0
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "oob_origin"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->createInitialTag()Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$id;->oob_container:I

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;->newInstance(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/OutOfBoxFragment;

    move-result-object v7

    invoke-virtual {v3, v6, v7, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/OutOfBoxActivity;->finish()V

    goto :goto_1
.end method
