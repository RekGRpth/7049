.class public interface abstract Lcom/google/android/apps/plus/phone/Pageable;
.super Ljava/lang/Object;
.source "Pageable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;
    }
.end annotation


# virtual methods
.method public abstract getCurrentPage()I
.end method

.method public abstract hasMore()Z
.end method

.method public abstract isDataSourceLoading()Z
.end method

.method public abstract loadMore()V
.end method

.method public abstract setLoadingListener(Lcom/google/android/apps/plus/phone/Pageable$LoadingListener;)V
.end method
