.class public final Lcom/google/android/apps/plus/phone/PageableCursor;
.super Landroid/database/AbstractCursor;
.source "PageableCursor.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;
    }
.end annotation


# instance fields
.field private mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

.field private mColumnNames:[Ljava/lang/String;

.field private mCurrentPage:I

.field private mCurrentPageHit:I

.field private mCursorCount:I

.field private mExtras:Landroid/os/Bundle;

.field private mPageRequests:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPinnedPages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final mProducer:Lcom/google/android/apps/plus/phone/PageProducer;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/PageProducer;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/phone/PageProducer;

    const/16 v1, 0x9

    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    new-instance v0, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPageRequests:Ljava/util/HashSet;

    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mExtras:Landroid/os/Bundle;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must provide a valid PageProducer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mProducer:Lcom/google/android/apps/plus/phone/PageProducer;

    return-void
.end method

.method private ensurePosition(I)V
    .locals 3
    .param p1    # I

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/PageableCursor;->getPageNumber(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mProducer:Lcom/google/android/apps/plus/phone/PageProducer;

    const/4 v2, 0x0

    invoke-interface {v1, p0, v0, v2}, Lcom/google/android/apps/plus/phone/PageProducer;->loadPage(Lcom/google/android/apps/plus/phone/PageableCursor;IZ)Z

    :cond_0
    return-void
.end method

.method private fillPages(I)V
    .locals 8
    .param p1    # I

    const/4 v7, 0x0

    const-string v4, "Pageable"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->snapshot()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Integer;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "Pageable"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "#fillPages; curPage: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", pages: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v4, p1, -0x4

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v0, v3, 0x9

    const/4 v1, 0x1

    :goto_0
    sub-int v4, p1, v3

    if-gt v1, v4, :cond_3

    add-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v4

    if-nez v4, :cond_1

    add-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->requestPage(I)V

    :cond_1
    sub-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v4

    if-nez v4, :cond_2

    sub-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->requestPage(I)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    sub-int v4, p1, v3

    add-int/lit8 v1, v4, 0x1

    :goto_1
    sub-int v4, p1, v3

    rsub-int/lit8 v4, v4, 0x9

    if-ge v1, v4, :cond_5

    add-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v4

    if-nez v4, :cond_4

    add-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->requestPage(I)V

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    sub-int v4, v0, p1

    add-int/lit8 v1, v4, 0x1

    :goto_2
    sub-int v4, v0, p1

    rsub-int/lit8 v4, v4, 0x9

    if-ge v1, v4, :cond_7

    sub-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v4

    if-nez v4, :cond_6

    sub-int v4, p1, v1

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/phone/PageableCursor;->requestPage(I)V

    :cond_6
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_7
    return-void
.end method

.method private getCursorAtPosition(I)Landroid/database/Cursor;
    .locals 5
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/PageableCursor;->ensurePosition(I)V

    invoke-static {p1}, Lcom/google/android/apps/plus/phone/PageableCursor;->getPageNumber(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Page not loaded; page: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursor(I)Landroid/database/Cursor;

    move-result-object v0

    if-gez p1, :cond_1

    :goto_0
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    return-object v0

    :cond_1
    rem-int/lit8 p1, p1, 0x64

    goto :goto_0
.end method

.method private static getPageNumber(I)I
    .locals 1
    .param p0    # I

    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    div-int/lit8 v0, p0, 0x64

    goto :goto_0
.end method

.method protected static getPageSize()I
    .locals 1

    const/16 v0, 0x64

    return v0
.end method

.method private hasPage(I)Z
    .locals 2
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private requestPage(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPageRequests:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPageRequests:Ljava/util/HashSet;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mProducer:Lcom/google/android/apps/plus/phone/PageProducer;

    const/4 v1, 0x0

    invoke-interface {v0, p0, p1, v1}, Lcom/google/android/apps/plus/phone/PageProducer;->requestPage(Lcom/google/android/apps/plus/phone/PageableCursor;IZ)V

    goto :goto_0
.end method


# virtual methods
.method protected final checkPosition()V
    .locals 1

    invoke-super {p0}, Landroid/database/AbstractCursor;->checkPosition()V

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->ensurePosition(I)V

    return-void
.end method

.method public final close()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->snapshot()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->evictAll()V

    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    return-void
.end method

.method public final getBlob(I)[B
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mColumnNames:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCursorCount:I

    return v0
.end method

.method protected final getCursor(I)Landroid/database/Cursor;
    .locals 3
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    :cond_0
    return-object v0
.end method

.method public final getDouble(I)D
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public final getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mExtras:Landroid/os/Bundle;

    return-object v0
.end method

.method public final getFloat(I)F
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public final getInt(I)I
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getLong(I)J
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getShort(I)S
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getType(I)I
    .locals 1
    .param p1    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    invoke-super {p0, p1}, Landroid/database/AbstractCursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public final isNull(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPos:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PageableCursor;->getCursorAtPosition(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public final onMove(II)Z
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v6, 0x0

    invoke-static {p2}, Lcom/google/android/apps/plus/phone/PageableCursor;->getPageNumber(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PageableCursor;->hasPage(I)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "Pageable"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->snapshot()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Integer;

    invoke-interface {v3, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "Pageable"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "#onMove; load page: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pages: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mProducer:Lcom/google/android/apps/plus/phone/PageProducer;

    invoke-interface {v3, p0, v1, v6}, Lcom/google/android/apps/plus/phone/PageProducer;->loadPage(Lcom/google/android/apps/plus/phone/PageableCursor;IZ)Z

    move-result v2

    :goto_0
    iget v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCurrentPage:I

    if-eq v3, v1, :cond_1

    iput v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCurrentPage:I

    iput v6, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCurrentPageHit:I

    :cond_1
    iget v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCurrentPageHit:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCurrentPageHit:I

    iget v3, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCurrentPageHit:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PageableCursor;->fillPages(I)V

    :cond_2
    return v2

    :cond_3
    const/4 v2, 0x1

    goto :goto_0
.end method

.method protected final setCursor(ILandroid/database/Cursor;Z)V
    .locals 2
    .param p1    # I
    .param p2    # Landroid/database/Cursor;
    .param p3    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mColumnNames:[Ljava/lang/String;

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mColumnNames:[Ljava/lang/String;

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPageRequests:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    if-eqz p2, :cond_1

    if-eqz p3, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    :goto_0
    return-void

    :cond_3
    if-nez p3, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mPinnedPages:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCachedPages:Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/apps/plus/phone/PageableCursor$PageCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final setCursorCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mCursorCount:I

    return-void
.end method

.method protected final setExtras(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    sget-object p1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PageableCursor;->mExtras:Landroid/os/Bundle;

    return-void
.end method
