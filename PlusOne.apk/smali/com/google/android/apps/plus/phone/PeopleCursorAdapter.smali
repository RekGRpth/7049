.class public final Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;
.super Landroid/support/v4/widget/CursorAdapter;
.source "PeopleCursorAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private mAlwaysHideLetterSections:Z

.field private mAvatarUrlColumnIndex:I

.field protected mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

.field private final mGaiaIdColumnIndex:I

.field private mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

.field private final mNameColumnIndex:I

.field private mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

.field private final mPackedCircleIdsColumnIndex:I

.field private final mPersonIdColumnIndex:I

.field private mShowAddButtonIfNeeded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIIILcom/google/android/apps/plus/fragments/CircleNameResolver;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPersonIdColumnIndex:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mGaiaIdColumnIndex:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPackedCircleIdsColumnIndex:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAvatarUrlColumnIndex:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAlwaysHideLetterSections:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mShowAddButtonIfNeeded:Z

    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    return-void
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v9, 0x0

    move-object v3, p1

    check-cast v3, Lcom/google/android/apps/plus/views/PeopleListItemView;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mCircleNameResolver:Lcom/google/android/apps/plus/fragments/CircleNameResolver;

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setCircleNameResolver(Lcom/google/android/apps/plus/fragments/CircleNameResolver;)V

    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPersonIdColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPersonId(Ljava/lang/String;)V

    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mGaiaIdColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAvatarUrlColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v2, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setContactName(Ljava/lang/String;)V

    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mPackedCircleIdsColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setPackedCircleIds(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mShowAddButtonIfNeeded:Z

    if-eqz v8, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setAddButtonVisible(Z)V

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    invoke-virtual {v3, v8}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V

    :cond_0
    iget-boolean v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAlwaysHideLetterSections:Z

    if-nez v8, :cond_1

    if-eqz p3, :cond_1

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v8

    const/16 v10, 0x14

    if-le v8, v10, :cond_1

    const/4 v8, 0x1

    :goto_0
    if-eqz v8, :cond_4

    invoke-static {v4}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v1

    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeader(C)V

    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    :goto_2
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/PeopleListItemView;->updateContentDescription()V

    return-void

    :cond_1
    move v8, v9

    goto :goto_0

    :cond_2
    iget v8, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/util/StringUtils;->firstLetter(Ljava/lang/String;)C

    move-result v6

    if-eq v6, v1, :cond_3

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeader(C)V

    goto :goto_1

    :cond_3
    invoke-virtual {v3, v9}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    goto :goto_1

    :cond_4
    invoke-virtual {v3, v9}, Lcom/google/android/apps/plus/views/PeopleListItemView;->setSectionHeaderVisible(Z)V

    goto :goto_2
.end method

.method public final convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 1
    .param p1    # Landroid/database/Cursor;

    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPositionForSection(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-static {p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;->createInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/views/PeopleListItemView;

    move-result-object v0

    return-object v0
.end method

.method public final setAlwaysHideLetterSections(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mAlwaysHideLetterSections:Z

    return-void
.end method

.method public final setOnActionButtonClickListener(Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mOnActionButtonClickListener:Lcom/google/android/apps/plus/views/PeopleListItemView$OnActionButtonClickListener;

    return-void
.end method

.method public final setShowAddButtonIfNeeded(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mShowAddButtonIfNeeded:Z

    return-void
.end method

.method public final swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2
    .param p1    # Landroid/database/Cursor;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    iget v1, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mNameColumnIndex:I

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;-><init>(Landroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleCursorAdapter;->mIndexer:Lcom/google/android/apps/plus/fragments/EsAlphabetIndexer;

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
