.class public Lcom/google/android/apps/plus/phone/PeopleListActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "PeopleListActivity.java"


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

.field private mSelectedViewType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final addParentStack(Landroid/support/v4/app/TaskStackBuilder;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/TaskStackBuilder;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->getCirclesActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    return-void
.end method

.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mSelectedViewType:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    return-object v0

    :pswitch_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleFriendAddsListFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleYouMayKnowListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleYouMayKnowListFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_2
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;

    const-string v1, "ORGANIZATION"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_3
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;

    const-string v1, "SCHOOL"

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/fragments/PeopleAlmaMaterListFragment;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_4
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleCelebritiesListFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_5
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleCircleListFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_6
    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/PeopleListFragment;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->host_dialog_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mSelectedViewType:I

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "people_view_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/PeopleListActivity;->mSelectedViewType:I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected final shouldUpRecreateTask()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "up_recreate_task"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
