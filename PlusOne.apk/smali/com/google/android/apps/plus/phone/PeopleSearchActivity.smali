.class public Lcom/google/android/apps/plus/phone/PeopleSearchActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PeopleSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;


# instance fields
.field private mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private isPickerMode()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "picker_mode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PEOPLE_SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 7
    .param p1    # Landroid/support/v4/app/Fragment;

    const/4 v3, 0x1

    const/4 v4, 0x0

    instance-of v2, p1, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    if-eqz v2, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    sget v2, Lcom/google/android/apps/plus/R$id;->progress_spinner:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setProgressBar(Landroid/widget/ProgressBar;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setOnSelectionChangeListener(Lcom/google/android/apps/plus/fragments/PeopleSearchFragment$OnSelectionChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v5, "search_circles_usage"

    const/4 v6, -0x1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setCircleUsageType(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->isPickerMode()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v5, v2}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setAddToCirclesActionEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v5, "search_pub_profiles_enabled"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setPublicProfileSearchEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v5, "search_phones_enabled"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setPhoneOnlyContactsEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v5, "search_plus_pages_enabled"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setPlusPagesEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v4, "search_in_circles_enabled"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setPeopleInCirclesEnabled(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    const-string v3, "query"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->setInitialQueryString(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move v2, v4

    goto :goto_0
.end method

.method public final onCircleSelected(Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->isPickerMode()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "circle_data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->finish()V

    return-void

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->people_search_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setContentView(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->showTitlebar(Z)V

    sget v0, Lcom/google/android/apps/plus/R$string;->search_people_tab_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setTitlebarTitle(Ljava/lang/String;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public final onPersonSelected(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/content/PersonData;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->isPickerMode()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "person_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "person_data"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-static {v2, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->startExternalActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, p1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->isIntentAccountActive()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->mSearchFragment:Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchFragment;->startSearch()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->finish()V

    goto :goto_0
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PeopleSearchActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
