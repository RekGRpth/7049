.class public Lcom/google/android/apps/plus/phone/PhotoComposeActivity;
.super Lcom/google/android/apps/plus/analytics/InstrumentedActivity;
.source "PhotoComposeActivity.java"

# interfaces
.implements Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;
.implements Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAdapter:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;

.field private mMediaRefs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mMediaRefsToRemove:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;"
        }
    .end annotation
.end field

.field private mStartingPosition:I

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;-><init>()V

    return-void
.end method

.method private finishActivity()V
    .locals 4

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefsToRemove:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v1, v3, [Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefsToRemove:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefsToRemove:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/api/MediaRef;

    aput-object v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v3, "photo_remove_from_compose"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v3, -0x1

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Lcom/google/android/apps/plus/api/MediaRef;
    .locals 1
    .param p1    # I

    if-gez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemPosition(Ljava/lang/Object;)I
    .locals 3
    .param p1    # Ljava/lang/Object;

    const/4 v1, -0x2

    instance-of v2, p1, Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefsToRemove:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->finishActivity()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/analytics/InstrumentedActivity;->onCreate(Landroid/os/Bundle;)V

    sget v0, Lcom/google/android/apps/plus/R$layout;->photo_compose_activity:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string v0, "account"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v0, "photo_index"

    invoke-virtual {v7, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mStartingPosition:I

    const-string v0, "mediarefs"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "mediarefs"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v8

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, v8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    const/4 v6, 0x0

    :goto_0
    array-length v0, v8

    if-ge v6, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    aget-object v0, v8, v6

    check-cast v0, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->finish()V

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mStartingPosition:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mStartingPosition:I

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_3

    :cond_2
    iput v2, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mStartingPosition:I

    :cond_3
    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object v1, p0

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter$MediaRefProvider;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;

    sget v0, Lcom/google/android/apps/plus/R$id;->view_pager:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mStartingPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefsToRemove:Ljava/util/List;

    return-void
.end method

.method public final onImageRemoved(Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefsToRemove:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mMediaRefs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->finishActivity()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeActivity;->mAdapter:Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/PhotoComposePagerAdapter;->notifyDataSetChanged()V

    return-void
.end method
