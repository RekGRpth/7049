.class public Lcom/google/android/apps/plus/phone/PhotoComposeFragment;
.super Lcom/google/android/apps/plus/phone/HostedFragment;
.source "PhotoComposeFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostedFragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

.field private mDeleteButton:Landroid/widget/ImageButton;

.field private mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostedFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    sget v0, Lcom/google/android/apps/plus/R$id;->remove_image_button:I

    if-ne v6, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;->onImageRemoved(Lcom/google/android/apps/plus/api/MediaRef;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$id;->background:I

    if-ne v6, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideo()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isVideoReady()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getOwnerGaiaId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVideoData()[B

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/phone/Intents;->getVideoViewActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_view_video_not_ready:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v8, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isPanorama()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/phone/Intents;->getViewPanoramaActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const-string v1, "photo_ref"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getSafeContext()Landroid/content/Context;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/fragments/VideoDataLoader;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/fragments/VideoDataLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLandroid/net/Uri;)V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const/4 v4, 0x0

    sget v1, Lcom/google/android/apps/plus/R$layout;->photo_compose_fragment:I

    invoke-virtual {p1, v1, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->remove_image_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mDeleteButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mDeleteButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->stage:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$id;->stage_media:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    sget v1, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lcom/google/android/apps/plus/R$id;->background:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->init(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnImageListener(Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;)V

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v4, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-object v0
.end method

.method public final onImageLoadFinished$46d55081()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$id;->loading:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final onImageScaled(F)V
    .locals 0
    .param p1    # F

    return-void
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mBackgroundView:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setVideoBlob([B)V

    goto :goto_0

    :cond_2
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    goto :goto_1
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final setRemoveImageListener(Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoComposeFragment;->mRemoveImageListener:Lcom/google/android/apps/plus/phone/PhotoComposeFragment$RemoveImageListener;

    return-void
.end method
