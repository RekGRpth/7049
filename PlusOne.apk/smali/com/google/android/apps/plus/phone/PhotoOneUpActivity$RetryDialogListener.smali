.class final Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;
.super Ljava/lang/Object;
.source "PhotoOneUpActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RetryDialogListener"
.end annotation


# instance fields
.field final mTag:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->mTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->mTag:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpActivity$RetryDialogListener;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpActivity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;->doDownload(Landroid/content/Context;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
