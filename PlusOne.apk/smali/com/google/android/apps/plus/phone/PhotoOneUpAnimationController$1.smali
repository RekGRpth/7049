.class final Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;
.super Ljava/lang/Object;
.source "PhotoOneUpAnimationController.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$000(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # invokes: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x2

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$402(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;F)F

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$500(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->getHideOffset(Z)I

    move-result v1

    int-to-float v1, v1

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$402(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;F)F

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mAdjustMargins:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$200(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$000(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # invokes: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    # getter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mAdjustMargins:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$200(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;Z)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;->this$0:Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    const/4 v1, 0x3

    # setter for: Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
