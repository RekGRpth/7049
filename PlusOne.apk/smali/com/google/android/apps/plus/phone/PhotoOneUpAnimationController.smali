.class public Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
.super Ljava/lang/Object;
.source "PhotoOneUpAnimationController.java"


# instance fields
.field private final mAdjustMargins:Z

.field private mCurrentOffset:F

.field private final mSlideFromTop:Z

.field private final mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

.field private mState:I

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;ZZ)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z
    .param p3    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController$1;-><init>(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    iput-boolean p3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mAdjustMargins:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mAdjustMargins:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;Z)V
    .locals 7
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    .param p1    # Z

    const/4 v2, 0x1

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v4, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-eqz v3, :cond_1

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    if-eqz p1, :cond_0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    mul-int/2addr v1, v2

    add-int v2, v3, v1

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    :goto_1
    invoke-virtual {v0, v4, v2, v5, v1}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget v6, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    if-eqz p1, :cond_2

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    mul-int/2addr v1, v2

    add-int/2addr v1, v6

    move v2, v3

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V

    return-void
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;F)F
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;
    .param p1    # F

    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    return v0
.end method

.method private startAnimation(FI)V
    .locals 4
    .param p1    # F
    .param p2    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/animation/Animation;->cancel()V

    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    invoke-direct {v0, v3, v3, v2, p1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideInListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private updateVisibility()V
    .locals 5

    const/4 v3, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    if-nez v2, :cond_3

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    if-nez v1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mAdjustMargins:Z

    if-eqz v2, :cond_7

    :cond_0
    move v2, v3

    :goto_1
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    if-nez v1, :cond_1

    iget-boolean v4, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mAdjustMargins:Z

    if-eqz v4, :cond_2

    :cond_1
    const/4 v3, 0x1

    :cond_2
    invoke-virtual {v2, v3}, Landroid/view/View;->setClickable(Z)V

    return-void

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->getHideOffset(Z)I

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-lez v2, :cond_4

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    int-to-float v4, v0

    cmpl-float v2, v2, v4

    if-gez v2, :cond_5

    :cond_4
    iget-boolean v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-lez v2, :cond_6

    iget v2, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    int-to-float v4, v0

    cmpg-float v2, v2, v4

    if-gtz v2, :cond_6

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    const/4 v1, 0x1

    goto :goto_0

    :cond_7
    const/16 v2, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final animate(Z)V
    .locals 4
    .param p1    # Z

    const/16 v3, 0x64

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_2

    :cond_0
    if-nez p1, :cond_2

    const/4 v1, 0x0

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->startAnimation(FI)V

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->updateVisibility()V

    return-void

    :cond_2
    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3

    iget v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    :cond_3
    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->getHideOffset(Z)I

    move-result v0

    int-to-float v1, v0

    invoke-direct {p0, v1, v3}, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->startAnimation(FI)V

    int-to-float v1, v0

    iput v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    goto :goto_0
.end method

.method protected getHideOffset(Z)I
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mSlideFromTop:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    neg-int v0, v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final setCurrentOffset(I)V
    .locals 1
    .param p1    # I

    int-to-float v0, p1

    iput v0, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mCurrentOffset:F

    return-void
.end method

.method public final setCurrentState(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/PhotoOneUpAnimationController;->mState:I

    return-void
.end method
