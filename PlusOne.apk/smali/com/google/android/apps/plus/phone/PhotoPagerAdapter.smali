.class public final Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;
.source "PhotoPagerAdapter.java"


# instance fields
.field final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field final mAllowPlusOne:Z

.field private final mDefaultAlbumName:Ljava/lang/String;

.field final mDisableComments:Z

.field final mEventId:Ljava/lang/String;

.field final mForceLoadId:Ljava/lang/Long;

.field private mPageable:Lcom/google/android/apps/plus/phone/Pageable;

.field final mStreamId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/support/v4/app/FragmentManager;
    .param p3    # Landroid/database/Cursor;
    .param p4    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Ljava/lang/Long;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Z
    .param p10    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/database/Cursor;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p5, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mForceLoadId:Ljava/lang/Long;

    iput-object p6, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mStreamId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mEventId:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mDefaultAlbumName:Ljava/lang/String;

    iput-boolean p9, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAllowPlusOne:Z

    iput-boolean p10, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mDisableComments:Z

    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/phone/Pageable;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Landroid/support/v4/app/Fragment;
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->isDataValid()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    invoke-interface {v1}, Lcom/google/android/apps/plus/phone/Pageable;->loadMore()V

    new-instance v1, Lcom/google/android/apps/plus/fragments/LoadingFragment;

    invoke-direct {v1}, Lcom/google/android/apps/plus/fragments/LoadingFragment;-><init>()V

    :goto_1
    return-object v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorPagerAdapter;->getItem(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    goto :goto_1
.end method

.method public final getItem$2282a066(Landroid/database/Cursor;)Landroid/support/v4/app/Fragment;
    .locals 12
    .param p1    # Landroid/database/Cursor;

    const/4 v2, 0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v10, 0x2

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v10, 0x4

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-eqz v10, :cond_0

    :goto_0
    const-string v10, "PLACEHOLDER"

    const/4 v11, 0x6

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoOneUpFragmentIntentBuilder(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v0

    iget-object v10, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setGaiaId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setPhotoUrl(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v11

    if-eqz v2, :cond_1

    sget-object v10, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_1
    invoke-virtual {v11, v10}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setMediaType(Lcom/google/android/apps/plus/api/MediaRef$MediaType;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setDisplayName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mDefaultAlbumName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAlbumName(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mStreamId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setStreamId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mEventId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setEventId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mAllowPlusOne:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setAllowPlusOne(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mForceLoadId:Ljava/lang/Long;

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setForceLoadId(Ljava/lang/Long;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mDisableComments:Z

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setDisableComments(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    move-result-object v10

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->setIsPlaceholder(Ljava/lang/Boolean;)Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;

    new-instance v5, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;

    invoke-direct {v5}, Lcom/google/android/apps/plus/fragments/PhotoOneUpFragment;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    return-object v5

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    sget-object v10, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_1
.end method

.method public final setPageable(Lcom/google/android/apps/plus/phone/Pageable;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/phone/Pageable;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PhotoPagerAdapter;->mPageable:Lcom/google/android/apps/plus/phone/Pageable;

    return-void
.end method
