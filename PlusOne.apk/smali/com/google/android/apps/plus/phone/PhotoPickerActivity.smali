.class public Lcom/google/android/apps/plus/phone/PhotoPickerActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PhotoPickerActivity.java"


# instance fields
.field private mCropMode:I

.field private mDisplayName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO_PICKER:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    const/16 v7, 0xb

    const/4 v6, 0x1

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v3, Lcom/google/android/apps/plus/R$layout;->photo_picker_activity:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->setContentView(I)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->photo_picker_fragment_container:I

    new-instance v4, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;

    invoke-direct {v4, v2}, Lcom/google/android/apps/plus/fragments/PhotoPickerFragment;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "display_name"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "photo_picker_crop_mode"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mCropMode:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v3, v7, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    :goto_0
    iget v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mCropMode:I

    if-eqz v3, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_picker_sublabel:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_1
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v7, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v8}, Landroid/app/ActionBar;->setSubtitle(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :cond_1
    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->showTitlebar(Z)V

    sget v3, Lcom/google/android/apps/plus/R$menu;->album_view_menu:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->createTitlebarButtons(I)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->mDisplayName:Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->setTitlebarTitle(Ljava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->setTitlebarSubtitle(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 3
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoPickerActivity;->finish()V

    return-void
.end method
