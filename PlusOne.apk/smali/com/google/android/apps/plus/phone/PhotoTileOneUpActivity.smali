.class public Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "PhotoTileOneUpActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/phone/HostActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mTileId:Ljava/lang/String;

.field private mViewId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method

.method private showProgressLoader(Z)V
    .locals 2
    .param p1    # Z

    sget v0, Lcom/google/android/apps/plus/R$id;->empty_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;-><init>()V

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->photo_tile_one_up_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PHOTO:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "tile_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "tile_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mTileId:Ljava/lang/String;

    :cond_0
    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "view_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mViewId:Ljava/lang/String;

    :cond_1
    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->getHostActionBar()Lcom/google/android/apps/plus/views/HostActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->initForDarkTransparentTheme()V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->showProgressLoader(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mTileId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-object v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 3
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x2

    check-cast p2, Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->showProgressLoader(Z)V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {p2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/Intents;->newPhotoTileOneUpFragmentIntentBuilder(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->setPhotoRef(Lcom/google/android/apps/plus/api/MediaRef;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mTileId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->setTileId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->mViewId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->setViewId(Ljava/lang/String;)Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/Intents$PhotoTileOneUpIntentBuilder;->build()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->setIntent(Landroid/content/Intent;)V

    new-instance v0, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/PhotoTileOneUpFragment;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PhotoTileOneUpActivity;->replaceFragment(Landroid/support/v4/app/Fragment;)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method public final onRefreshButtonClicked()V
    .locals 0

    return-void
.end method
