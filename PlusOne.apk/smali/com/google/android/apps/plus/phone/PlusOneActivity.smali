.class public Lcom/google/android/apps/plus/phone/PlusOneActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "PlusOneActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PlusOneActivity$GmsErrorDialogFragment;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

.field private mInsert:Z

.field private mRedirectIntent:Landroid/content/Intent;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method

.method private recordErrorAndFinish()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_ERROR_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->PLUSONE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v3, 0x1

    if-ne p1, v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mUrl:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 23
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v17

    if-eqz v17, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "GMS_error"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/phone/PlusOneActivity$GmsErrorDialogFragment;

    const/4 v3, 0x0

    move/from16 v0, v17

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/plus/phone/PlusOneActivity$GmsErrorDialogFragment;-><init>(IB)V

    const-string v3, "GMS_error"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v19

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v16

    const-string v1, "from_signup"

    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    const-string v1, "calling_package"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v1, Lcom/google/android/apps/plus/util/Property;->PLUS_CLIENTID:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/Property;->get()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v10, v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCertificate(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/String;

    move-result-object v11

    const-string v1, "com.google.circles.platform.intent.extra.APIKEY"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v1, "com.google.circles.platform.intent.extra.CLIENTID"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "com.google.circles.platform.intent.extra.APIVERSION"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v1, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const/4 v8, 0x0

    move-object v12, v6

    move-object v13, v1

    invoke-direct/range {v7 .. v13}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/ApiaryApiInfo;)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-static {v1}, Lcom/google/android/apps/plus/external/PlatformContractUtils;->getCallingPackageAnalytics(Lcom/google/android/apps/plus/api/ApiaryApiInfo;)Ljava/util/Map;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getAnalyticsInfo$7d6d37aa()Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    move-result-object v15

    if-nez p1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_CLICKED_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v1}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_2
    if-eqz v18, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordErrorAndFinish()V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mApiInfo:Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v21

    if-eqz v21, :cond_5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getCertificate()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordErrorAndFinish()V

    goto/16 :goto_0

    :cond_6
    const-string v1, "com.google.circles.platform.intent.extra.ENTITY"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mUrl:Ljava/lang/String;

    const-string v1, "com.google.circles.platform.intent.extra.TOKEN"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    const-string v1, "com.google.circles.platform.intent.extra.ACTION"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->recordErrorAndFinish()V

    goto/16 :goto_0

    :cond_8
    const-string v1, "delete"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mInsert:Z

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "_PLUSONE_IGNORE_caller_package"

    move-object/from16 v0, v16

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v20

    new-instance v2, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mInsert:Z

    if-eqz v1, :cond_a

    const-string v1, "com.google.android.gms.plus.action.PLUS_ONE"

    :goto_2
    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    const-string v3, "com.google.android.gms.plus.plusone.PlusOneActivity"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.EXTRA_SIGNED_UP"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.TOKEN"

    move-object/from16 v0, v22

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.URL"

    move-object/from16 v0, v20

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mRedirectIntent:Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mRedirectIntent:Landroid/content/Intent;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/phone/PlusOneActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x0

    goto :goto_1

    :cond_a
    const-string v1, "com.google.android.gms.plus.action.UNDO_PLUS_ONE"

    goto :goto_2
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOneActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnActivity;->finishIfNoAccount(Landroid/app/Activity;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return-void
.end method
