.class public final Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;
.super Lcom/google/android/apps/plus/phone/EsCursorAdapter;
.source "PlusOnePeopleAdapter.java"


# instance fields
.field private mExtraPeopleView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method

.method private isLastViewExtraPeopleCount()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    if-ne p1, v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    const/4 v4, 0x2

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    sget v4, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const/4 v4, 0x3

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCount()I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isLastViewExtraPeopleCount()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isExtraPeopleViewIndex(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p3}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isExtraPeopleViewIndex(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p2, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public final isExtraPeopleViewIndex(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isLastViewExtraPeopleCount()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/EsCursorAdapter;->getCount()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->isExtraPeopleViewIndex(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/apps/plus/R$layout;->acl_row_view:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public final setExtraPeopleCount(I)V
    .locals 7
    .param p1    # I

    const/4 v5, 0x0

    const/4 v6, 0x0

    if-gtz p1, :cond_0

    iput-object v5, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mContext:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/plus/R$layout;->acl_row_view:I

    invoke-virtual {v0, v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mExtraPeopleView:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$plurals;->plus_one_people_more_plus_ones:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, p1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
