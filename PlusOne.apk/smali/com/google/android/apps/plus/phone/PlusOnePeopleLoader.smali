.class public final Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "PlusOnePeopleLoader.java"


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mPlusOneId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;->mPlusOneId:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 13

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/PlusOnePeopleLoader;->mPlusOneId:Ljava/lang/String;

    const/16 v6, 0x32

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PlusOnePeopleLoader"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->logError(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/GetPlusOnePeopleOperation;->getPeople()Ljava/util/List;

    move-result-object v11

    if-eqz v11, :cond_0

    new-instance v7, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/apps/plus/fragments/PlusOnePeopleFragment$PeopleSetQuery;->PROJECTION:[Ljava/lang/String;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v7, v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    const/4 v8, 0x0

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/api/services/plusi/model/DataPerson;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    add-int/lit8 v9, v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v12, Lcom/google/api/services/plusi/model/DataPerson;->obfuscatedId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, v12, Lcom/google/api/services/plusi/model/DataPerson;->obfuscatedId:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, v12, Lcom/google/api/services/plusi/model/DataPerson;->userName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, v12, Lcom/google/api/services/plusi/model/DataPerson;->photoUrl:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v7, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v8, v9

    goto :goto_1

    :cond_2
    move-object v3, v7

    goto :goto_0
.end method
