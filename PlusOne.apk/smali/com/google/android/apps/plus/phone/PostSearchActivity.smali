.class public Lcom/google/android/apps/plus/phone/PostSearchActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "PostSearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

.field private mSelectedTab:I

.field private mTabBest:Landroid/widget/FrameLayout;

.field private mTabRecent:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method

.method private configureFragment()V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mSelectedTab:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->setSelectionMode(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;->setSelectionMode(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private configureTabButton(Landroid/view/ViewGroup;ZII)V
    .locals 6
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Z
    .param p3    # I
    .param p4    # I

    const/4 v5, 0x0

    if-eqz p2, :cond_0

    move v1, p4

    :goto_0
    if-eqz p2, :cond_1

    sget v0, Lcom/google/android/apps/plus/R$color;->search_tab_selected_text_color:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_0
    move v1, p3

    goto :goto_0

    :cond_1
    sget v0, Lcom/google/android/apps/plus/R$color;->search_tab_default_text_color:I

    goto :goto_1
.end method

.method private configureTabs()V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabBest:Landroid/widget/FrameLayout;

    iget v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mSelectedTab:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_star_gray_16:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_star_red_16:I

    invoke-direct {p0, v3, v0, v4, v5}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->configureTabButton(Landroid/view/ViewGroup;ZII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabRecent:Landroid/widget/FrameLayout;

    iget v3, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mSelectedTab:I

    if-ne v3, v1, :cond_1

    :goto_1
    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_time_grey_16:I

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_time_red_16:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->configureTabButton(Landroid/view/ViewGroup;ZII)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private selectTab(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mSelectedTab:I

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->configureTabs()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->configureFragment()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;-><init>()V

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->post_search_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SEARCH:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedPostSearchFragment;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->configureFragment()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabBest:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->selectTab(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabRecent:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->selectTab(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "selected_tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mSelectedTab:I

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$id;->tab_best:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabBest:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabBest:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->tab_recent:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabRecent:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mTabRecent:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/PostSearchActivity;->configureTabs()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/HostActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "selected_tab"

    iget v1, p0, Lcom/google/android/apps/plus/phone/PostSearchActivity;->mSelectedTab:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
