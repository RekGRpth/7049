.class public final Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;
.super Ljava/lang/Object;
.source "PromoCircleDeltaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;
    }
.end annotation


# instance fields
.field private mActivityPromoCircleDeltaMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;",
            ">;>;"
        }
    .end annotation
.end field

.field private mRequestDeltaMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mRequestDeltaMap:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final addPromoCircleDelta(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/CircleData;

    new-instance v0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    invoke-direct {v0, p2, p3, p4}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/CircleData;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mRequestDeltaMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mRequestDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public final getPromoDeltas(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public final removeDelta(I)Ljava/lang/String;
    .locals 5
    .param p1    # I

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mRequestDeltaMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mRequestDeltaMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    # getter for: Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->mActivityId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;->access$000(Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager$PromoCircleDelta;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/PromoCircleDeltaManager;->mActivityPromoCircleDeltaMap:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method
