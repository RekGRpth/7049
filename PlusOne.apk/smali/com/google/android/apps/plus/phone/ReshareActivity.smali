.class public Lcom/google/android/apps/plus/phone/ReshareActivity;
.super Lcom/google/android/apps/plus/fragments/EsFragmentActivity;
.source "ReshareActivity.java"


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mReshareFragment:Lcom/google/android/apps/plus/fragments/ReshareFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->RESHARE:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public final onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/Fragment;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->reshare_fragment:I

    if-ne v0, v1, :cond_0

    check-cast p1, Lcom/google/android/apps/plus/fragments/ReshareFragment;

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mReshareFragment:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mReshareFragment:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onDiscard()V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    sget v1, Lcom/google/android/apps/plus/R$layout;->reshare_activity:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ReshareActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ReshareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/EsAccount;

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ReshareActivity;->showTitlebar(Z)V

    sget v1, Lcom/google/android/apps/plus/R$string;->reshare_title:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ReshareActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ReshareActivity;->setTitlebarTitle(Ljava/lang/String;)V

    sget v1, Lcom/google/android/apps/plus/R$menu;->post_menu:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/phone/ReshareActivity;->createTitlebarButtons(I)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ReshareActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$menu;->post_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v2, 0x102002c

    if-ne v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/phone/ReshareActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    :goto_0
    return v1

    :cond_0
    sget v2, Lcom/google/android/apps/plus/R$id;->menu_post:I

    if-ne v0, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mReshareFragment:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->reshare()Z

    goto :goto_0

    :cond_1
    sget v2, Lcom/google/android/apps/plus/R$id;->menu_discard:I

    if-ne v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mReshareFragment:Lcom/google/android/apps/plus/fragments/ReshareFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/fragments/ReshareFragment;->onDiscard()V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    sget v1, Lcom/google/android/apps/plus/R$id;->menu_post:I

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    const/4 v1, 0x1

    return v1
.end method

.method protected final onPrepareTitlebarButtons(Landroid/view/Menu;)V
    .locals 4
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$id;->menu_post:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/fragments/EsFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ReshareActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ReshareActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected final onTitlebarLabelClick()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/ReshareActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/ReshareActivity;->goHome(Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method
