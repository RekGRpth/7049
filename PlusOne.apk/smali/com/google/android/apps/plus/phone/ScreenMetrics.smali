.class public final Lcom/google/android/apps/plus/phone/ScreenMetrics;
.super Ljava/lang/Object;
.source "ScreenMetrics.java"


# static fields
.field private static final MAX_PHOTO_COLUMN_DIPS:F

.field private static sInstance:Lcom/google/android/apps/plus/phone/ScreenMetrics;


# instance fields
.field public final itemMargin:I

.field public final longDimension:I

.field public final screenDisplayType:I

.field public final shortDimension:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_TILES:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x43160000

    :goto_0
    sput v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->MAX_PHOTO_COLUMN_DIPS:F

    return-void

    :cond_0
    const/high16 v0, 0x43960000

    goto :goto_0
.end method

.method private constructor <init>(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->shortDimension:I

    iput p2, p0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    iput p3, p0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    iput p4, p0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->itemMargin:I

    return-void
.end method

.method public static getAlbumColumns(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    const/high16 v0, 0x43fa0000

    const/high16 v1, 0x41a00000

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getColumns(Landroid/content/Context;FF)I

    move-result v0

    return v0
.end method

.method private static getColumns(Landroid/content/Context;FF)I
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # F
    .param p2    # F

    const/4 v9, 0x2

    const/4 v10, 0x1

    const/4 v8, 0x0

    const-string v11, "window"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v6, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eqz v6, :cond_0

    if-eqz v3, :cond_0

    iget v11, v1, Landroid/util/DisplayMetrics;->density:F

    cmpl-float v11, v11, v8

    if-nez v11, :cond_3

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v9, :cond_1

    move v4, v10

    :goto_0
    if-eqz v4, :cond_2

    move v8, v9

    :goto_1
    return v8

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    move v8, v10

    goto :goto_1

    :cond_3
    int-to-float v9, v6

    iget v10, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v5, v9, v10

    int-to-float v9, v3

    iget v10, v1, Landroid/util/DisplayMetrics;->density:F

    div-float v2, v9, v10

    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v9

    cmpg-float v10, v2, v5

    if-gez v10, :cond_4

    const/high16 v8, 0x41a00000

    :cond_4
    sub-float v0, v9, v8

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result v8

    div-float v8, v5, v8

    float-to-double v8, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v8, v8

    goto :goto_1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;
    .locals 13
    .param p0    # Landroid/content/Context;

    sget-object v11, Lcom/google/android/apps/plus/phone/ScreenMetrics;->sInstance:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-eqz v11, :cond_0

    sget-object v11, Lcom/google/android/apps/plus/phone/ScreenMetrics;->sInstance:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    :goto_0
    return-object v11

    :cond_0
    const-string v11, "window"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v11

    iget v11, v11, Landroid/content/res/Configuration;->orientation:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_2

    const/4 v5, 0x1

    :goto_1
    iget v9, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-eqz v9, :cond_1

    if-eqz v3, :cond_1

    iget v11, v0, Landroid/util/DisplayMetrics;->density:F

    const/4 v12, 0x0

    cmpl-float v11, v11, v12

    if-nez v11, :cond_3

    :cond_1
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v11, Lcom/google/android/apps/plus/R$dimen;->card_margin_percentage:I

    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    invoke-static {v9, v3}, Ljava/lang/Math;->min(II)I

    move-result v11

    int-to-float v11, v11

    mul-float/2addr v11, v6

    float-to-int v4, v11

    new-instance v11, Lcom/google/android/apps/plus/phone/ScreenMetrics;

    if-eqz v5, :cond_5

    move v12, v3

    :goto_3
    if-eqz v5, :cond_6

    :goto_4
    invoke-direct {v11, v12, v9, v1, v4}, Lcom/google/android/apps/plus/phone/ScreenMetrics;-><init>(IIII)V

    sput-object v11, Lcom/google/android/apps/plus/phone/ScreenMetrics;->sInstance:Lcom/google/android/apps/plus/phone/ScreenMetrics;

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    :cond_3
    int-to-float v11, v9

    iget v12, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v8, v11, v12

    int-to-float v11, v3

    iget v12, v0, Landroid/util/DisplayMetrics;->density:F

    div-float v2, v11, v12

    const v11, 0x44098000

    cmpl-float v11, v8, v11

    if-ltz v11, :cond_4

    const v11, 0x44098000

    cmpl-float v11, v2, v11

    if-ltz v11, :cond_4

    const/4 v1, 0x1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    move v12, v9

    goto :goto_3

    :cond_6
    move v9, v3

    goto :goto_4
.end method

.method public static getPhotoColumns(Landroid/content/Context;)I
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v0, Lcom/google/android/apps/plus/phone/ScreenMetrics;->MAX_PHOTO_COLUMN_DIPS:F

    const/high16 v1, 0x41a00000

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getColumns(Landroid/content/Context;FF)I

    move-result v0

    return v0
.end method
