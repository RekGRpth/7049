.class public final Lcom/google/android/apps/plus/phone/SignOnManager;
.super Ljava/lang/Object;
.source "SignOnManager.java"

# interfaces
.implements Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mActivity:Landroid/support/v4/app/FragmentActivity;

.field private final mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mIntent:Landroid/content/Intent;

.field private mIsResumed:Z

.field private mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/phone/SignOnManager;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/phone/SignOnManager;

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->getPendingInstantUploadCount()I

    move-result v0

    return v0
.end method

.method private doSignOut(Z)V
    .locals 5
    .param p1    # Z

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzActions;->SETTINGS_SIGNOUT:Lcom/google/android/apps/plus/analytics/OzActions;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v3, v4, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez p1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_2
    return-void
.end method

.method private getPendingInstantUploadCount()I
    .locals 15

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->INSTANT_UPLOAD_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    const/4 v13, 0x0

    if-eqz v14, :cond_0

    :try_start_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v13

    :cond_0
    if-eqz v14, :cond_1

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_1
    sget-object v3, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOAD_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v9

    const-string v3, "account"

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v9}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    move-object v3, v0

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    const/4 v11, 0x0

    const/4 v10, 0x0

    if-eqz v12, :cond_2

    :try_start_1
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v10

    :cond_2
    if-eqz v12, :cond_3

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_3
    sub-int v2, v10, v11

    add-int/2addr v2, v13

    return v2

    :catchall_0
    move-exception v2

    if-eqz v14, :cond_4

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v2

    :catchall_1
    move-exception v2

    if-eqz v12, :cond_5

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
.end method

.method private setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method private switchAccounts()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->allowDisconnect(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/EsService;->removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/phone/Intents;->getAccountsActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    return-void
.end method


# virtual methods
.method protected final continueSignOut(IZ)V
    .locals 8
    .param p1    # I
    .param p2    # Z

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v4, "SignOnManager.progress_dialog"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/DialogFragment;

    if-eqz v2, :cond_0

    :try_start_0
    invoke-virtual {v2}, Landroid/support/v4/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    if-nez p2, :cond_1

    if-lez p1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$plurals;->sign_out_message:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, p1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    sget v4, Lcom/google/android/apps/plus/R$string;->sign_out_title:I

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    sget v5, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    sget v6, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v1, v4, v5}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "downgrade_account"

    invoke-virtual {v3, v4, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v4, "SignOnManager.confirm_signoff"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/phone/SignOnManager;->doSignOut(Z)V

    goto :goto_1

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final isSignedIn()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityResult$6eb84b56(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :pswitch_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v2, "intent"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x3ff
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "run_oob"

    invoke-virtual {v6, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "intent"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "network_oob"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIntent:Landroid/content/Intent;

    const-string v8, "plus_pages"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/AccountSettingsData;

    iget-object v8, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/plus/oob/OutOfBoxResponseParcelable;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v6

    :goto_1
    invoke-static {v8, v0, v6, v4, v7}, Lcom/google/android/apps/plus/phone/Intents;->getOobIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;Lcom/google/android/apps/plus/content/AccountSettingsData;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_3

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    const/16 v7, 0x3ff

    invoke-virtual {v6, v2, v7}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    move-object v6, v7

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    if-eqz v5, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->hasGaiaId()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez p1, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$string;->plus_page_reminder:I

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getDisplayName()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v6, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v6, v1, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method public final onDialogCanceled$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogListClick(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogNegativeClick$20f9a4b7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public final onDialogPositiveClick(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v0, "downgrade_account"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/phone/SignOnManager;->doSignOut(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/phone/SignOnManager;->doSignOut(Z)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    return-void
.end method

.method public final onResume()Z
    .locals 5

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v3, "SignOnManager.confirm_signoff"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/fragments/AlertFragmentDialog;->setListener(Lcom/google/android/apps/plus/fragments/AlertFragmentDialog$AlertDialogListener;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/phone/SignOnManager;->setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mResultAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mOobAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v2, :cond_3

    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    :cond_1
    :goto_1
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/SignOnManager;->switchAccounts()V

    goto :goto_1

    :cond_5
    iput-boolean v4, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mIsResumed:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initiateConnection(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_1
.end method

.method public final signOut(Z)V
    .locals 4
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mActivity:Landroid/support/v4/app/FragmentActivity;

    sget v3, Lcom/google/android/apps/plus/R$string;->sign_out_pending:I

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/SignOnManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v2, "SignOnManager.progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/fragments/ProgressFragmentDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/plus/phone/SignOnManager$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/phone/SignOnManager$1;-><init>(Lcom/google/android/apps/plus/phone/SignOnManager;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/phone/SignOnManager$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
