.class public Lcom/google/android/apps/plus/phone/SquareMemberListActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "SquareMemberListActivity.java"


# instance fields
.field private mFragment:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/phone/SquareMemberListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareMemberListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    return-object v0
.end method

.method protected final getContentView()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->host_dialog_activity:I

    return v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->SQUARE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareMemberListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/SquareMemberListActivity;->mFragment:Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/HostedSquareMemberListFragment;->onSearchRequested()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
