.class final Lcom/google/android/apps/plus/phone/StreamAdapter$3;
.super Ljava/lang/Object;
.source "StreamAdapter.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/phone/StreamAdapter;->bindStreamView(Landroid/view/View;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

.field final synthetic val$animatedView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/StreamAdapter;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$3;->this$0:Lcom/google/android/apps/plus/phone/StreamAdapter;

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$3;->val$animatedView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$3;->val$animatedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$3;->val$animatedView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setRotationX(F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/StreamAdapter$3;->val$animatedView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method
