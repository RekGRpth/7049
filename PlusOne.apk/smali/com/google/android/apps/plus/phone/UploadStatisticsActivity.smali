.class public Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;
.super Lcom/google/android/apps/plus/phone/HostActivity;
.source "UploadStatisticsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/HostActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected final createDefaultFragment()Landroid/support/v4/app/Fragment;
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/plus/fragments/UploadStatisticsFragment;-><init>()V

    return-object v0
.end method

.method public final getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzViews;->UNKNOWN:Lcom/google/android/apps/plus/analytics/OzViews;

    return-object v0
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/phone/HostActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->isIntentAccountActive()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UploadStatisticsActivity;->finish()V

    :cond_0
    return-void
.end method
