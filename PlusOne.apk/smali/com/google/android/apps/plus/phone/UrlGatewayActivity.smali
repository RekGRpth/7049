.class public Lcom/google/android/apps/plus/phone/UrlGatewayActivity;
.super Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;
.source "UrlGatewayActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/phone/EsUrlGatewayActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->mRequestType:I

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->mRequestType:I

    const/16 v2, 0x1f

    if-ne v1, v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->redirectToBrowser()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->isReadyToRedirect()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->redirect()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const/high16 v1, 0x42800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/UrlGatewayActivity;->finish()V

    goto :goto_0
.end method
