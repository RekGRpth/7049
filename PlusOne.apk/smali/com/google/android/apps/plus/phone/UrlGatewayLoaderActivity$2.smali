.class final Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;
.super Ljava/lang/Object;
.source "UrlGatewayLoaderActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v0, v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileId:Ljava/lang/String;

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$000(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v2, v2, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v3, v3, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    invoke-static {v1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$000(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v2, v2, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v4, v4, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/ProfileLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v1, 0x1

    check-cast p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;

    instance-of v0, p1, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    check-cast p1, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$VanityUrlLoader;->isConnectionError()Z

    move-result v3

    # setter for: Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z
    invoke-static {v0, v3}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$102(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;Z)Z

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    # getter for: Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z
    invoke-static {v3}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$100(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;)Z

    move-result v3

    if-nez v3, :cond_4

    iget v3, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-eqz v3, :cond_4

    :goto_1
    iput-boolean v1, v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mProfileIdValidated:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v1, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->displayName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mName:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget-object v1, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->gaiaId:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mGaiaId:Ljava/lang/String;

    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2$1;-><init>(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/plus/fragments/ProfileLoader;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity$2;->this$0:Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;

    iget v0, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;->profileState:I

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_2
    # setter for: Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->mConnectionError:Z
    invoke-static {v3, v0}, Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;->access$102(Lcom/google/android/apps/plus/phone/UrlGatewayLoaderActivity;Z)Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Lcom/google/android/apps/plus/content/EsPeopleData$ProfileAndContactData;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
