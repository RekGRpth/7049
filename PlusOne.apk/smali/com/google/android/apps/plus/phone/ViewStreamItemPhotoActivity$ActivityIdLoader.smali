.class final Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;
.super Lcom/google/android/apps/plus/phone/EsCursorLoader;
.source "ViewStreamItemPhotoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActivityIdLoader"
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mStreamItemUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/net/Uri;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mStreamItemUri:Landroid/net/Uri;

    return-void
.end method

.method private loadMediaFromDatabase$73948607(Landroid/content/ContentResolver;Ljava/lang/String;)[B
    .locals 8
    .param p1    # Landroid/content/ContentResolver;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_VIEW_BY_ACTIVITY_ID_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "embed_media"

    aput-object v0, v2, v4

    move-object v0, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v7

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    new-array v7, v0, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v7, v3

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final esLoadInBackground()Landroid/database/Cursor;
    .locals 15

    const/4 v12, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mStreamItemUri:Landroid/net/Uri;

    # getter for: Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->STREAM_ITEM_PHOTO_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->access$000()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v1, 0x1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    const/4 v1, 0x2

    :try_start_1
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    if-eqz v4, :cond_0

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->loadMediaFromDatabase$73948607(Landroid/content/ContentResolver;Ljava/lang/String;)[B

    move-result-object v11

    if-nez v11, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->start()V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->getException()Ljava/lang/Exception;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "ViewStreamItemActivity"

    const-string v3, "Cannot download activity"

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->getException()Ljava/lang/Exception;

    move-result-object v1

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v14, 0x0

    :goto_1
    if-eqz v14, :cond_0

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity$ActivityIdLoader;->loadMediaFromDatabase$73948607(Landroid/content/ContentResolver;Ljava/lang/String;)[B

    move-result-object v11

    :cond_0
    new-instance v13, Lcom/google/android/apps/plus/phone/EsMatrixCursor;

    # getter for: Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->ACTIVITY_RESULT_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/apps/plus/phone/ViewStreamItemPhotoActivity;->access$100()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;-><init>([Ljava/lang/String;)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v12, v1, v2

    const/4 v2, 0x1

    aput-object v4, v1, v2

    const/4 v2, 0x2

    aput-object v11, v1, v2

    invoke-virtual {v13, v1}, Lcom/google/android/apps/plus/phone/EsMatrixCursor;->addRow([Ljava/lang/Object;)V

    return-object v13

    :catchall_0
    move-exception v1

    move-object v4, v9

    :goto_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->hasError()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "ViewStreamItemActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Cannot download activity: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->getErrorCode()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v14, 0x0

    goto :goto_1

    :cond_2
    const/4 v14, 0x1

    goto :goto_1

    :catchall_1
    move-exception v1

    goto :goto_2

    :cond_3
    move-object v4, v9

    goto :goto_0
.end method
