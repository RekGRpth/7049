.class public final Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;
.super Ljava/lang/Object;
.source "BlockingC2DMClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;
    }
.end annotation


# instance fields
.field private final mEvent:Ljava/util/concurrent/CountDownLatch;

.field private final mRealTimeChatListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

.field private mRegistrationToken:Ljava/lang/String;

.field private mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

.field private final mTimeoutMilliseconds:J

.field private mUsed:Z


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .param p1    # J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient$OnC2dmReceivedListener;-><init>(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRealTimeChatListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mEvent:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mTimeoutMilliseconds:J

    return-void
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;Lcom/google/android/apps/plus/service/ServiceResult;)Lcom/google/android/apps/plus/service/ServiceResult;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    return-object p1
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRegistrationToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mEvent:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method public final blockingGetC2dmToken(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mUsed:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This class is single-use."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mUsed:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRealTimeChatListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getOrRequestC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRegistrationToken:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRegistrationToken:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRealTimeChatListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mEvent:Ljava/util/concurrent/CountDownLatch;

    iget-wide v1, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mTimeoutMilliseconds:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "BlockingC2DMClient"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "BlockingC2DMClient"

    const-string v1, "Waiting for C2DM registration timed out."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, -0x2

    const-string v2, "Waiting for C2DM registration timed out."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    if-nez v0, :cond_1

    const-string v0, "BlockingC2DMClient"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "BlockingC2DMClient"

    const-string v1, "Result was not set by service."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    const-string v2, "Result was not set by service."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "BlockingC2DMClient"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "BlockingC2DMClient"

    const-string v2, "Waiting for C2DM registration interrupted."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6
    new-instance v1, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, -0x1

    const-string v3, "Waiting for C2DM registration interrupted."

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRealTimeChatListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    throw v0
.end method

.method public final hasError()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mRegistrationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->mServiceResult:Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
