.class final Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;
.super Ljava/lang/Object;
.source "BunchClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/realtimechat/BunchClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingRequest"
.end annotation


# instance fields
.field mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

.field public mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

.field public mRequestId:I

.field public mRetryCount:I

.field public mTimestamp:J

.field final synthetic this$0:Lcom/google/android/apps/plus/realtimechat/BunchClient;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;JI)V
    .locals 0
    .param p2    # I
    .param p3    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    .param p4    # J
    .param p6    # I

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->this$0:Lcom/google/android/apps/plus/realtimechat/BunchClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequestId:I

    iput-object p3, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    iput-wide p4, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mTimestamp:J

    iput p6, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRetryCount:I

    return-void
.end method
