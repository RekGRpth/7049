.class public final Lcom/google/android/apps/plus/realtimechat/BunchClient;
.super Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
.source "BunchClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;,
        Lcom/google/android/apps/plus/realtimechat/BunchClient$ResponseFailedException;,
        Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;,
        Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;,
        Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;
    }
.end annotation


# instance fields
.field private mBackgroundHandler:Landroid/os/Handler;

.field private mBackgroundThread:Landroid/os/HandlerThread;

.field private mClientVersion:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private mConnected:Z

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

.field private final mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

.field private final mQueuedCommands:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    const-string v5, "bunch"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;-><init>(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient$1;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mHandlerCallback:Landroid/os/Handler$Callback;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-direct {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/realtimechat/BunchClient;Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;)V
    .locals 8
    .param p0    # Lcom/google/android/apps/plus/realtimechat/BunchClient;
    .param p1    # Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    const/4 v7, 0x3

    const/4 v0, 0x0

    const-string v1, "BunchClient"

    invoke-static {v1, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BunchClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "retrySendRequest "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequestId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    monitor-enter p0

    :try_start_0
    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->expectResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "BunchClient"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "BunchClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending command "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] expecting response"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mTimestamp:J

    new-instance v2, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    iput-object v2, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iget-object v2, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RealTimeChat:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->addRequest(Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->retryOnTimeout(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    if-eqz v2, :cond_2

    iget v2, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRetryCount:I

    const-wide/16 v3, 0x3a98

    shl-long v2, v3, v2

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v5, 0x64

    invoke-virtual {v4, v5, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v5, v4, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v4, "BunchClient"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "BunchClient"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bunch request timeout "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v6}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " checking in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->createBatchCommandBuilderWithClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->toByteArray()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendMessage([B)Z

    move-result v0

    :cond_3
    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->shouldEnqueueIfDisconnected(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "BunchClient"

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "BunchClient"

    const-string v2, "queueing"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    new-instance v2, Landroid/util/Pair;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_1
    return-void

    :cond_6
    :try_start_1
    const-string v2, "BunchClient"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "BunchClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending command "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] not expecting response"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    const-string v0, "BunchClient"

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "BunchClient"

    const-string v1, "sent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$100(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private createBatchCommandBuilderWithClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mClientVersion:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static expectResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasReceiptRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasChatMessageRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ChatMessageRequest"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationJoinRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ConversationJoinRequest"

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationListRequest()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ConversationListRequest"

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationPreferenceRequest()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ConversationPreferenceRequest"

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationRenameRequest()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ConversationRenameRequest"

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationRequest()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "ConversationRequest"

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationSearchRequest()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "ConversationSearchRequest"

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasDeviceRegistrationRequest()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "DeviceRegistrationRequest"

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasEventSearchRequest()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "EventSearchRequest"

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasEventStreamRequest()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "EventStreamRequest"

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasGlobalConversationPreferencesRequest()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "GlobalConversationPreferencesRequest"

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasHangoutInviteFinishRequest()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "HangoutInviteFinishRequest"

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasHangoutInviteKeepAliveRequest()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "HangoutInviteKeepAliveRequest"

    goto :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasHangoutInviteReplyRequest()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "HangoutInviteReplyRequest"

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasHangoutRingFinishRequest()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "HangoutRingFinishRequest"

    goto :goto_0

    :cond_e
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasInviteRequest()Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "InviteRequest"

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasLeaveConversationRequest()Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "LeaveConversationRequest"

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasPingRequest()Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "PingRequest"

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasPresenceRequest()Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "PresenceRequest"

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasReceiptRequest()Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "ReceiptRequest"

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasReplyToInviteRequest()Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "ReplyToInviteRequest"

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasSetAclsRequest()Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "SetAclsRequest"

    goto/16 :goto_0

    :cond_15
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasSuggestionsRequest()Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "SuggestionsRequest"

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasTileEventRequest()Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "TileEventRequest"

    goto/16 :goto_0

    :cond_17
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasTypingRequest()Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "TypingRequest"

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasUserCreationRequest()Z

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "UserCreationRequest"

    goto/16 :goto_0

    :cond_19
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasUserInfoRequest()Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "UserInfoRequest"

    goto/16 :goto_0

    :cond_1a
    const-string v0, "Unknown"

    goto/16 :goto_0
.end method

.method private static getResponseTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasChatMessageResponse()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ChatMessageResponse"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationJoinResponse()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ConversationJoinResponse"

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationListResponse()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ConversationListResponse"

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationPreferenceResponse()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "ConversationPreferenceResponse"

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationRenameResponse()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ConversationRenameResponse"

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationResponse()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "ConversationResponse"

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationSearchResponse()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "ConversationSearchResponse"

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasDeviceRegistrationResponse()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "DeviceRegistrationResponse"

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasError()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "Error"

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasEventSearchResponse()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "EventSearchResponse"

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasEventSteamResponse()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "EventSteamResponse"

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasGlobalConversationPreferencesResponse()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "GlobalConversationPreferencesResponse"

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteFinishResponse()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "HangoutInviteFinishResponse"

    goto :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteKeepAliveResponse()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "HangoutInviteKeepAliveResponse"

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteReplyResponse()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "HangoutInviteReplyResponse"

    goto :goto_0

    :cond_e
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutInviteResponse()Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "HangoutInviteResponse"

    goto/16 :goto_0

    :cond_f
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasHangoutRingFinishResponse()Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "HangoutRingFinishResponse"

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasInviteResponse()Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "InviteResponse"

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasLeaveConversationResponse()Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "LeaveConversationResponse"

    goto/16 :goto_0

    :cond_12
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPingResponse()Z

    move-result v0

    if-eqz v0, :cond_13

    const-string v0, "PingResponse"

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPresenceResponse()Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "PresenceResponse"

    goto/16 :goto_0

    :cond_14
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasReceiptResponse()Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "ReceiptResponse"

    goto/16 :goto_0

    :cond_15
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasReplyToInviteResponse()Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "ReplyToInviteResponse"

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSetAclsResponse()Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "SetAclsResponse"

    goto/16 :goto_0

    :cond_17
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "SuggestionsResponse"

    goto/16 :goto_0

    :cond_18
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasTileEventResponse()Z

    move-result v0

    if-eqz v0, :cond_19

    const-string v0, "TileEventResponse"

    goto/16 :goto_0

    :cond_19
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasTypingResponse()Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "TypingResponse"

    goto/16 :goto_0

    :cond_1a
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserCreationResponse()Z

    move-result v0

    if-eqz v0, :cond_1b

    const-string v0, "UserCreationResponse"

    goto/16 :goto_0

    :cond_1b
    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserInfoResponse()Z

    move-result v0

    if-eqz v0, :cond_1c

    const-string v0, "UserInfoResponse"

    goto/16 :goto_0

    :cond_1c
    const-string v0, "Unknown"

    goto/16 :goto_0
.end method

.method private handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V
    .locals 11
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .param p3    # I
    .param p4    # I

    const/4 v10, 0x5

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->ERROR:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-eq p2, v1, :cond_0

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->ERROR_UNEXPECTED:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-eq p2, v1, :cond_0

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->ERROR_TEMPORARY:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-ne p2, v1, :cond_3

    :cond_0
    const/4 v1, 0x3

    if-ge p4, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->retry(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v3

    const-wide/16 v1, 0x3e8

    shl-long v8, v1, p4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    add-int/lit8 v6, p4, 0x1

    move-object v1, p0

    move v2, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;JI)V

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1, v7, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v1, "BunchClient"

    invoke-static {v1, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BunchClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Bunch server error: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " retrying in "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "BunchClient"

    invoke-static {v1, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BunchClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Bunch server error: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " giving up"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v1, "BunchClient"

    invoke-static {v1, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BunchClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Bunch server error: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " fatal"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private processResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;Ljava/util/List;)V
    .locals 41
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .param p2    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    const/16 v23, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v36

    const-wide/32 v38, 0x15f90

    sub-long v20, v36, v38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->trimOutdatedRequestIds(J)Ljava/util/List;

    move-result-object v34

    invoke-interface/range {v34 .. v34}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v33

    check-cast v33, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    if-eqz v33, :cond_0

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->retryOnTimeout(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v36

    if-eqz v36, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    move-object/from16 v36, v0

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->addRequest(Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v36

    monitor-exit p0

    throw v36

    :cond_1
    :try_start_1
    const-string v36, "BunchClient"

    const/16 v37, 0x4

    invoke-static/range {v36 .. v37}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v36

    if-eqz v36, :cond_0

    const-string v36, "BunchClient"

    new-instance v37, Ljava/lang/StringBuilder;

    const-string v38, "request "

    invoke-direct/range {v37 .. v38}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v38, v0

    invoke-static/range {v38 .. v38}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " type ["

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, "] timed out"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v12, Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;

    const/16 v36, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v36

    invoke-direct {v12, v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;B)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v33

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-object/from16 v38, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v38

    invoke-static {v0, v1, v2, v12}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->getData(Ljava/lang/String;)Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    move-result-object v23

    if-eqz v23, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    move-object/from16 v36, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v37

    invoke-virtual/range {v36 .. v37}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->removeRequest(Ljava/lang/String;)V

    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v27

    if-eqz v23, :cond_b

    const-string v36, "BunchClient"

    const/16 v37, 0x4

    invoke-static/range {v36 .. v37}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v36

    if-eqz v36, :cond_4

    const-string v36, "BunchClient"

    new-instance v37, Ljava/lang/StringBuilder;

    const-string v38, "Received "

    invoke-direct/range {v37 .. v38}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getResponseTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " ["

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getRequestClientId()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, "] processing"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    new-instance v29, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequestId:I

    move/from16 v36, v0

    const/16 v37, 0x1

    move-object/from16 v0, v29

    move/from16 v1, v36

    move/from16 v2, v37

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    move-object/from16 v0, p3

    move-object/from16 v1, v29

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v16, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;

    invoke-direct/range {v16 .. v16}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;-><init>()V

    new-instance v22, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;

    invoke-direct/range {v22 .. v22}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSerializedSize()I

    move-result v36

    move/from16 v0, v36

    int-to-long v0, v0

    move-wide/from16 v36, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;->setBytesTransferred(J)V

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getSerializedSize()I

    move-result v36

    move/from16 v0, v36

    int-to-long v0, v0

    move-wide/from16 v36, v0

    move-object/from16 v0, v22

    move-wide/from16 v1, v36

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;->setBytesTransferred(J)V

    new-instance v7, Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v7, v0, v1}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;-><init>(Lorg/apache/http/io/HttpTransportMetrics;Lorg/apache/http/io/HttpTransportMetrics;)V

    invoke-virtual {v7}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;->incrementRequestCount()V

    invoke-virtual {v7}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;->incrementResponseCount()V

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v0, v7}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->setConnectionMetrics(Lorg/apache/http/HttpConnectionMetrics;)V

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction()V

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onStartResultProcessing()V

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequestId:I

    move/from16 v25, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRetryCount:I

    move/from16 v30, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    move-object/from16 v19, v0

    sget-object v31, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserInfoResponse()Z

    move-result v36

    if-eqz v36, :cond_e

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getUserInfoResponse()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v37

    invoke-interface/range {v37 .. v37}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v37

    const-string v38, "BunchClient"

    const/16 v39, 0x3

    invoke-static/range {v38 .. v39}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v38

    if-eqz v38, :cond_5

    const-string v38, "BunchClient"

    new-instance v39, Ljava/lang/StringBuilder;

    const-string v40, "updateAcl "

    invoke-direct/range {v39 .. v40}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getAcl()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    move-result-object v40

    invoke-virtual/range {v39 .. v40}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v39

    invoke-virtual/range {v39 .. v39}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    invoke-static/range {v38 .. v39}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getAcl()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->getNumber()I

    move-result v38

    const/16 v39, 0x1

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    sget v38, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_key:I

    move-object/from16 v0, v36

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v38

    sget v39, Lcom/google/android/apps/plus/R$string;->key_acl_setting_anyone:I

    invoke-virtual/range {v38 .. v39}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v38

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_6
    :goto_1
    invoke-interface/range {v37 .. v37}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_7
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndResultProcessing()V

    move-object/from16 v0, v23

    iget-wide v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mTimestamp:J

    move-wide/from16 v36, v0

    invoke-static/range {v36 .. v37}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    const-string v36, "BunchClient"

    const/16 v37, 0x4

    invoke-static/range {v36 .. v37}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v36

    if-eqz v36, :cond_8

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v36, v0

    if-eqz v36, :cond_8

    const-string v36, "BunchClient"

    new-instance v37, Ljava/lang/StringBuilder;

    const-string v38, "command type ["

    invoke-direct/range {v37 .. v38}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v38, v0

    invoke-static/range {v38 .. v38}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, "] roundTripTime "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Long;->longValue()J

    move-result-wide v38

    sub-long v38, v27, v38

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " ms processingTime "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    sub-long v38, v13, v27

    invoke-virtual/range {v37 .. v39}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " inBytes "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSerializedSize()I

    move-result v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " outBytes "

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getSerializedSize()I

    move-result v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_9

    new-instance v36, Lcom/google/android/apps/plus/realtimechat/BunchClient$ResponseFailedException;

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient$ResponseFailedException;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)V

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    move-object/from16 v38, v0

    const/16 v39, 0x0

    invoke-static/range {v36 .. v39}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    :cond_a
    :goto_3
    return-void

    :cond_b
    const-string v36, "BunchClient"

    const/16 v37, 0x4

    invoke-static/range {v36 .. v37}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v36

    if-eqz v36, :cond_a

    const-string v36, "BunchClient"

    new-instance v37, Ljava/lang/StringBuilder;

    const-string v38, "Received "

    invoke-direct/range {v37 .. v38}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getResponseTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, " ["

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getRequestClientId()Ljava/lang/String;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, "] ignoring"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_c
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getAcl()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->getNumber()I

    move-result v38

    const/16 v39, 0x2

    move/from16 v0, v38

    move/from16 v1, v39

    if-ne v0, v1, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    sget v38, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_key:I

    move-object/from16 v0, v36

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v38

    sget v39, Lcom/google/android/apps/plus/R$string;->key_acl_setting_extended_circles:I

    invoke-virtual/range {v38 .. v39}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v38

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    :cond_d
    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getAcl()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;

    move-result-object v36

    invoke-virtual/range {v36 .. v36}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest$Acl;->getNumber()I

    move-result v36

    const/16 v38, 0x3

    move/from16 v0, v36

    move/from16 v1, v38

    if-ne v0, v1, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    sget v38, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_key:I

    move-object/from16 v0, v36

    move/from16 v1, v38

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v38

    sget v39, Lcom/google/android/apps/plus/R$string;->key_acl_setting_my_circles:I

    invoke-virtual/range {v38 .. v39}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v36

    move-object/from16 v2, v38

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_1

    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSetAclsResponse()Z

    move-result v36

    if-nez v36, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasTypingResponse()Z

    move-result v36

    if-nez v36, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPresenceResponse()Z

    move-result v36

    if-nez v36, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasTileEventResponse()Z

    move-result v36

    if-nez v36, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasReceiptResponse()Z

    move-result v36

    if-nez v36, :cond_7

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasPingResponse()Z

    move-result v36

    if-eqz v36, :cond_10

    const-string v36, "BunchClient"

    const/16 v37, 0x4

    invoke-static/range {v36 .. v37}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v36

    if-eqz v36, :cond_f

    const-string v36, "BunchClient"

    const-string v37, "Ping response from backend"

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    if-eqz v19, :cond_7

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;->onPingReceived(Lcom/google/android/apps/plus/realtimechat/BunchClient;)V

    goto/16 :goto_2

    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserCreationResponse()Z

    move-result v36

    if-eqz v36, :cond_12

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getUserCreationResponse()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_11

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v35

    move-object/from16 v3, p2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->processUserCreationResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_2

    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSuggestionsResponse()Z

    move-result v36

    if-eqz v36, :cond_14

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_13

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v32

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->processSuggestionsResponse$541cf8e7(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    goto/16 :goto_2

    :cond_14
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationListResponse()Z

    move-result v36

    if-eqz v36, :cond_16

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationListResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_15

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_15
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, p2

    invoke-static {v0, v1, v8, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processConversationListResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_2

    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasEventSteamResponse()Z

    move-result v36

    if-eqz v36, :cond_18

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getEventSteamResponse()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_17

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_17
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, p2

    invoke-static {v0, v1, v11, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processEventStreamResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_2

    :cond_18
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationResponse()Z

    move-result v36

    if-eqz v36, :cond_1a

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_19

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_19
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, p2

    invoke-static {v0, v1, v10, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processConversationResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_2

    :cond_1a
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasChatMessageResponse()Z

    move-result v36

    if-eqz v36, :cond_1c

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getChatMessageResponse()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1b

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_1b
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, p2

    invoke-static {v0, v1, v5, v2}, Lcom/google/android/apps/plus/content/EsConversationsData;->processChatMessageResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_2

    :cond_1c
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasInviteResponse()Z

    move-result v36

    if-eqz v36, :cond_1e

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1d

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v38, v0

    move-object/from16 v0, v36

    move-object/from16 v1, v37

    move-object/from16 v2, v17

    move-object/from16 v3, v38

    move-object/from16 v4, p2

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->processInviteResponse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    goto/16 :goto_2

    :cond_1e
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasConversationPreferenceResponse()Z

    move-result v36

    if-eqz v36, :cond_20

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getConversationPreferenceResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_1f

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsConversationsData;->processConversationPreferenceResponse$43e73c50(Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)V

    goto/16 :goto_2

    :cond_20
    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasLeaveConversationResponse()Z

    move-result v36

    if-eqz v36, :cond_22

    invoke-virtual/range {p1 .. p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getLeaveConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v31

    sget-object v36, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-object/from16 v0, v31

    move-object/from16 v1, v36

    if-eq v0, v1, :cond_21

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, v31

    move/from16 v3, v25

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->handleError(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;II)V

    goto/16 :goto_2

    :cond_21
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v36

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v37

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-object/from16 v38, v0

    invoke-static/range {v36 .. v38}, Lcom/google/android/apps/plus/content/EsConversationsData;->processLeaveConversationResponse$6cb3bb58(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    goto/16 :goto_2

    :cond_22
    const-string v36, "BunchClient"

    const/16 v37, 0x5

    invoke-static/range {v36 .. v37}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v36

    if-eqz v36, :cond_7

    const-string v36, "BunchClient"

    const-string v37, "Unexpected response from bunch server"

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private static retryOnTimeout(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasUserCreationRequest()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationListRequest()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasEventStreamRequest()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized sendQueuedCommands()V
    .locals 10

    monitor-enter p0

    :try_start_0
    const-string v4, "BunchClient"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "BunchClient"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " pending commands"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->createBatchCommandBuilderWithClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasChatMessageRequest()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x4e20

    cmp-long v4, v6, v8

    if-lez v4, :cond_2

    const/4 v4, 0x0

    :goto_1
    if-eqz v4, :cond_4

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v0, v4}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->getData(Ljava/lang/String;)Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    move-result-object v3

    if-nez v3, :cond_3

    const-string v4, "BunchClient"

    const/4 v6, 0x5

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "BunchClient"

    const-string v6, "null pendingRequest in sendQueuedCommand"

    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    :cond_2
    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    :try_start_3
    new-instance v4, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v4}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    iput-object v4, v3, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iget-object v6, v3, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v4, "RealTimeChat:"

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v4, "BunchClient"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "BunchClient"

    const-string v6, "dropping outdated command"

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->removeRequest(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->toByteArray()[B

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendMessage([B)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v4}, Ljava/util/Collection;->clear()V

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void
.end method

.method private declared-synchronized shouldEnqueueIfDisconnected(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z
    .locals 12
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    const/4 v9, 0x1

    const/4 v10, 0x0

    const/4 v8, 0x0

    monitor-enter p0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v8

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasInviteRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasEventStreamRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationRenameRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasLeaveConversationRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasReceiptRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasReplyToInviteRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasSuggestionsRequest()Z

    move-result v11

    if-nez v11, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasSetAclsRequest()Z

    move-result v11

    if-eqz v11, :cond_3

    :cond_2
    move v8, v9

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasChatMessageRequest()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getChatMessageRequest()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;->hasMessageClientId()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;->getMessageClientId()Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget-object v11, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasChatMessageRequest()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getChatMessageRequest()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;->hasMessageClientId()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getChatMessageRequest()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;->getMessageClientId()Ljava/lang/String;

    move-result-object v6

    :goto_2
    if-eqz v6, :cond_4

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    goto :goto_0

    :cond_5
    move-object v3, v10

    goto :goto_1

    :cond_6
    move-object v6, v10

    goto :goto_2

    :cond_7
    move v8, v9

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationRequest()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getConversationRequest()Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;->hasConversationClientId()Z

    move-result v11

    if-eqz v11, :cond_a

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;->getConversationClientId()Ljava/lang/String;

    move-result-object v1

    :goto_3
    iget-object v11, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasChatMessageRequest()Z

    move-result v11

    if-eqz v11, :cond_9

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getConversationRequest()Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;->hasConversationClientId()Z

    move-result v11

    if-eqz v11, :cond_b

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;->getConversationClientId()Ljava/lang/String;

    move-result-object v6

    :goto_4
    if-eqz v6, :cond_9

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    if-eqz v11, :cond_9

    goto/16 :goto_0

    :cond_a
    move-object v1, v10

    goto :goto_3

    :cond_b
    move-object v6, v10

    goto :goto_4

    :cond_c
    move v8, v9

    goto/16 :goto_0

    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8
.end method


# virtual methods
.method public final checkResponseReceived(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V
    .locals 9
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    const/4 v8, 0x3

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->getData(Ljava/lang/String;)Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->removeRequest(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;B)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    iget-object v7, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-static {v5, v6, v7, v0}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    const-string v5, "BunchClient"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "BunchClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Retrying command "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] expecting response"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v5, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRetryCount:I

    add-int/lit8 v3, v5, 0x1

    if-ge v3, v8, :cond_5

    invoke-static {p1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->retry(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    iget v5, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequestId:I

    add-int/lit8 v6, v3, 0x1

    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v7, v5, v6}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommands(Ljava/util/Collection;II)Z

    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v4, 0x0

    if-eqz v1, :cond_7

    iget-boolean v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    if-eqz v5, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->createBatchCommandBuilderWithClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->toByteArray()[B

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendMessage([B)Z

    move-result v4

    :cond_2
    if-nez v4, :cond_6

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->shouldEnqueueIfDisconnected(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "BunchClient"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    const-string v5, "BunchClient"

    const-string v6, "queueing"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    new-instance v6, Landroid/util/Pair;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v6, v7, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v5, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_1
    return-void

    :cond_5
    const/4 v1, 0x0

    :try_start_1
    const-string v5, "BunchClient"

    const/4 v6, 0x5

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "BunchClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Bunch request timeout "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v7}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " giving up"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_6
    const-string v5, "BunchClient"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "BunchClient"

    const-string v6, "sent"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_7
    const-string v5, "BunchClient"

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "BunchClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "response received for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final declared-synchronized connected()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized disconnect()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    invoke-super {p0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized hasPendingCommands()Z
    .locals 12

    const/4 v6, 0x1

    const/4 v7, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    const-wide/32 v10, 0x15f90

    sub-long v2, v8, v10

    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v8, v2, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->trimOutdatedRequestIds(J)Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    if-eqz v4, :cond_0

    iget-object v8, v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {v8}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->retryOnTimeout(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    iget-object v9, v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->addRequest(Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    :cond_1
    :try_start_1
    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    iget-object v9, v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->removeRequest(Ljava/lang/String;)V

    const-string v8, "BunchClient"

    const/4 v9, 0x4

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "BunchClient"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "request "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {v10}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " type ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v10}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] timed out"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;

    const/4 v8, 0x0

    invoke-direct {v0, p0, v8}, Lcom/google/android/apps/plus/realtimechat/BunchClient$TimedOutException;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;B)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v9

    iget-object v10, v4, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-static {v8, v9, v10, v0}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    goto :goto_0

    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    invoke-interface {v8}, Ljava/util/Collection;->isEmpty()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    if-nez v8, :cond_5

    :cond_4
    :goto_1
    monitor-exit p0

    return v6

    :cond_5
    :try_start_2
    const-string v8, "BunchClient"

    const/4 v9, 0x2

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_6

    const-string v8, "BunchClient"

    const-string v9, "hasPendingCommands"

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->dump()V

    :cond_6
    iget-object v8, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->isEmpty()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v8

    if-eqz v8, :cond_4

    move v6, v7

    goto :goto_1
.end method

.method protected final declared-synchronized onConnected()V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v1, "BunchClient"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BunchClient"

    const-string v2, "onConnected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    if-eqz v0, :cond_1

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;->onConnected(Lcom/google/android/apps/plus/realtimechat/BunchClient;)V

    :cond_1
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "BunchHandlerThread"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mHandlerCallback:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected final declared-synchronized onDisconnected(I)V
    .locals 8
    .param p1    # I

    monitor-enter p0

    :try_start_0
    const-string v5, "BunchClient"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "BunchClient"

    const-string v6, "Disconnected from server"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->getRequestIds()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->getData(Ljava/lang/String;)Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v5, "BunchClient"

    const/4 v6, 0x4

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "BunchClient"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "request "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {v7}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " type ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] failed due to disconnect"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->removeRequest(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5

    :cond_3
    :try_start_1
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->quit()Z

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundThread:Landroid/os/HandlerThread;

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v6, 0x65

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v6, 0x64

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    :cond_5
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    if-eqz v1, :cond_6

    invoke-interface {v1, p0, p1}, Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;->onDisconnected(Lcom/google/android/apps/plus/realtimechat/BunchClient;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_6
    monitor-exit p0

    return-void
.end method

.method protected final onMessageReceived([B)V
    .locals 14
    .param p1    # [B

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    if-nez v1, :cond_2

    const-string v1, "BunchClient"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BunchClient"

    const-string v2, "Message received after disconnect"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    return-void

    :cond_2
    monitor-exit p0

    :try_start_1
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->parseFrom([B)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getCurrentConversationRowId()Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;-><init>(Ljava/lang/Long;)V

    new-instance v5, Ljava/util/LinkedList;

    invoke-direct {v5}, Ljava/util/LinkedList;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getStateUpdateList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    const-string v2, "BunchClient"

    const/4 v7, 0x4

    invoke-static {v2, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v7, "BunchClient"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v2, "Received stateUpdate "

    invoke-direct {v8, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasChatMessage()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "ChatMessage"

    :goto_2
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-static {v2, v7, v1, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->processBunchServerUpdate(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V
    :try_end_1
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v1, "BunchClient"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "BunchClient"

    const-string v2, "Invalid BatchCommand message received"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    :cond_4
    :try_start_2
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasGroupConversationRename()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "GroupConversationRename"

    goto :goto_2

    :cond_5
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasInvalidateLocalCache()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "InvalidateLocalCache"

    goto :goto_2

    :cond_6
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasMembershipChange()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "MembershipChange"

    goto :goto_2

    :cond_7
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasMigration()Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "Migration"

    goto :goto_2

    :cond_8
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasNewConversation()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "NewConversation"

    goto :goto_2

    :cond_9
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasPresence()Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "Presence"

    goto :goto_2

    :cond_a
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasReceipt()Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "Receipt"

    goto :goto_2

    :cond_b
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasTileEvent()Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "TileEvent"

    goto :goto_2

    :cond_c
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->hasTyping()Z

    move-result v2

    if-eqz v2, :cond_d

    const-string v2, "Typing"

    goto :goto_2

    :cond_d
    const-string v2, "Unknown"

    goto :goto_2

    :cond_e
    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getResponseList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasUserCreationResponse()Z

    move-result v2

    if-eqz v2, :cond_1a

    const/4 v2, 0x1

    :goto_4
    invoke-direct {p0, v1, v3, v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->processResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;Ljava/util/List;)V

    move v4, v2

    goto :goto_3

    :cond_f
    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->shouldTriggerNotifications()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/4 v6, 0x0

    invoke-static {v1, v2, v6}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatNotifications;->update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    :cond_10
    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getClientVersionChanged()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->updateClientVersion()V

    :cond_11
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    monitor-enter p0
    :try_end_2
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v3}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getRequests()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_12
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationListRequest()Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_13
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    if-eqz v2, :cond_13

    iget-object v9, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    if-eqz v9, :cond_13

    iget-object v9, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->hasConversationListRequest()Z

    move-result v9

    if-eqz v9, :cond_13

    iget-object v2, v2, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mRequest:Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getConversationListRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getConversationListRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    move-result-object v9

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getType()Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest$Type;

    move-result-object v10

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getType()Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest$Type;

    move-result-object v11

    if-ne v10, v11, :cond_15

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->hasTimestamp()Z

    move-result v10

    if-eqz v10, :cond_14

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->hasTimestamp()Z

    move-result v10

    if-eqz v10, :cond_14

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getTimestamp()J

    move-result-wide v10

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getTimestamp()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-gtz v10, :cond_14

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_7
    if-nez v2, :cond_12

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_5

    :catchall_1
    move-exception v1

    :try_start_4
    monitor-exit p0

    throw v1
    :try_end_4
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_14
    :try_start_5
    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->hasConversationId()Z

    move-result v10

    if-eqz v10, :cond_15

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->hasConversationId()Z

    move-result v10

    if-eqz v10, :cond_15

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getConversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getConversationId()Ljava/lang/String;

    move-result-object v9

    if-ne v2, v9, :cond_15

    const/4 v2, 0x1

    goto :goto_6

    :cond_15
    const/4 v2, 0x0

    goto :goto_6

    :cond_16
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_17
    if-eqz v4, :cond_18

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendQueuedCommands()V

    :cond_18
    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v6, v1, v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommands(Ljava/util/Collection;II)Z

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mListener:Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;

    if-eqz v1, :cond_1

    invoke-interface {v1, p0, v5}, Lcom/google/android/apps/plus/realtimechat/BunchClient$BunchClientListener;->onResultsReceived(Lcom/google/android/apps/plus/realtimechat/BunchClient;Ljava/util/List;)V
    :try_end_6
    .catch Lcom/google/protobuf/InvalidProtocolBufferException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_0

    :cond_19
    move v2, v3

    goto :goto_7

    :cond_1a
    move v2, v4

    goto/16 :goto_4
.end method

.method public final sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    .param p2    # I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommands(Ljava/util/Collection;II)Z

    move-result v1

    return v1
.end method

.method public final sendCommands(Ljava/util/Collection;II)Z
    .locals 15
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;II)Z"
        }
    .end annotation

    const/4 v12, 0x0

    const-string v2, "BunchClient"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "BunchClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "sendCommands "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    monitor-enter p0

    :try_start_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->expectResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "BunchClient"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "BunchClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Sending command "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " ["

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "] expecting response"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;

    move-object v2, p0

    move/from16 v3, p2

    move/from16 v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;-><init>(Lcom/google/android/apps/plus/realtimechat/BunchClient;ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;JI)V

    new-instance v2, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v2}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    iput-object v2, v1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    iget-object v2, v1, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;->mMetrics:Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "RealTimeChat:"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mPendingRequestList:Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequestList;->addRequest(Ljava/lang/String;Lcom/google/android/apps/plus/realtimechat/BunchClient$PendingRequest;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_3
    :try_start_1
    const-string v2, "BunchClient"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "BunchClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Sending command "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getRequestTypeName(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " ["

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "] not expecting response"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mConnected:Z

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->createBatchCommandBuilderWithClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addAllRequest(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->toByteArray()[B

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendMessage([B)Z

    move-result v12

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-static {v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->retryOnTimeout(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    if-eqz v2, :cond_5

    const-wide/16 v2, 0x3a98

    shl-long v10, v2, p3

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    const/16 v3, 0x64

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v9

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v2, v9, v10, v11}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    const-string v2, "BunchClient"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "BunchClient"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Bunch request timeout "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->getRequestClientId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, " checking in "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    if-nez v12, :cond_9

    invoke-interface/range {p1 .. p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_7
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->shouldEnqueueIfDisconnected(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "BunchClient"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string v2, "BunchClient"

    const-string v3, "queueing"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mQueuedCommands:Ljava/util/Collection;

    new-instance v3, Landroid/util/Pair;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v13

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-direct {v3, v7, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    const-string v2, "BunchClient"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "BunchClient"

    const-string v3, "sent"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    return v12
.end method

.method public final declared-synchronized sendKeepAlive()V
    .locals 4

    monitor-enter p0

    :try_start_0
    const-string v0, "BunchClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "BunchClient"

    const-string v1, "Sending ping to bunch"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->ping(J)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->sendCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized updateClientVersion()V
    .locals 7

    monitor-enter p0

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget v3, v4, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/realtimechat/BunchClient;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->queryDatastoreVersion(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    :goto_1
    :try_start_2
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    sget-object v5, Lcom/google/wireless/webapps/Version$ClientVersion$App;->GOOGLE_PLUS:Lcom/google/wireless/webapps/Version$ClientVersion$App;

    invoke-virtual {v4, v5}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setApp(Lcom/google/wireless/webapps/Version$ClientVersion$App;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    sget-object v5, Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;->PUBLIC:Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;

    invoke-virtual {v4, v5}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setBuildType(Lcom/google/wireless/webapps/Version$ClientVersion$BuildType;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    sget-object v5, Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;->ANDROID:Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;

    invoke-virtual {v4, v5}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setPlatformType(Lcom/google/wireless/webapps/Version$ClientVersion$PlatformType;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setVersion(I)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setDataVersion(I)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setDeviceOs(Ljava/lang/String;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->setDeviceHardware(Ljava/lang/String;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->build()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/realtimechat/BunchClient;->mClientVersion:Lcom/google/wireless/webapps/Version$ClientVersion;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v2

    :try_start_3
    const-string v4, "BunchClient"

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "BunchClient"

    const-string v5, "Failed to parse database version"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    :catch_1
    move-exception v4

    goto :goto_0
.end method
