.class public final Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "CheckMessageSentOperation.java"


# static fields
.field private static final sHandler:Landroid/os/Handler;


# instance fields
.field final mFlags:I

.field final mMessageRowId:J

.field mSendFailed:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->sHandler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JI)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mMessageRowId:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mSendFailed:Z

    iput p5, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mFlags:I

    return-void
.end method

.method private static recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzActions;

    sget-object v0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->sHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$2;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 6

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v3, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mMessageRowId:J

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->checkMessageSentLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->sHandler:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation$1;-><init>(Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;)V

    const-wide/16 v3, 0x4e20

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mSendFailed:Z

    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mFlags:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_FAIL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mFlags:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_AUTO_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CONVERSATION_MANUAL_RETRY_SUCCESS:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->recordSystemEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final getResultValue()Ljava/lang/Object;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/CheckMessageSentOperation;->mSendFailed:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
