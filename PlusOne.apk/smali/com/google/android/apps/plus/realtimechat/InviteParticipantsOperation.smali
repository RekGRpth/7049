.class public final Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "InviteParticipantsOperation.java"


# instance fields
.field mAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field mConversationRowId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/content/AudienceData;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mConversationRowId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/ParticipantUtils;->getParticipantListFromAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AudienceData;)Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mConversationRowId:J

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/InviteParticipantsOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->inviteParticipantsLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/util/List;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    return-void
.end method
