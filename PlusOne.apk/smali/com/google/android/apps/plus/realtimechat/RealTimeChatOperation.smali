.class public abstract Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.super Ljava/lang/Object;
.source "RealTimeChatOperation.java"


# instance fields
.field protected final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected final mContext:Landroid/content/Context;

.field protected final mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;-><init>(Ljava/lang/Long;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    return-void
.end method


# virtual methods
.method public execute()V
    .locals 0

    return-void
.end method

.method public final getResponses()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->getRequests()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getResultCode()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getResultValue()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
