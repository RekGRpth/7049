.class final Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;
.super Ljava/lang/Object;
.source "RealTimeChatService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    # invokes: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->initWakeLock(Landroid/content/Context;)V
    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$200(Landroid/content/Context;)V

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$300()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "RealTimeChatService"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "RealTimeChatService"

    const-string v1, "release wake lock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$300()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_1
    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$400()Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "RealTimeChatService"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "RealTimeChatService"

    const-string v1, "Stop runnable: Stopping service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$1;->this$0:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->stopSelf()V

    :goto_0
    return-void

    :cond_3
    const-string v0, "RealTimeChatService"

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "RealTimeChatService"

    const-string v1, "Stop runnable: Not stopping, things to do"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sPendingRequests:Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$400()Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$PendingRequestList;->dump()V

    goto :goto_0
.end method
