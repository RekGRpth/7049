.class final Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;
.super Ljava/lang/Object;
.source "RealTimeChatService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->notifyUserPresenceChanged(JLjava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$conversationRowId:J

.field final synthetic val$isPresent:Z

.field final synthetic val$userId:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;Z)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;->val$conversationRowId:J

    iput-object p3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;->val$userId:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;->val$isPresent:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    # getter for: Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->sListeners:Ljava/util/List;
    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->access$600()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;->val$conversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;->val$userId:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService$10;->val$isPresent:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;->onUserPresenceChanged(JLjava/lang/String;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method
