.class public Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
.super Ljava/lang/Object;
.source "RealTimeChatServiceListener.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method onC2dmRegistration(Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onConnected()V
    .locals 0

    return-void
.end method

.method public onConversationCreated$2ae26fbd(ILcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/realtimechat/CreateConversationOperation$ConversationResult;
    .param p3    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    return-void
.end method

.method public onConversationsLoaded$abe99c5()V
    .locals 0

    return-void
.end method

.method public onDisconnected$13462e()V
    .locals 0

    return-void
.end method

.method public onResponseReceived$1587694a(ILcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;)V
    .locals 0
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    return-void
.end method

.method public onResponseTimeout(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onUserPresenceChanged(JLjava/lang/String;Z)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    return-void
.end method

.method public onUserTypingStatusChanged(JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    return-void
.end method
