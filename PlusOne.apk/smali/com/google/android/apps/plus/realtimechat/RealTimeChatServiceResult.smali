.class public final Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;
.super Ljava/lang/Object;
.source "RealTimeChatServiceResult.java"


# instance fields
.field private final mCommand:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

.field private final mErrorCode:I

.field private mRequestId:I


# direct methods
.method constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;-><init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    return-void
.end method

.method constructor <init>(IILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->mRequestId:I

    iput p2, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->mErrorCode:I

    iput-object p3, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->mCommand:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    return-void
.end method


# virtual methods
.method public final getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->mCommand:Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    return-object v0
.end method

.method public final getErrorCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->mErrorCode:I

    return v0
.end method

.method public final getRequestId()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->mRequestId:I

    return v0
.end method
