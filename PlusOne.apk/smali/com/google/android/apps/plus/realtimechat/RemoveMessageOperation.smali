.class public final Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "RemoveMessageOperation.java"


# instance fields
.field mMessageRowId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;->mMessageRowId:J

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/RemoveMessageOperation;->mMessageRowId:J

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsConversationsData;->removeMessageLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    return-void
.end method
