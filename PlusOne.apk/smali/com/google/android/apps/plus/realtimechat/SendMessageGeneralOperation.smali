.class public final Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "SendMessageGeneralOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;
    }
.end annotation


# instance fields
.field private mConversationRowId:J

.field private mImageUrl:Ljava/lang/String;

.field private mMessageRowId:Ljava/lang/Long;

.field private mRequestId:Ljava/lang/Integer;

.field private mRetry:Z

.field private final mServiceListener:Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

.field private mText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;-><init>(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRetry:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;J)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # J

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;-><init>(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    const-wide/16 v0, -0x1

    cmp-long v0, p6, v0

    if-eqz v0, :cond_0

    invoke-static {p6, p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    :cond_0
    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mConversationRowId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRetry:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;-><init>(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    iput-wide p3, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mConversationRowId:J

    iput-object p5, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mText:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRetry:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)J
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    iget-wide v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mConversationRowId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)Ljava/lang/Long;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRetry:Z

    return v0
.end method

.method private checkMessageSentAfterTimeout()V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$2;-><init>(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;)V

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private hasConnection()Z
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    if-nez v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private uploadPhoto(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 20
    .param p1    # Landroid/os/Bundle;
    .param p2    # Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$ServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    const-string v2, "message_row_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    const-string v2, "conversation_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v2, "conversation_name"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v10, v16

    const/16 v2, 0x3a

    const/16 v3, 0x5f

    invoke-virtual {v15, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "bunch"

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static/range {p2 .. p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "video/"

    invoke-virtual {v14, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v19, 0x1

    :goto_0
    if-eqz v19, :cond_1

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_1
    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    new-instance v17, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v8, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$1;

    move-object/from16 v9, p0

    move-object v13, v1

    invoke-direct/range {v8 .. v13}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation$1;-><init>(Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/api/MediaRef;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :cond_0
    const/16 v19, 0x0

    goto :goto_0

    :cond_1
    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_1
.end method


# virtual methods
.method public final execute()V
    .locals 9

    const/4 v1, 0x3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mRetry:Z

    if-eqz v0, :cond_3

    const-string v0, "SendMessageGeneral"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SendMessageGeneral"

    const-string v1, "retrySendMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->hasConnection()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/content/EsConversationsData;->resendMessageLocally$65290203(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "local_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->uploadPhoto(Landroid/os/Bundle;Ljava/lang/String;)V

    :cond_1
    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->checkMessageSentAfterTimeout()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v0, "SendMessageGeneral"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "SendMessageGeneral"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendOriginalMessage "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->hasConnection()Z

    move-result v8

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsConversationsData;->updateMessageUriAndSendLocally$4f1d5505(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JJLjava/lang/String;Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)V

    :goto_1
    if-eqz v8, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->checkMessageSentAfterTimeout()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/content/EsConversationsData;->insertLocalPhotoLocally$341823c7(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->uploadPhoto(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-wide v2, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mConversationRowId:J

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mText:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mImageUrl:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    move v6, v8

    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/plus/content/EsConversationsData;->sendMessageLocally(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    goto :goto_1
.end method

.method public final getResultValue()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendMessageGeneralOperation;->mMessageRowId:Ljava/lang/Long;

    return-object v0
.end method
