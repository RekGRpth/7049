.class public final Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;
.source "SendTileEventOperation.java"


# instance fields
.field private final mConversationId:Ljava/lang/String;

.field private final mTileEventData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTileEventType:Ljava/lang/String;

.field private final mTileType:Ljava/lang/String;

.field private final mTileVersion:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/HashMap;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    iput-object p3, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mConversationId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileType:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileVersion:I

    iput-object p6, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileEventType:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileEventData:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public final execute()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mOperationState:Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;

    iget-object v1, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mConversationId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileType:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileVersion:I

    iget-object v4, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileEventType:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/realtimechat/SendTileEventOperation;->mTileEventData:Ljava/util/HashMap;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/realtimechat/BunchCommands;->tileEventRequest(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatOperationState;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)V

    return-void
.end method
