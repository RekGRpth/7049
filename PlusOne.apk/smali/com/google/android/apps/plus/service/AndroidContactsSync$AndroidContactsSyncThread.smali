.class final Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;
.super Ljava/lang/Thread;
.source "AndroidContactsSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/AndroidContactsSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AndroidContactsSyncThread"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private volatile mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private volatile mThreadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mContext:Landroid/content/Context;

    const-string v0, "AndroidContactsSync"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->setName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->syncContactsForCurrentAccount()V

    return-void
.end method

.method private syncContactsForCurrentAccount()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    new-instance v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncContactsForCurrentAccount(Landroid/content/Context;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final cancel()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    return-void
.end method

.method public final requestSync(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz p1, :cond_1

    const-wide/16 v0, 0x1f4

    :goto_0
    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void

    :cond_1
    const-wide/16 v0, 0x1388

    goto :goto_0
.end method

.method public final run()V
    .locals 1

    const/16 v0, 0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread$1;-><init>(Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->mThreadHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->syncContactsForCurrentAccount()V

    invoke-static {}, Landroid/os/Looper;->loop()V

    return-void
.end method
