.class public final Lcom/google/android/apps/plus/service/AndroidContactsSync;
.super Ljava/lang/Object;
.source "AndroidContactsSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;,
        Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;
    }
.end annotation


# static fields
.field private static final ACTIVITY_PROJECTION:[Ljava/lang/String;

.field private static final ACTIVITY_STATE_COMPARATOR:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;",
            ">;"
        }
    .end annotation
.end field

.field private static final ACTIVITY_SUMMARY_PROJECTION:[Ljava/lang/String;

.field private static final AVATAR_URL_PROJECTION:[Ljava/lang/String;

.field private static final CONTACTS_PROJECTION:[Ljava/lang/String;

.field private static final DISPLAY_PHOTO_CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

.field private static final EMAIL_TYPE_CUSTOM:Ljava/lang/String;

.field private static final EMAIL_TYPE_HOME:Ljava/lang/String;

.field private static final EMAIL_TYPE_OTHER:Ljava/lang/String;

.field private static final EMAIL_TYPE_WORK:Ljava/lang/String;

.field private static final ENTITIES_PROJECTION:[Ljava/lang/String;

.field private static final GROUPS_PROJECTION:[Ljava/lang/String;

.field private static final LARGE_AVATAR_RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

.field private static final MY_PROFILE_PROJECTION:[Ljava/lang/String;

.field private static final PHONE_TYPE_ASSISTANT:Ljava/lang/String;

.field private static final PHONE_TYPE_CALLBACK:Ljava/lang/String;

.field private static final PHONE_TYPE_CAR:Ljava/lang/String;

.field private static final PHONE_TYPE_COMPANY_MAIN:Ljava/lang/String;

.field private static final PHONE_TYPE_CUSTOM:Ljava/lang/String;

.field private static final PHONE_TYPE_HOME:Ljava/lang/String;

.field private static final PHONE_TYPE_HOME_FAX:Ljava/lang/String;

.field private static final PHONE_TYPE_ISDN:Ljava/lang/String;

.field private static final PHONE_TYPE_MAIN:Ljava/lang/String;

.field private static final PHONE_TYPE_MOBILE:Ljava/lang/String;

.field private static final PHONE_TYPE_OTHER:Ljava/lang/String;

.field private static final PHONE_TYPE_OTHER_FAX:Ljava/lang/String;

.field private static final PHONE_TYPE_PAGER:Ljava/lang/String;

.field private static final PHONE_TYPE_RADIO:Ljava/lang/String;

.field private static final PHONE_TYPE_TELEX:Ljava/lang/String;

.field private static final PHONE_TYPE_TTY_TDD:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK_FAX:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK_MOBILE:Ljava/lang/String;

.field private static final PHONE_TYPE_WORK_PAGER:Ljava/lang/String;

.field private static final POSTAL_TYPE_CUSTOM:Ljava/lang/String;

.field private static final POSTAL_TYPE_HOME:Ljava/lang/String;

.field private static final POSTAL_TYPE_OTHER:Ljava/lang/String;

.field private static final POSTAL_TYPE_WORK:Ljava/lang/String;

.field private static final PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

.field private static final PROFILE_ENTITIES_PROJECTION:[Ljava/lang/String;

.field private static final PROFILE_PROJECTION:[Ljava/lang/String;

.field public static final PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final RAW_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final RAW_CONTACT_REFRESH_PROJECTION:[Ljava/lang/String;

.field private static final RAW_CONTACT_SOURCE_ID_PROJECTION:[Ljava/lang/String;

.field public static final STREAM_ITEMS_CONTENT_LIMIT_URI:Landroid/net/Uri;

.field public static final STREAM_ITEMS_PHOTO_URI:Landroid/net/Uri;

.field private static final STREAM_ITEMS_PROJECTION:[Ljava/lang/String;

.field public static final STREAM_ITEMS_URI:Landroid/net/Uri;

.field private static final THUMBNAILS_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

.field private static sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

.field private static sContactsProviderExists:Z

.field private static sContactsProviderStatusKnown:Z

.field private static sMaxStreamItemsPerRawContact:I

.field private static sPhoneTypeMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sThumbnailSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "profile/raw_contacts"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "stream_items"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_URI:Landroid/net/Uri;

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "stream_items_limit"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_CONTENT_LIMIT_URI:Landroid/net/Uri;

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "stream_items/photo"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PHOTO_URI:Landroid/net/Uri;

    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v1, "photo_dimensions"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->DISPLAY_PHOTO_CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "sync1"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "name"

    aput-object v1, v0, v5

    const-string v1, "last_updated_time"

    aput-object v1, v0, v3

    const-string v1, "profile_proto"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->MY_PROFILE_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "last_updated_time"

    aput-object v1, v0, v3

    const-string v1, "for_sharing"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->CONTACTS_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "person_id"

    aput-object v1, v0, v5

    const-string v1, "name"

    aput-object v1, v0, v3

    const-string v1, "contact_proto"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "sourceid"

    aput-object v1, v0, v5

    const-string v1, "mimetype"

    aput-object v1, v0, v3

    const-string v1, "data_id"

    aput-object v1, v0, v4

    const-string v1, "data1"

    aput-object v1, v0, v6

    const-string v1, "data2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "data3"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ENTITIES_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sync1"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "mimetype"

    aput-object v1, v0, v5

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "data1"

    aput-object v1, v0, v4

    const-string v1, "data2"

    aput-object v1, v0, v6

    const-string v1, "data3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_ENTITIES_PROJECTION:[Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_HOME:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_WORK:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_OTHER:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_CUSTOM:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_HOME:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_WORK:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_OTHER:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_CUSTOM:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK:Ljava/lang/String;

    const/4 v0, 0x7

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME_FAX:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_FAX:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MOBILE:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_PAGER:Ljava/lang/String;

    const/16 v0, 0xd

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER_FAX:Ljava/lang/String;

    const/16 v0, 0xa

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_COMPANY_MAIN:Ljava/lang/String;

    const/16 v0, 0x13

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ASSISTANT:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CAR:Ljava/lang/String;

    const/16 v0, 0xe

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_RADIO:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ISDN:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CALLBACK:Ljava/lang/String;

    const/16 v0, 0xf

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TELEX:Ljava/lang/String;

    const/16 v0, 0x10

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TTY_TDD:Ljava/lang/String;

    const/16 v0, 0x11

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_MOBILE:Ljava/lang/String;

    const/16 v0, 0x12

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_PAGER:Ljava/lang/String;

    const/16 v0, 0xc

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MAIN:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER:Ljava/lang/String;

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME_FAX:Ljava/lang/String;

    invoke-virtual {v0, v7, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_FAX:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MOBILE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_PAGER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER_FAX:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_COMPANY_MAIN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ASSISTANT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CAR:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_RADIO:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_ISDN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CALLBACK:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TELEX:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_TTY_TDD:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_MOBILE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK_PAGER:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_MAIN:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->GROUPS_PROJECTION:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "sync2"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->LARGE_AVATAR_RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "gaia_id"

    aput-object v1, v0, v5

    const-string v1, "avatar"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    const-string v1, "data_id"

    aput-object v1, v0, v4

    const-string v1, "mimetype"

    aput-object v1, v0, v6

    const-string v1, "sync3"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->THUMBNAILS_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "sourceid"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_SOURCE_ID_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "raw_contact_source_id"

    aput-object v1, v0, v3

    const-string v1, "stream_item_sync1"

    aput-object v1, v0, v4

    const-string v1, "timestamp"

    aput-object v1, v0, v6

    const-string v1, "stream_item_sync2"

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PROJECTION:[Ljava/lang/String;

    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "activity_id"

    aput-object v1, v0, v5

    const-string v1, "author_id"

    aput-object v1, v0, v3

    const-string v1, "created"

    aput-object v1, v0, v4

    const-string v1, "modified"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_SUMMARY_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "activity_id"

    aput-object v1, v0, v5

    const-string v1, "embed_media"

    aput-object v1, v0, v3

    const-string v1, "total_comment_count"

    aput-object v1, v0, v4

    const-string v1, "plus_one_data"

    aput-object v1, v0, v6

    const-string v1, "loc"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "original_author_name"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "annotation"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_PROJECTION:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "account_type"

    aput-object v1, v0, v3

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "data_set"

    aput-object v1, v0, v6

    const-string v1, "sourceid"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "sync2"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sync4"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_REFRESH_PROJECTION:[Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_STATE_COMPARATOR:Ljava/util/Comparator;

    return-void
.end method

.method private static addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;-><init>(B)V

    iput-object p1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static appendImgTag(Landroid/content/Context;Ljava/lang/StringBuilder;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/StringBuilder;
    .param p2    # I

    const-string v0, "<img src=\'res://"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'/>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method private static applyActivityChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;)V"
        }
    .end annotation

    const/4 v10, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-array v6, v10, [I

    aput v1, v6, v1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v9, :cond_2

    add-int/lit8 v4, v3, 0x20

    if-le v4, v9, :cond_0

    move v4, v9

    :cond_0
    const-string v0, "GooglePlusContactsSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v7, v3

    :goto_1
    if-ge v7, v4, :cond_1

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->dumpPersonActivityState(Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    invoke-static {p1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteObsoleteStreamItems(Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;)V

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;[I)V

    move v3, v4

    goto :goto_0

    :cond_2
    invoke-static {v8, v5, v10}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    return-void
.end method

.method private static buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Z)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p3    # Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            "Z)V"
        }
    .end annotation

    const-string v6, "GooglePlusContactsSync"

    const/4 v7, 0x3

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-static {p3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->dumpRawContactState(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;)V

    :cond_0
    const/4 v5, 0x0

    iget-wide v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sourceid"

    iget-object v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sync1"

    iget-wide v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_is_read_only"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/name"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data1"

    iget-object v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez p4, :cond_1

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0xa

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "conversation"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    sget v8, Lcom/google/android/apps/plus/R$string;->start_conversation_action_label:I

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0xe

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "hangout"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    sget v8, Lcom/google/android/apps/plus/R$string;->start_hangout_action_label:I

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0x14

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "addtocircle"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    sget v8, Lcom/google/android/apps/plus/R$string;->add_to_circle_action_label:I

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data4"

    const/16 v8, 0x1e

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data5"

    const-string v8, "view"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data3"

    sget v8, Lcom/google/android/apps/plus/R$string;->view_profile_action_label:I

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "raw_contact_id"

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "mimetype"

    const-string v8, "vnd.android.cursor.item/identity"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data2"

    const-string v8, "com.google"

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "data1"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "gprofile:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_0
    iget-object v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    iget-boolean v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    if-nez v6, :cond_5

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    :goto_2
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "_id=?"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    const-string v7, "sync1"

    iget-wide v8, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    iget-wide v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_7

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v6, "mimetype"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    iget-wide v6, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_6

    const-string v6, "raw_contact_id"

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    :goto_3
    const-string v6, "data1"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v6, "data2"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v6, "data3"

    iget-object v7, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    goto :goto_2

    :cond_6
    const-string v6, "raw_contact_id"

    iget-wide v7, p3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_3

    :cond_7
    iget-boolean v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->changed:Z

    if-eqz v6, :cond_3

    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v6, "_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    iget-wide v9, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v0, v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_3

    :cond_8
    return-void
.end method

.method private static buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;[Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p3    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    array-length v2, p3

    if-ge v0, v2, :cond_0

    aget-object v2, p3, v0

    invoke-virtual {p4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v2, 0x0

    invoke-static {p0, p1, p2, v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static declared-synchronized cancelSync()V
    .locals 2

    const-class v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->cancel()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static cleanUpActivityStateMap(Ljava/util/HashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;)V"
        }
    .end annotation

    const-wide/16 v8, 0x0

    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    const/4 v1, 0x0

    iget-object v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-boolean v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v6, :cond_2

    iget-wide v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v6, v6, v8

    if-eqz v6, :cond_4

    :cond_2
    iget-boolean v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v6, :cond_3

    iget-wide v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_4

    :cond_3
    iget-boolean v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    if-eqz v6, :cond_1

    :cond_4
    const/4 v1, 0x1

    :cond_5
    if-nez v1, :cond_0

    iget-object v6, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {p0, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_6
    return-void
.end method

.method private static clearAndroidCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const/4 v3, 0x0

    const-string v1, "Android:DeleteCircles"

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    return-void
.end method

.method private static clearAndroidContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const/4 v1, 0x0

    const-string v0, "Android:DeleteContacts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0, v1, v1, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    return-void
.end method

.method private static clearAndroidContactsForOtherAccounts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const/4 v11, 0x1

    const/4 v5, 0x0

    const/4 v10, 0x0

    new-array v4, v11, [Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v10

    const-string v0, "Android:DeleteProfilesOtherAccounts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v2, "data_set=\'plus\' AND account_name!=? AND account_type=\'com.google\'"

    invoke-static {p0, v0, v2, v4, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    const-string v0, "Android:DeleteContactsOtherAccounts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    const-string v2, "data_set=\'plus\' AND account_name!=? AND account_type=\'com.google\'"

    invoke-static {p0, v0, v2, v4, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    const-string v0, "Android:DeleteCirclesOtherAccounts"

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "caller_is_syncadapter"

    const-string v3, "true"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v11, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v10

    const-string v3, "data_set=\'plus\' AND account_name!=? AND account_type=\'com.google\'"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_2

    :goto_1
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p2, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    return-void
.end method

.method private static clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    const/4 v3, 0x0

    const-string v1, "Android:DeleteProfile"

    invoke-virtual {p2, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-wide/16 v8, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    const-wide/16 v1, 0x0

    cmp-long v1, v8, v1

    if-eqz v1, :cond_2

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    invoke-virtual {p2, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount(I)V

    :cond_2
    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    return-void

    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private static collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    return-void
.end method

.method public static deactivateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->cancelSync()V

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncCleanupStatus(Landroid/content/Context;Z)V

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncCleanupStatus(Landroid/content/Context;Z)V

    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private static deleteAndroidContacts(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v5, 0x0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    :goto_0
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_1
    return-void

    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v10, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    invoke-virtual {p4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->incrementCount()V

    goto :goto_2

    :cond_3
    const/4 v1, 0x1

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    goto :goto_1
.end method

.method private static deleteContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const-string v3, "GooglePlusContactsSync"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->dumpRawContactState(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;)V

    :cond_0
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "_id=?"

    new-array v5, v9, [Ljava/lang/String;

    iget-wide v6, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {p0, p2, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    return-void
.end method

.method private static deleteObsoleteStreamItems(Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;)V
    .locals 13
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;II",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v8

    move v2, p2

    :goto_0
    move/from16 v0, p3

    if-ge v2, v0, :cond_2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-boolean v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v9, :cond_0

    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_0

    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-static {v8, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "_id IN ("

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v2, p2

    :goto_2
    move/from16 v0, p3

    if-ge v2, v0, :cond_6

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-boolean v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v9, :cond_3

    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v11, 0x0

    cmp-long v9, v9, v11

    if-eqz v9, :cond_3

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    const/16 v9, 0x2c

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_4
    const/16 v9, 0x3f

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-wide v9, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_7

    :goto_4
    return-void

    :cond_7
    const/16 v9, 0x29

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {v8}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    invoke-virtual {v10, v11, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method private static deleteRemovedContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 4
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-boolean v3, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-nez v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {p0, p1, p3, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :cond_2
    return-void
.end method

.method private static downloadLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/HashMap;I)V
    .locals 20
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;I)V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    new-instance v15, Ljava/util/ArrayList;

    invoke-virtual/range {p3 .. p3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v13

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v13, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v17

    if-nez v17, :cond_4

    add-int/lit8 v16, v8, 0x8

    move/from16 v0, v16

    if-le v0, v13, :cond_0

    move/from16 v16, v13

    :cond_0
    move v5, v8

    :goto_1
    move/from16 v0, v16

    if-ge v5, v0, :cond_3

    invoke-virtual {v15, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    iget v0, v14, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p1

    invoke-static {v12, v0, v14}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->saveAvatarSignature(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;)V

    :cond_1
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, v14, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v6

    const/4 v3, 0x0

    :try_start_0
    new-instance v7, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v0, v14, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    sget-object v18, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v7, v0, v1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    const/16 v17, 0x28

    move/from16 v0, p4

    move/from16 v1, p4

    move/from16 v2, v17

    invoke-virtual {v6, v7, v0, v1, v2}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;

    move-result-object v17

    move-object/from16 v0, v17

    check-cast v0, [B

    move-object v3, v0
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_3
    if-eqz v3, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v17

    iget-wide v0, v14, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    move-wide/from16 v18, v0

    invoke-static/range {v17 .. v19}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v11

    const-string v17, "display_photo"

    move-object/from16 v0, v17

    invoke-static {v11, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    :try_start_1
    invoke-virtual {v12, v10}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;)Ljava/io/OutputStream;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v9}, Ljava/io/OutputStream;->close()V

    move-object/from16 v0, p1

    invoke-static {v12, v0, v14}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->saveAvatarSignature(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v4

    const-string v17, "GooglePlusContactsSync"

    const/16 v18, 0x6

    invoke-static/range {v17 .. v18}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v17

    if-eqz v17, :cond_1

    const-string v17, "GooglePlusContactsSync"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Could not store large avatar: "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v14, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :catch_1
    move-exception v4

    const-string v17, "GooglePlusContactsSync"

    const-string v18, "Could not download avatar"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_3
    move/from16 v8, v16

    goto/16 :goto_0

    :cond_4
    return-void
.end method

.method private static downloadMedia$637d96fa(Landroid/content/Context;Ljava/lang/String;)[B
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    const-string v4, "//"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "https:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$dimen;->android_stream_item_thumbnail_size:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    :try_start_0
    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v4, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v2, p1, v4}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    const/16 v4, 0x8

    invoke-virtual {v1, v2, v3, v3, v4}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v4

    :catch_0
    move-exception v0

    const-string v4, "GooglePlusContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not download media: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static dumpPersonActivityState(Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;)V
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    const-wide/16 v7, 0x0

    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[STREAM] Gaia ID: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  (raw_contact_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-nez v3, :cond_1

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v3, v3, v7

    if-eqz v3, :cond_1

    const-string v2, "[DELETE]"

    :goto_1
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (stream_item_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") created="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\', modified="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v3, :cond_2

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    cmp-long v3, v3, v7

    if-nez v3, :cond_2

    const-string v2, "[INSERT]"

    goto :goto_1

    :cond_2
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    if-eqz v3, :cond_0

    const-string v2, "[UPDATE]"

    goto :goto_1

    :cond_3
    return-void
.end method

.method private static dumpRawContactState(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;)V
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const-wide/16 v7, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-nez v3, :cond_1

    const-string v2, "[DELETE]"

    :goto_0
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (raw_contact_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") last_updated="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    if-nez v3, :cond_3

    const-string v2, "[DELETE]"

    :goto_2
    const-string v3, "GooglePlusContactsSync"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (data_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") \'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\', type="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    iget-wide v3, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    cmp-long v3, v3, v7

    if-nez v3, :cond_2

    const-string v2, "[INSERT]"

    goto/16 :goto_0

    :cond_2
    const-string v2, "[UPDATE]"

    goto/16 :goto_0

    :cond_3
    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    cmp-long v3, v3, v7

    if-nez v3, :cond_4

    const-string v2, "[INSERT]"

    goto :goto_2

    :cond_4
    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->changed:Z

    if-eqz v3, :cond_0

    const-string v2, "[UPDATE]"

    goto :goto_2

    :cond_5
    return-void
.end method

.method private static findChangesInCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;",
            ">;"
        }
    .end annotation

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v3, 0x0

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->GROUPS_PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-object v3

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    const/4 v1, 0x0

    invoke-direct {v7, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;-><init>(B)V

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    iget-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    invoke-virtual {v8, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const-string v1, "plus"

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    if-nez v7, :cond_2

    new-instance v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    invoke-direct {v7, v9}, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;-><init>(B)V

    const-string v1, "plus"

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->android_contact_group:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    iput-boolean v10, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->exists:Z

    const-string v1, "plus"

    invoke-virtual {v8, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_2
    move-object v3, v8

    goto :goto_0

    :cond_2
    const-string v1, "plus"

    invoke-virtual {v8, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method private static findChangesInContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_0

    const/4 v13, 0x0

    :goto_0
    return-object v13

    :cond_0
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    :goto_1
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v2, 0x0

    invoke-direct {v12, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;-><init>(B)V

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    iget-object v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v13, v2, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/AndroidContactsSync;->CONTACTS_PROJECTION:[Ljava/lang/String;

    const-string v5, "in_my_circles=1 AND profile_type!=2 AND for_sharing!=0"

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_4

    :goto_2
    :try_start_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v2, 0x1

    invoke-interface {v8, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    invoke-virtual {v13, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    if-nez v12, :cond_2

    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v2, 0x0

    invoke-direct {v12, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;-><init>(B)V

    iput-object v11, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    iput-wide v9, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    iget-object v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    invoke-virtual {v13, v2, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    const/4 v2, 0x1

    iput-boolean v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_2
    :try_start_2
    iget-wide v2, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    cmp-long v2, v2, v9

    if-nez v2, :cond_3

    invoke-virtual {v13, v11}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    iput-wide v9, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_3

    :cond_4
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method private static flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;
    .locals 5
    .param p0    # Landroid/content/ContentResolver;
    .param p2    # I
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;IZ)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-nez p3, :cond_2

    if-lt v2, p2, :cond_0

    :cond_2
    const/4 v1, 0x0

    :try_start_0
    const-string v3, "com.android.contacts"

    invoke-virtual {p0, v3, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_3
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "GooglePlusContactsSync"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "GooglePlusContactsSync"

    const-string v4, "Cannot apply a batch of content provider operations"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;
    .locals 1
    .param p0    # Landroid/content/ContentResolver;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;Z)[",
            "Landroid/content/ContentProviderResult;"
        }
    .end annotation

    const/16 v0, 0x80

    invoke-static {p0, p1, v0, p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move-result-object v0

    return-object v0
.end method

.method private static getCircleIdMap(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .locals 8
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    const/4 v3, 0x0

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->GROUPS_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-object v3

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v3, v6

    goto :goto_0
.end method

.method private static getEntitiesUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getLimitedRawContactSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)[Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v6, 0x0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v0, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "account_type"

    const-string v5, "com.google"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "data_set"

    const-string v5, "plus"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "account_name"

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v0, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const-string v0, "raw_contact_id"

    aput-object v0, v3, v6

    const-string v4, "starred!=0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const/4 v5, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "starred=0 AND times_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const-string v5, "times_contacted DESC LIMIT 8"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "starred=0 AND last_time_contacted>0 AND mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\'"

    const-string v5, "last_time_contacted DESC LIMIT 8"

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->collectRawContactIds$31aa4520(Landroid/content/Context;Ljava/util/HashSet;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-array v0, v6, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method private static getMaxStreamItemsPerRawContact(Landroid/content/Context;)I
    .locals 8
    .param p0    # Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v3, 0x0

    sget v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sMaxStreamItemsPerRawContact:I

    if-eqz v0, :cond_0

    sget v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sMaxStreamItemsPerRawContact:I

    :goto_0
    return v7

    :cond_0
    const/4 v7, 0x2

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_CONTENT_LIMIT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "max_items"

    aput-object v4, v2, v5

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    sput v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sMaxStreamItemsPerRawContact:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I
    .locals 8
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v6, 0x60

    sget-object v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;->DISPLAY_PHOTO_CONTENT_MAX_DIMENSIONS_URI:Landroid/net/Uri;

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v6

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    return v6

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_type"

    const-string v2, "com.google"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "data_set"

    const-string v2, "plus"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "caller_is_syncadapter"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static insertNewContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-boolean v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-eqz v0, :cond_0

    iget-wide v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V

    :cond_2
    return-void
.end method

.method public static isAndroidSyncSupported(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isContactsProviderAvailable(Landroid/content/Context;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isContactsProviderAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x1

    sget-boolean v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderStatusKnown:Z

    if-eqz v3, :cond_0

    sget-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderExists:Z

    :goto_0
    return v2

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "com.android.contacts"

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_1
    sput-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderExists:Z

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/content/ContentProviderClient;->release()Z

    :cond_1
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderStatusKnown:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    sget-boolean v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sContactsProviderExists:Z

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "GooglePlusContactsSync"

    const-string v3, "Cannot determine availability of the contacts provider"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private static limitStreamItemsPerRawContact(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;)V"
        }
    .end annotation

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getMaxStreamItemsPerRawContact(Landroid/content/Context;)I

    move-result v3

    invoke-virtual {p1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    iget-object v0, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-le v5, v3, :cond_0

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_STATE_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move v1, v3

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static loadContactCircleMembership$57209d63([Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 6
    .param p0    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    const/4 v2, 0x0

    :goto_0
    array-length v4, p0

    if-ge v2, v4, :cond_1

    aget-object v4, p0, v2

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const-string v4, "plus"

    invoke-virtual {p2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v4, "vnd.android.cursor.item/group_membership"

    invoke-virtual {v1}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v0

    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private static populateRawContactState(Landroid/content/Context;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    .param p2    # Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    const/4 v6, 0x0

    const/4 v10, 0x1

    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    if-eqz v5, :cond_1

    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->emails:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/DataEmail;

    const-string v5, "vnd.android.cursor.item/email_v2"

    iget-object v7, v2, Lcom/google/api/services/plusi/model/DataEmail;->value:Ljava/lang/String;

    invoke-static {p1, v5, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    iput-boolean v10, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    iget-object v5, v2, Lcom/google/api/services/plusi/model/DataEmail;->standardTag:Ljava/lang/Integer;

    iget-object v7, v2, Lcom/google/api/services/plusi/model/DataEmail;->customTag:Ljava/lang/String;

    if-nez v5, :cond_0

    move v5, v6

    :goto_1
    packed-switch v5, :pswitch_data_0

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object v7, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_1

    :pswitch_0
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    if-eqz v5, :cond_5

    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->phones:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/DataPhone;

    const-string v5, "vnd.android.cursor.item/phone_v2"

    iget-object v7, v4, Lcom/google/api/services/plusi/model/DataPhone;->value:Ljava/lang/String;

    invoke-static {p1, v5, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    iput-boolean v10, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    iget-object v7, v4, Lcom/google/api/services/plusi/model/DataPhone;->standardTag:Ljava/lang/Integer;

    iget-object v8, v4, Lcom/google/api/services/plusi/model/DataPhone;->customTag:Ljava/lang/String;

    if-nez v7, :cond_2

    const/4 v5, 0x0

    :goto_3
    if-eqz v5, :cond_3

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_2

    :cond_2
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sPhoneTypeMap:Landroid/util/SparseArray;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-virtual {v5, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_3

    :cond_3
    if-eqz v7, :cond_4

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    const/16 v7, 0x14

    if-ne v5, v7, :cond_4

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    sget v5, Lcom/google/android/apps/plus/R$string;->profile_item_phone_google_voice:I

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_2

    :cond_4
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_2

    :cond_5
    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    if-eqz v5, :cond_7

    iget-object v5, p2, Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;->addresses:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;

    const-string v5, "vnd.android.cursor.item/postal-address_v2"

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->value:Ljava/lang/String;

    invoke-static {p1, v5, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    iput-boolean v10, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    iget-object v5, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->standardTag:Ljava/lang/Integer;

    iget-object v7, v0, Lcom/google/api/services/plusi/model/DataCircleMemberPropertiesAddress;->customTag:Ljava/lang/String;

    if-nez v5, :cond_6

    move v5, v6

    :goto_5
    packed-switch v5, :pswitch_data_1

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object v7, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_4

    :cond_6
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_5

    :pswitch_3
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_4

    :pswitch_4
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_4

    :pswitch_5
    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_4

    :cond_7
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private static populateRawContactState$5160c72c(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/api/services/plusi/model/Contacts;)V
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    .param p1    # Lcom/google/api/services/plusi/model/Contacts;

    const/4 v8, 0x1

    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    if-eqz v5, :cond_4

    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->email:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/api/services/plusi/model/TaggedEmail;

    const-string v5, "vnd.android.cursor.item/email_v2"

    iget-object v6, v2, Lcom/google/api/services/plusi/model/TaggedEmail;->value:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    iput-boolean v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    iget-object v5, v2, Lcom/google/api/services/plusi/model/TaggedEmail;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    if-eqz v5, :cond_0

    iget-object v5, v2, Lcom/google/api/services/plusi/model/TaggedEmail;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    const-string v6, "HOME"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v6, "WORK"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v6, "OTHER"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v6, "CUSTOM"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->EMAIL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v6, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    if-eqz v5, :cond_9

    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->phone:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/TaggedPhone;

    const-string v5, "vnd.android.cursor.item/phone_v2"

    iget-object v6, v4, Lcom/google/api/services/plusi/model/TaggedPhone;->value:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    iput-boolean v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    iget-object v5, v4, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    if-eqz v5, :cond_5

    iget-object v5, v4, Lcom/google/api/services/plusi/model/TaggedPhone;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    const-string v6, "HOME"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string v6, "WORK"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_1

    :cond_7
    const-string v6, "OTHER"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_1

    :cond_8
    const-string v6, "CUSTOM"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PHONE_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v6, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_1

    :cond_9
    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    if-eqz v5, :cond_e

    iget-object v5, p1, Lcom/google/api/services/plusi/model/Contacts;->address:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_a
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/TaggedAddress;

    const-string v5, "vnd.android.cursor.item/postal-address_v2"

    iget-object v6, v0, Lcom/google/api/services/plusi/model/TaggedAddress;->value:Ljava/lang/String;

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->addData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    move-result-object v1

    iput-boolean v8, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->exists:Z

    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedAddress;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    if-eqz v5, :cond_a

    iget-object v5, v0, Lcom/google/api/services/plusi/model/TaggedAddress;->tag:Lcom/google/api/services/plusi/model/ContactTag;

    const-string v6, "HOME"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_HOME:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_2

    :cond_b
    const-string v6, "WORK"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_WORK:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_2

    :cond_c
    const-string v6, "OTHER"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_OTHER:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    goto :goto_2

    :cond_d
    const-string v6, "CUSTOM"

    iget-object v7, v5, Lcom/google/api/services/plusi/model/ContactTag;->tag:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->POSTAL_TYPE_CUSTOM:Ljava/lang/String;

    iput-object v6, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/ContactTag;->customTag:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    goto :goto_2

    :cond_e
    return-void
.end method

.method private static queryRawContactsRequiringActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getLimitedRawContactSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "_id IN ("

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v8, 0x0

    :goto_1
    array-length v0, v4

    if-ge v8, v0, :cond_3

    if-eqz v8, :cond_2

    const/16 v0, 0x2c

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v0, 0x3f

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_SOURCE_ID_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :cond_4
    :goto_2
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    new-instance v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    const/4 v0, 0x0

    invoke-direct {v11, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;-><init>(B)V

    iput-object v7, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    invoke-virtual {v12, v7, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v5, v12

    goto :goto_0
.end method

.method private static queryRawContactsRequiringLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;"
        }
    .end annotation

    const/4 v5, 0x0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getLimitedRawContactSet(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)[Ljava/lang/String;

    move-result-object v4

    array-length v1, v4

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id IN ("

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v9, 0x0

    :goto_1
    array-length v1, v4

    if-ge v9, v1, :cond_3

    if-eqz v9, :cond_2

    const/16 v1, 0x2c

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v1, 0x3f

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    const/16 v1, 0x29

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, " OR sync2"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->LARGE_AVATAR_RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :cond_4
    :goto_2
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    const/4 v1, 0x0

    invoke-direct {v12, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;-><init>(B)V

    iput-object v7, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-virtual {v13, v7, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    invoke-virtual {v13}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    new-instance v8, Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v8, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {p0, p1, v13, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->retrieveAvatarSignatures(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    :cond_6
    move-object v5, v13

    goto/16 :goto_0
.end method

.method private static queryRawContactsRequiringThumbnails(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;
    .locals 14
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getEntitiesUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->THUMBNAILS_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const-string v3, "(mimetype=\'vnd.android.cursor.item/photo\' OR mimetype=\'vnd.android.cursor.item/vnd.googleplus.profile\') AND (sync2=0 OR sync2 IS NULL)"

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    :goto_0
    return-object v4

    :cond_0
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    :cond_1
    :goto_1
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v13, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    if-nez v12, :cond_2

    new-instance v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    const/4 v0, 0x0

    invoke-direct {v12, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;-><init>(B)V

    iput-object v8, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    const/4 v0, 0x4

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-virtual {v13, v8, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const-string v0, "vnd.android.cursor.item/photo"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->dataId:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    const-string v3, "in_my_circles=1"

    move-object v0, p0

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    :cond_4
    :goto_2
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarUrlSignature(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v13, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    if-eqz v12, :cond_4

    iget v0, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    if-ne v0, v11, :cond_5

    invoke-virtual {v13, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_5
    :try_start_2
    iput v11, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    iput-object v6, v12, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    move-object v4, v13

    goto/16 :goto_0
.end method

.method private static queryStreamItemState$373650f4(Landroid/content/Context;Ljava/util/HashMap;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-nez v7, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {p1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    if-eqz v10, :cond_0

    new-instance v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    const/4 v1, 0x0

    invoke-direct {v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;-><init>(B)V

    iget-wide v1, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    const/4 v1, 0x4

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    iput-wide v1, v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    iget-object v1, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static reconcileActivitiesAndStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 18
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz p3, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITY_SUMMARY_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_SUMMARY_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v15

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    if-eqz v17, :cond_0

    const/4 v9, 0x0

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-object v2, v7, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v9, v7

    :cond_2
    if-eqz v9, :cond_5

    iget-wide v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    cmp-long v2, v2, v15

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    iput-boolean v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    iput-wide v15, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    :cond_3
    :goto_2
    const/4 v2, 0x1

    iput-boolean v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    :cond_5
    :try_start_1
    new-instance v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    const/4 v2, 0x0

    invoke-direct {v9, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;-><init>(B)V

    move-object/from16 v0, v17

    iget-wide v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    iput-wide v2, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    iput-object v8, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    iput-wide v10, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    iput-wide v15, v9, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_6
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method private static reconcileContacts(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/util/HashMap;)Z
    .locals 17
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Landroid/net/Uri;",
            "[",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;)Z"
        }
    .end annotation

    move-object/from16 v0, p2

    array-length v1, v0

    new-array v5, v1, [Ljava/lang/String;

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id IN ("

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v14, 0x0

    :goto_0
    move-object/from16 v0, p2

    array-length v1, v0

    if-ge v14, v1, :cond_1

    aget-object v1, p2, v14

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-wide v1, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v14

    if-eqz v14, :cond_0

    const/16 v1, 0x2c

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/16 v1, 0x3f

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_1
    const-string v1, ") AND mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ENTITIES_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-nez v13, :cond_2

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_2
    :goto_2
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v1, 0x2

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v1, 0x1

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v1, 0x3

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v1, 0x4

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v1, 0x5

    invoke-interface {v13, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static/range {v6 .. v12}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_3
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x1

    goto :goto_1
.end method

.method private static reconcileData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0    # Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p4, 0x0

    :cond_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 p5, 0x0

    :cond_1
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 p6, 0x0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->mimetype:Ljava/lang/String;

    invoke-static {v2, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data1:Ljava/lang/String;

    invoke-static {v2, p4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iput-wide p1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    invoke-static {v2, p5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    invoke-static {v2, p6}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_4
    iput-object p5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data2:Ljava/lang/String;

    iput-object p6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->data3:Ljava/lang/String;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->changed:Z

    :cond_5
    :goto_0
    return-void

    :cond_6
    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;-><init>(B)V

    iput-wide p1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$DataState;->dataId:J

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->data:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static declared-synchronized requestSync(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized requestSync(Landroid/content/Context;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Z

    const-class v1, Lcom/google/android/apps/plus/service/AndroidContactsSync;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit v1

    return-void

    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_2
    sget-object v0, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sAndroidSyncThread:Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AndroidContactsSyncThread;->requestSync(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static resizeThumbnail([BI)[B
    .locals 6
    .param p0    # [B
    .param p1    # I

    const/4 v3, 0x0

    if-nez p0, :cond_0

    move-object p0, v3

    :goto_0
    return-object p0

    :cond_0
    const/4 v4, 0x0

    array-length v5, p0

    invoke-static {p0, v4, v5}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    move-object p0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-gt v4, p1, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-gt v4, p1, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_2
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    if-nez v2, :cond_3

    move-object p0, v3

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0xfa0

    invoke-direct {v1, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    goto :goto_0
.end method

.method private static retrieveAvatarSignatures(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v5, v1, [Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "gaia_id IN ("

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v10, 0x0

    :goto_0
    array-length v1, v5

    if-ge v10, v1, :cond_1

    if-eqz v10, :cond_0

    const/16 v1, 0x2c

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const/16 v1, 0x3f

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v10

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_1
    const-string v1, ")"

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->AVATAR_URL_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-nez v8, :cond_2

    :goto_1
    return-void

    :cond_2
    :goto_2
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarUrlSignature(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    if-eqz v13, :cond_3

    iget v1, v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    if-ne v1, v12, :cond_4

    :cond_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_4
    :try_start_1
    iput v12, v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    iput-object v7, v13, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_1
.end method

.method private static saveAvatarSignature(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;)V
    .locals 6
    .param p0    # Landroid/content/ContentResolver;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    const/4 v5, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    iget-wide v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "sync2"

    iget v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v2, "sync3"

    iget v3, p2, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0, v0, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method private static shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static syncActivitiesForRawContact$520c64bf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v5, 0x0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;-><init>(B)V

    iput-object p4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->gaiaId:Ljava/lang/String;

    iput-wide p2, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    invoke-virtual {v1, p4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3, p2, p3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "stream_items"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {p0, v1, v2, v6, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryStreamItemState$373650f4(Landroid/content/Context;Ljava/util/HashMap;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "author_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p4, v4, v5

    invoke-static {p0, p1, v1, v3, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileActivitiesAndStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->limitStreamItemsPerRawContact(Landroid/content/Context;Ljava/util/HashMap;)V

    invoke-static {v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->cleanUpActivityStateMap(Ljava/util/HashMap;)V

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->applyActivityChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method protected static syncContactsForCurrentAccount(Landroid/content/Context;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Android contacts sync"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v6

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncClean(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    :try_start_1
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidContactsForOtherAccounts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncCleanupStatus(Landroid/content/Context;Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :goto_1
    :try_start_2
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncClean(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsSyncCleanupStatus(Landroid/content/Context;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_3
    if-nez v6, :cond_4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    const-string v1, "GooglePlusContactsSync"

    const-string v2, "Failed to clear out contacts"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0

    :cond_4
    :try_start_4
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    :cond_5
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_6
    :goto_2
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_7
    :goto_3
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "Android:Circles"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->findChangesInCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v2

    if-eqz v2, :cond_11

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getGroupsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;

    const-string v1, "GooglePlusContactsSync"

    const/4 v8, 0x3

    invoke-static {v1, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-boolean v1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->exists:Z

    if-nez v1, :cond_d

    const-string v1, "[DELETE]"

    :goto_5
    const-string v8, "GooglePlusContactsSync"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v9, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v9, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " (group_id="

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v9, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    invoke-virtual {v1, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, ")"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    iget-boolean v1, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->exists:Z

    if-eqz v1, :cond_f

    iget-wide v8, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-nez v1, :cond_f

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v8, "sourceid"

    iget-object v9, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleId:Ljava/lang/String;

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v8, "title"

    iget-object v0, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->circleName:Ljava/lang/String;

    invoke-virtual {v1, v8, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "group_is_read_only"

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v0, v1, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_9
    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryContactsSyncVersion(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I

    move-result v0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_6

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    const/16 v0, 0xd

    invoke-static {p0, v6, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsSyncVersion(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->clearAndroidProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    goto/16 :goto_3

    :cond_b
    const-string v0, "Android:MyProfile"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->CONTACT_BY_PERSON_ID_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/service/AndroidContactsSync;->MY_PROFILE_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v7

    :try_start_5
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    const/4 v0, 0x2

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    move-object v0, p0

    move-object v1, v6

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_c
    :try_start_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_d
    iget-wide v8, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    const-wide/16 v10, 0x0

    cmp-long v1, v8, v10

    if-nez v1, :cond_e

    const-string v1, "[INSERT]"

    goto/16 :goto_5

    :cond_e
    const-string v1, "[UPDATE]"

    goto/16 :goto_5

    :cond_f
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v8, "_id=?"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    iget-wide v11, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$CircleState;->groupId:J

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v10

    invoke-virtual {v1, v8, v9}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_10
    const/4 v0, 0x1

    invoke-static {v3, v5, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "sync1"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v1, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_11
    if-nez v2, :cond_1c

    const/4 v0, 0x0

    :goto_6
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    :cond_12
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "Android:Contacts"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->findChangesInContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getCircleIdMap(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v0, :cond_13

    if-eqz v1, :cond_13

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v6, v0, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->deleteRemovedContacts(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-static {p0, v6, v0, v3, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->insertNewContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    invoke-static {p0, v6, v0, v3, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateChangedContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V

    const/4 v1, 0x1

    invoke-static {v2, v3, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    :cond_13
    if-nez v0, :cond_1d

    const/4 v0, 0x0

    :goto_7
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    :cond_14
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_17

    const-string v0, "Android:LargeAvatars"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "display_max_dim"

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "large.avatar.size"

    const/16 v3, 0x100

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v0, v2, :cond_15

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "sync2"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v2, v5, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_15
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "large.avatar.size"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryRawContactsRequiringLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v1

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1e

    :cond_16
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    :cond_17
    :goto_8
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const-string v0, "Android:Activities"

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/content/EsPeopleData;->hasPublicCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryRawContactsRequiringActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_18

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1f

    :cond_18
    :goto_9
    if-eqz v0, :cond_19

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_19

    invoke-static {p0, v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->applyActivityChanges(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;)V

    :cond_19
    if-nez v0, :cond_21

    const/4 v0, 0x0

    :goto_a
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    :cond_1a
    :goto_b
    invoke-static {p0, v6, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncSmallAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_1b
    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto/16 :goto_0

    :cond_1c
    :try_start_7
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v0

    goto/16 :goto_6

    :cond_1d
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto/16 :goto_7

    :cond_1e
    invoke-static {p0, v6, p1, v1, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->downloadLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/HashMap;I)V

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    goto :goto_8

    :cond_1f
    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=? AND data_set=?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "com.google"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "plus"

    aput-object v5, v3, v4

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryStreamItemState$373650f4(Landroid/content/Context;Ljava/util/HashMap;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_20

    const/4 v0, 0x0

    goto :goto_9

    :cond_20
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v6, v0, v1, v2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileActivitiesAndStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->limitStreamItemsPerRawContact(Landroid/content/Context;Ljava/util/HashMap;)V

    invoke-static {v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->cleanUpActivityStateMap(Ljava/util/HashMap;)V

    goto :goto_9

    :cond_21
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    goto :goto_a

    :cond_22
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "account_name=? AND account_type=? AND data_set=?"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "com.google"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "plus"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_b
.end method

.method public static syncRawContact(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncRawContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private static syncRawContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V
    .locals 25
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v6, Lcom/google/android/apps/plus/service/AndroidContactsSync;->RAW_CONTACT_REFRESH_PROJECTION:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p2

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    if-nez v18, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-wide/16 v21, 0x0

    const/16 v20, 0x0

    const/16 v17, 0x0

    const-wide/16 v23, 0x0

    :try_start_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x2

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/4 v4, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "com.google"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "plus"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_2
    const-string v4, "GooglePlusContactsSync"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "GooglePlusContactsSync"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cannot refresh raw contact. It does not belong to the current account: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v21

    const/4 v4, 0x4

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/4 v4, 0x5

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    const/4 v4, 0x6

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v23

    :cond_5
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    const-wide/16 v4, 0x0

    cmp-long v4, v21, v4

    if-eqz v4, :cond_0

    invoke-static/range {v20 .. v20}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_6
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;-><init>(B)V

    iput-object v8, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->gaiaId:Ljava/lang/String;

    move-wide/from16 v0, v21

    iput-wide v0, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    move/from16 v0, v17

    iput v0, v5, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    invoke-virtual {v4, v8, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->retrieveAvatarSignatures(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_7

    new-instance v5, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v6, "Large avatar sync for a single raw contact"

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "display_max_dim"

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v4, v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->downloadLargeAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/util/HashMap;I)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :cond_7
    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->hasPublicCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v23

    const-wide/32 v6, 0x493e0

    cmp-long v4, v4, v6

    if-lez v4, :cond_9

    new-instance v14, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v4, "Single raw contact sync"

    invoke-virtual {v14, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v4, "Activities:Sync"

    invoke-virtual {v14, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    :try_start_2
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getMaxStreamItemsPerRawContact(Landroid/content/Context;)I

    move-result v12

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v13, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    invoke-static/range {v4 .. v14}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-virtual {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "sync4"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v6

    move-wide/from16 v0, v21

    invoke-static {v6, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v4, v7, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v21

    invoke-static {v0, v1, v2, v3, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncActivitiesForRawContact$520c64bf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    throw v4

    :catch_0
    move-exception v4

    :try_start_3
    const-string v5, "GooglePlusContactsSync"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "GooglePlusContactsSync"

    const-string v6, "Could not refresh posts"

    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_8
    invoke-virtual {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto/16 :goto_0

    :catchall_1
    move-exception v4

    invoke-virtual {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v14}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v4

    :cond_9
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v21

    invoke-static {v0, v1, v2, v3, v8}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncActivitiesForRawContact$520c64bf(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static syncSmallAvatars(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V
    .locals 21
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-static/range {p0 .. p2}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->shouldSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Z

    move-result v15

    if-nez v15, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v15, "Android:Avatars"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->queryRawContactsRequiringThumbnails(Landroid/content/ContentResolver;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/HashMap;

    move-result-object v12

    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/util/HashMap;->isEmpty()Z

    move-result v15

    if-eqz v15, :cond_2

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v15

    invoke-direct {v13, v15}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v10

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v15

    if-nez v15, :cond_8

    add-int/lit8 v14, v8, 0x8

    if-le v14, v10, :cond_3

    move v14, v10

    :cond_3
    move v5, v8

    :goto_2
    if-ge v5, v14, :cond_7

    invoke-virtual {v13, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;

    iget-object v15, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    if-eqz v15, :cond_5

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v7

    iget-object v15, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->avatarUrl:Ljava/lang/String;

    invoke-static {v15}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    const/4 v15, 0x2

    const/16 v16, 0x0

    const/16 v17, 0x28

    :try_start_0
    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v7, v2, v15, v0, v1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatar(Ljava/lang/String;IZI)Ljava/lang/Object;

    move-result-object v15

    move-object v0, v15

    check-cast v0, [B

    move-object v6, v0
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    if-eqz v6, :cond_5

    sget v15, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    if-nez v15, :cond_4

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "thumbnail_max_dim"

    invoke-static/range {v15 .. v16}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getPreferredAvatarSize(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v15

    sput v15, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    :cond_4
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v15

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "_id=?"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    iget-wide v0, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "sync3"

    iget v0, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->signature:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v15, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->dataId:J

    const-wide/16 v17, 0x0

    cmp-long v15, v15, v17

    if-eqz v15, :cond_6

    sget-object v15, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "_id=?"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    iget-wide v0, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->dataId:J

    move-wide/from16 v19, v0

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "data15"

    sget v17, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    move/from16 v0, v17

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->resizeThumbnail([BI)[B

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    :catch_0
    move-exception v4

    const-string v15, "GooglePlusContactsSync"

    const-string v16, "Cannot download avatar"

    move-object/from16 v0, v16

    invoke-static {v15, v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    :cond_6
    sget-object v15, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v15}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "raw_contact_id"

    iget-wide v0, v11, Lcom/google/android/apps/plus/service/AndroidContactsSync$AvatarState;->rawContactId:J

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "mimetype"

    const-string v17, "vnd.android.cursor.item/photo"

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    const-string v16, "data15"

    sget v17, Lcom/google/android/apps/plus/service/AndroidContactsSync;->sThumbnailSize:I

    move/from16 v0, v17

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->resizeThumbnail([BI)[B

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v15

    invoke-virtual {v3, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_7
    const/16 v15, 0x8

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v9, v3, v15, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;IZ)[Landroid/content/ContentProviderResult;

    move v8, v14

    goto/16 :goto_1

    :cond_8
    const/4 v15, 0x1

    invoke-static {v9, v3, v15}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish(I)V

    goto/16 :goto_0
.end method

.method private static updateChangedContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/HashMap;Ljava/util/ArrayList;Ljava/util/HashMap;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-boolean v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    if-eqz v0, :cond_0

    iget-wide v0, v8, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p2

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->updateContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V

    :cond_2
    return-void
.end method

.method private static updateContacts(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/HashMap;ZLjava/util/HashMap;)V
    .locals 14
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;",
            ">;Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v11

    invoke-static {p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getEntitiesUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v8

    sget-object v4, Lcom/google/android/apps/plus/content/EsProvider;->CONTACTS_URI:Landroid/net/Uri;

    invoke-static {v4, p1}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v12, :cond_6

    add-int/lit8 v13, v10, 0x20

    if-le v13, v12, :cond_0

    move v13, v12

    :cond_0
    sub-int v4, v13, v10

    new-array v6, v4, [Ljava/lang/String;

    const/4 v9, 0x0

    :goto_1
    array-length v4, v6

    if-ge v9, v4, :cond_1

    add-int v4, v10, v9

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    iget-object v4, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    aput-object v4, v6, v9

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "person_id IN ("

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    :goto_2
    array-length v7, v6

    if-ge v4, v7, :cond_3

    if-eqz v4, :cond_2

    const/16 v7, 0x2c

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v7, 0x3f

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    const/16 v4, 0x29

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_PROJECTION:[Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :cond_4
    :goto_3
    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    invoke-interface {v5, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-interface {v5, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-static {v7}, Lcom/google/android/apps/plus/content/EsPeopleData;->deserializeContactInfo([B)Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-static {p0, v4, v7}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->populateRawContactState(Landroid/content/Context;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/android/apps/plus/content/EsPeopleData$ContactInfo;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v4

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    throw v4

    :cond_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p4

    move-object/from16 v1, p6

    invoke-static {v6, v0, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->loadContactCircleMembership$57209d63([Ljava/lang/String;Ljava/util/HashMap;Ljava/util/HashMap;)V

    if-nez p5, :cond_7

    move-object/from16 v0, p4

    invoke-static {v2, v8, v6, v0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileContacts(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/util/HashMap;)Z

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    return-void

    :cond_7
    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {p0, v11, v0, v6, v1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;[Ljava/lang/String;Ljava/util/HashMap;)V

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v2, v0, v4}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move v10, v13

    goto/16 :goto_0
.end method

.method private static updateMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;J[B)V
    .locals 21
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # [B

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->isAndroidSyncSupported(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v15, 0x0

    new-instance v19, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;-><init>(B)V

    const/4 v5, 0x1

    move-object/from16 v0, v19

    iput-boolean v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->exists:Z

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->personId:Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    move-wide/from16 v0, p3

    move-object/from16 v2, v19

    iput-wide v0, v2, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_RAW_CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-eqz v13, :cond_0

    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x0

    invoke-interface {v13, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->lastUpdateTime:J

    const/4 v7, 0x1

    invoke-interface {v13, v7}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v7

    cmp-long v5, v5, v7

    if-nez v5, :cond_5

    const/4 v15, 0x1

    :cond_2
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_3

    if-nez v15, :cond_8

    :cond_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->fullName:Ljava/lang/String;

    if-eqz p5, :cond_4

    invoke-static {}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->getInstance()Lcom/google/api/services/plusi/model/SimpleProfileJson;

    move-result-object v5

    move-object/from16 v0, p5

    invoke-virtual {v5, v0}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/SimpleProfile;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    if-eqz v5, :cond_4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    if-eqz v5, :cond_4

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/CommonContent;->contacts:Lcom/google/api/services/plusi/model/Contacts;

    move-object/from16 v0, v19

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->populateRawContactState$5160c72c(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Lcom/google/api/services/plusi/model/Contacts;)V

    :cond_4
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_7

    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    sget-object v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_CONTENT_RAW_CONTACTS_URI:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "data"

    invoke-virtual {v5, v6}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_type"

    const-string v7, "com.google"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "data_set"

    const-string v7, "plus"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "account_name"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "caller_is_syncadapter"

    const-string v7, "true"

    invoke-virtual {v5, v6, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/plus/service/AndroidContactsSync;->PROFILE_ENTITIES_PROJECTION:[Ljava/lang/String;

    const-string v8, "mimetype IN (\'vnd.android.cursor.item/email_v2\',\'vnd.android.cursor.item/phone_v2\',\'vnd.android.cursor.item/postal-address_v2\',\'vnd.android.cursor.item/group_membership\')"

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v3

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    if-eqz v14, :cond_0

    :goto_2
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/4 v5, 0x0

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v5, 0x2

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v5, 0x3

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/4 v5, 0x4

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v5, v19

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->reconcileData(Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v5

    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_5
    const/4 v15, 0x0

    goto/16 :goto_1

    :catchall_1
    move-exception v5

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v5

    :cond_6
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_7
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_9

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getProfileRawContactUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v20

    :goto_3
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    invoke-static {v0, v1, v12, v2, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->buildContentProviderOperations(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;Z)V

    const/4 v5, 0x1

    invoke-static {v3, v12, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    move-result-object v18

    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-nez v5, :cond_8

    if-eqz v18, :cond_8

    move-object/from16 v0, v18

    array-length v5, v0

    if-lez v5, :cond_8

    const/4 v5, 0x0

    aget-object v5, v18, v5

    iget-object v0, v5, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v5

    move-object/from16 v0, v19

    iput-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    :cond_8
    move-object/from16 v0, v19

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v19

    iget-wide v6, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$RawContactState;->rawContactId:J

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->syncRawContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/net/Uri;)V

    goto/16 :goto_0

    :cond_9
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getRawContactsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v20

    goto :goto_3
.end method

.method private static updateStreamItems(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;IILjava/util/ArrayList;[I)V
    .locals 41
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # I
    .param p6    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;",
            ">;II",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentProviderOperation;",
            ">;[I)V"
        }
    .end annotation

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    new-instance v33, Ljava/lang/StringBuilder;

    invoke-direct/range {v33 .. v33}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "activity_id IN ("

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move/from16 v18, p3

    :goto_0
    move/from16 v0, v18

    move/from16 v1, p4

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;

    move-object/from16 v0, v34

    iget-object v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->activities:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_0
    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    iget-boolean v3, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->exists:Z

    if-eqz v3, :cond_0

    iget-boolean v3, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->changed:Z

    if-nez v3, :cond_1

    iget-wide v3, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    :cond_1
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/16 v3, 0x2c

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_2
    const/16 v3, 0x3f

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v3, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v34

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$PersonActivityState;->rawContactId:J

    iput-wide v3, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    iget-object v3, v10, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->activityId:Ljava/lang/String;

    invoke-virtual {v11, v3, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_5
    :goto_2
    return-void

    :cond_6
    const/16 v3, 0x29

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v38

    sget-object v3, Lcom/google/android/apps/plus/service/AndroidContactsSync;->STREAM_ITEMS_PHOTO_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_type"

    const-string v5, "com.google"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "data_set"

    const-string v5, "plus"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "account_name"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "caller_is_syncadapter"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v37

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$mipmap;->icon:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->app_name:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getResourceEntryName(I)Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/content/EsProvider;->ACTIVITIES_URI:Landroid/net/Uri;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/service/AndroidContactsSync;->ACTIVITY_PROJECTION:[Ljava/lang/String;

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    :cond_7
    :goto_3
    :try_start_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_19

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v26

    const/4 v3, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    const/4 v3, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v31

    const/16 v17, 0x0

    if-eqz v26, :cond_8

    invoke-static/range {v26 .. v26}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v17

    :cond_8
    invoke-virtual {v11, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v35

    move-object/from16 v0, v34

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_14

    invoke-static/range {v38 .. v38}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "raw_contact_id"

    move-object/from16 v0, v34

    iget-wide v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->rawContactId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "stream_item_sync1"

    invoke-virtual {v3, v4, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "res_package"

    move-object/from16 v0, v29

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "icon"

    move-object/from16 v0, v20

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "label"

    move-object/from16 v0, v23

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    :goto_4
    const/4 v3, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v24

    const/4 v3, 0x5

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    const/4 v3, 0x6

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v3, 0x7

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v24, :cond_9

    const-string v3, "<div><font color=\'#6e8ec6\'><b>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "</b></font></div>"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "<div>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "</div>"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    const/4 v3, 0x0

    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_15

    sget v3, Lcom/google/android/apps/plus/R$string;->originally_shared_post:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v28, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "<div>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "</div>"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "<blockquote>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v3, 0x1

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    const-string v5, "<div><font color=\'#777777\'>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</font></div>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    :goto_5
    if-eqz v17, :cond_d

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_c

    const-string v7, "<div><font color=\'#6e8ec6\'><b>"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "</b></font></div>"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_d

    const-string v5, "<div><font color=\'#777777\'>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</font></div>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    if-eqz v3, :cond_e

    const-string v3, "</blockquote>"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v39

    const-string v3, "text"

    move-object/from16 v0, v39

    invoke-virtual {v13, v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v3, "timestamp"

    move-object/from16 v0, v34

    iget-wide v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->created:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const-string v3, "stream_item_sync2"

    move-object/from16 v0, v34

    iget-wide v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->lastModified:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    const/4 v3, 0x0

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    if-eqz v14, :cond_f

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_comment_blue:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->appendImgTag(Landroid/content/Context;Ljava/lang/StringBuilder;I)V

    const-string v3, " "

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v33

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " "

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    if-eqz v31, :cond_11

    invoke-static/range {v31 .. v31}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v40

    if-eqz v40, :cond_11

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_10

    const-string v3, "&nbsp;&nbsp;"

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_plus_one:I

    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->appendImgTag(Landroid/content/Context;Ljava/lang/StringBuilder;I)V

    const-string v3, " "

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v0, v33

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " "

    move-object/from16 v0, v33

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_16

    const-string v3, "comments"

    invoke-virtual/range {v33 .. v33}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    :goto_6
    invoke-virtual {v13}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v36, 0x0

    move-object/from16 v0, v34

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_12

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->getStreamItemsUri(Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v34

    iget-wide v4, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "photo"

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_12
    const/16 v22, 0x0

    if-eqz v17, :cond_13

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_13

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getContentUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->downloadMedia$637d96fa(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v3

    if-eqz v3, :cond_13

    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;-><init>(B)V

    const/4 v5, 0x0

    iput v5, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->mediaIndex:I

    iput-object v3, v4, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->imageBytes:[B

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_13
    if-eqz v22, :cond_7

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x6

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v30

    const/16 v18, 0x0

    :goto_7
    move/from16 v0, v18

    move/from16 v1, v30

    if-ge v0, v1, :cond_18

    move-object/from16 v0, v22

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;

    const/4 v3, 0x0

    aget v4, p6, v3

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->imageBytes:[B

    array-length v5, v5

    add-int/2addr v4, v5

    aput v4, p6, v3

    move-object/from16 v0, v34

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_17

    invoke-static/range {v37 .. v37}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "stream_item_id"

    move/from16 v0, v35

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v27

    :goto_8
    const-string v3, "stream_item_photo_sync1"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "sort_index"

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "stream_item_photo_sync2"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "photo"

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ImageContainer;->imageBytes:[B

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v18, v18, 0x1

    goto :goto_7

    :cond_14
    move-object/from16 v0, v34

    iget-wide v3, v0, Lcom/google/android/apps/plus/service/AndroidContactsSync$ActivityState;->streamItemId:J

    move-object/from16 v0, v38

    invoke-static {v0, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v13

    goto/16 :goto_4

    :cond_15
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    const-string v5, "<div>"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</div>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_5

    :catchall_0
    move-exception v3

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v3

    :cond_16
    :try_start_1
    const-string v3, "comments"

    const-string v4, " "

    invoke-virtual {v13, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    goto/16 :goto_6

    :cond_17
    invoke-static/range {v36 .. v36}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v27

    goto :goto_8

    :cond_18
    const/4 v3, 0x0

    aget v3, p6, v3

    const/high16 v4, 0x40000

    if-lt v3, v4, :cond_7

    const/4 v3, 0x1

    move-object/from16 v0, p5

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, p6, v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_3

    :cond_19
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->flushBatch(Landroid/content/ContentResolver;Ljava/util/ArrayList;Z)[Landroid/content/ContentProviderResult;

    invoke-virtual/range {p5 .. p5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, p6, v3

    goto/16 :goto_2
.end method
