.class public final Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;
.super Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;
.source "AvatarResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/AvatarResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AvatarIdentifier"
.end annotation


# instance fields
.field protected mGaiaId:Ljava/lang/String;

.field private mHashCode:I

.field private mNextInPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

.field private mRounded:Z

.field private mSizeCategory:I

.field protected mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iget v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z

    return v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    if-ne p1, p0, :cond_1

    const/4 v1, 0x1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iget v2, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I

    iget v3, v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z

    iget-boolean v3, v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mFlags:I

    iget v3, v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mFlags:I

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public final getNextInPool()Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mNextInPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    :goto_0
    iget v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mFlags:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    goto :goto_0

    :cond_2
    const/16 v0, 0x1f

    iput v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    goto :goto_0
.end method

.method public final init(Ljava/lang/String;Ljava/lang/String;IZI)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z
    .param p5    # I

    invoke-super {p0, p5}, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->init(I)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I

    iput-boolean p4, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mHashCode:I

    return-void
.end method

.method public final setNextInPool(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mNextInPool:Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, "GAIA ID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z

    if-eqz v1, :cond_0

    const-string v1, "(rounded)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_0
    const-string v1, "tiny"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_1
    const-string v1, "small"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_2
    const-string v1, "medium"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
