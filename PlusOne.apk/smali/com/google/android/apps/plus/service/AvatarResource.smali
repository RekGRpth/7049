.class public final Lcom/google/android/apps/plus/service/AvatarResource;
.super Lcom/google/android/apps/plus/service/ImageResource;
.source "AvatarResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;
    }
.end annotation


# instance fields
.field private mAvatarUrl:Ljava/lang/String;

.field private mDownloadUrl:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/ImageResourceManager;
    .param p2    # Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/service/ImageResource;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;)V

    return-void
.end method

.method private getAvatarUrl()Ljava/lang/String;
    .locals 4

    iget-object v3, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mAvatarUrl:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mAvatarUrl:Ljava/lang/String;

    :goto_0
    return-object v3

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v2, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iget-object v3, v2, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, v2, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mGaiaId:Ljava/lang/String;

    invoke-static {v1, v0, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->loadAvatarUrl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mAvatarUrl:Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mAvatarUrl:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v3, v2, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mUrl:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mAvatarUrl:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method protected final decodeBitmap([B)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # [B

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/service/ImageResource;->decodeBitmap([B)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v2, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    # getter for: Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mRounded:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->access$100(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getRoundedBitmap(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eq v0, v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    :cond_0
    return-object v0
.end method

.method protected final downloadResource()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getResourceDownloader()Lcom/google/android/apps/plus/service/ResourceDownloader;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ResourceDownloader;->downloadResource(Lcom/google/android/apps/plus/service/Resource;)V

    return-void
.end method

.method public final getContentUri()Landroid/net/Uri;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getDownloadUrl()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mDownloadUrl:Ljava/lang/String;

    if-nez v2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I
    invoke-static {v0}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->access$000(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsAvatarData;->getAvatarSizeInPx(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AvatarResource;->getAvatarUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v1, v2, v4, v4}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getResizedUrl(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getStaticUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mDownloadUrl:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mDownloadUrl:Ljava/lang/String;

    return-object v2
.end method

.method public final getShortFileName()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/AvatarResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AvatarResource;->getAvatarUrl()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsMediaCache;->buildShortFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    # getter for: Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mSizeCategory:I
    invoke-static {v0}, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->access$000(Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    iget v2, v0, Lcom/google/android/apps/plus/service/AvatarResource$AvatarIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_0

    const-string v2, "-o"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :pswitch_0
    const-string v2, "-at"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const-string v2, "-as"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    const-string v2, "-am"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final load()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/AvatarResource;->getAvatarUrl()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/apps/plus/service/ImageResource;->load()V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/service/AvatarResource;->deliverDownloadError(I)V

    goto :goto_0
.end method
