.class final Lcom/google/android/apps/plus/service/EsService$5;
.super Ljava/lang/Object;
.source "EsService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/EsService;->processIntent1(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;II)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/EsService;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$gaiaId:Ljava/lang/String;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsService$5;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p4, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$gaiaId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    new-instance v12, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v12}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v14, 0x1

    const/4 v9, 0x0

    const/4 v11, 0x0

    :try_start_0
    new-instance v0, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$gaiaId:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v14, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed user photo; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$gaiaId:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v14, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed photo albums; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const-string v4, "camerasync"

    iget-object v5, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$gaiaId:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v14, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed camera photos; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$context:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x0

    const-string v4, "posts"

    iget-object v5, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$gaiaId:Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v14, 0x0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getErrorCode()I

    move-result v9

    invoke-virtual {v0}, Lcom/google/android/apps/plus/network/HttpOperation;->getReasonPhrase()Ljava/lang/String;

    move-result-object v11

    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    #photosHome; failed post photos; code: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-nez v14, :cond_4

    new-instance v13, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    invoke-direct {v13, v9, v11, v1}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v12, v13

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$5;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$intent:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-static {v1, v2, v12, v3}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    :goto_0
    return-void

    :catch_0
    move-exception v10

    :try_start_1
    new-instance v13, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v13, v1, v2, v10}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/EsService$5;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$intent:Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-static {v1, v2, v13, v3}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    move-object v12, v13

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService$5;->this$0:Lcom/google/android/apps/plus/service/EsService;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/EsService$5;->val$intent:Landroid/content/Intent;

    const/4 v4, 0x0

    invoke-static {v2, v3, v12, v4}, Lcom/google/android/apps/plus/service/EsService;->access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    throw v1
.end method
