.class public Lcom/google/android/apps/plus/service/EsService;
.super Landroid/app/Service;
.source "EsService.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;,
        Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;
    }
.end annotation


# static fields
.field private static sAccountSettingsResponses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/content/AccountSettingsData;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private static sBlockUserOperationFactory:Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

.field private static final sDismissPeopleLock:Ljava/lang/Object;

.field private static sHandler:Landroid/os/Handler;

.field private static final sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

.field private static sLastCameraMediaLocation:Ljava/lang/String;

.field private static sLastRequestId:Ljava/lang/Integer;

.field private static final sListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/service/EsServiceListener;",
            ">;"
        }
    .end annotation
.end field

.field private static sMuteUserOperationFactory:Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

.field private static sOutOfBoxResponses:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;",
            ">;"
        }
    .end annotation
.end field

.field private static final sPendingIntents:Ljava/util/Map;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private static sPeopleDataFactory:Lcom/google/android/apps/plus/content/PeopleData$Factory;

.field private static final sResults:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/apps/plus/service/ServiceResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

.field private mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

.field private mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

.field private final mStopRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sDismissPeopleLock:Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sResults:Ljava/util/Map;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/service/IntentPool;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/service/IntentPool;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;-><init>(B)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sOutOfBoxResponses:Ljava/util/Map;

    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/EsService$ResultsLinkedHashMap;-><init>(B)V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;-><init>(Lcom/google/android/apps/plus/service/EsService;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    new-instance v0, Lcom/google/android/apps/plus/service/EsService$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/EsService$1;-><init>(Lcom/google/android/apps/plus/service/EsService;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mStopRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$200()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 7
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/content/Intent;

    new-instance v6, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "Get nearby activities"

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v0, "Activities:SyncNearby"

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsProvider;->getActivitiesPageSize(Landroid/content/Context;)I

    move-result v4

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsPostsData;->doNearbyActivitiesSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p5, v0, v1}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v1, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p5, v1, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Z)V
    .locals 12
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p8    # Landroid/content/Intent;
    .param p9    # Z

    new-instance v11, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    move/from16 v0, p9

    invoke-virtual {v11, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->setFullSync(Z)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Get activities for circleId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " userId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " view: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v1, "Activities:SyncStream"

    invoke-virtual {v11, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    if-eqz p9, :cond_0

    const/16 v9, 0x14

    :goto_0
    const/4 v10, 0x0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v7, p9

    move-object/from16 v8, p7

    :try_start_0
    invoke-static/range {v1 .. v11}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    const/4 v2, 0x0

    move-object/from16 v0, p8

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_1
    return-void

    :cond_0
    if-nez p7, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsProvider;->getsActivitiesFirstPageSize(Landroid/content/Context;)I

    move-result v9

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsProvider;->getActivitiesPageSize(Landroid/content/Context;)I

    move-result v9

    goto :goto_0

    :catch_0
    move-exception v1

    :try_start_1
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v1, 0x0

    move-object/from16 v0, p8

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_1

    :catchall_0
    move-exception v1

    invoke-virtual {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p3    # Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;

    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "Notification sync"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p1, p2, v1, v0, v2}, Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, p3, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p3, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .locals 14
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, "circles_to_add"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    const-string v1, "circles_to_remove"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const-string v1, "participant_array"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v13

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v1, v13

    if-ge v2, v1, :cond_0

    aget-object v1, v13, v2

    check-cast v1, Lcom/google/android/apps/plus/util/ParticipantParcelable;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/ParticipantParcelable;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    move v12, v1

    :goto_1
    array-length v1, v13

    if-ge v12, v1, :cond_1

    aget-object v1, v13, v12

    move-object v2, v1

    check-cast v2, Lcom/google/android/apps/plus/util/ParticipantParcelable;

    new-instance v1, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/ParticipantParcelable;->getParticipantId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/ParticipantParcelable;->getName()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v2, p1

    move-object/from16 v3, p2

    invoke-direct/range {v1 .. v11}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->start()V

    const-string v2, "EsService"

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto :goto_1

    :cond_1
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->update(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Z
    .param p4    # Landroid/content/Intent;

    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "People sync"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, p2, v1, v0, p3}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, p4, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p4, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/service/EsService;
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Z
    .param p4    # Landroid/content/Intent;

    new-instance v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v0, "Exp sync"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, p2, v1, v0, p3}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->syncAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)Z

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v2, 0x0

    invoke-direct {p0, p4, v0, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    new-instance v2, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v0, 0x0

    invoke-direct {p0, p4, v2, v0}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method static accountsChanged(Landroid/content/Context;)I
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/plus/content/AccountSettingsData;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z
    .param p6    # Lcom/google/android/apps/plus/content/AccountSettingsData;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "display_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_plus_page"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "account_settings"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static addAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "oob_origin"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static addPeopleToCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/util/ParticipantParcelable;[Ljava/lang/String;)Ljava/lang/Integer;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [Lcom/google/android/apps/plus/util/ParticipantParcelable;
    .param p3    # [Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v8

    const-string v0, "op"

    const/16 v1, 0x2c8

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "acc"

    invoke-virtual {v8, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "participant_array"

    invoke-virtual {v8, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "circles_to_add"

    invoke-virtual {v8, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    if-eqz p2, :cond_1

    array-length v0, p2

    if-lez v0, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move-object v6, p2

    array-length v9, p2

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v9, :cond_0

    aget-object v10, v6, v7

    invoke-virtual {v10}, Lcom/google/android/apps/plus/util/ParticipantParcelable;->getParticipantId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;[Ljava/lang/String;Z)V

    :cond_1
    invoke-static {p0, v8}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static addPersonToCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v5, 0x1

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v6

    const-string v0, "op"

    const/16 v1, 0x2be

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "acc"

    invoke-virtual {v6, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "person_id"

    invoke-virtual {v6, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "person_name"

    invoke-virtual {v6, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "circles_to_add"

    new-array v1, v5, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p4, v1, v4

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "fire_and_forget"

    invoke-virtual {v6, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v4, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/CircleMembershipManager;->onStartAddToCircleRequest$3608be29(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    return-object v7
.end method

.method public static changeNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/NotificationSettingsData;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xcc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "notification_settings"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static clearNetworkTransactionsData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x7d1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method private completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p3    # Ljava/lang/Object;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/service/EsService$21;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/plus/service/EsService$21;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static createAddPeopleToCirclePendingIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[Lcom/google/android/apps/plus/util/ParticipantParcelable;[Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [Lcom/google/android/apps/plus/util/ParticipantParcelable;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "participant_array"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "circles_to_add"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "notif_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static createCircle(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "just_following"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static createComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p3    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x387

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "external_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createEventComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x35

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "text"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static createPostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static createPostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method public static createProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2cc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final declineSquareInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa90

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteCircles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "circle_ids"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static deleteComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x38c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteLocalPhotos(Landroid/content/Context;Ljava/util/ArrayList;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;)I"
        }
    .end annotation

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "media_refs"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deletePhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/Long;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x36

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deletePhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_ids"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deletePostPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static deleteProfilePlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2cd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static disableWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x8fd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method protected static doDeclineSquareInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const-string v1, "square_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, p1, v3}, Lcom/google/android/apps/plus/content/EsSquaresData;->dismissSquareInvitation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;

    const-string v5, "DECLINE_INVITATION"

    move-object v1, p0

    move-object v2, p1

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->start()V

    const-string v1, "EsService"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    return-void
.end method

.method protected static doDeleteCircles$6324374e(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    new-instance v0, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    if-eqz v6, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not delete circles"

    invoke-direct {v1, v2, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not delete circles: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/DeleteCirclesOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v7, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v7}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v1, "People sync after circle deletion"

    invoke-virtual {v7, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {p0, p1, v7, v4, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    return-void
.end method

.method protected static doDismissSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v1, "suggestions_ui"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "person_ids"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    const-string v1, "suggestion_ids"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {p0, p1, v4}, Lcom/google/android/apps/plus/content/EsPeopleData;->deleteFromSuggestedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    sget-object v11, Lcom/google/android/apps/plus/service/EsService;->sDismissPeopleLock:Ljava/lang/Object;

    monitor-enter v11

    :try_start_0
    new-instance v0, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;

    const-string v6, "REJECT"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;JLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->start()V

    const-string v1, "EsService"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/RecordSuggestionActionOperation;->logAndThrowExceptionIfFailed(Ljava/lang/String;)V

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v7, "op"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v7, 0x2c7

    if-ne v1, v7, :cond_2

    add-int/lit8 v1, v2, 0x1

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    if-le v2, v1, :cond_1

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_2
    return-void

    :cond_1
    monitor-exit v11

    goto :goto_2

    :catchall_0
    move-exception v1

    monitor-exit v11

    throw v1

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method protected static doReportAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const-string v1, "gaia_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "abuse_type"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;

    move-object v1, p0

    move-object v2, p1

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->start()V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->getException()Ljava/lang/Exception;

    move-result-object v7

    if-eqz v7, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_0

    new-instance v1, Ljava/io/IOException;

    const-string v2, "Could not report abuse"

    invoke-direct {v1, v2, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->hasError()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Could not report abuse: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/ReportProfileAbuseOperation;->getErrorCode()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v8, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    const-string v1, "Post-report-abuse sync"

    invoke-virtual {v8, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {p0, p1, v8, v5, v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    invoke-virtual {v8}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    return-void
.end method

.method public static editActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "reshare"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static editComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final editModerationState(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa91

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "moderation_state"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static editPhotoComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final editSquareMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa8e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "square_action"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static enableAndPerformWipeoutStats(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x8fc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private finalizeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;)V
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;

    const-string v1, "rid"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "rid"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p2, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sResults:Ljava/util/Map;

    invoke-interface {v1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v1, "from_pool"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/service/IntentPool;->put(Landroid/content/Intent;)V

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService;->mStopRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/EsService;->mStopRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const-string v1, "EsService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "EsService"

    const-string v2, "completeRequest: Stopping service in 5000 ms"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void
.end method

.method private static generateRequestId()I
    .locals 3

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/service/EsService;->sLastRequestId:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_1

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_1
    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    goto :goto_0
.end method

.method public static getActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "is_square_moderator"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getActivityAudience(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getActivityStream(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z

    const/4 v2, 0x0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v3, 0x17

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "view"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    if-eqz p3, :cond_0

    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-eqz p4, :cond_1

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    if-eqz p5, :cond_2

    const-string v1, "square_stream_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    if-eqz p6, :cond_3

    const-string v1, "cont_token"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v3, "newer"

    if-nez p6, :cond_4

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "from_widget"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public static getAlbumList(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz p2, :cond_0

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getAlbumPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "album_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getAllPhotoTiles(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x44

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "resumetoken"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getCreatePostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {p0, p1, p2}, Lcom/google/android/apps/plus/service/EsService;->createPostPlusOneIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.plus.analytics.intent.extra.FROM_NOTIFICATION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "notif_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getDeleteNotificationsIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)Landroid/app/PendingIntent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.plus.notif.clear"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "notif_id"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "op"

    const/16 v2, 0xce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x385

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getEventHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x384

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static getEventInvitees(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x38e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "include_blacklist"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getLastCameraMediaLocation()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sLastCameraMediaLocation:Ljava/lang/String;

    return-object v0
.end method

.method private static getMutateProfileOpForScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Landroid/graphics/RectF;)Lcom/google/android/apps/plus/api/MutateProfileOperation;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Intent;
    .param p3    # Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;
    .param p4    # Ljava/lang/String;
    .param p5    # Landroid/graphics/RectF;

    new-instance v5, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct {v5}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    new-instance v0, Lcom/google/api/services/plusi/model/CommonContent;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CommonContent;-><init>()V

    iput-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    new-instance v1, Lcom/google/api/services/plusi/model/ScrapbookInfo;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ScrapbookInfo;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    const-string v1, "COVER"

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->layout:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    new-instance v1, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iput-object p4, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->id:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    new-instance v1, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;-><init>()V

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iget v1, p5, Landroid/graphics/RectF;->left:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->left:Ljava/lang/Float;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iget v1, p5, Landroid/graphics/RectF;->top:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->top:Ljava/lang/Float;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iget v1, p5, Landroid/graphics/RectF;->right:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->right:Ljava/lang/Float;

    iget-object v0, v5, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

    iget v1, p5, Landroid/graphics/RectF;->bottom:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;->bottom:Ljava/lang/Float;

    new-instance v0, Lcom/google/android/apps/plus/api/MutateProfileOperation;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/api/MutateProfileOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    return-object v0
.end method

.method public static getNearbyActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "view"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "loc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    if-eqz p4, :cond_0

    const-string v1, "cont_token"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    const-string v2, "newer"

    if-nez p4, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/api/LocationQuery;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "loc_query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getNearbyLocations(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/api/LocationQuery;
    .param p3    # Lcom/google/android/apps/plus/content/DbLocation;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x29

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "loc_query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "loc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xcd

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static getPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    if-eqz p5, :cond_0

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getPhotoSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getPhotosHome(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getPhotosOfUser(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getProfileAndContact(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2bf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "refresh"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static getSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x259

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final getSquare(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa8d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final getSquares(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa8c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static getStreamPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/Integer;
    .param p5    # Ljava/lang/Integer;
    .param p6    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x34

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "stream_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "offset"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "max_count"

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static insertAnalyticsEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientOzEvent;",
            ">;)V"
        }
    .end annotation

    sget-object v3, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v4, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "op"

    const/16 v4, 0x3f3

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "acc"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_0
    invoke-static {p2}, Lcom/google/android/apps/plus/content/DbAnalyticsEvents;->serializeClientOzEventList(Ljava/util/List;)[B

    move-result-object v0

    const-string v3, "analytics_events"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v2}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v3, "EsService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "insertAnalyticsEvents: Failed to serialize the analytics events. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static insertCameraPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x460

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "filename"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/ClientOzEvent;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/ClientOzEvent;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->getInstance()Lcom/google/api/services/plusi/model/ClientOzEventJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/ClientOzEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method public static insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "people_suggestion_event"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method public static insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object v1, p2

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;J)V

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;)V

    return-void
.end method

.method private static insertPeopleSuggestionEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;",
            ">;)V"
        }
    .end annotation

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "people_suggestion_events"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method public static invitePeopleToEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/content/AudienceData;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x38b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static isOutOfBoxError(Ljava/lang/Throwable;)Z
    .locals 2
    .param p0    # Ljava/lang/Throwable;

    instance-of v0, p0, Lcom/google/android/apps/plus/api/OzServerException;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/google/android/apps/plus/api/OzServerException;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/api/OzServerException;->getErrorCode()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPhotoPlusOnePending(Ljava/lang/String;Ljava/lang/String;J)Z
    .locals 7
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "op"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v4, 0x38

    if-ne v1, v4, :cond_0

    const-string v4, "gaia_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "album_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "photo_id"

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v4, v4, p2

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isPostPlusOnePending(Ljava/lang/String;)Z
    .locals 5
    .param p0    # Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "op"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v4, 0x10

    if-eq v1, v4, :cond_1

    const/16 v4, 0x11

    if-ne v1, v4, :cond_0

    :cond_1
    const-string v4, "aid"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isProfilePlusOnePending(Ljava/lang/String;)Z
    .locals 5
    .param p0    # Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "op"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v4, 0x2cc

    if-eq v1, v4, :cond_1

    const/16 v4, 0x2cd

    if-ne v1, v4, :cond_0

    :cond_1
    const-string v4, "gaia_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isRequestPending(I)Z
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static localeChanged(Landroid/content/Context;)I
    .locals 3
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static manageEventGuest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "blacklist"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "email"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static markActivitiesAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Ljava/util/ArrayList;)Ljava/lang/Integer;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "op"

    const/16 v3, 0x19

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "acc"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "activity_id_list"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "creation_source_list"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v2, "start_view"

    invoke-virtual {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2
.end method

.method public static markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xc9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "notif_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static moderateComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZZZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "delete"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "report"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_undo"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static modifyCircleProperties(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2ce

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "circle_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "circle_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "just_following"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static mutateProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/SimpleProfile;)Ljava/lang/Integer;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/SimpleProfile;

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "op"

    const/16 v3, 0x2c0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "acc"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->getInstance()Lcom/google/api/services/plusi/model/SimpleProfileJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->toString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2
.end method

.method public static muteActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x12

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mute_state"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static nameTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Z)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/Long;
    .param p4    # Ljava/lang/Long;
    .param p5    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "shape_id"

    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "approved"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static notifyDeepLinkingInstall(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa28

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "package_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static photoPlusOne(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x38

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "album_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "plus_oned"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static plusOneComment(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "plusone_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "plus_oned"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static postActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Lcom/google/android/apps/plus/api/ApiaryActivity;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;ZLcom/google/android/apps/plus/api/BirthdayData;Lcom/google/android/apps/plus/content/DbEmbedEmotishare;Lcom/google/android/apps/plus/content/DbEmbedSquare;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/analytics/AnalyticsInfo;
    .param p3    # Lcom/google/android/apps/plus/api/ApiaryApiInfo;
    .param p4    # Lcom/google/android/apps/plus/api/ApiaryActivity;
    .param p5    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p6    # Ljava/lang/String;
    .param p7    # Ljava/lang/String;
    .param p9    # Lcom/google/android/apps/plus/content/DbLocation;
    .param p10    # Ljava/lang/String;
    .param p11    # Z
    .param p12    # Lcom/google/android/apps/plus/api/BirthdayData;
    .param p13    # Lcom/google/android/apps/plus/content/DbEmbedEmotishare;
    .param p14    # Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .param p15    # Ljava/lang/String;
    .param p16    # Ljava/lang/String;
    .param p17    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/analytics/AnalyticsInfo;",
            "Lcom/google/android/apps/plus/api/ApiaryApiInfo;",
            "Lcom/google/android/apps/plus/api/ApiaryActivity;",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ">;",
            "Lcom/google/android/apps/plus/content/DbLocation;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/apps/plus/api/BirthdayData;",
            "Lcom/google/android/apps/plus/content/DbEmbedEmotishare;",
            "Lcom/google/android/apps/plus/content/DbEmbedSquare;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "op"

    const/16 v3, 0x898

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "acc"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "analytics"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "apiInfo"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "activity"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "audience"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "external_id"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "content"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "loc"

    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "content_deep_link_id"

    invoke-virtual {v1, v2, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "save_post_acl"

    invoke-virtual {v1, v2, p11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "birthdata"

    move-object/from16 v0, p12

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "emotishare_embed"

    move-object/from16 v0, p13

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "square_embed"

    move-object/from16 v0, p14

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static/range {p15 .. p15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "album_title"

    move-object/from16 v0, p15

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-static/range {p16 .. p16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "target_album_id"

    move-object/from16 v0, p16

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_1
    invoke-static/range {p17 .. p17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "album_owner_id"

    move-object/from16 v0, p17

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    if-eqz p8, :cond_3

    const-string v2, "media"

    invoke-virtual {v1, v2, p8}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_3
    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    return v2
.end method

.method private static postClientLogsOperation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method public static postOnServiceThread(Ljava/lang/Runnable;)V
    .locals 2
    .param p0    # Ljava/lang/Runnable;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static postOnUiThread(Ljava/lang/Runnable;)V
    .locals 2
    .param p0    # Ljava/lang/Runnable;

    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sHandler:Landroid/os/Handler;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/content/EsPeopleData;->sHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private processIntent1(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;II)Z
    .locals 223
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # I
    .param p5    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    sparse-switch p4, :sswitch_data_0

    const/4 v6, 0x0

    :goto_0
    return v6

    :sswitch_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v10, "webupdates"

    move-object/from16 v0, p1

    invoke-static {v0, v6, v10}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/gms/auth/GoogleAuthUtil;->invalidateToken(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-direct/range {v4 .. v9}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->setAllowNonGooglePlusUsers(Z)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasSkinnyPlusPages()Z

    move-result v6

    if-eqz v6, :cond_2

    const/16 v201, 0x1

    :goto_1
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v201, :cond_4

    :cond_0
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasPlusPages()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v6

    move/from16 v0, p5

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->putAccountSettingsResponse(ILcom/google/android/apps/plus/content/AccountSettingsData;)V

    move-object/from16 v7, p2

    :goto_2
    if-eqz v7, :cond_7

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAccountsData;->syncExperiments(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "oob_origin"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v219

    invoke-static/range {v219 .. v219}, Lcom/google/android/apps/plus/oob/OutOfBoxMessages;->createOutOfBoxRequest(Ljava/lang/String;)Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    move-result-object v8

    new-instance v5, Lcom/google/android/apps/plus/api/OutOfBoxOperation;

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v6, p1

    invoke-direct/range {v5 .. v10}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->start()V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v6

    move/from16 v0, p5

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->putOutOfBoxResponse(ILcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v5}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    :cond_1
    :goto_3
    const/4 v6, 0x1

    goto/16 :goto_0

    :cond_2
    const/16 v201, 0x0

    goto :goto_1

    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->getAccountByName(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v6

    if-eqz v6, :cond_5

    move-object/from16 v7, p2

    goto :goto_2

    :cond_5
    const/4 v7, 0x0

    goto :goto_2

    :cond_6
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_3

    :cond_7
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_3

    :sswitch_1
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "display_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v6, "photo_url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v213

    const-string v6, "is_plus_page"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v18

    const-string v6, "account_settings"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v110

    check-cast v110, Lcom/google/android/apps/plus/content/AccountSettingsData;

    if-eqz v18, :cond_a

    new-instance v9, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v10

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    const/16 v16, -0x1

    invoke-direct/range {v9 .. v16}, Lcom/google/android/apps/plus/content/EsAccount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZI)V

    new-instance v4, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/16 v16, 0x1

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object v13, v4

    move-object/from16 v14, p1

    move-object v15, v9

    invoke-direct/range {v13 .. v18}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    if-eqz v6, :cond_9

    :cond_8
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->getAccountByName(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_a
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v110 .. v110}, Lcom/google/android/apps/plus/content/AccountSettingsData;->isChild()Z

    move-result v17

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->hasProfilePhoto()Z

    move-result v19

    move-object/from16 v13, p1

    move-object v14, v11

    move-object/from16 v16, v12

    invoke-static/range {v13 .. v19}, Lcom/google/android/apps/plus/content/EsAccountsData;->insertAccount(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    move-object/from16 v0, p1

    move-object/from16 v1, v213

    invoke-static {v0, v7, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->activateAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v110

    invoke-static {v0, v7, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveServerSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;)V

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :sswitch_2
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6, v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->deactivateAccount(Landroid/content/Context;Ljava/lang/String;Z)V

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :sswitch_3
    new-instance v6, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;

    const-wide/16 v13, 0x7530

    invoke-direct {v6, v13, v14}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;-><init>(J)V

    move-object/from16 v0, p0

    invoke-virtual {v6, v0}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->blockingGetC2dmToken(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/realtimechat/BlockingC2DMClient;->hasError()Z

    move-result v6

    if-nez v6, :cond_b

    const/4 v6, 0x1

    :goto_4
    if-nez v6, :cond_c

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->getStickyC2dmId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_c

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v10, 0x0

    const-string v13, "Failed to get C2DM registration."

    const/4 v14, 0x0

    invoke-direct {v6, v10, v13, v14}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_b
    const/4 v6, 0x0

    goto :goto_4

    :cond_c
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAccountsData;->upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :sswitch_4
    const-string v6, "loc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/DbLocation;

    const-string v6, "cont_token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    new-instance v6, Ljava/lang/Thread;

    new-instance v19, Lcom/google/android/apps/plus/service/EsService$3;

    move-object/from16 v20, p0

    move-object/from16 v21, p1

    move-object/from16 v22, p2

    move-object/from16 v25, p3

    invoke-direct/range {v19 .. v25}, Lcom/google/android/apps/plus/service/EsService$3;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/DbLocation;Ljava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_5
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "square_stream_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    const-string v6, "circle_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    const-string v6, "cont_token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v6, "view"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v29

    const-string v6, "from_widget"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v35

    new-instance v6, Ljava/lang/Thread;

    new-instance v25, Lcom/google/android/apps/plus/service/EsService$4;

    move-object/from16 v26, p0

    move-object/from16 v27, p1

    move-object/from16 v28, p2

    move-object/from16 v31, v11

    move-object/from16 v33, v24

    move-object/from16 v34, p3

    invoke-direct/range {v25 .. v35}, Lcom/google/android/apps/plus/service/EsService$4;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Z)V

    move-object/from16 v0, v25

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_6
    new-instance v36, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;

    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    const-string v6, "square_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    const-string v6, "is_square_moderator"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v43, v0

    move-object/from16 v37, p1

    move-object/from16 v38, p2

    move-object/from16 v42, p3

    invoke-direct/range {v36 .. v43}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/GetActivitiesByIdOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_7
    new-instance v36, Lcom/google/android/apps/plus/api/GetAudienceOperation;

    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v41, v0

    move-object/from16 v37, p1

    move-object/from16 v38, p2

    move-object/from16 v40, p3

    invoke-direct/range {v36 .. v41}, Lcom/google/android/apps/plus/api/GetAudienceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/GetAudienceOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_8
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    const-string v6, "reshare"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v43

    new-instance v36, Lcom/google/android/apps/plus/api/EditActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v40, v0

    move-object/from16 v37, p1

    move-object/from16 v38, p2

    move-object/from16 v39, p3

    invoke-direct/range {v36 .. v43}, Lcom/google/android/apps/plus/api/EditActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/EditActivityOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_9
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v49

    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    const-string v6, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v51

    check-cast v51, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v36, Lcom/google/android/apps/plus/api/ReshareActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v48, v0

    move-object/from16 v44, v36

    move-object/from16 v45, p1

    move-object/from16 v46, p2

    move-object/from16 v47, p3

    move-object/from16 v50, v42

    invoke-direct/range {v44 .. v51}, Lcom/google/android/apps/plus/api/ReshareActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/ReshareActivityOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_a
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    new-instance v36, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v56, v0

    const/16 v58, 0x1

    move-object/from16 v52, v36

    move-object/from16 v53, p1

    move-object/from16 v54, p2

    move-object/from16 v55, p3

    move-object/from16 v57, v41

    invoke-direct/range {v52 .. v58}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_b
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    new-instance v36, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v56, v0

    const/16 v58, 0x0

    move-object/from16 v52, v36

    move-object/from16 v53, p1

    move-object/from16 v54, p2

    move-object/from16 v55, p3

    move-object/from16 v57, v41

    invoke-direct/range {v52 .. v58}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/PostOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_c
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    new-instance v36, Lcom/google/android/apps/plus/api/DeleteActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v40, v0

    move-object/from16 v37, p1

    move-object/from16 v38, p2

    move-object/from16 v39, p3

    invoke-direct/range {v36 .. v41}, Lcom/google/android/apps/plus/api/DeleteActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/DeleteActivityOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_d
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "mute_state"

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v58

    new-instance v36, Lcom/google/android/apps/plus/api/MuteActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v56, v0

    move-object/from16 v52, v36

    move-object/from16 v53, p1

    move-object/from16 v54, p2

    move-object/from16 v55, p3

    move-object/from16 v57, v41

    invoke-direct/range {v52 .. v58}, Lcom/google/android/apps/plus/api/MuteActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/MuteActivityOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_e
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "source_stream_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v65

    new-instance v36, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v63, v0

    const-string v66, "SPAM"

    move-object/from16 v59, v36

    move-object/from16 v60, p1

    move-object/from16 v61, p2

    move-object/from16 v62, p3

    move-object/from16 v64, v41

    invoke-direct/range {v59 .. v66}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_f
    const-string v6, "activity_id_list"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v71

    const-string v6, "creation_source_list"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v72

    new-instance v36, Lcom/google/android/apps/plus/api/MarkItemReadOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v70, v0

    const/16 v73, 0x0

    move-object/from16 v66, v36

    move-object/from16 v67, p1

    move-object/from16 v68, p2

    move-object/from16 v69, p3

    invoke-direct/range {v66 .. v73}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;Ljava/util/List;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_10
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v36, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v57, v0

    move-object/from16 v52, v36

    move-object/from16 v53, p1

    move-object/from16 v54, p2

    move-object/from16 v55, v11

    move-object/from16 v56, p3

    invoke-direct/range {v52 .. v57}, Lcom/google/android/apps/plus/api/UserPhotoAlbumsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_11
    const-string v6, "resumetoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v78

    new-instance v36, Lcom/google/android/apps/plus/api/AllPhotosViewOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v77, v0

    const/16 v79, 0x1

    move-object/from16 v73, v36

    move-object/from16 v74, p1

    move-object/from16 v75, p2

    move-object/from16 v76, p3

    invoke-direct/range {v73 .. v79}, Lcom/google/android/apps/plus/api/AllPhotosViewOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_12
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "album_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v82

    const-string v6, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v86

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v85, v0

    move-object/from16 v79, v36

    move-object/from16 v80, p1

    move-object/from16 v81, p2

    move-object/from16 v83, v11

    move-object/from16 v84, p3

    invoke-direct/range {v79 .. v86}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_13
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "stream_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v90

    const-string v6, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v86

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v93, v0

    move-object/from16 v87, v36

    move-object/from16 v88, p1

    move-object/from16 v89, p2

    move-object/from16 v91, v11

    move-object/from16 v92, p3

    move-object/from16 v94, v86

    invoke-direct/range {v87 .. v94}, Lcom/google/android/apps/plus/api/PhotosInAlbumOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_14
    const-string v6, "text"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v97

    const-string v6, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v94

    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v96

    const-string v6, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v86

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v100, v0

    move-object/from16 v91, v36

    move-object/from16 v92, p1

    move-object/from16 v93, p2

    move-object/from16 v98, v86

    move-object/from16 v99, p3

    invoke-direct/range {v91 .. v100}, Lcom/google/android/apps/plus/api/PhotosCreateCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_15
    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    const/16 v41, 0x0

    const/16 v6, 0x23

    move-object/from16 v0, v104

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v195

    if-lez v195, :cond_d

    const/4 v6, 0x0

    move-object/from16 v0, v104

    move/from16 v1, v195

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v41

    :cond_d
    new-instance v36, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v102, v0

    move-object/from16 v98, v36

    move-object/from16 v99, p1

    move-object/from16 v100, p2

    move-object/from16 v101, p3

    move-object/from16 v103, v41

    move-object/from16 v105, v42

    invoke-direct/range {v98 .. v105}, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_16
    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    new-instance v36, Lcom/google/android/apps/plus/api/DeleteCommentOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v103, v0

    move-object/from16 v99, v36

    move-object/from16 v100, p1

    move-object/from16 v101, p2

    move-object/from16 v102, p3

    invoke-direct/range {v99 .. v104}, Lcom/google/android/apps/plus/api/DeleteCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_17
    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    const-string v6, "delete"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v105

    const-string v6, "is_undo"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v106

    new-instance v36, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v102, v0

    const/16 v103, 0x0

    move-object/from16 v98, v36

    move-object/from16 v99, p1

    move-object/from16 v100, p2

    move-object/from16 v101, p3

    invoke-direct/range {v98 .. v106}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_18
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "album_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v82

    const-string v6, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v94

    const-string v6, "plus_oned"

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v115

    if-nez v82, :cond_e

    const-wide/16 v113, 0x0

    :goto_5
    new-instance v36, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v117, v0

    move-object/from16 v107, v36

    move-object/from16 v108, p1

    move-object/from16 v109, p2

    move-wide/from16 v110, v94

    move-object/from16 v112, v11

    move-object/from16 v116, p3

    invoke-direct/range {v107 .. v117}, Lcom/google/android/apps/plus/api/PhotosPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;JZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :cond_e
    invoke-static/range {v82 .. v82}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v113

    goto :goto_5

    :sswitch_19
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v57, v0

    move-object/from16 v52, v36

    move-object/from16 v53, p1

    move-object/from16 v54, p2

    move-object/from16 v55, v11

    move-object/from16 v56, p3

    invoke-direct/range {v52 .. v57}, Lcom/google/android/apps/plus/api/PhotosOfUserOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_1a
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v94

    const-string v6, "shape_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v122

    const-string v6, "approved"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v124

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v126, v0

    move-object/from16 v116, v36

    move-object/from16 v117, p1

    move-object/from16 v118, p2

    move-wide/from16 v119, v94

    move-object/from16 v121, v11

    move-object/from16 v125, p3

    invoke-direct/range {v116 .. v126}, Lcom/google/android/apps/plus/api/PhotosNameTagApprovalOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;JZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_1b
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v96

    const-string v6, "taggee_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v132

    const-string v6, "photo_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    const-string v6, "shape_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v122

    const-string v6, "approved"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v129

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v134, v0

    move-object/from16 v125, v36

    move-object/from16 v126, p1

    move-object/from16 v127, p2

    move-object/from16 v128, v96

    move-object/from16 v130, v94

    move-object/from16 v131, v122

    move-object/from16 v133, p3

    invoke-direct/range {v125 .. v134}, Lcom/google/android/apps/plus/api/PhotosTagSuggestionApprovalOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_1c
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v11

    new-instance v6, Ljava/lang/Thread;

    new-instance v52, Lcom/google/android/apps/plus/service/EsService$5;

    move-object/from16 v53, p0

    move-object/from16 v54, p1

    move-object/from16 v55, p2

    move-object/from16 v56, v11

    move-object/from16 v57, p3

    invoke-direct/range {v52 .. v57}, Lcom/google/android/apps/plus/service/EsService$5;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v52

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_1d
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v6, "photo_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v55

    check-cast v55, Ljava/util/ArrayList;

    new-instance v36, Lcom/google/android/apps/plus/api/DeletePhotosOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v57, v0

    move-object/from16 v52, v36

    move-object/from16 v53, p1

    move-object/from16 v54, p2

    move-object/from16 v56, p3

    invoke-direct/range {v52 .. v57}, Lcom/google/android/apps/plus/api/DeletePhotosOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_1e
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v94

    new-instance v36, Lcom/google/android/apps/plus/api/GetPhotoOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    move-wide/from16 v138, v94

    move-object/from16 v140, v11

    invoke-direct/range {v133 .. v140}, Lcom/google/android/apps/plus/api/GetPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;JLjava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_1f
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v36, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    const/16 v139, 0x40

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    move-object/from16 v138, v11

    invoke-direct/range {v133 .. v139}, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;I)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_20
    const-string v6, "url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v63

    new-instance v36, Lcom/google/android/apps/plus/api/SavePhotoOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v64, v0

    move-object/from16 v59, v36

    move-object/from16 v60, p1

    move-object/from16 v61, p2

    move-object/from16 v62, p3

    invoke-direct/range {v59 .. v64}, Lcom/google/android/apps/plus/api/SavePhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_21
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v6, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v94

    new-instance v36, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v140, v0

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-wide/from16 v136, v94

    move-object/from16 v138, v11

    move-object/from16 v139, p3

    invoke-direct/range {v133 .. v140}, Lcom/google/android/apps/plus/api/PhotosReportAbuseOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/network/HttpOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_22
    const-string v6, "media_refs"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v208

    check-cast v208, Ljava/util/ArrayList;

    new-instance v206, Ljava/util/ArrayList;

    invoke-direct/range {v206 .. v206}, Ljava/util/ArrayList;-><init>()V

    new-instance v205, Ljava/util/ArrayList;

    invoke-direct/range {v205 .. v205}, Ljava/util/ArrayList;-><init>()V

    new-instance v221, Ljava/lang/StringBuilder;

    const-string v6, "media_url IN ("

    move-object/from16 v0, v221

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v208 .. v208}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v200

    :cond_f
    :goto_6
    invoke-interface/range {v200 .. v200}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_10

    invoke-interface/range {v200 .. v200}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v207

    check-cast v207, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {v207 .. v207}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual/range {v207 .. v207}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v204

    move-object/from16 v0, v206

    move-object/from16 v1, v204

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {v204 .. v204}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v205

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v6, "?,"

    move-object/from16 v0, v221

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    :cond_10
    const/16 v215, 0x1

    invoke-virtual/range {v206 .. v206}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_11

    invoke-virtual/range {v221 .. v221}, Ljava/lang/StringBuilder;->length()I

    move-result v222

    add-int/lit8 v6, v222, -0x1

    const-string v10, ")"

    move-object/from16 v0, v221

    move/from16 v1, v222

    invoke-virtual {v0, v6, v1, v10}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v220, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOADS_URI:Landroid/net/Uri;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-virtual/range {v221 .. v221}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v205 .. v205}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    move-object/from16 v0, v205

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    move-object/from16 v0, v220

    invoke-virtual {v10, v0, v13, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual/range {v206 .. v206}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v200

    :goto_7
    invoke-interface/range {v200 .. v200}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_11

    invoke-interface/range {v200 .. v200}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v204

    check-cast v204, Landroid/net/Uri;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    move-object/from16 v0, v204

    invoke-static {v6, v0}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->deleteLocalFileAndMediaStore(Landroid/content/ContentResolver;Landroid/net/Uri;)Z

    move-result v6

    and-int v215, v215, v6

    goto :goto_7

    :cond_11
    if-eqz v215, :cond_12

    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    :goto_8
    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_12
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v10, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v6, v10, v13, v14}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_8

    :sswitch_23
    const-string v6, "data"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v140

    const-string v6, "top_offset"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v147

    const-string v6, "coordinates"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v103

    check-cast v103, Landroid/graphics/RectF;

    if-eqz v103, :cond_13

    new-instance v36, Lcom/google/android/apps/plus/api/UploadMediaOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v138

    const-string v139, "scrapbook"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v141

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    invoke-direct/range {v133 .. v141}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/Integer;)V

    :goto_9
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->startThreaded()V

    goto/16 :goto_3

    :cond_13
    new-instance v36, Lcom/google/android/apps/plus/api/UploadMediaOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v138

    const-string v139, "scrapbook"

    invoke-static/range {v147 .. v147}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v141

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    invoke-direct/range {v133 .. v141}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/Integer;)V

    goto :goto_9

    :sswitch_24
    const-string v6, "photo_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    const-string v6, "top_offset"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v147

    const-string v6, "coordinates"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v103

    check-cast v103, Landroid/graphics/RectF;

    const-string v6, "is_gallery_photo"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v148

    if-eqz v103, :cond_14

    new-instance v36, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    new-instance v141, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v145, v0

    const/16 v147, 0x0

    move-object/from16 v142, p1

    move-object/from16 v143, p2

    move-object/from16 v144, p3

    move-object/from16 v146, v94

    invoke-direct/range {v141 .. v148}, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;IZ)V

    move-object/from16 v0, v36

    move-object/from16 v1, v141

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v101, v0

    move-object/from16 v98, p1

    move-object/from16 v99, p2

    move-object/from16 v100, p3

    move-object/from16 v102, v94

    invoke-static/range {v98 .. v103}, Lcom/google/android/apps/plus/service/EsService;->getMutateProfileOpForScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Landroid/graphics/RectF;)Lcom/google/android/apps/plus/api/MutateProfileOperation;

    move-result-object v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->startThreaded()V

    goto/16 :goto_3

    :cond_14
    new-instance v36, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v145, v0

    move-object/from16 v141, v36

    move-object/from16 v142, p1

    move-object/from16 v143, p2

    move-object/from16 v144, p3

    move-object/from16 v146, v94

    invoke-direct/range {v141 .. v148}, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;IZ)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/SetScrapbookPhotoOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_25
    const-string v6, "photo_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    const-string v6, "top_offset"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v147

    const-string v6, "coordinates"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v103

    check-cast v103, Landroid/graphics/RectF;

    if-eqz v103, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v101, v0

    move-object/from16 v98, p1

    move-object/from16 v99, p2

    move-object/from16 v100, p3

    move-object/from16 v102, v94

    invoke-static/range {v98 .. v103}, Lcom/google/android/apps/plus/service/EsService;->getMutateProfileOpForScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Landroid/graphics/RectF;)Lcom/google/android/apps/plus/api/MutateProfileOperation;

    move-result-object v36

    :goto_a
    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/MutateProfileOperation;->startThreaded()V

    goto/16 :goto_3

    :cond_15
    const-string v6, "layout"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v203

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v111, v0

    new-instance v112, Lcom/google/api/services/plusi/model/SimpleProfile;

    invoke-direct/range {v112 .. v112}, Lcom/google/api/services/plusi/model/SimpleProfile;-><init>()V

    new-instance v6, Lcom/google/api/services/plusi/model/CommonContent;

    invoke-direct {v6}, Lcom/google/api/services/plusi/model/CommonContent;-><init>()V

    move-object/from16 v0, v112

    iput-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    move-object/from16 v0, v112

    iget-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    new-instance v10, Lcom/google/api/services/plusi/model/ScrapbookInfo;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/ScrapbookInfo;-><init>()V

    iput-object v10, v6, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    move-object/from16 v0, v112

    iget-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    if-eqz v203, :cond_16

    :goto_b
    move-object/from16 v0, v203

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ScrapbookInfo;->layout:Ljava/lang/String;

    move-object/from16 v0, v112

    iget-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    new-instance v10, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;-><init>()V

    iput-object v10, v6, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    move-object/from16 v0, v112

    iget-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    move-object/from16 v0, v94

    iput-object v0, v6, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->id:Ljava/lang/String;

    move-object/from16 v0, v112

    iget-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    new-instance v10, Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;

    invoke-direct {v10}, Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;-><init>()V

    iput-object v10, v6, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->offset:Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;

    move-object/from16 v0, v112

    iget-object v6, v0, Lcom/google/api/services/plusi/model/SimpleProfile;->content:Lcom/google/api/services/plusi/model/CommonContent;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/CommonContent;->scrapbookInfo:Lcom/google/api/services/plusi/model/ScrapbookInfo;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/ScrapbookInfo;->fullBleedPhoto:Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/ScrapbookInfoFullBleedPhoto;->offset:Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;

    invoke-static/range {v147 .. v147}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, v6, Lcom/google/api/services/plusi/model/ScrapbookInfoOffset;->top:Ljava/lang/Integer;

    new-instance v36, Lcom/google/android/apps/plus/api/MutateProfileOperation;

    move-object/from16 v107, v36

    move-object/from16 v108, p1

    move-object/from16 v109, p2

    move-object/from16 v110, p3

    invoke-direct/range {v107 .. v112}, Lcom/google/android/apps/plus/api/MutateProfileOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    goto :goto_a

    :cond_16
    const-string v203, "FULL_BLEED"

    goto :goto_b

    :sswitch_26
    const-string v6, "data"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v140

    new-instance v36, Lcom/google/android/apps/plus/api/UploadMediaOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v138

    const-string v139, "profile"

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    invoke-direct/range {v133 .. v140}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_27
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v36, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    const/16 v139, 0x1

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    move-object/from16 v138, v11

    invoke-direct/range {v133 .. v139}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_28
    const-string v6, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    new-instance v36, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    const/16 v139, 0x0

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    move-object/from16 v138, v11

    invoke-direct/range {v133 .. v139}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/ProfileOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_29
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    new-instance v36, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v40, v0

    move-object/from16 v37, p1

    move-object/from16 v38, p2

    move-object/from16 v39, p3

    invoke-direct/range {v36 .. v42}, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/PostCommentStreamOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_2a
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    const-string v6, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v155

    const-string v6, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    new-instance v36, Lcom/google/android/apps/plus/api/PostEventCommentOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v153, v0

    move-object/from16 v149, v36

    move-object/from16 v150, p1

    move-object/from16 v151, p2

    move-object/from16 v152, p3

    move-object/from16 v154, v41

    move-object/from16 v156, v42

    invoke-direct/range {v149 .. v156}, Lcom/google/android/apps/plus/api/PostEventCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/PostEventCommentOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_2b
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v42

    new-instance v36, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v160, v0

    move-object/from16 v156, v36

    move-object/from16 v157, p1

    move-object/from16 v158, p2

    move-object/from16 v159, p3

    move-object/from16 v161, v41

    move-object/from16 v162, v104

    move-object/from16 v163, v42

    invoke-direct/range {v156 .. v163}, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/EditCommentStreamOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_2c
    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    new-instance v36, Lcom/google/android/apps/plus/api/DeleteCommentOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v111, v0

    move-object/from16 v107, v36

    move-object/from16 v108, p1

    move-object/from16 v109, p2

    move-object/from16 v110, p3

    move-object/from16 v112, v104

    invoke-direct/range {v107 .. v112}, Lcom/google/android/apps/plus/api/DeleteCommentOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/DeleteCommentOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_2d
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    const-string v6, "delete"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v163

    const-string v6, "is_undo"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v106

    new-instance v36, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v160, v0

    move-object/from16 v156, v36

    move-object/from16 v157, p1

    move-object/from16 v158, p2

    move-object/from16 v159, p3

    move-object/from16 v161, v41

    move-object/from16 v162, v104

    move/from16 v164, v106

    invoke-direct/range {v156 .. v164}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/ReportAbuseActivityOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_2e
    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    const-string v6, "photo_id"

    const-wide/16 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v13, v14}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v94

    const-string v6, "comment_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v104

    const-string v6, "plus_oned"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v173

    new-instance v36, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v168, v0

    move-object/from16 v164, v36

    move-object/from16 v165, p1

    move-object/from16 v166, p2

    move-object/from16 v167, p3

    move-object/from16 v169, v41

    move-wide/from16 v170, v94

    move-object/from16 v172, v104

    invoke-direct/range {v164 .. v173}, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;JLjava/lang/String;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/CommentOptimisticPlusOneOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_2f
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    if-eqz v6, :cond_17

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->isAborted()Z

    move-result v6

    if-nez v6, :cond_17

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->abort()V

    :cond_17
    const-string v6, "loc_query"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v169

    check-cast v169, Lcom/google/android/apps/plus/api/LocationQuery;

    const-string v6, "loc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v170

    check-cast v170, Lcom/google/android/apps/plus/content/DbLocation;

    new-instance v164, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v168, v0

    const/16 v171, 0x1

    move-object/from16 v165, p1

    move-object/from16 v166, p2

    move-object/from16 v167, p3

    invoke-direct/range {v164 .. v171}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/LocationQuery;Lcom/google/android/apps/plus/content/DbLocation;Z)V

    move-object/from16 v0, v164

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mStapToPlaceOperation:Lcom/google/android/apps/plus/api/SnapToPlaceOperation;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/api/SnapToPlaceOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_30
    const-string v6, "notif_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v210

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v210

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsNotificationData;->markNotificationAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    new-instance v36, Lcom/google/android/apps/plus/api/MarkItemReadOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v178, v0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v210, v6, v10

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v179

    const/16 v180, 0x0

    const/16 v181, 0x1

    move-object/from16 v174, v36

    move-object/from16 v175, p1

    move-object/from16 v176, p2

    move-object/from16 v177, p3

    invoke-direct/range {v174 .. v181}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/List;Ljava/util/List;Z)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_31
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNotificationData;->markAllNotificationsAsRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNotificationData;->getLatestNotificationTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)D

    move-result-wide v138

    const-wide/16 v13, 0x0

    cmpl-double v6, v138, v13

    if-lez v6, :cond_18

    new-instance v36, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v137, v0

    move-object/from16 v133, v36

    move-object/from16 v134, p1

    move-object/from16 v135, p2

    move-object/from16 v136, p3

    invoke-direct/range {v133 .. v139}, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;D)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/SetNotificationLastReadTimeOperation;->startThreaded()V

    goto/16 :goto_3

    :cond_18
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :sswitch_32
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNotificationData;->markAllNotificationsAsSeen(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    const-string v6, "notif_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_19

    const-string v6, "notif_id"

    const/4 v10, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancel(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    :goto_c
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_19
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/service/AndroidNotification;->cancelAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_c

    :sswitch_33
    const-string v6, "notification_settings"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v110

    check-cast v110, Lcom/google/android/apps/plus/content/NotificationSettingsData;

    new-instance v36, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v112, v0

    move-object/from16 v107, v36

    move-object/from16 v108, p1

    move-object/from16 v109, p2

    move-object/from16 v111, p3

    invoke-direct/range {v107 .. v112}, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/SetNotificationSettingsOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_34
    new-instance v36, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, v36

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_35
    new-instance v6, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/apps/plus/service/EsService$6;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v10, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$6;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v6, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_36
    const-string v6, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;->getRequest()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    move-result-object v8

    new-instance v36, Lcom/google/android/apps/plus/api/OutOfBoxOperation;

    const/16 v120, 0x0

    const/16 v121, 0x0

    move-object/from16 v116, v36

    move-object/from16 v117, p1

    move-object/from16 v118, p2

    move-object/from16 v119, v8

    invoke-direct/range {v116 .. v121}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->start()V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/OutOfBoxOperation;->getResponse()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v211

    move/from16 v0, p5

    move-object/from16 v1, v211

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->putOutOfBoxResponse(ILcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V

    if-eqz v211, :cond_1e

    move-object/from16 v0, v211

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    if-eqz v6, :cond_1e

    move-object/from16 v0, v211

    iget-object v6, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1e

    const-string v6, "EsService"

    const/4 v10, 0x3

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1a

    const-string v6, "EsService"

    const-string v10, "Get account info after signup"

    invoke-static {v6, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v10, "webupdates"

    move-object/from16 v0, p1

    invoke-static {v0, v6, v10}, Lcom/google/android/apps/plus/network/AuthData;->invalidateAuthToken(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/16 v119, 0x1

    const/16 v120, 0x0

    const/16 v121, 0x0

    move-object/from16 v116, v4

    move-object/from16 v117, p1

    move-object/from16 v118, p2

    invoke-direct/range {v116 .. v121}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->start()V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasError()Z

    move-result v6

    if-nez v6, :cond_1b

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getException()Ljava/lang/Exception;

    move-result-object v6

    if-eqz v6, :cond_1c

    :cond_1b
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_1c
    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->hasPlusPages()Z

    move-result v6

    if-eqz v6, :cond_1d

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v6

    move/from16 v0, p5

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->putAccountSettingsResponse(ILcom/google/android/apps/plus/content/AccountSettingsData;)V

    move-object/from16 v7, p2

    :goto_d
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_1d
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->getAccountByName(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_d

    :cond_1e
    new-instance v6, Lcom/google/android/apps/plus/service/ServiceResult;

    move-object/from16 v0, v36

    invoke-direct {v6, v0}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(Lcom/google/android/apps/plus/network/HttpOperation;)V

    const/4 v10, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v6, v10}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_3

    :sswitch_37
    new-instance v6, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/apps/plus/service/EsService$7;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct {v10, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$7;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {v6, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_38
    const-string v6, "profile"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v202

    invoke-static {}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->getInstance()Lcom/google/api/services/plusi/model/SimpleProfileJson;

    move-result-object v6

    move-object/from16 v0, v202

    invoke-virtual {v6, v0}, Lcom/google/api/services/plusi/model/SimpleProfileJson;->fromString(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v121

    check-cast v121, Lcom/google/api/services/plusi/model/SimpleProfile;

    new-instance v36, Lcom/google/android/apps/plus/api/MutateProfileOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v120, v0

    move-object/from16 v116, v36

    move-object/from16 v117, p1

    move-object/from16 v118, p2

    move-object/from16 v119, p3

    invoke-direct/range {v116 .. v121}, Lcom/google/android/apps/plus/api/MutateProfileOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/api/services/plusi/model/SimpleProfile;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/MutateProfileOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_39
    const-string v6, "person_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v177

    const-string v6, "muted"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v209

    sget-object v6, Lcom/google/android/apps/plus/service/EsService;->sPeopleDataFactory:Lcom/google/android/apps/plus/content/PeopleData$Factory;

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/PeopleData$Factory;->getInstance(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/PeopleData;

    move-result-object v198

    sget-object v6, Lcom/google/android/apps/plus/service/EsService;->sMuteUserOperationFactory:Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v198

    invoke-static {v0, v1, v2, v6, v3}, Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;->build(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)Lcom/google/android/apps/plus/api/MuteUserOperation;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v177

    move/from16 v2, v209

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/api/MuteUserOperation;->startThreaded(Ljava/lang/String;Z)V

    goto/16 :goto_3

    :sswitch_3a
    const-string v6, "person_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v177

    const-string v6, "person_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v178

    const-string v6, "blocked"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v197

    sget-object v6, Lcom/google/android/apps/plus/service/EsService;->sPeopleDataFactory:Lcom/google/android/apps/plus/content/PeopleData$Factory;

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/PeopleData$Factory;->getInstance(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/content/PeopleData;

    move-result-object v198

    sget-object v6, Lcom/google/android/apps/plus/service/EsService;->sBlockUserOperationFactory:Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, v198

    invoke-static {v0, v1, v2, v6, v3}, Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;->build(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/content/PeopleData;)Lcom/google/android/apps/plus/api/BlockUserOperation;

    move-result-object v36

    move-object/from16 v0, v36

    move-object/from16 v1, v177

    move-object/from16 v2, v178

    move/from16 v3, v197

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/plus/api/BlockUserOperation;->startThreaded(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_3

    :sswitch_3b
    new-instance v6, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/apps/plus/service/EsService$8;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v10, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$8;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v6, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_3c
    const-string v6, "person_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v177

    const-string v6, "person_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v178

    const-string v6, "circles_to_add"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v179

    const-string v6, "circles_to_remove"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v180

    const-string v6, "fire_and_forget"

    const/4 v10, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v181

    new-instance v36, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;

    const/16 v182, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v184, v0

    move-object/from16 v174, v36

    move-object/from16 v175, p1

    move-object/from16 v176, p2

    move-object/from16 v183, p3

    invoke-direct/range {v174 .. v184}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/SetCircleMembershipOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_3d
    const-string v6, "person_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v177

    const-string v6, "person_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v178

    const-string v6, "circles_to_add"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v179

    const-string v6, "circles_to_remove"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v180

    const-string v6, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    new-instance v36, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;

    const/16 v191, 0x0

    const/16 v192, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v194, v0

    move-object/from16 v182, v36

    move-object/from16 v183, p1

    move-object/from16 v184, p2

    move/from16 v185, p5

    move-object/from16 v186, v41

    move-object/from16 v187, v177

    move-object/from16 v188, v178

    move-object/from16 v189, v179

    move-object/from16 v190, v180

    move-object/from16 v193, p3

    invoke-direct/range {v182 .. v194}, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ZZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->startThreaded()V

    goto/16 :goto_3

    :sswitch_3e
    new-instance v6, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/apps/plus/service/EsService$9;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    invoke-direct {v10, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$9;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Intent;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {v6, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_3f
    new-instance v6, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/apps/plus/service/EsService$10;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v10, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$10;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v6, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    goto/16 :goto_3

    :sswitch_40
    const-string v6, "media_ref"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v214

    check-cast v214, Lcom/google/android/apps/plus/api/MediaRef;

    const-string v6, "album_title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v189

    const-string v6, "album_label"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v190

    const-string v6, "stream_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v217

    new-instance v218, Ljava/util/ArrayList;

    invoke-direct/range {v218 .. v218}, Ljava/util/ArrayList;-><init>()V

    const/16 v199, 0x0

    :goto_e
    move-object/from16 v0, v217

    array-length v6, v0

    move/from16 v0, v199

    if-ge v0, v6, :cond_1f

    aget-object v6, v217, v199

    move-object/from16 v0, v218

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v199, v199, 0x1

    goto :goto_e

    :cond_1f
    :try_start_0
    invoke-virtual/range {v214 .. v214}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v6

    const/16 v10, 0x280

    const/16 v13, 0x280

    move-object/from16 v0, p1

    invoke-static {v0, v6, v10, v13}, Lcom/google/android/apps/plus/content/EsPhotosData;->loadLocalBitmap(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v196

    new-instance v216, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v216 .. v216}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v10, 0x64

    move-object/from16 v0, v196

    move-object/from16 v1, v216

    invoke-virtual {v0, v6, v10, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual/range {v216 .. v216}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v191

    invoke-virtual/range {v196 .. v196}, Landroid/graphics/Bitmap;->recycle()V

    new-instance v36, Lcom/google/android/apps/plus/api/UploadMediaOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v186, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/content/EsAccount;->getGaiaId()Ljava/lang/String;

    move-result-object v187

    const-string v188, "messenger"

    move-object/from16 v182, v36

    move-object/from16 v183, p1

    move-object/from16 v184, p2

    move-object/from16 v185, p3

    invoke-direct/range {v182 .. v191}, Lcom/google/android/apps/plus/api/UploadMediaOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual/range {v36 .. v36}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->startThreaded()V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    :catch_0
    move-exception v212

    const-string v6, "EsService"

    const/4 v10, 0x6

    invoke-static {v6, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "EsService"

    const-string v10, "Could not load image"

    move-object/from16 v0, v212

    invoke-static {v6, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_2
        0x6 -> :sswitch_3
        0x7 -> :sswitch_1
        0xb -> :sswitch_6
        0xc -> :sswitch_7
        0xe -> :sswitch_8
        0x10 -> :sswitch_a
        0x11 -> :sswitch_b
        0x12 -> :sswitch_d
        0x13 -> :sswitch_e
        0x14 -> :sswitch_c
        0x15 -> :sswitch_9
        0x16 -> :sswitch_4
        0x17 -> :sswitch_5
        0x19 -> :sswitch_f
        0x1e -> :sswitch_29
        0x1f -> :sswitch_2a
        0x20 -> :sswitch_2b
        0x21 -> :sswitch_2c
        0x22 -> :sswitch_2d
        0x23 -> :sswitch_2e
        0x29 -> :sswitch_2f
        0x32 -> :sswitch_10
        0x33 -> :sswitch_12
        0x34 -> :sswitch_13
        0x35 -> :sswitch_14
        0x36 -> :sswitch_16
        0x37 -> :sswitch_17
        0x38 -> :sswitch_18
        0x39 -> :sswitch_19
        0x3a -> :sswitch_1a
        0x3b -> :sswitch_1b
        0x3c -> :sswitch_1c
        0x3d -> :sswitch_1d
        0x3e -> :sswitch_1e
        0x3f -> :sswitch_22
        0x40 -> :sswitch_15
        0x41 -> :sswitch_1f
        0x42 -> :sswitch_20
        0x43 -> :sswitch_21
        0x44 -> :sswitch_11
        0xc9 -> :sswitch_30
        0xca -> :sswitch_35
        0xcb -> :sswitch_31
        0xcc -> :sswitch_33
        0xcd -> :sswitch_34
        0xce -> :sswitch_32
        0x258 -> :sswitch_36
        0x2be -> :sswitch_3c
        0x2bf -> :sswitch_37
        0x2c0 -> :sswitch_38
        0x2c1 -> :sswitch_39
        0x2c2 -> :sswitch_3a
        0x2c3 -> :sswitch_3b
        0x2c6 -> :sswitch_3e
        0x2c8 -> :sswitch_3f
        0x2c9 -> :sswitch_24
        0x2ca -> :sswitch_23
        0x2cb -> :sswitch_26
        0x2cc -> :sswitch_27
        0x2cd -> :sswitch_28
        0x2d0 -> :sswitch_25
        0x2d1 -> :sswitch_3d
        0x320 -> :sswitch_40
    .end sparse-switch
.end method

.method private processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z
    .locals 202
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/content/Intent;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    sparse-switch p4, :sswitch_data_0

    const/4 v4, 0x0

    :goto_0
    return v4

    :sswitch_0
    new-instance v5, Lcom/google/android/apps/plus/api/EventHomePageOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v5, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/EventHomePageOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/EventHomePageOperation;->startThreaded()V

    :goto_1
    const/4 v4, 0x1

    goto :goto_0

    :sswitch_1
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v11, Ljava/lang/Thread;

    new-instance v4, Lcom/google/android/apps/plus/service/EsService$11;

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v10, p3

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/plus/service/EsService$11;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-direct {v11, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v11}, Ljava/lang/Thread;->start()V

    goto :goto_1

    :sswitch_2
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "pollingtoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v4, "resumetoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v4, "event_auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "invitationtoken"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v4, "fetchnewer"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    const-string v4, "resolvetokens"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    new-instance v4, Ljava/lang/Thread;

    new-instance v10, Lcom/google/android/apps/plus/service/EsService$12;

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object v14, v8

    move-object/from16 v17, v9

    move-object/from16 v21, p3

    invoke-direct/range {v10 .. v21}, Lcom/google/android/apps/plus/service/EsService$12;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/Intent;)V

    invoke-direct {v4, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto :goto_1

    :sswitch_3
    const-string v4, "include_blacklist"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v5, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v11, p3

    invoke-direct/range {v5 .. v12}, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/GetEventInviteeListOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_4
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "photo_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v198

    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v182, v198

    move-object/from16 v0, v198

    array-length v0, v0

    move/from16 v196, v0

    const/16 v190, 0x0

    :goto_2
    move/from16 v0, v190

    move/from16 v1, v196

    if-ge v0, v1, :cond_0

    aget-wide v111, v182, v190

    invoke-static/range {v111 .. v112}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v190, v190, 0x1

    goto :goto_2

    :cond_0
    new-instance v5, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v27, v0

    move-object/from16 v21, v5

    move-object/from16 v22, p1

    move-object/from16 v23, p2

    move-object/from16 v24, p3

    move-object/from16 v26, v8

    invoke-direct/range {v21 .. v27}, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/SharePhotosToEventOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_5
    const-string v4, "circle_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    const-string v4, "just_following"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v30

    new-instance v5, Lcom/google/android/apps/plus/api/CreateCircleOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v32, v0

    move-object/from16 v26, v5

    move-object/from16 v27, p1

    move-object/from16 v28, p2

    move-object/from16 v31, p3

    invoke-direct/range {v26 .. v32}, Lcom/google/android/apps/plus/api/CreateCircleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/CreateCircleOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_6
    const-string v4, "circle_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    const-string v4, "circle_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    const-string v4, "just_following"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v30

    new-instance v5, Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v38, v0

    move-object/from16 v31, v5

    move-object/from16 v32, p1

    move-object/from16 v33, p2

    move-object/from16 v35, v29

    move/from16 v36, v30

    move-object/from16 v37, p3

    invoke-direct/range {v31 .. v38}, Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/ModifyCirclePropertiesOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_7
    const-string v4, "volume_map"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v40

    check-cast v40, Ljava/util/HashMap;

    if-eqz v40, :cond_1

    new-instance v5, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v39, v0

    move-object/from16 v35, v5

    move-object/from16 v36, p1

    move-object/from16 v37, p2

    move-object/from16 v38, p3

    invoke-direct/range {v35 .. v40}, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/util/HashMap;)V

    :goto_3
    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;->startThreaded()V

    goto/16 :goto_1

    :cond_1
    const-string v4, "volume_type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v46

    const-string v4, "volume_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v47

    const-string v4, "volume"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v48

    check-cast v48, Lcom/google/android/apps/plus/content/VolumeSettings;

    new-instance v5, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v45, v0

    move-object/from16 v41, v5

    move-object/from16 v42, p1

    move-object/from16 v43, p2

    move-object/from16 v44, p3

    invoke-direct/range {v41 .. v48}, Lcom/google/android/apps/plus/api/SetVolumeControlsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/VolumeSettings;)V

    goto :goto_3

    :sswitch_8
    const-string v4, "circle_ids"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v53

    new-instance v4, Ljava/lang/Thread;

    new-instance v49, Lcom/google/android/apps/plus/service/EsService$13;

    move-object/from16 v50, p0

    move-object/from16 v51, p1

    move-object/from16 v52, p2

    move-object/from16 v54, p3

    invoke-direct/range {v49 .. v54}, Lcom/google/android/apps/plus/service/EsService$13;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Landroid/content/Intent;)V

    move-object/from16 v0, v49

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_9
    const-string v4, "refresh"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v58

    new-instance v4, Ljava/lang/Thread;

    new-instance v54, Lcom/google/android/apps/plus/service/EsService$14;

    move-object/from16 v55, p0

    move-object/from16 v56, p1

    move-object/from16 v57, p2

    move-object/from16 v59, p3

    invoke-direct/range {v54 .. v59}, Lcom/google/android/apps/plus/service/EsService$14;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;)V

    move-object/from16 v0, v54

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_a
    const-string v4, "refresh"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v58

    new-instance v4, Ljava/lang/Thread;

    new-instance v54, Lcom/google/android/apps/plus/service/EsService$15;

    move-object/from16 v55, p0

    move-object/from16 v56, p1

    move-object/from16 v57, p2

    move-object/from16 v59, p3

    invoke-direct/range {v54 .. v59}, Lcom/google/android/apps/plus/service/EsService$15;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;)V

    move-object/from16 v0, v54

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_b
    new-instance v4, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$16;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$16;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v4, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_c
    new-instance v5, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v5, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/GetBlockedPeopleOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_d
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->accountsChanged(Landroid/content/Context;)Ljava/util/List;

    move-result-object v199

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/service/EsService;->updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v199

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_e
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsProvider;->localeChanged(Landroid/content/Context;)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_f
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_3

    move-object/from16 v0, p1

    move-object/from16 v1, v180

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v194

    const-wide/16 v6, 0x0

    cmp-long v4, v194, v6

    if-ltz v4, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v194

    const-wide/32 v11, 0x36ee80

    cmp-long v4, v6, v11

    if-lez v4, :cond_3

    :cond_2
    invoke-virtual/range {v180 .. v180}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v4

    const-string v6, "com.google.android.apps.plus.content.EsProvider"

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    invoke-static {v4, v6, v7}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v180

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->wipeout(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/phone/InstantUpload;->isEnabled(Landroid/content/Context;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v200

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v200

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_10
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_11
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_4

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v180

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/content/EsProvider;->cleanupData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    move-object/from16 v0, p1

    move-object/from16 v1, v180

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->syncExperiments(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_4
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_12
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_5

    move-object/from16 v0, v180

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "people_suggestion_event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v104

    check-cast v104, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    if-eqz v104, :cond_5

    move-object/from16 v0, p1

    move-object/from16 v1, v180

    move-object/from16 v2, v104

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;)V

    :cond_5
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_13
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_7

    move-object/from16 v0, v180

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/ArrayList;

    move-result-object v62

    invoke-virtual/range {v62 .. v62}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "EsService"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "EsService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Sending "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v62 .. v62}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " people suggestion events"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v5, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v64, v0

    move-object/from16 v59, v5

    move-object/from16 v60, p1

    move-object/from16 v61, p2

    move-object/from16 v63, p3

    invoke-direct/range {v59 .. v64}, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;->startThreaded()V

    goto/16 :goto_1

    :cond_7
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_14
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_a

    move-object/from16 v0, v180

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v184

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v184

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->insert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)V

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->queryLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v192

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v185, v6, v192

    const-wide/16 v6, 0x0

    cmp-long v4, v192, v6

    if-ltz v4, :cond_8

    const-wide/32 v6, 0x5265c0

    cmp-long v4, v185, v6

    if-lez v4, :cond_a

    :cond_8
    const-string v4, "EsService"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    const-string v4, "EsService"

    const-string v6, "%d has passed since the last analytics syncs. Send the analytics data."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static/range {v185 .. v186}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v7, v11

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const-string v4, "analytics_sync"

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_a
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_15
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_b

    move-object/from16 v0, v180

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    const-string v4, "people_suggestion_events"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v62

    if-eqz v62, :cond_b

    invoke-virtual/range {v62 .. v62}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v62

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;->bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_b
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_16
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_d

    move-object/from16 v0, v180

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/List;

    move-result-object v187

    invoke-interface/range {v187 .. v187}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_d

    const-string v4, "EsService"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_c

    const-string v4, "EsService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Sending "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v187 .. v187}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " analytics events"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    new-instance v5, Lcom/google/android/apps/plus/api/PostClientLogsOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v5, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    move-object/from16 v0, v187

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->setClientOzEvents(Ljava/util/List;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->startThreaded()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6, v7}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->saveLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    goto/16 :goto_1

    :cond_d
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_17
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v180

    if-eqz v180, :cond_e

    move-object/from16 v0, v180

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, "analytics_events"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v183

    invoke-static/range {v183 .. v183}, Lcom/google/android/apps/plus/content/DbAnalyticsEvents;->deserializeClientOzEventList([B)Ljava/util/List;

    move-result-object v187

    if-eqz v187, :cond_e

    invoke-interface/range {v187 .. v187}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v187

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_e
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_18
    const-string v4, "search_query"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v66

    const-string v4, "newer"

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v197

    const-string v4, "search_mode"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v67

    if-nez v197, :cond_f

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v66

    move/from16 v3, v67

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/util/SearchUtils;->getContinuationToken(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v68

    :goto_4
    new-instance v5, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v70, v0

    move-object/from16 v63, v5

    move-object/from16 v64, p1

    move-object/from16 v65, p2

    move-object/from16 v69, p3

    invoke-direct/range {v63 .. v70}, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/SearchActivitiesOperation;->startThreaded()V

    goto/16 :goto_1

    :cond_f
    const/16 v68, 0x0

    goto :goto_4

    :sswitch_19
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsAccountsData;->uploadChangedSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_1a
    const-string v4, "filename"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v188

    :try_start_0
    move-object/from16 v0, p1

    move-object/from16 v1, v188

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->insertCameraPhoto(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/service/EsService;->sLastCameraMediaLocation:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_5
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :catch_0
    move-exception v4

    const/4 v4, 0x0

    sput-object v4, Lcom/google/android/apps/plus/service/EsService;->sLastCameraMediaLocation:Ljava/lang/String;

    goto :goto_5

    :sswitch_1b
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNetworkData;->clearTransactionData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_1c
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/plus/content/EsNetworkData;->resetStatsData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_1d
    const-string v4, "apiInfo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v80

    check-cast v80, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const-string v4, "activity"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v74

    check-cast v74, Lcom/google/android/apps/plus/api/ApiaryActivity;

    const-string v4, "external_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v77

    const-string v4, "content"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v75

    const-string v4, "media"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v76

    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v79

    check-cast v79, Lcom/google/android/apps/plus/content/AudienceData;

    const-string v4, "loc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v78

    check-cast v78, Lcom/google/android/apps/plus/content/DbLocation;

    const-string v4, "content_deep_link_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v81

    const-string v4, "save_post_acl"

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v82

    const-string v4, "birthdata"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v83

    check-cast v83, Lcom/google/android/apps/plus/api/BirthdayData;

    const-string v4, "emotishare_embed"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v84

    check-cast v84, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    const-string v4, "square_embed"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v85

    check-cast v85, Lcom/google/android/apps/plus/content/DbEmbedSquare;

    const-string v4, "album_title"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v86

    const-string v4, "target_album_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v87

    const-string v4, "album_owner_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v88

    new-instance v5, Lcom/google/android/apps/plus/api/PostActivityOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v73, v0

    move-object/from16 v69, v5

    move-object/from16 v70, p1

    move-object/from16 v71, p2

    move-object/from16 v72, p3

    invoke-direct/range {v69 .. v88}, Lcom/google/android/apps/plus/api/PostActivityOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/api/ApiaryActivity;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/api/ApiaryApiInfo;Ljava/lang/String;ZLcom/google/android/apps/plus/api/BirthdayData;Lcom/google/android/apps/plus/content/DbEmbedEmotishare;Lcom/google/android/apps/plus/content/DbEmbedSquare;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/PostActivityOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_1e
    const-string v4, "apiInfo"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v80

    check-cast v80, Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    const-string v4, "url"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v94

    const-string v4, "applyPlusOne"

    const/4 v6, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v181

    const-string v4, "token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v201

    new-instance v93, Landroid/content/ContentValues;

    invoke-direct/range {v93 .. v93}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "state"

    if-eqz v181, :cond_10

    sget-object v4, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_PLUSONED:Ljava/lang/Integer;

    :goto_6
    move-object/from16 v0, v93

    invoke-virtual {v0, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "token"

    move-object/from16 v0, v93

    move-object/from16 v1, v201

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual/range {v80 .. v80}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSourceInfo()Lcom/google/android/apps/plus/api/ApiaryApiInfo;

    move-result-object v4

    const-string v6, "content://com.google.android.apps.plus.content.ApiProvider/plusone"

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "apiKey"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getApiKey()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v7, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "clientId"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getClientId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v7, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "apiVersion"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getSdkVersion()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v7, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "pkg"

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/ApiaryApiInfo;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v7, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v92

    new-instance v4, Ljava/lang/Thread;

    new-instance v89, Lcom/google/android/apps/plus/service/EsService$17;

    move-object/from16 v90, p0

    move-object/from16 v91, p1

    move-object/from16 v95, p3

    invoke-direct/range {v89 .. v95}, Lcom/google/android/apps/plus/service/EsService$17;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v89

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :cond_10
    sget-object v4, Lcom/google/android/apps/plus/external/PlatformContract$PlusOneContent;->STATE_NOTPLUSONED:Ljava/lang/Integer;

    goto :goto_6

    :sswitch_1f
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "rsvp_type"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v100

    const-string v4, "event_auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v4, Ljava/lang/Thread;

    new-instance v95, Lcom/google/android/apps/plus/service/EsService$18;

    move-object/from16 v96, p0

    move-object/from16 v97, p1

    move-object/from16 v98, p2

    move-object/from16 v99, v8

    move-object/from16 v101, v9

    move-object/from16 v102, p3

    invoke-direct/range {v95 .. v102}, Lcom/google/android/apps/plus/service/EsService$18;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v95

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_20
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    const-string v6, "event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v104

    check-cast v104, Lcom/google/api/services/plusi/model/PlusEvent;

    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v79

    check-cast v79, Lcom/google/android/apps/plus/content/AudienceData;

    const-string v4, "external_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v77

    new-instance v5, Lcom/google/android/apps/plus/api/CreateEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v108, v0

    move-object/from16 v101, v5

    move-object/from16 v102, p1

    move-object/from16 v103, p2

    move-object/from16 v105, v79

    move-object/from16 v106, v77

    move-object/from16 v107, p3

    invoke-direct/range {v101 .. v108}, Lcom/google/android/apps/plus/api/CreateEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/content/AudienceData;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/CreateEventOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_21
    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v4

    const-string v6, "event"

    move-object/from16 v0, p3

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v104

    check-cast v104, Lcom/google/api/services/plusi/model/PlusEvent;

    new-instance v5, Lcom/google/android/apps/plus/api/UpdateEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v106, v0

    move-object/from16 v101, v5

    move-object/from16 v102, p1

    move-object/from16 v103, p2

    move-object/from16 v105, p3

    invoke-direct/range {v101 .. v106}, Lcom/google/android/apps/plus/api/UpdateEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/UpdateEventOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_22
    const-string v4, "fingerprint"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v110

    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v113

    const-string v4, "photo_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v111

    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v4, Ljava/lang/Thread;

    new-instance v105, Lcom/google/android/apps/plus/service/EsService$19;

    move-object/from16 v106, p0

    move-object/from16 v107, p1

    move-object/from16 v108, p2

    move-object/from16 v109, v8

    move-object/from16 v114, p3

    invoke-direct/range {v105 .. v114}, Lcom/google/android/apps/plus/service/EsService$19;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Landroid/content/Intent;)V

    move-object/from16 v0, v105

    invoke-direct {v4, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_23
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    new-instance v5, Lcom/google/android/apps/plus/api/DeleteEventOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v120, v0

    move-object/from16 v114, v5

    move-object/from16 v115, p1

    move-object/from16 v116, p2

    move-object/from16 v117, v8

    move-object/from16 v118, v9

    move-object/from16 v119, p3

    invoke-direct/range {v114 .. v120}, Lcom/google/android/apps/plus/api/DeleteEventOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/DeleteEventOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_24
    new-instance v5, Lcom/google/android/apps/plus/api/GetEventThemesOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v5, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/GetEventThemesOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_25
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v119

    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v79

    check-cast v79, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v5, Lcom/google/android/apps/plus/api/EventInviteOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v122, v0

    move-object/from16 v114, v5

    move-object/from16 v115, p1

    move-object/from16 v116, p2

    move-object/from16 v117, v8

    move-object/from16 v118, v9

    move-object/from16 v120, v79

    move-object/from16 v121, p3

    invoke-direct/range {v114 .. v122}, Lcom/google/android/apps/plus/api/EventInviteOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/EventInviteOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_26
    const-string v4, "event_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v4, "auth_key"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v4, "blacklist"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v125

    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v126

    const-string v4, "email"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v127

    new-instance v5, Lcom/google/android/apps/plus/api/EventManageGuestOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v129, v0

    move-object/from16 v120, v5

    move-object/from16 v121, p1

    move-object/from16 v122, p2

    move-object/from16 v123, v8

    move-object/from16 v124, v9

    move-object/from16 v128, p3

    invoke-direct/range {v120 .. v129}, Lcom/google/android/apps/plus/api/EventManageGuestOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/EventManageGuestOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_27
    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v4, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->wipeout(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    goto/16 :goto_1

    :sswitch_28
    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v4, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveContactsStatsWipeoutNeeded(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_29
    const-string v4, "acc"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/EsAccount;

    const-string v6, "timestamp"

    const-wide/16 v11, -0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v6, v11, v12}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    move-object/from16 v0, p0

    invoke-static {v0, v4, v6, v7}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :sswitch_2a
    const-string v4, "package_name"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v131

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v131

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->getByPackageName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;

    move-result-object v191

    if-eqz v191, :cond_11

    move-object/from16 v0, v191

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->authorName:Ljava/lang/String;

    move-object/from16 v129, v0

    move-object/from16 v0, v191

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->creationSource:Ljava/lang/String;

    move-object/from16 v130, v0

    move-object/from16 v0, v191

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->data:Ljava/lang/String;

    move-object/from16 v132, v0

    move-object/from16 v0, v191

    iget-object v0, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->launchSource:Ljava/lang/String;

    move-object/from16 v133, v0

    move-object/from16 v128, p1

    invoke-static/range {v128 .. v133}, Lcom/google/android/apps/plus/util/PlayStoreInstaller;->notifyCompletedInstall(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, v131

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData;->removeByPackageName(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V

    const-string v4, "stream_install_interactive_post"

    move-object/from16 v0, v191

    iget-object v6, v0, Lcom/google/android/apps/plus/content/EsDeepLinkInstallsData$DeepLinkInstall;->launchSource:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v189

    new-instance v6, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-direct {v6, v4}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    if-eqz v189, :cond_12

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->CALL_TO_ACTION_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_7
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v6, v4}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    :cond_11
    new-instance v4, Lcom/google/android/apps/plus/service/ServiceResult;

    invoke-direct {v4}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>()V

    const/4 v6, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4, v6}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_12
    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->DEEP_LINK_INSTALL_COMPLETED_NOTIFICATION:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_7

    :sswitch_2b
    new-instance v5, Lcom/google/android/apps/plus/api/GetSquaresOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v136, v0

    const/16 v137, 0x0

    move-object/from16 v132, v5

    move-object/from16 v133, p1

    move-object/from16 v134, p2

    move-object/from16 v135, p3

    invoke-direct/range {v132 .. v137}, Lcom/google/android/apps/plus/api/GetSquaresOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/GetSquaresOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_2c
    const-string v4, "square_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v135

    new-instance v132, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;

    const/16 v136, 0x0

    const/16 v137, 0x0

    move-object/from16 v133, p1

    move-object/from16 v134, p2

    invoke-direct/range {v132 .. v137}, Lcom/google/android/apps/plus/api/GetViewerSquareOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    new-instance v136, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;

    const/16 v139, 0x0

    const/16 v140, 0x0

    const-string v141, "SQUARES"

    move-object/from16 v137, p1

    move-object/from16 v138, p2

    move-object/from16 v142, v135

    invoke-direct/range {v136 .. v142}, Lcom/google/android/apps/plus/api/GetVolumeControlsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v5, v0, v1, v2, v4}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    move-object/from16 v0, v132

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    move-object/from16 v0, v136

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->add(Lcom/google/android/apps/plus/network/HttpOperation;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/ApiaryBatchOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_2d
    const-string v4, "square_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v135

    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v141

    const-string v4, "square_action"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v142

    new-instance v5, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v144, v0

    move-object/from16 v137, v5

    move-object/from16 v138, p1

    move-object/from16 v139, p2

    move-object/from16 v140, v135

    move-object/from16 v143, p3

    invoke-direct/range {v137 .. v144}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_2e
    const-string v4, "square_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v135

    const-string v4, "square_member_list"

    const/4 v6, 0x2

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v147

    const-string v4, "cont_token"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v68

    const-string v4, "max_count"

    const/16 v6, 0x64

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v149

    new-instance v5, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v151, v0

    move-object/from16 v143, v5

    move-object/from16 v144, p1

    move-object/from16 v145, p2

    move-object/from16 v146, v135

    move-object/from16 v148, v68

    move-object/from16 v150, p3

    invoke-direct/range {v143 .. v151}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;ILandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/ReadSquareMembersOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_2f
    new-instance v4, Ljava/lang/Thread;

    new-instance v6, Lcom/google/android/apps/plus/service/EsService$20;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/apps/plus/service/EsService$20;-><init>(Lcom/google/android/apps/plus/service/EsService;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;)V

    invoke-direct {v4, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1

    :sswitch_30
    const-string v4, "square_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v135

    const-string v4, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v154

    const-string v4, "moderation_state"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v155

    new-instance v5, Lcom/google/android/apps/plus/api/EditModerationStateOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v157, v0

    move-object/from16 v150, v5

    move-object/from16 v151, p1

    move-object/from16 v152, p2

    move-object/from16 v153, v135

    move-object/from16 v156, p3

    invoke-direct/range {v150 .. v157}, Lcom/google/android/apps/plus/api/EditModerationStateOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/EditModerationStateOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_31
    const-string v4, "gaia_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v161

    const-string v4, "square_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v135

    const-string v4, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v154

    const-string v4, "source_stream_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v164

    const-string v4, "remove_post"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v165

    const-string v4, "report"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v166

    new-instance v5, Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v160, v0

    const-string v167, "SPAM"

    move-object/from16 v156, v5

    move-object/from16 v157, p1

    move-object/from16 v158, p2

    move-object/from16 v159, p3

    move-object/from16 v162, v135

    move-object/from16 v163, v154

    invoke-direct/range {v156 .. v167}, Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/SquaresReportAndBanOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_32
    new-instance v167, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    const/16 v170, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v172, v0

    move-object/from16 v168, p1

    move-object/from16 v169, p2

    move-object/from16 v171, p3

    invoke-direct/range {v167 .. v172}, Lcom/google/android/apps/plus/api/GetSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZLandroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual/range {v167 .. v167}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_33
    const-string v4, "enabled"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v171

    const-string v4, "location_type"

    const/4 v6, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v172

    const-string v4, "audience"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v173

    check-cast v173, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v5, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v175, v0

    move-object/from16 v168, v5

    move-object/from16 v169, p1

    move-object/from16 v170, p2

    move-object/from16 v174, p3

    invoke-direct/range {v168 .. v175}, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZILcom/google/android/apps/plus/content/AudienceData;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/SaveLocationSharingSettingsOperation;->startThreaded()V

    goto/16 :goto_1

    :sswitch_34
    const-string v4, "aid"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v154

    new-instance v5, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/service/EsService;->mOperationListener:Lcom/google/android/apps/plus/service/EsService$ServiceOperationListener;

    move-object/from16 v179, v0

    move-object/from16 v174, v5

    move-object/from16 v175, p1

    move-object/from16 v176, p2

    move-object/from16 v177, v154

    move-object/from16 v178, p3

    invoke-direct/range {v174 .. v179}, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v5}, Lcom/google/android/apps/plus/api/RecordWhatsHotImpressionOperation;->startThreaded()V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_d
        0x5 -> :sswitch_e
        0x1a -> :sswitch_34
        0x1f4 -> :sswitch_9
        0x1f7 -> :sswitch_c
        0x259 -> :sswitch_32
        0x2c4 -> :sswitch_5
        0x2c5 -> :sswitch_8
        0x2c7 -> :sswitch_b
        0x2ce -> :sswitch_6
        0x2cf -> :sswitch_7
        0x384 -> :sswitch_0
        0x385 -> :sswitch_1
        0x386 -> :sswitch_1f
        0x387 -> :sswitch_20
        0x388 -> :sswitch_21
        0x389 -> :sswitch_22
        0x38a -> :sswitch_24
        0x38b -> :sswitch_25
        0x38c -> :sswitch_23
        0x38d -> :sswitch_2
        0x38e -> :sswitch_3
        0x38f -> :sswitch_4
        0x3ea -> :sswitch_f
        0x3ec -> :sswitch_10
        0x3ed -> :sswitch_11
        0x3f0 -> :sswitch_14
        0x3f1 -> :sswitch_26
        0x3f2 -> :sswitch_16
        0x3f3 -> :sswitch_17
        0x3f4 -> :sswitch_12
        0x3f5 -> :sswitch_15
        0x3f6 -> :sswitch_13
        0x44c -> :sswitch_18
        0x456 -> :sswitch_19
        0x460 -> :sswitch_1a
        0x4b0 -> :sswitch_a
        0x7d0 -> :sswitch_1c
        0x7d1 -> :sswitch_1b
        0x898 -> :sswitch_1d
        0x899 -> :sswitch_1e
        0x8fc -> :sswitch_27
        0x8fd -> :sswitch_28
        0x960 -> :sswitch_29
        0xa28 -> :sswitch_2a
        0xa8c -> :sswitch_2b
        0xa8d -> :sswitch_2c
        0xa8e -> :sswitch_2d
        0xa8f -> :sswitch_2e
        0xa90 -> :sswitch_2f
        0xa91 -> :sswitch_30
        0xa92 -> :sswitch_31
        0xaf0 -> :sswitch_33
    .end sparse-switch
.end method

.method private static putAccountSettingsResponse(ILcom/google/android/apps/plus/content/AccountSettingsData;)V
    .locals 2
    .param p0    # I
    .param p1    # Lcom/google/android/apps/plus/content/AccountSettingsData;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private static putOutOfBoxResponse(ILcom/google/api/services/plusi/model/MobileOutOfBoxResponse;)V
    .locals 2
    .param p0    # I
    .param p1    # Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sOutOfBoxResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public static readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x38d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "pollingtoken"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "resumetoken"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "invitationtoken"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "event_auth_key"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "fetchnewer"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static readEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    const/4 v3, 0x1

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x38d

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "event_auth_key"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "fetchnewer"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "resolvetokens"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final readSquareMembers(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;I)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa8f

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "square_member_list"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "cont_token"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "max_count"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/analytics/OzViews;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "extra_participant_ids"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v2, "extra_circle_ids"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    if-eqz p5, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->ADD_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_0
    invoke-static {p0, p1, v0, p2, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/analytics/OzActions;->REMOVE_CIRCLE_MEMBERS:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_0
.end method

.method private static recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;[Ljava/lang/String;Z)V
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/android/apps/plus/analytics/OzViews;
    .param p4    # [Ljava/lang/String;
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Lcom/google/android/apps/plus/analytics/OzViews;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    if-eqz p4, :cond_1

    array-length v0, p4

    if-lez v0, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object v6, p4

    array-length v9, p4

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v7, v6, v8

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V

    :cond_1
    return-void
.end method

.method public static recordWhatsHotImpression(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/EsServiceListener;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static removeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static removeAccountSettingsResponse(I)Lcom/google/android/apps/plus/content/AccountSettingsData;
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AccountSettingsData;

    return-object v0
.end method

.method public static removeIncompleteOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .locals 2
    .param p0    # I

    invoke-static {p0}, Lcom/google/android/apps/plus/service/EsService;->removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method public static removeOutOfBoxResponse(I)Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sOutOfBoxResponses:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    return-object v0
.end method

.method public static final removeReportAndBan(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z
    .param p7    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xa92

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "square_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "source_stream_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "remove_post"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "report"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;
    .locals 2
    .param p0    # I

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sResults:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/ServiceResult;

    return-object v0
.end method

.method public static reportActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "source_stream_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static reportPhotoAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;JLjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static reportPhotoComment$3486cdbb(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/Long;Ljava/lang/String;ZZ)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/Long;
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .param p5    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x37

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "comment_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "delete"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_undo"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static reportProfileAbuse(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "abuse_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static reshareActivity(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/AudienceData;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/AudienceData;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "aid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static saveLastContactedTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # J

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x960

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "timestamp"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static final saveLocationSharingSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ZILcom/google/android/apps/plus/content/AudienceData;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Z
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/content/AudienceData;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xaf0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "enabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "location_type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "audience"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static savePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ZLjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x42

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "full_res"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "description"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static scheduleSyncAlarm(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.plus.content.sync"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "op"

    const/16 v2, 0x3ea

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public static scheduleUnconditionalSyncAlarm(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v7, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-direct {v7, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "com.google.android.apps.plus.content.unconditionalsync"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "op"

    const/16 v2, 0x3ed

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p0, v1, v7, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    const-wide/32 v4, 0x2932e00

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    return-void
.end method

.method public static searchActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;IZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x44c

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "search_query"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "search_mode"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "newer"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendEventRsvp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x386

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "rsvp_type"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "event_auth_key"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static sendOutOfBoxRequest(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x258

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "content"

    new-instance v2, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;

    invoke-direct {v2, p2}, Lcom/google/android/apps/plus/oob/OutOfBoxRequestParcelable;-><init>(Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method private static setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    const-string v0, "EsService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v1, "EsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "setActiveAccount: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sput-object p0, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/EsAccount;->getPersonId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static setCircleMembership(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/Integer;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;

    const/4 v5, 0x1

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v6

    const-string v0, "op"

    const/16 v1, 0x2be

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "acc"

    invoke-virtual {v6, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "person_id"

    invoke-virtual {v6, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "person_name"

    invoke-virtual {v6, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "circles_to_add"

    invoke-virtual {v6, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "circles_to_remove"

    invoke-virtual {v6, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;[Ljava/lang/String;Z)V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;[Ljava/lang/String;Z)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static setCircleMembershipFromPromo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v5, 0x1

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v1, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v6

    const-string v0, "op"

    const/16 v1, 0x2d1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "acc"

    invoke-virtual {v6, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v0, "person_id"

    invoke-virtual {v6, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "person_name"

    invoke-virtual {v6, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "circles_to_add"

    invoke-virtual {v6, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "circles_to_remove"

    invoke-virtual {v6, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "aid"

    invoke-virtual {v6, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, p0

    move-object v1, p1

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;[Ljava/lang/String;Z)V

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->recordUpdateCircleAction(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzViews;Ljava/util/ArrayList;[Ljava/lang/String;Z)V

    invoke-static {p0, v6}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public static setCoverPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;IZ)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "top_offset"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "is_gallery_photo"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setCoverPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/graphics/RectF;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/RectF;
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "coordinates"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "is_gallery_photo"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setPersonBlocked(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "person_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "blocked"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static setPersonMuted(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2c1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "person_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "muted"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static setScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;ILjava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2d0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "top_offset"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "layout"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setScrapbookInfo(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/RectF;
    .param p4    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2d0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "coordinates"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "layout"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static setVolumeControl(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;IZ)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2cf

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "volume_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "volume_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "volume"

    new-instance v2, Lcom/google/android/apps/plus/content/VolumeSettings;

    invoke-direct {v2, p5, p4}, Lcom/google/android/apps/plus/content/VolumeSettings;-><init>(ZI)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static sharePhotosToEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/util/List;)I
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)I"
        }
    .end annotation

    sget-object v3, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v4, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    new-array v2, v3, [J

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    aput-wide v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v3, "op"

    const/16 v4, 0x38f

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "acc"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "event_id"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "photo_ids"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v3

    return v3
.end method

.method private static startCommand(Landroid/content/Context;Landroid/content/Intent;)I
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->ensureMainThread()V

    invoke-static {}, Lcom/google/android/apps/plus/service/EsService;->generateRequestId()I

    move-result v1

    const-string v2, "rid"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    invoke-virtual {v5}, Landroid/os/Bundle;->size()I

    move-result v3

    invoke-virtual {v7}, Landroid/os/Bundle;->size()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v7}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v9, "rid"

    invoke-virtual {v9, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_0

    const-string v3, "EsService"

    const/4 v4, 0x2

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "EsService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Op was pending: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "op"

    const/4 v6, -0x1

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const-string v3, "rid"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_7

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/service/IntentPool;->put(Landroid/content/Intent;)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_2
    return v1

    :cond_3
    invoke-virtual {v7, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    if-nez v9, :cond_5

    const-string v9, "EsService"

    const/4 v11, 0x3

    invoke-static {v9, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "pending request id key ["

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v11, "] has value null!"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    if-eqz v10, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_5
    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    :cond_7
    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sPendingIntents:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_2

    :cond_8
    move v3, v4

    goto/16 :goto_0
.end method

.method public static suggestedTagApproval(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3b

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "taggee_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "shape_id"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "approved"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static syncBlockedPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static syncComplete(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3ec

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "content"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    return-void
.end method

.method public static syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Z)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Z

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "refresh"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static tellServerNotificationsWereRead(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0xcb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    return-object v1
.end method

.method public static unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/EsServiceListener;

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method private static updateEsApiProvider(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "content://com.google.android.apps.plus.content.ApiProvider/account"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    return-void
.end method

.method public static updateEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/api/services/plusi/model/PlusEvent;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Lcom/google/api/services/plusi/model/PlusEvent;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x388

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "event"

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/api/services/plusi/model/PlusEventJson;->toByteArray(Ljava/lang/Object;)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static updateEventPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x389

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "event_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "gaia_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "photo_id"

    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v1, "fingerprint"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static updateNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/apps/plus/service/EsService$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/plus/service/EsService$2;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public static upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static uploadCoverPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[BLandroid/graphics/RectF;)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [B
    .param p3    # Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2ca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "album_id"

    const-string v2, "scrapbook"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "coordinates"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static uploadFullBleedPhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[BI)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [B
    .param p3    # I

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2ca

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "album_id"

    const-string v2, "scrapbook"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    const-string v1, "top_offset"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method

.method public static uploadImageThumbnail(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/apps/plus/api/MediaRef;)Ljava/lang/Integer;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/api/MediaRef;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/plus/content/EsAccount;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/plus/api/MediaRef;",
            ")",
            "Ljava/lang/Integer;"
        }
    .end annotation

    sget-object v2, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v3, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "op"

    const/16 v3, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "acc"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "album_title"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "album_label"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {p4, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    const-string v2, "stream_ids"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "media_ref"

    invoke-virtual {v0, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2
.end method

.method private static uploadPeopleSuggestionEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    invoke-static {p0}, Lcom/google/android/apps/plus/network/HttpOperation;->isConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x3f6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    :cond_0
    return-void
.end method

.method public static uploadProfilePhoto(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;[B)I
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # [B

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sIntentPool:Lcom/google/android/apps/plus/service/IntentPool;

    const-class v2, Lcom/google/android/apps/plus/service/EsService;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/service/IntentPool;->get(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "op"

    const/16 v2, 0x2cb

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "acc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->startCommand(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v1

    return v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    sget-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/google/android/apps/plus/content/PeopleData;->getFactory()Lcom/google/android/apps/plus/content/PeopleData$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sPeopleDataFactory:Lcom/google/android/apps/plus/content/PeopleData$Factory;

    invoke-static {}, Lcom/google/android/apps/plus/api/MuteUserOperation;->getFactory()Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sMuteUserOperationFactory:Lcom/google/android/apps/plus/api/MuteUserOperation$Factory;

    invoke-static {}, Lcom/google/android/apps/plus/api/BlockUserOperation;->getFactory()Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/EsService;->sBlockUserOperationFactory:Lcom/google/android/apps/plus/api/BlockUserOperation$Factory;

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/service/ServiceThread;

    sget-object v1, Lcom/google/android/apps/plus/service/EsService;->sHandler:Landroid/os/Handler;

    const-string v2, "ServiceThread"

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/apps/plus/service/ServiceThread;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceThread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ServiceThread;->quit()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    :cond_0
    return-void
.end method

.method public final onIntentProcessed(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V
    .locals 60
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/google/android/apps/plus/service/ServiceResult;
    .param p3    # Ljava/lang/Object;

    move-object/from16 v30, p0

    const-string v10, "op"

    const/4 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v45

    const-string v10, "rid"

    const/4 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    sparse-switch v45, :sswitch_data_0

    const-string v10, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "onIntentProcessed: Unhandled op code: "

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v45

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    :sswitch_0
    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/plus/service/EsService;->finalizeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;)V

    return-void

    :sswitch_1
    move-object/from16 v23, p3

    check-cast v23, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->getException()Ljava/lang/Exception;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->isOutOfBoxError(Ljava/lang/Throwable;)Z

    move-result v10

    if-eqz v10, :cond_2

    :cond_1
    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sAccountSettingsResponses:Ljava/util/Map;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->restoreAccountSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_2
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_1
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAccountAdded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_1

    :sswitch_2
    move-object/from16 v23, p3

    check-cast v23, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-nez v10, :cond_3

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsAccountsData;->restoreAccountSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_3
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAccountActivated$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_2

    :sswitch_3
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_3
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    goto :goto_3

    :cond_4
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    goto/16 :goto_0

    :sswitch_4
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAccountUpgraded(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4

    :sswitch_5
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_5
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "circle_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "view"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    const-string v10, "loc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "newer"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    const-string v11, "max_length"

    const/4 v13, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v11, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetActivities$35a362dd(IZILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5

    :sswitch_6
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_6
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "square_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetActivity$51e3eb1f(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_6

    :sswitch_7
    if-eqz p3, :cond_5

    check-cast p3, Lcom/google/android/apps/plus/api/GetAudienceOperation;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/api/GetAudienceOperation;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v29

    :goto_7
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_8
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, v29

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetActivityAudience$6db92636(ILcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_8

    :cond_5
    const/16 v29, 0x0

    goto :goto_7

    :sswitch_8
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_9
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_9

    :sswitch_9
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReshareActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_a

    :sswitch_a
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_b
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_b

    :sswitch_b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_c
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onMuteActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_c

    :sswitch_c
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_d
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportActivity$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_d

    :sswitch_d
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_e
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetAllPhotoTilesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_e

    :sswitch_e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_f
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetAlbumListComplete$6a63df5(I)V

    goto :goto_f

    :sswitch_f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_10
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetAlbumComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_10

    :sswitch_10
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_11
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetStreamPhotosComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_11

    :sswitch_11
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_12
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreatePhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_12

    :sswitch_12
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_13
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditPhotoCommentComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_13

    :sswitch_13
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_14
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeletePhotoCommentsComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_14

    :sswitch_14
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_15
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "is_undo"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v11, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportPhotoCommentsComplete$141714ed(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_15

    :sswitch_15
    const-string v10, "plus_oned"

    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v49

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_16
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move/from16 v0, v49

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPhotoPlusOneComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_16

    :sswitch_16
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_17
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetPhotosOfUserComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_17

    :sswitch_17
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_18
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "photo_id"

    const-wide/16 v16, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v10

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v11, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onNameTagApprovalComplete$4894d499(IJLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_18

    :sswitch_18
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_19
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "photo_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onTagSuggestionApprovalComplete$63505a2b(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_19

    :sswitch_19
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_1a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPhotosHomeComplete$6a63df5(I)V

    goto :goto_1a

    :sswitch_1a
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "photo_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_1b
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeletePhotosComplete$5d3076b3(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_1b

    :sswitch_1b
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "photo_id"

    const-wide/16 v16, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_1c
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportPhotoComplete$4894d499(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_1c

    :sswitch_1c
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "photo_id"

    const-wide/16 v16, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v16

    invoke-virtual {v0, v10, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v46

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_1d
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-wide/from16 v0, v46

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetPhoto$4894d499(IJ)V

    goto :goto_1d

    :sswitch_1d
    const-string v10, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/GetPhotosSettingsOperation;->getSettings()Lcom/google/api/services/plusi/model/DataSettings;

    move-result-object v54

    if-eqz v54, :cond_6

    move-object/from16 v0, v54

    iget-object v10, v0, Lcom/google/api/services/plusi/model/DataSettings;->enableDownloadPhoto:Ljava/lang/Boolean;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/PrimitiveUtils;->safeBoolean(Ljava/lang/Boolean;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/16 v33, 0x1

    :goto_1e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_1f
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move/from16 v0, v33

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetPhotoSettings$6e3d3b8d(IZ)V

    goto :goto_1f

    :cond_6
    const/16 v33, 0x0

    goto :goto_1e

    :sswitch_1e
    const-string v10, "full_res"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v10, "description"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v6, 0x0

    const/4 v9, 0x0

    if-eqz p3, :cond_7

    move-object/from16 v57, p3

    check-cast v57, Lcom/google/android/apps/plus/api/SavePhotoOperation;

    invoke-virtual/range {v57 .. v57}, Lcom/google/android/apps/plus/api/SavePhotoOperation;->getSaveToFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual/range {v57 .. v57}, Lcom/google/android/apps/plus/api/SavePhotoOperation;->getContentType()Ljava/lang/String;

    move-result-object v9

    :cond_7
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_20
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v10, p2

    invoke-virtual/range {v4 .. v10}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSavePhoto(ILjava/io/File;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_20

    :sswitch_1f
    const-string v10, "media_refs"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v42

    check-cast v42, Ljava/util/ArrayList;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_21
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, v42

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onLocalPhotoDelete(ILjava/util/ArrayList;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_21

    :sswitch_20
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_22
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateComment$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_22

    :sswitch_21
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_23
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateEventComment$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_23

    :sswitch_22
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_24
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_24

    :sswitch_23
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_25
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteComment$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_25

    :sswitch_24
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_26
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "is_undo"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v11, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onModerateComment$56b78e3(ILjava/lang/String;ZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_26

    :sswitch_25
    const-string v10, "plus_oned"

    const/4 v11, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v48

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_27
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "comment_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move/from16 v0, v48

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPlusOneComment$56b78e3(ZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_27

    :sswitch_26
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_28
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreatePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_28

    :sswitch_27
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_29
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeletePostPlusOne$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_29

    :sswitch_28
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "loc_query"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onLocationQuery$260d7f24(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_2a

    :sswitch_29
    const/16 v53, 0x0

    if-eqz p3, :cond_8

    check-cast p3, Lcom/google/android/apps/plus/api/UploadMediaOperation;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/api/UploadMediaOperation;->getUploadMediaResponse()Lcom/google/api/services/plusi/model/UploadMediaResponse;

    move-result-object v52

    if-eqz v52, :cond_8

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/google/api/services/plusi/model/UploadMediaResponse;->photo:Lcom/google/api/services/plusi/model/DataPhoto;

    if-eqz v10, :cond_8

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/google/api/services/plusi/model/UploadMediaResponse;->photo:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    if-eqz v10, :cond_8

    move-object/from16 v0, v52

    iget-object v10, v0, Lcom/google/api/services/plusi/model/UploadMediaResponse;->photo:Lcom/google/api/services/plusi/model/DataPhoto;

    iget-object v10, v10, Lcom/google/api/services/plusi/model/DataPhoto;->original:Lcom/google/api/services/plusi/model/DataImage;

    iget-object v0, v10, Lcom/google/api/services/plusi/model/DataImage;->url:Ljava/lang/String;

    move-object/from16 v53, v0

    :cond_8
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2b
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, v53

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onImageThumbnailUploaded$51902fbe(ILjava/lang/String;)V

    goto :goto_2b

    :sswitch_2a
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2c
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEventHomeRequestComplete$b5e9bbb(I)V

    goto :goto_2c

    :sswitch_2b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2d
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_2d

    :sswitch_2c
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2e
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReadEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_2e

    :sswitch_2d
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_2f
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetEventInviteesComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_2f

    :sswitch_2e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_30
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSharePhotosToEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_30

    :sswitch_2f
    if-eqz p3, :cond_b

    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/MarkItemReadOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getErrorCode()I

    move-result v34

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getException()Ljava/lang/Exception;

    move-result-object v37

    const/16 v10, 0xc8

    move/from16 v0, v34

    if-ne v0, v10, :cond_b

    if-nez v37, :cond_b

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->isNotificationType()Z

    move-result v10

    if-nez v10, :cond_b

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getItemIds()Ljava/util/List;

    move-result-object v27

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/MarkItemReadOperation;->getCreationSourceIds()Ljava/util/List;

    move-result-object v32

    if-eqz v27, :cond_b

    if-eqz v32, :cond_b

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_b

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_b

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v10

    invoke-interface/range {v32 .. v32}, Ljava/util/List;->size()I

    move-result v11

    if-ne v10, v11, :cond_b

    const/16 v39, 0x0

    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v56

    :goto_31
    move/from16 v0, v39

    move/from16 v1, v56

    if-ge v0, v1, :cond_b

    move-object/from16 v0, v27

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v32

    move/from16 v1, v39

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v31

    check-cast v31, Ljava/lang/String;

    const-string v10, "extra_activity_id"

    move-object/from16 v0, v26

    invoke-static {v10, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->createExtras(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v38

    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_a

    if-nez v38, :cond_9

    new-instance v38, Landroid/os/Bundle;

    invoke-direct/range {v38 .. v38}, Landroid/os/Bundle;-><init>()V

    :cond_9
    const-string v10, "extra_creation_source_id"

    move-object/from16 v0, v38

    move-object/from16 v1, v31

    invoke-virtual {v0, v10, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_a
    const-string v10, "start_view"

    sget-object v11, Lcom/google/android/apps/plus/analytics/OzViews;->HOME:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/analytics/OzViews;->ordinal()I

    move-result v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v59

    invoke-static/range {v59 .. v59}, Lcom/google/android/apps/plus/analytics/OzViews;->valueOf(I)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v58

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v11, Lcom/google/android/apps/plus/analytics/OzActions;->STREAM_MARK_ACTIVITY_AS_READ:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v30

    move-object/from16 v1, v58

    move-object/from16 v2, v38

    invoke-static {v0, v10, v11, v1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;Landroid/os/Bundle;)V

    add-int/lit8 v39, v39, 0x1

    goto :goto_31

    :cond_b
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_30
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_31
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_32
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-virtual {v4, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onChangeNotificationsRequestComplete$6a63df5(Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_32

    :sswitch_32
    if-eqz p3, :cond_c

    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/GetNotificationSettingsOperation;->getNotificationSettings()Lcom/google/android/apps/plus/content/NotificationSettingsData;

    move-result-object v54

    :goto_33
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_34
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v54

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetNotificationSettings$434dcfc8(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V

    goto :goto_34

    :cond_c
    const/16 v54, 0x0

    goto :goto_33

    :sswitch_33
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_35
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_d

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSyncNotifications$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_35

    :cond_d
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_34
    move-object/from16 v23, p3

    check-cast v23, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v23, :cond_e

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    :cond_e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_36
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onOobRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_36

    :sswitch_35
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_37
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_f

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "content"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_37

    :cond_f
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_36
    move-object/from16 v51, p3

    check-cast v51, Ljava/util/List;

    if-eqz v51, :cond_12

    const-string v10, "EsService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface/range {v51 .. v51}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_38
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_10

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v50

    check-cast v50, Ljava/lang/String;

    const-string v10, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "OP_ACCOUNTS_CHANGED removed: "

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v50

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_38

    :cond_10
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v10, :cond_12

    invoke-interface/range {v51 .. v51}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :cond_11
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_12

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sActiveAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    const/4 v10, 0x0

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsService;->setActiveAccount(Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->updateAllWidgets$1a552341(Landroid/content/Context;)V

    :cond_12
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_37
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_38
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_39
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "person_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetProfileAndContactComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_39

    :sswitch_39
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_3a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onMutateProfileComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_3a

    :sswitch_3a
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/network/HttpOperation;->isConnected(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_13

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v30

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/service/EsService;->uploadPeopleSuggestionEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_13
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_3b
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_3c
    if-eqz p3, :cond_15

    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;->getErrorCode()I

    move-result v34

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;->getException()Ljava/lang/Exception;

    move-result-object v37

    const/16 v10, 0xc8

    move/from16 v0, v34

    if-ne v0, v10, :cond_14

    if-eqz v37, :cond_16

    :cond_14
    const-string v10, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "RecordSuggestionActionsOperation failed ex "

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " errorCode "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v34

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/RecordSuggestionActionsOperation;->getEvents()Ljava/util/ArrayList;

    move-result-object v35

    if-eqz v35, :cond_15

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_15

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v30

    move-object/from16 v1, v35

    invoke-static {v0, v10, v1}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/ArrayList;)V

    const-string v10, "EsService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_15

    const-string v10, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "Insert "

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v35 .. v35}, Ljava/util/ArrayList;->size()I

    move-result v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " people suggestion events back to the database"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    :goto_3b
    const/16 p2, 0x0

    goto/16 :goto_0

    :cond_16
    const-string v10, "EsService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_15

    const-string v10, "EsService"

    const-string v11, "RecordSuggestionActionsOperation was successful"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3b

    :sswitch_3d
    const-string v10, "analytics_sync"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v43

    if-eqz v43, :cond_17

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v30

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/service/EsService;->postClientLogsOperation(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_17
    const-string v10, "analytics_sync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_18

    const-string v10, "analytics_sync"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_18
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_3e
    if-eqz p3, :cond_1a

    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/PostClientLogsOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getErrorCode()I

    move-result v34

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getException()Ljava/lang/Exception;

    move-result-object v37

    const/16 v10, 0xc8

    move/from16 v0, v34

    if-ne v0, v10, :cond_19

    if-eqz v37, :cond_1b

    :cond_19
    const-string v10, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "PostClientLogsOperation failed ex "

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v37

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " errorCode "

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, v34

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getClientOzEvents()Ljava/util/List;

    move-result-object v36

    if-eqz v36, :cond_1a

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1a

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v30

    move-object/from16 v1, v36

    invoke-static {v0, v10, v1}, Lcom/google/android/apps/plus/service/EsService;->insertAnalyticsEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    const-string v10, "EsService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1a

    const-string v10, "EsService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v13, "Insert "

    invoke-direct {v11, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v13, " analytics events back to the database"

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    :goto_3c
    const/16 p2, 0x0

    goto/16 :goto_0

    :cond_1b
    const-string v10, "EsService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_1a

    const-string v10, "EsService"

    const-string v11, "PostClientLogsOperation was successful"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3c

    :sswitch_3f
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_40
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_3d
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetCircleMembershipComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_3d

    :sswitch_41
    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->getRemovedCircles()Ljava/util/ArrayList;

    move-result-object v15

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->getAddedCircles()Ljava/util/ArrayList;

    move-result-object v14

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/PromoSetCircleMembershipOperation;->getPersonId()Ljava/lang/String;

    move-result-object v12

    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_3e
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object v10, v4

    move v11, v5

    move-object/from16 v13, p2

    invoke-virtual/range {v10 .. v15}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPromoSetCircleMembershipComplete$2f8b9cab(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto :goto_3e

    :sswitch_42
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_3f
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onRemovePeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_3f

    :sswitch_43
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_40
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onAddPeopleToCirclesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_40

    :sswitch_44
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_41
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "muted"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetMutedRequestComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_41

    :sswitch_45
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_42
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetBlockedRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_42

    :sswitch_46
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_43
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReportAbuseRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_43

    :sswitch_47
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_44
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateCircleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_44

    :sswitch_48
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_45
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onModifyCirclePropertiesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_45

    :sswitch_49
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_46
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetVolumeControlsRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_46

    :sswitch_4a
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_47
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteCirclesRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_47

    :sswitch_4b
    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-eqz v10, :cond_1c

    const-string v10, "suggestions_ui"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v10, "person_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v18

    const-string v10, "suggestion_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v19

    new-instance v16, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    const-string v17, "REJECT"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v21

    invoke-direct/range {v16 .. v22}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;J)V

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;)V

    :cond_1c
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_48
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDismissSuggestedPeopleRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_48

    :sswitch_4c
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_4d
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v25

    if-eqz v25, :cond_1e

    if-eqz p3, :cond_1d

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_1f

    sget-object v10, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_ENABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_49
    sget-object v11, Lcom/google/android/apps/plus/analytics/OzViews;->GENERAL_SETTINGS:Lcom/google/android/apps/plus/analytics/OzViews;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-static {v0, v1, v10, v11}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_1d
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/service/EsService;->uploadPeopleSuggestionEvents(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    :cond_1e
    const/16 p2, 0x0

    goto/16 :goto_0

    :cond_1f
    sget-object v10, Lcom/google/android/apps/plus/analytics/OzActions;->CAMERA_SYNC_DISABLED:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_49

    :sswitch_4e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCircleSyncComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4a

    :sswitch_4f
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_50
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4b
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onUploadProfilePhotoComplete(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4b

    :sswitch_51
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4c
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onUploadCoverPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4c

    :sswitch_52
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4d
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetCoverPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4d

    :sswitch_53
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4e
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSetScrapbookInfoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4e

    :sswitch_54
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_4f
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_4f

    :sswitch_55
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_50
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteProfilePlusOneRequestComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_50

    :sswitch_56
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_51
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSearchActivitiesComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_51

    :sswitch_57
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_58
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_52
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onInsertCameraPhotoComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_52

    :sswitch_59
    const/16 p2, 0x0

    goto/16 :goto_0

    :sswitch_5a
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_53
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_20

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onPostActivityResult(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_53

    :cond_20
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "analytics"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v28, :cond_0

    if-eqz v23, :cond_0

    sget-object v10, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_SHARE_POST_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    move-object/from16 v2, v28

    invoke-static {v0, v1, v2, v10}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto/16 :goto_0

    :sswitch_5b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_54
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_21

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_54

    :cond_21
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v10

    if-nez v10, :cond_22

    sget-object v24, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE:Lcom/google/android/apps/plus/analytics/OzActions;

    :goto_55
    const-string v10, "analytics"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v28, :cond_0

    if-eqz v24, :cond_0

    move-object/from16 v0, v30

    move-object/from16 v1, v23

    move-object/from16 v2, v28

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;)V

    goto/16 :goto_0

    :cond_22
    sget-object v24, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_WRITE_PLUSONE_ERROR:Lcom/google/android/apps/plus/analytics/OzActions;

    goto :goto_55

    :sswitch_5c
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_56
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSendEventRsvpComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_56

    :sswitch_5d
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_57
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onCreateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_57

    :sswitch_5e
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_58
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onUpdateEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_58

    :sswitch_5f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_59
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeleteEventComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_59

    :sswitch_60
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_5a
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_5a

    :sswitch_61
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_5b
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEventInviteComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5b

    :sswitch_62
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_5c
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEventManageGuestComplete(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5c

    :sswitch_63
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_5d
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetSquaresComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5d

    :sswitch_64
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_5e
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetSquareComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_5e

    :sswitch_65
    if-eqz p3, :cond_23

    check-cast p3, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/plus/api/EditSquareMembershipOperation;->getIsBlockingModerator()Z

    move-result v41

    :goto_5f
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_60
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move/from16 v0, v41

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditSquareMembershipComplete$4cb07f77(IZLcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_60

    :cond_23
    const/16 v41, 0x0

    goto :goto_5f

    :sswitch_66
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_61
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onReadSquareMembersComplete$6a63df5(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_61

    :sswitch_67
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_62
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "square_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onDeclineInvitationComplete$63505a2b(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_62

    :sswitch_68
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_63
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "square_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "moderation_state"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onEditModerationStateComplete$1b131e9(ILjava/lang/String;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_63

    :sswitch_69
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_64
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    const-string v10, "gaia_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    const-string v10, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onRemoveReportAndBan$51e3eb1f(ILcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_64

    :sswitch_6a
    if-eqz p3, :cond_24

    move-object/from16 v44, p3

    check-cast v44, Lcom/google/android/apps/plus/api/GetSettingsOperation;

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/apps/plus/api/GetSettingsOperation;->getAccountSettings()Lcom/google/android/apps/plus/content/AccountSettingsData;

    move-result-object v55

    :goto_65
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_66
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, v55

    move-object/from16 v1, p2

    invoke-virtual {v4, v5, v10, v0, v1}, Lcom/google/android/apps/plus/service/EsServiceListener;->onGetSettings(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/AccountSettingsData;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_66

    :cond_24
    const/16 v55, 0x0

    goto :goto_65

    :sswitch_6b
    sget-object v10, Lcom/google/android/apps/plus/service/EsService;->sListeners:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v40

    :goto_67
    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface/range {v40 .. v40}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/service/EsServiceListener;

    const-string v10, "acc"

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/EsAccount;

    move-object/from16 v0, p2

    invoke-virtual {v4, v5, v10, v0}, Lcom/google/android/apps/plus/service/EsServiceListener;->onSaveLocationSharingSettings(ILcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/ServiceResult;)V

    goto :goto_67

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_3
        0x4 -> :sswitch_36
        0x5 -> :sswitch_37
        0x6 -> :sswitch_4
        0x7 -> :sswitch_2
        0xb -> :sswitch_6
        0xc -> :sswitch_7
        0xe -> :sswitch_8
        0x10 -> :sswitch_26
        0x11 -> :sswitch_27
        0x12 -> :sswitch_b
        0x13 -> :sswitch_c
        0x14 -> :sswitch_a
        0x15 -> :sswitch_9
        0x16 -> :sswitch_5
        0x17 -> :sswitch_5
        0x19 -> :sswitch_2f
        0x1a -> :sswitch_30
        0x1e -> :sswitch_20
        0x1f -> :sswitch_21
        0x20 -> :sswitch_22
        0x21 -> :sswitch_23
        0x22 -> :sswitch_24
        0x23 -> :sswitch_25
        0x29 -> :sswitch_28
        0x32 -> :sswitch_e
        0x33 -> :sswitch_f
        0x34 -> :sswitch_10
        0x35 -> :sswitch_11
        0x36 -> :sswitch_13
        0x37 -> :sswitch_14
        0x38 -> :sswitch_15
        0x39 -> :sswitch_16
        0x3a -> :sswitch_17
        0x3b -> :sswitch_18
        0x3c -> :sswitch_19
        0x3d -> :sswitch_1a
        0x3e -> :sswitch_1c
        0x3f -> :sswitch_1f
        0x40 -> :sswitch_12
        0x41 -> :sswitch_1d
        0x42 -> :sswitch_1e
        0x43 -> :sswitch_1b
        0x44 -> :sswitch_d
        0xc9 -> :sswitch_30
        0xca -> :sswitch_33
        0xcb -> :sswitch_30
        0xcc -> :sswitch_31
        0xcd -> :sswitch_32
        0xce -> :sswitch_30
        0x1f4 -> :sswitch_4e
        0x1f7 -> :sswitch_4f
        0x258 -> :sswitch_34
        0x259 -> :sswitch_6a
        0x2be -> :sswitch_40
        0x2bf -> :sswitch_38
        0x2c0 -> :sswitch_39
        0x2c1 -> :sswitch_44
        0x2c2 -> :sswitch_45
        0x2c3 -> :sswitch_46
        0x2c4 -> :sswitch_47
        0x2c5 -> :sswitch_4a
        0x2c6 -> :sswitch_42
        0x2c7 -> :sswitch_4b
        0x2c8 -> :sswitch_43
        0x2c9 -> :sswitch_52
        0x2ca -> :sswitch_51
        0x2cb -> :sswitch_50
        0x2cc -> :sswitch_54
        0x2cd -> :sswitch_55
        0x2ce -> :sswitch_48
        0x2cf -> :sswitch_49
        0x2d0 -> :sswitch_53
        0x2d1 -> :sswitch_41
        0x320 -> :sswitch_29
        0x384 -> :sswitch_2a
        0x385 -> :sswitch_2b
        0x386 -> :sswitch_5c
        0x387 -> :sswitch_5d
        0x388 -> :sswitch_5e
        0x38a -> :sswitch_60
        0x38b -> :sswitch_61
        0x38c -> :sswitch_5f
        0x38d -> :sswitch_2c
        0x38e -> :sswitch_2d
        0x38f -> :sswitch_2e
        0x3ea -> :sswitch_4d
        0x3ec -> :sswitch_35
        0x3ed -> :sswitch_4c
        0x3f0 -> :sswitch_3d
        0x3f1 -> :sswitch_62
        0x3f2 -> :sswitch_3e
        0x3f3 -> :sswitch_3f
        0x3f4 -> :sswitch_3a
        0x3f5 -> :sswitch_3b
        0x3f6 -> :sswitch_3c
        0x44c -> :sswitch_56
        0x456 -> :sswitch_57
        0x460 -> :sswitch_58
        0x7d0 -> :sswitch_59
        0x7d1 -> :sswitch_59
        0x898 -> :sswitch_5a
        0x899 -> :sswitch_5b
        0x8fc -> :sswitch_0
        0x8fd -> :sswitch_0
        0x960 -> :sswitch_0
        0xa28 -> :sswitch_0
        0xa8c -> :sswitch_63
        0xa8d -> :sswitch_64
        0xa8e -> :sswitch_65
        0xa8f -> :sswitch_66
        0xa90 -> :sswitch_67
        0xa91 -> :sswitch_68
        0xa92 -> :sswitch_69
        0xaf0 -> :sswitch_6b
    .end sparse-switch
.end method

.method public final onServiceThreadEnd()V
    .locals 0

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsService;->mServiceThread:Lcom/google/android/apps/plus/service/ServiceThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/service/ServiceThread;->put(Landroid/content/Intent;)V

    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public final processIntent(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const/4 v8, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v0, "op"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    const-string v0, "rid"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const-string v0, "acc"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/content/EsAccount;

    move-object v0, p0

    move-object v3, p1

    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/service/EsService;->processIntent1(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;II)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v1, v2, p1, v4}, Lcom/google/android/apps/plus/service/EsService;->processIntent2$751513a6(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Unsupported op code: "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceResult;

    const/4 v3, 0x0

    invoke-direct {v0, v3, v8, v6}, Lcom/google/android/apps/plus/service/ServiceResult;-><init>(ILjava/lang/String;Ljava/lang/Exception;)V

    invoke-direct {p0, p1, v0, v8}, Lcom/google/android/apps/plus/service/EsService;->completeRequest(Landroid/content/Intent;Lcom/google/android/apps/plus/service/ServiceResult;Ljava/lang/Object;)V

    goto :goto_0
.end method
