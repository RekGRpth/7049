.class final Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "EsSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "SyncAdapterImpl"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    return-object v0
.end method

.method private isSubscribed(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x5

    new-array v4, v1, [Ljava/lang/String;

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    aput-object v1, v4, v7

    aput-object p2, v4, v3

    const/4 v1, 0x2

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x3

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v1, 0x4

    aput-object p3, v4, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v7

    const-string v3, "authority = ? AND feed = ? AND _sync_account = ? AND _sync_account_type = ? AND service = ?"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-nez v6, :cond_0

    move v1, v7

    :goto_0
    return v1

    :cond_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private subscribe(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/accounts/Account;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v0, "EsSyncAdapterService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsSyncAdapterService"

    const-string v1, "  --> Subscribe all feeds"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "feed"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_sync_account"

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "_sync_account_type"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "authority"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "service"

    invoke-virtual {v2, v1, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void
.end method

.method private updateSubscribedFeeds(Landroid/accounts/Account;)V
    .locals 11
    .param p1    # Landroid/accounts/Account;

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v8, 0x3

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {p1, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v2

    :goto_0
    if-eqz v0, :cond_5

    const/4 v1, 0x1

    const-string v4, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    const-string v5, "webupdates"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->isSubscribed(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    const-string v5, "webupdates"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->subscribe(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_0
    const-string v4, "com.google.plus.events"

    const-string v5, "events"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->isSubscribed(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "com.google.plus.events"

    const-string v5, "events"

    invoke-direct {p0, p1, v4, v5}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->subscribe(Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :cond_1
    if-eqz v1, :cond_3

    const-string v4, "EsSyncAdapterService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "EsSyncAdapterService"

    const-string v5, "  --> Already subscribed"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-array v4, v8, [Ljava/lang/String;

    const-string v5, "com.google.plus.notifications"

    aput-object v5, v4, v3

    const-string v3, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    aput-object v3, v4, v2

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v2, v4, v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "authority = ? AND feed = ? AND _sync_account_type = ?"

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    :cond_3
    :goto_1
    return-void

    :cond_4
    move v0, v3

    goto :goto_0

    :cond_5
    const-string v4, "EsSyncAdapterService"

    invoke-static {v4, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "EsSyncAdapterService"

    const-string v5, "  --> Unsubscribe all feeds"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "com.google.android.apps.plus.content.EsProvider"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "_sync_account=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " AND _sync_account_type=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " AND authority=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v7, Lcom/google/android/apps/plus/service/SubscribedFeeds$Feeds;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v8, v8, [Ljava/lang/String;

    iget-object v9, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v9, v8, v3

    iget-object v3, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    aput-object v3, v8, v2

    aput-object v5, v8, v10

    invoke-virtual {v4, v7, v6, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 19
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/ContentProviderClient;
    .param p5    # Landroid/content/SyncResult;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v4

    :goto_0
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz p2, :cond_2

    const-string v10, "initialize"

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    if-eqz v10, :cond_2

    const/4 v8, 0x1

    :goto_1
    if-eqz v8, :cond_4

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v11

    const-string v12, "com.google.android.apps.plus.content.EsProvider"

    if-eqz v7, :cond_3

    const/4 v10, 0x1

    :goto_2
    invoke-static {v11, v12, v10}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->updateSubscribedFeeds(Landroid/accounts/Account;)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v13}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v10

    const-string v11, "com.google"

    invoke-virtual {v10, v11}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v14

    if-eqz v14, :cond_b

    array-length v15, v14

    const/4 v10, 0x0

    move v12, v10

    :goto_4
    if-ge v12, v15, :cond_b

    aget-object v16, v14, v12

    move-object/from16 v0, v16

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v10, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    const-string v10, "com.google.android.apps.plus.content.EsProvider"

    move-object/from16 v0, v16

    invoke-static {v0, v10}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_7

    const/4 v10, 0x1

    :goto_5
    const-string v11, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    move-object/from16 v0, v16

    invoke-static {v0, v11}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v11

    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v11, v0, :cond_8

    const/4 v11, 0x1

    :goto_6
    if-eqz v17, :cond_9

    if-nez v11, :cond_6

    const-string v10, "EsSyncAdapterService"

    const/4 v11, 0x5

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_5

    const-string v10, "EsSyncAdapterService"

    const-string v11, "Activating IU sync adapter"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move-object/from16 v0, v16

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v11, 0x1

    invoke-static {v13, v10, v11}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->activateAccount(Landroid/content/Context;Ljava/lang/String;Z)V

    :cond_6
    :goto_7
    add-int/lit8 v10, v12, 0x1

    move v12, v10

    goto :goto_4

    :cond_7
    const/4 v10, 0x0

    goto :goto_5

    :cond_8
    const/4 v11, 0x0

    goto :goto_6

    :cond_9
    if-eqz v10, :cond_6

    if-eqz v11, :cond_6

    const-string v10, "EsSyncAdapterService"

    const/4 v11, 0x5

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_a

    const-string v10, "EsSyncAdapterService"

    const-string v11, "Deactivating IU sync adapter"

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move-object/from16 v0, v16

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v13, v10}, Lcom/google/android/apps/plus/iu/InstantUploadSyncService;->deactivateAccount(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_7

    :cond_b
    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->updateSubscribedFeeds(Landroid/accounts/Account;)V

    if-nez v7, :cond_c

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccountUnsafe(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->isAccountUpgradeRequired(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v10

    if-eqz v10, :cond_0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->upgradeAccount(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    invoke-static {v10}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    if-eqz v3, :cond_10

    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    const/4 v7, 0x1

    :goto_8
    if-eqz v7, :cond_0

    :cond_c
    if-eqz p2, :cond_e

    const-string v10, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_e

    const-string v10, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v10, "EsSyncAdapterService"

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_d

    const-string v10, "EsSyncAdapterService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "  --> Sync specific feed: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    const-string v10, "sync_from_tickle"

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v10, "https://m.google.com/app/feed/notifications?authority=com.google.plus.notifications"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    const-string v10, "sync_what"

    const/4 v11, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    new-instance v11, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-direct {v11, v12}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_NOTIFICATION_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v13, 0x0

    invoke-static {v10, v3, v11, v12, v13}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    :cond_e
    :goto_9
    move-object/from16 v0, p1

    iget-object v10, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->getAccountSyncState(Ljava/lang/String;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v10

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    :try_start_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    # getter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v11

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-static {v10, v3, v0, v11, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$100(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/os/Bundle;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/SyncResult;)V

    # getter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->isCanceled()Z

    move-result v10

    if-nez v10, :cond_f

    move-object v9, v3

    new-instance v10, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v10, v0, v9, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl$1;-><init>(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;Lcom/google/android/apps/plus/content/EsAccount;Landroid/os/Bundle;)V

    invoke-static {v10}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_f
    const/4 v10, 0x0

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    goto/16 :goto_3

    :catch_0
    move-exception v5

    const-string v10, "EsSyncAdapterService"

    const-string v11, "Failed to upgrade account"

    invoke-static {v10, v11, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_8

    :cond_11
    const-string v10, "com.google.plus.events"

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    const-string v10, "sync_what"

    const/4 v11, 0x2

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->context:Landroid/content/Context;

    new-instance v11, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzViews;->NOTIFICATIONS_SYSTEM:Lcom/google/android/apps/plus/analytics/OzViews;

    invoke-direct {v11, v12}, Lcom/google/android/apps/plus/analytics/AnalyticsInfo;-><init>(Lcom/google/android/apps/plus/analytics/OzViews;)V

    sget-object v12, Lcom/google/android/apps/plus/analytics/OzActions;->TICKLE_EVENT_RECEIVED:Lcom/google/android/apps/plus/analytics/OzActions;

    const/4 v13, 0x0

    invoke-static {v10, v3, v11, v12, v13}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->postRecordEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/AnalyticsInfo;Lcom/google/android/apps/plus/analytics/OzActions;Landroid/os/Bundle;)V

    goto :goto_9

    :cond_12
    const-string v10, "EsSyncAdapterService"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Unexpected feed: "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    :catchall_0
    move-exception v10

    const/4 v11, 0x0

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {v11}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    throw v10
.end method

.method public final onSyncCanceled()V
    .locals 1

    # getter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v0

    if-eqz v0, :cond_0

    # getter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    invoke-static {}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    :cond_0
    return-void
.end method
