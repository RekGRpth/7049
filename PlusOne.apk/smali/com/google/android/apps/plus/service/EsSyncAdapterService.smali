.class public Lcom/google/android/apps/plus/service/EsSyncAdapterService;
.super Landroid/app/Service;
.source "EsSyncAdapterService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;,
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;,
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;,
        Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncOperationState;
    }
.end annotation


# static fields
.field private static sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

.field private static sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

.field private static final sSyncAdapterLock:Ljava/lang/Object;

.field private static sSyncStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    sput-object p0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sCurrentSyncState:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    return-object p0
.end method

.method static synthetic access$100(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/os/Bundle;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Landroid/content/SyncResult;)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .param p4    # Landroid/content/SyncResult;

    const/4 v2, 0x1

    const/4 v8, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, p2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->requestAccountSync(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_0
    :goto_0
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->pollAccountSyncRequest()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_11

    new-instance v3, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;

    invoke-direct {v3, p4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncListener;-><init>(Landroid/content/SyncResult;)V

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    const-string v4, "G+ sync"

    invoke-virtual {p3, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v4, "sync_from_tickle"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v0, :cond_3

    :try_start_0
    const-string v5, "sync_what"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    if-ne v5, v2, :cond_3

    const-string v0, "EsSyncAdapterService"

    const/4 v5, 0x3

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EsSyncAdapterService"

    const-string v5, "onPerformSync: ====> Notifications Gsync begin"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-static {p0, p1, p3, v3, v4}, Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    :goto_1
    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    :goto_2
    invoke-virtual {p4}, Landroid/content/SyncResult;->hasError()Z

    move-result v3

    if-nez v3, :cond_10

    const-string v3, "EsSyncAdapterService"

    invoke-static {v3, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "EsSyncAdapterService"

    const-string v4, "onPerformSync: ====> Sync end with no errors."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->saveLastSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_5

    :try_start_1
    const-string v5, "sync_what"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_5

    const-string v0, "EsSyncAdapterService"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "EsSyncAdapterService"

    const-string v3, "onPerformSync: ====> Events Gsync begin"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/content/EsEventData;->syncCurrentEvents$1ef5a3b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    move v0, v1

    goto :goto_1

    :cond_5
    const-string v0, "EsSyncAdapterService"

    const/4 v5, 0x3

    invoke-static {v0, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "EsSyncAdapterService"

    const-string v5, "onPerformSync: ====> Full sync begin"

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryNotificationPushEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/apps/plus/api/RegisterDeviceOperation;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v0, p0, p1, v5, v6}, Lcom/google/android/apps/plus/api/RegisterDeviceOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/RegisterDeviceOperation;->start()V

    :cond_7
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->syncExperiments(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, p1, p3, v3}, Lcom/google/android/apps/plus/content/EsPostsData;->syncActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-static {p0, p1, p3, v3, v4}, Lcom/google/android/apps/plus/content/EsNotificationData;->syncNotifications(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    const/4 v0, 0x0

    invoke-static {p0, p1, p3, v3, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncMyProfile(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    const/4 v0, 0x1

    invoke-static {p0, p1, p3, v3, v0}, Lcom/google/android/apps/plus/content/EsPeopleData;->syncPeople(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->uploadChangedSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->syncSettings$41b2440d(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    const/4 v0, 0x0

    invoke-static {p0, p1, p3, v3, v0}, Lcom/google/android/apps/plus/content/EsEmotiShareData;->syncAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;Z)Z

    invoke-static {p0}, Lcom/google/android/apps/plus/service/AndroidContactsSync;->requestSync(Landroid/content/Context;)V

    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/content/EsPhotosData;->syncPhotos(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->isContactsStatsSyncEnabled(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAccountsData;->queryLastStatsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/32 v6, 0x5265c00

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    :cond_8
    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/service/ContactsStatsSync;->sync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    :cond_9
    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->removeAll(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/util/List;

    move-result-object v0

    const-string v4, "EsSyncAdapterService"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "EsSyncAdapterService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Sending "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " analytics events"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    if-eqz v0, :cond_c

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_c

    const-string v4, "SendAnalyticsData"

    invoke-virtual {p3, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/apps/plus/api/PostClientLogsOperation;

    const/4 v5, 0x0

    invoke-direct {v4, p0, p1, v5, v3}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Intent;Lcom/google/android/apps/plus/network/HttpOperation$OperationListener;)V

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->setClientOzEvents(Ljava/util/List;)V

    new-instance v3, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct {v3}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    invoke-virtual {v4, p3, v3}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->start(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getErrorCode()I

    move-result v3

    invoke-virtual {v4}, Lcom/google/android/apps/plus/api/PostClientLogsOperation;->getException()Ljava/lang/Exception;

    move-result-object v4

    const/16 v5, 0xc8

    if-ne v3, v5, :cond_b

    if-eqz v4, :cond_d

    :cond_b
    const-string v5, "EsSyncAdapterService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PostClientLogsOperation failed ex "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " errorCode "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1, v0}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->bulkInsert(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/util/List;)V

    :cond_c
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-static {p0, p1, v3, v4}, Lcom/google/android/apps/plus/content/EsAnalyticsData;->saveLastAnalyticsSyncTimestamp(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;J)V

    invoke-static {p0, p1, p3}, Lcom/google/android/apps/plus/content/EsEventData;->syncEventThemes$1ef5a3b9(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/content/EsProvider;->analyzeDatabase(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)V

    move v0, v2

    goto/16 :goto_1

    :cond_d
    const-string v0, "EsSyncAdapterService"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "EsSyncAdapterService"

    const-string v3, "PostClientLogsOperation was successful"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    :catch_0
    move-exception v0

    :try_start_2
    const-string v3, "EsSyncAdapterService"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_e

    const-string v3, "EsSyncAdapterService"

    const-string v4, "Sync failure"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_e
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v0, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    move v0, v1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    :try_start_3
    const-string v3, "EsSyncAdapterService"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_f

    const-string v3, "EsSyncAdapterService"

    const-string v4, "Sync failure"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_f
    iget-object v0, p4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v0, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v0, Landroid/content/SyncStats;->numParseExceptions:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    move v0, v1

    goto/16 :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {p3}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    # setter for: Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->mFullSync:Z
    invoke-static {p3, v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->access$302(Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;Z)Z

    throw v0

    :cond_10
    const-string v0, "EsSyncAdapterService"

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsSyncAdapterService"

    const-string v3, "onPerformSync: ====> Sync end with error(s)"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_11
    return-void
.end method

.method public static activateAccount(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {p1}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    const-string v1, "com.google.android.apps.plus.content.EsProvider"

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->resetSyncStates(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public static deactivateAccount(Ljava/lang/String;)V
    .locals 4
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    const-string v2, "com.google.android.apps.plus.content.EsProvider"

    invoke-static {v0, v2}, Landroid/content/ContentResolver;->cancelSync(Landroid/accounts/Account;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->cancel()V

    :cond_0
    return-void
.end method

.method public static getAccountSyncState(Ljava/lang/String;)Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;
    .locals 3
    .param p0    # Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncStates:Ljava/util/HashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private static resetSyncStates(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    invoke-static {p0}, Lcom/google/android/apps/plus/util/AccountsUtil;->getAccounts(Landroid/content/Context;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->deactivateAccount(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    sget-object v1, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/service/EsSyncAdapterService;->sSyncAdapter:Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncAdapterImpl;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
