.class public Lcom/google/android/apps/plus/service/Hangout$Info;
.super Ljava/lang/Object;
.source "Hangout.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/Hangout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Info"
.end annotation


# instance fields
.field private final domain:Ljava/lang/String;

.field private final id:Ljava/lang/String;

.field private final launchSource:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

.field private final nick:Ljava/lang/String;

.field private ringInvitees:Z

.field private final roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

.field private final serviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Hangout$RoomType;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/service/Hangout$LaunchSource;
    .param p7    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->ringInvitees:Z

    iput-object p1, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->serviceId:Ljava/lang/String;

    invoke-virtual {p4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->nick:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->launchSource:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    iput-boolean p7, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->ringInvitees:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/Hangout$Info;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    instance-of v5, p1, Lcom/google/android/apps/plus/service/Hangout$Info;

    if-nez v5, :cond_2

    move v4, v3

    goto :goto_0

    :cond_2
    move-object v2, p1

    check-cast v2, Lcom/google/android/apps/plus/service/Hangout$Info;

    iget-object v5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_3
    move v0, v4

    :goto_1
    iget-object v5, v2, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, v2, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    :cond_4
    move v1, v4

    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    iget-object v6, v2, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    if-ne v5, v6, :cond_7

    if-eqz v0, :cond_5

    if-nez v1, :cond_6

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    :cond_6
    iget-object v5, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    iget-object v6, v2, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    :cond_7
    move v4, v3

    goto :goto_0

    :cond_8
    move v0, v3

    goto :goto_1

    :cond_9
    move v1, v3

    goto :goto_2
.end method

.method public final getDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    return-object v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final getLaunchSource()Lcom/google/android/apps/plus/service/Hangout$LaunchSource;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->launchSource:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    return-object v0
.end method

.method public final getNick()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->nick:Ljava/lang/String;

    return-object v0
.end method

.method public final getRingInvitees()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->ringInvitees:Z

    return v0
.end method

.method public final getRoomType()Lcom/google/android/apps/plus/service/Hangout$RoomType;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    return-object v0
.end method

.method public final getServiceId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->serviceId:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Hangout$RoomType;->hashCode()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int v0, v1, v2

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->serviceId:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "%s@%s %s (%s, %s, %s)"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->nick:Ljava/lang/String;

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->launchSource:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    aput-object v2, v1, v7

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->ringInvitees:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%s:%s@%s %s (%s, %s, %s)"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->serviceId:Ljava/lang/String;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->domain:Ljava/lang/String;

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->roomType:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    aput-object v2, v1, v6

    iget-object v2, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->nick:Ljava/lang/String;

    aput-object v2, v1, v7

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->launchSource:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/apps/plus/service/Hangout$Info;->ringInvitees:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
