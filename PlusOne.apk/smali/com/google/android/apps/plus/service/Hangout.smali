.class public Lcom/google/android/apps/plus/service/Hangout;
.super Ljava/lang/Object;
.source "Hangout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/Hangout$1;,
        Lcom/google/android/apps/plus/service/Hangout$SupportStatus;,
        Lcom/google/android/apps/plus/service/Hangout$RoomType;,
        Lcom/google/android/apps/plus/service/Hangout$ApplicationEventListener;,
        Lcom/google/android/apps/plus/service/Hangout$Info;,
        Lcom/google/android/apps/plus/service/Hangout$LaunchSource;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z

.field public static final CONSUMER_HANGOUT_DOMAIN:Ljava/lang/String;

.field private static final HANGOUT_URL_PATTERN:Ljava/util/regex/Pattern;

.field private static sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

.field private static sCachedIsCreationSupported:Z

.field private static sCachedStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

.field private static sHangoutCreationSupportCacheIsDirty:Z

.field private static sHangoutSupportStatusCacheIsDirty:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    const-class v0, Lcom/google/android/apps/plus/service/Hangout;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->$assertionsDisabled:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/plus/service/Hangout;->CONSUMER_HANGOUT_DOMAIN:Ljava/lang/String;

    const-string v0, "http s? ://plus.google.com/hangouts/(    \\p{Alnum}+)"

    const/4 v2, 0x6

    invoke-static {v0, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/service/Hangout;->HANGOUT_URL_PATTERN:Ljava/util/regex/Pattern;

    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enterGreenRoom(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbEmbedHangout;)V
    .locals 23
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/content/DbEmbedHangout;

    sget-boolean v2, Lcom/google/android/apps/plus/service/Hangout;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->isInProgress()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "g:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/service/Hangout;->getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeGaiaIds()Ljava/util/ArrayList;

    move-result-object v13

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getAttendeeNames()Ljava/util/ArrayList;

    move-result-object v14

    const/16 v18, 0x0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getNumAttendees()I

    move-result v22

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v2, "g:"

    invoke-direct {v4, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFullName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-static {v15}, Lcom/google/android/apps/plus/service/Hangout;->getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->setFirstName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/google/android/apps/plus/service/Hangout$RoomType;->CONSUMER:Lcom/google/android/apps/plus/service/Hangout$RoomType;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getHangoutId()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/apps/plus/service/Hangout$LaunchSource;->Stream:Lcom/google/android/apps/plus/service/Hangout$LaunchSource;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p0

    invoke-static/range {v2 .. v12}, Lcom/google/android/apps/plus/phone/Intents;->getHangoutActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/service/Hangout$RoomType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/service/Hangout$LaunchSource;ZZLjava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v19

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/hangout/GCommApp;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/hangout/GCommApp;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/hangout/GCommApp;->isInAHangout()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/hangout/GCommApp;->getGCommService()Lcom/google/android/apps/plus/hangout/GCommService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/hangout/GCommService;->getNotificationIntent()Landroid/content/Intent;

    move-result-object v20

    if-eqz v20, :cond_2

    const-string v2, "hangout_info"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v17

    check-cast v17, Lcom/google/android/apps/plus/service/Hangout$Info;

    if-eqz v17, :cond_2

    # getter for: Lcom/google/android/apps/plus/service/Hangout$Info;->id:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/google/android/apps/plus/service/Hangout$Info;->access$000(Lcom/google/android/apps/plus/service/Hangout$Info;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/plus/content/DbEmbedHangout;->getHangoutId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v19, v20

    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static getFirstNameFromFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/lang/String;

    const/16 v1, 0x20

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/Hangout;->updateCacheDirtyFlags(Lcom/google/android/apps/plus/content/EsAccount;)V

    sget-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-ge v0, v2, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->OS_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    :goto_0
    sput-object v0, Lcom/google/android/apps/plus/service/Hangout;->sCachedStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/Hangout;->sCachedStatus:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    return-object v0

    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v3

    if-eqz v3, :cond_4

    array-length v0, v3

    if-lez v0, :cond_4

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    iget-object v6, v5, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-nez v6, :cond_2

    iget v0, v5, Landroid/content/pm/FeatureInfo;->reqGlEsVersion:I

    shr-int/lit8 v0, v0, 0x10

    int-to-short v0, v0

    const/4 v5, 0x2

    if-lt v0, v5, :cond_3

    const/4 v0, 0x1

    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    :cond_5
    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->DEVICE_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_0

    :cond_6
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    const-string v2, "armeabi-v7a"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v2, "armeabi-v7a"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->DEVICE_NOT_SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_0

    :cond_7
    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/EsAccount;->isPlusPage()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    sget-object v0, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->ACCOUNT_NOT_CONFIGURED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_0

    :cond_9
    sget-object v0, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    goto :goto_0
.end method

.method public static isAdvancedUiSupported(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_ADVANCED_HANGOUTS:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Landroid/view/TextureView;

    invoke-direct {v2, p0}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/view/TextureView;->getLayerType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "samsung"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Landroid/os/Build;->BRAND:Ljava/lang/String;

    const-string v3, "google"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xf

    if-gt v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public static isHangoutCreationSupported(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Z
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/Hangout;->updateCacheDirtyFlags(Lcom/google/android/apps/plus/content/EsAccount;)V

    sget-boolean v2, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    if-eqz v2, :cond_1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/service/Hangout;->getSupportedStatus(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/service/Hangout$SupportStatus;->SUPPORTED:Lcom/google/android/apps/plus/service/Hangout$SupportStatus;

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v3

    if-eqz v3, :cond_3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    const-string v6, "android.hardware.camera.front"

    iget-object v7, v5, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    :goto_1
    sput-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->sCachedIsCreationSupported:Z

    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    :cond_1
    sget-boolean v0, Lcom/google/android/apps/plus/service/Hangout;->sCachedIsCreationSupported:Z

    return v0

    :cond_2
    const-string v6, "android.hardware.camera"

    iget-object v5, v5, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private static updateCacheDirtyFlags(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v1, 0x1

    sget-object v0, Lcom/google/android/apps/plus/service/Hangout;->sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/service/Hangout;->sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutCreationSupportCacheIsDirty:Z

    sput-boolean v1, Lcom/google/android/apps/plus/service/Hangout;->sHangoutSupportStatusCacheIsDirty:Z

    sput-object p0, Lcom/google/android/apps/plus/service/Hangout;->sAccountForCachedStatus:Lcom/google/android/apps/plus/content/EsAccount;

    :cond_1
    return-void
.end method
