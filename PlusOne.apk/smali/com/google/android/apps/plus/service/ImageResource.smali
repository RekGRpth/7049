.class public abstract Lcom/google/android/apps/plus/service/ImageResource;
.super Lcom/google/android/apps/plus/service/Resource;
.source "ImageResource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;,
        Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;
    }
.end annotation


# instance fields
.field protected volatile mResourceType:I


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/ImageResourceManager;
    .param p2    # Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/service/Resource;-><init>(Lcom/google/android/apps/plus/service/ResourceManager;Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;)V

    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResourceType:I

    return-void
.end method

.method private static isBitmapPackingEnabled()Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected decodeBitmap([B)Landroid/graphics/Bitmap;
    .locals 2
    .param p1    # [B

    const/4 v0, 0x0

    array-length v1, p1

    invoke-static {p1, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final deliverData([BZ)V
    .locals 8
    .param p1    # [B
    .param p2    # Z

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v1, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    iget v4, v1, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_3

    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Saving image in file cache: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->getShortFileName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/plus/content/EsMediaCache;->write(Landroid/content/Context;Ljava/lang/String;[B)V

    :cond_1
    iget v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_5

    iget v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Request no longer needed, not delivering: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->getStatusAsString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget v4, v1, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_4

    const/4 v3, 0x0

    goto :goto_0

    :cond_4
    move v3, p2

    goto :goto_0

    :cond_5
    iget v4, v1, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Image decoding disabled. Delivering null to consumers: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v4, p0, v6, v7}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V

    goto :goto_1

    :cond_7
    iget v4, v1, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Image decoding disabled. Delivering bytes to consumers: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v4, p0, v6, p1}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V

    goto :goto_1

    :cond_9
    :try_start_0
    invoke-static {p1}, Lcom/google/android/apps/plus/util/GifImage;->isGif([B)Z

    move-result v2

    if-eqz v2, :cond_a

    const/4 v4, 0x1

    iput v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResourceType:I

    :cond_a
    iget v4, v1, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v4, v4, 0x4

    if-eqz v4, :cond_c

    if-eqz v2, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v5, 0x1

    new-instance v6, Lcom/google/android/apps/plus/util/GifImage;

    invoke-direct {v6, p1}, Lcom/google/android/apps/plus/util/GifImage;-><init>([B)V

    invoke-virtual {v4, p0, v5, v6}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_b

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Out of memory while decoding image: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_b
    new-instance v4, Lcom/google/android/apps/plus/service/ImageResource$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/service/ImageResource$1;-><init>(Lcom/google/android/apps/plus/service/ImageResource;)V

    invoke-static {v4}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v5, 0x7

    invoke-virtual {v4, p0, v5, v7}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_c
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/service/ImageResource;->decodeBitmap([B)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_d

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Delivering image to consumers: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_d
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v5, 0x1

    invoke-virtual {v4, p0, v5, v0}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v4

    if-eqz v4, :cond_f

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bad image; cannot decode: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_f
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v5, 0x6

    invoke-virtual {v4, p0, v5}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceStatus(Lcom/google/android/apps/plus/service/Resource;I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method protected abstract downloadResource()V
.end method

.method public final getCacheFileName()Ljava/io/File;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    iget v0, v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->getShortFileName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/content/EsMediaCache;->getMediaCacheFile(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResourceType()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResourceType:I

    return v0
.end method

.method public abstract getShortFileName()Ljava/lang/String;
.end method

.method public final getSizeInBytes()I
    .locals 5

    const v3, 0x7fffffff

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v4, v4, Landroid/graphics/Bitmap;

    if-eqz v4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v4, v4, Lcom/google/android/apps/plus/util/GifImage;

    if-eqz v4, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/plus/util/GifImage;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/GifImage;->getSizeEstimate()I

    move-result v3

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v4, v4, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;

    if-eqz v4, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;

    iget v3, v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->sizeInBytes:I

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v4, v4, [B

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v3, [B

    array-length v3, v3

    goto :goto_0
.end method

.method public final isCacheEnabled()Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    if-eq v0, v2, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    iget v0, v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v0, v0, 0xa

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/android/apps/plus/service/ImageResource;->isBitmapPackingEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public load()V
    .locals 6

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v2, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->getShortFileName()Ljava/lang/String;

    move-result-object v0

    iget v3, v2, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsMediaCache;->exists(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v3, p0, v4, v5}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iget v3, v2, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/apps/plus/content/EsMediaCache;->read(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    :cond_1
    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loaded image from file: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_2
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/google/android/apps/plus/service/ImageResource;->deliverData([BZ)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->isDebugLogEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Requesting download: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/service/ImageResource;->logDebug(Ljava/lang/String;)V

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v4, 0x3

    invoke-virtual {v3, p0, v4}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceStatus(Lcom/google/android/apps/plus/service/Resource;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/ImageResource;->downloadResource()V

    goto :goto_0
.end method

.method public final pack()V
    .locals 6

    iget v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v4, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    iget v4, v4, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v4, v4, 0x10

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v4, v4, Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/service/ImageResource;->isBitmapPackingEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    new-instance v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;-><init>(B)V

    iput-object v1, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->config:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iput v4, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->width:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iput v4, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->height:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    mul-int/2addr v4, v5

    iput v4, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->sizeInBytes:I

    iget v4, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->sizeInBytes:I

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->buffer:Ljava/nio/ByteBuffer;

    iget-object v4, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    const/16 v4, 0x9

    iput v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    const/4 v4, 0x7

    iput v4, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    goto :goto_0
.end method

.method public final recycle()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;

    iget v0, v0, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;->mFlags:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/service/Resource;->recycle()V

    return-void
.end method

.method public final unpack()V
    .locals 6

    const/4 v5, 0x1

    iget v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    instance-of v3, v3, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/google/android/apps/plus/service/ImageResource;->isBitmapPackingEnabled()Z

    move-result v3

    if-nez v3, :cond_2

    iput v5, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;

    :try_start_0
    iget v3, v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->width:I

    iget v4, v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->height:I

    iget-object v5, v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->config:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v3, v2, Lcom/google/android/apps/plus/service/ImageResource$PackedBitmap;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    const/4 v3, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mResource:Ljava/lang/Object;

    const/4 v3, 0x7

    iput v3, p0, Lcom/google/android/apps/plus/service/ImageResource;->mStatus:I

    goto :goto_0
.end method
