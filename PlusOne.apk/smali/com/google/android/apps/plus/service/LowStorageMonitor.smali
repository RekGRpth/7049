.class public Lcom/google/android/apps/plus/service/LowStorageMonitor;
.super Landroid/content/BroadcastReceiver;
.source "LowStorageMonitor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/LowStorageMonitor$CleanupTask;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/service/LowStorageMonitor$CleanupTask;

    invoke-direct {v1, v3}, Lcom/google/android/apps/plus/service/LowStorageMonitor$CleanupTask;-><init>(B)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/content/Context;

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/LowStorageMonitor$CleanupTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
