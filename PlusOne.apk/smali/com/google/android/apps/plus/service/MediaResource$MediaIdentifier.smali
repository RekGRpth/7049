.class public final Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;
.super Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;
.source "MediaResource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/service/MediaResource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaIdentifier"
.end annotation


# instance fields
.field private mHashCode:I

.field mHeight:I

.field mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mNextInPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

.field mSizeCategory:I

.field mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;-><init>()V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne p1, p0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    instance-of v3, p1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public final getNextInPool()Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mNextInPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHashCode:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHashCode:I

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHashCode:I

    return v0
.end method

.method public final init(ILcom/google/android/apps/plus/api/MediaRef;III)V
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->init(I)V

    iput-object p2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput p3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    iput p4, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    iput p5, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHashCode:I

    return-void
.end method

.method public final setNextInPool(Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mNextInPool:Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    const-string v0, ""

    iget v2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " no-disk-cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " accept-animation"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget v2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x8

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " disable-decoding"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget v2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " disable-recycling"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget v2, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x20

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " disable-webp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "{"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_1
    const-string v1, "thumbnail"

    goto/16 :goto_0

    :pswitch_2
    const-string v1, "large"

    goto/16 :goto_0

    :pswitch_3
    const-string v1, "portrait"

    goto/16 :goto_0

    :pswitch_4
    const-string v1, "landscape"

    goto/16 :goto_0

    :pswitch_5
    const-string v1, "full"

    goto/16 :goto_0

    :pswitch_6
    const-string v1, "original"

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method
