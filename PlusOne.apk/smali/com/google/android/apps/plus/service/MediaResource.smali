.class public final Lcom/google/android/apps/plus/service/MediaResource;
.super Lcom/google/android/apps/plus/service/ImageResource;
.source "MediaResource.java"

# interfaces
.implements Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;
    }
.end annotation


# static fields
.field private static sInitialized:Z

.field private static sLandscapeHeight:I

.field private static sLandscapeWidth:I

.field private static sPortraitHeight:I

.field private static sPortraitWidth:I


# instance fields
.field private mBaseUrl:Ljava/lang/String;

.field private mContentUri:Landroid/net/Uri;

.field private mContentUriInitialized:Z

.field private mDownloadUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/service/ImageResourceManager;
    .param p2    # Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/service/ImageResource;-><init>(Lcom/google/android/apps/plus/service/ImageResourceManager;Lcom/google/android/apps/plus/service/ImageResource$ImageResourceIdentifier;)V

    sget-boolean v2, Lcom/google/android/apps/plus/service/MediaResource;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v2

    iget v1, v2, Lcom/google/android/apps/plus/phone/ScreenMetrics;->longDimension:I

    div-int/lit8 v2, v1, 0x2

    sput v2, Lcom/google/android/apps/plus/service/MediaResource;->sPortraitHeight:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->media_max_portrait_aspect_ratio:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    div-int/2addr v2, v3

    sput v2, Lcom/google/android/apps/plus/service/MediaResource;->sPortraitWidth:I

    div-int/lit8 v2, v1, 0x2

    sput v2, Lcom/google/android/apps/plus/service/MediaResource;->sLandscapeWidth:I

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->media_min_landscape_aspect_ratio:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    div-int/2addr v2, v3

    sput v2, Lcom/google/android/apps/plus/service/MediaResource;->sLandscapeHeight:I

    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/service/MediaResource;->sInitialized:Z

    :cond_0
    return-void
.end method

.method private getBaseUrl()Ljava/lang/String;
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    :goto_0
    return-object v2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget-object v1, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    if-nez v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    const-string v3, "//"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget v2, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getStaticUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mBaseUrl:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final downloadResource()V
    .locals 12

    const/4 v11, 0x4

    const/4 v1, 0x3

    const/4 v10, 0x2

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget-object v0, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    const-string v4, "http"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    :goto_0
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ResourceManager;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget-object v3, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/MediaResource;->isDebugLogEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loading local resource "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/service/MediaResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v6}, Lcom/google/android/apps/plus/util/ImageUtils;->getMimeType(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/ImageUtils;->isVideoMimeType(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v6}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->isMediaStoreUri(Landroid/net/Uri;)Z

    move-result v8

    if-nez v7, :cond_1

    invoke-static {v5, p0, v6}, Lcom/google/android/apps/plus/content/MediaTypeDetector;->detect(Landroid/content/Context;Lcom/google/android/apps/plus/content/MediaTypeDetectionListener;Landroid/net/Uri;)V

    :cond_1
    :try_start_0
    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    iget v3, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    iget v9, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    if-ne v9, v10, :cond_4

    sget v3, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    move v4, v3

    :goto_1
    :pswitch_0
    if-eqz v8, :cond_7

    iget v1, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    if-eq v1, v10, :cond_2

    if-eqz v7, :cond_5

    :cond_2
    invoke-static {v5, v6, v4, v3}, Lcom/google/android/apps/plus/util/MediaStoreUtils;->getThumbnail(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_c

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/MediaResource;->deliverResource(Ljava/lang/Object;)V

    :goto_3
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    iget v9, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    packed-switch v9, :pswitch_data_0

    :pswitch_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    :catch_0
    move-exception v0

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/MediaResource;->deliverDownloadError(I)V

    goto :goto_3

    :pswitch_2
    :try_start_1
    sget v3, Lcom/google/android/picasastore/Config;->sScreenNailSize:I

    move v4, v3

    goto :goto_1

    :cond_5
    iget v0, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    if-ne v0, v2, :cond_6

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    :cond_6
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v6, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->createLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    :cond_7
    if-eqz v7, :cond_a

    const/4 v0, 0x3

    invoke-static {v5, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->getMaxThumbnailDimension(Landroid/content/Context;I)I

    move-result v0

    if-gt v4, v0, :cond_8

    if-le v3, v0, :cond_9

    :cond_8
    move v0, v2

    :goto_4
    invoke-static {v5, v6, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->createVideoThumbnail(Landroid/content/Context;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_4

    :cond_a
    iget v0, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    if-ne v0, v2, :cond_b

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v6, v0}, Lcom/google/android/apps/plus/util/ImageUtils;->createLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_2

    :cond_b
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_5

    :cond_c
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/MediaResource;->deliverDownloadError(I)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/service/MediaResource;->deliverDownloadError(I)V

    goto :goto_3

    :catch_2
    move-exception v0

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/MediaResource;->deliverDownloadError(I)V

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    check-cast v0, Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getResourceDownloader()Lcom/google/android/apps/plus/service/ResourceDownloader;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ResourceDownloader;->downloadResource(Lcom/google/android/apps/plus/service/Resource;)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final getContentUri()Landroid/net/Uri;
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mContentUriInitialized:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget-object v0, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mContentUri:Landroid/net/Uri;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mContentUriInitialized:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getDownloadUrl()Ljava/lang/String;
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v5, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    if-eqz v5, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    :goto_0
    return-object v3

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v5, v5, 0x4

    if-nez v5, :cond_1

    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v5, v5, 0x20

    if-nez v5, :cond_1

    move v0, v3

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/MediaResource;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    packed-switch v5, :pswitch_data_0

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1

    :pswitch_0
    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    if-eqz v5, :cond_2

    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    if-nez v5, :cond_3

    :cond_2
    iget v3, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    invoke-static {v3, v5, v2, v4, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getResizedUrl(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :cond_3
    iget v4, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    iget v5, v1, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    invoke-static {v4, v5, v2, v3, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getResizedUrl(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :pswitch_1
    sget v4, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    invoke-static {v2, v4, v3, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getMediaResourceUrl(Ljava/lang/String;IZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :pswitch_2
    sget v3, Lcom/google/android/picasastore/Config;->sScreenNailSize:I

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getMediaResourceUrl(Ljava/lang/String;IZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :pswitch_3
    sget v4, Lcom/google/android/apps/plus/service/MediaResource;->sPortraitWidth:I

    sget v5, Lcom/google/android/apps/plus/service/MediaResource;->sPortraitHeight:I

    invoke-static {v4, v5, v2, v3, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getResizedUrl(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :pswitch_4
    sget v4, Lcom/google/android/apps/plus/service/MediaResource;->sLandscapeWidth:I

    sget v5, Lcom/google/android/apps/plus/service/MediaResource;->sLandscapeHeight:I

    invoke-static {v4, v5, v2, v3, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getResizedUrl(IILjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :pswitch_5
    iput-object v2, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    :pswitch_6
    const/4 v3, -0x1

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/apps/plus/util/ImageUrlUtils;->getMediaResourceUrl(Ljava/lang/String;IZZ)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/plus/service/MediaResource;->mDownloadUrl:Ljava/lang/String;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final getShortFileName()Ljava/lang/String;
    .locals 5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;

    iget-object v1, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->hasUrl()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsMediaCache;->buildShortFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    iget v3, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mSizeCategory:I

    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    iget v3, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_0

    const-string v3, "-a"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget v3, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mFlags:I

    and-int/lit8 v3, v3, 0x20

    if-eqz v3, :cond_1

    const-string v3, "-o"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->hasLocalUri()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsMediaCache;->buildShortFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->hasPhotoId()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/plus/api/MediaRef;->getPhotoId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsMediaCache;->buildShortFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "A media ref should have a URI"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_1
    const/16 v3, 0x2d

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x78

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/google/android/apps/plus/service/MediaResource$MediaIdentifier;->mHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_2
    const-string v3, "-t"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_3
    const-string v3, "-b"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_4
    const-string v3, "-p"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_5
    const-string v3, "-l"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_6
    const-string v3, "-z"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final isGif()Z
    .locals 3

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/MediaResource;->getResourceType()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/service/MediaResource;->getBaseUrl()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    const-string v2, ".gif"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    goto :goto_0
.end method

.method public final onMediaTypeDetected(I)V
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/MediaResource;->isDebugLogEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Delivering resource type to consumers: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/MediaResource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " resource type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/MediaResource;->logDebug(Ljava/lang/String;)V

    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/MediaResource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceType(Lcom/google/android/apps/plus/service/Resource;I)V

    :cond_1
    return-void
.end method
