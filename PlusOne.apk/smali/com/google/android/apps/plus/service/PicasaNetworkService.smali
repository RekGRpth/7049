.class public Lcom/google/android/apps/plus/service/PicasaNetworkService;
.super Landroid/app/IntentService;
.source "PicasaNetworkService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "PicasaNetworkService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 23
    .param p1    # Landroid/content/Intent;

    const-string v20, "op_name"

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v20, "total_time"

    const-wide/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v17

    const-string v20, "net_duration"

    const-wide/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v13

    const-string v20, "sent_bytes"

    const-wide/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    const-string v20, "received_bytes"

    const-wide/16 v21, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move-wide/from16 v2, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v5

    const-string v20, "transaction_count"

    const/16 v21, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    new-instance v19, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;

    invoke-direct/range {v19 .. v19}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;-><init>()V

    new-instance v11, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;

    invoke-direct {v11}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;-><init>()V

    invoke-virtual {v11, v5, v6}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;->setBytesTransferred(J)V

    new-instance v15, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;

    invoke-direct {v15}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;-><init>()V

    invoke-virtual {v15, v7, v8}, Lorg/apache/http/impl/io/HttpTransportMetricsImpl;->setBytesTransferred(J)V

    new-instance v9, Lorg/apache/http/impl/HttpConnectionMetricsImpl;

    invoke-direct {v9, v11, v15}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;-><init>(Lorg/apache/http/io/HttpTransportMetrics;Lorg/apache/http/io/HttpTransportMetrics;)V

    const/4 v10, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v10, v0, :cond_0

    invoke-virtual {v9}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;->incrementRequestCount()V

    invoke-virtual {v9}, Lorg/apache/http/impl/HttpConnectionMetricsImpl;->incrementResponseCount()V

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, v19

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onBeginTransaction(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->setConnectionMetrics(Lorg/apache/http/HttpConnectionMetrics;)V

    sub-long v20, v17, v13

    move-object/from16 v0, v19

    move-wide/from16 v1, v17

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/network/HttpTransactionMetrics;->onEndTransaction(JJ)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move-object/from16 v3, v21

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/content/EsNetworkData;->insertData(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/network/HttpTransactionMetrics;Ljava/lang/Exception;)V

    return-void
.end method
