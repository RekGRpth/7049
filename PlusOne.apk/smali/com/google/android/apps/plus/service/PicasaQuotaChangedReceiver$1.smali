.class final Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;
.super Ljava/lang/Object;
.source "PicasaQuotaChangedReceiver.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver;

.field final synthetic val$account:Lcom/google/android/apps/plus/content/EsAccount;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$picasaFullSizeDisabled:Z

.field final synthetic val$quotaLimit:I

.field final synthetic val$quotaUsed:I

.field final synthetic val$wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver;Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;IIZLandroid/os/PowerManager$WakeLock;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->this$0:Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$context:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iput p4, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$quotaLimit:I

    iput p5, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$quotaUsed:I

    iput-boolean p6, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$picasaFullSizeDisabled:Z

    iput-object p7, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$wl:Landroid/os/PowerManager$WakeLock;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$account:Lcom/google/android/apps/plus/content/EsAccount;

    iget v2, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$quotaLimit:I

    iget v3, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$quotaUsed:I

    iget-boolean v4, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$picasaFullSizeDisabled:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/InstantUpload;->showOutOfQuotaNotification(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;IIZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/PicasaQuotaChangedReceiver$1;->val$wl:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0
.end method
