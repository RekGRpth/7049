.class public abstract Lcom/google/android/apps/plus/service/Resource;
.super Ljava/lang/Object;
.source "Resource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;,
        Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;
    }
.end annotation


# instance fields
.field private mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

.field private mConsumerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;",
            ">;"
        }
    .end annotation
.end field

.field protected mDebugLogEnabled:Z

.field protected volatile mHttpStatusCode:I

.field protected final mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

.field protected final mManager:Lcom/google/android/apps/plus/service/ResourceManager;

.field protected volatile mResource:Ljava/lang/Object;

.field protected volatile mStatus:I

.field private mTag:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/service/ResourceManager;Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/ResourceManager;
    .param p2    # Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    iput-object p2, p0, Lcom/google/android/apps/plus/service/Resource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    const-string v0, "EsResource"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/service/Resource;->mDebugLogEnabled:Z

    return-void
.end method

.method private static appendConsumer(Ljava/lang/StringBuilder;Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V
    .locals 2
    .param p0    # Ljava/lang/StringBuilder;
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    instance-of v0, p1, Landroid/view/View;

    if-eqz v0, :cond_0

    const-string v0, " context: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz p2, :cond_1

    const-string v0, " tag: "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    return-void
.end method

.method private isRegistered(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)Z
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;
    .param p2    # Ljava/lang/Object;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-ne v4, p1, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    if-nez v4, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    :goto_0
    return v3

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->matches(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static statusToString(I)Ljava/lang/String;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "canceled"

    goto :goto_0

    :pswitch_1
    const-string v0, "downloading"

    goto :goto_0

    :pswitch_2
    const-string v0, "loading"

    goto :goto_0

    :pswitch_3
    const-string v0, "not found"

    goto :goto_0

    :pswitch_4
    const-string v0, "out of memory"

    goto :goto_0

    :pswitch_5
    const-string v0, "packed"

    goto :goto_0

    :pswitch_6
    const-string v0, "permanent error"

    goto :goto_0

    :pswitch_7
    const-string v0, "ready"

    goto :goto_0

    :pswitch_8
    const-string v0, "transient error"

    goto :goto_0

    :pswitch_9
    const-string v0, "undefined"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_7
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_8
        :pswitch_6
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public abstract deliverData([BZ)V
.end method

.method public final deliverDownloadError(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/Resource;->mDebugLogEnabled:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Request no longer needed, not delivering status change: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", current status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    invoke-static {v1}, Lcom/google/android/apps/plus/service/Resource;->statusToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ignored new status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/Resource;->statusToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/Resource;->mDebugLogEnabled:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Delivering error code to consumers: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/Resource;->statusToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceStatus(Lcom/google/android/apps/plus/service/Resource;I)V

    goto :goto_0
.end method

.method public final deliverHttpError$4f708078(I)V
    .locals 2
    .param p1    # I

    const/16 v0, 0x194

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v1, 0x4

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceStatus(Lcom/google/android/apps/plus/service/Resource;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v1, 0x6

    invoke-virtual {v0, p0, v1, p1}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverHttpError(Lcom/google/android/apps/plus/service/Resource;II)V

    goto :goto_0
.end method

.method public final deliverResource(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1, p1}, Lcom/google/android/apps/plus/service/ResourceManager;->deliverResourceContent(Lcom/google/android/apps/plus/service/Resource;ILjava/lang/Object;)V

    return-void
.end method

.method public getCacheFileName()Ljava/io/File;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getContentUri()Landroid/net/Uri;
.end method

.method public abstract getDownloadUrl()Ljava/lang/String;
.end method

.method public final getHttpStatusCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/service/Resource;->mHttpStatusCode:I

    return v0
.end method

.method public final getIdentifier()Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    return-object v0
.end method

.method public final getResource()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mResource:Ljava/lang/Object;

    return-object v0
.end method

.method public final getResourceConsumerCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract getSizeInBytes()I
.end method

.method public final getStatus()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    return v0
.end method

.method public final getStatusAsString()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    invoke-static {v0}, Lcom/google/android/apps/plus/service/Resource;->statusToString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isCacheEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isDebugLogEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/Resource;->mDebugLogEnabled:Z

    return v0
.end method

.method public final isPreloading()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ResourceManager;->isPreloadingResource(Lcom/google/android/apps/plus/service/Resource;)Z

    move-result v0

    return v0
.end method

.method public final isRegistered(Lcom/google/android/apps/plus/service/ResourceConsumer;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/service/Resource;->isRegistered(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public abstract load()V
.end method

.method public final logDebug(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/service/Resource;->mDebugLogEnabled:Z

    if-eqz v0, :cond_0

    const-string v0, "EsResource"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EsResource"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "EsResource"

    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final notifyConsumers()V
    .locals 5

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->ensureMainThread()V

    iget-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    iget-object v3, v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->consumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v4, v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->tag:Ljava/lang/Object;

    invoke-interface {v3, p0, v4}, Lcom/google/android/apps/plus/service/ResourceConsumer;->onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    invoke-interface {v3, p0, v4}, Lcom/google/android/apps/plus/service/ResourceConsumer;->onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public recycle()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mResource:Ljava/lang/Object;

    return-void
.end method

.method public final register(Lcom/google/android/apps/plus/service/ResourceConsumer;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->ensureMainThread()V

    invoke-direct {p0, p1, v5}, Lcom/google/android/apps/plus/service/Resource;->isRegistered(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    invoke-direct {v2, p1, v5}, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;-><init>(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ResourceManager;->onFirstConsumerRegistered(Lcom/google/android/apps/plus/service/Resource;)V

    :cond_0
    invoke-interface {p1, p0, v5}, Lcom/google/android/apps/plus/service/ResourceConsumer;->onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eqz v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    iget-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;-><init>(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v5, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iput-object v5, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    invoke-direct {v2, p1, v5}, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;-><init>(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iput-object v5, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x40

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n  ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/service/Resource;->mId:Lcom/google/android/apps/plus/service/Resource$ResourceIdentifier;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n  Status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/plus/service/Resource;->mStatus:I

    invoke-static {v5}, Lcom/google/android/apps/plus/service/Resource;->statusToString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mResource:Ljava/lang/Object;

    if-eqz v4, :cond_0

    const-string v4, "\n  Size:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/Resource;->getSizeInBytes()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    const-string v4, "\n  Consumers:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    const-string v4, "\n   "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->consumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v5, v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->tag:Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/plus/service/Resource;->appendConsumer(Ljava/lang/StringBuilder;Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-eqz v4, :cond_3

    const-string v4, "\n   "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iget-object v5, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/plus/service/Resource;->appendConsumer(Ljava/lang/StringBuilder;Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)V

    :cond_2
    :goto_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    :cond_3
    const-string v4, "\n   none"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/service/ResourceConsumer;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->ensureMainThread()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iput-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumer:Lcom/google/android/apps/plus/service/ResourceConsumer;

    iput-object v3, p0, Lcom/google/android/apps/plus/service/Resource;->mTag:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ResourceManager;->onLastConsumerUnregistered(Lcom/google/android/apps/plus/service/Resource;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;

    invoke-virtual {v0, p1, v3}, Lcom/google/android/apps/plus/service/Resource$ResourceConsumerHolder;->matches(Lcom/google/android/apps/plus/service/ResourceConsumer;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mConsumerList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/Resource;->mManager:Lcom/google/android/apps/plus/service/ResourceManager;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/ResourceManager;->onLastConsumerUnregistered(Lcom/google/android/apps/plus/service/Resource;)V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
