.class public final Lcom/google/android/apps/plus/service/ResourceDownloadRequest;
.super Lcom/android/volley/Request;
.source "ResourceDownloadRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/volley/Request",
        "<[B>;"
    }
.end annotation


# static fields
.field static final MAX_GIF_DOWNLOAD:I


# instance fields
.field private final mResource:Lcom/google/android/apps/plus/service/Resource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    const/high16 v0, 0x800000

    sput v0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->MAX_GIF_DOWNLOAD:I

    :goto_0
    return-void

    :cond_0
    const/high16 v0, 0x200000

    sput v0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->MAX_GIF_DOWNLOAD:I

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/service/Resource;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/android/volley/Request;-><init>(Ljava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->setShouldCache(Z)V

    return-void
.end method


# virtual methods
.method public final deliverError(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1    # Lcom/android/volley/VolleyError;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to download "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EsResource"

    invoke-static {v2, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    instance-of v1, p1, Lcom/google/android/apps/plus/service/VolleyOutOfMemoryError;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/Resource;->deliverDownloadError(I)V

    :goto_0
    return-void

    :cond_1
    instance-of v1, p1, Lcom/android/volley/NoConnectionError;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/service/Resource;->deliverDownloadError(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    iget v0, v1, Lcom/android/volley/NetworkResponse;->statusCode:I

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/service/Resource;->deliverHttpError$4f708078(I)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final bridge synthetic deliverResponse(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;

    check-cast p1, [B

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/plus/service/Resource;->deliverData([BZ)V

    return-void
.end method

.method public final getPriority()Lcom/android/volley/Request$Priority;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->isPreloading()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/volley/Request$Priority;->LOW:Lcom/android/volley/Request$Priority;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/volley/Request$Priority;->NORMAL:Lcom/android/volley/Request$Priority;

    goto :goto_0
.end method

.method public final getResource()Lcom/google/android/apps/plus/service/Resource;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    return-object v0
.end method

.method public final getUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getDownloadUrl()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 5
    .param p1    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<[B>;"
        }
    .end annotation

    const/4 v1, -0x1

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v3, "Content-Type"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v3, "Content-Type"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    if-eqz v0, :cond_0

    const-string v2, "image/gif"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v1, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->MAX_GIF_DOWNLOAD:I

    :cond_0
    if-lez v1, :cond_4

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->data:[B

    array-length v2, v2

    if-le v2, v1, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/service/Resource;->isDebugLogEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Download is too large: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/android/volley/NetworkResponse;->data:[B

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", allowed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/service/ResourceDownloadRequest;->mResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/service/Resource;->logDebug(Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/android/volley/VolleyError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Download is too large: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/android/volley/NetworkResponse;->data:[B

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", allowed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/volley/VolleyError;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_2
    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v3, "content-type"

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v3, "content-type"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_4
    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->data:[B

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v2

    goto :goto_1
.end method
