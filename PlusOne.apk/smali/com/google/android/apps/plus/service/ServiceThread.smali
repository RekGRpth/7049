.class public final Lcom/google/android/apps/plus/service/ServiceThread;
.super Ljava/lang/Thread;
.source "ServiceThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;
    }
.end annotation


# instance fields
.field private mIntentProcessor:Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;

.field private final mMainHandler:Landroid/os/Handler;

.field private final mProcessQueueRunnable:Ljava/lang/Runnable;

.field private final mQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mThreadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;)V
    .locals 1
    .param p1    # Landroid/os/Handler;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/service/ServiceThread$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/service/ServiceThread$1;-><init>(Lcom/google/android/apps/plus/service/ServiceThread;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mProcessQueueRunnable:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mMainHandler:Landroid/os/Handler;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/service/ServiceThread;->setName(Ljava/lang/String;)V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mQueue:Ljava/util/Queue;

    iput-object p3, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mIntentProcessor:Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/service/ServiceThread;)Ljava/util/Queue;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mQueue:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/service/ServiceThread;)Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mIntentProcessor:Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/service/ServiceThread;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mProcessQueueRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/service/ServiceThread;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/service/ServiceThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mThreadHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final put(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mQueue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mThreadHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mProcessQueueRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public final quit()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mThreadHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mThreadHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Looper;->quit()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    :cond_1
    return-void
.end method

.method public final run()V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->prepare()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mThreadHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mMainHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/plus/service/ServiceThread$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/service/ServiceThread$2;-><init>(Lcom/google/android/apps/plus/service/ServiceThread;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-static {}, Landroid/os/Looper;->loop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mIntentProcessor:Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/service/ServiceThread;->mIntentProcessor:Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;

    invoke-interface {v0}, Lcom/google/android/apps/plus/service/ServiceThread$IntentProcessor;->onServiceThreadEnd()V

    :cond_0
    return-void
.end method
