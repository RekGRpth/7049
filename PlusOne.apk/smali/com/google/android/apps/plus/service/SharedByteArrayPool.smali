.class public final Lcom/google/android/apps/plus/service/SharedByteArrayPool;
.super Ljava/lang/Object;
.source "SharedByteArrayPool.java"


# static fields
.field private static sByteArrayPool:Lcom/android/volley/toolbox/ByteArrayPool;


# direct methods
.method public static getInstance()Lcom/android/volley/toolbox/ByteArrayPool;
    .locals 2

    sget-object v0, Lcom/google/android/apps/plus/service/SharedByteArrayPool;->sByteArrayPool:Lcom/android/volley/toolbox/ByteArrayPool;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/volley/toolbox/ByteArrayPool;

    const/high16 v1, 0x20000

    invoke-direct {v0, v1}, Lcom/android/volley/toolbox/ByteArrayPool;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/service/SharedByteArrayPool;->sByteArrayPool:Lcom/android/volley/toolbox/ByteArrayPool;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/service/SharedByteArrayPool;->sByteArrayPool:Lcom/android/volley/toolbox/ByteArrayPool;

    return-object v0
.end method
