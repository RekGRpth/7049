.class public Lcom/google/android/apps/plus/settings/AboutSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "AboutSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final PRIVACY:Landroid/net/Uri;

.field private static final TERMS:Landroid/net/Uri;

.field private static sLicenseKey:Ljava/lang/String;

.field private static sNetworkStatsKey:Ljava/lang/String;

.field private static sNetworkTransactionsKey:Ljava/lang/String;

.field private static sPrivacyKey:Ljava/lang/String;

.field private static sRemoveAccountKey:Ljava/lang/String;

.field private static sTermsKey:Ljava/lang/String;

.field private static sUploadStatsKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "http://m.google.com/app/plus/serviceurl?type=tos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->TERMS:Landroid/net/Uri;

    const-string v0, "http://m.google.com/app/plus/serviceurl?type=privacy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->PRIVACY:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->PRIVACY:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$100()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->TERMS:Landroid/net/Uri;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sign_out"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sLicenseKey:Ljava/lang/String;

    if-nez v10, :cond_0

    sget v10, Lcom/google/android/apps/plus/R$string;->license_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sLicenseKey:Ljava/lang/String;

    sget v10, Lcom/google/android/apps/plus/R$string;->privacy_policy_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sPrivacyKey:Ljava/lang/String;

    sget v10, Lcom/google/android/apps/plus/R$string;->terms_of_service_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sTermsKey:Ljava/lang/String;

    sget v10, Lcom/google/android/apps/plus/R$string;->remove_account_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sRemoveAccountKey:Ljava/lang/String;

    sget v10, Lcom/google/android/apps/plus/R$string;->network_transactions_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkTransactionsKey:Ljava/lang/String;

    sget v10, Lcom/google/android/apps/plus/R$string;->network_stats_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkStatsKey:Ljava/lang/String;

    sget v10, Lcom/google/android/apps/plus/R$string;->upload_stats_preference_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sput-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sUploadStatsKey:Ljava/lang/String;

    :cond_0
    sget-boolean v10, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v10, :cond_1

    sget v10, Lcom/google/android/apps/plus/R$xml;->network_stats_preferences:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->addPreferencesFromResource(I)V

    :cond_1
    sget v10, Lcom/google/android/apps/plus/R$xml;->about_preferences:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->addPreferencesFromResource(I)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    const-string v10, "build_version"

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    iget-object v11, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    sget-boolean v10, Lcom/google/android/apps/plus/util/EsLog;->ENABLE_DOGFOOD_FEATURES:Z

    if-eqz v10, :cond_2

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkTransactionsKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$1;

    invoke-direct {v10, p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v3, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sNetworkStatsKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$2;

    invoke-direct {v10, p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$2;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v4, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sUploadStatsKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$3;

    invoke-direct {v10, p0, v0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$3;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-virtual {v9, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    :cond_2
    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sLicenseKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$4;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$4;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v2, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sPrivacyKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$5;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v6, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sTermsKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$6;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v8, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->sRemoveAccountKey:Ljava/lang/String;

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    new-instance v10, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$7;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity$7;-><init>(Lcom/google/android/apps/plus/settings/AboutSettingsActivity;)V

    invoke-virtual {v7, v10}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    return-void

    :catch_0
    move-exception v1

    const-string v10, "build_version"

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/AboutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    const-string v11, "?"

    invoke-virtual {v10, v11}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->preferences_remove_account_title:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/apps/plus/R$string;->preferences_remove_account_dialog_message:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/apps/plus/R$string;->ok:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/apps/plus/R$string;->cancel:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
