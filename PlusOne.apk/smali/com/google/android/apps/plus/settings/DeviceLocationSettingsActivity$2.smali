.class final Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;
.super Ljava/lang/Object;
.source "DeviceLocationSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5
    .param p1    # Landroid/preference/Preference;

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$string;->url_param_help_location:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/apps/plus/util/HelpUrl;->getHelpUrl(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$2;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v3, 0x1

    return v3
.end method
