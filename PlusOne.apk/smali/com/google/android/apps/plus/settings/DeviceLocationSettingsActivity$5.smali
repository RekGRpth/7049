.class final Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;
.super Ljava/lang/Object;
.source "DeviceLocationSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->setupPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

.field final synthetic val$locationReportingSettingsIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    iput-object p2, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;->val$locationReportingSettingsIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 2
    .param p1    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;->this$0:Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity$5;->val$locationReportingSettingsIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/DeviceLocationSettingsActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    return v0
.end method
