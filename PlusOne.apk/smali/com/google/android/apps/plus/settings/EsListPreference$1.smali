.class final Lcom/google/android/apps/plus/settings/EsListPreference$1;
.super Ljava/lang/Object;
.source "EsListPreference.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/EsListPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/EsListPreference;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/EsListPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntries:[Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$000(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$100(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-nez p2, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v6}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$200(Lcom/google/android/apps/plus/settings/EsListPreference;)Landroid/view/LayoutInflater;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$layout;->simple_list_item_2_single_choice:I

    invoke-virtual {v6, v7, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    :goto_0
    const v6, 0x1020014

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntries:[Ljava/lang/CharSequence;
    invoke-static {v6}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$000(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;

    move-result-object v6

    aget-object v6, v6, p1

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x1020015

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaryArgument:Ljava/lang/CharSequence;
    invoke-static {v6}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$300(Lcom/google/android/apps/plus/settings/EsListPreference;)Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;
    invoke-static {v6}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$400(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;

    move-result-object v6

    aget-object v6, v6, p1

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v7, v4, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaryArgument:Ljava/lang/CharSequence;
    invoke-static {v8}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$300(Lcom/google/android/apps/plus/settings/EsListPreference;)Ljava/lang/CharSequence;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    sget v6, Lcom/google/android/apps/plus/R$id;->radio:I

    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I
    invoke-static {v6}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$500(Lcom/google/android/apps/plus/settings/EsListPreference;)I

    move-result v6

    if-ne p1, v6, :cond_2

    :goto_2
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    return-object v3

    :cond_0
    move-object v3, p2

    goto :goto_0

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;
    invoke-static {v6}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$400(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;

    move-result-object v6

    aget-object v6, v6, p1

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    move v4, v5

    goto :goto_2
.end method

.method public final getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference$1;->this$0:Lcom/google/android/apps/plus/settings/EsListPreference;

    # getter for: Lcom/google/android/apps/plus/settings/EsListPreference;->mEntries:[Ljava/lang/CharSequence;
    invoke-static {v0}, Lcom/google/android/apps/plus/settings/EsListPreference;->access$000(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method
