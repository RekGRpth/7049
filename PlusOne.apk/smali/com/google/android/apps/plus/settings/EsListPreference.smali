.class public Lcom/google/android/apps/plus/settings/EsListPreference;
.super Landroid/preference/DialogPreference;
.source "EsListPreference.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;
    }
.end annotation


# instance fields
.field private mClickedDialogEntryIndex:I

.field private mEntries:[Ljava/lang/CharSequence;

.field private mEntrySummaries:[Ljava/lang/CharSequence;

.field private mEntrySummaryArgument:Ljava/lang/CharSequence;

.field private mEntryValues:[Ljava/lang/CharSequence;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/settings/EsListPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->EsListPreference:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntries:[Ljava/lang/CharSequence;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntries:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/settings/EsListPreference;)Landroid/view/LayoutInflater;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mInflater:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/settings/EsListPreference;)Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaryArgument:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/settings/EsListPreference;)[Ljava/lang/CharSequence;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaries:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/settings/EsListPreference;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;

    iget v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/settings/EsListPreference;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/EsListPreference;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I

    return p1
.end method

.method private getValueIndex()I
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mValue:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method


# virtual methods
.method protected onDialogClosed(Z)V
    .locals 3
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onDialogClosed(Z)V

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/EsListPreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/EsListPreference;->setValue(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/content/res/TypedArray;
    .param p2    # I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 4
    .param p1    # Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntries:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntryValues:[Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ListPreference requires an entries array and an entryValues array."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/EsListPreference;->getValueIndex()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I

    new-instance v0, Lcom/google/android/apps/plus/settings/EsListPreference$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/settings/EsListPreference$1;-><init>(Lcom/google/android/apps/plus/settings/EsListPreference;)V

    iget v1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mClickedDialogEntryIndex:I

    new-instance v2, Lcom/google/android/apps/plus/settings/EsListPreference$2;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/settings/EsListPreference$2;-><init>(Lcom/google/android/apps/plus/settings/EsListPreference;)V

    invoke-virtual {p1, v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {p1, v3, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/preference/DialogPreference;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, v0, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;->value:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/EsListPreference;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/preference/DialogPreference;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/EsListPreference;->isPersistent()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mValue:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/apps/plus/settings/EsListPreference$SavedState;->value:Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 1
    .param p1    # Z
    .param p2    # Ljava/lang/Object;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mValue:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/EsListPreference;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/settings/EsListPreference;->setValue(Ljava/lang/String;)V

    return-void

    :cond_0
    check-cast p2, Ljava/lang/String;

    goto :goto_0
.end method

.method public final setEntrySummaryArgument(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mEntrySummaryArgument:Ljava/lang/CharSequence;

    return-void
.end method

.method public final setValue(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/EsListPreference;->mValue:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/settings/EsListPreference;->persistString(Ljava/lang/String;)Z

    return-void
.end method
