.class public Lcom/google/android/apps/plus/settings/HangoutSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "HangoutSettingsActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    sget v5, Lcom/google/android/apps/plus/R$xml;->hangout_preferences:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/HangoutSettingsActivity;->addPreferencesFromResource(I)V

    sget v5, Lcom/google/android/apps/plus/R$string;->hangout_ringtone_setting_key:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/HangoutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v5, Lcom/google/android/apps/plus/R$string;->hangout_ringtone_setting_default_value:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/HangoutSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4, v1, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/HangoutSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {p0, v5, v1, v0}, Lcom/google/android/apps/plus/settings/HangoutSettingsActivity;->getRingtoneName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;

    invoke-direct {v5, p0, v1, v0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/BaseSettingsActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    if-eqz v2, :cond_0

    invoke-virtual {v3, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
