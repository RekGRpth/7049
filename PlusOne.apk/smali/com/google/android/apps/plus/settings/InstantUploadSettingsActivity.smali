.class public Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "InstantUploadSettingsActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;,
        Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;
    }
.end annotation


# static fields
.field private static final BUY_QUTOA_URI:Landroid/net/Uri;

.field private static final LEARN_MORE_URI:Landroid/net/Uri;

.field private static final PROJECTION_PICASA_SETTINGS:[Ljava/lang/String;

.field private static sBuyQuotaKey:Ljava/lang/String;

.field private static sConnectionPhotoKey:Ljava/lang/String;

.field private static sConnectionVideoKey:Ljava/lang/String;

.field private static sInstantUploadKey:Ljava/lang/String;

.field private static sLearnMoreKey:Ljava/lang/String;

.field private static sMatchFilter:Landroid/content/IntentFilter;

.field private static sOnBatteryKey:Ljava/lang/String;

.field private static sRoamingUploadKey:Ljava/lang/String;

.field private static sSyncNowKey:Ljava/lang/String;

.field private static sUploadSizeKey:Ljava/lang/String;

.field private static sWifiOnly:Z


# instance fields
.field private mIsReceiverRegistered:Z

.field private mIsUploading:Z

.field private mMasterSyncEnabled:Z

.field private mPhotoSyncEnabled:Z

.field private mQuotaLimit:I

.field private mQuotaUsed:I

.field private mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/content/AsyncTaskLoader",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private final mUploadsProgressReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "auto_upload_enabled"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "sync_on_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "sync_on_roaming"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "sync_on_battery"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "video_upload_wifi_only"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "upload_full_resolution"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "quota_limit"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "quota_used"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->PROJECTION_PICASA_SETTINGS:[Ljava/lang/String;

    const-string v0, "https://www.google.com/settings/storage/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->BUY_QUTOA_URI:Landroid/net/Uri;

    const-string v0, "https://support.google.com/plus/?p=full_size_upload"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->LEARN_MORE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    iput v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    iput v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    new-instance v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mUploadsProgressReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z

    return v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->unregisterUploadProgressReceiver()V

    return-void
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sOnBatteryKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sUploadSizeKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    return v0
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    return v0
.end method

.method static synthetic access$1700()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->PROJECTION_PICASA_SETTINGS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mMasterSyncEnabled:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;I)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_unknown:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_no_wifi:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_roaming:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_no_power:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_user_auth:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_quota:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_no_sdcard:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_now_status_no_background_data:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic access$2000(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mPhotoSyncEnabled:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->registerUploadProgressReceiver()V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Ljava/util/Map;)V
    .locals 9
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
    .param p1    # Ljava/util/Map;

    const/4 v7, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mMasterSyncEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mPhotoSyncEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "auto_upload_enabled"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v4, v2, :cond_2

    move v5, v2

    :goto_1
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getMasterSwitch()Landroid/widget/Switch;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/Switch;->setChecked(Z)V

    :goto_3
    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "sync_on_roaming"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_4
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sOnBatteryKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    const-string v1, "sync_on_battery"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v2, :cond_7

    move v1, v2

    :goto_5
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-eqz v1, :cond_8

    move v1, v2

    :goto_6
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_9

    const-string v1, "WIFI_ONLY"

    sget v4, Lcom/google/android/apps/plus/R$string;->photo_connection_preference_summary_wifi:I

    :goto_7
    invoke-virtual {v0, v4}, Landroid/preference/ListPreference;->setSummary(I)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-eqz v1, :cond_a

    move v1, v2

    :goto_8
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_b

    const-string v1, "WIFI_ONLY"

    sget v4, Lcom/google/android/apps/plus/R$string;->video_connection_preference_summary_wifi:I

    :goto_9
    invoke-virtual {v0, v4}, Landroid/preference/ListPreference;->setSummary(I)V

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    const-string v0, "upload_full_resolution"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const-string v0, "quota_limit"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    const-string v0, "quota_used"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    iget v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    if-eq v0, v7, :cond_c

    iget v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    if-eq v0, v7, :cond_c

    move v0, v2

    :goto_a
    iget v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    invoke-static {p0, v4}, Lcom/google/android/apps/plus/phone/InstantUpload;->getSizeText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    iget v7, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    iget v8, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    sub-int/2addr v7, v8

    invoke-static {p0, v7}, Lcom/google/android/apps/plus/phone/InstantUpload;->getSizeText(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    if-eqz v0, :cond_d

    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_size_quota_available:I

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v7, v8, v3

    aput-object v4, v8, v2

    invoke-virtual {p0, v0, v8}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    :goto_b
    iget v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    iget v7, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->isOutOfQuota(II)Z

    move-result v0

    if-eqz v0, :cond_e

    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_size_preference_summary_overquota:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    :goto_c
    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sUploadSizeKey:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/settings/EsListPreference;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/EsListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/settings/EsListPreference;->setValue(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/settings/EsListPreference;->setEntrySummaryArgument(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->updateEnabledStates(Z)V

    goto/16 :goto_0

    :cond_2
    move v5, v3

    goto/16 :goto_1

    :cond_3
    move v0, v3

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_5

    move v1, v2

    :goto_d
    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto/16 :goto_3

    :cond_5
    move v1, v3

    goto :goto_d

    :cond_6
    move v1, v3

    goto/16 :goto_4

    :cond_7
    move v1, v3

    goto/16 :goto_5

    :cond_8
    const-string v1, "sync_on_wifi_only"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/16 :goto_6

    :cond_9
    const-string v1, "WIFI_OR_MOBILE"

    sget v4, Lcom/google/android/apps/plus/R$string;->photo_connection_preference_summary_mobile:I

    goto/16 :goto_7

    :cond_a
    const-string v1, "video_upload_wifi_only"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto/16 :goto_8

    :cond_b
    const-string v1, "WIFI_OR_MOBILE"

    sget v4, Lcom/google/android/apps/plus/R$string;->video_connection_preference_summary_mobile:I

    goto/16 :goto_9

    :cond_c
    move v0, v3

    goto/16 :goto_a

    :cond_d
    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_size_quota_unknown:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_b

    :cond_e
    if-ne v6, v2, :cond_f

    const-string v1, "FULL"

    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_size_preference_summary_full:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_c

    :cond_f
    const-string v1, "STANDARD"

    sget v0, Lcom/google/android/apps/plus/R$string;->photo_upload_size_preference_summary_standard:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_c
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sSyncNowKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->BUY_QUTOA_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$500()Landroid/net/Uri;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->LEARN_MORE_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)Landroid/net/Uri;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->UPLOAD_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->updateEnabledStates(Z)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    return-void
.end method

.method private recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/analytics/OzActions;

    move-object v1, p0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    invoke-static {v1, v0, p1, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    :cond_0
    return-void
.end method

.method private registerUploadProgressReceiver()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsReceiverRegistered:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsReceiverRegistered:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mUploadsProgressReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sMatchFilter:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method private unregisterUploadProgressReceiver()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsReceiverRegistered:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsReceiverRegistered:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mUploadsProgressReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_0
    return-void
.end method

.method private updateEnabledStates(Z)V
    .locals 5
    .param p1    # Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaLimit:I

    iget v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mQuotaUsed:I

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/iu/InstantUploadFacade;->isOutOfQuota(II)Z

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz p1, :cond_0

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-nez v1, :cond_3

    :cond_0
    move v1, v3

    :goto_0
    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz p1, :cond_4

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-nez v1, :cond_4

    move v1, v3

    :goto_1
    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz p1, :cond_1

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-nez v1, :cond_5

    :cond_1
    move v1, v3

    :goto_2
    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz p1, :cond_6

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-nez v1, :cond_6

    move v1, v3

    :goto_3
    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sUploadSizeKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz p1, :cond_7

    if-nez v0, :cond_7

    move v1, v3

    :goto_4
    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sBuyQuotaKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sLearnMoreKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    if-eqz p1, :cond_2

    sget-boolean v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-nez v1, :cond_8

    :cond_2
    move v1, v3

    :goto_5
    invoke-virtual {v4, v1}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz p1, :cond_9

    sget-boolean v4, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    if-nez v4, :cond_9

    :goto_6
    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sOnBatteryKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    sget-object v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sSyncNowKey:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    return-void

    :cond_3
    move v1, v2

    goto/16 :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5

    :cond_9
    move v3, v2

    goto :goto_6
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    packed-switch p2, :pswitch_data_0

    :goto_0
    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->dismissDialog(I)V

    return-void

    :pswitch_0
    iput-boolean v4, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mIsUploading:Z

    sget-object v3, Lcom/google/android/apps/plus/analytics/OzActions;->CS_SETTINGS_SYNC_ALL:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->recordUserAction(Lcom/google/android/apps/plus/analytics/OzActions;)V

    sget-object v3, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sSyncNowKey:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_sync_preference_cancel_title:I

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setTitle(I)V

    sget v3, Lcom/google/android/apps/plus/R$string;->photo_upload_starting_summary:I

    invoke-virtual {v1, v3}, Landroid/preference/Preference;->setSummary(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$5;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$5;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;Lcom/google/android/apps/plus/content/EsAccount;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->registerUploadProgressReceiver()V

    new-array v3, v4, [Ljava/lang/Void;

    const/4 v4, 0x0

    aput-object v4, v3, v5

    invoke-virtual {v2, v3}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    if-nez v12, :cond_0

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_instant_upload_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_roaming_upload_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_on_battery_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sOnBatteryKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_connection_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->video_connection_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_sync_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sSyncNowKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_upload_size_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sUploadSizeKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_buy_quota_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sBuyQuotaKey:Ljava/lang/String;

    sget v12, Lcom/google/android/apps/plus/R$string;->photo_learn_more_preference_key:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sLearnMoreKey:Ljava/lang/String;

    new-instance v12, Landroid/content/IntentFilter;

    invoke-direct {v12}, Landroid/content/IntentFilter;-><init>()V

    sput-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sMatchFilter:Landroid/content/IntentFilter;

    const-string v13, "com.google.android.apps.plus.iu.upload_all_progress"

    invoke-virtual {v12, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v12, "connectivity"

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    const/4 v12, 0x0

    invoke-virtual {v2, v12}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v12

    if-nez v12, :cond_1

    const/4 v12, 0x1

    :goto_0
    sput-boolean v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sWifiOnly:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccountFromIntent()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_2

    sget v12, Lcom/google/android/apps/plus/R$string;->not_signed_in:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {p0, v12, v13}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->finish()V

    :goto_1
    return-void

    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    :cond_2
    sget v12, Lcom/google/android/apps/plus/R$xml;->photo_preferences:I

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->addPreferencesFromResource(I)V

    new-instance v7, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;

    invoke-direct {v7, p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$PhotoPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sInstantUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const/4 v13, 0x0

    move-object v12, v5

    check-cast v12, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0, v13, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->hookMasterSwitch(Landroid/preference/PreferenceCategory;Landroid/preference/CheckBoxPreference;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionPhotoKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sConnectionVideoKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sRoamingUploadKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sOnBatteryKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sUploadSizeKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    invoke-virtual {v11, v7}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sBuyQuotaKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$2;

    invoke-direct {v12, p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$2;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    invoke-virtual {v1, v12}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sLearnMoreKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v6

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$3;

    invoke-direct {v12, p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$3;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    invoke-virtual {v6, v12}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    sget-object v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->sSyncNowKey:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v10

    new-instance v12, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$4;

    invoke-direct {v12, p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$4;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    invoke-virtual {v10, v12}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    check-cast v5, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v12

    invoke-direct {p0, v12}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->updateEnabledStates(Z)V

    goto :goto_1
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/apps/plus/R$string;->photo_upload_confirmation:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/apps/plus/R$string;->yes:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v1, Lcom/google/android/apps/plus/R$string;->no:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onDestroy()V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    invoke-virtual {v0}, Landroid/support/v4/content/AsyncTaskLoader;->reset()V

    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    invoke-virtual {v0}, Landroid/support/v4/content/AsyncTaskLoader;->stopLoading()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->unregisterUploadProgressReceiver()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/EsAccount;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/util/AccountsUtil;->newAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    const-string v1, "com.google.android.apps.plus.iu.EsGoogleIuProvider"

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mPhotoSyncEnabled:Z

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mMasterSyncEnabled:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mMasterSyncEnabled:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mPhotoSyncEnabled:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity$SystemSettingLoader;-><init>(Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->mSettingsLoader:Landroid/support/v4/content/AsyncTaskLoader;

    invoke-virtual {v1}, Landroid/support/v4/content/AsyncTaskLoader;->startLoading()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/InstantUploadSettingsActivity;->finish()V

    goto :goto_0
.end method
