.class public Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "MessengerSettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;,
        Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$TimeoutRunnable;,
        Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;
    }
.end annotation


# instance fields
.field private mAclSummaryToSet:Ljava/lang/Integer;

.field private mAclValueToSet:Ljava/lang/String;

.field private mCurrentBackend:Ljava/lang/String;

.field private mRequestId:Ljava/lang/Integer;

.field private final mServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

.field private mTimeoutRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclSummaryToSet:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->processSetAclResult(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mCurrentBackend:Ljava/lang/String;

    return-object v0
.end method

.method private processSetAclResult(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V
    .locals 8
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    const/4 v7, 0x1

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->hasSetAclsResponse()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->getSetAclsResponse()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->hasStatus()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v5

    sget-object v6, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-ne v5, v6, :cond_0

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_key:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iget-object v5, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclSummaryToSet:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/preference/ListPreference;->setSummary(I)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_key:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->dismissDialog(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mHandler:Landroid/os/Handler;

    iget-object v6, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v5, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->dismissDialog(I)V

    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->showDialog(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v10, "request_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v10, "request_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v10, "acl_value"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    const-string v10, "acl_value"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;

    :cond_1
    const-string v10, "acl_summary_string_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v10, "acl_summary_string_id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclSummaryToSet:Ljava/lang/Integer;

    :cond_2
    sget v10, Lcom/google/android/apps/plus/R$xml;->realtimechat_preferences:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->addPreferencesFromResource(I)V

    invoke-static {}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->debuggable()Z

    move-result v10

    if-eqz v10, :cond_3

    sget v10, Lcom/google/android/apps/plus/R$xml;->realtimechat_development_preferences:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->addPreferencesFromResource(I)V

    sget v10, Lcom/google/android/apps/plus/R$string;->realtimechat_backend_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    new-instance v10, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$BackendPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;B)V

    invoke-virtual {v2, v10}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v8

    sget v10, Lcom/google/android/apps/plus/R$string;->realtimechat_backend_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget v11, Lcom/google/android/apps/plus/R$string;->debug_realtimechat_default_backend:I

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v8, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mCurrentBackend:Ljava/lang/String;

    :cond_3
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    sget v10, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v10, Lcom/google/android/apps/plus/R$string;->realtimechat_default_acl_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-interface {v5, v4, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v9, -0x1

    sget v10, Lcom/google/android/apps/plus/R$string;->key_acl_setting_anyone:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    sget v9, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_subtitle_anyone:I

    :cond_4
    :goto_0
    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    new-instance v10, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;

    invoke-direct {v10, p0}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity$AclPreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;)V

    invoke-virtual {v1, v10}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const/4 v10, -0x1

    if-eq v9, v10, :cond_5

    invoke-virtual {v1, v9}, Landroid/preference/Preference;->setSummary(I)V

    :cond_5
    sget v10, Lcom/google/android/apps/plus/R$string;->realtimechat_ringtone_setting_key:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    sget v10, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_default_value:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v10, 0x0

    invoke-virtual {p0, v10, v4, v3}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getRingtoneName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v10, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;

    invoke-direct {v10, p0, v4, v3}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/BaseSettingsActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    if-eqz v6, :cond_6

    invoke-virtual {v7, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_6
    return-void

    :cond_7
    sget v10, Lcom/google/android/apps/plus/R$string;->key_acl_setting_my_circles:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    sget v9, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_subtitle_my_circles:I

    goto :goto_0

    :cond_8
    sget v10, Lcom/google/android/apps/plus/R$string;->key_acl_setting_extended_circles:I

    invoke-virtual {p0, v10}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    sget v9, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_subtitle_extended_circles:I

    goto :goto_0
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_update_pending_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_update_pending:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_update_failed_title:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_acl_update_failed:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->unregisterListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->registerListener(Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatService;->removeResult(I)Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceResult;->getCommand()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->processSetAclResult(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->dismissDialog(I)V

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mTimeoutRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "acl_value"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclValueToSet:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclSummaryToSet:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    const-string v0, "acl_summary_string_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/MessengerSettingsActivity;->mAclSummaryToSet:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    return-void
.end method
