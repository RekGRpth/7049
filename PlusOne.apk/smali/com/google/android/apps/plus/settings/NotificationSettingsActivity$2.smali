.class final Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;
.super Ljava/lang/Object;
.source "NotificationSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    if-ne p2, v0, :cond_0

    move v0, v1

    :goto_0
    # invokes: Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->updatedEnabledStates(Z)V
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->access$300(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Z)V

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
