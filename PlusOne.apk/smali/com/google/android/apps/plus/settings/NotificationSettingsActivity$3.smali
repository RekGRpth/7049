.class final Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;
.super Ljava/lang/Object;
.source "NotificationSettingsActivity.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

.field final synthetic val$setting:Lcom/google/android/apps/plus/content/NotificationSetting;

.field final synthetic val$settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSetting;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    iput-object p2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->val$setting:Lcom/google/android/apps/plus/content/NotificationSetting;

    iput-object p3, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->val$settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 6
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x1

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->val$setting:Lcom/google/android/apps/plus/content/NotificationSetting;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/content/NotificationSetting;->setEnabled(Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Lcom/google/android/apps/plus/content/NotificationSetting;

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->val$setting:Lcom/google/android/apps/plus/content/NotificationSetting;

    invoke-direct {v3, v4}, Lcom/google/android/apps/plus/content/NotificationSetting;-><init>(Lcom/google/android/apps/plus/content/NotificationSetting;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    const/4 v4, 0x0

    invoke-direct {v3, v4, v1}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/content/NotificationSettingsData;

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->val$settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getEmailAddress()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->val$settings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getMobileNotificationType()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/google/android/apps/plus/content/NotificationSettingsData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    iget-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;->this$0:Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/google/android/apps/plus/service/EsService;->changeNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Ljava/lang/Integer;

    return v5
.end method
