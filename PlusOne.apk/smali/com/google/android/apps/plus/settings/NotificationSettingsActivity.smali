.class public Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
.super Lcom/google/android/apps/plus/settings/BaseSettingsActivity;
.source "NotificationSettingsActivity.java"


# instance fields
.field private mGetNotificationsRequestId:Ljava/lang/Integer;

.field private mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

.field private final mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;-><init>()V

    new-instance v0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$1;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)Ljava/lang/Integer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSettingsData;)Lcom/google/android/apps/plus/content/NotificationSettingsData;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
    .param p1    # Lcom/google/android/apps/plus/content/NotificationSettingsData;

    iput-object p1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->updatedEnabledStates(Z)V

    return-void
.end method

.method private setupPreferences()V
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v14

    if-eqz v14, :cond_0

    invoke-virtual {v14}, Landroid/preference/PreferenceScreen;->removeAll()V

    :cond_0
    sget v20, Lcom/google/android/apps/plus/R$xml;->notifications_preferences:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->addPreferencesFromResource(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v14

    sget v20, Lcom/google/android/apps/plus/R$string;->notifications_preference_enabled_key:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    check-cast v11, Landroid/preference/CheckBoxPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    if-eqz v4, :cond_2

    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    :goto_0
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v11}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->hookMasterSwitch(Landroid/preference/PreferenceCategory;Landroid/preference/CheckBoxPreference;)V

    new-instance v20, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;

    invoke-direct/range {v20 .. v21}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$2;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    sget v20, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_key:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v16

    sget v20, Lcom/google/android/apps/plus/R$string;->notifications_preference_ringtone_default_value:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v10, v7}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getRingtoneName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    new-instance v20, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v10, v7}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity$RingtonePreferenceChangeListener;-><init>(Lcom/google/android/apps/plus/settings/BaseSettingsActivity;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    if-eqz v15, :cond_1

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    move-object/from16 v20, v0

    if-eqz v20, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    move-object/from16 v18, v0

    const/4 v8, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getCategoriesCount()I

    move-result v5

    :goto_1
    if-ge v8, v5, :cond_5

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/content/NotificationSettingsData;->getCategory(I)Lcom/google/android/apps/plus/content/NotificationSettingsCategory;

    move-result-object v6

    new-instance v13, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getDescription()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    add-int/lit8 v20, v8, 0x2

    move/from16 v0, v20

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    invoke-virtual {v14, v13}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    const/4 v9, 0x0

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getSettingsCount()I

    move-result v19

    :goto_2
    move/from16 v0, v19

    if-ge v9, v0, :cond_3

    invoke-virtual {v6, v9}, Lcom/google/android/apps/plus/content/NotificationSettingsCategory;->getSetting(I)Lcom/google/android/apps/plus/content/NotificationSetting;

    move-result-object v17

    new-instance v12, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    sget v20, Lcom/google/android/apps/plus/R$layout;->label_preference:I

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setLayoutResource(I)V

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/NotificationSetting;->getDescription()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/NotificationSetting;->isEnabled()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    new-instance v20, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$3;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;Lcom/google/android/apps/plus/content/NotificationSetting;Lcom/google/android/apps/plus/content/NotificationSettingsData;)V

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v13, v12}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_2
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_4
    new-instance v13, Landroid/preference/PreferenceCategory;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    sget v20, Lcom/google/android/apps/plus/R$string;->notifications_preference_no_network_category:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    const/16 v20, 0x7d0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    invoke-virtual {v14, v13}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    new-instance v12, Landroid/preference/Preference;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    sget v20, Lcom/google/android/apps/plus/R$layout;->label_preference:I

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setLayoutResource(I)V

    sget v20, Lcom/google/android/apps/plus/R$string;->notifications_preference_no_network_alert:I

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v20, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$4;

    invoke-direct/range {v20 .. v21}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity$4;-><init>(Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;)V

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {v13, v12}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_5
    invoke-virtual {v11}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->updatedEnabledStates(Z)V

    return-void
.end method

.method private updatedEnabledStates(Z)V
    .locals 6
    .param p1    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    const/4 v1, 0x0

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-virtual {v4, v1}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    sget v5, Lcom/google/android/apps/plus/R$string;->notifications_preference_enabled_key:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_1
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "pending_request_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    :cond_0
    const-string v0, "notification_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "notification_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/NotificationSettingsData;

    iput-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    :cond_1
    return-void
.end method

.method public onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1    # I
    .param p2    # Landroid/os/Bundle;

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0a003f
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onPause()V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->unregisterListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    return-void
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x0

    const v3, 0x7f0a003f

    invoke-super {p0}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onResume()V

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mServiceListener:Lcom/google/android/apps/plus/service/EsServiceListener;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->registerListener(Lcom/google/android/apps/plus/service/EsServiceListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->getAccount()Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    if-nez v2, :cond_1

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->getNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->showDialog(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->isRequestPending(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/apps/plus/service/EsService;->removeResult(I)Lcom/google/android/apps/plus/service/ServiceResult;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/ServiceResult;->hasError()Z

    move-result v2

    if-eqz v2, :cond_3

    iput-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    iput-object v4, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-direct {p0}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->setupPreferences()V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->dismissDialog(I)V

    goto :goto_0

    :cond_3
    invoke-static {p0, v0}, Lcom/google/android/apps/plus/service/EsService;->getNotificationSettings(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/settings/BaseSettingsActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string v0, "pending_request_id"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mGetNotificationsRequestId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    if-eqz v0, :cond_1

    const-string v0, "notification_settings"

    iget-object v1, p0, Lcom/google/android/apps/plus/settings/NotificationSettingsActivity;->mNotificationSettings:Lcom/google/android/apps/plus/content/NotificationSettingsData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-void
.end method
