.class public final Lcom/google/android/apps/plus/util/EventDateUtils;
.super Ljava/lang/Object;
.source "EventDateUtils.java"


# static fields
.field private static sAbsoluteDateFormat:Ljava/lang/String;

.field private static sEndDateFormat:Ljava/lang/String;

.field private static sLongDateFormatter:Ljava/text/DateFormat;

.field private static sMediumDateFormatter:Ljava/text/DateFormat;

.field private static sRelativeBeginDateFormat:Ljava/lang/String;

.field private static sRelativeEndDateFormat:Ljava/lang/String;

.field private static sStartDateFormat:Ljava/lang/String;

.field private static sTimeFormatter:Ljava/text/DateFormat;

.field private static sToday:Ljava/lang/String;

.field private static sTodayMsec:J

.field private static sTomorrow:Ljava/lang/String;

.field private static sTomorrowMsec:J

.field private static sYesterday:Ljava/lang/String;

.field private static sYesterdayMsec:J


# direct methods
.method private static format(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 2
    .param p0    # Ljava/text/DateFormat;
    .param p1    # Ljava/util/Date;
    .param p2    # Ljava/util/TimeZone;

    invoke-virtual {p0}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_0
    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-object v1
.end method

.method public static getDateRange(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/api/services/plusi/model/EventTime;
    .param p2    # Lcom/google/api/services/plusi/model/EventTime;
    .param p3    # Z

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeStrings(Landroid/content/Context;)V

    iget-object v4, p1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDateDisplayLine(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v2}, Lcom/google/android/apps/plus/util/EventDateUtils;->getDisplayTime(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    const-string v4, "%s %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private static getDisplayTime(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # J

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeFormats(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getDisplayTime(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/util/TimeZone;

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeFormats(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v2}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v2, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    sget-object v2, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static getSingleDateDisplayLine(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # J

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeFormats(Landroid/content/Context;)V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    sget-object v2, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v1, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static getSingleDateDisplayLine(Landroid/content/Context;JLjava/util/TimeZone;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/util/TimeZone;

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeFormats(Landroid/content/Context;)V

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    sget-object v4, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    monitor-enter v4

    :try_start_0
    sget-object v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v3}, Ljava/text/DateFormat;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v3, p3}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    sget-object v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v3, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v1

    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;ZLjava/util/TimeZone;Z)Ljava/lang/String;
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/api/services/plusi/model/EventTime;
    .param p2    # Z
    .param p3    # Ljava/util/TimeZone;
    .param p4    # Z

    iget-object v7, p1, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v6, 0x0

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeFormats(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/EventDateUtils;->initializeStrings(Landroid/content/Context;)V

    sget-wide v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    cmp-long v7, v2, v7

    if-lez v7, :cond_2

    sget-wide v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    const-wide/32 v9, 0x5265c00

    add-long/2addr v7, v9

    cmp-long v7, v2, v7

    if-gez v7, :cond_2

    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    :cond_0
    :goto_0
    if-nez p3, :cond_1

    if-eqz p1, :cond_1

    iget-object v7, p1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, p1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object p3

    iget-object v7, p1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->areTimeZoneIdsEquivalent(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    const/4 p3, 0x0

    :cond_1
    if-eqz v6, :cond_7

    if-eqz p2, :cond_6

    sget-object v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeEndDateFormat:Ljava/lang/String;

    :goto_1
    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v6, v8, v9

    const/4 v9, 0x1

    sget-object v10, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-static {v10, v1, p3}, Lcom/google/android/apps/plus/util/EventDateUtils;->format(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_2
    return-object v4

    :cond_2
    sget-wide v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    cmp-long v7, v2, v7

    if-lez v7, :cond_3

    sget-wide v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    const-wide/32 v9, 0x5265c00

    add-long/2addr v7, v9

    cmp-long v7, v2, v7

    if-gez v7, :cond_3

    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrow:Ljava/lang/String;

    goto :goto_0

    :cond_3
    sget-wide v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    cmp-long v7, v2, v7

    if-lez v7, :cond_4

    sget-wide v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    const-wide/32 v9, 0x5265c00

    add-long/2addr v7, v9

    cmp-long v7, v2, v7

    if-gez v7, :cond_4

    sget-object v6, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterday:Ljava/lang/String;

    goto :goto_0

    :cond_4
    if-eqz p4, :cond_0

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v7, p1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    if-eqz v7, :cond_5

    iget-object v7, p1, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_5
    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v7, 0x7

    const/4 v8, 0x2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v0, v7, v8, v9}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_6
    sget-object v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeBeginDateFormat:Ljava/lang/String;

    goto :goto_1

    :cond_7
    sget-object v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sAbsoluteDateFormat:Ljava/lang/String;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sget-object v10, Lcom/google/android/apps/plus/util/EventDateUtils;->sMediumDateFormatter:Ljava/text/DateFormat;

    invoke-static {v10, v1, p3}, Lcom/google/android/apps/plus/util/EventDateUtils;->format(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget-object v10, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    invoke-static {v10, v1, p3}, Lcom/google/android/apps/plus/util/EventDateUtils;->format(Ljava/text/DateFormat;Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    if-eqz p2, :cond_8

    sget-object v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sEndDateFormat:Ljava/lang/String;

    :goto_3
    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    :cond_8
    sget-object v7, Lcom/google/android/apps/plus/util/EventDateUtils;->sStartDateFormat:Ljava/lang/String;

    goto :goto_3
.end method

.method private static initializeFormats(Landroid/content/Context;)V
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sMediumDateFormatter:Ljava/text/DateFormat;

    if-nez v0, :cond_0

    invoke-static {p0}, Landroid/text/format/DateFormat;->getMediumDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sMediumDateFormatter:Ljava/text/DateFormat;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sLongDateFormatter:Ljava/text/DateFormat;

    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/EventDateUtils;->sTimeFormatter:Ljava/text/DateFormat;

    :cond_0
    return-void
.end method

.method private static initializeStrings(Landroid/content/Context;)V
    .locals 8
    .param p0    # Landroid/content/Context;

    const-wide/32 v6, 0x5265c00

    const/4 v5, 0x0

    sget-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$string;->today:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sToday:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->tomorrow:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrow:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->yesterday:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterday:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_relative_start_date_format:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeBeginDateFormat:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_relative_end_date_format:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sRelativeEndDateFormat:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_absolute_date_format:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sAbsoluteDateFormat:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_end_date_format:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sEndDateFormat:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_start_date_format:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sStartDateFormat:Ljava/lang/String;

    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    sget-wide v3, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    cmp-long v1, v1, v3

    if-lez v1, :cond_1

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    sput-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    add-long/2addr v1, v6

    sput-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTomorrowMsec:J

    sget-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sTodayMsec:J

    sub-long/2addr v1, v6

    sput-wide v1, Lcom/google/android/apps/plus/util/EventDateUtils;->sYesterdayMsec:J

    :cond_1
    return-void
.end method

.method public static shouldShowDay(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z
    .locals 8
    .param p0    # J
    .param p2    # Ljava/util/TimeZone;
    .param p3    # J
    .param p5    # Ljava/util/TimeZone;
    .param p6    # Z

    sub-long v4, p3, p0

    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_5

    const-wide/32 v6, 0x240c8400

    cmp-long v6, v4, v6

    if-gez v6, :cond_5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    if-eqz p2, :cond_0

    invoke-virtual {v2, p2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_0
    invoke-virtual {v2, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    if-eqz p5, :cond_1

    invoke-virtual {v0, p5}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    :cond_1
    invoke-virtual {v0, p3, p4}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v6, 0x7

    invoke-virtual {v2, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v6, 0x7

    invoke-virtual {v0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-lt v3, v1, :cond_3

    if-nez p6, :cond_2

    if-ne v3, v1, :cond_3

    :cond_2
    const-wide/32 v6, 0x5265c00

    cmp-long v6, v4, v6

    if-gez v6, :cond_4

    :cond_3
    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_4
    const/4 v6, 0x0

    goto :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_0
.end method
