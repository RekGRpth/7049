.class final Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;
.super Landroid/os/HandlerThread;
.source "GifDrawable.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/util/GifDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DecoderThread"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    const-string v0, "GifDecoder"

    invoke-direct {p0, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)Z
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/plus/util/GifDrawable;

    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    move v1, v2

    :goto_0
    return v1

    :cond_0
    :pswitch_1
    :try_start_0
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$000(Lcom/google/android/apps/plus/util/GifDrawable;)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mEndOfFile:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$100(Lcom/google/android/apps/plus/util/GifDrawable;)Z

    move-result v3

    if-eqz v3, :cond_1

    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$200(Lcom/google/android/apps/plus/util/GifDrawable;)I

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;->access$302(Lcom/google/android/apps/plus/util/GifDrawable;Z)Z

    :cond_1
    :goto_2
    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mEndOfFile:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$100(Lcom/google/android/apps/plus/util/GifDrawable;)Z

    move-result v3

    if-eqz v3, :cond_2

    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$300(Lcom/google/android/apps/plus/util/GifDrawable;)Z

    move-result v3

    if-nez v3, :cond_2

    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$500(Lcom/google/android/apps/plus/util/GifDrawable;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$700(Lcom/google/android/apps/plus/util/GifDrawable;)Landroid/os/Handler;

    move-result-object v3

    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$700(Lcom/google/android/apps/plus/util/GifDrawable;)Landroid/os/Handler;

    move-result-object v4

    const/16 v5, 0xb

    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mNextFrameDelay:I
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$600(Lcom/google/android/apps/plus/util/GifDrawable;)I

    move-result v6

    invoke-virtual {v4, v5, v6, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;->access$102(Lcom/google/android/apps/plus/util/GifDrawable;Z)Z

    goto :goto_1

    :cond_3
    # getter for: Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$200(Lcom/google/android/apps/plus/util/GifDrawable;)I

    move-result v3

    if-le v3, v1, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$400(Lcom/google/android/apps/plus/util/GifDrawable;)V

    goto :goto_2

    :cond_4
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;->access$502(Lcom/google/android/apps/plus/util/GifDrawable;Z)Z

    goto :goto_2

    :pswitch_2
    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifDrawable;->access$400(Lcom/google/android/apps/plus/util/GifDrawable;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
