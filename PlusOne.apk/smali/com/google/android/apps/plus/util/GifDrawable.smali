.class public final Lcom/google/android/apps/plus/util/GifDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "GifDrawable.java"

# interfaces
.implements Landroid/graphics/drawable/Animatable;
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/apps/plus/views/Recyclable;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;
    }
.end annotation


# static fields
.field private static final NETSCAPE2_0:[B

.field private static sDecoderHandler:Landroid/os/Handler;

.field private static sDecoderThread:Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;

.field private static sPaint:Landroid/graphics/Paint;

.field private static sScalePaint:Landroid/graphics/Paint;


# instance fields
.field private mActiveColorTable:[I

.field private mAnimationEnabled:Z

.field private mBackgroundColor:I

.field private mBackup:[I

.field private mBackupSaved:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBlock:[B

.field private mColors:[I

.field private final mData:[B

.field private mDisposalMethod:I

.field private volatile mDone:Z

.field private mEndOfFile:Z

.field private volatile mError:Z

.field private mFirstFrameReady:Z

.field private mFrameCount:I

.field private mFrameDelay:I

.field private mFrameHeight:I

.field private mFrameStep:I

.field private mFrameWidth:I

.field private mFrameX:I

.field private mFrameY:I

.field private final mGifImage:Lcom/google/android/apps/plus/util/GifImage;

.field private final mHandler:Landroid/os/Handler;

.field private mHeight:I

.field private mInterlace:Z

.field private mIntrinsicHeight:I

.field private mIntrinsicWidth:I

.field private mLastFrameTime:J

.field private mLocalColorTable:[I

.field private mLocalColorTableSize:I

.field private mLocalColorTableUsed:Z

.field private mNextFrameDelay:I

.field private mPixelStack:[B

.field private mPixels:[B

.field private mPosition:I

.field private mPrefix:[S

.field private mRecycled:Z

.field private mRunning:Z

.field private mScale:Z

.field private mScaleFactor:F

.field private mScheduled:Z

.field private mSuffix:[B

.field private mTransparency:Z

.field private mTransparentColorIndex:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "NETSCAPE2.0"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/GifDrawable;->NETSCAPE2_0:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/plus/util/GifImage;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/util/GifImage;

    const/16 v3, 0x1000

    const/4 v5, 0x2

    const/4 v2, 0x1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    const/16 v1, 0x100

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    iput v5, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    new-array v1, v3, [S

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    new-array v1, v3, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    const/16 v1, 0x1001

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    iput-boolean v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mAnimationEnabled:Z

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderThread:Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;

    invoke-direct {v1}, Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderThread:Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;->start()V

    new-instance v1, Landroid/os/Handler;

    sget-object v3, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderThread:Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderThread:Lcom/google/android/apps/plus/util/GifDrawable$DecoderThread;

    invoke-direct {v1, v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    sput-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    :cond_0
    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_1

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sScalePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/util/GifImage;->getData()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    iget v1, v1, Lcom/google/android/apps/plus/util/GifImage;->mHeaderSize:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/util/GifImage;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameStep:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/util/GifImage;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    iget v1, v1, Lcom/google/android/apps/plus/util/GifImage;->mBackgroundColor:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundColor:I

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    iget-boolean v1, v1, Lcom/google/android/apps/plus/util/GifImage;->mError:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v1, :cond_2

    :try_start_0
    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v4, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    sget v1, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    const/16 v5, 0x40

    if-ge v1, v5, :cond_3

    move v1, v2

    :goto_0
    if-eqz v1, :cond_4

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    :goto_1
    invoke-static {v3, v4, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    mul-int v0, v1, v3

    new-array v1, v0, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    sget-object v3, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    const/16 v4, 0xa

    invoke-virtual {v3, v4, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_2
    :goto_2
    return-void

    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/util/GifDrawable;)V
    .locals 22
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mEndOfFile:Z

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mNextFrameDelay:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    :cond_1
    :goto_1
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    sparse-switch v2, :sswitch_data_1

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto :goto_1

    :pswitch_0
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    array-length v6, v6

    invoke-static {v2, v3, v4, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    :pswitch_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundColor:I

    :cond_2
    const/4 v3, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    if-ge v3, v4, :cond_0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    add-int/2addr v4, v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    mul-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    add-int/2addr v5, v4

    :goto_3
    if-ge v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    aput v2, v6, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :sswitch_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    and-int/lit8 v3, v2, 0x1c

    shr-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    mul-int/lit8 v2, v2, 0xa

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mNextFrameDelay:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mNextFrameDelay:I

    const/16 v3, 0xa

    if-gt v2, v3, :cond_4

    const/16 v2, 0x64

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mNextFrameDelay:I

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_4

    :sswitch_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readBlock()I

    const/4 v3, 0x1

    const/4 v2, 0x0

    :goto_5
    sget-object v4, Lcom/google/android/apps/plus/util/GifDrawable;->NETSCAPE2_0:[B

    array-length v4, v4

    if-ge v2, v4, :cond_2a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    aget-byte v4, v4, v2

    sget-object v5, Lcom/google/android/apps/plus/util/GifDrawable;->NETSCAPE2_0:[B

    aget-byte v5, v5, v2

    if-eq v4, v5, :cond_7

    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_8

    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readBlock()I

    move-result v2

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-eqz v2, :cond_6

    goto/16 :goto_1

    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto/16 :goto_1

    :sswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto/16 :goto_1

    :sswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    goto/16 :goto_1

    :sswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->readShort()I

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    sub-int/2addr v4, v5

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameStep:I

    mul-int/2addr v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    array-length v3, v3

    if-le v2, v3, :cond_9

    new-array v2, v2, [B

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v3, v2, 0xff

    and-int/lit8 v2, v3, 0x40

    if-eqz v2, :cond_b

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mInterlace:Z

    and-int/lit16 v2, v3, 0x80

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableUsed:Z

    const-wide/high16 v4, 0x4000000000000000L

    and-int/lit8 v2, v3, 0x7

    add-int/lit8 v2, v2, 0x1

    int-to-double v2, v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    double-to-int v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableSize:I

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableUsed:Z

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    if-nez v2, :cond_a

    const/16 v2, 0x100

    new-array v2, v2, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTableSize:I

    const/4 v2, 0x0

    :goto_9
    if-ge v2, v4, :cond_d

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v7, v6, 0x1

    move-object/from16 v0, p0

    iput v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v5, v5, v6

    and-int/lit16 v5, v5, 0xff

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v8, v7, 0x1

    move-object/from16 v0, p0

    iput v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v6, v6, v7

    and-int/lit16 v6, v6, 0xff

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v9, v8, 0x1

    move-object/from16 v0, p0

    iput v9, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v7, v7, v8

    and-int/lit16 v7, v7, 0xff

    const/high16 v8, -0x1000000

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v5, v8

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    or-int/2addr v5, v7

    aput v5, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_c
    const/4 v2, 0x0

    goto :goto_8

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mLocalColorTable:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    :cond_e
    :goto_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    aget v2, v2, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    const/4 v5, 0x0

    aput v5, v3, v4

    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    if-nez v3, :cond_10

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    :cond_10
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v3, :cond_25

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    mul-int v14, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v5, v4, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v3, v3, v4

    and-int/lit16 v15, v3, 0xff

    const/4 v3, 0x1

    shl-int v16, v3, v15

    add-int/lit8 v17, v16, 0x1

    add-int/lit8 v11, v16, 0x2

    const/4 v10, -0x1

    add-int/lit8 v9, v15, 0x1

    const/4 v3, 0x1

    shl-int/2addr v3, v9

    add-int/lit8 v8, v3, -0x1

    const/4 v3, 0x0

    :goto_b
    move/from16 v0, v16

    if-ge v3, v0, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    const/4 v5, 0x0

    aput-short v5, v4, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    int-to-byte v5, v3

    aput-byte v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTable:[I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    iget v2, v2, Lcom/google/android/apps/plus/util/GifImage;->mBackgroundIndex:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    if-ne v2, v3, :cond_e

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackgroundColor:I

    goto/16 :goto_a

    :cond_12
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    :cond_13
    if-ge v3, v14, :cond_21

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v18, v13, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    if-eqz v12, :cond_21

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int v18, v13, v12

    :cond_14
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    move/from16 v0, v18

    if-ge v12, v0, :cond_13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v19, v13, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v12, v12, v13

    and-int/lit16 v12, v12, 0xff

    shl-int/2addr v12, v6

    add-int/2addr v7, v12

    add-int/lit8 v6, v6, 0x8

    :goto_c
    if-lt v6, v9, :cond_14

    and-int v12, v7, v8

    shr-int v13, v7, v9

    sub-int v7, v6, v9

    move/from16 v0, v16

    if-ne v12, v0, :cond_15

    add-int/lit8 v9, v15, 0x1

    const/4 v6, 0x1

    shl-int/2addr v6, v9

    add-int/lit8 v8, v6, -0x1

    add-int/lit8 v11, v16, 0x2

    const/4 v10, -0x1

    move v6, v7

    move v7, v13

    goto :goto_c

    :cond_15
    move/from16 v0, v17

    if-ne v12, v0, :cond_1a

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    :cond_16
    :goto_d
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->skip()V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v3, :cond_25

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_17

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/util/GifDrawable;->backupFrame()V

    :cond_17
    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, 0x0

    :goto_e
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    if-ge v3, v7, :cond_23

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mInterlace:Z

    if-eqz v7, :cond_26

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameHeight:I

    if-lt v4, v7, :cond_18

    add-int/lit8 v6, v6, 0x1

    packed-switch v6, :pswitch_data_1

    :cond_18
    :goto_f
    add-int v7, v4, v5

    move/from16 v21, v4

    move v4, v7

    move/from16 v7, v21

    :goto_10
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameY:I

    add-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    if-ge v7, v8, :cond_22

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    mul-int/2addr v7, v8

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameX:I

    add-int/2addr v8, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameWidth:I

    add-int v10, v8, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameStep:I

    mul-int/2addr v7, v3

    move v9, v8

    :goto_11
    if-ge v9, v10, :cond_22

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    add-int/lit8 v8, v7, 0x1

    aget-byte v7, v11, v7

    and-int/lit16 v7, v7, 0xff

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    aget v7, v11, v7

    if-eqz v7, :cond_19

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    aput v7, v11, v9

    :cond_19
    add-int/lit8 v7, v9, 0x1

    move v9, v7

    move v7, v8

    goto :goto_11

    :cond_1a
    const/4 v6, -0x1

    if-ne v10, v6, :cond_1b

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    add-int/lit8 v5, v3, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    aget-byte v10, v10, v12

    aput-byte v10, v6, v3

    move v3, v5

    move v6, v7

    move v10, v12

    move v7, v13

    move v5, v12

    goto/16 :goto_c

    :cond_1b
    if-lt v12, v11, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v19, v0

    add-int/lit8 v6, v4, 0x1

    int-to-byte v5, v5

    aput-byte v5, v19, v4

    const/16 v4, 0x1001

    if-ne v6, v4, :cond_28

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    goto/16 :goto_d

    :cond_1c
    move v5, v6

    :goto_12
    move/from16 v0, v16

    if-lt v4, v0, :cond_1f

    const/16 v6, 0x1001

    if-ge v4, v6, :cond_1d

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    aget-short v6, v6, v4

    if-ne v4, v6, :cond_1e

    :cond_1d
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    goto/16 :goto_d

    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v19, v0

    add-int/lit8 v6, v5, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    move-object/from16 v20, v0

    aget-byte v20, v20, v4

    aput-byte v20, v19, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    aget-short v4, v5, v4

    const/16 v5, 0x1001

    if-ne v6, v5, :cond_1c

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    goto/16 :goto_d

    :cond_1f
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    aget-byte v6, v6, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v19, v0

    add-int/lit8 v4, v5, 0x1

    int-to-byte v0, v6

    move/from16 v20, v0

    aput-byte v20, v19, v5

    const/16 v5, 0x1000

    if-ge v11, v5, :cond_20

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPrefix:[S

    int-to-short v10, v10

    aput-short v10, v5, v11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mSuffix:[B

    int-to-byte v10, v6

    aput-byte v10, v5, v11

    add-int/lit8 v11, v11, 0x1

    and-int v5, v11, v8

    if-nez v5, :cond_20

    const/16 v5, 0x1000

    if-ge v11, v5, :cond_20

    add-int/lit8 v9, v9, 0x1

    add-int/2addr v8, v11

    :cond_20
    move v5, v4

    :goto_13
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    add-int/lit8 v4, v3, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixelStack:[B

    move-object/from16 v19, v0

    add-int/lit8 v5, v5, -0x1

    aget-byte v19, v19, v5

    aput-byte v19, v10, v3

    if-gtz v5, :cond_27

    move v3, v4

    move v10, v12

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v13

    goto/16 :goto_c

    :cond_21
    :goto_14
    if-ge v3, v14, :cond_16

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mPixels:[B

    add-int/lit8 v4, v3, 0x1

    const/4 v6, 0x0

    aput-byte v6, v5, v3

    move v3, v4

    goto :goto_14

    :pswitch_3
    const/4 v4, 0x4

    goto/16 :goto_f

    :pswitch_4
    const/4 v4, 0x2

    const/4 v5, 0x4

    goto/16 :goto_f

    :pswitch_5
    const/4 v4, 0x1

    const/4 v5, 0x2

    goto/16 :goto_f

    :cond_22
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_e

    :cond_23
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparency:Z

    if-eqz v3, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mActiveColorTable:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mTransparentColorIndex:I

    aput v2, v3, v4

    :cond_24
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    :cond_25
    :goto_15
    return-void

    :sswitch_7
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/util/GifDrawable;->mEndOfFile:Z

    goto :goto_15

    :cond_26
    move v7, v3

    goto/16 :goto_10

    :cond_27
    move v3, v4

    goto :goto_13

    :cond_28
    move v4, v10

    move v5, v6

    goto/16 :goto_12

    :cond_29
    move v5, v4

    move v4, v12

    goto/16 :goto_12

    :cond_2a
    move v2, v3

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x21 -> :sswitch_1
        0x2c -> :sswitch_6
        0x3b -> :sswitch_7
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_5
        0xf9 -> :sswitch_2
        0xfe -> :sswitch_4
        0xff -> :sswitch_3
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/util/GifDrawable;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mEndOfFile:Z

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/util/GifDrawable;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mEndOfFile:Z

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/util/GifDrawable;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/util/GifDrawable;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/util/GifDrawable;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/util/GifDrawable;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mGifImage:Lcom/google/android/apps/plus/util/GifImage;

    iget v0, v0, Lcom/google/android/apps/plus/util/GifImage;->mHeaderSize:I

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameCount:I

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDisposalMethod:I

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/util/GifDrawable;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/util/GifDrawable;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/util/GifDrawable;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mNextFrameDelay:I

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/util/GifDrawable;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/util/GifDrawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private backupFrame()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    if-nez v1, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    array-length v1, v1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    iget-object v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackup:[I

    iget-object v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    array-length v3, v3

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBackupSaved:Z

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "GifDrawable"

    const-string v2, "GifDrawable.backupFrame threw an OOME"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private readBlock()I
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v0, v1, 0xff

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    iget-object v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBlock:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    :cond_0
    return v0
.end method

.method private readShort()I
    .locals 5

    iget-object v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v0, v2, 0xff

    iget-object v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v2, v2, v3

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v2, v1, 0x8

    or-int/2addr v2, v0

    return v2
.end method

.method private skip()V
    .locals 4

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mData:[B

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    aget-byte v1, v1, v2

    and-int/lit16 v0, v1, 0xff

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mPosition:I

    if-gtz v0, :cond_0

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRecycled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrameReady:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScaleFactor:F

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScaleFactor:F

    invoke-virtual {p1, v0, v1, v2, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sScalePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScheduled:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mLastFrameTime:J

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mLastFrameTime:J

    iget-wide v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mLastFrameTime:J

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->start()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/util/GifDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 9
    .param p1    # Landroid/os/Message;

    const/4 v8, 0x1

    const/4 v2, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFrameDelay:I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mColors:[I

    iget v3, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    iget v7, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    iput-boolean v8, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrameReady:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScheduled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->invalidateSelf()V

    :cond_0
    move v2, v8

    :cond_1
    return v2
.end method

.method public final isRunning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    return v0
.end method

.method public final isValid()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mError:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mFirstFrameReady:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onBoundsChange(Landroid/graphics/Rect;)V
    .locals 3
    .param p1    # Landroid/graphics/Rect;

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScale:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mIntrinsicHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScaleFactor:F

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onRecycle()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRecycled:Z

    return-void
.end method

.method public final run()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRecycled:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mDone:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/apps/plus/util/GifDrawable;->sDecoderHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final scheduleSelf(Ljava/lang/Runnable;J)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;
    .param p2    # J

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mAnimationEnabled:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Landroid/graphics/drawable/Drawable;->scheduleSelf(Ljava/lang/Runnable;J)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mScheduled:Z

    :cond_0
    return-void
.end method

.method public final setAlpha(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public final setAnimationEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mAnimationEnabled:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mAnimationEnabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mAnimationEnabled:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->start()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->stop()V

    goto :goto_0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1    # Landroid/graphics/ColorFilter;

    return-void
.end method

.method public final setVisible(ZZ)Z
    .locals 1
    .param p1    # Z
    .param p2    # Z

    invoke-super {p0, p1, p2}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    move-result v0

    if-eqz p1, :cond_2

    if-nez v0, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->start()V

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->stop()V

    goto :goto_0
.end method

.method public final start()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mLastFrameTime:J

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->run()V

    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/util/GifDrawable;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/util/GifDrawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public final unscheduleSelf(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->unscheduleSelf(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/GifDrawable;->mRunning:Z

    return-void
.end method
