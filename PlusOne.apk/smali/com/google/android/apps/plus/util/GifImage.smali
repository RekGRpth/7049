.class public final Lcom/google/android/apps/plus/util/GifImage;
.super Ljava/lang/Object;
.source "GifImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/GifImage$GifHeaderStream;
    }
.end annotation


# static fields
.field private static final sColorTableBuffer:[B


# instance fields
.field mBackgroundColor:I

.field mBackgroundIndex:I

.field private final mData:[B

.field mError:Z

.field mGlobalColorTable:[I

.field mGlobalColorTableSize:I

.field mGlobalColorTableUsed:Z

.field mHeaderSize:I

.field private mHeight:I

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x300

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/plus/util/GifImage;->sColorTableBuffer:[B

    return-void
.end method

.method public constructor <init>([B)V
    .locals 5
    .param p1    # [B

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v3, 0x100

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTable:[I

    iput-object p1, p0, Lcom/google/android/apps/plus/util/GifImage;->mData:[B

    new-instance v0, Lcom/google/android/apps/plus/util/GifImage$GifHeaderStream;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/apps/plus/util/GifImage$GifHeaderStream;-><init>(Lcom/google/android/apps/plus/util/GifImage;[BB)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v3

    const/16 v4, 0x47

    if-ne v3, v4, :cond_1

    move v3, v2

    :goto_0
    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v3

    const/16 v4, 0x49

    if-ne v3, v4, :cond_2

    move v3, v2

    :goto_1
    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v3

    const/16 v4, 0x46

    if-ne v3, v4, :cond_3

    move v3, v2

    :goto_2
    if-nez v3, :cond_4

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mError:Z

    :cond_0
    :goto_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/GifImage$GifHeaderStream;->getPosition()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mHeaderSize:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_4
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/GifImage$GifHeaderStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_5
    return-void

    :cond_1
    move v3, v1

    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1

    :cond_3
    move v3, v1

    goto :goto_2

    :cond_4
    const-wide/16 v3, 0x3

    :try_start_2
    invoke-virtual {v0, v3, v4}, Ljava/io/InputStream;->skip(J)J

    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifImage;->readShort(Ljava/io/InputStream;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/util/GifImage;->mWidth:I

    invoke-static {v0}, Lcom/google/android/apps/plus/util/GifImage;->readShort(Ljava/io/InputStream;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/util/GifImage;->mHeight:I

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v3

    and-int/lit16 v4, v3, 0x80

    if-eqz v4, :cond_5

    move v1, v2

    :cond_5
    iput-boolean v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTableUsed:Z

    const/4 v1, 0x2

    and-int/lit8 v3, v3, 0x7

    shl-int/2addr v1, v3

    iput v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTableSize:I

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mBackgroundIndex:I

    const-wide/16 v3, 0x1

    invoke-virtual {v0, v3, v4}, Ljava/io/InputStream;->skip(J)J

    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTableUsed:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mError:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTable:[I

    iget v3, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTableSize:I

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/plus/util/GifImage;->readColorTable(Ljava/io/InputStream;[II)Z

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTable:[I

    iget v3, p0, Lcom/google/android/apps/plus/util/GifImage;->mBackgroundIndex:I

    aget v1, v1, v3

    iput v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mBackgroundColor:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/util/GifImage;->mError:Z

    goto :goto_4

    :catch_1
    move-exception v1

    goto :goto_5
.end method

.method public static isGif([B)Z
    .locals 4
    .param p0    # [B

    const/4 v0, 0x1

    const/4 v1, 0x0

    array-length v2, p0

    const/4 v3, 0x3

    if-lt v2, v3, :cond_0

    aget-byte v2, p0, v1

    const/16 v3, 0x47

    if-ne v2, v3, :cond_0

    aget-byte v2, p0, v0

    const/16 v3, 0x49

    if-ne v2, v3, :cond_0

    const/4 v2, 0x2

    aget-byte v2, p0, v2

    const/16 v3, 0x46

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static readColorTable(Ljava/io/InputStream;[II)Z
    .locals 13
    .param p0    # Ljava/io/InputStream;
    .param p1    # [I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x0

    sget-object v10, Lcom/google/android/apps/plus/util/GifImage;->sColorTableBuffer:[B

    monitor-enter v10

    mul-int/lit8 v7, p2, 0x3

    :try_start_0
    sget-object v11, Lcom/google/android/apps/plus/util/GifImage;->sColorTableBuffer:[B

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    if-ge v6, v7, :cond_0

    monitor-exit v10

    :goto_0
    return v9

    :cond_0
    const/4 v2, 0x0

    const/4 v4, 0x0

    move v5, v4

    move v3, v2

    :goto_1
    if-ge v3, p2, :cond_1

    sget-object v9, Lcom/google/android/apps/plus/util/GifImage;->sColorTableBuffer:[B

    add-int/lit8 v4, v5, 0x1

    aget-byte v9, v9, v5

    and-int/lit16 v8, v9, 0xff

    sget-object v9, Lcom/google/android/apps/plus/util/GifImage;->sColorTableBuffer:[B

    add-int/lit8 v5, v4, 0x1

    aget-byte v9, v9, v4

    and-int/lit16 v1, v9, 0xff

    sget-object v9, Lcom/google/android/apps/plus/util/GifImage;->sColorTableBuffer:[B

    add-int/lit8 v4, v5, 0x1

    aget-byte v9, v9, v5

    and-int/lit16 v0, v9, 0xff

    add-int/lit8 v2, v3, 0x1

    const/high16 v9, -0x1000000

    shl-int/lit8 v11, v8, 0x10

    or-int/2addr v9, v11

    shl-int/lit8 v11, v1, 0x8

    or-int/2addr v9, v11

    or-int/2addr v9, v0

    aput v9, p1, v3

    move v5, v4

    move v3, v2

    goto :goto_1

    :cond_1
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v9, 0x1

    goto :goto_0

    :catchall_0
    move-exception v9

    monitor-exit v10

    throw v9
.end method

.method private static readShort(Ljava/io/InputStream;)I
    .locals 2
    .param p0    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final getData()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifImage;->mData:[B

    return-object v0
.end method

.method public final getHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/GifImage;->mHeight:I

    return v0
.end method

.method public final getSizeEstimate()I
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/util/GifImage;->mData:[B

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/util/GifImage;->mGlobalColorTable:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    return v0
.end method

.method public final getWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/GifImage;->mWidth:I

    return v0
.end method
