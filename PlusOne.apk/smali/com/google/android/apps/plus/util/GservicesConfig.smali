.class public final Lcom/google/android/apps/plus/util/GservicesConfig;
.super Ljava/lang/Object;
.source "GservicesConfig.java"


# direct methods
.method public static isInstantShareEnabled(Landroid/content/Context;)Z
    .locals 4
    .param p0    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "plusone:enable_instant_share"

    invoke-static {v2, v3, v1}, Lcom/google/android/gsf/EsGservices;->getBoolean(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/util/Property;->ENABLE_INSTANT_SHARE:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
