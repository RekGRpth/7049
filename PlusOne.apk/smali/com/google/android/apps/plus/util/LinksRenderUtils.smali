.class public final Lcom/google/android/apps/plus/util/LinksRenderUtils;
.super Ljava/lang/Object;
.source "LinksRenderUtils.java"


# static fields
.field protected static sAppInviteTopAreaBackgroundPaint:Landroid/graphics/Paint;

.field protected static sDeepLinkIcon:Landroid/graphics/Bitmap;

.field protected static sDeepLinkTextPaint:Landroid/text/TextPaint;

.field protected static sDefaultTextSpacing:I

.field protected static sHorizontalSpacing:I

.field protected static sIconHorizontalSpacing:I

.field protected static sImageBorderPaint:Landroid/graphics/Paint;

.field protected static sImageHorizontalSpacing:I

.field protected static sImageMaxWidthPercentage:F

.field protected static sLinkTitleTextPaint:Landroid/text/TextPaint;

.field protected static sLinkUrlTextPaint:Landroid/text/TextPaint;

.field private static sLinksCardViewInitialized:Z

.field protected static sLinksTopAreaBackgroundPaint:Landroid/graphics/Paint;

.field protected static sMaxImageDimension:I

.field protected static final sResizePaint:Landroid/graphics/Paint;

.field protected static sTransparentOverlayPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public static createBackgroundDestRect(IIIILandroid/graphics/Rect;)V
    .locals 1
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Rect;

    const/4 v0, 0x0

    invoke-virtual {p4, v0, v0, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public static createBackgroundSourceRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 8
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Rect;

    const/4 v7, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v5, v2

    int-to-float v6, v1

    div-float v0, v5, v6

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v6

    int-to-float v6, v6

    div-float v3, v5, v6

    cmpl-float v5, v0, v3

    if-lez v5, :cond_0

    int-to-float v5, v1

    mul-float/2addr v5, v3

    float-to-int v5, v5

    sub-int v5, v2, v5

    div-int/lit8 v4, v5, 0x2

    sub-int v5, v2, v4

    invoke-virtual {p2, v4, v7, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    :goto_0
    return-void

    :cond_0
    int-to-float v5, v2

    div-float/2addr v5, v3

    float-to-int v5, v5

    sub-int v5, v1, v5

    div-int/lit8 v4, v5, 0x2

    sub-int v5, v1, v4

    invoke-virtual {p2, v7, v4, v2, v5}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public static createDeepLinkButton(Landroid/content/Context;Ljava/lang/String;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, p4, v1

    const/4 v2, 0x1

    const/4 v5, 0x1

    invoke-static {p0, v2, v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getTotalDefaultPadding(Landroid/content/Context;ZZ)I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v12

    sget-object v0, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    int-to-float v1, v12

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, v0, v1, v2}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    const/16 v0, 0x13

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkIcon:Landroid/graphics/Bitmap;

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v11, 0x0

    move-object v1, p0

    move-object/from16 v7, p5

    move v8, p2

    move/from16 v9, p3

    move-object v10, v3

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createDescription$5f444f9d(Landroid/content/Context;Ljava/lang/String;II)Landroid/text/StaticLayout;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-lez p2, :cond_0

    const/16 v0, 0x8

    invoke-static {p0, v0}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, p3, v1

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sub-int/2addr v1, v2

    invoke-static {v0, p1, v1, p2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createImageRects(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 7
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Rect;
    .param p5    # Landroid/graphics/Rect;

    sget v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    add-int/lit8 v1, v3, 0x0

    sub-int v3, p0, p1

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v2, v3, 0x0

    add-int v3, v1, p1

    add-int v4, v2, p1

    invoke-virtual {p4, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v3, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v3

    float-to-int v0, v3

    add-int v3, v1, v0

    add-int v4, v2, v0

    add-int v5, v1, p1

    sub-int/2addr v5, v0

    add-int v6, v2, p1

    sub-int/2addr v6, v0

    invoke-virtual {p5, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public static createImageSourceRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 6
    .param p0    # Landroid/graphics/Bitmap;
    .param p1    # Landroid/graphics/Rect;

    const/4 v5, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-le v2, v1, :cond_0

    sub-int v3, v2, v1

    div-int/lit8 v3, v3, 0x2

    add-int v4, v2, v1

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p1, v3, v5, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_0
    return-void

    :cond_0
    sub-int v3, v0, v1

    div-int/lit8 v3, v3, 0x2

    add-int v4, v0, v1

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {p1, v5, v3, v2, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method public static createTitle$24306a25(Ljava/lang/String;II)Landroid/text/StaticLayout;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-lez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sub-int/2addr v1, v2

    invoke-static {v0, p0, v1, p1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createUrl$54208d16(Ljava/lang/String;II)Landroid/text/StaticLayout;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # I

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-lez p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, p2, v1

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sIconHorizontalSpacing:I

    sub-int/2addr v1, v2

    invoke-static {v0, p0, v1, p1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 1
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # Landroid/graphics/Rect;
    .param p5    # Landroid/graphics/Rect;
    .param p6    # Landroid/graphics/Rect;

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, p3, p4, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sTransparentOverlayPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p4, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p1, p2, p5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    invoke-virtual {p0, p6, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method public static drawTitleDeepLinkAndUrl(Landroid/graphics/Canvas;IILandroid/text/StaticLayout;Landroid/text/StaticLayout;Lcom/google/android/apps/plus/views/ClickableButton;Landroid/text/StaticLayout;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/text/StaticLayout;
    .param p4    # Landroid/text/StaticLayout;
    .param p5    # Lcom/google/android/apps/plus/views/ClickableButton;
    .param p6    # Landroid/text/StaticLayout;
    .param p7    # Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    add-int/2addr v1, v2

    add-int/2addr p1, v1

    if-eqz p3, :cond_0

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p3, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p1

    int-to-float v1, v1

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr p2, v1

    :cond_0
    if-eqz p4, :cond_1

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p4, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p1

    int-to-float v1, v1

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr p2, v1

    :cond_1
    if-eqz p6, :cond_3

    if-eqz p7, :cond_2

    int-to-float v1, p1

    invoke-virtual {p6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    invoke-virtual {p7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p2

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p0, p7, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {p7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget v2, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sIconHorizontalSpacing:I

    add-int/2addr v1, v2

    add-int/2addr p1, v1

    :cond_2
    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p6, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p1

    int-to-float v1, v1

    neg-int v2, p2

    int-to-float v2, v2

    invoke-virtual {p0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p6}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr p2, v1

    :cond_3
    if-eqz p5, :cond_4

    sget v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDefaultTextSpacing:I

    add-int/2addr p2, v1

    invoke-virtual {p5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->left:I

    sub-int v1, p1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int v2, p2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    invoke-virtual {p5, p0}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    :cond_4
    return-void
.end method

.method public static getAppInviteTopAreaBackgroundPaint()Landroid/graphics/Paint;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sAppInviteTopAreaBackgroundPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public static getImageMaxWidthPercentage()F
    .locals 1

    sget v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageMaxWidthPercentage:F

    return v0
.end method

.method public static getMaxImageDimension()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sMaxImageDimension:I

    return v0
.end method

.method public static getTransparentOverlayPaint()Landroid/graphics/Paint;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sTransparentOverlayPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x1

    sget-boolean v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksCardViewInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v6, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksCardViewInitialized:Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_exit_to_app_20:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkIcon:Landroid/graphics/Bitmap;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sTransparentOverlayPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_links_background_tint:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinksTopAreaBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->solid_black:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sAppInviteTopAreaBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_app_invite_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_links_image_border:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_image_stroke_dimension:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_shadow_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_shadow_x:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_shadow_y:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_shadow_text:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDeepLinkTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_links_title_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_title_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_title_text_shadow_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_links_title_text_shadow_x:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_links_title_text_shadow_y:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->card_links_title_text_shadow:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkTitleTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_title_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_links_url_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_url_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_url_text_shadow_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_links_url_text_shadow_x:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_links_url_text_shadow_y:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->card_links_url_text_shadow:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sLinkUrlTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_url_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_links_image_max_width_percent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageMaxWidthPercentage:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_links_image_dimension:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sMaxImageDimension:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_links_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sHorizontalSpacing:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_links_image_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sImageHorizontalSpacing:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_links_icon_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sIconHorizontalSpacing:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/LinksRenderUtils;->sDefaultTextSpacing:I

    :cond_0
    return-void
.end method

.method public static makeLinkUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "www."

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
