.class public final Lcom/google/android/apps/plus/util/LocationUtils;
.super Ljava/lang/Object;
.source "LocationUtils.java"


# direct methods
.method public static convertLocationToPlace(Lcom/google/api/services/plusi/model/Location;)Lcom/google/api/services/plusi/model/Place;
    .locals 7
    .param p0    # Lcom/google/api/services/plusi/model/Location;

    const-wide v5, 0x416312d000000000L

    if-nez p0, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/google/api/services/plusi/model/Place;

    invoke-direct {v1}, Lcom/google/api/services/plusi/model/Place;-><init>()V

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    new-instance v2, Lcom/google/api/services/plusi/model/GeoCoordinates;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/GeoCoordinates;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Location;->latitudeE7:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/GeoCoordinates;->latitude:Ljava/lang/Double;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->geo:Lcom/google/api/services/plusi/model/GeoCoordinates;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Location;->longitudeE7:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-double v3, v3

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    iput-object v3, v2, Lcom/google/api/services/plusi/model/GeoCoordinates;->longitude:Ljava/lang/Double;

    :cond_2
    iget-object v2, p0, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v0, p0, Lcom/google/api/services/plusi/model/Location;->locationTag:Ljava/lang/String;

    :goto_1
    if-eqz v0, :cond_0

    iput-object v0, v1, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/api/services/plusi/model/Place;->description:Ljava/lang/String;

    new-instance v2, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    invoke-direct {v2}, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;-><init>()V

    iput-object v2, v1, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v2, v1, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v3, p0, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/api/services/plusi/model/Location;->bestAddress:Ljava/lang/String;

    goto :goto_1
.end method
