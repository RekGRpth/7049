.class public final Lcom/google/android/apps/plus/util/MentionTokenizer;
.super Ljava/lang/Object;
.source "MentionTokenizer.java"

# interfaces
.implements Landroid/widget/MultiAutoCompleteTextView$Tokenizer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findTokenEnd(Ljava/lang/CharSequence;II)I
    .locals 8
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # I
    .param p2    # I

    const/16 v7, 0xa

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    const/4 v5, 0x0

    move v2, p1

    :goto_0
    if-ge v2, p2, :cond_0

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    if-ne v0, v7, :cond_1

    move p2, v2

    :cond_0
    :goto_1
    return p2

    :cond_1
    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_6

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x4

    if-lt v5, v6, :cond_2

    move p2, v2

    goto :goto_1

    :cond_2
    add-int/lit8 v3, v2, 0x1

    :goto_2
    if-ge v3, v4, :cond_4

    invoke-interface {p0, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    if-ne v1, v7, :cond_3

    move p2, v2

    goto :goto_1

    :cond_3
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_4

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    if-ne v3, v4, :cond_5

    move p2, v2

    goto :goto_1

    :cond_5
    move v2, v3

    invoke-interface {p0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    :cond_6
    if-le v2, p2, :cond_8

    invoke-static {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;->isMentionTrigger(C)Z

    move-result v6

    if-eqz v6, :cond_8

    if-eqz v2, :cond_7

    add-int/lit8 v6, v2, -0x1

    invoke-interface {p0, v6}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_8

    :cond_7
    move p2, v2

    goto :goto_1

    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static isMentionTrigger(C)Z
    .locals 1
    .param p0    # C

    const/16 v0, 0x2b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x40

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final findTokenEnd(Ljava/lang/CharSequence;I)I
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I

    invoke-static {p1, p2, p2}, Lcom/google/android/apps/plus/util/MentionTokenizer;->findTokenEnd(Ljava/lang/CharSequence;II)I

    move-result v0

    return v0
.end method

.method public final findTokenStart(Ljava/lang/CharSequence;I)I
    .locals 9
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    add-int/lit8 v1, p2, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_1
    return p2

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;->isMentionTrigger(C)Z

    move-result v3

    if-eqz v3, :cond_7

    if-eqz v1, :cond_2

    add-int/lit8 v3, v1, -0x1

    invoke-interface {p1, v3}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_2
    instance-of v3, p1, Landroid/text/Spannable;

    if-eqz v3, :cond_5

    move-object v3, p1

    check-cast v3, Landroid/text/Spannable;

    const-class v6, Lcom/google/android/apps/plus/views/MentionSpan;

    invoke-interface {v3, v1, v1, v6}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/apps/plus/views/MentionSpan;

    if-eqz v3, :cond_3

    array-length v3, v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_2
    if-nez v3, :cond_0

    invoke-static {p1, v1, p2}, Lcom/google/android/apps/plus/util/MentionTokenizer;->findTokenEnd(Ljava/lang/CharSequence;II)I

    move-result v2

    :goto_3
    if-ge v2, p2, :cond_6

    invoke-interface {p1, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v3

    if-eqz v3, :cond_6

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    move-object v3, p1

    check-cast v3, Landroid/text/Spannable;

    const-class v6, Landroid/text/style/URLSpan;

    invoke-interface {v3, v1, v1, v6}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/text/style/URLSpan;

    if-eqz v3, :cond_5

    array-length v6, v3

    if-eqz v6, :cond_5

    array-length v7, v3

    move v6, v5

    :goto_4
    if-ge v6, v7, :cond_5

    aget-object v8, v3, v6

    invoke-static {v8}, Lcom/google/android/apps/plus/views/MentionSpan;->isMention(Landroid/text/style/URLSpan;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v3, v4

    goto :goto_2

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_5
    move v3, v5

    goto :goto_2

    :cond_6
    if-ne v2, p2, :cond_7

    move p2, v1

    goto :goto_1

    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public final terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 7
    .param p1    # Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v6

    if-eqz v6, :cond_0

    add-int/lit8 v0, v6, -0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v4, p1

    :goto_0
    return-object v4

    :cond_1
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_2

    new-instance v4, Landroid/text/SpannableString;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, p1

    check-cast v0, Landroid/text/Spanned;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
