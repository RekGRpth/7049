.class public final Lcom/google/android/apps/plus/util/NotificationUtils;
.super Ljava/lang/Object;
.source "NotificationUtils.java"


# static fields
.field private static final INSTANT_SHARE_NOTIFICATION_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->event_instant_share_notification:I

    sput v0, Lcom/google/android/apps/plus/util/NotificationUtils;->INSTANT_SHARE_NOTIFICATION_ID:I

    return-void
.end method

.method public static cancelInstantShareEnabled(Landroid/content/Context;)V
    .locals 3
    .param p0    # Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v1, "InstantShare"

    sget v2, Lcom/google/android/apps/plus/util/NotificationUtils;->INSTANT_SHARE_NOTIFICATION_ID:I

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    return-void
.end method

.method public static notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 1
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/app/PendingIntent;

    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/apps/plus/util/NotificationUtils;->notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V

    return-void
.end method

.method public static notifyInstantShareEnabled(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/app/PendingIntent;Z)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Z

    sget v4, Lcom/google/android/apps/plus/R$string;->event_instant_share_enabled_notification_subtitle:I

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz p3, :cond_0

    move-object v3, v2

    :goto_0
    new-instance v0, Landroid/app/Notification;

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_stat_instant_share:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v0, v4, v3, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    invoke-virtual {v0, p0, p1, v2, p2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v4, 0x22

    iput v4, v0, Landroid/app/Notification;->flags:I

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    const-string v4, "InstantShare"

    sget v5, Lcom/google/android/apps/plus/util/NotificationUtils;->INSTANT_SHARE_NOTIFICATION_ID:I

    invoke-virtual {v1, v4, v5, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method
