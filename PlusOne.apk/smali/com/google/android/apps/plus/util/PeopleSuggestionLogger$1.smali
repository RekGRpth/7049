.class final Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;
.super Ljava/lang/Object;
.source "PeopleSuggestionLogger.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->attachListener(Landroid/widget/AbsListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private oldFirstVisibleItem:I

.field private oldVisibleItemCount:I

.field final synthetic this$0:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;)V
    .locals 1

    const/4 v0, -0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->this$0:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->oldFirstVisibleItem:I

    iput v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->oldVisibleItemCount:I

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->oldFirstVisibleItem:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->oldVisibleItemCount:I

    if-eq v0, p3, :cond_1

    :cond_0
    iput p2, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->oldFirstVisibleItem:I

    iput p3, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->oldVisibleItemCount:I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->this$0:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->recordAppearedOnScreenViewsForLogging(Landroid/widget/AbsListView;)V

    :cond_1
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;->this$0:Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->recordAppearedOnScreenViewsForLogging(Landroid/widget/AbsListView;)V

    return-void
.end method
