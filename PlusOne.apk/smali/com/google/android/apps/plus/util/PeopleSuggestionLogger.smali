.class public final Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;
.super Ljava/lang/Object;
.source "PeopleSuggestionLogger.java"


# instance fields
.field private final mShownSuggestionIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    return-void
.end method

.method private recordAppearedOnScreenViewForLogging(Lcom/google/android/apps/plus/views/PeopleListRowView;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getSuggestionId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "PeopleSuggestionLogger"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSuggestionLogger"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Record the SHOW suggestion event - personId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getPersonId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", personName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getPersonName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", suggestionId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PeopleListRowView;->getSuggestionId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public final attachListener(Landroid/widget/AbsListView;)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;

    new-instance v0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger$1;-><init>(Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;)V

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method public final insertEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Map$Entry;

    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v1, "PeopleSuggestionLogger"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PeopleSuggestionLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Insert "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SHOW suggestion events into database"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;

    const-string v1, "SHOW"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;J)V

    invoke-static {p1, p2, v0}, Lcom/google/android/apps/plus/service/EsService;->insertPeopleSuggestionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/content/PeopleSuggestionEvent;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    :cond_2
    return-void
.end method

.method public final recordAppearedOnScreenViewsForLogging(Landroid/widget/AbsListView;)V
    .locals 11
    .param p1    # Landroid/widget/AbsListView;

    invoke-virtual {p1}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    invoke-virtual {p1, v2}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v8, v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;

    if-eqz v8, :cond_0

    check-cast v0, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/PeopleSuggestionsView;->getAppearedOnScreenViews(Landroid/view/View;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/PeopleListRowView;

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->recordAppearedOnScreenViewForLogging(Lcom/google/android/apps/plus/views/PeopleListRowView;)V

    goto :goto_1

    :cond_0
    instance-of v8, v0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    if-eqz v8, :cond_1

    move-object v4, v0

    check-cast v4, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getRowView()Landroid/view/View;

    move-result-object v5

    instance-of v8, v5, Lcom/google/android/apps/plus/views/PeopleListRowView;

    if-eqz v8, :cond_1

    invoke-static {v5, p1}, Lcom/google/android/apps/plus/util/ViewUtils;->hasAppearedOnScreen(Landroid/view/View;Landroid/view/View;)Z

    move-result v8

    if-eqz v8, :cond_1

    check-cast v5, Lcom/google/android/apps/plus/views/PeopleListRowView;

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->recordAppearedOnScreenViewForLogging(Lcom/google/android/apps/plus/views/PeopleListRowView;)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string v8, "PeopleSuggestionLogger"

    const/4 v9, 0x3

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v8, "PeopleSuggestionLogger"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Total number of recorded SHOW suggestion events: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void
.end method

.method public final recordAppearedOnScreenViewsForLogging(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;

    iget-object v3, p0, Lcom/google/android/apps/plus/util/PeopleSuggestionLogger;->mShownSuggestionIdMap:Ljava/util/Map;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getPersonId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getSuggestionId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "PeopleSuggestionLogger"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PeopleSuggestionLogger"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Record the SHOW suggestion event - personId: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getPersonId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", personName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getPersonName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", suggestionId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->getSuggestionId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
