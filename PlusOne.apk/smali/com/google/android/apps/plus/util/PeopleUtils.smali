.class public final Lcom/google/android/apps/plus/util/PeopleUtils;
.super Ljava/lang/Object;
.source "PeopleUtils.java"


# direct methods
.method public static in(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 8
    .param p0    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v7

    invoke-static {v7, v5}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v7

    if-nez v7, :cond_1

    :cond_0
    :goto_1
    return v6

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_3

    aget-object v1, v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v3, :cond_4

    aget-object v4, v0, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v7

    invoke-static {v7, v4}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/SquareTargetData;Lcom/google/android/apps/plus/content/SquareTargetData;)Z

    move-result v7

    if-eqz v7, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    const/4 v6, 0x1

    goto :goto_1
.end method

.method public static in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z
    .locals 6
    .param p0    # [Lcom/google/android/apps/plus/content/CircleData;
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    move-object v0, p0

    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z
    .locals 5
    .param p0    # [Lcom/google/android/apps/plus/content/PersonData;
    .param p1    # Lcom/google/android/apps/plus/content/PersonData;

    move-object v0, p0

    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->isSamePerson(Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static in([Lcom/google/android/apps/plus/content/SquareTargetData;Lcom/google/android/apps/plus/content/SquareTargetData;)Z
    .locals 6
    .param p0    # [Lcom/google/android/apps/plus/content/SquareTargetData;
    .param p1    # Lcom/google/android/apps/plus/content/SquareTargetData;

    move-object v0, p0

    array-length v2, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static isEmpty(Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static merge(Ljava/lang/Iterable;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;)",
            "Lcom/google/android/apps/plus/content/AudienceData;"
        }
    .end annotation

    if-nez p0, :cond_0

    const/4 v9, 0x0

    :goto_0
    return-object v9

    :cond_0
    new-instance v4, Ljava/util/LinkedHashSet;

    invoke-direct {v4}, Ljava/util/LinkedHashSet;-><init>()V

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6}, Ljava/util/LinkedHashSet;-><init>()V

    const/4 v7, 0x0

    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {v4, v8}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v5

    if-eqz v5, :cond_4

    invoke-static {v6, v5}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getTotalPersonCount()I

    move-result v9

    add-int/2addr v7, v9

    goto :goto_1

    :cond_5
    new-instance v9, Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {v9, v10, v11, v12, v7}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;I)V

    goto :goto_0
.end method

.method public static normalizeAudience(Lcom/google/android/apps/plus/content/AudienceData;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/AudienceData;

    if-nez p0, :cond_0

    const/4 p0, 0x0

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/util/PeopleUtils$1;

    invoke-direct {v2}, Lcom/google/android/apps/plus/util/PeopleUtils$1;-><init>()V

    invoke-static {v1, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/plus/util/PeopleUtils$2;

    invoke-direct {v2}, Lcom/google/android/apps/plus/util/PeopleUtils$2;-><init>()V

    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0
.end method
