.class public final Lcom/google/android/apps/plus/util/PlusBarUtils;
.super Ljava/lang/Object;
.source "PlusBarUtils.java"


# static fields
.field public static sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field public static sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field private static sInitialized:Z

.field public static sNotPlusOnedTextPaint:Landroid/text/TextPaint;

.field public static sPlusBarXPadding:I

.field public static sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field public static sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

.field public static sPlusOnedTextPaint:Landroid/text/TextPaint;


# direct methods
.method public static init(Landroid/content/Context;)V
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x1

    sget-boolean v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sInitialized:Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_white_pressed:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_plusone_red:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->btn_plusone_red_pressed:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_shadow_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_shadow_x:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_shadow_y:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_shadow_text:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_plus_oned_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_plus_bar_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusBarXPadding:I

    :cond_0
    return-void
.end method
