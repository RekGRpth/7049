.class public final Lcom/google/android/apps/plus/util/PlusOneViewUtils;
.super Ljava/lang/Object;
.source "PlusOneViewUtils.java"


# direct methods
.method public static shouldShowTags(Landroid/content/Context;J)Z
    .locals 10
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const/4 v6, 0x0

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    cmp-long v5, p1, v8

    if-eqz v5, :cond_3

    move v4, v7

    :goto_0
    if-eqz v4, :cond_2

    const-string v5, "window"

    invoke-virtual {p0, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v3

    if-eq v3, v7, :cond_0

    const/4 v5, 0x3

    if-ne v3, v5, :cond_4

    :cond_0
    move v2, v7

    :goto_1
    if-eqz v2, :cond_1

    const/16 v5, 0x320

    if-lt v1, v5, :cond_5

    :cond_1
    move v4, v7

    :cond_2
    :goto_2
    return v4

    :cond_3
    move v4, v6

    goto :goto_0

    :cond_4
    move v2, v6

    goto :goto_1

    :cond_5
    move v4, v6

    goto :goto_2
.end method
