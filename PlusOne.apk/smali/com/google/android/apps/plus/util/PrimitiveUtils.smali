.class public final Lcom/google/android/apps/plus/util/PrimitiveUtils;
.super Ljava/lang/Object;
.source "PrimitiveUtils.java"


# direct methods
.method public static safeBoolean(Ljava/lang/Boolean;)Z
    .locals 1
    .param p0    # Ljava/lang/Boolean;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public static safeDouble(Ljava/lang/Double;)D
    .locals 2
    .param p0    # Ljava/lang/Double;

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_0
.end method

.method public static safeInt(Ljava/lang/Integer;)I
    .locals 1
    .param p0    # Ljava/lang/Integer;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static safeLong(Ljava/lang/Long;)J
    .locals 2
    .param p0    # Ljava/lang/Long;

    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method
