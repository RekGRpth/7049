.class public final Lcom/google/android/apps/plus/util/QuickActions;
.super Ljava/lang/Object;
.source "QuickActions.java"


# direct methods
.method public static show(Landroid/view/View;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/View$OnCreateContextMenuListener;Landroid/view/MenuItem$OnMenuItemClickListener;ZZ)Landroid/app/Dialog;
    .locals 18
    .param p0    # Landroid/view/View;
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p3    # Landroid/view/View$OnCreateContextMenuListener;
    .param p4    # Landroid/view/MenuItem$OnMenuItemClickListener;
    .param p5    # Z
    .param p6    # Z

    if-nez p3, :cond_0

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_0
    const/4 v4, 0x2

    new-array v12, v4, [I

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v4, 0x0

    aget v13, v12, v4

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/WindowManager;

    invoke-interface {v14}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v10

    div-int/lit8 v4, v10, 0x2

    if-ge v13, v4, :cond_4

    const/4 v7, 0x1

    :goto_0
    if-eqz v7, :cond_5

    move v15, v13

    :goto_1
    const/4 v4, 0x2

    new-array v4, v4, [I

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v5, 0x1

    aget v17, v4, v5

    if-eqz p1, :cond_1

    const/4 v5, 0x2

    new-array v5, v5, [I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    const/4 v6, 0x1

    aget v4, v4, v6

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v17

    :cond_1
    if-eqz p6, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v4

    sub-int v17, v4, v17

    :cond_2
    new-instance v3, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p5

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;-><init>(Landroid/content/Context;Landroid/view/ContextMenu$ContextMenuInfo;Landroid/view/MenuItem$OnMenuItemClickListener;ZZZ)V

    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    invoke-interface {v0, v3, v1, v2}, Landroid/view/View$OnCreateContextMenuListener;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v11, v4, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, -0x3f400000

    mul-float/2addr v4, v11

    const/high16 v5, 0x3f000000

    add-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v16, v0

    if-eqz p6, :cond_3

    const/16 v16, 0x0

    :cond_3
    add-int v4, v17, v16

    invoke-virtual {v3, v15, v4}, Lcom/google/android/apps/plus/util/QuickActionsContextMenu;->showAnchoredAt(II)V

    return-object v3

    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v4, v13

    sub-int v15, v10, v4

    goto/16 :goto_1
.end method
