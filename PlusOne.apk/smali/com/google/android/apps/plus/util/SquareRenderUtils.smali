.class public final Lcom/google/android/apps/plus/util/SquareRenderUtils;
.super Ljava/lang/Object;
.source "SquareRenderUtils.java"


# static fields
.field protected static sContentYPadding:I

.field protected static sInvitationTextPaint:Landroid/text/TextPaint;

.field protected static sSquareBitmap:Landroid/graphics/Bitmap;

.field protected static sSquareNameTextPaint:Landroid/text/TextPaint;

.field private static sSquareRenderUtilsInitialized:Z


# direct methods
.method public static createInvitation$5f0039f6(Landroid/content/Context;III)Landroid/text/StaticLayout;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    sub-int v1, p3, p1

    sget-object v2, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    div-int v0, v1, v2

    if-lez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$string;->card_square_invite_invitation:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-static {v1, v2, p2, v0, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static createSquareName$3db85e19(Lcom/google/android/apps/plus/content/DbEmbedSquare;III)Landroid/text/StaticLayout;
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/content/DbEmbedSquare;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sub-int v1, p3, p1

    sget-object v2, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    div-int v0, v1, v2

    if-lez v0, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-static {v1, v2, p2, v0, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static drawIconAndText$65511810(Landroid/graphics/Canvas;IIILandroid/text/StaticLayout;Landroid/text/StaticLayout;)V
    .locals 7
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/text/StaticLayout;
    .param p5    # Landroid/text/StaticLayout;

    const/4 v6, 0x0

    sget-object v0, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    add-int v1, v3, v4

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    :cond_0
    if-eqz p5, :cond_1

    invoke-virtual {p5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    :cond_1
    sub-int v3, p3, v1

    div-int/lit8 v2, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int v3, p2, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    int-to-float v4, v2

    const/4 v5, 0x0

    invoke-virtual {p0, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    if-eqz p4, :cond_2

    int-to-float v3, v2

    invoke-virtual {p0, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p4, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    :cond_2
    if-eqz p5, :cond_3

    int-to-float v3, v2

    invoke-virtual {p0, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p5, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0, v6, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p5}, Landroid/text/StaticLayout;->getHeight()I

    sget v3, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    :cond_3
    return-void
.end method

.method public static getContentYPadding()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    return v0
.end method

.method public static getSquareIconBitmap()Landroid/graphics/Bitmap;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v6, 0x1

    sget-boolean v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareRenderUtilsInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v6, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareRenderUtilsInitialized:Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_square_invite_invitation_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_invitation_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_shadow_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_shadow_x:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_shadow_y:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->card_square_invite_shadow_text:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sInvitationTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_invitation_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_square_invite_name_text:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_name_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_shadow_radius:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_shadow_x:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget v4, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_shadow_y:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->card_square_invite_shadow_text:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareNameTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_square_invite_name_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_community_share:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sSquareBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_content_y_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/util/SquareRenderUtils;->sContentYPadding:I

    :cond_0
    return-void
.end method
