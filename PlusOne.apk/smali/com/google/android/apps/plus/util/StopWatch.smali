.class public final Lcom/google/android/apps/plus/util/StopWatch;
.super Ljava/lang/Object;
.source "StopWatch.java"


# instance fields
.field private mIsRunning:Z

.field private final mLabel:Ljava/lang/String;

.field private mStartNanos:J

.field private mStopNanos:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/apps/plus/util/StopWatch;->mStartNanos:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/util/StopWatch;->mStopNanos:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/StopWatch;->mIsRunning:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/util/StopWatch;->mLabel:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getElapsedMsec()J
    .locals 6

    iget-boolean v2, p0, Lcom/google/android/apps/plus/util/StopWatch;->mIsRunning:Z

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/util/StopWatch;->mStartNanos:J

    sub-long v0, v2, v4

    :goto_0
    const-wide/32 v2, 0xf4240

    div-long v2, v0, v2

    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/apps/plus/util/StopWatch;->mStartNanos:J

    sub-long v0, v2, v4

    goto :goto_0
.end method

.method public final start()Lcom/google/android/apps/plus/util/StopWatch;
    .locals 2

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/plus/util/StopWatch;->mStartNanos:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/util/StopWatch;->mIsRunning:Z

    return-object p0
.end method
