.class public final Lcom/google/android/apps/plus/util/StreamCardViewGroupData;
.super Ljava/lang/Object;
.source "StreamCardViewGroupData.java"


# static fields
.field private static sInstance:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;


# instance fields
.field public final albumBitmap:Landroid/graphics/Bitmap;

.field public final albumImagePadding:I

.field public final albumResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

.field public final authorBitmap:Landroid/graphics/Bitmap;

.field public final avatarMargin:I

.field public final avatarSize:I

.field public final background:Landroid/graphics/drawable/NinePatchDrawable;

.field public final boldSpan:Landroid/text/style/StyleSpan;

.field public final bottomBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field public final bottomBorderPadding:I

.field public final buttonBitmapTextSpacing:I

.field public final cardWithHeroTitleMaxLines:I

.field public final checkinIcon:Landroid/graphics/Bitmap;

.field public final circlesBitmap:Landroid/graphics/Bitmap;

.field public final commentAuthorBitmap:Landroid/graphics/Bitmap;

.field public final commenterAvatarSize:I

.field public final commenterAvatarSpacing:I

.field public final commenterIndicatorHeight:I

.field public final commentsBitmap:Landroid/graphics/Bitmap;

.field public final communityIcon:Landroid/graphics/Bitmap;

.field public final contentXPadding:I

.field public final contentYPadding:I

.field public final cornerIconPadding:I

.field public final defaultLinkCardContentHeight:I

.field public final defaultPadding:I

.field public final drawRect:Landroid/graphics/Rect;

.field public final googlePlayIcon:Landroid/graphics/Bitmap;

.field public final graySpamBackgroundPaint:Landroid/graphics/Paint;

.field public final graySpamMinHeight:I

.field public final graySpamWarningBitmap:Landroid/graphics/Bitmap;

.field public final graySpamXPadding:I

.field public final graySpamYPadding:I

.field public final hangoutBackgroundPaint:Landroid/graphics/Paint;

.field public final hangoutIcon:Landroid/graphics/Bitmap;

.field public final iconTextPadding:I

.field public final imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field public final interactivePostTitleMaxLines:I

.field public final leftBorderPadding:I

.field public final linkDescriptionMaxLines:I

.field public final linkIcon:Landroid/graphics/Bitmap;

.field public final linkImageBorderPaint:Landroid/graphics/Paint;

.field public final linkTitleMaxLines:I

.field public final locationBitmaps:[Landroid/graphics/Bitmap;

.field public final locationIconXPadding:I

.field public final locationIconYPaddingCheckin:I

.field public final locationIconYPaddingLocation:I

.field public final locationTextXPadding:I

.field public final locationYOffset:I

.field public final maxCommentLines:I

.field public final maxHangoutAvatarsToDisplay:I

.field public final mediaMetaDataMaxLines:I

.field public final mediaMetaDataPadding:I

.field public final mediaOverlayPaint:Landroid/graphics/Paint;

.field public final mediaPaint:Landroid/graphics/Paint;

.field public final mediaTitleMaxLines:I

.field public final minimumOverlayMediaHeight:I

.field public final panoramaBitmap:Landroid/graphics/Bitmap;

.field public final pressedStateBackground:Landroid/graphics/drawable/Drawable;

.field public final reshareBackgroundPaint:Landroid/graphics/Paint;

.field public final reshareBitmap:Landroid/graphics/Bitmap;

.field public final resizePaint:Landroid/graphics/Paint;

.field public final reviewLocationBitmap:Landroid/graphics/Bitmap;

.field public final reviewLocationIconPadding:I

.field public final reviewMaxLines:I

.field public final rightBorderPadding:I

.field public final roundedCardBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field public final screenCoordinates:[I

.field public final separatorPaint:Landroid/graphics/Paint;

.field public final squareMediumAvatarBitmap:Landroid/graphics/Bitmap;

.field public final squaresBitmap:Landroid/graphics/Bitmap;

.field public final streamCardBorderPadding:Landroid/graphics/Rect;

.field public final topBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field public final topBorderPadding:I

.field public final videoBitmap:Landroid/graphics/Bitmap;

.field public final whatsHotBitmap:Landroid/graphics/Bitmap;

.field public final whatsHotHeaderBitmap:Landroid/graphics/Bitmap;

.field public final xDoublePadding:I

.field public final xPadding:I

.field public final yDoublePadding:I

.field public final yPadding:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1    # Landroid/content/Context;

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v8}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    new-array v1, v8, [I

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    invoke-static {p1, v7}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->authorBitmap:Landroid/graphics/Bitmap;

    invoke-static {p1, v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commentAuthorBitmap:Landroid/graphics/Bitmap;

    invoke-static {p1, v6}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->squareMediumAvatarBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_comment_16:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commentsBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_reshare_16:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->overlay_lightcycle:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->panoramaBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ov_play_video_48:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->videoBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_overlay_album:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_error_white:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamWarningBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_circles_white_16:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->circlesBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_whats_hot_red_16:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->whatsHotBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_communities_green_16:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->squaresBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_whats_hot_color_24:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->whatsHotHeaderBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->album_item_placeholder:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_content_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_content_y_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_metadata_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_icon_text_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->iconTextPadding:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaOverlayPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaOverlayPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_media_overlay:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_media:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamBackgroundPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_gray_spam_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBackgroundPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_reshare_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkImageBorderPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkImageBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_links_image_border:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkImageBorderPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkImageBorderPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_links_image_stroke_dimension:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_separator_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_separator_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->background:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_card_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->roundedCardBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_cardtop_white:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_cardbtm_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->bottomBackground:Landroid/graphics/drawable/NinePatchDrawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_card_border_left_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_card_border_right_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->rightBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_card_border_top_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBorderPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_card_border_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->bottomBorderPadding:I

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    iget v3, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBorderPadding:I

    iget v4, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->rightBorderPadding:I

    iget v5, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->bottomBorderPadding:I

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->streamCardBorderPadding:Landroid/graphics/Rect;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->xPadding:I

    iget v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->xPadding:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->xDoublePadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_y_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->yPadding:I

    iget v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->yPadding:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->yDoublePadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_min_overlay_media_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->minimumOverlayMediaHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_gray_spam_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamXPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_gray_spam_y_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamYPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_gray_spam_min_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamMinHeight:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_avatar_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarMargin:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->hangoutBackgroundPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->hangoutBackgroundPaint:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_hangouts_yellow_24:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->hangoutIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$integer;->card_max_hangout_avatars:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->maxHangoutAvatarsToDisplay:I

    new-array v1, v8, [Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_metadata_location:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v6

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_location_red_12:I

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v1, v7

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationBitmaps:[Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_checkin_small:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->checkinIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_tag_y_offset:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationYOffset:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_tag_text_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationTextXPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_tag_icon_x_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconXPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_tag_icon_y_padding_checkin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconYPaddingCheckin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_tag_icon_y_padding_location:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconYPaddingLocation:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_location_red_12:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewLocationBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_place_review_location_icon_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewLocationIconPadding:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_place_review_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->google_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->googlePlayIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_card_with_hero_title_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->cardWithHeroTitleMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_link_title_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkTitleMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_link_description_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkDescriptionMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->interactive_post_title_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->interactivePostTitleMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_media_title_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaTitleMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_media_metadata_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataMaxLines:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_link_yellow_12:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->linkIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_corner_icon_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->cornerIconPadding:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_communities_green_12:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->communityIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_album_image_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumImagePadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_card_link_default_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultLinkCardContentHeight:I

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->boldSpan:Landroid/text/style/StyleSpan;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_commenter_avatar_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSpacing:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_commenter_indicator_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterIndicatorHeight:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_max_comment_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->maxCommentLines:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->riviera_button_bitmap_text_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->buttonBitmapTextSpacing:I

    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->sInstance:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->sInstance:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->sInstance:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    return-object v0
.end method
