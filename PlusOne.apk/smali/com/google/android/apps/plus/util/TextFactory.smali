.class public final Lcom/google/android/apps/plus/util/TextFactory;
.super Ljava/lang/Object;
.source "TextFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;,
        Lcom/google/android/apps/plus/util/TextFactory$TextStyle;
    }
.end annotation


# static fields
.field private static sFontSizeObserver:Landroid/database/ContentObserver;

.field private static sLightTypeFace:Landroid/graphics/Typeface;

.field private static sStyles:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/plus/util/TextFactory$TextStyle;",
            ">;"
        }
    .end annotation
.end field

.field private static sTextPaints:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/util/TextFactory;->sStyles:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/util/TextFactory;->sTextPaints:Landroid/util/SparseArray;

    const-string v0, "sans-serif-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/util/TextFactory;->sLightTypeFace:Landroid/graphics/Typeface;

    return-void
.end method

.method static synthetic access$000()Landroid/util/SparseArray;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/TextFactory;->sTextPaints:Landroid/util/SparseArray;

    return-object v0
.end method

.method public static getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # I

    sget-object v4, Lcom/google/android/apps/plus/util/TextFactory;->sFontSizeObserver:Landroid/database/ContentObserver;

    if-nez v4, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/plus/util/TextFactory$1;

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Lcom/google/android/apps/plus/util/TextFactory$1;-><init>(Landroid/os/Handler;Landroid/content/res/Resources;)V

    sput-object v5, Lcom/google/android/apps/plus/util/TextFactory;->sFontSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "font_scale"

    invoke-static {v5}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/plus/util/TextFactory;->sFontSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    const/4 v2, 0x0

    sget-object v4, Lcom/google/android/apps/plus/util/TextFactory;->sTextPaints:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;->paint:Landroid/text/TextPaint;

    :cond_1
    if-nez v2, :cond_2

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/util/TextFactory;->getTextStyle(Landroid/content/Context;I)Lcom/google/android/apps/plus/util/TextFactory$TextStyle;

    move-result-object v3

    new-instance v2, Landroid/text/TextPaint;

    invoke-direct {v2}, Landroid/text/TextPaint;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget v4, v3, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->color:I

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setColor(I)V

    iget v4, v3, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->sizeRes:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget v4, Lcom/google/android/apps/plus/R$color;->card_link:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Landroid/text/TextPaint;->linkColor:I

    iget v4, v3, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->typeface:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    sget-object v4, Lcom/google/android/apps/plus/util/TextFactory;->sTextPaints:Landroid/util/SparseArray;

    new-instance v5, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;

    invoke-direct {v5, v2, v3}, Lcom/google/android/apps/plus/util/TextFactory$TextPaintInfo;-><init>(Landroid/text/TextPaint;Lcom/google/android/apps/plus/util/TextFactory$TextStyle;)V

    invoke-virtual {v4, p1, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    return-object v2

    :pswitch_0
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0

    :pswitch_1
    sget-object v4, Lcom/google/android/apps/plus/util/TextFactory;->sLightTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static getTextStyle(Landroid/content/Context;I)Lcom/google/android/apps/plus/util/TextFactory$TextStyle;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x2

    sget-object v4, Lcom/google/android/apps/plus/util/TextFactory;->sStyles:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;-><init>(B)V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/util/TextFactory;->sStyles:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0

    :pswitch_0
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_h3:I

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_text_h3:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    :goto_1
    iput v2, v0, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->color:I

    iput v3, v0, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->sizeRes:I

    iput v1, v0, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->typeface:I

    goto :goto_0

    :pswitch_1
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_title:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_title:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_2
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_text:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_text:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_3
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_text:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_text_white:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_4
    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_default_text_size:I

    sget v1, Lcom/google/android/apps/plus/R$color;->card_gray_spam_text:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_5
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_btn:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_btn:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_6
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_btn:I

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_text_btn:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_1

    :pswitch_7
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_btn:I

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_text_btn_white:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_1

    :pswitch_8
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_body_metadata:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_body_metadata:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_9
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_metadata:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_metadata:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto :goto_1

    :pswitch_a
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_h5:I

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_text_h5:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_1

    :pswitch_b
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_body:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_body:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto/16 :goto_1

    :pswitch_c
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_body:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_body_gray:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto/16 :goto_1

    :pswitch_d
    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_h2:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_h2:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v3

    move v3, v2

    move v2, v1

    move v1, v5

    goto/16 :goto_1

    :pswitch_e
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_h2:I

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_text_h2:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_1

    :pswitch_f
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_h1:I

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_text_media_h1:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto/16 :goto_1

    :pswitch_10
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_setup_h1:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_setup_h1_blue:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v2

    move v2, v1

    move v1, v5

    goto/16 :goto_1

    :pswitch_11
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_setup_h1:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_setup_h1_cyan:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v2

    move v2, v1

    move v1, v5

    goto/16 :goto_1

    :pswitch_12
    sget v3, Lcom/google/android/apps/plus/R$dimen;->riviera_text_size_media_setup_h1:I

    sget v1, Lcom/google/android/apps/plus/R$color;->riviera_text_media_setup_h1_red:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    move v5, v2

    move v2, v1

    move v1, v5

    goto/16 :goto_1

    :pswitch_13
    sget v2, Lcom/google/android/apps/plus/R$color;->card_interactive_post_button_text:I

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_interactive_post_button_text_size:I

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_4
        :pswitch_3
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_8
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public static getTextView(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;
    .locals 5
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/util/AttributeSet;
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    new-instance v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v1, 0x9

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/util/TextFactory;->getTextStyle(Landroid/content/Context;I)Lcom/google/android/apps/plus/util/TextFactory$TextStyle;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v3, v1, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->sizeRes:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v0, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget v2, v1, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->color:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget v1, v1, Lcom/google/android/apps/plus/util/TextFactory$TextStyle;->typeface:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/google/android/apps/plus/util/TextFactory;->sLightTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
