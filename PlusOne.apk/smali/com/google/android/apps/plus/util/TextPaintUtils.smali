.class public final Lcom/google/android/apps/plus/util/TextPaintUtils;
.super Ljava/lang/Object;
.source "TextPaintUtils.java"


# static fields
.field private static sFontSizeObserver:Landroid/database/ContentObserver;

.field private static final sTextPaintsAndSizeResIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/text/TextPaint;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/util/TextPaintUtils;->sTextPaintsAndSizeResIds:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000()Ljava/util/ArrayList;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/util/TextPaintUtils;->sTextPaintsAndSizeResIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;
    .locals 1
    .param p0    # Landroid/text/TextPaint;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method

.method public static createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;
    .locals 6
    .param p0    # Landroid/text/TextPaint;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/text/Layout$Alignment;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;Landroid/text/TextUtils$TruncateAt;)Landroid/text/StaticLayout;

    move-result-object v0

    return-object v0
.end method

.method public static createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;Landroid/text/TextUtils$TruncateAt;)Landroid/text/StaticLayout;
    .locals 12
    .param p0    # Landroid/text/TextPaint;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/text/Layout$Alignment;
    .param p5    # Landroid/text/TextUtils$TruncateAt;

    const/4 v2, 0x0

    invoke-static {p2, v2}, Ljava/lang/Math;->max(II)I

    move-result p2

    if-nez p3, :cond_1

    const-string v3, ""

    :goto_0
    new-instance v2, Landroid/text/StaticLayout;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, p0

    move v5, p2

    move-object/from16 v6, p4

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object v1, v2

    :cond_0
    return-object v1

    :cond_1
    const/4 v2, 0x1

    if-ne p3, v2, :cond_2

    move-object/from16 v0, p5

    invoke-static {p1, p0, p2, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->smartEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :cond_2
    new-instance v1, Landroid/text/StaticLayout;

    const/high16 v6, 0x3f800000

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p1

    move-object v3, p0

    move v4, p2

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v2

    if-le v2, p3, :cond_0

    add-int/lit8 v2, p3, -0x2

    invoke-virtual {v1, v2}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v11

    new-instance v10, Landroid/text/SpannableStringBuilder;

    const/4 v2, 0x0

    invoke-interface {p1, v2, v11}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-direct {v10, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p1, v11, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-static {v2, p0, p2, v0}, Lcom/google/android/apps/plus/util/TextPaintUtils;->smartEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v3, v10

    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 5
    .param p0    # Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/plus/util/TextPaintUtils;->sFontSizeObserver:Landroid/database/ContentObserver;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/plus/util/TextPaintUtils$1;

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils$1;-><init>(Landroid/os/Handler;Landroid/content/res/Resources;)V

    sput-object v2, Lcom/google/android/apps/plus/util/TextPaintUtils;->sFontSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "font_scale"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/plus/util/TextPaintUtils;->sFontSizeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    :cond_0
    return-void
.end method

.method public static layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;
    .locals 13
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/graphics/Bitmap;
    .param p5    # Landroid/graphics/Rect;
    .param p6    # I
    .param p7    # Ljava/lang/CharSequence;
    .param p8    # Landroid/graphics/Point;
    .param p9    # Landroid/text/TextPaint;
    .param p10    # Z

    move-object/from16 v0, p8

    invoke-virtual {v0, p0, p1}, Landroid/graphics/Point;->set(II)V

    if-eqz p4, :cond_0

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int v11, v3, p6

    sub-int/2addr p2, v11

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v3, p0

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v4, p1

    move-object/from16 v0, p5

    invoke-virtual {v0, p0, p1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v3, v11

    move-object/from16 v0, p8

    iget v4, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    :cond_0
    const/4 v3, 0x0

    invoke-static {p2, v3}, Ljava/lang/Math;->max(II)I

    move-result p2

    if-gtz p2, :cond_1

    const-string p7, ""

    :cond_1
    if-eqz p10, :cond_3

    const/4 v3, 0x1

    move-object/from16 v0, p9

    move-object/from16 v1, p7

    invoke-static {v0, v1, p2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-eqz p4, :cond_4

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    :goto_1
    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result p3

    if-eqz p4, :cond_2

    invoke-virtual/range {p4 .. p4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v3, p3, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    div-int/lit8 v10, v3, 0x2

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v3, v10}, Landroid/graphics/Rect;->offset(II)V

    :cond_2
    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    sub-int v3, p3, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    div-int/lit8 v12, v3, 0x2

    move-object/from16 v0, p8

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p8

    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v12

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Point;->set(II)V

    return-object v2

    :cond_3
    new-instance v2, Landroid/text/StaticLayout;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p7

    move-object/from16 v4, p9

    move v5, p2

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static registerTextPaint(Landroid/text/TextPaint;I)V
    .locals 3
    .param p0    # Landroid/text/TextPaint;
    .param p1    # I

    sget-object v0, Lcom/google/android/apps/plus/util/TextPaintUtils;->sTextPaintsAndSizeResIds:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static smartEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;
    .locals 7
    .param p0    # Ljava/lang/CharSequence;
    .param p1    # Landroid/text/TextPaint;
    .param p2    # I
    .param p3    # Landroid/text/TextUtils$TruncateAt;

    const/4 v6, 0x0

    const/4 v5, -0x1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v4, 0xd

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    const/16 v4, 0xa

    invoke-virtual {v2, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ne v1, v5, :cond_0

    if-ne v0, v5, :cond_0

    move-object v3, p0

    :goto_0
    int-to-float v4, p2

    invoke-static {v3, p1, v4, p3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v4

    return-object v4

    :cond_0
    if-ne v1, v5, :cond_1

    invoke-interface {p0, v6, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :cond_1
    if-ne v0, v5, :cond_2

    invoke-interface {p0, v6, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0

    :cond_2
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-interface {p0, v6, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    goto :goto_0
.end method
