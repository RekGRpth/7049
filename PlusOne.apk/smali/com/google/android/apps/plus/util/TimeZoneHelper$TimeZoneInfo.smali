.class public final Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;
.super Ljava/lang/Object;
.source "TimeZoneHelper.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/util/TimeZoneHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TimeZoneInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private mOffset:J

.field private mPosition:I

.field private mTimeZone:Ljava/util/TimeZone;


# direct methods
.method public constructor <init>(Ljava/util/TimeZone;)V
    .locals 1
    .param p1    # Ljava/util/TimeZone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mTimeZone:Ljava/util/TimeZone;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mPosition:I

    return-void
.end method


# virtual methods
.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;

    iget-object v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mTimeZone:Ljava/util/TimeZone;

    invoke-virtual {v1}, Ljava/util/TimeZone;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final getOffset()J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mOffset:J

    return-wide v0
.end method

.method public final getPosition()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mPosition:I

    return v0
.end method

.method public final getTimeZone()Ljava/util/TimeZone;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mTimeZone:Ljava/util/TimeZone;

    return-object v0
.end method

.method public final setOffset(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mOffset:J

    return-void
.end method

.method public final setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/util/TimeZoneHelper$TimeZoneInfo;->mPosition:I

    return-void
.end method
