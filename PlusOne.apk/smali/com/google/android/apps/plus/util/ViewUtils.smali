.class public final Lcom/google/android/apps/plus/util/ViewUtils;
.super Ljava/lang/Object;
.source "ViewUtils.java"


# static fields
.field private static final sChildRect:Landroid/graphics/Rect;

.field private static final sLocation:[I

.field private static final sParentRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/util/ViewUtils;->sParentRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/util/ViewUtils;->sChildRect:Landroid/graphics/Rect;

    const/4 v0, 0x2

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    return-void
.end method

.method public static getAbsoluteRect(Landroid/graphics/Rect;Landroid/view/View;)V
    .locals 5
    .param p0    # Landroid/graphics/Rect;
    .param p1    # Landroid/view/View;

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    sget-object v0, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v0, v0, v3

    sget-object v1, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v1, v1, v4

    sget-object v2, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v2, v2, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v3, v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    return-void
.end method

.method public static hasAppearedOnScreen(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)Z
    .locals 5
    .param p0    # Landroid/graphics/Rect;
    .param p1    # Landroid/graphics/Rect;
    .param p2    # Landroid/graphics/Rect;

    const/4 v1, 0x0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1, p2}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v3, v4

    int-to-float v3, v3

    div-float v0, v2, v3

    const v2, 0x3f333333

    cmpl-float v2, v0, v2

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static hasAppearedOnScreen(Landroid/view/View;Landroid/view/View;)Z
    .locals 10
    .param p0    # Landroid/view/View;
    .param p1    # Landroid/view/View;

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v2, v3

    :cond_1
    :goto_0
    return v2

    :cond_2
    sget-object v4, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    sget-object v4, Lcom/google/android/apps/plus/util/ViewUtils;->sParentRect:Landroid/graphics/Rect;

    sget-object v5, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v5, v5, v3

    sget-object v6, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v6, v6, v2

    sget-object v7, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v7, v7, v3

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    sget-object v8, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v8, v8, v2

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v4, 0x2

    new-array v0, v4, [I

    invoke-virtual {p0, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    sget-object v4, Lcom/google/android/apps/plus/util/ViewUtils;->sChildRect:Landroid/graphics/Rect;

    sget-object v5, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v5, v5, v3

    sget-object v6, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v6, v6, v2

    sget-object v7, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v7, v7, v3

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v8

    add-int/2addr v7, v8

    sget-object v8, Lcom/google/android/apps/plus/util/ViewUtils;->sLocation:[I

    aget v8, v8, v2

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v4, Lcom/google/android/apps/plus/util/ViewUtils;->sChildRect:Landroid/graphics/Rect;

    sget-object v5, Lcom/google/android/apps/plus/util/ViewUtils;->sParentRect:Landroid/graphics/Rect;

    invoke-virtual {v4, v5}, Landroid/graphics/Rect;->intersect(Landroid/graphics/Rect;)Z

    move-result v4

    if-eqz v4, :cond_3

    sget-object v4, Lcom/google/android/apps/plus/util/ViewUtils;->sChildRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/util/ViewUtils;->sChildRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    mul-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v6

    mul-int/2addr v5, v6

    int-to-float v5, v5

    div-float v1, v4, v5

    const v4, 0x3f333333

    cmpl-float v4, v1, v4

    if-gez v4, :cond_1

    move v2, v3

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public static hierarchyToString(Landroid/view/View;)Ljava/lang/String;
    .locals 5
    .param p0    # Landroid/view/View;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/4 v1, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    :goto_0
    if-eqz v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_0

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static isViewAttached(Landroid/view/View;)Z
    .locals 1
    .param p0    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
