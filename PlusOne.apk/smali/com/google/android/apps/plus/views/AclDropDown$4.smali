.class final Lcom/google/android/apps/plus/views/AclDropDown$4;
.super Ljava/lang/Object;
.source "AclDropDown.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/AclDropDown;->init(Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;Lcom/google/android/apps/plus/views/AudienceView;Lcom/google/android/apps/plus/content/EsAccount;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/AclDropDown;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/AclDropDown;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown$4;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1    # Landroid/view/animation/Animation;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$4;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->setVisibility(I)V

    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1    # Landroid/view/animation/Animation;

    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1    # Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$4;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$100(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$4;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$100(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronDirection(Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$4;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->post_open_acl_drop_down:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->setChevronContentDescription(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
