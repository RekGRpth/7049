.class public interface abstract Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
.super Ljava/lang/Object;
.source "AclDropDown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/AclDropDown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AclDropDownHost"
.end annotation


# virtual methods
.method public abstract canPostToSquare()Z
.end method

.method public abstract getLoaderManager()Landroid/support/v4/app/LoaderManager;
.end method

.method public abstract launchAclPicker()V
.end method

.method public abstract launchSquarePicker()V
.end method

.method public abstract updatePostUI()V
.end method
