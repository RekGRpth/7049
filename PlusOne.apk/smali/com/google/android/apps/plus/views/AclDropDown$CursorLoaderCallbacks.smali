.class final Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;
.super Ljava/lang/Object;
.source "AclDropDown.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/AclDropDown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CursorLoaderCallbacks"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/AclDropDown;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/AclDropDown;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/AclDropDown;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    return-void
.end method


# virtual methods
.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->access$000(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/AclDropDown;->access$800(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v2

    const/16 v3, 0xd

    sget-object v4, Lcom/google/android/apps/plus/views/AclDropDown$CirclesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->access$000(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/phone/EsCursorLoader;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/content/EsProvider;->ACCOUNT_STATUS_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;
    invoke-static {v3}, Lcom/google/android/apps/plus/views/AclDropDown;->access$800(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/content/EsProvider;->appendAccountParameter(Landroid/net/Uri;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/AclDropDown$AccountStatusQuery;->PROJECTION:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/phone/EsCursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v4

    goto :goto_0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$000(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    if-ne v0, v6, :cond_3

    if-eqz p2, :cond_5

    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x4

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->isValidCircleType(I)Z
    invoke-static {v4, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->access$900(Lcom/google/android/apps/plus/views/AclDropDown;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v4, 0x9

    if-ne v2, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/apps/plus/R$string;->acl_public:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    new-instance v5, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v5, v1, v2, v0, v3}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->createPublicAclButton(Lcom/google/android/apps/plus/content/CircleData;)V
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1000(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_0

    :cond_1
    const/16 v4, 0x8

    if-ne v2, v4, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    new-instance v5, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v5, v1, v2, v0, v3}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->createDomainAclButton(Lcom/google/android/apps/plus/content/CircleData;)V
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1100(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x5

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/apps/plus/R$string;->acl_your_circles:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    new-instance v5, Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {v5, v1, v2, v0, v3}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->createYourCirclesAclButton(Lcom/google/android/apps/plus/content/CircleData;)V
    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1200(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/CircleData;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$000(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    if-ne v0, v7, :cond_5

    if-eqz p2, :cond_5

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserialize([B)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->isValidCustomAudience(Lcom/google/android/apps/plus/content/AudienceData;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1300(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1400(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v0

    if-nez v0, :cond_6

    move-object v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->createDefaultAclButton(Lcom/google/android/apps/plus/content/AudienceData;)V
    invoke-static {v2, v0}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1500(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/AudienceData;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # invokes: Lcom/google/android/apps/plus/views/AclDropDown;->allCirclesAreValidType(Lcom/google/android/apps/plus/content/AudienceData;)Z
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1600(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->access$100(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/AclDropDown;->access$100(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    # getter for: Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/AclDropDown;->access$100(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AudienceView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AudienceView;->isEdited()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->access$400(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_4
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbAudienceData;->deserializeList([B)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;->this$0:Lcom/google/android/apps/plus/views/AclDropDown;

    new-array v3, v7, [I

    sget v4, Lcom/google/android/apps/plus/R$id;->local_acl_button1:I

    aput v4, v3, v5

    sget v4, Lcom/google/android/apps/plus/R$id;->local_acl_button2:I

    aput v4, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/plus/views/AclDropDown;->access$1700(Lcom/google/android/apps/plus/views/AclDropDown;Ljava/util/ArrayList;Landroid/view/View;[I)V

    :cond_5
    return-void

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
