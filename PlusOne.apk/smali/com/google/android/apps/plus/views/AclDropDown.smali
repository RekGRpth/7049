.class public Lcom/google/android/apps/plus/views/AclDropDown;
.super Landroid/widget/LinearLayout;
.source "AclDropDown.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;,
        Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;,
        Lcom/google/android/apps/plus/views/AclDropDown$CirclesQuery;,
        Lcom/google/android/apps/plus/views/AclDropDown$AccountStatusQuery;
    }
.end annotation


# instance fields
.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private mCircleUsageType:I

.field private mCreateAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

.field private mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

.field private mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

.field private mDomainCircle:Lcom/google/android/apps/plus/content/CircleData;

.field private mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

.field private mHistoryAudienceArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;"
        }
    .end annotation
.end field

.field private mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

.field private mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

.field private mPublicCircle:Lcom/google/android/apps/plus/content/CircleData;

.field private mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

.field private mSlideInDown:Landroid/view/animation/Animation;

.field private mSlideOutUp:Landroid/view/animation/Animation;

.field private mSquaresAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

.field private mYourCircles:Lcom/google/android/apps/plus/content/CircleData;

.field private mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/views/AudienceView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->createPublicAclButton(Lcom/google/android/apps/plus/content/CircleData;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->createDomainAclButton(Lcom/google/android/apps/plus/content/CircleData;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->createYourCirclesAclButton(Lcom/google/android/apps/plus/content/CircleData;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCustomAudience(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 4
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsAccountsData;->compareAudiences(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic access$1500(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->createDefaultAclButton(Lcom/google/android/apps/plus/content/AudienceData;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->allCirclesAreValidType(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/google/android/apps/plus/views/AclDropDown;Ljava/util/ArrayList;Landroid/view/View;[I)V
    .locals 12
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Ljava/util/ArrayList;
    .param p2    # Landroid/view/View;
    .param p3    # [I

    const/4 v9, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    if-nez p1, :cond_3

    move v2, v3

    :goto_0
    array-length v0, p3

    new-array v0, v0, [Lcom/google/android/apps/plus/views/PostAclButtonView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v0, Ljava/util/ArrayList;

    array-length v1, p3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    move v1, v3

    move v4, v3

    :goto_1
    array-length v0, p3

    if-ge v4, v0, :cond_0

    aget v0, p3, v4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PostAclButtonView;

    move v5, v1

    :cond_2
    if-ge v5, v2, :cond_9

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/AudienceData;

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v1, v6}, Lcom/google/android/apps/plus/content/EsAccountsData;->compareAudiences(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCustomAudience(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v8, v1

    move v1, v5

    :goto_2
    if-nez v8, :cond_4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    aput-object v9, v5, v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v8}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v5

    if-lez v5, :cond_5

    const/4 v5, 0x1

    :goto_4
    iget-object v6, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    aput-object v0, v6, v4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    aget-object v10, v0, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v8, v0}, Lcom/google/android/apps/plus/content/AudienceData;->toNameList(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v11

    if-eqz v5, :cond_6

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_nav_communities:I

    move v7, v0

    :goto_5
    if-eqz v5, :cond_7

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_communities_grey:I

    move v6, v0

    :goto_6
    if-eqz v5, :cond_8

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_done_save_ok_green:I

    :goto_7
    invoke-virtual {v10, v11, v7, v6, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;III)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    aget-object v0, v0, v4

    new-instance v5, Lcom/google/android/apps/plus/views/AclDropDown$9;

    invoke-direct {v5, p0, v8}, Lcom/google/android/apps/plus/views/AclDropDown$9;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    aget-object v0, v0, v4

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    move v5, v3

    goto :goto_4

    :cond_6
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_person_active:I

    move v7, v0

    goto :goto_5

    :cond_7
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_acl_custom_inactive:I

    move v6, v0

    goto :goto_6

    :cond_8
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_done_save_ok_blue:I

    goto :goto_7

    :cond_9
    move-object v8, v9

    move v1, v5

    goto :goto_2
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/CircleData;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicCircle:Lcom/google/android/apps/plus/content/CircleData;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/AclDropDown;Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/AudienceView;->replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/CircleData;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainCircle:Lcom/google/android/apps/plus/content/CircleData;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/CircleData;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCircles:Lcom/google/android/apps/plus/content/CircleData;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/AclDropDown;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/views/AclDropDown;I)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AclDropDown;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCircleType(I)Z

    move-result v0

    return v0
.end method

.method private allCirclesAreValidType(Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCircleType(I)Z

    move-result v4

    if-nez v4, :cond_0

    const/4 v4, 0x0

    :goto_1
    return v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    goto :goto_1
.end method

.method private createDefaultAclButton(Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v4, 0x0

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    sget v3, Lcom/google/android/apps/plus/R$id;->default_acl_button:I

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AclDropDown;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCustomAudience(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v3

    if-nez v3, :cond_0

    iput-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v1, 0x1

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/apps/plus/content/AudienceData;->toNameList(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    if-eqz v1, :cond_2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_nav_communities:I

    move v4, v3

    :goto_2
    if-eqz v1, :cond_3

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_communities_grey:I

    :goto_3
    sget v7, Lcom/google/android/apps/plus/R$drawable;->ic_done_save_ok_blue:I

    invoke-virtual {v5, v6, v4, v3, v7}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;III)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v4, Lcom/google/android/apps/plus/views/AclDropDown$8;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/views/AclDropDown$8;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_person_active:I

    move v4, v3

    goto :goto_2

    :cond_3
    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_acl_custom_inactive:I

    goto :goto_3
.end method

.method private createDomainAclButton(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainCircle:Lcom/google/android/apps/plus/content/CircleData;

    sget v1, Lcom/google/android/apps/plus/R$id;->domain_acl_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_acl_domain_active:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_acl_domain_inactive:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_done_save_ok_green:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;III)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v2, Lcom/google/android/apps/plus/views/AclDropDown$6;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/AclDropDown$6;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    goto :goto_0
.end method

.method private createPublicAclButton(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicCircle:Lcom/google/android/apps/plus/content/CircleData;

    sget v1, Lcom/google/android/apps/plus/R$id;->public_acl_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_public_active:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_public:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_done_save_ok_green:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;III)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v2, Lcom/google/android/apps/plus/views/AclDropDown$5;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/AclDropDown$5;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    goto :goto_0
.end method

.method private createYourCirclesAclButton(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCircles:Lcom/google/android/apps/plus/content/CircleData;

    sget v1, Lcom/google/android/apps/plus/R$id;->your_circles_acl_button:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AclDropDown;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-nez p1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_circles_active:I

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_circles:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_done_save_ok_blue:I

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;III)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v2, Lcom/google/android/apps/plus/views/AclDropDown$7;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/AclDropDown$7;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static isAudienceCircle(Lcom/google/android/apps/plus/content/AudienceData;I)Z
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/content/AudienceData;
    .param p1    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v2

    if-ne v2, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isValidCircleType(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCircleUsageType:I

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    const/16 v2, 0x9

    if-eq p1, v2, :cond_1

    const/16 v2, 0x8

    if-ne p1, v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    :sswitch_1
    if-eq p1, v0, :cond_0

    const/4 v2, 0x5

    if-eq p1, v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xf -> :sswitch_1
    .end sparse-switch
.end method

.method private isValidCustomAudience(Lcom/google/android/apps/plus/content/AudienceData;)Z
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v2

    if-nez v3, :cond_2

    if-nez v2, :cond_2

    if-ne v0, v5, :cond_2

    invoke-virtual {p1, v4}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v1

    const/4 v6, 0x5

    if-eq v1, v6, :cond_0

    const/16 v6, 0x8

    if-eq v1, v6, :cond_0

    const/16 v6, 0x9

    if-eq v1, v6, :cond_0

    :cond_2
    if-lez v2, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;->canPostToSquare()Z

    move-result v6

    if-eqz v6, :cond_0

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AclDropDown;->allCirclesAreValidType(Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v5

    goto :goto_0
.end method


# virtual methods
.method public final hideAclOverlay()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideOutUp:Landroid/view/animation/Animation;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AclDropDown;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    return-void
.end method

.method public final init(Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;Lcom/google/android/apps/plus/views/AudienceView;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 11
    .param p1    # Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;
    .param p2    # Lcom/google/android/apps/plus/views/AudienceView;
    .param p3    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p4    # I

    const-wide/16 v9, 0xfa

    const/4 v8, 0x0

    const/4 v7, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput p4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCircleUsageType:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$layout;->acl_dropdown:I

    invoke-virtual {v2, v3, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->addView(Landroid/view/View;)V

    const/16 v2, 0x9

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCircleType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicCircle:Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->createPublicAclButton(Lcom/google/android/apps/plus/content/CircleData;)V

    :cond_0
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCircleType(I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCircles:Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->createYourCirclesAclButton(Lcom/google/android/apps/plus/content/CircleData;)V

    :cond_1
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->isValidCircleType(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainCircle:Lcom/google/android/apps/plus/content/CircleData;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->createDomainAclButton(Lcom/google/android/apps/plus/content/CircleData;)V

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->createDefaultAclButton(Lcom/google/android/apps/plus/content/AudienceData;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->create_acl_button:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PostAclButtonView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCreateAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCreateAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    sget v3, Lcom/google/android/apps/plus/R$string;->post_create_custom_acl:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_right:I

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCreateAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCreateAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v3, Lcom/google/android/apps/plus/views/AclDropDown$1;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/views/AclDropDown$1;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mCreateAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v2, v7}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;->canPostToSquare()Z

    move-result v2

    if-eqz v2, :cond_3

    sget v2, Lcom/google/android/apps/plus/R$id;->squares_acl_button:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/AclDropDown;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/PostAclButtonView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSquaresAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSquaresAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    sget v3, Lcom/google/android/apps/plus/R$string;->square_member_item_text_acl:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_communities_grey:I

    sget v5, Lcom/google/android/apps/plus/R$drawable;->ic_communities_grey:I

    sget v6, Lcom/google/android/apps/plus/R$drawable;->ic_right:I

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;III)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSquaresAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSquaresAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    new-instance v3, Lcom/google/android/apps/plus/views/AclDropDown$2;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/views/AclDropDown$2;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSquaresAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v2, v7}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setVisibility(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$anim;->slide_in_down_self:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideInDown:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideInDown:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$anim;->decelerate_interpolator:I

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideInDown:Landroid/view/animation/Animation;

    invoke-virtual {v2, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$anim;->slide_out_up_self:I

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideOutUp:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideOutUp:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$anim;->accelerate_interpolator:I

    invoke-virtual {v2, v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideOutUp:Landroid/view/animation/Animation;

    invoke-virtual {v2, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideInDown:Landroid/view/animation/Animation;

    new-instance v3, Lcom/google/android/apps/plus/views/AclDropDown$3;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/views/AclDropDown$3;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideOutUp:Landroid/view/animation/Animation;

    new-instance v3, Lcom/google/android/apps/plus/views/AclDropDown$4;

    invoke-direct {v3, p0}, Lcom/google/android/apps/plus/views/AclDropDown$4;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSavedDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    const/4 v2, 0x2

    new-instance v3, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;

    invoke-direct {v3, p0, v7}, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;B)V

    invoke-virtual {v0, v2, v8, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicCircle:Lcom/google/android/apps/plus/content/CircleData;

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainCircle:Lcom/google/android/apps/plus/content/CircleData;

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCircles:Lcom/google/android/apps/plus/content/CircleData;

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHost:Lcom/google/android/apps/plus/views/AclDropDown$AclDropDownHost;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;

    invoke-direct {v3, p0, v7}, Lcom/google/android/apps/plus/views/AclDropDown$CursorLoaderCallbacks;-><init>(Lcom/google/android/apps/plus/views/AclDropDown;B)V

    invoke-virtual {v0, v2, v8, v3}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_5
    return-void
.end method

.method public final showAclOverlay()V
    .locals 6

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v4

    const/16 v5, 0x9

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->isAudienceCircle(Lcom/google/android/apps/plus/content/AudienceData;I)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v4

    const/16 v5, 0x8

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->isAudienceCircle(Lcom/google/android/apps/plus/content/AudienceData;I)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v4

    const/4 v5, 0x5

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/AclDropDown;->isAudienceCircle(Lcom/google/android/apps/plus/content/AudienceData;I)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAudience:Lcom/google/android/apps/plus/content/AudienceData;

    invoke-static {v4, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->compareAudiences(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    :cond_3
    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    if-eqz v4, :cond_a

    const/4 v3, 0x0

    :goto_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    array-length v4, v4

    if-ge v3, v4, :cond_a

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAclButtonArray:[Lcom/google/android/apps/plus/views/PostAclButtonView;

    aget-object v1, v4, v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mHistoryAudienceArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz v1, :cond_4

    if-eqz v0, :cond_9

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/content/EsAccountsData;->compareAudiences(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v4

    if-eqz v4, :cond_9

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setActive()V

    :cond_4
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mPublicAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setInactive()V

    goto :goto_0

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDomainAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setInactive()V

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mYourCirclesAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setInactive()V

    goto :goto_2

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mDefaultAclButton:Lcom/google/android/apps/plus/views/PostAclButtonView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setInactive()V

    goto :goto_3

    :cond_9
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setInactive()V

    goto :goto_5

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AclDropDown;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_b

    invoke-static {p0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AclDropDown;->mSlideInDown:Landroid/view/animation/Animation;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/AclDropDown;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_b
    return-void
.end method
