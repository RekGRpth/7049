.class public Lcom/google/android/apps/plus/views/AlbumCardViewGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "AlbumCardViewGroup.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected mAlbumAdapter:Landroid/widget/ListAdapter;

.field protected mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field protected mAlbumNameLayout:Landroid/text/StaticLayout;

.field protected mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

.field protected mHeroTop:I

.field protected mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

.field protected mMediaCountLayout:Landroid/text/StaticLayout;

.field protected mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

.field protected mRequestedHeights:[I

.field protected mRequestedWidths:[I

.field protected mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

.field protected mScrollViewHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeroTop:I

    return-void
.end method

.method private goToPhotoOneUp(I)V
    .locals 8
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v7

    if-eqz v7, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v0, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v3, v3, p1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v4

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mIsSquarePost:Z

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;->onMediaClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;ZLcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected final createHero(III)I
    .locals 14
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move/from16 v0, p2

    iput v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeroTop:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    iget v4, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    move/from16 v0, p3

    invoke-direct {v2, v0, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ColumnGridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    add-int p2, p2, v1

    move/from16 v3, p2

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, p2, v1

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v9, p3, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getAlbumName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v13

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaTitleMaxLines:I

    invoke-static {v13, v7, v9, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v1, v2

    add-int p2, p2, v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v11

    sget v1, Lcom/google/android/apps/plus/R$string;->riviera_album_media_count:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getReportedMediaCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v11, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    const/4 v1, 0x2

    invoke-static {v11, v1}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v13

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataMaxLines:I

    invoke-static {v13, v12, v9, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaCountLayout:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaCountLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v1, v2

    add-int p2, p2, v1

    move/from16 v10, p2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v8, 0x1

    :goto_0
    if-eqz v8, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/views/ClickableRect;

    sub-int v5, v10, v3

    move v2, p1

    move/from16 v4, p3

    move-object v6, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_1
    return p2

    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    add-int/2addr p3, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    add-int v1, p2, p4

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, p3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaCountLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v0, p2, p3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableRect;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->isPressed()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p2, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    :cond_3
    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaCountLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaCountLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    return p3
.end method

.method public final getOneUpIntent(Z)Landroid/content/Intent;
    .locals 6
    .param p1    # Z

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getOneUpIntent(Z)Landroid/content/Intent;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v3, v5}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    array-length v3, v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    aget v3, v3, v5

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    array-length v3, v3

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    aget v3, v3, v5

    if-lez v3, :cond_2

    const-string v3, "photo_width"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "photo_height"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    aget v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    array-length v3, v3

    if-lez v3, :cond_3

    const-string v3, "photo_ref"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v4, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "is_album"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "album_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 26
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v2, 0x1e

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getMediaCount()I

    move-result v17

    move/from16 v0, v17

    new-array v2, v0, [Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    move/from16 v0, v17

    new-array v2, v0, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    move/from16 v0, v17

    new-array v2, v0, [I

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    const/high16 v20, -0x80000000

    const/4 v12, 0x0

    :goto_0
    move/from16 v0, v17

    if-ge v12, v0, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2, v12}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v11

    move/from16 v0, v20

    if-le v11, v0, :cond_0

    move/from16 v20, v11

    :cond_0
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamHeight:I

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v14

    move/from16 v0, v20

    if-le v0, v14, :cond_3

    const/high16 v2, 0x3f800000

    int-to-float v3, v14

    mul-float/2addr v2, v3

    move/from16 v0, v20

    int-to-float v3, v0

    div-float v18, v2, v3

    :goto_1
    const/16 v21, 0x0

    const/high16 v23, -0x80000000

    const/16 v22, 0x0

    const/4 v12, 0x0

    :goto_2
    move/from16 v0, v17

    if-ge v12, v0, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2, v12}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v18

    float-to-int v0, v2

    move/from16 v24, v0

    add-int v21, v21, v24

    move/from16 v0, v24

    move/from16 v1, v23

    if-le v0, v1, :cond_2

    move/from16 v23, v24

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v18

    float-to-int v0, v2

    move/from16 v22, v0

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    move-object/from16 v25, v0

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v4

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getVideoUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    :goto_3
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getMediaType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    aput-object v2, v25, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_3
    const/high16 v18, 0x3f800000

    goto :goto_1

    :cond_4
    const/4 v7, 0x0

    goto :goto_3

    :cond_5
    const v2, 0x3f666666

    move/from16 v0, v21

    int-to-float v3, v0

    mul-float/2addr v2, v3

    float-to-int v2, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->getNumberOfColumnsForWidth(I)I

    move-result v2

    move/from16 v0, p3

    move/from16 v1, v17

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mSpan:I

    const v2, 0x3f666666

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mSpan:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->getAvailableWidth(Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v15, v2

    move/from16 v0, v23

    if-le v0, v15, :cond_6

    const/high16 v2, 0x3f800000

    int-to-float v3, v15

    mul-float/2addr v2, v3

    move/from16 v0, v23

    int-to-float v3, v0

    div-float v19, v2, v3

    move/from16 v0, v22

    int-to-float v2, v0

    mul-float v2, v2, v19

    float-to-int v0, v2

    move/from16 v22, v0

    :cond_6
    const/4 v12, 0x0

    :goto_4
    move/from16 v0, v17

    if-ge v12, v0, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    invoke-virtual {v2, v12}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v18

    float-to-int v0, v2

    move/from16 v24, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v2

    int-to-float v2, v2

    mul-float v2, v2, v18

    float-to-int v11, v2

    move/from16 v0, v22

    if-gt v11, v0, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    aput v24, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    aput v11, v2, v12

    :goto_5
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    const/high16 v3, 0x3f800000

    move/from16 v0, v24

    int-to-float v4, v0

    mul-float/2addr v3, v4

    move/from16 v0, v22

    int-to-float v4, v0

    mul-float/2addr v3, v4

    int-to-float v4, v11

    div-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    aput v22, v2, v12

    goto :goto_5

    :cond_8
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    new-instance v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$1;-><init>(Lcom/google/android/apps/plus/views/AlbumCardViewGroup;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumAdapter:Landroid/widget/ListAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v9

    move/from16 v0, v17

    new-array v2, v0, [Lcom/google/android/apps/plus/views/ImageResourceView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v12, 0x0

    :goto_6
    move/from16 v0, v17

    if-ge v12, v0, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v3, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {v3, v9}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    aput-object v3, v2, v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v3, v2, v12

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumImagePadding:I

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumImagePadding:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v12, v2, :cond_9

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumImagePadding:I

    :goto_7
    sget-object v6, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumImagePadding:I

    invoke-virtual {v3, v4, v5, v2, v6}, Lcom/google/android/apps/plus/views/ImageResourceView;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    const/high16 v3, 0x3f000000

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setZoomBias(F)V

    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setFadeIn(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setClickable(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    sget-object v3, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumResourceLoadingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceLoadingDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setTag(Ljava/lang/Object;)V

    new-instance v13, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    aget v3, v3, v12

    invoke-direct {v13, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    invoke-virtual {v2, v13}, Lcom/google/android/apps/plus/views/ImageResourceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_6

    :cond_9
    const/4 v2, 0x0

    goto :goto_7

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v12

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setScaleMode(I)V

    goto :goto_8

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->removeView(Landroid/view/View;)V

    :cond_c
    new-instance v2, Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-direct {v2, v9}, Lcom/google/android/apps/plus/views/ColumnGridView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setColumnCount(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setOrientation(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget v3, Lcom/google/android/apps/plus/R$color;->riviera_album_background:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setBackgroundResource(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumAdapter:Landroid/widget/ListAdapter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    new-instance v3, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup$2;-><init>(Lcom/google/android/apps/plus/views/AlbumCardViewGroup;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->setRecyclerListener(Lcom/google/android/apps/plus/views/ColumnGridView$RecyclerListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->addView(Landroid/view/View;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->goToPhotoOneUp(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v2, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->goToPhotoOneUp(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/high16 v4, 0x40000000

    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onLayout(ZIIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeroTop:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mWidthDimension:I

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sub-int/2addr v1, v2

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->rightBorderPadding:I

    sub-int v0, v1, v2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeightDimension:I

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/ColumnGridView;->measure(II)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    sget-object v2, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    iget v3, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeroTop:I

    sget-object v4, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    add-int/2addr v4, v0

    iget v5, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeroTop:I

    iget v6, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/apps/plus/views/ColumnGridView;->layout(IIII)V

    :cond_0
    return-void
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onRecycle()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mDbEmbedAlbum:Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollView:Lcom/google/android/apps/plus/views/ColumnGridView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumAdapter:Landroid/widget/ListAdapter;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mImageResourceViews:[Lcom/google/android/apps/plus/views/ImageResourceView;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaRefs:[Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedWidths:[I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mRequestedHeights:[I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameClickRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mAlbumNameLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mMediaCountLayout:Landroid/text/StaticLayout;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mHeroTop:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/AlbumCardViewGroup;->mScrollViewHeight:I

    return-void
.end method
