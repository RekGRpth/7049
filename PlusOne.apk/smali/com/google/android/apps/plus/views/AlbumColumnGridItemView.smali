.class public Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;
.super Lcom/google/android/apps/plus/views/ImageResourceView;
.source "AlbumColumnGridItemView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;


# static fields
.field private static sCommentCountPaint:Landroid/text/TextPaint;

.field private static sCommentImage:Landroid/graphics/Bitmap;

.field private static sDisabledArea:Landroid/graphics/Rect;

.field private static sDisabledPaint:Landroid/graphics/Paint;

.field private static sInfoHeight:I

.field private static sInfoInnerPadding:I

.field private static sInfoLeftMargin:I

.field private static sInfoPaint:Landroid/graphics/Paint;

.field private static sInfoRightMargin:I

.field private static sInitialized:Z

.field private static sNotifyImage:Landroid/graphics/Bitmap;

.field private static sNotifyRightMargin:I

.field private static sNotifyTopMargin:I

.field private static sPlusOneCountPaint:Landroid/text/TextPaint;

.field private static sPlusOneImage:Landroid/graphics/Bitmap;


# instance fields
.field private mCommentCount:Ljava/lang/CharSequence;

.field private mNotify:Z

.field private mPlusOneCount:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setSizeCategory(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setReleaseImageWhenPaused(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->setDefaultIconEnabled(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInitialized:Z

    if-nez v1, :cond_0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->album_comment_count_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->album_comment_count_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->album_comment_count_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->album_plusone_count_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->album_plusone_count_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->album_plusone_count_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->album_info_background_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sDisabledPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->album_disabled_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sDisabledPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sDisabledArea:Landroid/graphics/Rect;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->album_info_inner_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoInnerPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->album_info_right_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoRightMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->album_info_left_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoLeftMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->album_info_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->album_notification_right_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyRightMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->album_notification_top_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyTopMargin:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->photo_plusone:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneImage:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->photo_comment:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentImage:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->tag:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyImage:Landroid/graphics/Bitmap;

    sput-boolean v3, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInitialized:Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v10

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v10}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->hasImage()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sDisabledArea:Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sDisabledArea:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sDisabledPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mNotify:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyRightMargin:I

    sub-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v8, v0, v1

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyImage:Landroid/graphics/Bitmap;

    int-to-float v1, v8

    sget v2, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sNotifyTopMargin:I

    int-to-float v2, v2

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getHeight()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoHeight:I

    sub-int v12, v0, v1

    const/4 v1, 0x0

    int-to-float v2, v12

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    sget v11, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoLeftMargin:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoHeight:I

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v12

    int-to-float v9, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float v7, v0, v1

    int-to-float v0, v12

    sget v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoHeight:I

    int-to-float v1, v1

    sub-float/2addr v1, v7

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float v5, v0, v1

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneImage:Landroid/graphics/Bitmap;

    int-to-float v1, v11

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoInnerPadding:I

    add-int/2addr v0, v1

    add-int/2addr v11, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-float v4, v11

    sget-object v6, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getWidth()I

    move-result v11

    sget v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoRightMargin:I

    sub-int/2addr v11, v0

    int-to-float v0, v11

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v11, v0

    sget v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoInnerPadding:I

    sub-int/2addr v11, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int/2addr v11, v0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoHeight:I

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentImage:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v12

    int-to-float v9, v0

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v0}, Landroid/text/TextPaint;->descent()F

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float v7, v0, v1

    int-to-float v0, v12

    sget v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoHeight:I

    int-to-float v1, v1

    sub-float/2addr v1, v7

    const/high16 v2, 0x40000000

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float v5, v0, v1

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentImage:Landroid/graphics/Bitmap;

    int-to-float v1, v11

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v9, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sInfoInnerPadding:I

    add-int/2addr v0, v1

    add-int/2addr v11, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-float v4, v11

    sget-object v6, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->sCommentCountPaint:Landroid/text/TextPaint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public setCommentCount(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x63

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mCommentCount:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public setNotification(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mNotify:Z

    return-void
.end method

.method public setPlusOneCount(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x63

    if-le v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->mPlusOneCount:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final shouldHighlightOnPress()Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AlbumColumnGridItemView;->isEnabled()Z

    move-result v0

    return v0
.end method
