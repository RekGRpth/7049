.class public Lcom/google/android/apps/plus/views/AnimatedThemeView;
.super Landroid/view/SurfaceView;
.source "AnimatedThemeView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# static fields
.field private static sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;


# instance fields
.field private mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

.field private mUrl:Ljava/lang/String;

.field private mVideoResource:Lcom/google/android/apps/plus/service/Resource;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;-><init>(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;-><init>(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView$1;-><init>(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/AnimatedThemeView;)Landroid/view/SurfaceHolder;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/AnimatedThemeView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/views/AnimatedThemeView;Landroid/view/SurfaceHolder;)Landroid/view/SurfaceHolder;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AnimatedThemeView;
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->release()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/AnimatedThemeView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/AnimatedThemeView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->openVideo()V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolderCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    return-void
.end method

.method private openVideo()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->release()V

    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->release()V

    goto :goto_0
.end method

.method private release()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    :cond_0
    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->sResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v2, 0x6

    const/16 v3, 0x22

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->unbindResources()V

    return-void
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getCacheFileName()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mUrl:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->openVideo()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->invalidate()V

    :cond_0
    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer$OnErrorListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    :cond_0
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 2
    .param p1    # Landroid/media/MediaPlayer$OnPreparedListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    :cond_0
    return-void
.end method

.method public setVideoUrl(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method

.method public final startPlayback()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->openVideo()V

    return-void
.end method

.method public final stopPlayback()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/AnimatedThemeView;->release()V

    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AnimatedThemeView;->mVideoResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
