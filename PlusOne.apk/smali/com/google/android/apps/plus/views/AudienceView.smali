.class public Lcom/google/android/apps/plus/views/AudienceView;
.super Landroid/widget/FrameLayout;
.source "AudienceView.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/AudienceView$SavedState;,
        Lcom/google/android/apps/plus/views/AudienceView$CirclesQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field protected mAudienceChangedCallback:Ljava/lang/Runnable;

.field protected final mCanRemoveChips:Z

.field protected mChipContainer:Landroid/view/ViewGroup;

.field protected final mChips:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/content/AudienceData;",
            ">;"
        }
    .end annotation
.end field

.field protected mEdited:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/views/AudienceView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # Z

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->init()V

    iput-boolean p4, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    return-void
.end method

.method private getCloseIcon()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_acl_x:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 17
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    new-instance v4, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-direct {v4, v15}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getAudience()Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v7

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_7

    const/4 v8, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v12

    :goto_0
    if-ge v8, v12, :cond_1

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/apps/plus/util/PeopleUtils;->in(Lcom/google/android/apps/plus/content/AudienceData;Lcom/google/android/apps/plus/content/AudienceData;)Z

    move-result v15

    if-eqz v15, :cond_0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v15, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v1

    array-length v11, v1

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v11, :cond_3

    aget-object v2, v1, v9

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z

    move-result v15

    if-nez v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v16, Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v1

    array-length v11, v1

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v11, :cond_5

    aget-object v14, v1, v9

    invoke-static {v7, v14}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v15

    if-nez v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v16, Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, v16

    invoke-direct {v0, v14}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargets()[Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v1

    array-length v11, v1

    const/4 v9, 0x0

    :goto_3
    if-ge v9, v11, :cond_7

    aget-object v13, v1, v9

    invoke-static {v6, v13}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/SquareTargetData;Lcom/google/android/apps/plus/content/SquareTargetData;)Z

    move-result v15

    if-nez v15, :cond_6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v16, Lcom/google/android/apps/plus/content/AudienceData;

    move-object/from16 v0, v16

    invoke-direct {v0, v13}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/SquareTargetData;)V

    invoke-virtual/range {v15 .. v16}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    return-void
.end method


# virtual methods
.method protected addChip(I)V
    .locals 2
    .param p1    # I

    sget v1, Lcom/google/android/apps/plus/R$layout;->people_audience_view_chip:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/AudienceView;->inflate(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public final addCircle(Lcom/google/android/apps/plus/content/CircleData;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/content/CircleData;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PeopleUtils;->merge(Ljava/lang/Iterable;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/CircleData;Lcom/google/android/apps/plus/content/CircleData;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CIRCLE_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v1, v3, v4, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v4, p1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_0
.end method

.method public final addPerson(Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/content/PersonData;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/google/android/apps/plus/util/PeopleUtils;->merge(Ljava/lang/Iterable;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/google/android/apps/plus/util/PeopleUtils;->in([Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_PERSON_ADDED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v1}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    new-instance v4, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v4, p1}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/PersonData;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_0
.end method

.method public final getAudience()Lcom/google/android/apps/plus/content/AudienceData;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/PeopleUtils;->merge(Ljava/lang/Iterable;)Lcom/google/android/apps/plus/content/AudienceData;

    move-result-object v0

    return-object v0
.end method

.method protected getChipCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    return v0
.end method

.method protected final inflate(I)Landroid/view/View;
    .locals 2
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected init()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->audience_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AudienceView;->inflate(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AudienceView;->addView(Landroid/view/View;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->people_audience_view_chip_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/AudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    return-void
.end method

.method public final initLoaders(Landroid/support/v4/app/LoaderManager;)V
    .locals 2
    .param p1    # Landroid/support/v4/app/LoaderManager;

    sget v0, Lcom/google/android/apps/plus/R$id;->audience_circle_name_loader_id:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public final isEdited()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/analytics/OzViews;->getViewForLogging(Landroid/content/Context;)Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    sget-object v4, Lcom/google/android/apps/plus/analytics/OzActions;->PLATFORM_AUDIENCE_VIEW_CLICKED:Lcom/google/android/apps/plus/analytics/OzActions;

    invoke-static {v0, v3, v4, v2}, Lcom/google/android/apps/plus/analytics/EsAnalytics;->recordActionEvent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/analytics/OzActions;Lcom/google/android/apps/plus/analytics/OzViews;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, p1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_0
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    sget v0, Lcom/google/android/apps/plus/R$id;->audience_circle_name_loader_id:I

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    const/4 v3, 0x5

    sget-object v4, Lcom/google/android/apps/plus/views/AudienceView$CirclesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 10
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    const/4 v9, 0x1

    const/4 v1, 0x0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    sget v2, Lcom/google/android/apps/plus/R$id;->audience_circle_name_loader_id:I

    if-ne v0, v2, :cond_8

    if-eqz p2, :cond_7

    sget-boolean v0, Lcom/google/android/apps/plus/views/AudienceView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/AudienceView$CirclesQuery;->PROJECTION:[Ljava/lang/String;

    invoke-interface {p2}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v5

    if-ne v5, v9, :cond_3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    move v0, v1

    :goto_1
    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    invoke-interface {p2, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getSize()I

    move-result v5

    invoke-direct {v0, v6, v7, v8, v5}, Lcom/google/android/apps/plus/content/CircleData;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    new-instance v5, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-direct {v5, v0}, Lcom/google/android/apps/plus/content/AudienceData;-><init>(Lcom/google/android/apps/plus/content/CircleData;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v5

    if-eq v5, v9, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v5

    if-ne v5, v9, :cond_1

    :cond_4
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    :cond_7
    return-void

    :cond_8
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->audience:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->edited:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/AudienceView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    iput-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->audience:Ljava/util/ArrayList;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/AudienceView$SavedState;->edited:Z

    return-object v0
.end method

.method protected removeLastChip()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    goto :goto_0
.end method

.method public final removePerson(Lcom/google/android/apps/plus/content/PersonData;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/content/PersonData;

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/content/AudienceData;->getUser(I)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v3

    invoke-static {v3, p1}, Lcom/google/android/apps/plus/content/EsPeopleData;->isSamePerson(Lcom/google/android/apps/plus/content/PersonData;Lcom/google/android/apps/plus/content/PersonData;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v1, v0

    :cond_1
    if-eqz v1, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->update()V

    :cond_2
    return-void
.end method

.method public final replaceAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/AudienceView;->mEdited:Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AudienceView;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    return-void
.end method

.method public setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method public setAudienceChangedCallback(Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/AudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    return-void
.end method

.method public setDefaultAudience(Lcom/google/android/apps/plus/content/AudienceData;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/content/AudienceData;

    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/AudienceView;->setAudience(Lcom/google/android/apps/plus/content/AudienceData;)V

    :cond_0
    return-void
.end method

.method protected update()V
    .locals 24

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChips:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/content/AudienceData;

    sget-boolean v2, Lcom/google/android/apps/plus/views/AudienceView;->$assertionsDisabled:Z

    if-nez v2, :cond_0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v2

    const/4 v4, 0x1

    if-eq v2, v4, :cond_0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2

    :cond_0
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getCircles()[Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v21, v0

    const/16 v19, 0x0

    :goto_1
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    aget-object v7, v16, v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v4

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/util/AccountsUtil;->isRestrictedCircleForAccount(Lcom/google/android/apps/plus/content/EsAccount;I)Z

    move-result v8

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v6

    :goto_2
    add-int/lit8 v10, v3, 0x1

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_acl_circles:I

    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getCloseIcon()I

    move-result v5

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/plus/views/AudienceView;->updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V

    add-int/lit8 v19, v19, 0x1

    move v3, v10

    goto :goto_1

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v4, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    :pswitch_0
    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_acl_public:I

    goto :goto_3

    :pswitch_1
    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_acl_extended:I

    goto :goto_3

    :pswitch_2
    sget v4, Lcom/google/android/apps/plus/R$drawable;->ic_acl_domain:I

    goto :goto_3

    :cond_2
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getUsers()[Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v16

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v21, v0

    const/16 v19, 0x0

    move v10, v3

    :goto_4
    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    aget-object v14, v16, v19

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v6

    :goto_5
    add-int/lit8 v3, v10, 0x1

    const/4 v11, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getCloseIcon()I

    move-result v12

    const/4 v15, 0x0

    move-object/from16 v9, p0

    move-object v13, v6

    invoke-virtual/range {v9 .. v15}, Lcom/google/android/apps/plus/views/AudienceView;->updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V

    add-int/lit8 v19, v19, 0x1

    move v10, v3

    goto :goto_4

    :cond_3
    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x104000e

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_5

    :cond_5
    move v3, v10

    goto/16 :goto_0

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/AudienceView;->getChipCount()I

    move-result v17

    :goto_6
    move/from16 v0, v17

    if-ge v3, v0, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    add-int/lit8 v23, v2, -0x1

    :goto_7
    if-ltz v23, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-ne v2, v4, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_8
    add-int/lit8 v23, v23, -0x1

    goto :goto_7

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/AudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    :cond_a
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method protected updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/Object;
    .param p6    # Z

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getChipCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le p1, v3, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/AudienceView;->addChip(I)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mChipContainer:Landroid/view/ViewGroup;

    invoke-virtual {v3, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2, v5, p3, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    instance-of v1, p5, Lcom/google/android/apps/plus/content/CircleData;

    if-eqz p6, :cond_3

    sget v3, Lcom/google/android/apps/plus/R$drawable;->chip_red:I

    :goto_0
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/AudienceView;->mCanRemoveChips:Z

    if-eqz v3, :cond_1

    if-eqz v1, :cond_6

    sget v2, Lcom/google/android/apps/plus/R$string;->edit_audience_content_description_remove_circle:I

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p4, v4, v5

    invoke-virtual {v3, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_2
    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_3
    if-eqz v1, :cond_4

    move-object v3, p5

    check-cast v3, Lcom/google/android/apps/plus/content/CircleData;

    :goto_2
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    sget v3, Lcom/google/android/apps/plus/R$drawable;->chip_blue:I

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_0
    sget v3, Lcom/google/android/apps/plus/R$drawable;->chip_green:I

    goto :goto_0

    :cond_5
    sget v3, Lcom/google/android/apps/plus/R$drawable;->chip_blue:I

    goto :goto_0

    :cond_6
    sget v2, Lcom/google/android/apps/plus/R$string;->edit_audience_content_description_remove:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
