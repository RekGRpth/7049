.class public Lcom/google/android/apps/plus/views/BoundedLinearLayout;
.super Landroid/widget/LinearLayout;
.source "BoundedLinearLayout.java"


# instance fields
.field private mMaxHeight:I

.field private mMaxWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    iput v0, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    iput v0, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->initFromAttributeSet$643f623b(Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    iput v0, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->initFromAttributeSet$643f623b(Landroid/util/AttributeSet;)V

    return-void
.end method

.method private initFromAttributeSet$643f623b(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/util/AttributeSet;

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    nop

    :array_0
    .array-data 4
        0x101011f
        0x1010120
    .end array-data
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget v4, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    if-ltz v4, :cond_0

    sparse-switch v2, :sswitch_data_0

    :goto_0
    iget v4, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    :cond_0
    :goto_1
    iget v4, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    if-ltz v4, :cond_1

    sparse-switch v0, :sswitch_data_1

    :goto_2
    iget v4, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_1
    :goto_3
    invoke-static {v3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-super {p0, v4, v5}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void

    :sswitch_0
    const/high16 v2, 0x40000000

    iget v3, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    goto :goto_1

    :sswitch_1
    const/high16 v2, 0x40000000

    goto :goto_0

    :sswitch_2
    const/high16 v0, 0x40000000

    iget v1, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    goto :goto_3

    :sswitch_3
    const/high16 v0, 0x40000000

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_2
    .end sparse-switch
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->invalidate()V

    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->mMaxWidth:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/BoundedLinearLayout;->invalidate()V

    return-void
.end method
