.class public Lcom/google/android/apps/plus/views/CardTitleDescriptionView;
.super Landroid/view/ViewGroup;
.source "CardTitleDescriptionView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ItemClickListener;


# static fields
.field private static sDescriptionTextPaint:Landroid/text/TextPaint;

.field private static sInitialized:Z


# instance fields
.field private mDateCorner:Landroid/graphics/Point;

.field private mDateTextView:Landroid/widget/TextView;

.field private mDescriptionCorner:Landroid/graphics/Point;

.field private mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mTitleCorner:Landroid/graphics/Point;

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-boolean v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sInitialized:Z

    if-nez v1, :cond_0

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sDescriptionTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_activity_description_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_description_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->comment_link:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, v1, Landroid/text/TextPaint;->linkColor:I

    sget-object v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sDescriptionTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_description_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sput-boolean v4, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sInitialized:Z

    :cond_0
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_title_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_activity_title_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_time_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_activity_time_color:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    new-instance v1, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/ConstrainedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget-object v2, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->sDescriptionTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setTextPaint(Landroid/text/TextPaint;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final onAvatarClick$16da05f7(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/high16 v12, -0x80000000

    const/4 v11, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-static {v4, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    sub-int v9, v4, v1

    iput v9, v8, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iput v11, v8, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->x:I

    invoke-static {v9, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/widget/TextView;->measure(II)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iput v11, v8, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iput v11, v8, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iget v9, v8, Landroid/graphics/Point;->y:I

    sub-int v10, v0, v7

    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    add-int/2addr v9, v10

    iput v9, v8, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateCorner:Landroid/graphics/Point;

    iget v9, v8, Landroid/graphics/Point;->y:I

    sub-int v10, v7, v0

    invoke-static {v11, v10}, Ljava/lang/Math;->max(II)I

    move-result v10

    add-int/2addr v9, v10

    iput v9, v8, Landroid/graphics/Point;->y:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleCorner:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    add-int/2addr v8, v7

    add-int/lit8 v5, v8, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->getLength()I

    move-result v8

    if-lez v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-static {v4, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    sub-int v10, v3, v5

    invoke-static {v10, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->measure(II)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->getMeasuredHeight()I

    move-result v2

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    iput v11, v8, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionCorner:Landroid/graphics/Point;

    iput v5, v8, Landroid/graphics/Point;->y:I

    add-int/2addr v5, v2

    :cond_0
    invoke-static {v4, p1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->resolveSize(II)I

    move-result v8

    invoke-static {v5, p2}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->resolveSize(II)I

    move-result v9

    invoke-virtual {p0, v8, v9}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->setMeasuredDimension(II)V

    return-void
.end method

.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 2
    .param p1    # Landroid/text/style/URLSpan;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onLinkClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/EventActionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDateTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, p4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setHtmlText(Ljava/lang/String;Z)V

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->mDescriptionTextView:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
