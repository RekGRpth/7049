.class public final Lcom/google/android/apps/plus/views/CircleAvatarGridView;
.super Landroid/view/ViewGroup;
.source "CircleAvatarGridView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sBackground:Landroid/graphics/drawable/Drawable;

.field private static sCardBorderBottomPadding:I

.field private static sCardBorderHeightPadding:I

.field private static sCardBorderLeftPadding:I

.field private static sCardBorderRightPadding:I

.field private static sCardBorderTopPadding:I

.field private static sCardBorderWidthPadding:I

.field private static sCircleNameTextColor:I

.field private static sCircleNameTweak:I

.field private static sInitialized:Z

.field private static sIntraAvatarPadding:I

.field private static sNameAreaHeight:I

.field private static sNameTextSize:F

.field private static sOutsidePadding:I


# instance fields
.field private mCircleId:Ljava/lang/String;

.field private mCircleName:Ljava/lang/String;

.field private mCircleNameView:Landroid/widget/TextView;

.field private mContext:Landroid/content/Context;

.field private mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

.field private mMainAvatarSize:I

.field private mSecondaryAvatarSize:I

.field private mSelector:Landroid/graphics/drawable/Drawable;

.field private mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->init$643f623b(Landroid/content/Context;)V

    return-void
.end method

.method private init$643f623b(Landroid/content/Context;)V
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mContext:Landroid/content/Context;

    sget-boolean v1, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderLeftPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_right_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderRightPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_top_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderTopPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->card_border_bottom_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderBottomPadding:I

    sget v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderLeftPadding:I

    sget v3, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderRightPadding:I

    add-int/2addr v2, v3

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderWidthPadding:I

    sget v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderTopPadding:I

    sget v3, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderBottomPadding:I

    add-int/2addr v2, v3

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderHeightPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->circle_outside_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->circle_name_area_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sNameAreaHeight:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->circle_intra_avatar_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sIntraAvatarPadding:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->circle_name_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sNameTextSize:F

    sget v2, Lcom/google/android/apps/plus/R$dimen;->circle_name_tweak:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCircleNameTweak:I

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_link:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCircleNameTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sBackground:Landroid/graphics/drawable/Drawable;

    sput-boolean v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sInitialized:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->stream_list_selector:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    sget-object v1, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v1, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderLeftPadding:I

    sget v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    add-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderTopPadding:I

    sget v3, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    add-int/2addr v2, v3

    sget v3, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderRightPadding:I

    sget v4, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    add-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderBottomPadding:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->setPadding(IIII)V

    new-instance v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/views/ImageResourceView;->setSizeCategory(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setId(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_avatar:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceLoadingDrawable(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_avatar:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissingDrawable(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissing(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->addView(Landroid/view/View;)V

    new-array v1, v7, [Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    new-instance v2, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {v2, p1}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v1, v1, v0

    add-int/lit8 v2, v0, 0x65

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setId(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/AvatarView;->enableScale(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sNameTextSize:F

    invoke-virtual {v1, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCircleNameTextColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLines(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;)V
    .locals 10
    .param p1    # Landroid/database/Cursor;

    const/4 v9, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleId:Ljava/lang/String;

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleName:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    const/4 v4, 0x5

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v7, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v8, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v7, v3, v8}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    :cond_0
    move-object v7, v4

    move v4, v6

    :goto_0
    invoke-virtual {v7, v4}, Lcom/google/android/apps/plus/views/ImageResourceView;->setDimmed(Z)V

    const/4 v1, 0x1

    :goto_1
    if-gt v1, v9, :cond_3

    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v0, v4, 0x4

    add-int/lit8 v4, v0, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v4, v0, 0x1

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    add-int/lit8 v7, v1, -0x1

    aget-object v4, v4, v7

    invoke-virtual {v4, v2, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v2, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    add-int/lit8 v7, v1, -0x1

    aget-object v4, v4, v7

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/AvatarView;->setDimmed(Z)V

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissing(Z)V

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    if-nez v2, :cond_0

    move-object v7, v4

    move v4, v5

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    add-int/lit8 v7, v1, -0x1

    aget-object v4, v4, v7

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setDimmed(Z)V

    goto :goto_2

    :cond_3
    return-void
.end method

.method protected final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method protected final drawableStateChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->invalidate()V

    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    return-void
.end method

.method public final getCircleId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleId:Ljava/lang/String;

    return-object v0
.end method

.method public final getCircleName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleName:Ljava/lang/String;

    return-object v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 17
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sget v12, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderLeftPadding:I

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    add-int v1, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatarSize:I

    add-int/2addr v12, v1

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sIntraAvatarPadding:I

    add-int v5, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    add-int/2addr v12, v5

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sIntraAvatarPadding:I

    add-int v3, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatarSize:I

    add-int v2, v1, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    add-int v6, v5, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    add-int v4, v3, v12

    sget v12, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderTopPadding:I

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    add-int v9, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    add-int/2addr v12, v9

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sIntraAvatarPadding:I

    add-int v11, v12, v13

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    add-int v8, v9, v12

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    add-int v10, v11, v12

    sub-int v12, p5, p3

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderBottomPadding:I

    sub-int v7, v12, v13

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v12, v1, v9, v2, v10}, Lcom/google/android/apps/plus/views/ImageResourceView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v13, 0x0

    aget-object v12, v12, v13

    invoke-virtual {v12, v5, v9, v6, v8}, Lcom/google/android/apps/plus/views/AvatarView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v13, 0x1

    aget-object v12, v12, v13

    invoke-virtual {v12, v3, v9, v4, v8}, Lcom/google/android/apps/plus/views/AvatarView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v13, 0x2

    aget-object v12, v12, v13

    invoke-virtual {v12, v5, v11, v6, v10}, Lcom/google/android/apps/plus/views/AvatarView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v13, 0x3

    aget-object v12, v12, v13

    invoke-virtual {v12, v3, v11, v4, v10}, Lcom/google/android/apps/plus/views/AvatarView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    sget v13, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCircleNameTweak:I

    sub-int v13, v7, v13

    invoke-virtual {v12, v1, v10, v4, v13}, Landroid/widget/TextView;->layout(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v13, 0x0

    const/4 v14, 0x0

    sub-int v15, p4, p2

    sub-int v16, p5, p3

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-void
.end method

.method protected final onMeasure(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/high16 v7, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    sget v4, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v3, v4

    sget v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sIntraAvatarPadding:I

    mul-int/lit8 v5, v5, 0x3

    sub-int/2addr v4, v5

    sget v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderWidthPadding:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    iget v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    mul-int/lit8 v4, v4, 0x2

    sget v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sIntraAvatarPadding:I

    add-int/2addr v4, v5

    iput v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatarSize:I

    sget v4, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatarSize:I

    add-int/2addr v4, v5

    sget v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sNameAreaHeight:I

    add-int/2addr v4, v5

    sget v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sCardBorderHeightPadding:I

    add-int v0, v4, v5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatarSize:I

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatarSize:I

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/views/ImageResourceView;->measure(II)V

    iget v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->getTinyAvatarSize(Landroid/content/Context;)I

    move-result v5

    if-gt v4, v5, :cond_0

    const/4 v2, 0x0

    :goto_0
    const/4 v1, 0x0

    :goto_1
    const/4 v4, 0x4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v4, v4, v1

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v4, v4, v1

    iget v5, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSecondaryAvatarSize:I

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallAvatarSize(Landroid/content/Context;)I

    move-result v5

    if-gt v4, v5, :cond_1

    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumAvatarSize(Landroid/content/Context;)I

    const/4 v2, 0x2

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    sget v5, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sOutsidePadding:I

    mul-int/lit8 v5, v5, 0x2

    sub-int v5, v3, v5

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->sNameAreaHeight:I

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->measure(II)V

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->setMeasuredDimension(II)V

    return-void
.end method

.method public final onRecycle()V
    .locals 3

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleId:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mCircleNameView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mMainAvatar:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSmallAvatars:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final requestLayout()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->forceLayout()V

    return-void
.end method

.method protected final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleAvatarGridView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
