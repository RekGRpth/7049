.class public Lcom/google/android/apps/plus/views/CircleNameAndCountView;
.super Landroid/view/ViewGroup;
.source "CircleNameAndCountView.java"


# instance fields
.field private mCountTextView:Landroid/view/View;

.field private mIconView:Landroid/view/View;

.field private mNameTextView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    const v0, 0x1020014

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    const v0, 0x1020015

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    const v0, 0x1020006

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v4, p5, p3

    sub-int/2addr v4, v0

    div-int/lit8 v3, v4, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingLeft()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    add-int v5, v2, v1

    add-int v6, v3, v0

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    add-int v5, v2, v1

    add-int v6, v2, v1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v3

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/view/View;->layout(IIII)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    sub-int v5, p4, v5

    const/4 v6, 0x0

    sub-int v7, p4, p2

    sub-int v8, p5, p3

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1    # I
    .param p2    # I

    const/4 v14, 0x0

    move/from16 v0, p1

    invoke-static {v14, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->resolveSize(II)I

    move-result v13

    const/4 v14, 0x0

    move/from16 v0, p2

    invoke-static {v14, v0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->resolveSize(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_4

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getVisibility()I

    move-result v14

    if-nez v14, :cond_5

    const/4 v5, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    const/4 v3, 0x0

    const/4 v1, 0x0

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    :cond_0
    const/4 v6, 0x0

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mIconView:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    iget v6, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    :cond_1
    add-int v12, v8, v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingLeft()I

    move-result v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingRight()I

    move-result v10

    add-int v14, v12, v9

    add-int v11, v14, v10

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    sparse-switch v14, :sswitch_data_0

    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mNameTextView:Landroid/view/View;

    const/high16 v15, 0x40000000

    invoke-static {v8, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    const/high16 v16, 0x40000000

    move/from16 v0, v16

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->mCountTextView:Landroid/view/View;

    const/high16 v15, 0x40000000

    invoke-static {v3, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    const/high16 v16, 0x40000000

    move/from16 v0, v16

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Landroid/view/View;->measure(II)V

    :cond_3
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v14

    sparse-switch v14, :sswitch_data_1

    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v4}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->setMeasuredDimension(II)V

    return-void

    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_1

    :sswitch_0
    add-int v13, v11, v6

    goto :goto_2

    :sswitch_1
    if-eqz v13, :cond_6

    add-int v14, v11, v6

    if-ge v14, v13, :cond_2

    :cond_6
    add-int v13, v11, v6

    goto :goto_2

    :sswitch_2
    sub-int v14, v13, v9

    sub-int/2addr v14, v10

    sub-int/2addr v14, v3

    sub-int/2addr v14, v6

    const/4 v15, 0x0

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-static {v8, v14}, Ljava/lang/Math;->min(II)I

    move-result v8

    goto :goto_2

    :sswitch_3
    invoke-static {v7, v1}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingTop()I

    move-result v15

    add-int/2addr v14, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/CircleNameAndCountView;->getPaddingBottom()I

    move-result v15

    add-int v4, v14, v15

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_3
        0x0 -> :sswitch_3
    .end sparse-switch
.end method
