.class final Lcom/google/android/apps/plus/views/CircleSettingsStreamView$1;
.super Ljava/lang/Object;
.source "CircleSettingsStreamView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->init$23ad5828(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/CircleSettingsStreamView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$1;->this$0:Lcom/google/android/apps/plus/views/CircleSettingsStreamView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$1;->this$0:Lcom/google/android/apps/plus/views/CircleSettingsStreamView;

    # getter for: Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mOnClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->access$100(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$1;->this$0:Lcom/google/android/apps/plus/views/CircleSettingsStreamView;

    # getter for: Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mOnClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->access$100(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;->onAvatarClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
