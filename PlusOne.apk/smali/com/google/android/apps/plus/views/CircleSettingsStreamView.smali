.class public Lcom/google/android/apps/plus/views/CircleSettingsStreamView;
.super Landroid/view/ViewGroup;
.source "CircleSettingsStreamView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;,
        Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;
    }
.end annotation


# static fields
.field private static sAvatarMinSize:I

.field private static sAvatarsBackground:Landroid/graphics/drawable/BitmapDrawable;

.field private static sCardBackground:Landroid/graphics/drawable/Drawable;

.field private static sCountBackgroundColor:I

.field private static sInitialized:Z

.field private static sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

.field private static sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

.field private static sPaddingBottom:I

.field private static sPaddingLeft:I

.field private static sPaddingRight:I

.field private static sPaddingTop:I

.field private static sSettingsAreaMaxHeight:I

.field private static sSettingsBackground:Landroid/graphics/drawable/Drawable;

.field private static sSettingsTextColor:I

.field private static sSettingsTextIconPadding:I

.field private static sSettingsTextSize:F

.field private static sXPadding:I

.field private static sYPadding:I


# instance fields
.field private mAvatarCount:I

.field private mAvatarDisplayed:I

.field private mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

.field private mAvatarsBackground:Landroid/widget/ImageView;

.field private mBlockSize:I

.field private mIsSubscribed:Z

.field private mMemberCount:I

.field private mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

.field private mOnClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

.field private mSettingsLayout:Landroid/widget/FrameLayout;

.field private mSettingsView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->init$23ad5828(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->init$23ad5828(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->init$23ad5828(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sCountBackgroundColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/CircleSettingsStreamView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mOnClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    return-object v0
.end method

.method private init$23ad5828(Landroid/content/Context;)V
    .locals 8
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v7, -0x2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-boolean v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sInitialized:Z

    if-nez v3, :cond_0

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_border_left_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingLeft:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_border_top_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_border_right_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingRight:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->card_border_bottom_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingBottom:I

    sget v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingLeft:I

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingRight:I

    add-int/2addr v3, v4

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sXPadding:I

    sget v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingBottom:I

    add-int/2addr v3, v4

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sYPadding:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_circle_min_avatar_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sAvatarMinSize:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_circle_settings_max_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsAreaMaxHeight:I

    sget v3, Lcom/google/android/apps/plus/R$drawable;->icn_notification_enabled:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->icn_notification_disabled:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sCardBackground:Landroid/graphics/drawable/Drawable;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->bg_blue_tile:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    sput-object v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sAvatarsBackground:Landroid/graphics/drawable/BitmapDrawable;

    sget-object v4, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    sget-object v5, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v3, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeXY(Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    sget v3, Lcom/google/android/apps/plus/R$drawable;->circle_settings_background:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsBackground:Landroid/graphics/drawable/Drawable;

    sget v3, Lcom/google/android/apps/plus/R$color;->stream_circle_count_bg:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sCountBackgroundColor:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_circle_settings_size:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextSize:F

    sget v3, Lcom/google/android/apps/plus/R$color;->stream_circle_settings_text:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextColor:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_circle_settings_padding:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextIconPadding:I

    const/4 v3, 0x1

    sput-boolean v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sInitialized:Z

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-direct {v1, v7}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarsBackground:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarsBackground:Landroid/widget/ImageView;

    sget-object v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sAvatarsBackground:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarsBackground:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->addView(Landroid/view/View;)V

    const/4 v0, 0x0

    :goto_0
    const/16 v3, 0x9

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    new-instance v4, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {v4, p1}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;)V

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v3, v3, v0

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setAvatarSize(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v3, v3, v0

    new-instance v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$1;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$1;-><init>(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v3, v3, v0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    invoke-direct {v3, p1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    new-instance v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$2;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$2;-><init>(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->addView(Landroid/view/View;)V

    new-instance v3, Landroid/widget/TextView;

    invoke-direct {v3, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextSize:F

    invoke-virtual {v3, v6, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextColor:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    const/16 v4, 0x10

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_circle_settings:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextIconPadding:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    new-instance v3, Landroid/widget/FrameLayout;

    invoke-direct {v3, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    sget v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextIconPadding:I

    sget v5, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsTextIconPadding:I

    invoke-virtual {v3, v4, v6, v5, v6}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v6, 0x11

    invoke-direct {v5, v7, v7, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v4, v5}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    new-instance v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$3;

    invoke-direct {v4, p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$3;-><init>(Lcom/google/android/apps/plus/views/CircleSettingsStreamView;)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    sget-object v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->addView(Landroid/view/View;)V

    sget-object v3, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sCardBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method


# virtual methods
.method public final bind(Landroid/database/Cursor;Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;)V
    .locals 11
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    const/16 v10, 0x9

    const/4 v5, 0x1

    const/4 v9, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    check-cast v1, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v6}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->getInt(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCount:I

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->getInt(I)I

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mIsSubscribed:Z

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    iget v7, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCount:I

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->bindMemberCount(I)V

    invoke-virtual {v1}, Lcom/google/android/apps/plus/fragments/HostedStreamFragment$CircleSettingsStreamCursor;->getCircleAvatarsCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    move v2, v6

    :goto_1
    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarCount:I

    iget v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarCount:I

    if-lez v4, :cond_4

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v3, 0x0

    :goto_2
    iget v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarCount:I

    if-ge v3, v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v4, v4, v3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v4, v4, v3

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_2

    :cond_1
    move v4, v6

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v6

    goto :goto_1

    :cond_3
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    goto :goto_1

    :cond_4
    iget v3, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarCount:I

    :goto_3
    if-ge v3, v10, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v4, v4, v3

    invoke-virtual {v4, v9}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v4, v4, v3

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsView:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mIsSubscribed:Z

    if-eqz v4, :cond_6

    sget-object v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sNotificationsEnabled:Landroid/graphics/drawable/Drawable;

    :goto_4
    invoke-virtual {v5, v4, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object p2, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mOnClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    return-void

    :cond_6
    sget-object v4, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sNotificationsDisabled:Landroid/graphics/drawable/Drawable;

    goto :goto_4
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    sub-int v7, p4, p2

    sub-int v4, p5, p3

    sget v1, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingLeft:I

    sget v8, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    iget v9, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    add-int v0, v8, v9

    const/4 v5, 0x0

    :goto_0
    iget v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    if-ge v5, v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v8, v8, v5

    sget v9, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    iget v10, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    add-int/2addr v10, v1

    invoke-virtual {v8, v1, v9, v10, v0}, Lcom/google/android/apps/plus/views/AvatarView;->layout(IIII)V

    iget v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    add-int/2addr v1, v8

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    sget v8, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingLeft:I

    iget v9, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    iget v10, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    mul-int/2addr v9, v10

    add-int v2, v8, v9

    sget v8, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingRight:I

    sub-int v3, v7, v8

    sget v8, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    iget v9, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    add-int v6, v8, v9

    iget v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarCount:I

    iget v9, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    if-ge v8, v9, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarsBackground:Landroid/widget/ImageView;

    sget v9, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingLeft:I

    iget v10, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    iget v11, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarCount:I

    mul-int/2addr v10, v11

    add-int/2addr v9, v10

    sget v10, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    invoke-virtual {v8, v9, v10, v2, v6}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarsBackground:Landroid/widget/ImageView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    sget v9, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingTop:I

    invoke-virtual {v8, v2, v9, v3, v6}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->layout(IIII)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    sget v9, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingLeft:I

    sget v10, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sPaddingBottom:I

    sub-int v10, v4, v10

    invoke-virtual {v8, v9, v6, v3, v10}, Landroid/widget/FrameLayout;->layout(IIII)V

    return-void

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarsBackground:Landroid/widget/ImageView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/high16 v9, 0x40000000

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sXPadding:I

    sub-int v6, v5, v6

    div-int/lit8 v6, v6, 0xa

    iput v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    sget v7, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sAvatarMinSize:I

    if-ge v6, v7, :cond_2

    sget v6, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sXPadding:I

    sub-int v6, v5, v6

    div-int/lit8 v6, v6, 0x6

    iput v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    const/4 v6, 0x5

    iput v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    :goto_0
    iget v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCount:I

    const/16 v7, 0x3e7

    if-le v6, v7, :cond_0

    iget v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    :cond_0
    iget v4, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    sget v6, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sSettingsAreaMaxHeight:I

    if-le v4, v6, :cond_1

    iget v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    int-to-float v6, v6

    const/high16 v7, 0x3f400000

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v4

    :cond_1
    iget v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    add-int/2addr v6, v4

    sget v7, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sYPadding:I

    add-int v1, v6, v7

    iput v1, v3, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    sget v6, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sXPadding:I

    sub-int v6, v5, v6

    iget v7, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    iget v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    mul-int/2addr v7, v8

    sub-int v0, v6, v7

    const/4 v2, 0x0

    :goto_1
    iget v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    if-ge v2, v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v6, v6, v2

    iget v7, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const/16 v6, 0x9

    iput v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarDisplayed:I

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCountView:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    iget v8, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mBlockSize:I

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView$MemberCountView;->measure(II)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mSettingsLayout:Landroid/widget/FrameLayout;

    sget v7, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->sXPadding:I

    sub-int v7, v5, v7

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/widget/FrameLayout;->measure(II)V

    invoke-virtual {p0, v5, v1}, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0x9

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mAvatarViews:[Lcom/google/android/apps/plus/views/AvatarView;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mMemberCount:I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/CircleSettingsStreamView;->mOnClickListener:Lcom/google/android/apps/plus/views/CircleSettingsStreamView$CircleSettingsClickListener;

    return-void
.end method

.method public final shouldHighlightOnPress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
