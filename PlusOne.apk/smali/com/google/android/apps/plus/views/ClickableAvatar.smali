.class final Lcom/google/android/apps/plus/views/ClickableAvatar;
.super Ljava/lang/Object;
.source "ClickableAvatar.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;,
        Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;
    }
.end annotation


# static fields
.field private static sImageSelectedPaint:Landroid/graphics/Paint;


# instance fields
.field private mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

.field private final mAvatarSizeCategory:I

.field private final mAvatarUrl:Ljava/lang/String;

.field private mClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

.field private mClicked:Z

.field private mContentDescription:Ljava/lang/CharSequence;

.field private final mContentRect:Landroid/graphics/Rect;

.field private final mGaiaId:Ljava/lang/String;

.field private final mIsAvatarRound:Z

.field private mSuggestionClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;

.field private final mSuggestionId:Ljava/lang/String;

.field private final mUserName:Ljava/lang/String;

.field private final mView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V
    .locals 8
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;
    .param p6    # I

    const/4 v6, 0x2

    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;IZ)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;
    .param p6    # I
    .param p7    # Z

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;IZ)V

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;IZ)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;
    .param p7    # I
    .param p8    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    iput-object p6, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mGaiaId:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mSuggestionId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mUserName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentDescription:Ljava/lang/CharSequence;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarUrl:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarSizeCategory:I

    iput-boolean p8, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mIsAvatarRound:Z

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$color;->image_selected_stroke:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final bindResources()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarUrl:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarUrl:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarSizeCategory:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mIsAvatarRound:Z

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mGaiaId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mGaiaId:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarSizeCategory:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mIsAvatarRound:Z

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarByGaiaId(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    goto :goto_0
.end method

.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final drawSelectionRect(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mIsAvatarRound:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/ClickableAvatar;->sImageSelectedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x3

    if-ne p3, v2, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClicked:Z

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_2

    if-ne p3, v0, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClicked:Z

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClicked:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClicked:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mUserName:Ljava/lang/String;

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;->onAvatarClick$16da05f7(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mSuggestionClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mSuggestionClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mGaiaId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mUserName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mSuggestionId:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;->onAvatarClick$14e1ec6d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClicked:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final isClicked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClicked:Z

    return v0
.end method

.method public final onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method

.method public final setClickListener(Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    return-void
.end method

.method public final setRect(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mContentRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public final setSuggestionClickListener(Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mSuggestionClickListener:Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " gaia id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mUserName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mView:Landroid/view/View;

    if-eqz v1, :cond_0

    const-string v1, " view: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mView:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public final unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableAvatar;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
