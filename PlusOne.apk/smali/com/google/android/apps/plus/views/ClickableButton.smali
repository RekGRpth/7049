.class public final Lcom/google/android/apps/plus/views/ClickableButton;
.super Ljava/lang/Object;
.source "ClickableButton.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    }
.end annotation


# static fields
.field private static sClickableButtonInitialized:Z

.field private static sDefaultBitmapTextXSpacing:I

.field private static sExtraTextXPadding:I

.field private static sTextXPadding:I


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapTextXSpacing:I

.field private mClicked:Z

.field private mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field private mContentDescription:Ljava/lang/CharSequence;

.field private mContext:Landroid/content/Context;

.field private mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

.field private mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

.field private mRect:Landroid/graphics/Rect;

.field private mTextLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p4    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p5    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p6    # I
    .param p7    # I
    .param p8    # Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/text/TextPaint;
    .param p5    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p6    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p7    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p8    # I
    .param p9    # I

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move-object v10, p3

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/text/TextPaint;
    .param p5    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p6    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p7    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p8    # I
    .param p9    # I
    .param p10    # Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V
    .locals 13
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/text/TextPaint;
    .param p5    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p6    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p7    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p8    # I
    .param p9    # I
    .param p10    # Ljava/lang/CharSequence;

    const/4 v11, 0x0

    const/4 v12, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;ZI)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;ZI)V
    .locals 16
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Ljava/lang/CharSequence;
    .param p4    # Landroid/text/TextPaint;
    .param p5    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p6    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p7    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p8    # I
    .param p9    # I
    .param p10    # Ljava/lang/CharSequence;
    .param p11    # Z
    .param p12    # I

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/views/ClickableButton;->initialize(Landroid/content/Context;)V

    if-ltz p12, :cond_0

    :goto_0
    move/from16 v0, p12

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmapTextXSpacing:I

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mContext:Landroid/content/Context;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ClickableButton;->mContentDescription:Ljava/lang/CharSequence;

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/drawable/NinePatchDrawable;->getMinimumWidth()I

    move-result v12

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/drawable/NinePatchDrawable;->getMinimumHeight()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    if-eqz p3, :cond_1

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmapTextXSpacing:I

    :goto_1
    if-nez p3, :cond_2

    const/4 v15, 0x0

    move v5, v15

    :goto_2
    if-nez p2, :cond_3

    const/4 v14, 0x0

    :goto_3
    if-nez p2, :cond_4

    const/4 v13, 0x0

    :goto_4
    new-instance v2, Landroid/graphics/Rect;

    add-int v3, v5, v14

    add-int/2addr v3, v10

    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int v3, v3, p8

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/plus/views/ClickableButton;->getTextXPadding(Z)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v15, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v11, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int v4, v4, p9

    move/from16 v0, p8

    move/from16 v1, p9

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    return-void

    :cond_0
    sget p12, Lcom/google/android/apps/plus/views/ClickableButton;->sDefaultBitmapTextXSpacing:I

    goto :goto_0

    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    :cond_2
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v5, v2

    new-instance v2, Landroid/text/StaticLayout;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v15

    goto :goto_2

    :cond_3
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    goto :goto_3

    :cond_4
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v13

    goto :goto_4
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Landroid/text/TextPaint;
    .param p4    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p5    # Landroid/graphics/drawable/NinePatchDrawable;
    .param p6    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p7    # I
    .param p8    # I

    const/4 v2, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object v10, p2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    return-void
.end method

.method private static getTextXPadding(Z)I
    .locals 1
    .param p0    # Z

    if-eqz p0, :cond_0

    sget v0, Lcom/google/android/apps/plus/views/ClickableButton;->sExtraTextXPadding:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/apps/plus/views/ClickableButton;->sTextXPadding:I

    goto :goto_0
.end method

.method public static getTotalDefaultPadding(Landroid/content/Context;ZZ)I
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Z

    invoke-static {p0}, Lcom/google/android/apps/plus/views/ClickableButton;->initialize(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getTextXPadding(Z)I

    move-result v1

    mul-int/lit8 v0, v1, 0x2

    sget v1, Lcom/google/android/apps/plus/views/ClickableButton;->sDefaultBitmapTextXSpacing:I

    add-int/2addr v0, v1

    return v0
.end method

.method private static initialize(Landroid/content/Context;)V
    .locals 2
    .param p0    # Landroid/content/Context;

    sget-boolean v1, Lcom/google/android/apps/plus/views/ClickableButton;->sClickableButtonInitialized:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/ClickableButton;->sClickableButtonInitialized:Z

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->clickable_button_horizontal_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/ClickableButton;->sTextXPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->clickable_button_extra_horizontal_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/ClickableButton;->sExtraTextXPadding:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->clickable_button_bitmap_text_x_spacing:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/ClickableButton;->sDefaultBitmapTextXSpacing:I

    :cond_0
    return-void
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final createAbsoluteCoordinatesCopy(II)Lcom/google/android/apps/plus/views/ClickableButton;
    .locals 11
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_0

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, p1

    iget-object v9, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, p2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mContentDescription:Ljava/lang/CharSequence;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v4

    goto :goto_0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1    # Landroid/graphics/Canvas;

    const/4 v6, 0x0

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    if-eqz v7, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClickedBackground:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_0
    if-eqz v0, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v7, :cond_5

    move v2, v6

    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-nez v7, :cond_6

    :cond_1
    move v1, v6

    :goto_2
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-nez v7, :cond_7

    move v3, v6

    :goto_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v7

    sub-int/2addr v7, v2

    sub-int/2addr v7, v1

    sub-int/2addr v7, v3

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    int-to-float v7, v4

    int-to-float v8, v5

    const/4 v9, 0x0

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    add-int v6, v2, v1

    add-int/2addr v4, v6

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getHeight()I

    move-result v8

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    int-to-float v6, v4

    int-to-float v7, v5

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v6, v4

    int-to-float v6, v6

    neg-int v7, v5

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mDefaultBackground:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_0

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    goto :goto_1

    :cond_6
    iget v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mBitmapTextXSpacing:I

    goto :goto_2

    :cond_7
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mTextLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    goto :goto_3
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x3

    if-ne p3, v2, :cond_2

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-nez v2, :cond_3

    if-ne p3, v1, :cond_0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    goto :goto_0

    :cond_3
    packed-switch p3, :pswitch_data_0

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    goto :goto_1

    :pswitch_1
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    invoke-interface {v2, p0}, Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;->onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V

    :cond_4
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mClicked:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final offset(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->offset(II)V

    :cond_0
    return-void
.end method

.method public final setHeight(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_0
    return-void
.end method

.method public final setWidthAndPosition(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/Rect;->offsetTo(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ClickableButton;->mRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, p1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :cond_0
    return-void
.end method
