.class public final Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;
.super Ljava/lang/Object;
.source "ClickableItem.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ClickableItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClickableItemsComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/plus/views/ClickableItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I
    .locals 8
    .param p0    # Lcom/google/android/apps/plus/views/ClickableItem;
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {p0}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/apps/plus/views/ClickableItem;->getRect()Landroid/graphics/Rect;

    move-result-object v4

    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    iget v7, v4, Landroid/graphics/Rect;->top:I

    if-gt v6, v7, :cond_1

    const/4 v2, -0x1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    if-lt v6, v7, :cond_2

    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    iget v6, v1, Landroid/graphics/Rect;->left:I

    iget v7, v4, Landroid/graphics/Rect;->left:I

    sub-int v2, v6, v7

    if-nez v2, :cond_0

    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int v5, v6, v7

    if-eqz v5, :cond_3

    move v2, v5

    goto :goto_0

    :cond_3
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v6, v7

    if-eqz v0, :cond_4

    move v2, v0

    goto :goto_0

    :cond_4
    iget v6, v1, Landroid/graphics/Rect;->right:I

    iget v7, v4, Landroid/graphics/Rect;->right:I

    sub-int v3, v6, v7

    if-eqz v3, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v7

    sub-int v2, v6, v7

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method
