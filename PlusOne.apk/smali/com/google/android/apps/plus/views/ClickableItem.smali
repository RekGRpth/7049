.class public interface abstract Lcom/google/android/apps/plus/views/ClickableItem;
.super Ljava/lang/Object;
.source "ClickableItem.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/apps/plus/views/ClickableItem;",
        ">;"
    }
.end annotation


# static fields
.field public static final sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    return-void
.end method


# virtual methods
.method public abstract getContentDescription()Ljava/lang/CharSequence;
.end method

.method public abstract getRect()Landroid/graphics/Rect;
.end method

.method public abstract handleEvent(III)Z
.end method
