.class public final Lcom/google/android/apps/plus/views/ClickableStaticLayout;
.super Lcom/google/android/apps/plus/views/PositionedStaticLayout;
.source "ClickableStaticLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableItem;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;,
        Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;
    }
.end annotation


# instance fields
.field private final mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

.field private mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

.field private mContentDescription:Ljava/lang/CharSequence;

.field private final mSpannedText:Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V
    .locals 9
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Landroid/text/TextPaint;
    .param p3    # I
    .param p4    # Landroid/text/Layout$Alignment;
    .param p5    # F
    .param p6    # F
    .param p7    # Z
    .param p8    # Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentDescription:Ljava/lang/CharSequence;

    instance-of v1, p1, Landroid/text/Spanned;

    if-eqz v1, :cond_0

    check-cast p1, Landroid/text/Spanned;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    goto :goto_0
.end method

.method private static buildStateSpans(Landroid/content/Context;Ljava/lang/String;Landroid/text/Html$TagHandler;IZ)Landroid/text/SpannableStringBuilder;
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/text/Html$TagHandler;
    .param p3    # I
    .param p4    # Z

    if-nez p1, :cond_1

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    :cond_0
    return-object v0

    :cond_1
    const/4 v6, 0x0

    invoke-static {p1, v6, p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const/4 v6, 0x0

    invoke-interface {v1}, Landroid/text/Spanned;->length()I

    move-result v7

    const-class v8, Landroid/text/style/URLSpan;

    invoke-virtual {v0, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/text/style/URLSpan;

    const/4 v2, 0x0

    :goto_0
    array-length v6, v5

    if-ge v2, v6, :cond_0

    aget-object v4, v5, v2

    new-instance v3, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v4}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v7

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->getSpanFlags(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual {v0, v3, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 3
    .param p0    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {v2, p0, v2, v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Landroid/content/Context;Ljava/lang/String;Landroid/text/Html$TagHandler;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static buildStateSpans(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/SpannableStringBuilder;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Landroid/text/Html$TagHandler;

    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-static {v0, p0, p1, v1, v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Landroid/content/Context;Ljava/lang/String;Landroid/text/Html$TagHandler;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0
.end method

.method public static createConstrainedLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)Lcom/google/android/apps/plus/views/ClickableStaticLayout;
    .locals 12
    .param p0    # Landroid/text/TextPaint;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    const/4 v1, 0x0

    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    if-nez p3, :cond_1

    const-string v2, ""

    :goto_0
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move v4, p2

    move-object/from16 v9, p4

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object v0, v1

    :cond_0
    return-object v0

    :cond_1
    const/4 v1, 0x1

    if-ne p3, v1, :cond_2

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {p1, p0, p2, v1}, Lcom/google/android/apps/plus/util/TextPaintUtils;->smartEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v2

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p0

    move v3, p2

    move-object/from16 v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLineCount()I

    move-result v1

    if-le v1, p3, :cond_0

    add-int/lit8 v1, p3, -0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLineEnd(I)I

    move-result v11

    new-instance v10, Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x0

    invoke-interface {p1, v1, v11}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v10, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p1, v11, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, p0, p2, v3}, Lcom/google/android/apps/plus/util/TextPaintUtils;->smartEllipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v10, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-object v2, v10

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1    # Ljava/lang/Object;
    .param p2    # Ljava/lang/Object;

    check-cast p1, Lcom/google/android/apps/plus/views/ClickableItem;

    check-cast p2, Lcom/google/android/apps/plus/views/ClickableItem;

    sget-object v0, Lcom/google/android/apps/plus/views/ClickableItem;->sComparator:Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/ClickableItem$ClickableItemsComparator;->compare(Lcom/google/android/apps/plus/views/ClickableItem;Lcom/google/android/apps/plus/views/ClickableItem;)I

    move-result v0

    return v0
.end method

.method public final getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentDescription:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    return-object v0
.end method

.method public final handleEvent(III)Z
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x3

    if-ne p3, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    iput-object v7, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    if-nez v4, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_4

    if-ne p3, v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    iput-object v7, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    :cond_3
    move v2, v3

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v4, p1, v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mContentArea:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int v5, p2, v5

    int-to-float v5, v5

    invoke-static {v8, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-static {v6, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getLineForVertical(I)I

    move-result v5

    invoke-static {v8, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getWidth()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    invoke-static {v6, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-virtual {p0, v5, v4}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getOffsetForHorizontal(IF)I

    move-result v0

    if-gez v0, :cond_5

    move v2, v3

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mSpannedText:Landroid/text/Spanned;

    const-class v5, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-interface {v4, v0, v0, v5}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    array-length v4, v1

    if-nez v4, :cond_6

    move v2, v3

    goto :goto_0

    :cond_6
    packed-switch p3, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    aget-object v3, v1, v3

    iput-object v3, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    aget-object v5, v1, v3

    if-ne v4, v5, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickListener:Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;

    aget-object v5, v1, v3

    invoke-interface {v4, v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;->onSpanClick(Landroid/text/style/URLSpan;)V

    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;->setClicked(Z)V

    iput-object v7, p0, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->mClickedSpan:Lcom/google/android/apps/plus/views/ClickableStaticLayout$StateURLSpan;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
