.class public Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;
.super Landroid/view/ViewGroup$LayoutParams;
.source "ColumnGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ColumnGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field column:I

.field id:J

.field public isBoxStart:Z

.field public majorSpan:I

.field public minorSpan:I

.field orientation:I

.field position:I

.field viewType:I


# direct methods
.method public constructor <init>(II)V
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v3, -0x3

    const/4 v0, -0x1

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    if-eq p2, v3, :cond_1

    move v1, p2

    :goto_0
    if-ne p1, v2, :cond_2

    move p2, v0

    :cond_0
    :goto_1
    invoke-direct {p0, v1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    iput p1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    return-void

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    if-ne p2, v3, :cond_0

    move p2, v0

    goto :goto_1
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;-><init>(II)V

    iput p3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    iput p4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    iput v5, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->ColumnGridView_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    invoke-virtual {v0, v5, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v5}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    if-ne v1, v3, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    if-eq v1, v4, :cond_0

    const-string v1, "ColumnGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inflation setting LayoutParams height to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - must be MATCH_PARENT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    if-eq v1, v4, :cond_0

    const-string v1, "ColumnGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Inflation setting LayoutParams width to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - must be MATCH_PARENT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, 0x1

    const/4 v3, -0x1

    invoke-direct {p0, p1}, Landroid/view/ViewGroup$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    iput v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    if-eq v0, v3, :cond_0

    const-string v0, "ColumnGridView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Constructing LayoutParams with height "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - must be MATCH_PARENT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->height:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    if-eq v0, v3, :cond_0

    const-string v0, "ColumnGridView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Constructing LayoutParams with width "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - must be MATCH_PARENT"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput v3, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->width:I

    goto :goto_0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ColumnGridView.LayoutParams: id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->id:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " major="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->majorSpan:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->minorSpan:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " pos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->position:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->viewType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " col="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->column:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " boxstart="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->isBoxStart:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " orient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/ColumnGridView$LayoutParams;->orientation:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
