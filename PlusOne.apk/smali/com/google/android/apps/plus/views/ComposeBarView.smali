.class public Lcom/google/android/apps/plus/views/ComposeBarView;
.super Landroid/widget/FrameLayout;
.source "ComposeBarView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;
    }
.end annotation


# instance fields
.field private mOnComposeBarMeasuredListener:Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ComposeBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ComposeBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ComposeBarView;->mOnComposeBarMeasuredListener:Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ComposeBarView;->mOnComposeBarMeasuredListener:Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ComposeBarView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ComposeBarView;->getMeasuredHeight()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;->onComposeBarMeasured(II)V

    :cond_0
    return-void
.end method

.method public setOnComposeBarMeasuredListener(Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ComposeBarView;->mOnComposeBarMeasuredListener:Lcom/google/android/apps/plus/views/ComposeBarView$OnComposeBarMeasuredListener;

    return-void
.end method

.method public final shouldHighlightOnPress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
