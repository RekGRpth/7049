.class public Lcom/google/android/apps/plus/views/ConstrainedTextView;
.super Landroid/view/View;
.source "ConstrainedTextView.java"


# instance fields
.field private mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

.field private mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mEllipsize:Z

.field private mMaxHeight:I

.field private mMaxLines:I

.field private mText:Ljava/lang/CharSequence;

.field private mTextPaint:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v7, -0x1

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v6, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mEllipsize:Z

    sget-object v4, Lcom/google/android/apps/plus/R$styleable;->ConstrainedTextView:[I

    invoke-virtual {p1, p2, v4, v5, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v4, 0x2

    invoke-virtual {v0, v4, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v6, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    const/4 v4, 0x5

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v4, Landroid/text/TextPaint;

    invoke-direct {v4}, Landroid/text/TextPaint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v6}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v1}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v0, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v1, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move v2, v3

    :goto_0
    return v2

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4, v0, v1, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->handleEvent(III)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->invalidate()V

    goto :goto_0

    :cond_0
    move v2, v3

    goto :goto_0

    :pswitch_2
    iput-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->handleEvent(III)Z

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->invalidate()V

    move v2, v3

    goto :goto_0

    :pswitch_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v4, 0x3

    invoke-interface {v3, v0, v1, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v5, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->invalidate()V

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final getLength()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 16
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v12

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    const/high16 v1, -0x80000000

    if-eq v13, v1, :cond_0

    const/high16 v1, 0x40000000

    if-eq v13, v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    if-gez v1, :cond_0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    if-ltz v1, :cond_5

    :cond_0
    const/4 v10, 0x1

    :goto_0
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    if-ltz v1, :cond_1

    if-nez v13, :cond_6

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    if-eqz v1, :cond_3

    if-nez v13, :cond_2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    if-ltz v1, :cond_7

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v14, v1

    div-int v15, v12, v14

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    if-ltz v1, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    invoke-static {v15, v1}, Ljava/lang/Math;->min(II)I

    move-result v15

    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    if-eqz v1, :cond_9

    if-gtz v15, :cond_4

    if-nez v13, :cond_9

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mEllipsize:Z

    if-eqz v1, :cond_8

    if-eqz v10, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-static {v1, v2, v4, v15, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->createConstrainedLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->setPosition(II)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->getHeight()I

    move-result v11

    :goto_4
    move/from16 v0, p2

    invoke-static {v11, v0}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setMeasuredDimension(II)V

    return-void

    :cond_5
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    invoke-static {v12, v1}, Ljava/lang/Math;->min(II)I

    move-result v12

    goto :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    goto :goto_2

    :cond_8
    new-instance v1, Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v6, 0x3f800000

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    goto :goto_3

    :cond_9
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mContentLayout:Lcom/google/android/apps/plus/views/ClickableStaticLayout;

    const/4 v11, 0x0

    goto :goto_4
.end method

.method public setClickListener(Lcom/google/android/apps/plus/views/ItemClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ItemClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    return-void
.end method

.method public setHtmlText(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-static {p1}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mEllipsize:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxHeight:I

    return-void
.end method

.method public setMaxLines(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mMaxLines:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Z)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Z

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mEllipsize:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mText:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    return-void

    :cond_0
    const-string p1, ""

    goto :goto_0
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    return-void
.end method

.method public setTextPaint(Landroid/text/TextPaint;)V
    .locals 0
    .param p1    # Landroid/text/TextPaint;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    return-void
.end method

.method public setTextSize(F)V
    .locals 1
    .param p1    # F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTextSize(F)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 1
    .param p1    # Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConstrainedTextView;->mTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->requestLayout()V

    return-void
.end method
