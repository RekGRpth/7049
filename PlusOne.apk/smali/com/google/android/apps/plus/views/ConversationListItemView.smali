.class public Lcom/google/android/apps/plus/views/ConversationListItemView;
.super Landroid/widget/RelativeLayout;
.source "ConversationListItemView.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static sBackgroundChecked:Landroid/graphics/drawable/Drawable;

.field private static sBackgroundUnchecked:I

.field private static sDefaultUserImage:Landroid/graphics/Bitmap;

.field private static sInitialized:Z


# instance fields
.field private mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mChecked:Z

.field private mLastMessageTextView:Landroid/widget/TextView;

.field private mMuted:Z

.field private mMutedIcon:Landroid/widget/ImageView;

.field private mNameTextView:Landroid/widget/TextView;

.field private mPosition:I

.field private mTimeSinceTextView:Landroid/widget/TextView;

.field private mUnreadCount:I

.field private mUnreadCountTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/ConversationListItemView;->sInitialized:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/ConversationListItemView;->sInitialized:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->list_selected_holo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ConversationListItemView;->sBackgroundChecked:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$color;->clear:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ConversationListItemView;->sBackgroundUnchecked:I

    :cond_0
    iput v2, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCount:I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMuted:Z

    sget-object v1, Lcom/google/android/apps/plus/views/ConversationListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ConversationListItemView;->sDefaultUserImage:Landroid/graphics/Bitmap;

    :cond_1
    return-void
.end method

.method private refreshUnreadCountView()V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCount:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final clear()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mLastMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mChecked:Z

    return v0
.end method

.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->avatarFull:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->avatarLeftFull:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->avatarUpperLeft:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->avatarLowerLeft:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->avatarUpperRight:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->avatarLowerRight:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->conversationName:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mNameTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->lastMessage:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mLastMessageTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->timeSince:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mTimeSinceTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->unreadCount:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->mutedIcon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMutedIcon:Landroid/widget/ImageView;

    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mChecked:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mChecked:Z

    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/ConversationListItemView;->sBackgroundChecked:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    sget v0, Lcom/google/android/apps/plus/views/ConversationListItemView;->sBackgroundUnchecked:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public setConversationName(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setLastMessage(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mLastMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setMuted(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMuted:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMuted:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMuted:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMutedIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->refreshUnreadCountView()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMutedIcon:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setParticipantsId(Ljava/util/List;Ljava/lang/String;)V
    .locals 7
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v2, 0x8

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v4, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_6

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLeftFullView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerLeftView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarUpperRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mAvatarLowerRightView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mPosition:I

    return-void
.end method

.method public setTimeSince(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setUnreadCount(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCount:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCountTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->refreshUnreadCountView()V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final updateContentDescription()V
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ConversationListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mMuted:Z

    if-eqz v5, :cond_0

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_description_muted:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget v5, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCount:I

    if-lez v5, :cond_1

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_description_unread_count:I

    new-array v6, v9, [Ljava/lang/Object;

    iget v7, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mUnreadCount:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mNameTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_2

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_description_title:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_3

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_description_time_since:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v4, v6, v8

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ConversationListItemView;->mLastMessageTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-lez v5, :cond_4

    sget v5, Lcom/google/android/apps/plus/R$string;->realtimechat_conversation_description_message:I

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v1, v6, v8

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/ConversationListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
