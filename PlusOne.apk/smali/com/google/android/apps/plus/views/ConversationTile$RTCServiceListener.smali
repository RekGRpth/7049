.class final Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;
.super Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;
.source "ConversationTile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ConversationTile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RTCServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/ConversationTile;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/ConversationTile;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-direct {p0}, Lcom/google/android/apps/plus/realtimechat/RealTimeChatServiceListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/ConversationTile;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ConversationTile;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;-><init>(Lcom/google/android/apps/plus/views/ConversationTile;)V

    return-void
.end method


# virtual methods
.method public final onUserPresenceChanged(JLjava/lang/String;Z)V
    .locals 4
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    if-eqz p4, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/views/ConversationTile;

    # getter for: Lcom/google/android/apps/plus/views/ConversationTile;->mActiveParticipantIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/ConversationTile;->access$100(Lcom/google/android/apps/plus/views/ConversationTile;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/views/ConversationTile;

    # getter for: Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/ConversationTile;->access$200(Lcom/google/android/apps/plus/views/ConversationTile;)Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/views/ConversationTile;

    # getter for: Lcom/google/android/apps/plus/views/ConversationTile;->mConversationRowId:Ljava/lang/Long;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/ConversationTile;->access$200(Lcom/google/android/apps/plus/views/ConversationTile;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/views/ConversationTile;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ConversationTile;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/Tile$ParticipantPresenceListener;->onParticipantPresenceChanged()V

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ConversationTile$RTCServiceListener;->this$0:Lcom/google/android/apps/plus/views/ConversationTile;

    # getter for: Lcom/google/android/apps/plus/views/ConversationTile;->mActiveParticipantIds:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/ConversationTile;->access$100(Lcom/google/android/apps/plus/views/ConversationTile;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method
