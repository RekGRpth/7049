.class public Lcom/google/android/apps/plus/views/CoverPhotoImageView;
.super Lcom/google/android/apps/plus/views/ImageResourceView;
.source "CoverPhotoImageView.java"


# static fields
.field private static sFullBleedHeight:I

.field private static sNegativeSpace:I


# instance fields
.field private mCoverPhotoMatrix:Landroid/graphics/Matrix;

.field private mLayout:I

.field private mLayoutHeight:I

.field private mLayoutWidth:I

.field private mOffset:I

.field private mRequiredWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setSizeCategory(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setScaleMode(I)V

    const/high16 v1, 0x3f000000

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setZoomBias(F)V

    sget v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_cover_photo_full_bleed_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sFullBleedHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_cover_photo_negative_space:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setSizeCategory(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setScaleMode(I)V

    const/high16 v1, 0x3f000000

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setZoomBias(F)V

    sget v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_cover_photo_full_bleed_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sFullBleedHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_cover_photo_negative_space:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setSizeCategory(I)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setScaleMode(I)V

    const/high16 v1, 0x3f000000

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setZoomBias(F)V

    sget v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_cover_photo_full_bleed_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sFullBleedHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_cover_photo_negative_space:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    :cond_0
    return-void
.end method


# virtual methods
.method protected final onDrawMedia(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    const/16 v8, 0x3ac

    const/high16 v7, 0x446b0000

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getIntrinsicWidth()I

    move-result v2

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    const/high16 v4, 0x3f800000

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    if-le v5, v6, :cond_2

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    int-to-float v5, v5

    int-to-float v6, v2

    div-float v4, v5, v6

    iget v3, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mOffset:I

    if-ge v3, v8, :cond_4

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    int-to-float v5, v5

    div-float v0, v5, v7

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mOffset:I

    int-to-float v5, v5

    mul-float/2addr v5, v0

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    const/4 v6, 0x0

    int-to-float v7, v1

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget-object v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mCoverPhotoMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setOverrideMatrix(Landroid/graphics/Matrix;)V

    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onDrawMedia(Landroid/graphics/Canvas;)V

    return-void

    :cond_2
    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    if-le v5, v2, :cond_3

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    int-to-float v5, v5

    int-to-float v6, v2

    div-float v4, v5, v6

    :cond_3
    iget v3, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    goto :goto_0

    :cond_4
    if-le v3, v8, :cond_0

    int-to-float v5, v3

    div-float v4, v5, v7

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mOffset:I

    int-to-float v5, v5

    mul-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/4 v8, 0x2

    const/4 v2, 0x1

    const v7, 0x3fe38e39

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getMeasuredWidth()I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    if-ne v6, v8, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v8, :cond_3

    :goto_0
    if-eqz v2, :cond_5

    iput v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->getRootView()Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_0

    sget v5, Lcom/google/android/apps/plus/R$id;->fragment_container:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sget v6, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sNegativeSpace:I

    sub-int/2addr v5, v6

    iput v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    :cond_0
    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    if-gtz v5, :cond_1

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    int-to-float v5, v5

    const v6, 0x4069999a

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    :cond_1
    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    int-to-float v5, v5

    div-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    if-ge v1, v5, :cond_4

    :goto_1
    iput v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    :goto_2
    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setCustomImageSize(II)V

    :cond_2
    :goto_3
    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setMeasuredDimension(II)V

    return-void

    :cond_3
    move v2, v5

    goto :goto_0

    :cond_4
    iget v1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    goto :goto_1

    :cond_5
    iget v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    int-to-float v5, v5

    div-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    goto :goto_2

    :cond_6
    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    if-ne v6, v2, :cond_2

    sget v6, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->sFullBleedHeight:I

    iput v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    if-eqz v6, :cond_2

    iget v3, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayoutHeight:I

    int-to-float v6, v6

    const v7, 0x40a71c72

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    iget v6, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    if-le v6, v3, :cond_7

    iget v3, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mRequiredWidth:I

    :cond_7
    const/16 v6, 0x3ac

    if-le v3, v6, :cond_8

    const/16 v3, 0x3ac

    :cond_8
    invoke-virtual {p0, v3, v5}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setCustomImageSize(II)V

    goto :goto_3
.end method

.method public setLayout(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "COVER"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "FULL_BLEED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mLayout:I

    goto :goto_0
.end method

.method public setTopOffset(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mOffset:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->mOffset:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->invalidate()V

    :cond_0
    return-void
.end method
