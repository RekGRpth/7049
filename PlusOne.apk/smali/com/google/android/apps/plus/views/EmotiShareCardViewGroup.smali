.class public Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "EmotiShareCardViewGroup.java"


# instance fields
.field private mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

.field protected mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

.field protected mDbEmbedEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

.field protected mDestRect:Landroid/graphics/Rect;

.field private mDestRectF:Landroid/graphics/RectF;

.field private mIsShowingDrawable:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMatrixInverse:Landroid/graphics/Matrix;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field protected mSrcRect:Landroid/graphics/Rect;

.field private mSrcRectF:Landroid/graphics/RectF;

.field protected mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrixInverse:Landroid/graphics/Matrix;

    return-void
.end method

.method private hasBitmap(Lcom/google/android/apps/plus/service/Resource;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/service/Resource;

    invoke-static {p1}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->hasImage(Lcom/google/android/apps/plus/service/Resource;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static hasImage(Lcom/google/android/apps/plus/service/Resource;)Z
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/service/Resource;

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isAnimationSupported()Z
    .locals 2

    sget-object v0, Lcom/google/android/apps/plus/util/Property;->ENABLE_STREAM_GIF_ANIMATION:Lcom/google/android/apps/plus/util/Property;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/util/Property;->getBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/phone/EsApplication;->sMemoryClass:I

    const/16 v1, 0x40

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->bindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->isAnimationSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    sget-object v0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_1
    return-void
.end method

.method protected final createHero(III)I
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-float v1, p3

    const v2, 0x3fb9999a

    div-float/2addr v1, v2

    float-to-int v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    add-int v2, p1, p3

    add-int v3, p2, v0

    invoke-virtual {v1, p1, p2, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->setEmpty()V

    add-int v1, p2, v0

    return v1
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->hasBitmap(Lcom/google/android/apps/plus/service/Resource;)Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-static {v3}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->hasImage(Lcom/google/android/apps/plus/service/Resource;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/apps/plus/util/GifImage;

    if-eqz v3, :cond_3

    move v3, v4

    :goto_0
    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    new-instance v6, Lcom/google/android/apps/plus/util/GifDrawable;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/util/GifImage;

    invoke-direct {v6, v3}, Lcom/google/android/apps/plus/util/GifDrawable;-><init>(Lcom/google/android/apps/plus/util/GifImage;)V

    iput-object v6, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->isAnimationSupported()Z

    move-result v6

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v3, Lcom/google/android/apps/plus/util/GifDrawable;

    invoke-virtual {v3, v6}, Lcom/google/android/apps/plus/util/GifDrawable;->setAnimationEnabled(Z)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_4

    instance-of v3, v0, Lcom/google/android/apps/plus/util/GifDrawable;

    if-eqz v3, :cond_1

    move-object v3, v0

    check-cast v3, Lcom/google/android/apps/plus/util/GifDrawable;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/util/GifDrawable;->isValid()Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    move v1, v4

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v3, p2, p3}, Landroid/graphics/Rect;->offsetTo(II)V

    if-eqz v1, :cond_8

    if-nez v0, :cond_5

    move p3, v5

    :cond_2
    :goto_2
    return p3

    :cond_3
    move v3, v5

    goto :goto_0

    :cond_4
    move v1, v5

    goto :goto_1

    :cond_5
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mIsShowingDrawable:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    :cond_6
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    invoke-virtual {v3, v5, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRectF:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRectF:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRectF:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRectF:Landroid/graphics/RectF;

    sget-object v8, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v6, v7, v8}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrix:Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {v3, v6}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {v3}, Landroid/graphics/Matrix;->reset()V

    :cond_7
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v0, v5, v5, v3, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mIsShowingDrawable:Z

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr p3, v3

    goto :goto_2

    :cond_8
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->hasBitmap(Lcom/google/android/apps/plus/service/Resource;)Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    :goto_3
    if-nez v3, :cond_a

    move p3, v5

    :goto_4
    goto/16 :goto_2

    :cond_9
    const/4 v3, 0x0

    goto :goto_3

    :cond_a
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mIsShowingDrawable:Z

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->setEmpty()V

    :cond_b
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v4, v5, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    :cond_c
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    sget-object v7, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mIsShowingDrawable:Z

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    add-int/2addr p3, v3

    goto :goto_4
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v1, 0x24

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDbEmbedEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDbEmbedEmotiShare:Lcom/google/android/apps/plus/content/DbEmbedEmotishare;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedEmotishare;->getImageRef()Lcom/google/android/apps/plus/api/MediaRef;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    return-void
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mDestRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mSrcRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mMatrixInverse:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mIsShowingDrawable:Z

    return-void
.end method

.method public unbindResources()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mStaticImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_2
    iput-object v1, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EmotiShareCardViewGroup;->mAnimatedDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    goto :goto_0
.end method
