.class public Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;
.super Landroid/widget/ImageSwitcher;
.source "EsAlbumCoverSwitcher.java"


# instance fields
.field private mCurrentRefIndex:I

.field private mRefArray:[Lcom/google/android/apps/plus/api/MediaRef;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ImageSwitcher;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->removeAllViews()V

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ImageSwitcher;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->getChildCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setRefs([Lcom/google/android/apps/plus/api/MediaRef;Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 6
    .param p1    # [Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # Lcom/google/android/apps/plus/api/MediaRef;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->mRefArray:[Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    const/4 v2, 0x0

    move-object v0, p1

    array-length v3, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {p2, v4}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iput v2, p0, Lcom/google/android/apps/plus/views/EsAlbumCoverSwitcher;->mCurrentRefIndex:I

    :cond_1
    return-void
.end method
