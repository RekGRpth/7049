.class public Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;
.super Lcom/google/android/apps/plus/views/CardViewLayout;
.source "EventActivityUpdateCardLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sAvatarMarginLeft:I

.field private static sAvatarMarginTop:I

.field private static sAvatarSize:I

.field private static sDescriptionMarginBottom:I

.field private static sDescriptionMarginLeft:I

.field private static sDescriptionMarginRight:I

.field private static sDescriptionTopAvatarHeightPercentage:F

.field private static sInitialized:Z


# instance fields
.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

.field private mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/android/apps/plus/views/EventUpdate;Lcom/google/android/apps/plus/views/EventActionListener;Z)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/views/EventUpdate;
    .param p2    # Lcom/google/android/apps/plus/views/EventActionListener;
    .param p3    # Z

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/EventUpdate;->ownerName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/EventUpdate;->ownerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/EventUpdate;->ownerName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-wide v3, v3, Lcom/google/android/apps/plus/views/EventUpdate;->timestamp:J

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/EventUpdate;->comment:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, p3}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/EventUpdate;->gaiaId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/EventUpdate;->gaiaId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    :cond_2
    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->setListener(Lcom/google/android/apps/plus/views/EventActionListener;)V

    goto :goto_0
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardViewLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_text_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_text_margin_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionMarginRight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_padding_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_margin_left:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarMarginLeft:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_magin_top:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarMarginTop:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_text_top_avatar_percentage:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionTopAvatarHeightPercentage:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_activity_avatar_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarSize:I

    sput-boolean v5, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sInitialized:Z

    :cond_0
    sget v1, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarMarginLeft:I

    sget v2, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarMarginTop:I

    sget v3, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionMarginRight:I

    sget v4, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionMarginBottom:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->addPadding(IIII)V

    new-instance v1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/AvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/AvatarView;->setRounded(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected measureChildren(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    add-int/lit8 v2, v7, 0x0

    add-int/lit8 v0, v1, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v10, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarSize:I

    const/high16 v11, 0x40000000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    sget v11, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sAvatarSize:I

    const/high16 v12, 0x40000000

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/google/android/apps/plus/views/AvatarView;->measure(II)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->setCorner(Landroid/view/View;II)V

    sget v9, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionTopAvatarHeightPercentage:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/AvatarView;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    add-int/lit8 v5, v9, 0x0

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/views/AvatarView;->getMeasuredWidth()I

    move-result v9

    add-int/lit8 v9, v9, 0x0

    sget v10, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->sDescriptionMarginLeft:I

    add-int v4, v9, v10

    sub-int v6, v2, v4

    sub-int v3, v0, v5

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    if-nez v9, :cond_0

    const/4 v8, 0x1

    :goto_0
    iget-object v10, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    const/high16 v9, 0x40000000

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    if-eqz v8, :cond_1

    const/4 v9, 0x0

    :goto_1
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v10, v11, v9}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->measure(II)V

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-static {v9, v4, v5}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->setCorner(Landroid/view/View;II)V

    if-nez v8, :cond_2

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {p0, v9}, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->setClickable(Z)V

    return-void

    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    const/high16 v9, -0x80000000

    goto :goto_1

    :cond_2
    const/4 v9, 0x0

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    check-cast p1, Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onAvatarClicked(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mUpdate:Lcom/google/android/apps/plus/views/EventUpdate;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventActionListener;->onUpdateCardClicked(Lcom/google/android/apps/plus/views/EventUpdate;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardViewLayout;->onRecycle()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v2, v2, v1}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mTextDescriptionView:Lcom/google/android/apps/plus/views/CardTitleDescriptionView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CardTitleDescriptionView;->setListener(Lcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventActivityUpdateCardLayout;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
