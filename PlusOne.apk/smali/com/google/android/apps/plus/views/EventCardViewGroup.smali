.class public Lcom/google/android/apps/plus/views/EventCardViewGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "EventCardViewGroup.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# instance fields
.field private mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->bindResources()V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->bindResources(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method protected final createHero(III)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->layout(Landroid/content/Context;III)I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 1
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    invoke-virtual {v0, p3, p1}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->draw$680d9d43(ILandroid/graphics/Canvas;)I

    move-result v0

    add-int/2addr v0, p3

    return v0
.end method

.method public final getOneUpIntent(Z)Landroid/content/Intent;
    .locals 3
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getOneUpIntent(Z)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "event_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "owner_id"

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 3
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v1, 0xf

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-virtual {v1, p0, v2}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->bind(Lcom/google/android/apps/plus/service/ResourceConsumer;Lcom/google/api/services/plusi/model/PlusEvent;)V

    return-void
.end method

.method public onRecycle()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->clear()V

    return-void
.end method

.method public unbindResources()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventCardViewGroup;->mDrawer:Lcom/google/android/apps/plus/views/StreamEventCardDrawer;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/StreamEventCardDrawer;->unbindResources(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    return-void
.end method
