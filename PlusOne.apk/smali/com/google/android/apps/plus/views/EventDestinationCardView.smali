.class public Lcom/google/android/apps/plus/views/EventDestinationCardView;
.super Lcom/google/android/apps/plus/views/CardView;
.source "EventDestinationCardView.java"


# instance fields
.field private mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mIgnoreHeight:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/EventCardDrawer;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setPaddingEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setFocusable(Z)V

    return-void
.end method


# virtual methods
.method public final bindData$5ae0320(Lcom/google/api/services/plusi/model/PlusEvent;)V
    .locals 2
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mItemClickListener:Lcom/google/android/apps/plus/views/ItemClickListener;

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/android/apps/plus/views/EventCardDrawer;->bind(Lcom/google/android/apps/plus/views/CardView;Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->requestLayout()V

    return-void
.end method

.method protected final draw(Landroid/graphics/Canvas;IIII)I
    .locals 2
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    add-int v1, p3, p5

    invoke-virtual {v0, p3, v1, p1}, Lcom/google/android/apps/plus/views/EventCardDrawer;->draw$680d9d43(IILandroid/graphics/Canvas;)I

    move-result v0

    return v0
.end method

.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->name:Ljava/lang/String;

    invoke-static {v5, v8}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v9, v9, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v10, 0x0

    invoke-static {v8, v9, v10, v11}, Lcom/google/android/apps/plus/util/EventDateUtils;->getDateRange(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;Lcom/google/api/services/plusi/model/EventTime;Z)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsEventData;->isEventHangout(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v8, v0, v1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getDisplayString(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v8, :cond_0

    sget v9, Lcom/google/android/apps/plus/R$string;->event_location_accessibility_description:I

    new-array v10, v11, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    :goto_0
    aput-object v8, v10, v12

    invoke-virtual {v3, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    :cond_0
    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v8}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpStatus(Lcom/google/api/services/plusi/model/PlusEvent;)I

    move-result v8

    packed-switch v8, :pswitch_data_0

    :goto_1
    invoke-static {v5, v4}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v8, v8, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto :goto_0

    :pswitch_0
    sget v8, Lcom/google/android/apps/plus/R$string;->event_rsvp_accessibility_description:I

    new-array v9, v11, [Ljava/lang/Object;

    sget v10, Lcom/google/android/apps/plus/R$string;->card_event_going_prompt:I

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v3, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_1
    sget v8, Lcom/google/android/apps/plus/R$string;->event_rsvp_accessibility_description:I

    new-array v9, v11, [Ljava/lang/Object;

    sget v10, Lcom/google/android/apps/plus/R$string;->card_event_declined_prompt:I

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v3, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_2
    sget v8, Lcom/google/android/apps/plus/R$string;->event_rsvp_accessibility_description:I

    new-array v9, v11, [Ljava/lang/Object;

    sget v10, Lcom/google/android/apps/plus/R$string;->card_event_maybe_prompt:I

    invoke-virtual {v3, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-virtual {v3, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_3
    sget v8, Lcom/google/android/apps/plus/R$string;->card_event_invited_prompt:I

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final getEvent()Lcom/google/api/services/plusi/model/PlusEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-object v0
.end method

.method protected final layoutElements(IIII)I
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    move v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/EventCardDrawer;->layout(IIZII)I

    move-result v0

    return v0
.end method

.method protected final onBindResources()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onBindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->onBindResources()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    if-eqz v4, :cond_2

    move v0, v3

    :goto_1
    sget v4, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sLeftBorderPadding:I

    sget v6, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sTopBorderPadding:I

    sget v7, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sLeftBorderPadding:I

    sget v8, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sRightBorderPadding:I

    add-int/2addr v7, v8

    sub-int v7, v3, v7

    sget v8, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sTopBorderPadding:I

    sget v9, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sBottomBorderPadding:I

    add-int/2addr v8, v9

    sub-int v8, v0, v8

    invoke-virtual {p0, v4, v6, v7, v8}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->layoutElements(IIII)I

    move-result v2

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mIgnoreHeight:Z

    if-eqz v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sTopBorderPadding:I

    add-int/2addr v4, v2

    sget v6, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sBottomBorderPadding:I

    add-int/2addr v4, v6

    sget v6, Lcom/google/android/apps/plus/views/EventDestinationCardView;->sYPadding:I

    add-int v0, v4, v6

    :cond_0
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->setMeasuredDimension(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDestinationCardView;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v4, v5, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    return-void

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public onRecycle()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onRecycle()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    return-void
.end method

.method protected final onUnbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDestinationCardView;->mDrawer:Lcom/google/android/apps/plus/views/EventCardDrawer;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventCardDrawer;->onUnbindResources()V

    invoke-super {p0}, Lcom/google/android/apps/plus/views/CardView;->onUnbindResources()V

    return-void
.end method
