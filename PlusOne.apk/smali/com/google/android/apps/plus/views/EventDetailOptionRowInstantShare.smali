.class public Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;
.super Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;
.source "EventDetailOptionRowInstantShare.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sAfterInstantDescription:Ljava/lang/String;

.field private static sBeforeInstantDescription:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sInstantShareDrawable:Landroid/graphics/drawable/Drawable;

.field private static sInstantTitle:Ljava/lang/String;


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

.field private mInstantIcon:Landroid/widget/ImageView;

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/android/apps/plus/fragments/EventActiveState;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/fragments/EventActiveState;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v0, p1, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v1, p1, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setClickable(Z)V

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sInstantTitle:Ljava/lang/String;

    iget-boolean v0, p1, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareExpired:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sAfterInstantDescription:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mInstantIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    invoke-super {p0, v1, v0, v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->bind(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setClickable(Z)V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sBeforeInstantDescription:Ljava/lang/String;

    goto :goto_1
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x0

    const/4 v3, -0x2

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_party_mode_1up:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sInstantShareDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$string;->instant_share_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sBeforeInstantDescription:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->instant_share_after_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sAfterInstantDescription:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_instantshare_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sInstantTitle:Ljava/lang/String;

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sInitialized:Z

    :cond_0
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mInstantIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mInstantIcon:Landroid/widget/ImageView;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->sInstantShareDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v1, Landroid/widget/CheckBox;

    invoke-direct {v1, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setClickable(Z)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    if-ne p0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mEventState:Lcom/google/android/apps/plus/fragments/EventActiveState;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/fragments/EventActiveState;->isInstantShareEnabled:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onInstantShareToggle(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/EventActionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowInstantShare;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    return-void
.end method
