.class public Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;
.super Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;
.source "EventDetailOptionRowLocation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sDirectionIcon:Landroid/graphics/drawable/Drawable;

.field private static sHangoutIcon:Landroid/graphics/drawable/Drawable;

.field private static sHangoutJoinIcon:Landroid/graphics/drawable/Drawable;

.field private static sHangoutJoinText:Ljava/lang/String;

.field private static sHangoutTitle:Ljava/lang/String;

.field private static sHangoutbeforeText:Ljava/lang/String;

.field private static sLocationIcon:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mLaunchIcon:Landroid/widget/ImageView;

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mLocation:Z

.field private mTypeIcon:Landroid/widget/ImageView;

.field private sInitialized:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 8
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/views/EventActionListener;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLocation:Z

    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLocation:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mTypeIcon:Landroid/widget/ImageView;

    sget-object v5, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sLocationIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLaunchIcon:Landroid/widget/ImageView;

    sget-object v5, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sDirectionIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v2, v4, Lcom/google/api/services/plusi/model/Place;->description:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/Place;->address:Lcom/google/api/services/plusi/model/EmbedsPostalAddress;

    iget-object v0, v4, Lcom/google/api/services/plusi/model/EmbedsPostalAddress;->name:Ljava/lang/String;

    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mTypeIcon:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLaunchIcon:Landroid/widget/ImageView;

    invoke-super {p0, v2, v0, v4, v5}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->bind(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    :goto_2
    return-void

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    iget-object v4, p1, Lcom/google/api/services/plusi/model/PlusEvent;->location:Lcom/google/api/services/plusi/model/Place;

    iget-object v2, v4, Lcom/google/api/services/plusi/model/Place;->name:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mTypeIcon:Landroid/widget/ImageView;

    sget-object v5, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutbeforeText:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLaunchIcon:Landroid/widget/ImageView;

    sget-object v5, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutJoinIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLaunchIcon:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutJoinText:Ljava/lang/String;

    :cond_3
    sget-object v4, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mTypeIcon:Landroid/widget/ImageView;

    invoke-super {p0, v4, v1, v5, v3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->bind(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;Landroid/view/View;)V

    goto :goto_2
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_details_location:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sLocationIcon:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_directions:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sDirectionIcon:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_hangout_1up:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutIcon:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_hangout_title:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutTitle:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_hangout_before:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutbeforeText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_detail_hangout_during:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutJoinText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_right:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sHangoutJoinIcon:Landroid/graphics/drawable/Drawable;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->sInitialized:Z

    :cond_0
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mTypeIcon:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLaunchIcon:Landroid/widget/ImageView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->setClickable(Z)V

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mLocation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onLocationClicked()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowLocation;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onHangoutClicked()V

    goto :goto_0
.end method
