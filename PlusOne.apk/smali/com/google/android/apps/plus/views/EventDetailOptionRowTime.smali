.class public Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;
.super Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;
.source "EventDetailOptionRowTime.java"


# static fields
.field private static sClockIconDrawabale:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mClockIcon:Landroid/widget/ImageView;

.field private sInitialized:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bind$3ba8bae(Lcom/google/api/services/plusi/model/PlusEvent;)V
    .locals 20
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;

    const/4 v15, 0x0

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->getContext()Landroid/content/Context;

    move-result-object v13

    const/4 v7, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getSystemTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v7

    :cond_0
    const/16 v17, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v2, :cond_1

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v2, :cond_1

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v5, v5, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const/4 v8, 0x1

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/plus/util/EventDateUtils;->shouldShowDay(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v17

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v3, 0x0

    move/from16 v0, v17

    invoke-static {v13, v2, v3, v7, v0}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v15

    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const/4 v11, 0x0

    move-object v10, v7

    invoke-static/range {v5 .. v11}, Lcom/google/android/apps/plus/util/EventDateUtils;->shouldShowDay(JLjava/util/TimeZone;JLjava/util/TimeZone;Z)Z

    move-result v16

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/api/services/plusi/model/PlusEvent;->endTime:Lcom/google/api/services/plusi/model/EventTime;

    const/4 v4, 0x1

    if-eqz v17, :cond_5

    if-eqz v16, :cond_5

    const/4 v2, 0x1

    :goto_0
    invoke-static {v13, v3, v4, v7, v2}, Lcom/google/android/apps/plus/util/EventDateUtils;->getSingleDisplayLine(Landroid/content/Context;Lcom/google/api/services/plusi/model/EventTime;ZLjava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v14

    :cond_2
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    if-eqz v14, :cond_3

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timeMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v12, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->startTime:Lcom/google/api/services/plusi/model/EventTime;

    iget-object v2, v2, Lcom/google/api/services/plusi/model/EventTime;->timezone:Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventHangout(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    invoke-static {v2, v12, v3}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->getDisplayString(Ljava/lang/String;Ljava/util/Calendar;Z)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_4

    invoke-virtual/range {v18 .. v19}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->mClockIcon:Landroid/widget/ImageView;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-super {v0, v15, v1, v2, v3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->bind(Ljava/lang/String;Ljava/util/List;Landroid/view/View;Landroid/view/View;)V

    return-void

    :cond_5
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected final init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionRowLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {p1}, Lcom/google/android/apps/plus/util/TimeZoneHelper;->initialize(Landroid/content/Context;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_details_time:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sClockIconDrawabale:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sInitialized:Z

    :cond_0
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->mClockIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->mClockIcon:Landroid/widget/ImageView;

    sget-object v2, Lcom/google/android/apps/plus/views/EventDetailOptionRowTime;->sClockIconDrawabale:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
