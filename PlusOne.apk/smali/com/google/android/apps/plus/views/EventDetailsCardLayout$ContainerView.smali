.class Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;
.super Landroid/view/ViewGroup;
.source "EventDetailsCardLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/EventDetailsCardLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContainerView"
.end annotation


# instance fields
.field private mDividerLeft:I

.field private mDividerPaint:Landroid/graphics/Paint;

.field private mDividerTop:I

.field private mDrawDivider:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->card_event_divider:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_divider_stroke_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method public final clearDivider()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDrawDivider:Z

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDrawDivider:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerLeft:I

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerTop:I

    int-to-float v2, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerLeft:I

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->getMeasuredHeight()I

    move-result v0

    # getter for: Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->sPaddingBottom:I
    invoke-static {}, Lcom/google/android/apps/plus/views/EventDetailsCardLayout;->access$000()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    return-void
.end method

.method public final setDivider(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDrawDivider:Z

    iput p1, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerLeft:I

    iput p2, p0, Lcom/google/android/apps/plus/views/EventDetailsCardLayout$ContainerView;->mDividerTop:I

    return-void
.end method
