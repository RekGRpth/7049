.class public Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsOptionTitleDescription.java"


# static fields
.field private static sDescriptionColor:I

.field private static sDescriptionSize:F

.field private static sInitialized:Z

.field private static sTitleColor:I

.field private static sTitleSize:F


# instance fields
.field private mActiveDescriptionCount:I

.field private mAttrs:Landroid/util/AttributeSet;

.field private mContext:Landroid/content/Context;

.field private mDefStyle:I

.field private mDescriptionViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x1

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mAttrs:Landroid/util/AttributeSet;

    iput p3, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDefStyle:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget-boolean v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sInitialized:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$color;->event_card_details_option_title_color:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleColor:I

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_details_option_title_size:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleSize:F

    sget v0, Lcom/google/android/apps/plus/R$color;->event_card_details_option_description_color:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionColor:I

    sget v0, Lcom/google/android/apps/plus/R$dimen;->event_card_details_option_description_size:I

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionSize:F

    sput-boolean v5, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sInitialized:Z

    :cond_0
    sget v3, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sTitleColor:I

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-nez p1, :cond_0

    move-object p1, p2

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->bind(Ljava/lang/String;Ljava/util/List;)V

    return-void

    :cond_0
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final bind(Ljava/lang/String;Ljava/util/List;)V
    .locals 13
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v10, v0, -0x1

    :goto_1
    if-ltz v10, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->removeView(Landroid/view/View;)V

    add-int/lit8 v10, v10, -0x1

    goto :goto_1

    :cond_0
    move v0, v5

    goto :goto_0

    :cond_1
    iput v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mActiveDescriptionCount:I

    if-eqz p2, :cond_4

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    const/4 v9, 0x0

    :goto_2
    if-ge v9, v7, :cond_4

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le v9, v0, :cond_2

    iget-object v12, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mAttrs:Landroid/util/AttributeSet;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDefStyle:I

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionSize:F

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->sDescriptionColor:I

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/views/TextViewUtils;->createText(Landroid/content/Context;Landroid/util/AttributeSet;IFIZZ)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->addView(Landroid/view/View;)V

    iget v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mActiveDescriptionCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mActiveDescriptionCount:I

    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public final clear()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->removeView(Landroid/view/View;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mActiveDescriptionCount:I

    return-void
.end method

.method protected measureChildren(II)V
    .locals 8
    .param p1    # I
    .param p2    # I

    const/high16 v7, -0x80000000

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-static {v5, v4, v7, v1, v6}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->measure(Landroid/view/View;IIII)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-static {v5, v6, v6}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->setCorner(Landroid/view/View;II)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v6

    :goto_0
    add-int/lit8 v0, v5, 0x0

    const/4 v2, 0x0

    :goto_1
    iget v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mActiveDescriptionCount:I

    if-ge v2, v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mDescriptionViews:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3, v4, v7, v1, v6}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->measure(Landroid/view/View;IIII)V

    invoke-static {v3, v6, v0}, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->setCorner(Landroid/view/View;II)V

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v0, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventDetailsOptionTitleDescription;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    goto :goto_0

    :cond_1
    return-void
.end method
