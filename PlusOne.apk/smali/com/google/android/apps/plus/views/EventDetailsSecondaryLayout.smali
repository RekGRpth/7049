.class public Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventDetailsSecondaryLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static sInitialized:Z

.field private static sPadding:I

.field private static sSeeInviteesTextColor:I

.field private static sSeeInvitesHeight:I

.field private static sSeeInvitesText:Ljava/lang/String;

.field private static sSeeInvitesTextSize:F


# instance fields
.field private mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

.field private mHideInvitees:Z

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mViewInvitees:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-boolean v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_secondary_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sPadding:I

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_details_see_invitees_color:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInviteesTextColor:I

    sget v2, Lcom/google/android/apps/plus/R$string;->event_button_view_all_invitees:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesText:Ljava/lang/String;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_see_invitees_size:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesTextSize:F

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_see_invitees_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesHeight:I

    sput-boolean v5, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sInitialized:Z

    :cond_0
    new-instance v2, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget-object v3, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesTextSize:F

    invoke-virtual {v2, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget v3, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInviteesTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->Theme:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    const/4 v3, 0x5

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    sget v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sPadding:I

    invoke-virtual {p0, v4, v4, v4, v2}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->addPadding(IIII)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 8
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/fragments/EventActiveState;
    .param p3    # Lcom/google/android/apps/plus/views/EventActionListener;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getEventEndTime(Lcom/google/api/services/plusi/model/PlusEvent;)J

    move-result-wide v6

    cmp-long v6, v0, v6

    if-lez v6, :cond_0

    move v3, v4

    :goto_0
    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    if-eqz v6, :cond_1

    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventOptions;->hideGuestList:Ljava/lang/Boolean;

    if-eqz v6, :cond_1

    iget-object v6, p1, Lcom/google/api/services/plusi/model/PlusEvent;->eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

    iget-object v6, v6, Lcom/google/api/services/plusi/model/EventOptions;->hideGuestList:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    iget-boolean v6, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isOwner:Z

    if-nez v6, :cond_1

    :goto_1
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mHideInvitees:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mHideInvitees:Z

    if-eqz v4, :cond_2

    const/16 v2, 0x8

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v4, p1, p3, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/views/EventActionListener;Z)V

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->invalidate()V

    return-void

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_1

    :cond_2
    move v2, v5

    goto :goto_2
.end method

.method public final clear()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    return-void
.end method

.method protected measureChildren(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/high16 v6, 0x40000000

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mHideInvitees:Z

    if-nez v2, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mGuestSummary:Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->size()I

    move-result v2

    if-lez v2, :cond_1

    sget v2, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sPadding:I

    add-int/lit8 v2, v2, 0x0

    invoke-static {v4, v1, v6, v3, v3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->measure(Landroid/view/View;IIII)V

    invoke-static {v4, v3, v3}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->setCorner(Landroid/view/View;II)V

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {v4, v3}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setVisibility(I)V

    :goto_0
    add-int/lit8 v0, v2, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->sSeeInvitesHeight:I

    invoke-static {v2, v1, v6, v4, v6}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->measure(Landroid/view/View;IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->setCorner(Landroid/view/View;II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    :cond_0
    return-void

    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/EventInviteeSummaryLayout;->setVisibility(I)V

    move v2, v3

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mViewInvitees:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventDetailsSecondaryLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onViewAllInviteesClicked()V

    :cond_0
    return-void
.end method
