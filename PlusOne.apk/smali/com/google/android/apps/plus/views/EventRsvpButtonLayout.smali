.class public Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;
.super Landroid/widget/LinearLayout;
.source "EventRsvpButtonLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

.field private mMaybeDivider:Landroid/view/View;

.field private mMaybeView:Landroid/view/View;

.field private mNoView:Landroid/view/View;

.field private mYesView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/android/apps/plus/views/EventRsvpListener;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/views/EventRsvpListener;
    .param p2    # Z

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    if-eqz p2, :cond_0

    const/16 v0, 0x8

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeDivider:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mYesView:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    const-string v1, "ATTENDING"

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    const-string v1, "MAYBE"

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mNoView:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    const-string v1, "NOT_ATTENDING"

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/apps/plus/R$id;->maybeButton:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->yesButton:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mYesView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mYesView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->noButton:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mNoView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mNoView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->maybeDivider:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->mMaybeDivider:Landroid/view/View;

    return-void
.end method
