.class public Lcom/google/android/apps/plus/views/EventRsvpLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventRsvpLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/EventRsvpListener;


# static fields
.field private static sBackgroundColor:I

.field private static sDividerPaint:Landroid/graphics/Paint;

.field private static sInitialized:Z

.field private static sRsvpSectionHeight:I


# instance fields
.field private mEventOver:Z

.field private mListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mMeasuredWidth:I

.field private mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

.field private mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    sget-boolean v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$color;->event_card_details_rsvp_background:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sBackgroundColor:I

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_divider:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_divider_stroke_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    sput-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$color;->card_event_divider:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->event_card_divider_stroke_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget v2, Lcom/google/android/apps/plus/R$dimen;->event_card_details_rsvp_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sRsvpSectionHeight:I

    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sInitialized:Z

    :cond_0
    sget v2, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sBackgroundColor:I

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setBackgroundColor(I)V

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/plus/R$layout;->event_rsvp_button_layout:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->addView(Landroid/view/View;)V

    new-instance v2, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->addView(Landroid/view/View;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setWillNotDraw(Z)V

    return-void
.end method

.method private setRsvpView(Ljava/lang/String;Z)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v7, 0x4

    const/high16 v9, 0x3f800000

    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v6, "MAYBE"

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "NOT_RESPONDED"

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_0
    move v3, v5

    :goto_0
    const-string v6, "NOT_RESPONDED"

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mEventOver:Z

    if-eqz v6, :cond_1

    if-nez v3, :cond_4

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getVisibility()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->setVisibility(I)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setVisibility(I)V

    if-eqz p2, :cond_2

    if-nez v1, :cond_2

    new-instance v2, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v2, v9, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v6, 0x1f4

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v2, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, v5}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v3, v4

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v5, v4}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->setVisibility(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v4, v7}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/fragments/EventActiveState;
    .param p3    # Lcom/google/android/apps/plus/views/EventActionListener;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mEventOver:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v2, p1, p2, p0, p3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventRsvpListener;Lcom/google/android/apps/plus/views/EventActionListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mEventOver:Z

    invoke-virtual {v2, p0, v3}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->bind(Lcom/google/android/apps/plus/views/EventRsvpListener;Z)V

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setRsvpView(Ljava/lang/String;Z)V

    return-void
.end method

.method protected measureChildren(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    sget v0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sRsvpSectionHeight:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v3, v3}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v0, v4}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpButtonLayout:Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventRsvpButtonLayout;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setCorner(Landroid/view/View;II)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    invoke-static {v1, v2, v4, v0, v4}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->measure(Landroid/view/View;IIII)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mRsvpSpinnerLayout:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setCorner(Landroid/view/View;II)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mMeasuredWidth:I

    int-to-float v3, v0

    sget-object v5, Lcom/google/android/apps/plus/views/EventRsvpLayout;->sDividerPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public final onRsvpChanged(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/EventRsvpLayout;->setRsvpView(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpLayout;->mListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0, p1}, Lcom/google/android/apps/plus/views/EventActionListener;->onRsvpChanged(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
