.class public Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;
.super Lcom/google/android/apps/plus/views/ExactLayout;
.source "EventRsvpSpinnerLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;
    }
.end annotation


# static fields
.field private static sAddPhotosDrawable:Landroid/graphics/drawable/Drawable;

.field private static sAddPhotosText:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sInviteMoreDrawable:Landroid/graphics/drawable/Drawable;

.field private static sInviteMoreText:Ljava/lang/String;

.field private static sPadding:I


# instance fields
.field private mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

.field private mCurrentSelectionIndex:I

.field private mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

.field private mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

.field private mEventOver:Z

.field private mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

.field private mRsvpSpinner:Landroid/widget/Spinner;

.field private mShowActionButton:Z

.field private mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ExactLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->init(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    sget-boolean v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->event_card_details_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_rsvp_invite_more:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_button_invite_more_label:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreText:Ljava/lang/String;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->icn_events_rsvp_add_photo:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosDrawable:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$string;->event_button_add_photos_label:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosText:Ljava/lang/String;

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInitialized:Z

    :cond_0
    new-instance v1, Landroid/widget/Spinner;

    invoke-direct {v1, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    new-instance v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->addView(Landroid/view/View;)V

    sget v1, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v2, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sget v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setPadding(IIII)V

    return-void
.end method


# virtual methods
.method public final bind(Lcom/google/api/services/plusi/model/PlusEvent;Lcom/google/android/apps/plus/fragments/EventActiveState;Lcom/google/android/apps/plus/views/EventRsvpListener;Lcom/google/android/apps/plus/views/EventActionListener;)V
    .locals 6
    .param p1    # Lcom/google/api/services/plusi/model/PlusEvent;
    .param p2    # Lcom/google/android/apps/plus/fragments/EventActiveState;
    .param p3    # Lcom/google/android/apps/plus/views/EventRsvpListener;
    .param p4    # Lcom/google/android/apps/plus/views/EventActionListener;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/plus/content/EsEventData;->isEventOver(Lcom/google/api/services/plusi/model/PlusEvent;J)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    new-instance v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;-><init>(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;Landroid/content/Context;Z)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v3, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->temporalRsvpValue:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v2, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->temporalRsvpValue:Ljava/lang/String;

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-static {v3, v2}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->notifyDataSetChanged()V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    iget-boolean v4, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->isRsvpEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setEnabled(Z)V

    iget v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    const-string v5, "ATTENDING"

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    invoke-static {v3}, Lcom/google/android/apps/plus/content/EsEventData;->canViewerAddPhotos(Lcom/google/api/services/plusi/model/PlusEvent;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-nez v3, :cond_3

    iget-boolean v3, p2, Lcom/google/android/apps/plus/fragments/EventActiveState;->canInviteOthers:Z

    if-eqz v3, :cond_3

    :cond_1
    const/4 v3, 0x1

    :goto_1
    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    return-void

    :cond_2
    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getRsvpType(Lcom/google/api/services/plusi/model/PlusEvent;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method protected measureChildren(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    if-nez v2, :cond_0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-static {v2, v1, v4, v5, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->measure(Landroid/view/View;IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-static {v2, v5, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setCorner(Landroid/view/View;II)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mRsvpSpinner:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/Spinner;->getMeasuredHeight()I

    move-result v3

    invoke-static {v2, v1, v4, v3, v4}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->measure(Landroid/view/View;IIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    add-int/lit8 v3, v1, 0x0

    sget v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    add-int/2addr v3, v4

    invoke-static {v2, v3, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->setCorner(Landroid/view/View;II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    invoke-virtual {v2, v5}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEvent:Lcom/google/api/services/plusi/model/PlusEvent;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sAddPhotosDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->bind(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void

    :cond_0
    sget v2, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sPadding:I

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    sget-object v3, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreText:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->sInviteMoreDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->bind(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/EventActionButtonLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mActionButton:Lcom/google/android/apps/plus/views/EventActionButtonLayout;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onAddPhotosClicked()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventActionListener:Lcom/google/android/apps/plus/views/EventActionListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/EventActionListener;->onInviteMoreClicked()V

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    if-eq v1, p3, :cond_1

    iget v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_2

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_1
    iput p3, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    iget v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mCurrentSelectionIndex:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mSpinnerAdapter:Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;

    const-string v5, "ATTENDING"

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;->access$000(Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout$RsvpSpinnerAdapter;Ljava/lang/String;)I

    move-result v4

    if-ne v1, v4, :cond_4

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mShowActionButton:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->requestLayout()V

    :cond_1
    return-void

    :cond_2
    move v0, v3

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    const-string v4, "ATTENDING"

    invoke-interface {v1, v4}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mEventOver:Z

    if-eqz v1, :cond_3

    const-string v1, "NOT_ATTENDING"

    :goto_3
    invoke-interface {v4, v1}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v1, "MAYBE"

    goto :goto_3

    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventRsvpSpinnerLayout;->mListener:Lcom/google/android/apps/plus/views/EventRsvpListener;

    const-string v4, "NOT_ATTENDING"

    invoke-interface {v1, v4}, Lcom/google/android/apps/plus/views/EventRsvpListener;->onRsvpChanged(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
