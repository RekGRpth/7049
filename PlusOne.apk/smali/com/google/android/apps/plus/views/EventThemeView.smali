.class public Lcom/google/android/apps/plus/views/EventThemeView;
.super Lcom/google/android/apps/plus/views/ImageResourceView;
.source "EventThemeView.java"


# static fields
.field private static sMediaHeight:I

.field private static sMediaWidth:I


# instance fields
.field private mImageRequested:Z

.field private mThemeImageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventThemeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventThemeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageResourceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EventThemeView;->init(Landroid/content/Context;)V

    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    sget v0, Lcom/google/android/apps/plus/views/EventThemeView;->sMediaWidth:I

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaWidth(Landroid/content/Context;)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventThemeView;->sMediaWidth:I

    invoke-static {v0}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/views/EventThemeView;->sMediaHeight:I

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setSizeCategory(I)V

    sget v0, Lcom/google/android/apps/plus/views/EventThemeView;->sMediaWidth:I

    sget v1, Lcom/google/android/apps/plus/views/EventThemeView;->sMediaHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setCustomImageSize(II)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/plus/views/ImageResourceView;->onLayout(ZIIII)V

    sub-int v0, p5, p3

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mImageRequested:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mImageRequested:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    const/high16 v3, 0x40000000

    if-ne v2, v3, :cond_1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    :cond_0
    :goto_0
    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsEventData;->getThemeMediaHeight(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    const/high16 v3, -0x80000000

    if-ne v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    return-void
.end method

.method public setEventTheme(Lcom/google/api/services/plusi/model/Theme;)V
    .locals 1
    .param p1    # Lcom/google/api/services/plusi/model/Theme;

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsEventData;->getImageUrl(Lcom/google/api/services/plusi/model/Theme;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setImageUrl(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->setScaleMode(I)V

    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mImageRequested:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/EventThemeView;->mThemeImageUrl:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/views/EventThemeView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/EventThemeView;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;Z)V

    goto :goto_0
.end method
