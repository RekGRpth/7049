.class public Lcom/google/android/apps/plus/views/ExactLayout;
.super Landroid/view/ViewGroup;
.source "ExactLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;
    }
.end annotation


# instance fields
.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private final mClickableItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    return-void
.end method

.method public static getBound(Landroid/view/View;I)I
    .locals 4
    .param p0    # Landroid/view/View;
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    packed-switch p1, :pswitch_data_0

    const/4 v3, -0x1

    :goto_0
    return v3

    :pswitch_0
    iget v3, v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    goto :goto_0

    :pswitch_1
    iget v3, v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    goto :goto_0

    :pswitch_2
    iget v3, v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    add-int/2addr v3, v0

    goto :goto_0

    :pswitch_3
    iget v3, v2, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    add-int/2addr v3, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static varargs getMaxHeight([Landroid/view/View;)I
    .locals 6
    .param p0    # [Landroid/view/View;

    const/4 v1, 0x0

    array-length v4, p0

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    move v0, v2

    :goto_0
    if-ltz v0, :cond_1

    aget-object v3, p0, v0

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method public static measure(Landroid/view/View;IIII)V
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {p3, v0}, Ljava/lang/Math;->max(II)I

    move-result p3

    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p3, p4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->measure(II)V

    return-void
.end method

.method public static setCenterBounds(Landroid/view/View;II)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    iput p2, v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->verticalBound:I

    iput p1, v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->horizontalBound:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public static setCorner(Landroid/view/View;II)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(II)V

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iput p1, v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    iput p2, v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    goto :goto_0
.end method

.method public static varargs verticallyCenter(I[Landroid/view/View;)V
    .locals 7
    .param p0    # I
    .param p1    # [Landroid/view/View;

    array-length v5, p1

    add-int/lit8 v5, v5, -0x1

    const/4 v6, 0x0

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    const v3, 0x7fffffff

    move v0, v2

    :goto_0
    if-ltz v0, :cond_1

    aget-object v4, p1, v0

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    iget v5, v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_1
    if-ltz v0, :cond_3

    aget-object v4, p1, v0

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    iget v5, v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    invoke-static {v4, v5, v3}, Lcom/google/android/apps/plus/views/ExactLayout;->setCorner(Landroid/view/View;II)V

    iget v5, v1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->horizontalBound:I

    invoke-static {v4, v5, p0}, Lcom/google/android/apps/plus/views/ExactLayout;->setCenterBounds(Landroid/view/View;II)V

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    return-void
.end method


# virtual methods
.method protected final addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final addPadding(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingRight()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingBottom()I

    move-result v3

    add-int/2addr v3, p4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ExactLayout;->setPadding(IIII)V

    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v2, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    :goto_1
    return v4

    :pswitch_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    :goto_2
    if-ltz v0, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v5

    if-eqz v5, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->invalidate()V

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :pswitch_2
    iput-object v7, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    :goto_3
    if-ltz v0, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->invalidate()V

    goto :goto_0

    :pswitch_3
    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->invalidate()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingTop()I

    move-result v1

    neg-int v2, v0

    int-to-float v2, v2

    neg-int v3, v1

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ExactLayout;->onDrawCustom(Landroid/graphics/Canvas;)V

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected onDrawCustom(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # Landroid/graphics/Canvas;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 13
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v9, 0x8

    if-eq v6, v9, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingLeft()I

    move-result v9

    const/4 v10, 0x0

    iget v11, v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->horizontalBound:I

    sub-int/2addr v11, v8

    div-int/lit8 v11, v11, 0x2

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    add-int v3, v9, v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingTop()I

    move-result v9

    const/4 v10, 0x0

    iget v11, v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->verticalBound:I

    sub-int/2addr v11, v1

    div-int/lit8 v11, v11, 0x2

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v10

    add-int v7, v9, v10

    iget v9, v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    add-int/2addr v9, v3

    iget v10, v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    add-int/2addr v10, v7

    iget v11, v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    add-int/2addr v11, v3

    add-int/2addr v11, v8

    iget v12, v4, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    add-int/2addr v12, v1

    add-int/2addr v12, v7

    invoke-virtual {v5, v9, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 23
    .param p1    # I
    .param p2    # I

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingLeft()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingRight()I

    move-result v22

    add-int v18, v21, v22

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingTop()I

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getPaddingBottom()I

    move-result v22

    add-int v17, v21, v22

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v20

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v7

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v19

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    sub-int v21, v19, v18

    move/from16 v0, v21

    move/from16 v1, v20

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    sub-int v21, v6, v17

    move/from16 v0, v21

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/google/android/apps/plus/views/ExactLayout;->measureChildren(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildCount()I

    move-result v4

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v4, :cond_1

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15}, Landroid/view/View;->getVisibility()I

    move-result v16

    const/16 v21, 0x8

    move/from16 v0, v16

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;

    iget v0, v9, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->x:I

    move/from16 v21, v0

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredWidth()I

    move-result v22

    add-int v14, v21, v22

    iget v0, v9, Lcom/google/android/apps/plus/views/ExactLayout$LayoutParams;->y:I

    move/from16 v21, v0

    invoke-virtual {v15}, Landroid/view/View;->getMeasuredHeight()I

    move-result v22

    add-int v3, v21, v22

    invoke-static {v11, v14}, Ljava/lang/Math;->max(II)I

    move-result v11

    invoke-static {v10, v3}, Ljava/lang/Math;->max(II)I

    move-result v10

    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/ExactLayout;->onMeasureCustom$7347e0dc(I)Landroid/graphics/Point;

    move-result-object v5

    if-eqz v5, :cond_2

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ExactLayout;->setWillNotDraw(Z)V

    iget v0, v5, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v11, v0}, Ljava/lang/Math;->max(II)I

    move-result v11

    iget v0, v5, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v10

    :cond_2
    add-int v21, v11, v18

    move/from16 v0, v21

    move/from16 v1, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/ExactLayout;->resolveSize(II)I

    move-result v21

    add-int v22, v10, v17

    move/from16 v0, v22

    move/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/ExactLayout;->resolveSize(II)I

    move-result v22

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ExactLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onMeasureCustom$7347e0dc(I)Landroid/graphics/Point;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public onRecycle()V
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ExactLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v3, v2, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v3, :cond_0

    check-cast v2, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {v2}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected final removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mClickableItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ExactLayout;->mBackground:Landroid/graphics/drawable/Drawable;

    return-void
.end method
