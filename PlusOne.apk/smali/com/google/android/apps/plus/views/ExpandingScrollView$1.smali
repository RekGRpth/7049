.class final Lcom/google/android/apps/plus/views/ExpandingScrollView$1;
.super Ljava/lang/Object;
.source "ExpandingScrollView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/ExpandingScrollView;->onLayout(ZIIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/ExpandingScrollView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    # getter for: Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$000(Lcom/google/android/apps/plus/views/ExpandingScrollView;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/16 v0, 0x3e8

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    # getter for: Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z
    invoke-static {v4}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$000(Lcom/google/android/apps/plus/views/ExpandingScrollView;)Z

    move-result v4

    if-eqz v4, :cond_1

    # getter for: Lcom/google/android/apps/plus/views/ExpandingScrollView;->sBigBounceInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$100()Landroid/view/animation/Interpolator;

    move-result-object v3

    :goto_1
    const/4 v2, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_0
    const/16 v0, 0x2ee

    goto :goto_0

    :cond_1
    # getter for: Lcom/google/android/apps/plus/views/ExpandingScrollView;->sBounceInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$200()Landroid/view/animation/Interpolator;

    move-result-object v3

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    # getter for: Lcom/google/android/apps/plus/views/ExpandingScrollView;->mOriginalTranslationY:I
    invoke-static {v5}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$300(Lcom/google/android/apps/plus/views/ExpandingScrollView;)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    int-to-long v5, v0

    invoke-virtual {v4, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$402(Lcom/google/android/apps/plus/views/ExpandingScrollView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->access$502(Lcom/google/android/apps/plus/views/ExpandingScrollView;Z)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;->this$0:Lcom/google/android/apps/plus/views/ExpandingScrollView;

    new-instance v5, Lcom/google/android/apps/plus/views/ExpandingScrollView$1$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView$1$1;-><init>(Lcom/google/android/apps/plus/views/ExpandingScrollView$1;)V

    add-int/lit16 v6, v0, 0xc8

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
