.class public Lcom/google/android/apps/plus/views/ExpandingScrollView;
.super Lcom/google/android/apps/plus/views/ScrollableViewGroup;
.source "ExpandingScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;
    }
.end annotation


# static fields
.field private static final sBigBounceInterpolator:Landroid/view/animation/Interpolator;

.field private static final sBounceInterpolator:Landroid/view/animation/Interpolator;

.field private static sInitialized:Z

.field private static sMinExposureLand:I

.field private static sMinExposurePort:I


# instance fields
.field private mAlwaysExpanded:Z

.field private mAnimateInRunnable:Ljava/lang/Runnable;

.field private mBigBounce:Z

.field private mCanAnimate:Z

.field private mHasPlayedAnimation:Z

.field private mLastTouchEvent:Landroid/view/MotionEvent;

.field private mLastTouchY:I

.field private mMaxScroll:I

.field private mMinExposure:I

.field private mMinExposureLand:I

.field private mMinExposurePort:I

.field private mOriginalTranslationY:I

.field private mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

.field private mTouchSlop:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    invoke-direct {v0}, Landroid/view/animation/OvershootInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sBounceInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/OvershootInterpolator;

    const/high16 v1, 0x41700000

    invoke-direct {v0, v1}, Landroid/view/animation/OvershootInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sBigBounceInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;)V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-boolean v2, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sInitialized:Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_land:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposureLand:I

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_port:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposurePort:I

    sput-boolean v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sInitialized:Z

    :cond_0
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mTouchSlop:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-boolean v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sInitialized:Z

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_land:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposureLand:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_port:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposurePort:I

    sput-boolean v4, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sInitialized:Z

    :cond_0
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mTouchSlop:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAttributeValues(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x1

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-boolean v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sInitialized:Z

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_land:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposureLand:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_port:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposurePort:I

    sput-boolean v4, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sInitialized:Z

    :cond_0
    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mTouchSlop:I

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setAttributeValues(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/ExpandingScrollView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ExpandingScrollView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z

    return v0
.end method

.method static synthetic access$100()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sBigBounceInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$200()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sBounceInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/ExpandingScrollView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ExpandingScrollView;

    iget v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mOriginalTranslationY:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/views/ExpandingScrollView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ExpandingScrollView;
    .param p1    # Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/views/ExpandingScrollView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ExpandingScrollView;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mHasPlayedAnimation:Z

    return v0
.end method

.method private setAttributeValues(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/plus/R$styleable;->ExpandingScrollView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposureLand:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposureLand:I

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/plus/views/ExpandingScrollView;->sMinExposurePort:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposurePort:I

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->clearAnimation()V

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onDetachedFromWindow()V

    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    const/4 v10, 0x1

    const/4 v11, 0x0

    iget-boolean v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    if-eqz v9, :cond_1

    :cond_0
    :goto_0
    return v11

    :cond_1
    iget-object v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v9}, Landroid/widget/Scroller;->isFinished()Z

    move-result v9

    if-nez v9, :cond_2

    move v11, v10

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v8, v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->updatePosition(Landroid/view/MotionEvent;)V

    iput v8, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mLastTouchY:I

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getScrollY()I

    move-result v5

    iget v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    if-ne v5, v9, :cond_7

    move v9, v10

    :goto_1
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

    iget v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mLastTouchY:I

    sub-int v0, v8, v9

    if-gez v0, :cond_8

    move v4, v10

    :goto_2
    invoke-virtual {p0, v11}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v9

    if-nez v9, :cond_9

    invoke-virtual {v2, v11}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getTop()I

    move-result v9

    if-nez v9, :cond_9

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getScrollY()I

    move-result v9

    if-nez v9, :cond_9

    :cond_3
    move v3, v10

    :goto_3
    if-eqz v5, :cond_4

    if-nez v4, :cond_0

    if-eqz v3, :cond_0

    iget v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    if-ne v5, v9, :cond_0

    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    iget v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mTouchSlop:I

    if-gt v0, v9, :cond_5

    iget v9, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mTouchSlop:I

    neg-int v9, v9

    if-ge v0, v9, :cond_0

    :cond_5
    const/4 v1, 0x0

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getChildCount()I

    move-result v6

    :goto_4
    if-ge v1, v6, :cond_a

    invoke-virtual {v2, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    instance-of v9, v7, Lcom/google/android/apps/plus/views/StreamOneUpCommentView;

    if-eqz v9, :cond_6

    invoke-virtual {v7, v11}, Landroid/view/View;->setPressed(Z)V

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    move v9, v11

    goto :goto_1

    :cond_8
    move v4, v11

    goto :goto_2

    :cond_9
    move v3, v11

    goto :goto_3

    :cond_a
    move v11, v10

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    if-eqz v3, :cond_0

    move p3, v2

    :goto_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int p5, p3, v3

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/view/View;->layout(IIII)V

    move p3, p5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget p3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    goto :goto_0

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    if-eqz v3, :cond_5

    move v3, v2

    :goto_2
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setScrollLimits(II)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    if-eqz v3, :cond_6

    :goto_3
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->scrollTo(I)V

    :cond_2
    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mHasPlayedAnimation:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mCanAnimate:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getTranslationY()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mOriginalTranslationY:I

    iget v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mOriginalTranslationY:I

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    if-eqz v2, :cond_7

    iget v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    :goto_4
    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setTranslationY(F)V

    new-instance v2, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView$1;-><init>(Lcom/google/android/apps/plus/views/ExpandingScrollView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAnimateInRunnable:Ljava/lang/Runnable;

    const-wide/16 v3, 0xfa

    invoke-virtual {p0, v2, v3, v4}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    return-void

    :cond_5
    iget v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    goto :goto_2

    :cond_6
    iget v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    goto :goto_3

    :cond_7
    iget v2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposure:I

    goto :goto_4
.end method

.method public onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposureLand:I

    iput v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposure:I

    :goto_0
    iget v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposure:I

    sub-int v4, v0, v4

    iput v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    const/4 v3, 0x0

    const/high16 v4, 0x40000000

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildCount()I

    move-result v4

    if-ge v1, v4, :cond_1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1, v2}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    iget v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposurePort:I

    iput v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposure:I

    goto :goto_0

    :cond_1
    iget v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    add-int/2addr v4, v0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExpanded:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

    iget v1, v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposureLand:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposureLand:I

    iget v1, v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mExposurePort:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposurePort:I

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;->mBigBounce:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->getScrollY()I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    if-ne v0, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    new-instance v0, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;

    iget v3, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposureLand:I

    iget v4, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposurePort:I

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ExpandingScrollView$SavedState;-><init>(Landroid/os/Parcelable;ZIIZ)V

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected final onScrollFinished(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    if-gez p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->smoothScrollTo(I)V

    if-ltz p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mRestoreExpandedScrollPosition:Ljava/lang/Boolean;

    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMaxScroll:I

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mLastTouchEvent:Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setAlwaysExpanded(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mAlwaysExpanded:Z

    return-void
.end method

.method public setBigBounce(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mBigBounce:Z

    return-void
.end method

.method public setCanAnimate(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mCanAnimate:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mCanAnimate:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mHasPlayedAnimation:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ExpandingScrollView;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setMinimumExposure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposureLand:I

    iput p2, p0, Lcom/google/android/apps/plus/views/ExpandingScrollView;->mMinExposurePort:I

    return-void
.end method
