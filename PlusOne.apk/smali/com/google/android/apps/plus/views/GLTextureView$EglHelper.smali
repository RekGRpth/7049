.class final Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;
.super Ljava/lang/Object;
.source "GLTextureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/GLTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EglHelper"
.end annotation


# instance fields
.field mEgl:Ljavax/microedition/khronos/egl/EGL10;

.field mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field final synthetic this$0:Lcom/google/android/apps/plus/views/GLTextureView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/GLTextureView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private throwEglException(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->throwEglException(Ljava/lang/String;I)V

    return-void
.end method

.method private static throwEglException(Ljava/lang/String;I)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final createSurface(Landroid/graphics/SurfaceTexture;)Ljavax/microedition/khronos/opengles/GL;
    .locals 9
    .param p1    # Landroid/graphics/SurfaceTexture;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "egl not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglDisplay not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "mEglConfig not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v7, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v8, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v4, v5, v6, v7, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$500(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;->destroySurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)V

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$500(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v4, v5, v6, v7, p1}, Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;->createWindowSurface(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v4, v5, :cond_7

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v1

    const/16 v4, 0x300b

    if-ne v1, v4, :cond_5

    const-string v4, "EglHelper"

    const-string v5, "createWindowSurface returned EGL_BAD_NATIVE_WINDOW."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    const/4 v2, 0x0

    :cond_6
    :goto_0
    return-object v2

    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v4, v5, v6, v7, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "eglMakeCurrent"

    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->throwEglException(Ljava/lang/String;)V

    :cond_8
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v4}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mGLWrapper:Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$600(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mGLWrapper:Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$600(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;->wrap$40ce139f()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    :cond_9
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mDebugFlags:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$700(Lcom/google/android/apps/plus/views/GLTextureView;)I

    move-result v4

    and-int/lit8 v4, v4, 0x3

    if-eqz v4, :cond_6

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mDebugFlags:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$700(Lcom/google/android/apps/plus/views/GLTextureView;)I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_a

    const/4 v0, 0x1

    :cond_a
    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mDebugFlags:I
    invoke-static {v4}, Lcom/google/android/apps/plus/views/GLTextureView;->access$700(Lcom/google/android/apps/plus/views/GLTextureView;)I

    move-result v4

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_b

    new-instance v3, Lcom/google/android/apps/plus/views/GLTextureView$LogWriter;

    invoke-direct {v3}, Lcom/google/android/apps/plus/views/GLTextureView$LogWriter;-><init>()V

    :cond_b
    invoke-static {v2, v0, v3}, Landroid/opengl/GLDebugHelper;->wrap(Ljavax/microedition/khronos/opengles/GL;ILjava/io/Writer;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v2

    goto :goto_0
.end method

.method public final finish()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/GLTextureView;->access$400(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;->destroyContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    iput-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    :cond_1
    return-void
.end method

.method public final start()V
    .locals 6

    const/4 v5, 0x0

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v1

    check-cast v1, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "eglGetDisplay failed"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "eglInitialize failed"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLConfigChooser:Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/GLTextureView;->access$300(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->this$0:Lcom/google/android/apps/plus/views/GLTextureView;

    # getter for: Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/GLTextureView;->access$400(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;->createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v1, v2, :cond_3

    :cond_2
    iput-object v5, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    const-string v1, "createContext"

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->throwEglException(Ljava/lang/String;)V

    :cond_3
    iput-object v5, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    return-void
.end method

.method public final swap()Z
    .locals 5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v1, "eglSwapBuffers"

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;->throwEglException(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :pswitch_1
    const/4 v1, 0x0

    goto :goto_1

    :pswitch_2
    const-string v1, "EglHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "eglSwapBuffers returned EGL_BAD_NATIVE_WINDOW. tid="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x300b
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
