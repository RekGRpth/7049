.class public Lcom/google/android/apps/plus/views/GLTextureView;
.super Landroid/view/TextureView;
.source "GLTextureView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;,
        Lcom/google/android/apps/plus/views/GLTextureView$LogWriter;,
        Lcom/google/android/apps/plus/views/GLTextureView$GLThread;,
        Lcom/google/android/apps/plus/views/GLTextureView$EglHelper;,
        Lcom/google/android/apps/plus/views/GLTextureView$SimpleEGLConfigChooser;,
        Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;,
        Lcom/google/android/apps/plus/views/GLTextureView$BaseConfigChooser;,
        Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;,
        Lcom/google/android/apps/plus/views/GLTextureView$DefaultWindowSurfaceFactory;,
        Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;,
        Lcom/google/android/apps/plus/views/GLTextureView$DefaultContextFactory;,
        Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;,
        Lcom/google/android/apps/plus/views/GLTextureView$Renderer;,
        Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;
    }
.end annotation


# static fields
.field private static final sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;


# instance fields
.field private mDebugFlags:I

.field private mDetached:Z

.field private mEGLConfigChooser:Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

.field private mEGLContextClientVersion:I

.field private mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

.field private mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

.field private mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

.field private mGLWrapper:Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;

.field private mPreserveEGLContextOnPause:Z

.field private mSizeChanged:Z

.field private mViewRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;-><init>(B)V

    sput-object v0, Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/GLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z

    invoke-virtual {p0, p0}, Lcom/google/android/apps/plus/views/GLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/views/GLTextureView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/google/android/apps/plus/views/GLTextureView;Z)Z
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mSizeChanged:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLThread;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/GLTextureView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextClientVersion:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLConfigChooser:Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/views/GLTextureView;)Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLWrapper:Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/GLTextureView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mDebugFlags:I

    return v0
.end method

.method static synthetic access$800()Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/GLTextureView;->sGLThreadManager:Lcom/google/android/apps/plus/views/GLTextureView$GLThreadManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/views/GLTextureView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/GLTextureView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mPreserveEGLContextOnPause:Z

    return v0
.end method

.method private checkRenderThreadState()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 3

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mDetached:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mViewRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->getRenderMode()I

    move-result v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mViewRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;Lcom/google/android/apps/plus/views/GLTextureView$Renderer;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->setRenderMode(I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->start()V

    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mDetached:Z

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->requestExitAndWait()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mDetached:Z

    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    return-void
.end method

.method public final onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->onPause()V

    return-void
.end method

.method public final onResume()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->onResume()V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->surfaceCreated()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->onWindowResize(II)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->surfaceDestroyed()V

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1
    .param p1    # Landroid/graphics/SurfaceTexture;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->onWindowResize(II)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1    # Landroid/graphics/SurfaceTexture;

    return-void
.end method

.method public final requestRender()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->requestRender()V

    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mDebugFlags:I

    return-void
.end method

.method public setEGLConfigChooser(IIIIII)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/plus/views/GLTextureView$ComponentSizeChooser;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;IIIIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/GLTextureView;->setEGLConfigChooser(Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;)V

    return-void
.end method

.method public setEGLConfigChooser(Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView;->checkRenderThreadState()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLConfigChooser:Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1
    .param p1    # Z

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$SimpleEGLConfigChooser;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/views/GLTextureView$SimpleEGLConfigChooser;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/GLTextureView;->setEGLConfigChooser(Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;)V

    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView;->checkRenderThreadState()V

    iput p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextClientVersion:I

    return-void
.end method

.method public setEGLContextFactory(Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView;->checkRenderThreadState()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView;->checkRenderThreadState()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    return-void
.end method

.method public setGLWrapper(Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLWrapper:Lcom/google/android/apps/plus/views/GLTextureView$GLWrapper;

    return-void
.end method

.method public setPreserveEGLContextOnPause(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mPreserveEGLContextOnPause:Z

    return-void
.end method

.method public setRenderMode(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->setRenderMode(I)V

    return-void
.end method

.method public setRenderer(Lcom/google/android/apps/plus/views/GLTextureView$Renderer;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/GLTextureView;->checkRenderThreadState()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLConfigChooser:Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$SimpleEGLConfigChooser;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/GLTextureView$SimpleEGLConfigChooser;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLConfigChooser:Lcom/google/android/apps/plus/views/GLTextureView$EGLConfigChooser;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$DefaultContextFactory;

    invoke-direct {v0, p0, v2}, Lcom/google/android/apps/plus/views/GLTextureView$DefaultContextFactory;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLContextFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLContextFactory;

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$DefaultWindowSurfaceFactory;

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/views/GLTextureView$DefaultWindowSurfaceFactory;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mEGLWindowSurfaceFactory:Lcom/google/android/apps/plus/views/GLTextureView$EGLWindowSurfaceFactory;

    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mViewRenderer:Lcom/google/android/apps/plus/views/GLTextureView$Renderer;

    new-instance v0, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;-><init>(Lcom/google/android/apps/plus/views/GLTextureView;Lcom/google/android/apps/plus/views/GLTextureView$Renderer;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/GLTextureView;->mGLThread:Lcom/google/android/apps/plus/views/GLTextureView$GLThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/GLTextureView$GLThread;->start()V

    return-void
.end method
