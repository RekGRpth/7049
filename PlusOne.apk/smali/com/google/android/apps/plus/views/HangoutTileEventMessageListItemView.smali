.class public Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;
.super Landroid/widget/RelativeLayout;
.source "HangoutTileEventMessageListItemView.java"


# static fields
.field private static sInitialized:Z

.field private static sMessageColor:I

.field private static sTimestampColor:I


# instance fields
.field private mText:Landroid/widget/TextView;

.field private mTimestamp:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sInitialized:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView$1;-><init>(Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sInitialized:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$color;->realtimechat_system_information_foreground:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sMessageColor:I

    sget v1, Lcom/google/android/apps/plus/R$color;->realtimechat_system_information_timestamp:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sTimestampColor:I

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sInitialized:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->timestamp:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mTimestamp:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mText:Landroid/widget/TextView;

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mText:Landroid/widget/TextView;

    check-cast p1, Ljava/lang/String;

    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setTimeSince(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mTimestamp:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setType(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x6

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mText:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sMessageColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mTimestamp:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->sTimestampColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    return-void
.end method

.method public final updateContentDescription()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mTimestamp:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->realtimechat_message_description_time_since:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->mText:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$string;->realtimechat_message_description_message:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/HangoutTileEventMessageListItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
