.class public Lcom/google/android/apps/plus/views/HostActionBar;
.super Landroid/widget/LinearLayout;
.source "HostActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/HostActionBar$SavedState;,
        Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;,
        Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;,
        Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
    }
.end annotation


# instance fields
.field private mActionButton1:Landroid/widget/ImageView;

.field private mActionButton1Visible:Z

.field private mActionButton2:Landroid/widget/ImageView;

.field private mActionButton2Visible:Z

.field private mActionId1:I

.field private mActionId2:I

.field private mActive:Z

.field private mContextActionMode:Z

.field private mCurrentButtonActionText:Ljava/lang/String;

.field private mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

.field private mDoneButton:Landroid/view/View;

.field private mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

.field private mDoneTitle:Landroid/widget/TextView;

.field private mDoneTitleVisible:Z

.field private mInvalidateActionBarRunnable:Ljava/lang/Runnable;

.field private mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

.field private mNotificationButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;

.field private mNotificationCount:I

.field private mNotificationCountOverflow:Landroid/view/View;

.field private mNotificationCountText:Landroid/widget/TextView;

.field private mNotificationsButton:Landroid/view/View;

.field private mNotificationsButtonVisible:Z

.field private mNotificationsMode:Z

.field private mNotificationsProgressIndicatorVisible:Z

.field private mOverflowMenuButton:Landroid/view/View;

.field private mOverflowPopupMenu:Ljava/lang/Object;

.field private mOverflowPopupMenuVisible:Z

.field private mPrimarySpinner:Landroid/widget/Spinner;

.field private mPrimarySpinnerContainer:Landroid/view/View;

.field private mPrimarySpinnerVisible:Z

.field private mProgressIndicator:Landroid/view/View;

.field private mProgressIndicatorVisible:Z

.field private mRefreshButton:Landroid/widget/ImageView;

.field private mRefreshButtonVisible:Z

.field private mRefreshButtonVisibleIfRoom:Z

.field private mRefreshHighlighted:Z

.field private mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

.field private mSearchViewContainer:Landroid/view/View;

.field private mSearchViewVisible:Z

.field private mShareMenuButton:Landroid/view/View;

.field private mShareMenuVisible:Z

.field private mSharePopupMenu:Ljava/lang/Object;

.field private mSharePopupMenuVisible:Z

.field private mTitle:Landroid/widget/TextView;

.field private mTitleVisible:Z

.field private mUpButton:Landroid/view/View;

.field private mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar$1;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar$1;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar$1;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/HostActionBar;)Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/HostActionBar;)Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/views/HostActionBar;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/HostActionBar;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/apps/plus/views/HostActionBar;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/HostActionBar;
    .param p1    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showOverflowMenu()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/HostActionBar;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showSharePopupMenu()V

    return-void
.end method

.method private configurePopupMenuListeners(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v3, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;B)V

    move-object v1, p1

    check-cast v1, Landroid/widget/PopupMenu;

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListenerV14;

    invoke-virtual {v1, v2}, Landroid/widget/PopupMenu;->setOnDismissListener(Landroid/widget/PopupMenu$OnDismissListener;)V

    :goto_0
    check-cast p1, Landroid/widget/PopupMenu;

    invoke-virtual {p1, v0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;

    invoke-direct {v0, p0, v3}, Lcom/google/android/apps/plus/views/HostActionBar$PopupMenuListener;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;B)V

    goto :goto_0
.end method

.method private getOverflowPopupMenu()Ljava/lang/Object;
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->configurePopupMenuListeners(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    goto :goto_0
.end method

.method private isInProgress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsProgressIndicatorVisible:Z

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    goto :goto_0
.end method

.method private isOverflowMenuSupported()Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private prepareOverflowMenu()Z
    .locals 6

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    if-nez v5, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    check-cast v5, Landroid/widget/PopupMenu;

    invoke-virtual {v5}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5, v3}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x0

    invoke-interface {v3}, Landroid/view/Menu;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-interface {v3, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private prepareSharePopupMenu()Z
    .locals 11

    const/4 v10, 0x0

    const/4 v9, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    if-nez v8, :cond_1

    move v0, v9

    :cond_0
    return v0

    :cond_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    check-cast v8, Landroid/widget/PopupMenu;

    invoke-virtual {v8}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    invoke-virtual {v8, v6}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    const/4 v0, 0x0

    invoke-interface {v6}, Landroid/view/Menu;->size()I

    move-result v7

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_0

    invoke-interface {v6, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const/4 v3, 0x0

    const/4 v5, 0x0

    :goto_1
    array-length v8, v10

    if-ge v5, v8, :cond_2

    aget v8, v10, v5

    if-ne v2, v8, :cond_4

    const/4 v3, 0x1

    :cond_2
    if-nez v3, :cond_3

    const/4 v0, 0x1

    invoke-interface {v4, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private showOverflowMenu()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getOverflowPopupMenu()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/PopupMenu;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareOverflowMenu()Z

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method

.method private showSharePopupMenu()V
    .locals 4

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    :goto_0
    check-cast v1, Landroid/widget/PopupMenu;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareSharePopupMenu()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    :cond_0
    return-void

    :cond_1
    new-instance v1, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-direct {v1, v2, v3}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->configurePopupMenuListeners(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    goto :goto_0
.end method

.method private showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/CharSequence;

    const/4 v8, 0x0

    const/4 v7, 0x2

    new-array v3, v7, [I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    invoke-virtual {p1, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v4, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v0, p2, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    const/16 v7, 0x35

    aget v8, v3, v8

    sub-int v8, v4, v8

    div-int/lit8 v9, v6, 0x2

    sub-int/2addr v8, v9

    invoke-virtual {v5, v7, v8, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    const/4 v7, 0x1

    return v7
.end method


# virtual methods
.method public final commit()V
    .locals 7

    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v1, :cond_4

    move v1, v3

    :goto_1
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitle:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitleVisible:Z

    if-eqz v1, :cond_5

    move v1, v3

    :goto_2
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    if-eqz v1, :cond_6

    move v1, v3

    :goto_3
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerContainer:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_7

    move v1, v3

    :goto_4
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-virtual {v1, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewContainer:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    if-eqz v1, :cond_8

    move v1, v3

    :goto_5
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_9

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    if-eqz v1, :cond_9

    move v1, v4

    :goto_6
    invoke-virtual {v5, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setVisible(Z)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isInProgress()Z

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v1

    if-eqz v1, :cond_a

    if-nez v0, :cond_a

    move v1, v3

    :goto_7
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshHighlighted:Z

    if-eqz v1, :cond_b

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_b

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_refresh_blue:I

    :goto_8
    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    if-eqz v0, :cond_c

    move v1, v3

    :goto_9
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_d

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-eqz v1, :cond_d

    move v1, v3

    :goto_a
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-eqz v1, :cond_e

    move v1, v3

    :goto_b
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButton:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButtonVisible:Z

    if-eqz v1, :cond_f

    move v1, v3

    :goto_c
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isOverflowMenuSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    if-eqz v1, :cond_10

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareSharePopupMenu()Z

    :cond_1
    :goto_d
    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/google/android/apps/plus/R$string;->home_menu_notifications:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTitle(I)V

    :cond_2
    return-void

    :cond_3
    move v1, v3

    goto/16 :goto_0

    :cond_4
    move v1, v2

    goto/16 :goto_1

    :cond_5
    move v1, v2

    goto/16 :goto_2

    :cond_6
    move v1, v2

    goto/16 :goto_3

    :cond_7
    move v1, v2

    goto/16 :goto_4

    :cond_8
    move v1, v2

    goto/16 :goto_5

    :cond_9
    move v1, v3

    goto/16 :goto_6

    :cond_a
    move v1, v2

    goto :goto_7

    :cond_b
    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_refresh:I

    goto :goto_8

    :cond_c
    move v1, v2

    goto :goto_9

    :cond_d
    move v1, v2

    goto :goto_a

    :cond_e
    move v1, v2

    goto :goto_b

    :cond_f
    move v1, v2

    goto :goto_c

    :cond_10
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    if-eqz v1, :cond_11

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareOverflowMenu()Z

    goto :goto_d

    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getOverflowPopupMenu()Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->prepareOverflowMenu()Z

    move-result v5

    if-eqz v5, :cond_12

    :goto_e
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_d

    :cond_12
    move v3, v2

    goto :goto_e
.end method

.method public final dismissPopupMenus()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenu:Ljava/lang/Object;

    check-cast v0, Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenu:Ljava/lang/Object;

    check-cast v0, Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->dismiss()V

    :cond_1
    return-void
.end method

.method public final finishContextActionMode()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitleVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final getSearchViewAdapter()Lcom/google/android/apps/plus/views/SearchViewAdapter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    return-object v0
.end method

.method public final hideNotificationsProgressIndicator()V
    .locals 4

    const/16 v1, 0x8

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsProgressIndicatorVisible:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final hideProgressIndicator()V
    .locals 4

    const/16 v2, 0x8

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-eqz v3, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method public final initForDarkTheme()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$color;->title_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public final initForDarkTransparentTheme()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$color;->transparent_black_title_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->up_caret:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_ab_back_holo_dark:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method

.method public final invalidateActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->removeCallbacksOnUiThread(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mInvalidateActionBarRunnable:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final isNotificationsMode()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    return v0
.end method

.method public final isRefreshButtonVisible()Z
    .locals 6

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisibleIfRoom:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    move v0, v2

    :goto_1
    iget v4, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-nez v4, :cond_0

    if-nez v0, :cond_0

    move v2, v3

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;->onUpButtonClick()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButton:Landroid/view/View;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;->onNotificationsButtonClick()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;->onDoneButtonClick()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    if-ne p1, v0, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showOverflowMenu()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    if-ne p1, v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showSharePopupMenu()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onRefreshButtonClicked()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId1:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onActionButtonClicked(I)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId2:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onActionButtonClicked(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    const/16 v3, 0x8

    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/apps/plus/R$id;->up:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->done:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->done_title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitle:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->title:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->primary_spinner_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerContainer:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->primary_spinner:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$layout;->simple_spinner_item:I

    invoke-direct {v0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDefaultPrimarySpinnerAdapter:Landroid/widget/SpinnerAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->search_view_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewContainer:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->search_src_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->createInstance(Landroid/view/View;)Lcom/google/android/apps/plus/views/SearchViewAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewAdapter:Lcom/google/android/apps/plus/views/SearchViewAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->requestFocus(Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->share_menu_anchor:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isOverflowMenuSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$id;->refresh_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->action_button_1:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->action_button_2:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->progress_indicator:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->notifications_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButton:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->notification_count:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    const-string v1, "99"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->notification_count_overflow:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->menu:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isOverflowMenuSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mCurrentButtonActionText:Ljava/lang/String;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowMenuButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    invoke-interface {v0, p3}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onPrimarySpinnerSelectionChange(I)V

    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuButton:Landroid/view/View;

    if-ne p1, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->share_menu_anchor_content_description:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->menu_refresh:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    if-ne p1, v1, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->showTooltip(Landroid/view/View;Ljava/lang/CharSequence;)Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->overflowPopupMenuVisible:Z

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/views/HostActionBar$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HostActionBar$2;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->sharePopupMenuVisible:Z

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/android/apps/plus/views/HostActionBar$3;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HostActionBar$3;-><init>(Lcom/google/android/apps/plus/views/HostActionBar;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/HostActionBar;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;-><init>(Landroid/os/Parcelable;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mOverflowPopupMenuVisible:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->overflowPopupMenuVisible:Z

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSharePopupMenuVisible:Z

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/HostActionBar$SavedState;->sharePopupMenuVisible:Z

    :cond_0
    return-object v0
.end method

.method public final reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitleVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisibleIfRoom:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsProgressIndicatorVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mShareMenuVisible:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshHighlighted:Z

    return-void
.end method

.method public setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    return-void
.end method

.method public setNotificationCount(I)V
    .locals 7
    .param p1    # I

    const/16 v2, 0x8

    const/4 v6, 0x0

    iput p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButtonVisible:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->badge_grey:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$plurals;->accessibility_notification_count_description:I

    iget v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButton:Landroid/view/View;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->badge_red:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    iget v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    const/16 v1, 0x63

    if-gt v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCount:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationCountOverflow:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setNotificationsMode(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->showRefreshButton()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->invalidateActionBar()V

    goto :goto_0
.end method

.method public setOnDoneButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnDoneButtonClickListener;

    return-void
.end method

.method public setOnNotificationButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnNotificationButtonClickListener;

    return-void
.end method

.method public setOnUpButtonClickListener(Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButtonListener:Lcom/google/android/apps/plus/views/HostActionBar$OnUpButtonClickListener;

    return-void
.end method

.method public setPrimarySpinnerSelection(I)V
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onPrimarySpinnerSelectionChange(I)V

    :cond_0
    return-void
.end method

.method public setUpButtonContentDescription(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mCurrentButtonActionText:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mCurrentButtonActionText:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final showActionButton(III)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v3, 0x1

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-nez v0, :cond_3

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    iput p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId1:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1Visible:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isInProgress()Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-nez v0, :cond_5

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    iput p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionId2:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2Visible:Z

    if-eqz v3, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only two action buttons are supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final showDoneTitle(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitleVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitle:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method public final showNotificationsButton()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButtonVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showNotificationsProgressIndicator()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsProgressIndicatorVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showPrimarySpinner(Landroid/widget/SpinnerAdapter;I)V
    .locals 3
    .param p1    # Landroid/widget/SpinnerAdapter;
    .param p2    # I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerVisible:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    invoke-interface {p1}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, p2}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mListener:Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;

    invoke-interface {v1, p2}, Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;->onPrimarySpinnerSelectionChange(I)V

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mPrimarySpinnerContainer:Landroid/view/View;

    if-lez v0, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final showProgressIndicator()V
    .locals 2

    const/16 v1, 0x8

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicatorVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisibleIfRoom:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActionButton2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mProgressIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method public final showRefreshButton()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showRefreshButtonIfRoom()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButtonVisibleIfRoom:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isInProgress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->isRefreshButtonVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showSearchView()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mSearchViewContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final showTitle(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final showTitle(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitle:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mTitleVisible:Z

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public final startContextActionMode()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mContextActionMode:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mUpButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mDoneButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public final updateRefreshButtonIcon(Z)V
    .locals 3
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshHighlighted:Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mRefreshButton:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostActionBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/HostActionBar;->mNotificationsMode:Z

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_refresh_blue:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void

    :cond_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_refresh:I

    goto :goto_0
.end method
