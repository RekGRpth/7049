.class public Lcom/google/android/apps/plus/views/HostLayout;
.super Landroid/widget/FrameLayout;
.source "HostLayout.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;
.implements Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;
.implements Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;
    }
.end annotation


# instance fields
.field private mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

.field private mFragmentManager:Landroid/support/v4/app/FragmentManager;

.field private mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

.field private mNavigationBar:Landroid/widget/ListView;

.field private mNotificationBar:Landroid/widget/ListView;

.field private mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

.field private mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

.field private mSlidingBackground:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/HostLayout;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/HostLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mSlidingBackground:Landroid/view/View;

    return-object v0
.end method

.method private hideKeyboard()V
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/HostActionBar;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final attachActionBar()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    :cond_0
    return-void
.end method

.method public final getActionBar()Lcom/google/android/apps/plus/views/HostActionBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    return-object v0
.end method

.method public final getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const-string v1, "hosted"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/phone/HostedFragment;

    return-object v0
.end method

.method public final getNavigationBar()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    return-object v0
.end method

.method public final getNotificationBar()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationBar:Landroid/widget/ListView;

    return-object v0
.end method

.method public final hideNavigationBar()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->close()V

    goto :goto_0
.end method

.method public final hideNotificationBar()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNotificationBarVisibilityChange(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->close()V

    goto :goto_0
.end method

.method public final isNavigationBarVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isOpen()Z

    move-result v0

    return v0
.end method

.method public final isNotificationBarVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isOpen()Z

    move-result v0

    return v0
.end method

.method public final onActionBarInvalidated()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->attachActionBar()V

    return-void
.end method

.method public final onActionButtonClicked(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onActionButtonClicked(I)V

    :cond_0
    return-void
.end method

.method public final onAttachFragment(Lcom/google/android/apps/plus/phone/HostedFragment;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/phone/HostedFragment;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->attachActionBar(Lcom/google/android/apps/plus/views/HostActionBar;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->commit()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/apps/plus/R$id;->title_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/HostActionBar;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/HostActionBar;->setHostActionBarListener(Lcom/google/android/apps/plus/views/HostActionBar$HostActionBarListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->navigation_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    sget v0, Lcom/google/android/apps/plus/R$id;->fragment_sliding_background:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mSlidingBackground:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->panel:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/NavigationBarLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setOnNavigationBarStateChange(Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->notification_panel:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/NotificationBarLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    sget v0, Lcom/google/android/apps/plus/R$id;->notification_bar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/HostLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationBar:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setOnNotificationBarStateChange(Lcom/google/android/apps/plus/views/NotificationBarLayout$OnNotificationBarStateChange;)V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isOpen()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mSlidingBackground:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/plus/views/HostLayout$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/plus/views/HostLayout$1;-><init>(Lcom/google/android/apps/plus/views/HostLayout;I)V

    invoke-virtual {v2, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v2, v1, v1, v3, v4}, Landroid/widget/ListView;->layout(IIII)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->getNavigationBarWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ListView;->measure(II)V

    :cond_0
    return-void
.end method

.method public final onNavigationBarClosed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mSlidingBackground:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/plus/views/HostLayout$4;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HostLayout$4;-><init>(Lcom/google/android/apps/plus/views/HostLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNavigationBarVisibilityChange(Z)V

    :cond_0
    return-void
.end method

.method public final onNavigationBarOpened()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNavigationBarVisibilityChange(Z)V

    :cond_0
    return-void
.end method

.method public final onNotificationBarClosed()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNotificationBarVisibilityChange(Z)V

    :cond_0
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1    # Landroid/view/Menu;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public final onPrimarySpinnerSelectionChange(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->onPrimarySpinnerSelectionChange(I)V

    :cond_0
    return-void
.end method

.method public final onRefreshButtonClicked()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNotificationRefreshButtonClick()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->refresh()V

    goto :goto_0
.end method

.method public final saveHostedFragmentState()Landroid/support/v4/app/Fragment$SavedState;
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->shouldPersistStateToHost()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->saveFragmentInstanceState(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment$SavedState;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    return-void
.end method

.method public final showFragment(Lcom/google/android/apps/plus/phone/HostedFragment;Landroid/support/v4/app/Fragment$SavedState;Z)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/phone/HostedFragment;
    .param p2    # Landroid/support/v4/app/Fragment$SavedState;
    .param p3    # Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->getCurrentHostedFragment()Lcom/google/android/apps/plus/phone/HostedFragment;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p3, :cond_2

    sget-object v1, Lcom/google/android/apps/plus/analytics/OzViews;->LEFT_NAV:Lcom/google/android/apps/plus/analytics/OzViews;

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->detachActionBar()V

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/HostActionBar;->reset()V

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Lcom/google/android/apps/plus/phone/HostedFragment;->setInitialSavedState(Landroid/support/v4/app/Fragment$SavedState;)V

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$id;->fragment_container:I

    const-string v4, "hosted"

    invoke-virtual {v2, v3, p1, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    if-eqz p3, :cond_4

    const/16 v3, 0x1003

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    :goto_2
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/HostLayout;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/plus/phone/HostedFragment;->recordNavigationAction()V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/HostedFragment;->getViewForLogging()Lcom/google/android/apps/plus/analytics/OzViews;

    move-result-object v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_2
.end method

.method public final showNotificationBar()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mListener:Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/HostLayout$HostLayoutListener;->onNotificationBarVisibilityChange(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->dismissPopupMenus()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideKeyboard()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationBar:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->open()V

    goto :goto_0
.end method

.method public final showNotificationBarDelayed()V
    .locals 3

    new-instance v0, Lcom/google/android/apps/plus/views/HostLayout$3;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/HostLayout$3;-><init>(Lcom/google/android/apps/plus/views/HostLayout;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/plus/views/HostLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public final toggleNavigationBarVisibility()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNavigationBar()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mActionBar:Lcom/google/android/apps/plus/views/HostActionBar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/HostActionBar;->dismissPopupMenus()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNotificationBar()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideKeyboard()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNavigationBar:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mSlidingBackground:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/plus/views/HostLayout$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/HostLayout$2;-><init>(Lcom/google/android/apps/plus/views/HostLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mPanel:Lcom/google/android/apps/plus/views/NavigationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->open()V

    goto :goto_0
.end method

.method public final toggleNotificationBarVisibility()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/HostLayout;->mNotificationPanel:Lcom/google/android/apps/plus/views/NotificationBarLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/NotificationBarLayout;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->hideNotificationBar()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/HostLayout;->showNotificationBar()V

    goto :goto_0
.end method
