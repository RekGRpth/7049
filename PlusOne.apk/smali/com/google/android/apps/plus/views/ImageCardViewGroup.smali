.class public Lcom/google/android/apps/plus/views/ImageCardViewGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "ImageCardViewGroup.java"


# instance fields
.field protected mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

.field protected mDescriptionLayout:Landroid/text/StaticLayout;

.field protected mDesiredHeight:I

.field protected mDesiredInverseAspectRatio:F

.field protected mDesiredWidth:I

.field protected mDestRect:Landroid/graphics/Rect;

.field protected mFadeEndTime:J

.field protected mFadeIn:Z

.field protected mFadeRunnable:Ljava/lang/Runnable;

.field protected mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field protected mHeroTop:I

.field protected mImageResource:Lcom/google/android/apps/plus/service/Resource;

.field protected mMediaAreaRect:Landroid/graphics/Rect;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field protected mMediaTitleLayout:Landroid/text/StaticLayout;

.field protected final mResizePaintWithAlpha:Landroid/graphics/Paint;

.field protected mSrcRect:Landroid/graphics/Rect;

.field protected mSubtitleLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredInverseAspectRatio:F

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mResizePaintWithAlpha:Landroid/graphics/Paint;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeIn:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mResizePaintWithAlpha:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    return-void
.end method

.method private addSpacing(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->shouldOverlayMedia()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, p2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, p2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr p1, p2

    goto :goto_0
.end method

.method private static drawLayout(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I
    .locals 2
    .param p0    # Landroid/graphics/Canvas;
    .param p1    # Landroid/text/StaticLayout;
    .param p2    # I
    .param p3    # I

    if-eqz p1, :cond_0

    int-to-float v0, p2

    int-to-float v1, p3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1, p0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p3, v0

    :cond_0
    return p3
.end method

.method private shouldOverlayMedia()Z
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    sget-object v1, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->minimumOverlayMediaHeight:I

    if-lt v0, v1, :cond_0

    const/high16 v0, 0x3f800000

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    const v1, 0x3f800054

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindResources()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->bindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeIn:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected createCustomLayout(Landroid/content/Context;III)I
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return p3
.end method

.method protected final createHero(III)I
    .locals 17
    .param p1    # I
    .param p2    # I
    .param p3    # I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_0
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroTop:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    add-int p2, p2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    add-int v5, p1, p3

    move/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p2

    invoke-virtual {v4, v0, v1, v5, v2}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v4, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v11, p3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_b

    const/4 v15, 0x1

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    const/4 v13, 0x1

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getSubtitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_d

    const/4 v14, 0x1

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->hasCustomLayout()Z

    move-result v12

    if-eqz v15, :cond_1

    sget-object v4, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->addSpacing(II)I

    move-result p2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaTitleMaxLines:I

    invoke-static {v5, v4, v11, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->shouldOverlayMedia()Z

    move-result v5

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    sub-int v4, v6, v4

    iput v4, v5, Landroid/graphics/Rect;->top:I

    :cond_1
    :goto_3
    if-nez v14, :cond_2

    if-eqz v13, :cond_4

    :cond_2
    sget-object v4, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->addSpacing(II)I

    move-result p2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getSubtitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    const/16 v6, 0x8

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataMaxLines:I

    invoke-static {v5, v4, v11, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->shouldOverlayMedia()Z

    move-result v5

    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    sub-int v4, v6, v4

    iput v4, v5, Landroid/graphics/Rect;->top:I

    :cond_3
    :goto_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getDescription()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataMaxLines:I

    invoke-static {v5, v4, v11, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->shouldOverlayMedia()Z

    move-result v5

    if-eqz v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    sub-int v4, v6, v4

    iput v4, v5, Landroid/graphics/Rect;->top:I

    :cond_4
    :goto_5
    if-eqz v12, :cond_6

    if-nez v14, :cond_5

    if-eqz v13, :cond_11

    :cond_5
    sget-object v4, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    move/from16 v16, v0

    :goto_6
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->addSpacing(II)I

    move-result p2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->createCustomLayout(Landroid/content/Context;III)I

    move-result v4

    sub-int v4, v4, p2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->shouldOverlayMedia()Z

    move-result v5

    if-eqz v5, :cond_12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->top:I

    sub-int v4, v6, v4

    iput v4, v5, Landroid/graphics/Rect;->top:I

    :cond_6
    :goto_7
    if-nez v15, :cond_7

    if-nez v14, :cond_7

    if-nez v13, :cond_7

    if-eqz v12, :cond_8

    :cond_7
    sget-object v4, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->addSpacing(II)I

    move-result p2

    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v4, :cond_a

    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isClickable()Z

    move-result v4

    if-eqz v4, :cond_a

    new-instance v4, Lcom/google/android/apps/plus/views/ClickableRect;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroTop:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroTop:I

    sub-int v8, p2, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/apps/plus/R$string;->riviera_media_content_description:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v9, p0

    invoke-direct/range {v4 .. v10}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_a
    return p2

    :cond_b
    const/4 v15, 0x0

    goto/16 :goto_0

    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_1

    :cond_d
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int p2, p2, v4

    goto/16 :goto_3

    :cond_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int p2, p2, v4

    goto/16 :goto_4

    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int p2, p2, v4

    goto/16 :goto_5

    :cond_11
    sget-object v4, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v16, v0

    goto/16 :goto_6

    :cond_12
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    iget v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v4

    iput v6, v5, Landroid/graphics/Rect;->bottom:I

    add-int p2, p2, v4

    goto/16 :goto_7
.end method

.method protected drawCustomLayout(Landroid/graphics/Canvas;III)I
    .locals 0
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return p3
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 22
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    move-object/from16 v17, v0

    if-nez v17, :cond_11

    const/4 v8, 0x0

    :goto_0
    if-eqz v8, :cond_6

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeIn:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeIn:Z

    move/from16 v17, v0

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mResizePaintWithAlpha:Landroid/graphics/Paint;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    const-wide/16 v19, 0xfa

    add-long v17, v17, v19

    move-wide/from16 v0, v17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeEndTime:J

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeIn:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    new-instance v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/ImageCardViewGroup$1;-><init>(Lcom/google/android/apps/plus/views/ImageCardViewGroup;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    const/high16 v17, 0x3f800000

    int-to-float v0, v9

    move/from16 v18, v0

    mul-float v17, v17, v18

    int-to-float v0, v10

    move/from16 v18, v0

    div-float v5, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v10, v9}, Landroid/graphics/Rect;->set(IIII)V

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredInverseAspectRatio:F

    move/from16 v17, v0

    sub-float v17, v17, v5

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(F)F

    move-result v17

    const v18, 0x3c23d70a

    cmpg-float v17, v17, v18

    if-gez v17, :cond_12

    const/4 v13, 0x1

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_3

    if-eqz v13, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, p4

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    :cond_3
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    add-int v17, p2, p4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v18, v0

    add-int v18, v18, p3

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, v17

    move/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v17, v0

    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    if-nez v13, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_18

    const/4 v15, 0x1

    :goto_3
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    if-eqz v15, :cond_19

    move/from16 v18, p4

    :goto_4
    if-eqz v15, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v17, v0

    :goto_5
    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v18

    move/from16 v4, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    move-object/from16 v19, v0

    if-eqz v15, :cond_1b

    const/16 v17, 0x0

    move/from16 v18, v17

    :goto_6
    if-eqz v15, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v20, v0

    move-object/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v17

    move/from16 v3, p4

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    sget-object v18, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaPaint:Landroid/graphics/Paint;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mResizePaintWithAlpha:Landroid/graphics/Paint;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getCenterBitmap()Landroid/graphics/Bitmap;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    sub-int v17, p4, v17

    div-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v18, v0

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    sub-int v18, v18, v19

    div-int/lit8 v18, v18, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    move-object/from16 v3, v19

    invoke-virtual {v0, v11, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    add-int p3, p3, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->hasCustomLayout()Z

    move-result v17

    if-eqz v17, :cond_e

    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->shouldOverlayMedia()Z

    move-result v17

    if-eqz v17, :cond_1d

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaOverlayPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Rect;->height()I

    move-result v17

    sub-int p3, p3, v17

    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v17, v0

    add-int p2, p2, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v17, v0

    add-int p3, p3, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->drawLayout(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I

    move-result p3

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-eqz v17, :cond_a

    :cond_9
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v17, v0

    add-int p3, p3, v17

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->drawLayout(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I

    move-result p3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->drawLayout(Landroid/graphics/Canvas;Landroid/text/StaticLayout;II)I

    move-result p3

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v17, v0

    sub-int p2, p2, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->hasCustomLayout()Z

    move-result v17

    if-eqz v17, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-eqz v17, :cond_1e

    :cond_b
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    move/from16 v17, v0

    :goto_9
    add-int p3, p3, v17

    invoke-virtual/range {p0 .. p4}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->drawCustomLayout(Landroid/graphics/Canvas;III)I

    move-result p3

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v17, v0

    if-nez v17, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->hasCustomLayout()Z

    move-result v17

    if-eqz v17, :cond_e

    :cond_d
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v17, v0

    add-int p3, p3, v17

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v17, v0

    if-eqz v17, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/views/ClickableRect;->isClicked()Z

    move-result v17

    if-eqz v17, :cond_10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->isPressed()Z

    move-result v17

    if-nez v17, :cond_f

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->isFocused()Z

    move-result v17

    if-eqz v17, :cond_10

    :cond_f
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_10
    return p3

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/graphics/Bitmap;

    move-object/from16 v8, v17

    goto/16 :goto_0

    :cond_12
    const/4 v13, 0x0

    goto/16 :goto_1

    :cond_13
    move/from16 v7, p4

    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v5

    move/from16 v0, v17

    float-to-int v6, v0

    const/4 v12, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v17, v0

    if-eqz v17, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v17

    if-eqz v17, :cond_16

    const/high16 v17, 0x3f800000

    cmpg-float v17, v5, v17

    if-gez v17, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    sub-int v17, v17, v6

    div-int/lit8 v16, v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    add-int v19, v6, v16

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    move/from16 v3, p4

    move/from16 v4, v19

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-gt v6, v0, :cond_15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    sub-int v17, v17, v6

    div-int/lit8 v16, v17, 0x2

    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    add-int v18, v12, v7

    add-int v19, v16, v6

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v12, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    :cond_15
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v17, v17, v5

    move/from16 v0, v17

    float-to-int v7, v0

    sub-int v17, p4, v7

    div-int/lit8 v12, v17, 0x2

    goto :goto_a

    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-gt v6, v0, :cond_17

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    sub-int v17, v17, v6

    div-int/lit8 v16, v17, 0x2

    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    add-int v18, v12, v7

    add-int v19, v16, v6

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v12, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    :cond_17
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    div-float v17, v17, v5

    move/from16 v0, v17

    float-to-int v7, v0

    sub-int v17, p4, v7

    div-int/lit8 v12, v17, 0x2

    goto :goto_b

    :cond_18
    const/4 v15, 0x0

    goto/16 :goto_3

    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    move/from16 v18, v17

    goto/16 :goto_4

    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move/from16 v17, v0

    goto/16 :goto_5

    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v17, v0

    move/from16 v18, v17

    goto/16 :goto_6

    :cond_1c
    const/16 v17, 0x0

    goto/16 :goto_7

    :cond_1d
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget-object v14, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaPaint:Landroid/graphics/Paint;

    goto/16 :goto_8

    :cond_1e
    sget-object v17, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    move/from16 v17, v0

    goto/16 :goto_9
.end method

.method protected getCenterBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isAlbum()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->albumBitmap:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->videoBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isPanorama()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->panoramaBitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getDescription()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getOneUpIntent(Z)Landroid/content/Intent;
    .locals 4
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getOneUpIntent(Z)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    if-lez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    if-lez v2, :cond_2

    const-string v2, "photo_width"

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "photo_height"

    iget v3, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_3

    const-string v2, "photo_ref"

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "is_album"

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isAlbum()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "album_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected getSubtitle()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected hasCustomLayout()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 15
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v1, 0x1d

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getWidth()S

    move-result v12

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getHeight()S

    move-result v11

    if-eqz v12, :cond_0

    if-nez v11, :cond_1

    :cond_0
    move-object/from16 v0, p2

    iget v12, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->columnWidth:I

    move v11, v12

    :cond_1
    const/high16 v1, 0x3f800000

    int-to-float v2, v11

    mul-float/2addr v1, v2

    int-to-float v2, v12

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredInverseAspectRatio:F

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v5

    const v10, 0x7fffffff

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getVideoUrl()Ljava/lang/String;

    move-result-object v14

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {v14, v12, v11}, Lcom/google/android/apps/plus/util/ImageUtils;->rewriteYoutubeMediaUrl(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v13

    invoke-static {v14, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v12}, Lcom/google/android/apps/plus/util/ImageUtils;->getYoutubeMediaWidth(I)I

    move-result v10

    move-object v5, v13

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v14}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    :goto_0
    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getPhotoId()J

    move-result-wide v3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getMediaType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-static {v12, v10}, Ljava/lang/Math;->min(II)I

    move-result v12

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->getNumberOfColumnsForWidth(I)I

    move-result v1

    move/from16 v0, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSpan:I

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSpan:I

    move-object/from16 v0, p2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->getAvailableWidth(Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredInverseAspectRatio:F

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    move-object/from16 v0, p2

    iget v1, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamHeight:I

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/plus/util/StreamLayoutInfo;->streamWidth:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v9

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    if-le v1, v9, :cond_3

    iput v9, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    iget v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredInverseAspectRatio:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    :cond_3
    return-void

    :cond_4
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public final onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getAlbumId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getOwnerId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v4

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mIsSquarePost:Z

    move-object v5, p0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;->onMediaClicked(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;ZLcom/google/android/apps/plus/views/UpdateCardViewGroup;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V

    goto :goto_0
.end method

.method public onRecycle()V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->onRecycle()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDbEmbedMedia:Lcom/google/android/apps/plus/content/DbEmbedMedia;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDestRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaAreaRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredWidth:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredHeight:I

    iput v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mHeroTop:I

    const/high16 v0, 0x3f800000

    iput v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDesiredInverseAspectRatio:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeRunnable:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mResizePaintWithAlpha:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeEndTime:J

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mFadeIn:Z

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mMediaTitleLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mSubtitleLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mDescriptionLayout:Landroid/text/StaticLayout;

    return-void
.end method

.method public unbindResources()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
