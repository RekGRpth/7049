.class public Lcom/google/android/apps/plus/views/LocalReviewListItemView;
.super Landroid/widget/RelativeLayout;
.source "LocalReviewListItemView.java"


# instance fields
.field private mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

.field private mAuthorName:Landroid/widget/TextView;

.field private mIsFullText:Z

.field private mPublishDate:Landroid/widget/TextView;

.field private mRatingAspects:Landroid/widget/TextView;

.field private mReviewText:Landroid/widget/TextView;

.field private mTopBorder:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->top_border:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mTopBorder:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->author_avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->author_name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->publish_date:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->rating_aspects:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->review_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    return-void
.end method

.method public setAuthorAvatarOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setIsFullText(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mIsFullText:Z

    return-void
.end method

.method public setReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .locals 10
    .param p1    # Lcom/google/api/services/plusi/model/GoogleReviewProto;

    const/4 v0, 0x0

    const/16 v9, 0x8

    const/4 v2, 0x0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileId:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    :goto_0
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->author:Lcom/google/api/services/plusi/model/AuthorProto;

    iget-object v3, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    iget-object v3, v3, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/AuthorProto;->profileLink:Lcom/google/api/services/plusi/model/PlacePageLink;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlacePageLink;->text:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->publishDate:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    if-eqz v1, :cond_5

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->zagatAspectRatings:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    move-object v3, v0

    :goto_3
    if-eqz v3, :cond_9

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    move v1, v2

    :goto_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    new-instance v6, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->getContext()Landroid/content/Context;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$style;->ProfileLocalUserRating_AspectLabel:I

    invoke-direct {v6, v7, v8}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v5, "\u00a0"

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$style;->ProfileLocalUserRating_AspectValue:I

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v0, "\u00a0/\u00a03"

    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->getContext()Landroid/content/Context;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$style;->ProfileLocalUserRating_AspectExplanation:I

    invoke-direct {v5, v6, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq v1, v0, :cond_1

    const-string v0, "  "

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorAvatar:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mAuthorName:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mPublishDate:Landroid/widget/TextView;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_5
    move-object v3, v0

    goto/16 :goto_3

    :cond_6
    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v0, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->snippet:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mIsFullText:Z

    if-eqz v1, :cond_7

    iget-object v1, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v0, p1, Lcom/google/api/services/plusi/model/GoogleReviewProto;->fullText:Ljava/lang/String;

    :cond_7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v1, "\\<.*?>"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mRatingAspects:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mReviewText:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6
.end method

.method public setTopBorderVisible(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mTopBorder:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->mTopBorder:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
