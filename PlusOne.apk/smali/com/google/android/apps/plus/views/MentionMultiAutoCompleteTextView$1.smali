.class final Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;
.super Ljava/lang/Object;
.source "MentionMultiAutoCompleteTextView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 13
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    instance-of v10, p1, Landroid/text/Spannable;

    if-eqz v10, :cond_2

    move-object v9, p1

    check-cast v9, Landroid/text/Spannable;

    add-int v10, p2, p3

    add-int/lit8 v2, v10, -0x1

    const/4 v6, 0x0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    # invokes: Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getPersonList()Ljava/util/List;
    invoke-static {v10}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->access$000(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)Ljava/util/List;

    move-result-object v8

    const-class v10, Landroid/text/style/URLSpan;

    invoke-interface {v9, p2, v2, v10}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    invoke-static {v5}, Lcom/google/android/apps/plus/views/MentionSpan;->isMention(Landroid/text/style/URLSpan;)Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v9, v5}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    const/4 v6, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    if-eqz v6, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    # invokes: Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getPersonList()Ljava/util/List;
    invoke-static {v10}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->access$000(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)Ljava/util/List;

    move-result-object v1

    iget-object v10, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v10, v8, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->updateMentionAcls(Ljava/util/List;Ljava/util/List;)V

    :cond_2
    iget-object v10, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    sget v11, Lcom/google/android/apps/plus/R$dimen;->plus_mention_suggestion_popup_offset:I

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v10

    float-to-int v7, v10

    iget-object v10, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v11}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getCursorYPosition()I

    move-result v11

    add-int/2addr v11, v7

    iget-object v12, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getHeight()I

    move-result v12

    sub-int/2addr v11, v12

    invoke-virtual {v10, v11}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setDropDownVerticalOffset(I)V

    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;->this$0:Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    # getter for: Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->access$100(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)Lcom/google/android/apps/plus/util/MentionTokenizer;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gt v1, v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    # invokes: Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->adjustInputMethod(Z)V
    invoke-static {v2, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->access$200(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;Z)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
