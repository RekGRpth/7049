.class public final Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
.super Landroid/widget/MultiAutoCompleteTextView;
.source "MentionMultiAutoCompleteTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$SavedState;
    }
.end annotation


# instance fields
.field private mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

.field private mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

.field private mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->themedApplicationContext(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->themedApplicationContext(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->themedApplicationContext(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Landroid/widget/MultiAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getPersonList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)Lcom/google/android/apps/plus/util/MentionTokenizer;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionTokenizer:Lcom/google/android/apps/plus/util/MentionTokenizer;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;Z)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;
    .param p1    # Z

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->adjustInputMethod(Z)V

    return-void
.end method

.method private adjustInputMethod(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getInputType()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-eq v3, v4, :cond_0

    iget v3, v1, Lcom/google/android/apps/plus/phone/ScreenMetrics;->screenDisplayType:I

    if-eq v3, v4, :cond_0

    if-nez p1, :cond_2

    :cond_0
    const v3, -0x10001

    and-int v2, v0, v3

    :goto_0
    if-eq v0, v2, :cond_1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setRawInputType(I)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/plus/util/SoftInput;->getInputMethodManager(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v3, p0}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    :cond_1
    return-void

    :cond_2
    const/high16 v3, 0x10000

    or-int v2, v0, v3

    goto :goto_0
.end method

.method private getPersonList()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v9

    invoke-interface {v9}, Landroid/text/Spannable;->length()I

    move-result v10

    const/4 v12, 0x0

    invoke-interface {v9}, Landroid/text/Spannable;->length()I

    move-result v13

    const-class v14, Lcom/google/android/apps/plus/views/MentionSpan;

    invoke-interface {v9, v12, v13, v14}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/apps/plus/views/MentionSpan;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    const/4 v1, 0x0

    array-length v8, v2

    :goto_0
    if-ge v1, v8, :cond_2

    aget-object v12, v2, v1

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MentionSpan;->getAggregateId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    invoke-virtual {v7, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    aget-object v12, v2, v1

    invoke-interface {v9, v12}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v11

    aget-object v12, v2, v1

    invoke-interface {v9, v12}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v12, v0, 0x1

    invoke-static {v10, v12}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-interface {v9, v11, v12}, Landroid/text/Spannable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v12, "+"

    invoke-virtual {v3, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    const/4 v12, 0x1

    invoke-virtual {v3, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-static {v5, v3}, Lcom/google/android/apps/plus/content/EsPeopleData;->buildPersonFromPersonIdAndName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v6
.end method

.method private static themedApplicationContext(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/content/Context;
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/util/AttributeSet;

    if-eqz p1, :cond_1

    const/4 v2, 0x0

    const-string v3, "theme_style"

    invoke-interface {p1, v2, v3}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "dark"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$style;->CircleBrowserTheme_DarkActionBar:I

    :goto_0
    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v2

    :cond_0
    sget v1, Lcom/google/android/apps/plus/R$style;->CircleBrowserTheme:I

    goto :goto_0

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$style;->CircleBrowserTheme:I

    goto :goto_0
.end method


# virtual methods
.method protected final convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 8
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Landroid/database/Cursor;

    new-instance v4, Landroid/text/SpannableString;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "+"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->convertSelectionToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    const-string v5, "person_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcom/google/android/apps/plus/views/MentionSpan;

    invoke-direct {v1, v2}, Lcom/google/android/apps/plus/views/MentionSpan;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-interface {v4, v1, v5, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    return-object v4
.end method

.method public final destroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->close()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public final getCursorYPosition()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v1

    goto :goto_0
.end method

.method public final getCursorYTop()I
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v1

    goto :goto_0
.end method

.method public final init(Landroid/support/v4/app/Fragment;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Lcom/google/android/apps/plus/views/AudienceView;)V
    .locals 6
    .param p1    # Landroid/support/v4/app/Fragment;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/google/android/apps/plus/views/AudienceView;

    const/4 v5, 0x1

    new-instance v0, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v3

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;-><init>(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/LoaderManager;Lcom/google/android/apps/plus/content/EsAccount;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setPublicProfileSearchEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setIncludePlusPages(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->setMention(Ljava/lang/String;)V

    iput-object p4, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/google/android/apps/plus/util/MentionTokenizer;

    invoke-direct {v0}, Lcom/google/android/apps/plus/util/MentionTokenizer;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setTokenizer(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setThreshold(I)V

    new-instance v0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$1;-><init>(Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->adjustInputMethod(Z)V

    return-void
.end method

.method protected final onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStart()V

    :cond_0
    return-void
.end method

.method protected final onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onStop()V

    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 12
    .param p1    # Landroid/os/Parcelable;

    move-object v6, p1

    check-cast v6, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$SavedState;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v8

    invoke-super {p0, v8}, Landroid/widget/MultiAutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    iget-object v9, v6, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$SavedState;->adapterState:Landroid/os/Bundle;

    invoke-virtual {v8, v9}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onCreate(Landroid/os/Bundle;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getEditableText()Landroid/text/Editable;

    move-result-object v7

    invoke-interface {v7}, Landroid/text/Editable;->length()I

    move-result v1

    const/4 v8, 0x0

    const-class v9, Landroid/text/style/URLSpan;

    invoke-interface {v7, v8, v1, v9}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/text/style/URLSpan;

    move-object v0, v5

    array-length v3, v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v4, v0, v2

    invoke-static {v4}, Lcom/google/android/apps/plus/views/MentionSpan;->isMention(Landroid/text/style/URLSpan;)Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v8, Lcom/google/android/apps/plus/views/MentionSpan;

    invoke-direct {v8, v4}, Lcom/google/android/apps/plus/views/MentionSpan;-><init>(Landroid/text/style/URLSpan;)V

    invoke-interface {v7, v4}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v9

    invoke-interface {v7, v4}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v10

    invoke-interface {v7, v4}, Landroid/text/Editable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v11

    invoke-interface {v7, v4}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    invoke-interface {v7, v8, v9, v10, v11}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/widget/MultiAutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    if-eqz v2, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mMentionCursorAdapter:Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/fragments/PeopleSearchListAdapter;->onSaveInstanceState(Landroid/os/Bundle;)V

    :cond_0
    new-instance v2, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$SavedState;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView$SavedState;-><init>(Landroid/os/Parcelable;Landroid/os/Bundle;)V

    return-object v2
.end method

.method protected final replaceText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getPersonList()Ljava/util/List;

    move-result-object v1

    invoke-super {p0, p1}, Landroid/widget/MultiAutoCompleteTextView;->replaceText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->getPersonList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->updateMentionAcls(Ljava/util/List;Ljava/util/List;)V

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->adjustInputMethod(Z)V

    return-void
.end method

.method public final setHtml(Ljava/lang/String;)V
    .locals 15
    .param p1    # Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    const/4 v12, 0x0

    invoke-interface {v2}, Landroid/text/Spanned;->length()I

    move-result v13

    const-class v14, Ljava/lang/Object;

    invoke-interface {v2, v12, v13, v14}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    array-length v12, v9

    add-int/lit8 v3, v12, -0x1

    :goto_1
    if-ltz v3, :cond_8

    aget-object v8, v9, v3

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v1

    instance-of v12, v8, Landroid/text/style/StyleSpan;

    if-eqz v12, :cond_5

    move-object v12, v8

    check-cast v12, Landroid/text/style/StyleSpan;

    invoke-virtual {v12}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v10

    const/4 v12, 0x1

    if-ne v10, v12, :cond_3

    const-string v12, "*"

    invoke-virtual {v7, v1, v12}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v12, "*"

    invoke-virtual {v7, v6, v12}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    :goto_2
    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_3
    const/4 v12, 0x2

    if-ne v10, v12, :cond_4

    const-string v12, "_"

    invoke-virtual {v7, v1, v12}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v12, "_"

    invoke-virtual {v7, v6, v12}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_4
    const/4 v12, 0x3

    if-ne v10, v12, :cond_1

    const-string v12, "*_"

    invoke-virtual {v7, v1, v12}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const-string v12, "_*"

    invoke-virtual {v7, v6, v12}, Landroid/text/SpannableStringBuilder;->insert(ILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_5
    instance-of v12, v8, Landroid/text/style/URLSpan;

    if-eqz v12, :cond_1

    move-object v12, v8

    check-cast v12, Landroid/text/style/URLSpan;

    invoke-virtual {v12}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_6

    invoke-static {v11}, Lcom/google/android/apps/plus/phone/Intents;->isProfileUrl(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    if-eqz v6, :cond_2

    add-int/lit8 v12, v6, -0x1

    invoke-virtual {v7, v12}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v0

    const/16 v12, 0x2b

    if-ne v0, v12, :cond_2

    invoke-static {v11}, Lcom/google/android/apps/plus/phone/Intents;->getPersonIdFromProfileUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v4, Lcom/google/android/apps/plus/views/MentionSpan;

    invoke-direct {v4, v5}, Lcom/google/android/apps/plus/views/MentionSpan;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v6, -0x1

    const/4 v13, 0x0

    invoke-virtual {v7, v4, v12, v1, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_2

    :cond_6
    if-eqz v11, :cond_7

    invoke-static {v11}, Lcom/google/android/apps/plus/phone/Intents;->isHashtagUrl(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    :cond_7
    invoke-virtual {v7, v6, v1, v11}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    :cond_8
    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method protected final updateMentionAcls(Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/content/PersonData;",
            ">;)V"
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/PersonData;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/AudienceView;->addPerson(Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/content/PersonData;

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/content/EsPeopleData;->isPersonInList(Lcom/google/android/apps/plus/content/PersonData;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MentionMultiAutoCompleteTextView;->mAudienceView:Lcom/google/android/apps/plus/views/AudienceView;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/plus/views/AudienceView;->removePerson(Lcom/google/android/apps/plus/content/PersonData;)V

    goto :goto_1
.end method
