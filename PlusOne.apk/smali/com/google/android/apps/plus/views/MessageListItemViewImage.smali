.class public Lcom/google/android/apps/plus/views/MessageListItemViewImage;
.super Landroid/widget/RelativeLayout;
.source "MessageListItemViewImage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;
    }
.end annotation


# static fields
.field private static sDefaultUserImage:Landroid/graphics/Bitmap;

.field private static sFailedAuthorColor:I

.field private static sNormalAuthorColor:I


# instance fields
.field private mAuthorArrow:Landroid/widget/ImageView;

.field private mAuthorNameTextView:Landroid/widget/TextView;

.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mCancelButton:Landroid/widget/TextView;

.field private mFullResUrl:Ljava/lang/String;

.field private mGaiaId:Ljava/lang/String;

.field private mImageFrame:Landroid/view/View;

.field private mImageHeight:Ljava/lang/Integer;

.field private mImageWidth:Ljava/lang/Integer;

.field private mMeasuredListener:Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;

.field private mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

.field private mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

.field private mMessageRowId:J

.field private mMessageStatus:I

.field private mPosition:I

.field private mRetryButton:Landroid/widget/TextView;

.field private mShowAuthor:Z

.field private mShowStatus:Z

.field private mStatusImage:Landroid/widget/ImageView;

.field private mTimeSinceTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageWidth:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageHeight:Ljava/lang/Integer;

    sget-object v1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->sDefaultUserImage:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getMediumDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->sDefaultUserImage:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$color;->realtimechat_message_author:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->sNormalAuthorColor:I

    sget v1, Lcom/google/android/apps/plus/R$color;->realtimechat_message_author_failed:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->sFailedAuthorColor:I

    :cond_0
    return-void
.end method

.method private updateStatusImages()V
    .locals 4

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowStatus:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageStatus:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageStatus:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageStatus:I

    if-ne v0, v2, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->sFailedAuthorColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_huddle_sending:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_huddle_sent:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_huddle_read:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    sget v1, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->sNormalAuthorColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowAuthor:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final clear()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mGaiaId:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public final getFullResUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mFullResUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final getImageHeight()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageHeight:Ljava/lang/Integer;

    return-object v0
.end method

.method public final getImageWidth()Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageWidth:Ljava/lang/Integer;

    return-object v0
.end method

.method public final hideAuthor()V
    .locals 2

    const/16 v1, 0x8

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowAuthor:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->updateStatusImages()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/MessageClickListener;->onUserImageClicked(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    iget-wide v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageRowId:J

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/MessageClickListener;->onRetryButtonClicked(J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mCancelButton:Landroid/widget/TextView;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    iget-wide v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageRowId:J

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/MessageClickListener;->onCancelButtonClicked(J)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mFullResUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/MessageClickListener;->onMediaImageClick(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 2

    const/4 v1, 0x1

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar_image:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->authorArrow:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorArrow:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->authorName:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->messageImage:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->image_frame:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageFrame:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->timeSince:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->message_status:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mStatusImage:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/apps/plus/R$id;->retry_send:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mRetryButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->cancel_send:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mCancelButton:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mCancelButton:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowStatus:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowAuthor:Z

    return-void
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getMeasuredWidth()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageWidth:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->getMeasuredHeight()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageHeight:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMeasuredListener:Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMeasuredListener:Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setAuthorName(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setGaiaId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    return-void
.end method

.method public setImage(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mFullResUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageFrame:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1    # Ljava/lang/CharSequence;

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageImageView:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mImageFrame:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$color;->clear:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setBackgroundResource(I)V

    return-void
.end method

.method public setMessageClickListener(Lcom/google/android/apps/plus/views/MessageClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/MessageClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageClickListener:Lcom/google/android/apps/plus/views/MessageClickListener;

    return-void
.end method

.method public setMessageRowId(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageRowId:J

    return-void
.end method

.method public setMessageStatus(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z

    iput p1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMessageStatus:I

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowStatus:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->updateStatusImages()V

    return-void
.end method

.method public setOnMeasureListener(Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mMeasuredListener:Lcom/google/android/apps/plus/views/MessageListItemViewImage$OnMeasuredListener;

    return-void
.end method

.method public setPosition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mPosition:I

    return-void
.end method

.method public setTimeSince(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final showAuthor()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowAuthor:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->updateStatusImages()V

    return-void
.end method

.method public final updateContentDescription()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mShowAuthor:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mAuthorNameTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$string;->realtimechat_message_description_author:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->mTimeSinceTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-lez v4, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$string;->realtimechat_message_description_time_since:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$string;->realtimechat_message_description_image:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/MessageListItemViewImage;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method
