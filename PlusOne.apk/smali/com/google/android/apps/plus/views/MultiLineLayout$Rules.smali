.class Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;
.super Ljava/lang/Object;
.source "MultiLineLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/MultiLineLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Rules"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/MultiLineLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/MultiLineLayout;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;-><init>(Lcom/google/android/apps/plus/views/MultiLineLayout;)V

    return-void
.end method


# virtual methods
.method public apply(I)V
    .locals 13
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingLeft()I

    move-result v7

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingTop()I

    move-result v11

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingLeft()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingTop()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    iget-object v12, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v12}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingRight()I

    move-result v12

    sub-int v9, v0, v12

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getChildCount()I

    move-result v6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    const/4 v12, 0x1

    invoke-static {v0, v12}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$302(Lcom/google/android/apps/plus/views/MultiLineLayout;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    const/4 v12, 0x0

    # setter for: Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I
    invoke-static {v0, v12}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$402(Lcom/google/android/apps/plus/views/MultiLineLayout;I)I

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v6, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v12, 0x8

    if-eq v0, v12, :cond_2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->measure(Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    # getter for: Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$400(Lcom/google/android/apps/plus/views/MultiLineLayout;)I

    move-result v0

    if-ge v0, v5, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    # setter for: Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I
    invoke-static {v0, v5}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$402(Lcom/google/android/apps/plus/views/MultiLineLayout;I)I

    :cond_0
    add-int v0, v2, v4

    if-le v0, v9, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    # operator++ for: Lcom/google/android/apps/plus/views/MultiLineLayout;->mNumLines:I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->access$308(Lcom/google/android/apps/plus/views/MultiLineLayout;)I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->this$0:Lcom/google/android/apps/plus/views/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingLeft()I

    move-result v2

    add-int v0, v10, v11

    add-int/2addr v3, v0

    const/4 v10, 0x0

    :cond_1
    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;->layout(Landroid/view/View;IIII)V

    add-int v0, v4, v7

    add-int/2addr v2, v0

    invoke-static {v10, v5}, Ljava/lang/Math;->max(II)I

    move-result v10

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected layout(Landroid/view/View;IIII)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    return-void
.end method

.method protected measure(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method
