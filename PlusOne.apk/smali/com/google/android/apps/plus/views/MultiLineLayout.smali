.class public Lcom/google/android/apps/plus/views/MultiLineLayout;
.super Landroid/view/ViewGroup;
.source "MultiLineLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/MultiLineLayout$Rules;
    }
.end annotation


# instance fields
.field private mChipHeight:I

.field private mNumLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mNumLines:I

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/MultiLineLayout;Landroid/view/View;II)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/MultiLineLayout;
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/MultiLineLayout;->measureChild(Landroid/view/View;II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/MultiLineLayout;II)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/MultiLineLayout;
    .param p1    # I
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/plus/views/MultiLineLayout;->setMeasuredDimension(II)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/views/MultiLineLayout;I)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/MultiLineLayout;
    .param p1    # I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mNumLines:I

    return v0
.end method

.method static synthetic access$308(Lcom/google/android/apps/plus/views/MultiLineLayout;)I
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/views/MultiLineLayout;

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mNumLines:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mNumLines:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/MultiLineLayout;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/MultiLineLayout;

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/views/MultiLineLayout;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/MultiLineLayout;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I

    return p1
.end method


# virtual methods
.method public final getHeightForNumLines(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/MultiLineLayout;->getPaddingTop()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mChipHeight:I

    mul-int/2addr v2, p1

    add-int/2addr v1, v2

    return v1
.end method

.method public final getNumLines()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/MultiLineLayout;->mNumLines:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    new-instance v0, Lcom/google/android/apps/plus/views/MultiLineLayout$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/MultiLineLayout$1;-><init>(Lcom/google/android/apps/plus/views/MultiLineLayout;)V

    sub-int v1, p4, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MultiLineLayout$1;->apply(I)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    new-instance v0, Lcom/google/android/apps/plus/views/MultiLineLayout$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/plus/views/MultiLineLayout$2;-><init>(Lcom/google/android/apps/plus/views/MultiLineLayout;II)V

    const v1, 0x7fffffff

    invoke-static {v1, p1}, Lcom/google/android/apps/plus/views/MultiLineLayout;->resolveSize(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/MultiLineLayout$2;->apply(I)V

    return-void
.end method
