.class public Lcom/google/android/apps/plus/views/NavigationBarLayout;
.super Lcom/google/android/apps/plus/views/SlidingPanelLayout;
.source "NavigationBarLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

.field private mMaxNavigationBarWidth:I

.field private mMinNavigationBarWidth:I

.field private mNavigationBarWidth:I

.field private mNavigationBarWidthPercent:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setVertical(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setVertical(Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setVertical(Z)V

    return-void
.end method

.method private isScrolling()Z
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->getScroll()I

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    neg-int v1, v1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->smoothScrollTo(I)V

    goto :goto_0
.end method

.method public final getNavigationBarWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onFinishInflate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_min_navigation_bar_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mMinNavigationBarWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_max_navigation_bar_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mMaxNavigationBarWidth:I

    sget v1, Lcom/google/android/apps/plus/R$integer;->host_navigation_bar_width_percent:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidthPercent:I

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-ltz v2, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mPanel:Landroid/view/View;

    sub-int v1, p4, p2

    sub-int v2, p5, p3

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isScrolling()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, v3, v3}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->scrollTo(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    neg-int v0, v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->scrollTo(II)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/high16 v4, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-super {p0, p1, p2}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onMeasure(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mPanel:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    iget v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidthPercent:I

    mul-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x64

    iget v3, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mMinNavigationBarWidth:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mMaxNavigationBarWidth:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    iget v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    neg-int v2, v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setScrollLimits(II)V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onScrollChanged(IIII)V

    if-nez p1, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;->onNavigationBarClosed()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    neg-int v0, v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;->onNavigationBarOpened()V

    goto :goto_0
.end method

.method protected final onScrollFinished(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    if-gez p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    neg-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->smoothScrollTo(I)V

    :goto_0
    return-void

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->smoothScrollTo(I)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->isScrolling()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method public final open()V
    .locals 2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mOpen:Z

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->setScrollEnabled(Z)V

    iget v0, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mNavigationBarWidth:I

    neg-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->smoothScrollTo(I)V

    goto :goto_0
.end method

.method public performClick()Z
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/SlidingPanelLayout;->performClick()Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/NavigationBarLayout;->close()V

    const/4 v0, 0x1

    return v0
.end method

.method public setOnNavigationBarStateChange(Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/NavigationBarLayout;->mListener:Lcom/google/android/apps/plus/views/NavigationBarLayout$OnNavigationBarStateChange;

    return-void
.end method
