.class public Lcom/google/android/apps/plus/views/OneUpLinkView;
.super Landroid/view/View;
.source "OneUpLinkView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;
    }
.end annotation


# static fields
.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field protected static sLinkBitmap:Landroid/graphics/Bitmap;

.field protected static sMaxWidth:I

.field private static sMinExposureLand:I

.field private static sMinExposurePort:I

.field private static sOneUpLinkViewInitialized:Z

.field protected static final sResizePaint:Landroid/graphics/Paint;


# instance fields
.field protected mAvailableContentHeight:I

.field protected mBackgroundDestRect:Landroid/graphics/Rect;

.field protected mBackgroundSrcRect:Landroid/graphics/Rect;

.field protected mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field protected mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mDeepLinkLabel:Ljava/lang/String;

.field protected mDeepLinkListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

.field protected mDefaultVerticalPadding:I

.field protected mHasSeenImage:Z

.field protected mImageBorderRect:Landroid/graphics/Rect;

.field protected mImageDimension:I

.field protected mImageRect:Landroid/graphics/Rect;

.field protected mImageResource:Lcom/google/android/apps/plus/service/Resource;

.field protected mImageSourceRect:Landroid/graphics/Rect;

.field protected mIsAppInvite:Z

.field protected mLinkDescription:Ljava/lang/String;

.field protected mLinkDescriptionLayout:Landroid/text/StaticLayout;

.field protected mLinkTitle:Ljava/lang/String;

.field protected mLinkTitleLayout:Landroid/text/StaticLayout;

.field protected mLinkUrl:Ljava/lang/String;

.field protected mLinkUrlLayout:Landroid/text/StaticLayout;

.field protected mListener:Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;

.field protected mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field protected mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sResizePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/OneUpLinkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/OneUpLinkView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-boolean v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sOneUpLinkViewInitialized:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sOneUpLinkViewInitialized:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$drawable;->ic_metadata_link:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sLinkBitmap:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_land:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposureLand:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_min_height_port:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposurePort:I

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->riviera_default_padding:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDefaultVerticalPadding:I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageBorderRect:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageSourceRect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    iget v3, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v6, :cond_1

    move v0, v4

    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    return v5

    :cond_1
    move v0, v5

    goto :goto_0

    :pswitch_1
    if-eqz v0, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v6, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableButton;->handleEvent(III)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->invalidate()V

    :cond_2
    move v5, v4

    goto :goto_1

    :pswitch_2
    iput-object v7, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v0, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v6, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableButton;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_3

    move v1, v4

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->invalidate()V

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->performClick()Z

    goto :goto_1

    :cond_3
    move v1, v5

    goto :goto_2

    :pswitch_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->invalidate()V

    move v5, v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected getMinExposureLand()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposureLand:I

    return v0
.end method

.method protected getMinExposurePort()I
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMinExposurePort:I

    return v0
.end method

.method public final init(Lcom/google/android/apps/plus/api/MediaRef;ILcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
    .param p8    # Ljava/lang/String;
    .param p9    # Z

    iput-object p4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitle:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescription:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkLabel:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    iput-object p8, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrl:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput p2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mType:I

    iput-object p3, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mListener:Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;

    iput-boolean p9, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mIsAppInvite:Z

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x3a83126f

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->setAlpha(F)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->invalidate()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->unbindResources()V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getAppInviteTopAreaBackgroundPaint()Landroid/graphics/Paint;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_d

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    float-to-int v13, v1

    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-nez v1, :cond_9

    const/4 v14, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-nez v1, :cond_a

    const/4 v12, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-nez v1, :cond_b

    const/4 v11, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-nez v1, :cond_c

    const/4 v15, 0x0

    :goto_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    sub-int/2addr v1, v14

    sub-int/2addr v1, v12

    sub-int/2addr v1, v11

    sub-int/2addr v1, v15

    div-int/lit8 v1, v1, 0x2

    add-int v5, v1, v13

    :goto_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mIsAppInvite:Z

    if-eqz v1, :cond_e

    const/4 v10, 0x0

    :goto_7
    move-object/from16 v3, p1

    invoke-static/range {v3 .. v10}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->drawTitleDeepLinkAndUrl(Landroid/graphics/Canvas;IILandroid/text/StaticLayout;Landroid/text/StaticLayout;Lcom/google/android/apps/plus/views/ClickableButton;Landroid/text/StaticLayout;Landroid/graphics/Bitmap;)V

    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mHasSeenImage:Z

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    if-nez v1, :cond_3

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/OneUpLinkView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v3, 0x3f800000

    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v6, 0x1f4

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/views/OneUpLinkView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    :cond_4
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mHasSeenImage:Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageSourceRect:Landroid/graphics/Rect;

    invoke-static {v2, v1}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createImageSourceRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createBackgroundSourceRect(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageSourceRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageBorderRect:Landroid/graphics/Rect;

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v7}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    float-to-int v13, v1

    goto/16 :goto_1

    :cond_8
    const/4 v13, 0x0

    goto/16 :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v14

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v12

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v11

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v15

    goto/16 :goto_5

    :cond_d
    const/4 v5, 0x0

    goto/16 :goto_6

    :cond_e
    sget-object v10, Lcom/google/android/apps/plus/views/OneUpLinkView;->sLinkBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_7
.end method

.method public onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getMeasuredHeight()I

    move-result v6

    sget v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sMaxWidth:I

    if-gt v7, v0, :cond_6

    iput v6, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_8

    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getMaxImageDimension()I

    move-result v9

    iget v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    if-nez v0, :cond_0

    int-to-float v0, v7

    invoke-static {}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->getImageMaxWidthPercentage()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    invoke-static {v9, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->bindResources()V

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundDestRect:Landroid/graphics/Rect;

    invoke-static {v0, v1, v7, v6, v2}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createBackgroundDestRect(IIIILandroid/graphics/Rect;)V

    iget v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    iget v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageBorderRect:Landroid/graphics/Rect;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createImageRects(IIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    sub-int v4, v7, v0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mIsAppInvite:Z

    if-eqz v0, :cond_9

    sget v0, Lcom/google/android/apps/plus/R$integer;->interactive_post_title_max_lines:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitle:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    invoke-static {v0, v10, v4}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createTitle$24306a25(Ljava/lang/String;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/lit8 v3, v0, 0x0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$integer;->riviera_link_description_max_lines:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    if-ne v10, v0, :cond_2

    const/4 v0, 0x1

    add-int/lit8 v1, v8, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v8

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mIsAppInvite:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescription:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    invoke-static {v0, v1, v8, v4}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createDescription$5f444f9d(Landroid/content/Context;Ljava/lang/String;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkDescriptionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v3, v0

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrl:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mIsAppInvite:Z

    if-eqz v0, :cond_a

    const/4 v0, 0x0

    :goto_3
    sub-int v0, v4, v0

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createUrl$54208d16(Ljava/lang/String;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mLinkUrlLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v3, v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkLabel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDefaultVerticalPadding:I

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkLabel:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkListener:Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/util/LinksRenderUtils;->createDeepLinkButton(Landroid/content/Context;Ljava/lang/String;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mDeepLinkButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mBackgroundSrcRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    return-void

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getMinExposureLand()I

    move-result v0

    sub-int v0, v6, v0

    iput v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->getMinExposurePort()I

    move-result v0

    sub-int v0, v6, v0

    iput v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    goto/16 :goto_0

    :cond_8
    iget v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mAvailableContentHeight:I

    iput v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageDimension:I

    goto/16 :goto_1

    :cond_9
    sget v0, Lcom/google/android/apps/plus/R$integer;->riviera_link_title_max_lines:I

    goto/16 :goto_2

    :cond_a
    sget-object v0, Lcom/google/android/apps/plus/views/OneUpLinkView;->sLinkBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    goto :goto_3
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/OneUpLinkView;->invalidate()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mListener:Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mListener:Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/OneUpLinkView$BackgroundViewLoadedListener;->onBackgroundViewLoaded$6a6d9065()V

    :cond_0
    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/OneUpLinkView;->mImageSourceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    return-void
.end method
