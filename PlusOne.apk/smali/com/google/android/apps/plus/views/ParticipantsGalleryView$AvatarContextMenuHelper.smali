.class final Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;
.super Ljava/lang/Object;
.source "ParticipantsGalleryView.java"

# interfaces
.implements Landroid/view/ContextMenu$ContextMenuInfo;
.implements Landroid/view/MenuItem$OnMenuItemClickListener;
.implements Landroid/view/View$OnCreateContextMenuListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ParticipantsGalleryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AvatarContextMenuHelper"
.end annotation


# instance fields
.field private final mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private final mContext:Landroid/content/Context;

.field private final mParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-void
.end method


# virtual methods
.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3
    .param p1    # Landroid/view/ContextMenu;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ContextMenu$ContextMenuInfo;

    new-instance v1, Landroid/view/MenuInflater;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/plus/R$menu;->conversation_avatar_menu:I

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->menu_avatar_profile:I

    invoke-interface {p1, v1}, Landroid/view/ContextMenu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    invoke-interface {v0, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    return-void
.end method

.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mParticipant:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/apps/plus/phone/Intents;->getProfileActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x1

    return v1
.end method
