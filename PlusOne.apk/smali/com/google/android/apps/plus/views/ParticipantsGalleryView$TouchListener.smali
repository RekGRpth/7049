.class final Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ParticipantsGalleryView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ParticipantsGalleryView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TouchListener"
.end annotation


# instance fields
.field private final avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

.field private final gestureDetector:Landroid/view/GestureDetector;

.field final synthetic this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;Lcom/google/android/apps/plus/views/OverlayedAvatarView;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->gestureDetector:Landroid/view/GestureDetector;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p0}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    return-void
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->access$000(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->access$000(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;->onAvatarDoubleClicked(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->access$000(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->this$0:Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    # getter for: Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->access$000(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->avatarView:Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;->onAvatarClicked(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Lcom/google/wireless/realtimechat/proto/Data$Participant;)V

    :cond_0
    const/4 v1, 0x1

    return v1
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;->gestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
