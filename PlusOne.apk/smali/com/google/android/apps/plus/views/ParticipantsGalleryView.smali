.class public Lcom/google/android/apps/plus/views/ParticipantsGalleryView;
.super Landroid/widget/FrameLayout;
.source "ParticipantsGalleryView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;,
        Lcom/google/android/apps/plus/views/ParticipantsGalleryView$AvatarContextMenuHelper;,
        Lcom/google/android/apps/plus/views/ParticipantsGalleryView$SimpleCommandListener;,
        Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field protected avatarContextMenuDialog:Landroid/app/Dialog;

.field private mAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

.field private mEmptyMessageView:Landroid/widget/TextView;

.field private mParticipantListButton:Landroid/view/View;

.field private mParticipantTrayAvatars:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v4, Lcom/google/android/apps/plus/R$layout;->participants_gallery_view:I

    invoke-virtual {v2, v4, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->empty_message:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mEmptyMessageView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$id;->participant_tray_avatars:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    sget v4, Lcom/google/android/apps/plus/R$id;->show_participant_list_button:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantListButton:Landroid/view/View;

    if-eqz p2, :cond_0

    sget-object v4, Lcom/google/android/apps/plus/R$styleable;->ParticipantsGalleryFragment:[I

    invoke-virtual {p1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-virtual {v3, v5, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setBackgroundColor(I)V

    invoke-virtual {v3, v6}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mEmptyMessageView:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantListButton:Landroid/view/View;

    new-instance v5, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$1;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$1;-><init>(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;)Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/ParticipantsGalleryView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    return-object v0
.end method


# virtual methods
.method public final addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "#setAccount needs to be called first"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mEmptyMessageView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    invoke-static {p1, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->create(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setTag(Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/google/android/apps/plus/content/EsConversationsData;->convertParticipantType(Lcom/google/wireless/realtimechat/proto/Data$Participant;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setParticipantType(I)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->hasParticipantId()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsPeopleData;->extractGaiaId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setParticipantGaiaId(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v1, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView$TouchListener;-><init>(Lcom/google/android/apps/plus/views/ParticipantsGalleryView;Lcom/google/android/apps/plus/views/OverlayedAvatarView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getFullName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final addParticipants(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final dismissAvatarMenuDialog()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->avatarContextMenuDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->avatarContextMenuDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->avatarContextMenuDialog:Landroid/app/Dialog;

    :cond_0
    return-void
.end method

.method public final removeAllParticipants()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mEmptyMessageView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public setAccount(Lcom/google/android/apps/plus/content/EsAccount;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;

    sget-boolean v0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/content/EsAccount;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1
    .param p1    # I

    sget v0, Lcom/google/android/apps/plus/R$id;->root_view:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method public setCommandListener(Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mCommandListener:Lcom/google/android/apps/plus/views/ParticipantsGalleryView$CommandListener;

    return-void
.end method

.method public setEmptyMessage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mEmptyMessageView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/OverlayedAvatarView;
    .param p2    # Z

    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$color;->participants_gallery_active_border:I

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setBorderResource(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setParticipantActive(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantTrayAvatars:Landroid/view/ViewGroup;

    invoke-virtual {v3, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getParticipantId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setParticipantListButtonVisibility(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->mParticipantListButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setParticipantLoudestSpeaker(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/OverlayedAvatarView;
    .param p2    # Z

    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$color;->participants_gallery_loudest_speaker_border:I

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/OverlayedAvatarView;->setBorderResource(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setParticipants(Ljava/util/HashMap;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->removeAllParticipants()V

    new-instance v5, Ljava/util/HashSet;

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->dismissAvatarMenuDialog()V

    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v0

    const/4 v6, 0x1

    invoke-virtual {p0, v0, v6}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v0

    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->addParticipant(Landroid/view/LayoutInflater;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/android/apps/plus/views/OverlayedAvatarView;

    move-result-object v0

    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/plus/views/ParticipantsGalleryView;->setParticipantActive(Lcom/google/android/apps/plus/views/OverlayedAvatarView;Z)V

    goto :goto_2

    :cond_4
    return-void
.end method
