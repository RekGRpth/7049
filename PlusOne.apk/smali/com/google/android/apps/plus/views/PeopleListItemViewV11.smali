.class public Lcom/google/android/apps/plus/views/PeopleListItemViewV11;
.super Lcom/google/android/apps/plus/views/PeopleListItemView;
.source "PeopleListItemViewV11.java"

# interfaces
.implements Landroid/widget/AbsListView$SelectionBoundsAdjuster;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PeopleListItemView;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public adjustListItemSelectionBounds(Landroid/graphics/Rect;)V
    .locals 2
    .param p1    # Landroid/graphics/Rect;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListItemViewV11;->mSectionHeaderVisible:Z

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/google/android/apps/plus/views/PeopleListItemViewV11;->mSectionHeaderHeight:I

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    :cond_0
    return-void
.end method

.method protected final setSectionHeaderBackgroundColor()V
    .locals 0

    return-void
.end method
