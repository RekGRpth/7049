.class public Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;
.super Landroid/widget/FrameLayout;
.source "PeopleListRowAsCardView.java"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;


# static fields
.field protected static sBackgroundBody:Landroid/graphics/drawable/Drawable;

.field protected static sBackgroundCard:Landroid/graphics/drawable/Drawable;

.field protected static sBackgroundFooter:Landroid/graphics/drawable/Drawable;

.field protected static sBackgroundHeader:Landroid/graphics/drawable/Drawable;

.field protected static sDefaultText:Ljava/lang/String;


# instance fields
.field protected mRowDividerView:Landroid/view/View;

.field protected mRowView:Landroid/view/View;

.field protected mViewAll:Landroid/view/View;

.field protected mViewAllText:Landroid/widget/TextView;

.field protected mWrapper:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundHeader:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundCard:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos_header:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundHeader:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos_body:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundBody:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->bg_tacos_footer:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundFooter:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$string;->find_people_view_all:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sDefaultText:Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method public final enableViewAll(ZLjava/lang/String;)Landroid/view/View;
    .locals 3
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mViewAll:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowDividerView:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mViewAll:Landroid/view/View;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mViewAllText:Landroid/widget/TextView;

    if-eqz p2, :cond_3

    :goto_2
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mViewAll:Landroid/view/View;

    return-object v0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1

    :cond_3
    sget-object p2, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sDefaultText:Ljava/lang/String;

    goto :goto_2
.end method

.method public final getRowView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowView:Landroid/view/View;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    sget v0, Lcom/google/android/apps/plus/R$id;->wrapper:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mWrapper:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->divider:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowDividerView:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->row:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowView:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->view_all:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mViewAll:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->view_all_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mViewAllText:Landroid/widget/TextView;

    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowView:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/AbsListView$RecyclerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowView:Landroid/view/View;

    check-cast v0, Landroid/widget/AbsListView$RecyclerListener;

    invoke-interface {v0, p1}, Landroid/widget/AbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setIsBodyRow()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mWrapper:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundBody:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->enableViewAll(ZLjava/lang/String;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowDividerView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setIsFirstAndLastRow(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    invoke-virtual {p0, v2, p1, v2, p2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mWrapper:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundCard:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->enableViewAll(ZLjava/lang/String;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowDividerView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setIsFirstRow(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0, v2, p1, v2, v2}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mWrapper:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundHeader:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->enableViewAll(ZLjava/lang/String;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowDividerView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setIsLastRow(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    invoke-virtual {p0, v2, v2, v2, p1}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mWrapper:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->sBackgroundFooter:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->enableViewAll(ZLjava/lang/String;)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowAsCardView;->mRowDividerView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
