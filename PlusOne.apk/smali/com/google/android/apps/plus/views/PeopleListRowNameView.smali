.class public Lcom/google/android/apps/plus/views/PeopleListRowNameView;
.super Landroid/widget/RelativeLayout;
.source "PeopleListRowNameView.java"


# static fields
.field private static sDetailsTextSize:F

.field private static sNamePaddingBottom:I

.field private static sNameTextSize:F


# instance fields
.field private mDetailsView:Landroid/widget/TextView;

.field private mDetailsViewIsVisible:Z

.field private mNameView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PeopleListRowNameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PeopleListRowNameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget v1, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNameTextSize:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->people_list_row_name_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNameTextSize:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->people_list_row_detail_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sDetailsTextSize:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->people_list_row_name_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNamePaddingBottom:I

    :cond_0
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNameTextSize:F

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sDetailsTextSize:F

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method final bind(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsViewIsVisible:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsViewIsVisible:Z

    if-eqz v0, :cond_1

    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsViewIsVisible:Z

    if-eqz v2, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    const/16 v1, 0x8

    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v6, 0x0

    sub-int v3, p4, p2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNamePaddingBottom:I

    add-int v1, v2, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int v0, v1, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v4, v6, v6, v3, v2}, Landroid/widget/TextView;->layout(IIII)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    sget v5, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNamePaddingBottom:I

    add-int/2addr v5, v2

    invoke-virtual {v4, v6, v5, v3, v0}, Landroid/widget/TextView;->layout(IIII)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/high16 v10, 0x40000000

    const/4 v7, 0x1

    const/high16 v6, -0x80000000

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    if-nez v0, :cond_2

    move v4, v5

    :goto_0
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v8, v9, v4}, Landroid/widget/TextView;->measure(II)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsViewIsVisible:Z

    if-eqz v4, :cond_1

    if-lez v0, :cond_0

    sget v4, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNamePaddingBottom:I

    add-int/2addr v4, v2

    sub-int/2addr v0, v4

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getLineCount()I

    move-result v4

    rsub-int/lit8 v1, v4, 0x3

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    if-ne v1, v7, :cond_3

    move v4, v7

    :goto_1
    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    if-nez v0, :cond_4

    :goto_2
    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v7, v5}, Landroid/widget/TextView;->measure(II)V

    sget v4, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->sNamePaddingBottom:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->mDetailsView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v2, v4

    :cond_1
    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/plus/views/PeopleListRowNameView;->setMeasuredDimension(II)V

    return-void

    :cond_2
    move v4, v6

    goto :goto_0

    :cond_3
    move v4, v5

    goto :goto_1

    :cond_4
    move v5, v6

    goto :goto_2
.end method
