.class public interface abstract Lcom/google/android/apps/plus/views/PeopleSuggestionsView$OnClickListener;
.super Ljava/lang/Object;
.source "PeopleSuggestionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/PeopleSuggestionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnClickListener"
.end annotation


# virtual methods
.method public abstract onAddToCircleButtonClicked(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method public abstract onAvatarClicked(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onFindClassmates()V
.end method

.method public abstract onFindCoworkers()V
.end method

.method public abstract onFollowInterestingPeople()V
.end method

.method public abstract onFriendsAddViewAll()V
.end method

.method public abstract onInACircleButtonClicked(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onOrganizationViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V
.end method

.method public abstract onSchoolViewAll(Lcom/google/api/services/plusi/model/ListResponse;)V
.end method

.method public abstract onYouMayKnowViewAll()V
.end method
