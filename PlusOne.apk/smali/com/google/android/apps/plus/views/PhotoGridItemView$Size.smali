.class public final Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;
.super Ljava/lang/Object;
.source "PhotoGridItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/PhotoGridItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Size"
.end annotation


# instance fields
.field public final height:I

.field public final width:I


# direct methods
.method constructor <init>(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->width:I

    iput p2, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->height:I

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    instance-of v2, p1, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    if-eqz v2, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;

    iget v2, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->width:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->width:I

    if-ne v2, v3, :cond_0

    iget v2, v0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->height:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->height:I

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public final hashCode()I
    .locals 3

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->width:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->height:I

    const v2, 0xffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "size["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoGridItemView$Size;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
