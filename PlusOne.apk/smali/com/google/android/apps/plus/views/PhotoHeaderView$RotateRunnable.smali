.class final Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;
.super Ljava/lang/Object;
.source "PhotoHeaderView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/PhotoHeaderView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RotateRunnable"
.end annotation


# instance fields
.field private mAppliedRotation:F

.field private final mHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

.field private mLastRuntime:J

.field private mRunning:Z

.field private mStop:Z

.field private mTargetRotation:F

.field private mVelocity:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mStop:Z

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    cmpl-float v5, v5, v6

    if-eqz v5, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mLastRuntime:J

    const-wide/16 v7, -0x1

    cmp-long v5, v5, v7

    if-eqz v5, :cond_7

    iget-wide v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mLastRuntime:J

    sub-long v0, v2, v5

    :goto_1
    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mVelocity:F

    long-to-float v6, v0

    mul-float v4, v5, v6

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    add-float/2addr v5, v4

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_3

    :cond_2
    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    add-float/2addr v5, v4

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    :cond_3
    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    sub-float v4, v5, v6

    :cond_4
    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->access$400(Lcom/google/android/apps/plus/views/PhotoHeaderView;FZ)V

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    add-float/2addr v5, v4

    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    iget v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->stop()V

    :cond_5
    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mLastRuntime:J

    :cond_6
    iget-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mStop:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v5, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_7
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method public final start(F)V
    .locals 2
    .param p1    # F

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mRunning:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mTargetRotation:F

    const/high16 v1, 0x43fa0000

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mVelocity:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mAppliedRotation:F

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mLastRuntime:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mStop:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mRunning:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final stop()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mRunning:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->mStop:Z

    return-void
.end method
