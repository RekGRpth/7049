.class public Lcom/google/android/apps/plus/views/PhotoHeaderView;
.super Landroid/view/View;
.source "PhotoHeaderView.java"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;,
        Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static sBackgroundColor:I

.field private static sCommentBitmap:Landroid/graphics/Bitmap;

.field private static sCommentCountLeftMargin:I

.field private static sCommentCountPaint:Landroid/text/TextPaint;

.field private static sCommentCountTextWidth:I

.field private static sCropDimPaint:Landroid/graphics/Paint;

.field private static sCropPaint:Landroid/graphics/Paint;

.field private static sCropSizeCoverWidth:I

.field private static sCropSizeProfile:I

.field private static sHasMultitouchDistinct:Z

.field private static sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sInitialized:Z

.field private static sMediaNotFoundTextPaint:Landroid/text/TextPaint;

.field private static sMediaNotFoundTitle:Ljava/lang/String;

.field private static sPanoramaImage:Landroid/graphics/Bitmap;

.field private static sPhotoOverlayBottomPadding:I

.field private static sPhotoOverlayRightPadding:I

.field private static sPlusOneBitmap:Landroid/graphics/Bitmap;

.field private static sPlusOneBottomMargin:I

.field private static sPlusOneCountLeftMargin:I

.field private static sPlusOneCountPaint:Landroid/text/TextPaint;

.field private static sPlusOneCountTextWidth:I

.field private static sProcessingMediaBackgroundPaint:Landroid/graphics/Paint;

.field private static sProcessingMediaSubTitle:Ljava/lang/String;

.field private static sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

.field private static sProcessingMediaSubTitleVerticalPosition:I

.field private static sProcessingMediaTitle:Ljava/lang/String;

.field private static sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

.field private static sProcessingMediaTitleVerticalPosition:I

.field private static sTagPaint:Landroid/graphics/Paint;

.field private static sTagTextBackgroundPaint:Landroid/graphics/Paint;

.field private static sTagTextPadding:I

.field private static sTagTextPaint:Landroid/text/TextPaint;

.field private static sVideoImage:Landroid/graphics/Bitmap;

.field private static sVideoNotReadyImage:Landroid/graphics/Bitmap;


# instance fields
.field private mAllowCrop:Z

.field private mAnimate:Z

.field private mBackgroundColor:I

.field private mCommentText:Ljava/lang/String;

.field private mCoverPhotoCoordinates:Landroid/graphics/RectF;

.field private mCoverPhotoOffset:Ljava/lang/Integer;

.field private mCropMode:I

.field private mCropRect:Landroid/graphics/Rect;

.field private mCropSizeHeight:I

.field private mCropSizeWidth:I

.field private mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

.field private mDoubleTapDebounce:Z

.field private mDoubleTapToZoomEnabled:Z

.field private mDrawMatrix:Landroid/graphics/Matrix;

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mExternalClickListener:Landroid/view/View$OnClickListener;

.field private mFixedHeight:I

.field private mFlingDebounce:Z

.field private mFullScreen:Z

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHaveLayout:Z

.field private mImageIsInvalid:Z

.field private mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

.field mInitialTranslationY:F

.field private mIsDoubleTouch:Z

.field private mIsPlaceHolder:Z

.field private mLastTwoFingerUp:J

.field private mLoadAnimatedImage:Z

.field private mMatrix:Landroid/graphics/Matrix;

.field private mMaxScale:F

.field mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mMinScale:F

.field mOriginalAspectRatio:F

.field private mOriginalMatrix:Landroid/graphics/Matrix;

.field private mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

.field private mPlusOneText:Ljava/lang/String;

.field private mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

.field private mRotation:F

.field private mScaleFactor:F

.field private mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

.field private mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

.field private mShouldTriggerViewLoaded:Z

.field private mShowTagShape:Z

.field private mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

.field private mTagName:Ljava/lang/CharSequence;

.field private mTagNameBackground:Landroid/graphics/RectF;

.field private mTagShape:Landroid/graphics/RectF;

.field private mTempDst:Landroid/graphics/RectF;

.field private mTempSrc:Landroid/graphics/RectF;

.field private mTransformNoScaling:Z

.field private mTransformVerticalOnly:Z

.field private mTransformsEnabled:Z

.field private mTranslateRect:Landroid/graphics/RectF;

.field private mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

.field private mValues:[F

.field private mVideoBlob:[B

.field private mVideoReady:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->initialize()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->initialize()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/PhotoHeaderView;FFF)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/PhotoHeaderView;
    .param p1    # F
    .param p2    # F
    .param p3    # F

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->scale(FFF)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/views/PhotoHeaderView;FF)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/PhotoHeaderView;
    .param p1    # F
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->translate(FF)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->snap()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/PhotoHeaderView;FZ)V
    .locals 3
    .param p0    # Lcom/google/android/apps/plus/views/PhotoHeaderView;
    .param p1    # F
    .param p2    # Z

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    return-void
.end method

.method private clearDrawable()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lcom/google/android/apps/plus/views/Recyclable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lcom/google/android/apps/plus/views/Recyclable;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/Recyclable;->onRecycle()V

    :cond_1
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method private configureBounds(Z)V
    .locals 15
    .param p1    # Z

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    if-nez p1, :cond_2

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_4

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    if-eqz v6, :cond_4

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    sub-int/2addr v9, v10

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    const/4 v11, 0x0

    const/4 v12, 0x0

    int-to-float v13, v6

    int-to-float v14, v7

    invoke-virtual {v10, v11, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_8

    int-to-float v7, v7

    int-to-float v6, v6

    div-float v6, v7, v6

    iput v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    int-to-float v6, v9

    int-to-float v7, v8

    div-float/2addr v6, v7

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    const/4 v10, 0x3

    if-ne v7, v10, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    if-eqz v7, :cond_6

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    cmpl-float v6, v7, v6

    if-lez v6, :cond_5

    int-to-float v6, v8

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v11, v11

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v12

    int-to-float v6, v6

    invoke-virtual {v7, v9, v10, v11, v6}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    sget-object v10, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v6, v7, v9, v10}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v7, 0x5

    aget v6, v6, v7

    iput v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mInitialTranslationY:F

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoOffset:Ljava/lang/Integer;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoOffset:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getCoverPhotoTopOffset(Landroid/graphics/Matrix;)F

    move-result v7

    sub-float/2addr v6, v7

    int-to-float v7, v8

    const/high16 v8, 0x446b0000

    div-float/2addr v7, v8

    mul-float/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    const/4 v8, 0x0

    invoke-virtual {v7, v8, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getInternalScale()F

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    const/high16 v7, 0x40000000

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    const/high16 v8, 0x41000000

    mul-float/2addr v7, v8

    const/high16 v8, 0x41000000

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMaxScale:F

    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iput-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz p1, :cond_0

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    sub-float v0, v6, v7

    const/high16 v6, 0x3f800000

    div-float/2addr v6, v0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    mul-float v3, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v3, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    int-to-float v6, v2

    mul-float/2addr v6, v3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    mul-float v4, v6, v7

    int-to-float v6, v1

    mul-float/2addr v6, v3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    mul-float v5, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    neg-float v7, v4

    neg-float v8, v5

    invoke-virtual {v6, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_0

    :cond_5
    int-to-float v6, v9

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    div-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v10, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v11

    int-to-float v6, v6

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    int-to-float v11, v11

    invoke-virtual {v7, v9, v10, v6, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    :cond_6
    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    cmpl-float v6, v7, v6

    if-lez v6, :cond_7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    int-to-float v7, v8

    iget v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    mul-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    sub-int v11, v6, v7

    int-to-float v11, v11

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    int-to-float v12, v12

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-virtual {v9, v10, v11, v12, v6}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    :cond_7
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    int-to-float v7, v9

    iget v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    div-float/2addr v7, v9

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    iget-object v9, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    sub-int v10, v6, v7

    int-to-float v10, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    add-int/2addr v6, v7

    int-to-float v6, v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    invoke-virtual {v9, v10, v11, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1

    :cond_8
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempDst:Landroid/graphics/RectF;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v11

    int-to-float v11, v11

    invoke-virtual {v6, v7, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1
.end method

.method private getCoverPhotoTopOffset(Landroid/graphics/Matrix;)F
    .locals 10
    .param p1    # Landroid/graphics/Matrix;

    const/high16 v9, 0x40000000

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_0

    const/high16 v7, -0x40800000

    :goto_0
    return v7

    :cond_0
    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {p1, v7}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x5

    aget v7, v7, v8

    iget v8, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mInitialTranslationY:F

    sub-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x4

    aget v4, v7, v8

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v7, v2

    mul-float/2addr v7, v4

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    sub-int v0, v7, v8

    int-to-float v7, v1

    div-float/2addr v7, v9

    int-to-float v8, v0

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    int-to-float v8, v6

    sub-float v5, v7, v8

    div-float/2addr v5, v4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    const/high16 v7, 0x446b0000

    int-to-float v8, v3

    div-float v4, v7, v8

    mul-float/2addr v5, v4

    neg-float v7, v5

    goto :goto_0
.end method

.method private getInternalScale()F
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private initialize()V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-boolean v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sInitialized:Z

    if-nez v3, :cond_0

    sput-boolean v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sInitialized:Z

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_photodetail_comment:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_photodetail_plus:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->video_overlay:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoImage:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->ic_loading_video:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$drawable;->overlay_lightcycle:I

    invoke-static {v1, v3}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPanoramaImage:Landroid/graphics/Bitmap;

    sget v3, Lcom/google/android/apps/plus/R$color;->photo_background_color:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sBackgroundColor:I

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_info_plusone_count_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_text_size:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_info_comment_count_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_text_size:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_tag_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_stroke_width:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_shadow_radius:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    sget v5, Lcom/google/android/apps/plus/R$color;->photo_tag_shadow_color:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v3, v4, v6, v6, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_crop_profile_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSizeProfile:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_crop_cover_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSizeCoverWidth:I

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_crop_dim_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_crop_highlight_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_crop_stroke_width:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_tag_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v6, v6, v6, v4}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_tag_text_size:I

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_title_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_subtitle_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaBackgroundPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_background_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget v3, Lcom/google/android/apps/plus/R$string;->media_processing_message:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitle:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$string;->media_processing_message_subtitle:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitle:Ljava/lang/String;

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_title_vertical_position:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleVerticalPosition:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_processing_message_subtitle_vertical_position:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleVerticalPosition:I

    sget v3, Lcom/google/android/apps/plus/R$string;->media_not_found_message:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTitle:Ljava/lang/String;

    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_processing_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    sget v4, Lcom/google/android/apps/plus/R$dimen;->media_not_found_message_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v3, v2}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    sput-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/apps/plus/R$color;->photo_tag_text_background_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_overlay_right_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayRightPadding:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_overlay_bottom_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayBottomPadding:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_count_left_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountLeftMargin:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_comment_count_text_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountTextWidth:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_count_left_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountLeftMargin:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_count_text_width:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountTextWidth:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_info_plusone_bottom_margin:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBottomMargin:I

    sget v3, Lcom/google/android/apps/plus/R$dimen;->photo_tag_text_padding:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    sput-boolean v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sHasMultitouchDistinct:Z

    :cond_0
    new-instance v3, Landroid/view/GestureDetector;

    const/4 v4, 0x0

    sget-boolean v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sHasMultitouchDistinct:Z

    if-nez v5, :cond_1

    :goto_0
    invoke-direct {v3, v0, p0, v4, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v2, Landroid/view/ScaleGestureDetector;

    invoke-direct {v2, v0, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    new-instance v2, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;-><init>(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static onStart()V
    .locals 0

    return-void
.end method

.method public static onStop()V
    .locals 0

    return-void
.end method

.method private scale(FFF)V
    .locals 6
    .param p1    # F
    .param p2    # F
    .param p3    # F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    neg-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    invoke-static {p1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMaxScale:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result p1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getInternalScale()F

    move-result v0

    div-float v1, p1, v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->snap()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotation:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVisualScale()F

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;->onImageScaled(F)V

    :cond_0
    return-void
.end method

.method private snap()V
    .locals 15

    const/high16 v14, 0x41a00000

    const/high16 v13, 0x40000000

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v11, v12}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v11, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->left:I

    int-to-float v3, v11

    :goto_0
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v11, :cond_2

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    int-to-float v4, v11

    :goto_1
    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v11, Landroid/graphics/RectF;->left:F

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v11, Landroid/graphics/RectF;->right:F

    sub-float v11, v6, v1

    sub-float v12, v4, v3

    cmpg-float v11, v11, v12

    if-gez v11, :cond_3

    sub-float v11, v4, v3

    add-float v12, v6, v1

    sub-float/2addr v11, v12

    div-float/2addr v11, v13

    add-float v8, v3, v11

    :goto_2
    iget-boolean v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v11, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    :goto_3
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    :goto_4
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_8

    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    div-float/2addr v10, v13

    add-float v9, v5, v10

    :goto_5
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-gtz v10, :cond_0

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v14

    if-lez v10, :cond_b

    :cond_0
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-virtual {v10, v8, v9}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;->start(FF)Z

    :goto_6
    return-void

    :cond_1
    move v3, v10

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v11

    int-to-float v4, v11

    goto :goto_1

    :cond_3
    cmpl-float v11, v1, v3

    if-lez v11, :cond_4

    sub-float v8, v3, v1

    goto :goto_2

    :cond_4
    cmpg-float v11, v6, v4

    if-gez v11, :cond_5

    sub-float v8, v4, v6

    goto :goto_2

    :cond_5
    const/4 v8, 0x0

    goto :goto_2

    :cond_6
    move v5, v10

    goto :goto_3

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4

    :cond_8
    cmpl-float v10, v7, v5

    if-lez v10, :cond_9

    sub-float v9, v5, v7

    goto :goto_5

    :cond_9
    cmpg-float v10, v0, v2

    if-gez v10, :cond_a

    sub-float v9, v2, v0

    goto :goto_5

    :cond_a
    const/4 v9, 0x0

    goto :goto_5

    :cond_b
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_6
.end method

.method private translate(FF)Z
    .locals 12
    .param p1    # F
    .param p2    # F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTempSrc:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v10, v11}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v3, v10

    :goto_0
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v4, v10

    :goto_1
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v1, v10, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v6, v10, Landroid/graphics/RectF;->right:F

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float v10, v3, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    sub-float v11, v4, v11

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    :goto_2
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_4

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    int-to-float v5, v10

    :goto_3
    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v10

    :goto_4
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v10, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_6

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v10, v5, v10

    iget-object v11, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    sub-float v11, v2, v11

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    :goto_5
    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v10, v8, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    cmpl-float v10, v8, p1

    if-nez v10, :cond_8

    cmpl-float v10, v9, p2

    if-nez v10, :cond_8

    const/4 v10, 0x1

    :goto_6
    return v10

    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v10

    int-to-float v4, v10

    goto :goto_1

    :cond_2
    sub-float v10, v6, v1

    sub-float v11, v4, v3

    cmpg-float v10, v10, v11

    if-gez v10, :cond_3

    sub-float v10, v4, v3

    add-float v11, v6, v1

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    add-float v8, v3, v10

    goto :goto_2

    :cond_3
    sub-float v10, v4, v6

    sub-float v11, v3, v1

    invoke-static {v11, p1}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v8

    goto :goto_2

    :cond_4
    const/4 v5, 0x0

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v10

    int-to-float v2, v10

    goto :goto_4

    :cond_6
    sub-float v10, v0, v7

    sub-float v11, v2, v5

    cmpg-float v10, v10, v11

    if-gez v10, :cond_7

    sub-float v10, v2, v5

    add-float v11, v0, v7

    sub-float/2addr v10, v11

    const/high16 v11, 0x40000000

    div-float/2addr v10, v11

    add-float v9, v5, v10

    goto :goto_5

    :cond_7
    sub-float v10, v2, v0

    sub-float v11, v5, v7

    invoke-static {v11, p2}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v9

    goto :goto_5

    :cond_8
    const/4 v10, 0x0

    goto :goto_6
.end method

.method private unbindCurrentResource()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/MediaResource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_0
    return-void
.end method

.method private unbindPendingResource()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/MediaResource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_0
    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 6

    const/16 v4, 0x10

    const/4 v2, 0x3

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/api/MediaRef;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/phone/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoadAnimatedImage:Z

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    if-ne v0, v2, :cond_3

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/16 v2, 0x3ac

    const/4 v3, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IIILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_2
    :goto_0
    return-void

    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v1, v2, v4, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    goto :goto_0
.end method

.method public final bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1    # Landroid/graphics/RectF;
    .param p2    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    return-void
.end method

.method public final destroy()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->stop()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->clearDrawable()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindPendingResource()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindCurrentResource()V

    return-void
.end method

.method public final doAnimate(Z)V
    .locals 2
    .param p1    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAnimate:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAnimate:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Lcom/google/android/apps/plus/util/GifDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Lcom/google/android/apps/plus/util/GifDrawable;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAnimate:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/util/GifDrawable;->setAnimationEnabled(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    :cond_1
    return-void
.end method

.method public final enableImageTransforms(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    :cond_0
    return-void
.end method

.method public final getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCoverPhotoCoordinates(Landroid/graphics/RectF;)V
    .locals 11
    .param p1    # Landroid/graphics/RectF;

    const/high16 v10, 0x3f800000

    const/4 v9, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    invoke-virtual {v7, v8}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x2

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v5

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x5

    aget v7, v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mValues:[F

    const/4 v8, 0x4

    aget v4, v7, v8

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v7, v3

    mul-float v1, v7, v4

    int-to-float v7, v2

    mul-float v0, v7, v4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v7, v5

    int-to-float v7, v7

    div-float/2addr v7, v1

    invoke-static {v9, v7}, Ljava/lang/Math;->max(FF)F

    move-result v7

    iput v7, p1, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v6

    int-to-float v7, v7

    div-float/2addr v7, v0

    invoke-static {v9, v7}, Ljava/lang/Math;->max(FF)F

    move-result v7

    iput v7, p1, Landroid/graphics/RectF;->top:F

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    sub-int/2addr v7, v5

    int-to-float v7, v7

    div-float/2addr v7, v1

    invoke-static {v10, v7}, Ljava/lang/Math;->min(FF)F

    move-result v7

    iput v7, p1, Landroid/graphics/RectF;->right:F

    iget-object v7, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v6

    int-to-float v7, v7

    div-float/2addr v7, v0

    invoke-static {v10, v7}, Ljava/lang/Math;->min(FF)F

    move-result v7

    iput v7, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method public final getCroppedPhoto()Landroid/graphics/Bitmap;
    .locals 15

    const/4 v1, 0x0

    const/high16 v14, 0x446b0000

    iget-boolean v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v12, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    packed-switch v12, :pswitch_data_0

    const/high16 v11, 0x43800000

    move v3, v11

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->right:I

    iget-object v13, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    sub-int v0, v12, v13

    const/high16 v12, 0x43800000

    int-to-float v13, v0

    div-float v8, v12, v13

    move v7, v8

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    neg-int v9, v12

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    neg-int v10, v12

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    :goto_1
    float-to-int v12, v11

    float-to-int v13, v3

    sget-object v14, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v12, v13, v14}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5, v4}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    add-int v12, v9, v10

    if-eqz v12, :cond_2

    int-to-float v12, v9

    int-to-float v13, v10

    invoke-virtual {v5, v12, v13}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_2
    add-float v12, v8, v7

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-eqz v12, :cond_3

    invoke-virtual {v5, v8, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    :cond_3
    iget v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mBackgroundColor:I

    invoke-virtual {v2, v12}, Landroid/graphics/Canvas;->drawColor(I)V

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v12, :cond_0

    invoke-virtual {v2, v5}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :pswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    iget-object v13, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v13}, Lcom/google/android/apps/plus/api/MediaRef;->getLocalUri()Landroid/net/Uri;

    move-result-object v13

    const/16 v14, 0x500

    invoke-static {v12, v13, v14}, Lcom/google/android/apps/plus/util/ImageUtils;->createLocalBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :pswitch_1
    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v12

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v12, v6

    const/high16 v13, 0x44800000

    cmpg-float v12, v12, v13

    if-gez v12, :cond_4

    iget-object v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_0

    :cond_4
    const/high16 v11, 0x446b0000

    iget v12, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalAspectRatio:F

    mul-float v3, v14, v12

    int-to-float v12, v6

    div-float v7, v14, v12

    move v8, v7

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v4, 0x0

    goto :goto_1

    :catch_0
    move-exception v12

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getFullBleedTopOffset()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getCoverPhotoTopOffset(Landroid/graphics/Matrix;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public final getVideoData()[B
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    return-object v0
.end method

.method public final getVisualScale()F
    .locals 2

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getInternalScale()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000

    goto :goto_0
.end method

.method public final hideTagShape()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShowTagShape:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    return-void
.end method

.method public final init(Lcom/google/android/apps/plus/api/MediaRef;Z)V
    .locals 2
    .param p1    # Lcom/google/android/apps/plus/api/MediaRef;
    .param p2    # Z

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsPlaceHolder:Z

    sget v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sBackgroundColor:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/api/MediaRef;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-ne v1, p1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindPendingResource()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mBackgroundColor:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindResources()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_0
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final isPanorama()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->PANORAMA:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/MediaResource;->getResourceType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isPhotoBound()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVideo()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isVideoReady()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoReady:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1

    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindPendingResource()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindCurrentResource()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapDebounce:Z

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getInternalScale()F

    move-result v0

    const/high16 v2, 0x3fc00000

    mul-float v1, v0, v2

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMaxScale:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->start(FFFF)Z

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapDebounce:Z

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mSnapRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$SnapRunnable;->stop()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 27
    .param p1    # Landroid/graphics/Canvas;

    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mBackgroundColor:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsPlaceHolder:Z

    if-eqz v2, :cond_3

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    int-to-float v5, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    int-to-float v6, v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitle:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sget v8, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleVerticalPosition:I

    int-to-float v8, v8

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaTitleTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitle:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    sget v8, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleVerticalPosition:I

    int-to-float v8, v8

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sProcessingMediaSubTitleTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_0
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayBottomPadding:I

    sub-int v26, v2, v7

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v7

    sub-float/2addr v2, v7

    float-to-int v15, v2

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayRightPadding:I

    sub-int/2addr v2, v7

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountTextWidth:I

    sub-int v25, v2, v7

    sub-int v26, v26, v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    move/from16 v0, v25

    int-to-float v7, v0

    move/from16 v0, v26

    int-to-float v8, v0

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->ascent()F

    move-result v9

    sub-float/2addr v8, v9

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentCountLeftMargin:I

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v2, v7

    sub-int v25, v25, v2

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCommentBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v25

    int-to-float v7, v0

    move/from16 v0, v26

    int-to-float v8, v0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBottomMargin:I

    sub-int v26, v26, v2

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v7}, Landroid/text/TextPaint;->descent()F

    move-result v7

    sub-float/2addr v2, v7

    float-to-int v0, v2

    move/from16 v21, v0

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    move/from16 v0, v21

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPhotoOverlayRightPadding:I

    sub-int/2addr v2, v7

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountTextWidth:I

    sub-int v25, v2, v7

    sub-int v26, v26, v20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    move/from16 v0, v25

    int-to-float v7, v0

    move/from16 v0, v26

    int-to-float v8, v0

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->ascent()F

    move-result v9

    sub-float/2addr v8, v9

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    sget v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneCountLeftMargin:I

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    add-int/2addr v2, v7

    sub-int v25, v25, v2

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPlusOneBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v25

    int-to-float v7, v0

    move/from16 v0, v26

    int-to-float v8, v0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_c

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v23

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoReady:Z

    if-eqz v2, :cond_a

    sget-object v24, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoImage:Landroid/graphics/Bitmap;

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v2, v7

    div-int/lit8 v17, v2, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v2, v7

    div-int/lit8 v18, v2, 0x2

    move/from16 v0, v17

    int-to-float v2, v0

    move/from16 v0, v18

    int-to-float v7, v0

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v2, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v7}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShowTagShape:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float v3, v2, v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    mul-float v2, v2, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float v4, v2, v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    mul-float v2, v2, v19

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float v5, v2, v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagShape:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    mul-float v2, v2, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    add-float v6, v2, v7

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    const/high16 v2, 0x40000000

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    int-to-float v7, v7

    mul-float/2addr v2, v7

    sub-float v7, v5, v3

    const/high16 v8, 0x40000000

    div-float/2addr v7, v8

    add-float/2addr v7, v3

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    invoke-interface {v11}, Ljava/lang/CharSequence;->length()I

    move-result v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v8

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v9}, Landroid/text/TextPaint;->descent()F

    move-result v9

    sget-object v10, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v10}, Landroid/text/TextPaint;->ascent()F

    move-result v10

    sub-float/2addr v9, v10

    add-float/2addr v8, v2

    add-float/2addr v9, v2

    const/high16 v2, 0x40000000

    div-float v2, v8, v2

    sub-float v2, v7, v2

    const/4 v7, 0x0

    cmpg-float v7, v2, v7

    if-gez v7, :cond_7

    const/4 v2, 0x0

    :cond_7
    add-float v7, v2, v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v10

    int-to-float v10, v10

    cmpl-float v10, v7, v10

    if-lez v10, :cond_e

    sub-float v2, v5, v8

    :goto_3
    add-float v7, v6, v9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v8

    int-to-float v8, v8

    cmpl-float v8, v7, v8

    if-lez v8, :cond_d

    sub-float v6, v4, v9

    :goto_4
    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    int-to-float v7, v7

    add-float v11, v2, v7

    sget v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPadding:I

    int-to-float v7, v7

    add-float/2addr v7, v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    invoke-virtual {v8, v2, v6, v5, v4}, Landroid/graphics/RectF;->set(FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagNameBackground:Landroid/graphics/RectF;

    const/high16 v8, 0x40400000

    const/high16 v9, 0x40400000

    sget-object v10, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextBackgroundPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v9, v10}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTagName:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v10

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float v12, v7, v2

    sget-object v13, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sTagTextPaint:Landroid/text/TextPaint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v13}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v22

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    int-to-float v10, v2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    int-to-float v11, v2

    sget-object v12, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropDimPaint:Landroid/graphics/Paint;

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawMatrix:Landroid/graphics/Matrix;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_a
    sget-object v24, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sVideoNotReadyImage:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->isPanorama()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPanoramaImage:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    sub-int/2addr v2, v7

    div-int/lit8 v17, v2, 0x2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v2

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPanoramaImage:Landroid/graphics/Bitmap;

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int/2addr v2, v7

    div-int/lit8 v18, v2, 0x2

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sPanoramaImage:Landroid/graphics/Bitmap;

    move/from16 v0, v17

    int-to-float v7, v0

    move/from16 v0, v18

    int-to-float v8, v0

    const/4 v9, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    :cond_c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageIsInvalid:Z

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTitle:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sget-object v9, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sMediaNotFoundTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7, v8, v9}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_d
    move v4, v7

    goto/16 :goto_4

    :cond_e
    move v5, v7

    goto/16 :goto_3
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformVerticalOnly:Z

    if-eqz v0, :cond_0

    const/4 p3, 0x0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFlingDebounce:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->start(FF)Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFlingDebounce:Z

    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    const/4 v10, 0x1

    iput-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getWidth()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getHeight()I

    move-result v6

    iget-boolean v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v10, :cond_0

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    packed-switch v10, :pswitch_data_0

    invoke-static {v8, v6}, Ljava/lang/Math;->min(II)I

    move-result v7

    sget v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSizeProfile:I

    const/high16 v0, 0x3f800000

    :goto_0
    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v10

    iput v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeWidth:I

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeWidth:I

    int-to-float v10, v10

    div-float/2addr v10, v0

    float-to-int v10, v10

    iput v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeHeight:I

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeWidth:I

    sub-int v10, v8, v10

    div-int/lit8 v2, v10, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeHeight:I

    sub-int v10, v6, v10

    div-int/lit8 v4, v10, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeWidth:I

    add-int v3, v2, v10

    iget v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropSizeHeight:I

    add-int v1, v4, v10

    iget-object v10, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropRect:Landroid/graphics/Rect;

    invoke-virtual {v10, v2, v4, v3, v1}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    return-void

    :pswitch_0
    int-to-float v10, v8

    const v11, 0x3dcccccd

    mul-float/2addr v10, v11

    float-to-int v9, v10

    sub-int v7, v8, v9

    sget v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSizeCoverWidth:I

    const v0, 0x3fe38e39

    goto :goto_0

    :pswitch_1
    int-to-float v10, v8

    const v11, 0x3dcccccd

    mul-float/2addr v10, v11

    float-to-int v9, v10

    sub-int v7, v8, v9

    sget v5, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sCropSizeCoverWidth:I

    const v0, 0x40a71c72

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getStatus()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    if-eqz v1, :cond_0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    invoke-interface {v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;->onImageLoadFinished$46d55081()V

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShouldTriggerViewLoaded:Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->clearDrawable()V

    invoke-virtual {p1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_9

    check-cast v2, Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_7

    if-eqz v2, :cond_7

    move v3, v4

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_a

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    instance-of v3, v3, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    if-eq v2, v3, :cond_4

    :cond_1
    if-eqz v2, :cond_8

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-ne v3, v6, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-eq v3, v6, :cond_8

    :cond_2
    :goto_2
    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMinScale:F

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->clearDrawable()V

    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_3

    if-eqz v2, :cond_3

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v3, v6, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_3
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    :cond_4
    :goto_4
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoadAnimatedImage:Z

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoadAnimatedImage:Z

    if-eqz v3, :cond_5

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoadAnimatedImage:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindPendingResource()V

    sget-object v3, Lcom/google/android/apps/plus/views/PhotoHeaderView;->sImageManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    const/4 v6, 0x3

    const/16 v7, 0x14

    invoke-virtual {v3, v4, v6, v7, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMedia(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPendingResource:Lcom/google/android/apps/plus/service/MediaResource;

    :cond_5
    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLoadAnimatedImage:Z

    :cond_6
    const/4 v1, 0x1

    iput-boolean v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageIsInvalid:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindCurrentResource()V

    check-cast p1, Lcom/google/android/apps/plus/service/MediaResource;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCurrentResource:Lcom/google/android/apps/plus/service/MediaResource;

    goto/16 :goto_0

    :cond_7
    move v3, v5

    goto :goto_1

    :cond_8
    move v4, v5

    goto :goto_2

    :cond_9
    instance-of v3, v2, Lcom/google/android/apps/plus/util/GifImage;

    if-eqz v3, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/util/GifDrawable;

    check-cast v2, Lcom/google/android/apps/plus/util/GifImage;

    invoke-direct {v0, v2}, Lcom/google/android/apps/plus/util/GifDrawable;-><init>(Lcom/google/android/apps/plus/util/GifImage;)V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAnimate:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/util/GifDrawable;->setAnimationEnabled(Z)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    goto :goto_4

    :pswitch_2
    const/4 v1, 0x1

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageIsInvalid:Z

    goto/16 :goto_0

    :cond_a
    move v4, v3

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 7
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformNoScaling:Z

    if-eqz v3, :cond_1

    :cond_0
    :goto_0
    return v6

    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    const/high16 v4, 0x3f800000

    sub-float v2, v3, v4

    cmpg-float v3, v2, v5

    if-gez v3, :cond_2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    cmpl-float v3, v3, v5

    if-gtz v3, :cond_3

    :cond_2
    cmpl-float v3, v2, v5

    if-lez v3, :cond_4

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    cmpg-float v3, v3, v5

    if-gez v3, :cond_4

    :cond_3
    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    :cond_4
    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    add-float/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3d23d70a

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getInternalScale()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v3

    mul-float v1, v0, v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v4

    invoke-direct {p0, v1, v3, v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->scale(FFF)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->stop()V

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleFactor:F

    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2
    .param p1    # Landroid/view/ScaleGestureDetector;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapDebounce:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->resetTransformations()V

    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFlingDebounce:Z

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/view/MotionEvent;
    .param p3    # F
    .param p4    # F

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLastTwoFingerUp:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformVerticalOnly:Z

    if-eqz v2, :cond_0

    const/4 p3, 0x0

    :cond_0
    const-wide/16 v2, 0x190

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformsEnabled:Z

    if-eqz v2, :cond_1

    neg-float v2, p3

    neg-float v3, p4

    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->translate(FF)Z

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mIsDoubleTouch:Z

    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleGetureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    # getter for: Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->mRunning:Z
    invoke-static {v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->access$000(Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->snap()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLastTwoFingerUp:J

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v2, v4, :cond_2

    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mLastTwoFingerUp:J

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final resetTransformations()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mOriginalMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    return-void
.end method

.method public setCommentCount(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_0

    :cond_2
    const/16 v1, 0x63

    if-le p1, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCommentText:Ljava/lang/String;

    goto :goto_1
.end method

.method public setCropMode(I)V
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mHaveLayout:Z

    if-eqz v3, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot set crop after view has been laid out"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    if-eqz v3, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Cannot unset crop mode"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mAllowCrop:Z

    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCropMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformVerticalOnly:Z

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTransformNoScaling:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDoubleTapToZoomEnabled:Z

    :cond_3
    return-void
.end method

.method public setCropModeCoverCoordinates(Landroid/graphics/RectF;)V
    .locals 0
    .param p1    # Landroid/graphics/RectF;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoCoordinates:Landroid/graphics/RectF;

    return-void
.end method

.method public setCropModeFullBleedOffset(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mCoverPhotoOffset:Ljava/lang/Integer;

    return-void
.end method

.method public setFixedHeight(I)V
    .locals 4
    .param p1    # I

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    if-eq p1, v2, :cond_1

    move v0, v1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFixedHeight:I

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->setMeasuredDimension(II)V

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->configureBounds(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFullScreen(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-eq p1, v0, :cond_1

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mFullScreen:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mScaleRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$ScaleRunnable;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mTranslateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$TranslateRunnable;->stop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mRotateRunnable:Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PhotoHeaderView$RotateRunnable;->stop()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    :cond_1
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mExternalClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setOnImageListener(Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mImageListener:Lcom/google/android/apps/plus/views/PhotoHeaderView$OnImageListener;

    return-void
.end method

.method public setPlusOneCount(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_0

    :cond_2
    const/16 v1, 0x63

    if-le p1, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$string;->ninety_nine_plus:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mPlusOneText:Ljava/lang/String;

    goto :goto_1
.end method

.method public setVideoBlob([B)V
    .locals 3
    .param p1    # [B

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoBlob:[B

    if-eqz p1, :cond_1

    invoke-static {}, Lcom/google/api/services/plusi/model/DataVideoJson;->getInstance()Lcom/google/api/services/plusi/model/DataVideoJson;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/api/services/plusi/model/DataVideoJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/api/services/plusi/model/DataVideo;

    const-string v1, "FINAL"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "READY"

    iget-object v2, v0, Lcom/google/api/services/plusi/model/DataVideo;->status:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mVideoReady:Z

    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final showTagShape()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mShowTagShape:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    return-void
.end method

.method public unbindResources()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindPendingResource()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->unbindCurrentResource()V

    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoHeaderView;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
