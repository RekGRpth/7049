.class public Lcom/google/android/apps/plus/views/PhotoTagAvatarView;
.super Landroid/widget/CompoundButton;
.source "PhotoTagAvatarView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;


# static fields
.field private static sAvatarHeight:Ljava/lang/Integer;

.field private static sAvatarWidth:Ljava/lang/Integer;


# instance fields
.field private mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

.field private mDrawRect:Landroid/graphics/Rect;

.field private mSubjectGaiaId:Ljava/lang/String;

.field private mTagHeight:I

.field private mTagLeft:I

.field private mTagTop:I

.field private mTagWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/CompoundButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mDrawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarWidth:Ljava/lang/Integer;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_tag_scroller_avatar_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarWidth:Ljava/lang/Integer;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->photo_tag_scroller_avatar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarHeight:Ljava/lang/Integer;

    :cond_0
    return-void
.end method


# virtual methods
.method public bindResources()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mSubjectGaiaId:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mSubjectGaiaId:Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarByGaiaId(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/CompoundButton;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->bindResources()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/CompoundButton;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->unbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mDrawRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v2, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/CompoundButton;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getWidth()I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingRight()I

    const/4 v4, 0x0

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagWidth:I

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getGravity()I

    move-result v6

    and-int/lit8 v6, v6, 0x70

    sparse-switch v6, :sswitch_data_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingTop()I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagHeight:I

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getGravity()I

    move-result v6

    and-int/lit8 v6, v6, 0x7

    sparse-switch v6, :sswitch_data_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingLeft()I

    move-result v3

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagWidth:I

    :goto_1
    iput v3, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagLeft:I

    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagTop:I

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagLeft:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingLeft()I

    move-result v7

    add-int v0, v6, v7

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagTop:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingTop()I

    move-result v7

    add-int v1, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mDrawRect:Landroid/graphics/Rect;

    sget-object v7, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarWidth:Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/2addr v7, v0

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarHeight:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v6, v0, v1, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    return-void

    :sswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getHeight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagHeight:I

    div-int/lit8 v7, v7, 0x2

    sub-int v5, v6, v7

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagHeight:I

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingBottom()I

    move-result v7

    sub-int v2, v6, v7

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagHeight:I

    sub-int v5, v2, v6

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingRight()I

    move-result v7

    sub-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    iget v7, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagWidth:I

    div-int/lit8 v7, v7, 0x2

    sub-int v3, v6, v7

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagWidth:I

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getWidth()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingRight()I

    move-result v7

    sub-int v4, v6, v7

    iget v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagWidth:I

    sub-int v3, v4, v6

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x50 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x5 -> :sswitch_3
    .end sparse-switch
.end method

.method public onMeasure(II)V
    .locals 10
    .param p1    # I
    .param p2    # I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mSubjectGaiaId:Ljava/lang/String;

    if-eqz v8, :cond_0

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarWidth:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sget-object v8, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->sAvatarHeight:Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingTop()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingBottom()I

    move-result v9

    add-int v7, v8, v9

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingLeft()I

    move-result v8

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getPaddingRight()I

    move-result v9

    add-int v2, v8, v9

    add-int v6, v2, v1

    add-int v5, v7, v0

    :goto_0
    iput v6, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagWidth:I

    iput v5, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mTagHeight:I

    invoke-super {p0, p1, p2}, Landroid/widget/CompoundButton;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getMeasuredWidth()I

    move-result v8

    invoke-static {v8, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getMeasuredHeight()I

    move-result v8

    invoke-static {v8, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-virtual {p0, v4, v3}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setMeasuredDimension(II)V

    return-void

    :cond_0
    const/4 v6, 0x0

    const/4 v5, 0x0

    goto :goto_0
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->invalidate()V

    return-void
.end method

.method public setSubjectGaiaId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mSubjectGaiaId:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->unbindResources()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mSubjectGaiaId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->bindResources()V

    :cond_0
    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->mAvatarResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
