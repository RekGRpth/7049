.class public Lcom/google/android/apps/plus/views/PhotoTagScroller;
.super Landroid/widget/HorizontalScrollView;
.source "PhotoTagScroller.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PhotoTagScroller$PhotoShapeQuery;
    }
.end annotation


# instance fields
.field private mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

.field private mExternalClickListener:Landroid/view/View$OnClickListener;

.field private mHideTags:Z

.field private mMyApprovedShapeId:Ljava/lang/Long;

.field private mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

.field private final mScrollerRect:Landroid/graphics/Rect;

.field private mShapeNeedsApproval:Z

.field private mTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/PhotoTagAvatarView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mHideTags:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mScrollerRect:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public final bind(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Landroid/database/Cursor;Landroid/view/ViewGroup;)V
    .locals 30
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p3    # Landroid/database/Cursor;
    .param p4    # Landroid/view/ViewGroup;

    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    move-object/from16 v26, v0

    if-eqz v26, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    move-object/from16 v26, v0

    sget v27, Lcom/google/android/apps/plus/R$id;->tag_shape_id:I

    invoke-virtual/range {v26 .. v27}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Long;

    if-eqz v24, :cond_2

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    :goto_0
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->clear()V

    const/16 v26, 0x0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mShapeNeedsApproval:Z

    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->removeAllViews()V

    const/16 v26, -0x1

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_0
    :goto_1
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v26

    if-eqz v26, :cond_7

    const/16 v26, 0x4

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    const/16 v26, 0x7

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v26, 0x6

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    const/16 v26, 0x2

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const/16 v26, 0x5

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v26, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v26

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v6

    const-string v26, "PENDING"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v15

    const-string v26, "SUGGESTED"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v23

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/EsAccount;->isMyGaiaId(Ljava/lang/String;)Z

    move-result v13

    const-string v26, "DETECTED"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_0

    const-string v26, "REJECTED"

    move-object/from16 v0, v20

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_0

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v26

    if-nez v26, :cond_0

    invoke-static {}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->getInstance()Lcom/google/api/services/plusi/model/DataRectRelativeJson;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Lcom/google/api/services/plusi/model/DataRectRelativeJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/DataRectRelative;

    if-eqz v13, :cond_5

    if-nez v15, :cond_4

    if-nez v23, :cond_4

    sget v26, Lcom/google/android/apps/plus/R$layout;->photo_tag_view:I

    const/16 v27, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    :goto_2
    move-object/from16 v0, p4

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    sget v26, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setSubjectGaiaId(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    cmp-long v26, v8, v17

    if-nez v26, :cond_1

    move-object v14, v5

    :cond_1
    new-instance v19, Landroid/graphics/RectF;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->left:Ljava/lang/Double;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/lang/Double;->floatValue()F

    move-result v26

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->top:Ljava/lang/Double;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/lang/Double;->floatValue()F

    move-result v27

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->right:Ljava/lang/Double;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Double;->floatValue()F

    move-result v28

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/api/services/plusi/model/DataRectRelative;->bottom:Ljava/lang/Double;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Double;->floatValue()F

    move-result v29

    move-object/from16 v0, v19

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_rect:I

    move/from16 v0, v26

    move-object/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setTag(ILjava/lang/Object;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_name:I

    move/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setTag(ILjava/lang/Object;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_id:I

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v5, v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setTag(ILjava/lang/Object;)V

    if-eqz v13, :cond_0

    if-eqz v15, :cond_6

    sget v26, Lcom/google/android/apps/plus/R$id;->name:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    move-object/from16 v0, v26

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_2
    const-wide/16 v8, 0x0

    goto/16 :goto_0

    :cond_3
    const-wide/16 v8, 0x0

    goto/16 :goto_0

    :cond_4
    sget v26, Lcom/google/android/apps/plus/R$layout;->photo_tag_approval_view:I

    const/16 v27, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_approve:I

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_id:I

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_is_suggestion:I

    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_gaiaid:I

    move/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_deny:I

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_id:I

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v27

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_is_suggestion:I

    invoke-static/range {v23 .. v23}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v27

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_gaiaid:I

    move/from16 v0, v26

    move-object/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v11, 0x0

    const/16 v26, 0x1

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mShapeNeedsApproval:Z

    goto/16 :goto_2

    :cond_5
    sget v26, Lcom/google/android/apps/plus/R$layout;->photo_tag_view:I

    const/16 v27, 0x0

    move/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v12, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v11

    goto/16 :goto_2

    :cond_6
    if-eqz v23, :cond_0

    sget v26, Lcom/google/android/apps/plus/R$id;->name:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    move-object/from16 v0, v26

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v26, Lcom/google/android/apps/plus/R$id;->second:I

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    sget v27, Lcom/google/android/apps/plus/R$string;->photo_view_tag_suggestion_of_you:I

    invoke-virtual/range {v26 .. v27}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v26

    if-lez v26, :cond_9

    if-eqz v14, :cond_8

    move-object v5, v14

    :goto_3
    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_rect:I

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/graphics/RectF;

    sget v26, Lcom/google/android/apps/plus/R$id;->tag_shape_name:I

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v19

    move-object/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V

    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setChecked(Z)V

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->invalidate()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/PhotoTagScroller;->requestLayout()V

    return-void

    :cond_8
    const/16 v26, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    sget v26, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual/range {v25 .. v26}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    invoke-virtual/range {v26 .. v28}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method public final getMyApprovedShapeId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mMyApprovedShapeId:Ljava/lang/Long;

    return-object v0
.end method

.method public final hasTags()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mTags:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7
    .param p1    # Landroid/widget/CompoundButton;
    .param p2    # Z

    const/4 v5, 0x0

    instance-of v4, p1, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-eqz v4, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-eqz p2, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-ne p1, v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-eqz v1, :cond_2

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->setChecked(Z)V

    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-nez v4, :cond_4

    move-object v3, v5

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-nez v4, :cond_5

    move-object v2, v5

    :goto_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->bindTagData(Landroid/graphics/RectF;Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/PhotoHeaderView;->invalidate()V

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    if-ne p1, v4, :cond_2

    iput-object v5, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    sget v6, Lcom/google/android/apps/plus/R$id;->tag_shape_rect:I

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/RectF;

    move-object v3, v4

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mCheckedAvatar:Lcom/google/android/apps/plus/views/PhotoTagAvatarView;

    sget v5, Lcom/google/android/apps/plus/R$id;->tag_shape_name:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/views/PhotoTagAvatarView;->getTag(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    move-object v2, v4

    goto :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method public setExternalOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mExternalClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setHeaderView(Lcom/google/android/apps/plus/views/PhotoHeaderView;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PhotoHeaderView;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PhotoTagScroller;->mPhotoHeader:Lcom/google/android/apps/plus/views/PhotoHeaderView;

    return-void
.end method
