.class public Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;
.super Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.source "PlaceReviewCardGroup.java"


# instance fields
.field private mLocationIconRect:Landroid/graphics/Rect;

.field private mLocationLayoutCorner:Landroid/graphics/Point;

.field private mReview:Lcom/google/api/services/plusi/model/PlaceReview;

.field private mReviewBodyLayout:Landroid/text/StaticLayout;

.field private mReviewLocationLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationIconRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationLayoutCorner:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method protected final createHero(III)I
    .locals 12
    .param p1    # I
    .param p2    # I
    .param p3    # I

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBorderPadding:I

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p2, v0

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->rightBorderPadding:I

    add-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    sub-int/2addr p3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v7, v0, Lcom/google/api/services/plusi/model/PlaceReview;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    add-int/2addr v0, p1

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    add-int/2addr v0, v1

    const/4 v3, 0x0

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewLocationBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationIconRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewLocationIconPadding:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationLayoutCorner:Landroid/graphics/Point;

    const/16 v1, 0xa

    invoke-static {v11, v1}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v9

    const/4 v10, 0x0

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewLocationLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p2, v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v0, v0, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewMaxLines:I

    if-lez v0, :cond_1

    const/16 v0, 0xa

    invoke-static {v11, v0}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/PlaceReview;->reviewBody:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewMaxLines:I

    invoke-static {v0, v1, p3, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewBodyLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p2, v0

    :cond_1
    return p2
.end method

.method protected final drawHero(Landroid/graphics/Canvas;III)I
    .locals 3
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBorderPadding:I

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewLocationLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    neg-int v0, v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationLayoutCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reviewLocationBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mLocationIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v2, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewBodyLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    add-int/2addr v0, p2

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    int-to-float v1, p3

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sub-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    neg-int v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReviewBodyLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p3, v0

    :cond_1
    return p3
.end method

.method protected final hasHero()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v1, 0x20

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {}, Lcom/google/api/services/plusi/model/PlaceReviewJson;->getInstance()Lcom/google/api/services/plusi/model/PlaceReviewJson;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/api/services/plusi/model/PlaceReviewJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/api/services/plusi/model/PlaceReview;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PlaceReviewCardGroup;->mReview:Lcom/google/api/services/plusi/model/PlaceReview;

    return-void
.end method
