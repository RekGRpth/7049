.class public Lcom/google/android/apps/plus/views/PlusOneAnimatorView;
.super Landroid/view/View;
.source "PlusOneAnimatorView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;
    }
.end annotation


# static fields
.field private static final sAccelerateInterpolator:Landroid/view/animation/Interpolator;

.field private static final sDecelerateInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mAnimStage:I

.field private mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mNextButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mOriginalTranslateY:I

.field private mPlusOneAnimListener:Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const v1, 0x3f99999a

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->sAccelerateInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/4 v2, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mNextButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mPlusOneAnimListener:Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;->onPlusOneAnimFinished()V

    :cond_0
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    const/4 v1, 0x0

    const/high16 v3, 0x3f800000

    iget v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mNextButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mNextButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x4b

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x40000000

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->invalidate()V

    iget v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v1, 0x10e

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mOriginalTranslateY:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    iget v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    goto :goto_0

    :pswitch_2
    iput-object v1, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mPlusOneAnimListener:Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;

    invoke-interface {v0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;->onPlusOneAnimFinished()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->invalidate()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2, v2}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public final startPlusOneAnim(Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;Lcom/google/android/apps/plus/views/ClickableButton;Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;
    .param p2    # Lcom/google/android/apps/plus/views/ClickableButton;
    .param p3    # Lcom/google/android/apps/plus/views/ClickableButton;

    const/high16 v5, 0x40000000

    const/4 v3, 0x0

    iput v3, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mAnimStage:I

    iput-object p1, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mPlusOneAnimListener:Lcom/google/android/apps/plus/views/PlusOneAnimatorView$PlusOneAnimListener;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mNextButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    iget v2, v1, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->setX(F)V

    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->setY(F)V

    invoke-virtual {v1, v3, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    invoke-virtual {p3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->getTranslationY()F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mOriginalTranslateY:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mCurrentButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/lit8 v0, v2, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0x10e

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->mOriginalTranslateY:I

    sub-int/2addr v3, v0

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->sAccelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PlusOneAnimatorView;->requestLayout()V

    return-void
.end method
