.class public Lcom/google/android/apps/plus/views/PostAclButtonView;
.super Landroid/widget/FrameLayout;
.source "PostAclButtonView.java"


# instance fields
.field private mButton:Landroid/view/View;

.field private mCheck:Landroid/widget/ImageView;

.field private mCheckId:Ljava/lang/Integer;

.field private mIcon:Landroid/widget/ImageView;

.field private mIconActiveId:Ljava/lang/Integer;

.field private mIconInactiveId:Ljava/lang/Integer;

.field private mLabel:Ljava/lang/String;

.field private mText:Lcom/google/android/apps/plus/views/ConstrainedTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->post_acl_button:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->addView(Landroid/view/View;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mButton:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->acl_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mText:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->acl_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIcon:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/apps/plus/R$id;->acl_check:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mCheck:Landroid/widget/ImageView;

    return-void
.end method

.method private initialize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/Integer;
    .param p4    # Ljava/lang/Integer;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIconActiveId:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIconInactiveId:Ljava/lang/Integer;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mCheckId:Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setLabelText(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setInactive()V

    return-void
.end method

.method private static setImageDrawable(Landroid/widget/ImageView;Ljava/lang/Integer;)V
    .locals 1
    .param p0    # Landroid/widget/ImageView;
    .param p1    # Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public final initialize(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public final initialize(Ljava/lang/String;III)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p2}, Ljava/lang/Integer;-><init>(I)V

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p3}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p4}, Ljava/lang/Integer;-><init>(I)V

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/plus/views/PostAclButtonView;->initialize(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    return-void
.end method

.method public setActive()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIconActiveId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setImageDrawable(Landroid/widget/ImageView;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mCheck:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mCheckId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setImageDrawable(Landroid/widget/ImageView;Ljava/lang/Integer;)V

    return-void
.end method

.method public setInactive()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIcon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mIconInactiveId:Ljava/lang/Integer;

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setImageDrawable(Landroid/widget/ImageView;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mCheck:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/PostAclButtonView;->setImageDrawable(Landroid/widget/ImageView;Ljava/lang/Integer;)V

    return-void
.end method

.method public setLabelText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mLabel:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mText:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mText:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/PostAclButtonView;->mButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
