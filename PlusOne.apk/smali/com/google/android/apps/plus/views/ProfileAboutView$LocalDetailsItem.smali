.class final Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;
.super Lcom/google/android/apps/plus/views/ProfileAboutView$Item;
.source "ProfileAboutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/ProfileAboutView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LocalDetailsItem"
.end annotation


# instance fields
.field final knownForTerms:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final openingHoursFull:Ljava/lang/String;

.field final openingHoursSummary:Ljava/lang/String;

.field final phone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView$Item;-><init>(B)V

    iput-object p2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->phone:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->knownForTerms:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursSummary:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursFull:Ljava/lang/String;

    return-void
.end method
