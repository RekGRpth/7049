.class public Lcom/google/android/apps/plus/views/ProfileAboutView;
.super Lcom/google/android/apps/plus/views/EsScrollView;
.source "ProfileAboutView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/ColumnGridView$PressedHighlightable;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$Item;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;,
        Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;
    }
.end annotation


# static fields
.field private static final sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

.field private static sDeviceLocationBackground:Landroid/graphics/drawable/Drawable;

.field private static sDeviceLocationNotSharedBackground:Landroid/graphics/drawable/Drawable;

.field private static sEducationBackground:Landroid/graphics/drawable/Drawable;

.field private static sEmploymentBackground:Landroid/graphics/drawable/Drawable;

.field private static sIsExpandedMarginBottom:I

.field private static sLocationsBackground:Landroid/graphics/drawable/Drawable;

.field private static sPlusOneStandardTextColor:I

.field private static sPlusOneTextSize:F

.field private static sPlusOnedByMeTextColor:I


# instance fields
.field private mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

.field mEditEnabled:Z

.field private mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

.field private final mInflater:Landroid/view/LayoutInflater;

.field mIsExpanded:Z

.field private mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

.field private mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

.field private mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$color;->card_plus_oned_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_card_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sIsExpandedMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEmploymentBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEducationBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sLocationsBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationNotSharedBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$color;->card_plus_oned_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_card_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sIsExpandedMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEmploymentBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEducationBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sLocationsBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationNotSharedBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/EsScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDefaultPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    if-nez v1, :cond_0

    sget v1, Lcom/google/android/apps/plus/R$color;->card_plus_oned_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$color;->card_not_plus_oned_text:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->card_plus_oned_text_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    sget v1, Lcom/google/android/apps/plus/R$dimen;->profile_card_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sput v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sIsExpandedMarginBottom:I

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEmploymentBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEducationBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sLocationsBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationBackground:Landroid/graphics/drawable/Drawable;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_selectable_item_background:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationNotSharedBackground:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method static synthetic access$200()F
    .locals 1

    sget v0, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneTextSize:F

    return v0
.end method

.method private addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V
    .locals 8
    .param p1    # Lcom/google/api/services/plusi/model/GoogleReviewProto;
    .param p2    # Landroid/view/View;
    .param p3    # I

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget v2, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_local_user_review:I

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    sget v2, Lcom/google/android/apps/plus/R$id;->local_review_item:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/LocalReviewListItemView;

    if-nez v6, :cond_0

    move v3, v4

    :goto_0
    if-nez v3, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setTopBorderVisible(Z)V

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setAuthorAvatarOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Integer;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v3, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/LocalReviewListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    move v3, v5

    goto :goto_1
.end method

.method private bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const v3, 0x1020006

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const v3, 0x1020014

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v3, 0x1020015

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private bindExpandArea()V
    .locals 5

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showExpandButtonText:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v3, :cond_1

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_show_less:I

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v3, :cond_2

    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_up:I

    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v4, v0, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->container:Landroid/view/View;

    invoke-virtual {v3, v4, v4, v4, v2}, Landroid/view/View;->setPadding(IIII)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateDeviceLocationInHeaderVisibility()V

    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->profile_show_more:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/apps/plus/R$drawable;->icn_events_arrow_down:I

    sget v2, Lcom/google/android/apps/plus/views/ProfileAboutView;->sIsExpandedMarginBottom:I

    goto :goto_1
.end method

.method private bindIntroductionView(Landroid/view/View;Ljava/lang/String;)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;

    move-object v4, p1

    check-cast v4, Landroid/widget/TextView;

    new-instance v5, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;

    invoke-direct {v5, p0}, Lcom/google/android/apps/plus/views/ProfileAboutView$IntroductionTagHandler;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;)V

    invoke-static {p2, v5}, Lcom/google/android/apps/plus/views/ClickableStaticLayout;->buildStateSpans(Ljava/lang/String;Landroid/text/Html$TagHandler;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-eqz v1, :cond_1

    const/4 v5, 0x0

    invoke-virtual {v0, v5, v1}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    :cond_1
    add-int/lit8 v1, v2, -0x1

    :goto_1
    if-ltz v1, :cond_2

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v5

    if-eqz v5, :cond_2

    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v2, -0x1

    if-eq v1, v5, :cond_3

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0, v5, v2}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    :cond_3
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v4}, Landroid/widget/TextView;->getMovementMethod()Landroid/text/method/MovementMethod;

    move-result-object v3

    instance-of v5, v3, Landroid/text/method/LinkMovementMethod;

    if-nez v5, :cond_4

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    :cond_4
    return-void
.end method

.method private bindLinkView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    if-eqz p3, :cond_0

    const v1, 0x1020006

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v3, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v2, p3, v3}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    :cond_0
    const v1, 0x1020014

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x1020015

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p4, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1, p2, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static bindMapView(Lcom/google/android/apps/plus/views/ImageResourceView;Ljava/lang/String;)V
    .locals 2
    .param p0    # Lcom/google/android/apps/plus/views/ImageResourceView;
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v1, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setVisibility(I)V

    goto :goto_0
.end method

.method private bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/SectionHeaderView;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p1, p2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setText(I)V

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, p3}, Lcom/google/android/apps/plus/views/SectionHeaderView;->enableEditIcon(Z)V

    return-void
.end method

.method private enableAvatarChangePhotoIcon(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarChoosePhotoIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private enableCoverPhotoChangePhotoIcon(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhotoChoosePhotoIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static enableDivider(Landroid/view/View;Z)V
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # Z

    sget v1, Lcom/google/android/apps/plus/R$id;->divider:I

    invoke-virtual {p0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;
    .locals 7
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    invoke-direct {p0, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v2, p3, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v2, 0x1020014

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x1020015

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-static {p2, p5, p6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    return-object p2

    :cond_1
    invoke-virtual {v1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private getString(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initProfileLayout()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->header:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->details:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    return-void
.end method

.method private static setupContentDescription(Landroid/view/View;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2
    .param p0    # Landroid/view/View;
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # Ljava/lang/CharSequence;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateDeviceLocationInHeaderVisibility()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private updateGenericListSectionDividers(Landroid/view/ViewGroup;)V
    .locals 5
    .param p1    # Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_1
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eq v2, v0, :cond_1

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    move v1, v2

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final addAddress(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_multi_line_with_icon:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_address:I

    :goto_1
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->address_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final addCircleReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .locals 3
    .param p1    # Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->circle_activity:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V

    return-void
.end method

.method public final addEducationLocation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_multi_line:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v8, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSectionLastLocation:Landroid/view/View;

    return-void
.end method

.method public final addEmail(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_two_line_with_icon:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v4, v4, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    sget v1, Lcom/google/android/apps/plus/R$drawable;->profile_email:I

    :goto_1
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->email_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v1, p0, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final addEmploymentLocation(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_multi_line:I

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v8, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSectionLastLocation:Landroid/view/View;

    return-void
.end method

.method public final addLink(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/apps/plus/R$layout;->profile_item_link:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindLinkView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    sget v1, Lcom/google/android/apps/plus/R$id;->link_content:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 p4, 0x0

    goto :goto_0
.end method

.method public final addLocalReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .locals 3
    .param p1    # Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->local_reviews:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V

    return-void
.end method

.method public final addLocation(Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    sget v4, Lcom/google/android/apps/plus/R$layout;->profile_item_location:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v5, v5, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;-><init>(Ljava/lang/String;Z)V

    const v2, 0x1020006

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->current:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x1020014

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->address:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->address:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v2, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    const/4 v2, 0x4

    goto :goto_0
.end method

.method public final addPhoneNumber(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/16 v7, 0x8

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mInflater:Landroid/view/LayoutInflater;

    sget v5, Lcom/google/android/apps/plus/R$layout;->profile_item_phone:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v3, v5, v6, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v3, :cond_1

    sget v3, Lcom/google/android/apps/plus/R$drawable;->profile_phone:I

    :goto_1
    invoke-direct {p0, v2, v3, v5, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindDataView(Landroid/view/View;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->phone_content:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v3, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v3, p0, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->send_text_button:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$id;->vertical_divider:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p3, :cond_2

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    return-void

    :cond_0
    move v3, v4

    goto :goto_0

    :cond_1
    move v3, v4

    goto :goto_1

    :cond_2
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public final addYourReview(Lcom/google/api/services/plusi/model/GoogleReviewProto;)V
    .locals 3
    .param p1    # Lcom/google/api/services/plusi/model/GoogleReviewProto;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->user_activity:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->addReviewToParent(Lcom/google/api/services/plusi/model/GoogleReviewProto;Landroid/view/View;I)V

    return-void
.end method

.method public final clearAddresses()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public final clearAllReviews()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->user_activity:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->circle_activity:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->local_reviews:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    return-void
.end method

.method public final clearDeviceLocationContent()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    return-void
.end method

.method public final clearEducationLocations()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSectionLastLocation:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->no_items:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final clearEmails()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public final clearEmploymentLocations()V
    .locals 4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSectionLastLocation:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->no_items:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final clearLinks()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public final clearLocations()V
    .locals 3

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->no_items:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public final clearPhoneNumbers()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    return-void
.end method

.method public final enableContactSection(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_section_contact:I

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->contactSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableDeviceLocationSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_current_location:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->edit:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v2, 0x44f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setTag(Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableEducationSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_education:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSectionLastLocation:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSectionLastLocation:Landroid/view/View;

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    if-eqz v2, :cond_1

    sget v2, Lcom/google/android/apps/plus/R$id;->edit:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v2, 0x44d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setTag(Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEducationBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableHompageSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->homepage:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->homepage_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_links:I

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    return-void

    :cond_0
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public final enableLinksSection(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_section_links:I

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->linksSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableLocalDetailsSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->local_details:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_1

    move v2, v3

    :goto_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->local_details_header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_local_section_details:I

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :cond_0
    return-void

    :cond_1
    const/16 v2, 0x8

    goto :goto_0
.end method

.method public final enableLocalEditorialReviewsSection(Z)V
    .locals 3
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->zagat:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public final enableLocalReviewsSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->local_reviews:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_local_section_reviews:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableLocalYourActivitySection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->user_activity:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_local_section_your_activity:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableLocalYourCirclesActivitySection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->circle_activity:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_local_section_activity_from_your_circles:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :goto_0
    return-void

    :cond_0
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableLocationsSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_places:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->edit:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v2, 0x44e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setTag(Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/apps/plus/views/ProfileAboutView;->sLocationsBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enablePersonalSection(Z)V
    .locals 4
    .param p1    # Z

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v1, Lcom/google/android/apps/plus/R$string;->profile_section_personal:I

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final enableWorkSection(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    if-eqz p1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_employment:I

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSectionLastLocation:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSectionLastLocation:Landroid/view/View;

    invoke-static {v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    :cond_0
    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    if-eqz v2, :cond_1

    sget v2, Lcom/google/android/apps/plus/R$id;->edit:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/16 v2, 0x44c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setTag(Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/apps/plus/views/ProfileAboutView;->sEmploymentBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/SectionHeaderView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final init(ZZ)V
    .locals 2
    .param p1    # Z
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->initProfileLayout()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationContainer:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationHelp:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mEditEnabled:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v6, Lcom/google/android/apps/plus/R$id;->expand:I

    if-ne v1, v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v8, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    sget v6, Lcom/google/android/apps/plus/R$string;->expand_more_info_content_description:I

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->requestLayout()V

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    if-nez v6, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v9, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    sget v6, Lcom/google/android/apps/plus/R$string;->collapse_more_info_content_description:I

    invoke-direct {p0, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    sget v6, Lcom/google/android/apps/plus/R$id;->avatar_image:I

    if-ne v1, v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onAvatarClicked()V

    goto :goto_1

    :cond_4
    sget v6, Lcom/google/android/apps/plus/R$id;->cover_photo_image:I

    if-ne v1, v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6, v8}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCoverPhotoClicked(I)V

    goto :goto_1

    :cond_5
    sget v6, Lcom/google/android/apps/plus/R$id;->photo_1:I

    if-ne v1, v6, :cond_6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6, v9}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCoverPhotoClicked(I)V

    goto :goto_1

    :cond_6
    sget v6, Lcom/google/android/apps/plus/R$id;->photo_2:I

    if-ne v1, v6, :cond_7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    const/4 v7, 0x2

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCoverPhotoClicked(I)V

    goto :goto_1

    :cond_7
    sget v6, Lcom/google/android/apps/plus/R$id;->photo_3:I

    if-ne v1, v6, :cond_8

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    const/4 v7, 0x3

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCoverPhotoClicked(I)V

    goto :goto_1

    :cond_8
    sget v6, Lcom/google/android/apps/plus/R$id;->photo_4:I

    if-ne v1, v6, :cond_9

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    const/4 v7, 0x4

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCoverPhotoClicked(I)V

    goto :goto_1

    :cond_9
    sget v6, Lcom/google/android/apps/plus/R$id;->photo_5:I

    if-ne v1, v6, :cond_a

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    const/4 v7, 0x5

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCoverPhotoClicked(I)V

    goto :goto_1

    :cond_a
    sget v6, Lcom/google/android/apps/plus/R$id;->location:I

    if-ne v1, v6, :cond_b

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iget-object v7, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$LocationItem;->address:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocationClicked(Ljava/lang/String;)V

    goto :goto_1

    :cond_b
    sget v6, Lcom/google/android/apps/plus/R$id;->email_content:I

    if-ne v1, v6, :cond_c

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEmailClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    sget v6, Lcom/google/android/apps/plus/R$id;->phone_content:I

    if-ne v1, v6, :cond_d

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onPhoneNumberClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_d
    sget v6, Lcom/google/android/apps/plus/R$id;->send_text_button:I

    if-ne v1, v6, :cond_e

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onSendTextClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    sget v6, Lcom/google/android/apps/plus/R$id;->address_content:I

    if-ne v1, v6, :cond_f

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onAddressClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_f
    sget v6, Lcom/google/android/apps/plus/R$id;->link_content:I

    if-ne v1, v6, :cond_10

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLinkClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_10
    sget v6, Lcom/google/android/apps/plus/R$id;->circles_button:I

    if-eq v1, v6, :cond_11

    sget v6, Lcom/google/android/apps/plus/R$id;->add_to_circles_button:I

    if-ne v1, v6, :cond_12

    :cond_11
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onCirclesButtonClicked()V

    goto/16 :goto_1

    :cond_12
    sget v6, Lcom/google/android/apps/plus/R$id;->map_button:I

    if-ne v1, v6, :cond_13

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalMapClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_13
    sget v6, Lcom/google/android/apps/plus/R$id;->directions_button:I

    if-ne v1, v6, :cond_14

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalDirectionsClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_14
    sget v6, Lcom/google/android/apps/plus/R$id;->call_button:I

    if-ne v1, v6, :cond_15

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-interface {v7, v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalCallClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_15
    sget v6, Lcom/google/android/apps/plus/R$id;->zagat_explanation:I

    if-ne v1, v6, :cond_16

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onZagatExplanationClicked()V

    goto/16 :goto_1

    :cond_16
    sget v6, Lcom/google/android/apps/plus/R$id;->local_review_item:I

    if-ne v1, v6, :cond_17

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Integer;

    aget-object v6, v6, v8

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Integer;

    aget-object v6, v6, v9

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6, v5, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onLocalReviewClicked(II)V

    goto/16 :goto_1

    :cond_17
    sget v6, Lcom/google/android/apps/plus/R$id;->author_avatar:I

    if-ne v1, v6, :cond_18

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/AvatarView;->getGaiaId()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onReviewAuthorAvatarClicked(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_18
    sget v6, Lcom/google/android/apps/plus/R$id;->plus_one:I

    if-ne v1, v6, :cond_19

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onPlusOneClicked()V

    goto/16 :goto_1

    :cond_19
    sget v6, Lcom/google/android/apps/plus/R$id;->expand:I

    if-ne v1, v6, :cond_1a

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iget-boolean v7, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    invoke-interface {v6, v7}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onExpandClicked(Z)V

    goto/16 :goto_1

    :cond_1a
    sget v6, Lcom/google/android/apps/plus/R$id;->header:I

    if-ne v1, v6, :cond_1b

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_1

    :pswitch_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEditEmploymentClicked()V

    goto/16 :goto_1

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEditEducationClicked()V

    goto/16 :goto_1

    :pswitch_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEditPlacesLivedClicked()V

    goto/16 :goto_1

    :pswitch_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEditDeviceLocationClicked()V

    goto/16 :goto_1

    :cond_1b
    sget v6, Lcom/google/android/apps/plus/R$id;->content:I

    if-ne v1, v6, :cond_1c

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    goto/16 :goto_1

    :pswitch_4
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onEditDeviceLocationClicked()V

    goto/16 :goto_1

    :cond_1c
    sget v6, Lcom/google/android/apps/plus/R$id;->header_device_location:I

    if-eq v1, v6, :cond_1d

    sget v6, Lcom/google/android/apps/plus/R$id;->device_location_map:I

    if-ne v1, v6, :cond_1e

    :cond_1d
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iget-object v7, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;->lat:Ljava/lang/Double;

    iget-object v8, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;->lng:Ljava/lang/Double;

    invoke-interface {v6, v7, v8}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onDeviceLocationClicked(Ljava/lang/Double;Ljava/lang/Double;)V

    goto/16 :goto_1

    :cond_1e
    sget v6, Lcom/google/android/apps/plus/R$id;->help:I

    if-ne v1, v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    invoke-interface {v6}, Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;->onDeviceLocationHelpClicked()V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x44c
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x44f
        :pswitch_4
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/apps/plus/views/EsScrollView;->onFinishInflate()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->initProfileLayout()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setVerticalFadingEdgeEnabled(Z)V

    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setFadingEdgeLength(I)V

    return-void
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->onRecycle()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->onRecycle()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationContainer:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationHelp:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    return-void
.end method

.method public setAddedByCount(Ljava/lang/Integer;)V
    .locals 8
    .param p1    # Ljava/lang/Integer;

    const/4 v7, 0x0

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$plurals;->profile_added_by:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, p1}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addedByCount:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setAvatarToDefault(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissing(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableAvatarChangePhotoIcon(Z)V

    return-void
.end method

.method public setAvatarUrl(Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->avatarImage:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableAvatarChangePhotoIcon(Z)V

    return-void
.end method

.method public setBirthday(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/16 v7, 0x3e9

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_two_line:I

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_item_birthday:I

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v6, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setCircles(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CirclesButton;->setCircles(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public setCoverPhotoFullBleedOffset(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setTopOffset(I)V

    return-void
.end method

.method public setCoverPhotoLayout(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setLayout(Ljava/lang/String;)V

    return-void
.end method

.method public setCoverPhotoToDefault(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const-string v1, "FULL_BLEED"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setLayout(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setScaleMode(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setResourceMissing(Z)V

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableCoverPhotoChangePhotoIcon(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setCoverPhotoUrl(Ljava/lang/String;ZZ)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    const/4 v4, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->setCoverPhotoToDefault(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    new-instance v1, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v2, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    if-nez p2, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setFocusable(Z)V

    :goto_1
    invoke-direct {p0, p2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableCoverPhotoChangePhotoIcon(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setFocusable(Z)V

    goto :goto_1
.end method

.method public setDeviceLocationInHeader(Lcom/google/api/services/plusi/model/DeviceLocation;)V
    .locals 4
    .param p1    # Lcom/google/api/services/plusi/model/DeviceLocation;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationContainer:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->lat:Ljava/lang/Double;

    iget-object v3, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->lng:Ljava/lang/Double;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationText:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateDeviceLocationInHeaderVisibility()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->deviceLocationContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public setDeviceLocationNotAvailable()V
    .locals 10

    const/4 v2, 0x0

    const/4 v9, 0x0

    sget v0, Lcom/google/android/apps/plus/R$string;->profile_location_not_available:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_multi_line:I

    move-object v0, p0

    move-object v4, v2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v8

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationNotSharedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v0, 0x44f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v7, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setDeviceLocationNotShared()V
    .locals 10

    const/4 v9, 0x0

    sget v0, Lcom/google/android/apps/plus/R$string;->profile_not_sharing_location_title:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v0, Lcom/google/android/apps/plus/R$string;->profile_not_sharing_location_description:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_multi_line:I

    move-object v0, p0

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View;

    move-result-object v8

    invoke-static {v8, v9}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationSection:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-virtual {v7, v9}, Landroid/view/ViewGroup;->setVisibility(I)V

    sget-object v0, Lcom/google/android/apps/plus/views/ProfileAboutView;->sDeviceLocationNotSharedBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v0, 0x44f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v7, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setDeviceLocationUrl(Ljava/lang/String;Lcom/google/api/services/plusi/model/DeviceLocation;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/api/services/plusi/model/DeviceLocation;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationMap:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindMapView(Lcom/google/android/apps/plus/views/ImageResourceView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationMap:Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz p2, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;

    iget-object v3, p2, Lcom/google/api/services/plusi/model/DeviceLocation;->lat:Ljava/lang/Double;

    iget-object v4, p2, Lcom/google/api/services/plusi/model/DeviceLocation;->lng:Ljava/lang/Double;

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$DeviceLocationItem;-><init>(Ljava/lang/Double;Ljava/lang/Double;)V

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationMap:Lcom/google/android/apps/plus/views/ImageResourceView;

    if-eqz p1, :cond_0

    move-object v1, p0

    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->deviceLocationMapContainer:Landroid/view/View;

    if-eqz p1, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public setDisplayPolicies(Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    const/16 v1, 0x8

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->buttons:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showDetailsAlways:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->expandArea:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->requestLayout()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    if-eqz v3, :cond_1

    move-object v1, v2

    goto :goto_0

    :cond_1
    move v0, v1

    move-object v1, v2

    goto :goto_0
.end method

.method public setEducation(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->education:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setEmployer(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/16 v2, 0x8

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->employer:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setGender(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    const/16 v7, 0x3e8

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$layout;->profile_item_two_line:I

    sget v4, Lcom/google/android/apps/plus/R$string;->profile_item_gender:I

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getLabeledStringView(Landroid/view/ViewGroup;Landroid/view/View;IILjava/lang/String;)Landroid/view/View;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iput-object v6, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->personalSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setHomepage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->homepage:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$id;->link_content:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, p2, p3, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindLinkView(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    new-instance v2, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView$ItemOnTouchListener;-><init>(Lcom/google/android/apps/plus/views/ProfileAboutView;B)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setIntroduction(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_introduction:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindIntroductionView(Landroid/view/View;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->introduction:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setIsExpanded(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mIsExpanded:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindExpandArea()V

    return-void
.end method

.method public setLocalActions(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/16 v9, 0x8

    const/4 v8, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->local_actions:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;

    invoke-direct {v0, p2, p1, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->title:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->mapsCid:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://maps.google.com/maps?cid="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v4, "&q="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->title:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->address:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "http://maps.google.com/maps?daddr="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_1

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, ", "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/plus/R$id;->map_button:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/R$id;->directions_button:I

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$id;->vertical_divider_call:I

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    sget v7, Lcom/google/android/apps/plus/R$id;->call_button:I

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->phone:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v6, v8}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalActionsItem;->phone:Ljava/lang/String;

    invoke-virtual {v7, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_2
    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setLocalDetails(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v3, v3, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v4, Lcom/google/android/apps/plus/R$id;->local_details:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/apps/plus/R$id;->local_details_content:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget v3, Lcom/google/android/apps/plus/R$id;->known_for_terms_row:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    sget v3, Lcom/google/android/apps/plus/R$id;->phone_row:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    sget v3, Lcom/google/android/apps/plus/R$id;->open_hours_row:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->knownForTerms:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->knownForTerms:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v3

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    if-nez v4, :cond_0

    const-string v4, " \u00b7 "

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    move v3, v4

    move v4, v3

    goto :goto_0

    :cond_2
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->known_for_terms_value:I

    invoke-virtual {v5, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->phone:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    const/4 v3, 0x0

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->phone_value:I

    invoke-virtual {v6, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->phone:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursFull:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursFull:Ljava/lang/String;

    move-object v4, v3

    :goto_3
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    const/4 v3, 0x0

    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->open_hours_value:I

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    return-void

    :cond_3
    const/16 v3, 0x8

    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    const/16 v3, 0x8

    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_5
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalDetailsItem;->openingHoursSummary:Ljava/lang/String;

    move-object v4, v3

    goto :goto_3

    :cond_6
    const/16 v3, 0x8

    invoke-virtual {v7, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4
.end method

.method public setLocalEditorialReviews(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 12
    .param p1    # Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    sget v2, Lcom/google/android/apps/plus/R$id;->zagat:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;-><init>(Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    const-string v2, "ZAGAT_OFFICIAL"

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->source:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    iget-object v1, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v1, v1, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    sget v1, Lcom/google/android/apps/plus/R$id;->zagat_logo:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    sget v1, Lcom/google/android/apps/plus/R$id;->user_rated_logo:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/4 v1, 0x4

    new-array v9, v1, [Landroid/view/View;

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$id;->rating_item_1:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/plus/R$id;->rating_item_2:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/plus/R$id;->rating_item_3:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/plus/R$id;->rating_item_4:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v9, v1

    sget v1, Lcom/google/android/apps/plus/R$id;->zagat_editorial_text:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/apps/plus/R$id;->review_count_and_price:I

    invoke-virtual {v6, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v7, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/16 v3, 0x8

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const/4 v3, 0x0

    move v5, v3

    :goto_1
    const/4 v3, 0x4

    if-ge v5, v3, :cond_3

    if-ge v5, v8, :cond_1

    aget-object v3, v9, v5

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    aget-object v3, v9, v5

    sget v4, Lcom/google/android/apps/plus/R$id;->rating_label:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->labelDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v3, v9, v5

    sget v4, Lcom/google/android/apps/plus/R$id;->rating_value:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->scores:Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingsProto;->aspectRating:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;

    iget-object v4, v4, Lcom/google/api/services/plusi/model/ZagatAspectRatingProto;->valueDisplay:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    :cond_0
    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    if-ne v5, v8, :cond_2

    if-eqz v7, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    if-eqz v3, :cond_2

    aget-object v3, v9, v5

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    aget-object v3, v9, v5

    sget v4, Lcom/google/android/apps/plus/R$id;->rating_label:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    aget-object v3, v9, v5

    sget v4, Lcom/google/android/apps/plus/R$id;->rating_value:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    aget-object v3, v9, v5

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    :cond_3
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceLabel:Ljava/lang/String;

    if-nez v3, :cond_4

    const/4 v3, 0x1

    :goto_3
    if-eqz v7, :cond_5

    if-eqz v3, :cond_5

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    iget-object v2, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->editorialText:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    sget v3, Lcom/google/android/apps/plus/R$string;->profile_local_from_zagat:I

    invoke-direct {p0, v3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$style;->ProfileLocalEditorialRating_FromZagat:I

    invoke-direct {v4, v5, v7}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/SpannableUtils;->appendWithSpan(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->editorialText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    sget v1, Lcom/google/android/apps/plus/R$id;->zagat_explanation:I

    invoke-virtual {v6, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    :cond_5
    if-nez v7, :cond_a

    iget v4, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    if-gtz v4, :cond_6

    if-eqz v3, :cond_a

    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    if-lez v5, :cond_7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/apps/plus/R$plurals;->profile_local_review_count:I

    iget v8, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget v11, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->reviewCount:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v7, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    if-eqz v3, :cond_9

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_8

    const-string v3, " \u00b7 "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    iget-object v3, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$LocalEditorialReviewItem;->priceValue:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_a
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_b
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method

.method public setLocation(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mPolicy:Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DisplayPolicies;->showInfoIcons:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->text:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->location:Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$InfoRow;->container:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public setLocationUrl(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->map:Lcom/google/android/apps/plus/views/ImageResourceView;

    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindMapView(Lcom/google/android/apps/plus/views/ImageResourceView;Ljava/lang/String;)V

    return-void
.end method

.method public setName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->fullName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->fullName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    if-eqz v0, :cond_3

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v4, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->givenName:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    move v1, v3

    :goto_3
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->familyName:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    :goto_4
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    invoke-virtual {p1, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->givenName:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->familyName:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->givenName:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->familyName:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_3

    :cond_5
    move v3, v2

    goto :goto_4
.end method

.method public setNoEducationLocations()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearEducationLocations()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->educationSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->no_items:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setNoEmploymentLocations()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearEmploymentLocations()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->workSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->no_items:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setNoLocations()V
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->clearLocations()V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locationsSection:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/apps/plus/R$id;->no_items:I

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public setOnClickListener(Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mOnClickListener:Lcom/google/android/apps/plus/views/ProfileAboutView$OnClickListener;

    return-void
.end method

.method public setPlusOneData(Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->plusOneButton:Landroid/widget/Button;

    if-eqz p1, :cond_1

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOnedByMeTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->plusone_by_me_button:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    sget v1, Lcom/google/android/apps/plus/views/ProfileAboutView;->sPlusOneStandardTextColor:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    sget v1, Lcom/google/android/apps/plus/R$drawable;->plusone_button:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method public setScrapbookAlbumUrls(Ljava/lang/Long;[Ljava/lang/String;Z)V
    .locals 6
    .param p1    # Ljava/lang/Long;
    .param p2    # [Ljava/lang/String;
    .param p3    # Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->coverPhoto:Lcom/google/android/apps/plus/views/CoverPhotoImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/CoverPhotoImageView;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookAlbum:Landroid/view/View;

    invoke-virtual {v2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    array-length v2, p2

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v0

    new-instance v3, Lcom/google/android/apps/plus/api/MediaRef;

    aget-object v4, p2, v0

    sget-object v5, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v3, v4, v5}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/plus/views/ImageResourceView;->setMediaRef(Lcom/google/android/apps/plus/api/MediaRef;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->scrapbookPhoto:[Lcom/google/android/apps/plus/views/ImageResourceView;

    aget-object v2, v2, v0

    invoke-virtual {v2, p0}, Lcom/google/android/apps/plus/views/ImageResourceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-direct {p0, p3}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableCoverPhotoChangePhotoIcon(Z)V

    return-void
.end method

.method public setTagLine(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/SectionHeaderView;

    sget v2, Lcom/google/android/apps/plus/R$string;->profile_section_tagline:I

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindSectionHeader(Lcom/google/android/apps/plus/views/SectionHeaderView;IZ)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    sget v3, Lcom/google/android/apps/plus/R$id;->content:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->bindIntroductionView(Landroid/view/View;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->tagLine:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final shouldHighlightOnPress()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final showAddToCircles(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    sget v1, Lcom/google/android/apps/plus/R$string;->follow:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    sget v1, Lcom/google/android/apps/plus/R$string;->add_to_circles:I

    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final showBlocked()V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public final showError(ZLjava/lang/String;)V
    .locals 3
    .param p1    # Z
    .param p2    # Ljava/lang/String;

    const/4 v2, 0x0

    const/16 v1, 0x8

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->container:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->container:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mProfileLayout:Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$ProfileLayout;->error:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final showNone()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public final showProgress()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->circlesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->addToCirclesButton:Lcom/google/android/apps/plus/views/CirclesButton;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/CirclesButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->blockedText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mHeader:Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$HeaderLayout;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public final updateContactSectionDividers()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->addresses:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->phoneNumbers:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v2, v2, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->emails:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    :cond_2
    if-eqz v1, :cond_3

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    :cond_3
    return-void
.end method

.method public final updateLinksSectionDividers()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->links:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateGenericListSectionDividers(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public final updateLocationsSectionDividers()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v0, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->locations:Landroid/view/ViewGroup;

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/ProfileAboutView;->updateGenericListSectionDividers(Landroid/view/ViewGroup;)V

    return-void
.end method

.method public final updatePersonalSectionDividers()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->gender:Landroid/view/View;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/ProfileAboutView;->mDetails:Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;

    iget-object v0, v1, Lcom/google/android/apps/plus/views/ProfileAboutView$DetailsLayout;->birthday:Landroid/view/View;

    :cond_1
    if-eqz v0, :cond_2

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/ProfileAboutView;->enableDivider(Landroid/view/View;Z)V

    :cond_2
    return-void
.end method
