.class public Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;
.super Landroid/widget/RelativeLayout;
.source "RelativeLayoutWithLayoutNotifications.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;
    }
.end annotation


# instance fields
.field private layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    return-void
.end method

.method public onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v1, v2, v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    sub-int v0, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->onMeasure$3b4dfe4b(II)V

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    return-void
.end method

.method public onMeasure$3b4dfe4b(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    return-void
.end method

.method public setLayoutListener(Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications;->layoutListener:Lcom/google/android/apps/plus/views/RelativeLayoutWithLayoutNotifications$LayoutListener;

    return-void
.end method
