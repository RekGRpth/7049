.class public abstract Lcom/google/android/apps/plus/views/ScrollableViewGroup;
.super Landroid/view/ViewGroup;
.source "ScrollableViewGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;
    }
.end annotation


# static fields
.field private static final sInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field private mFlingVelocity:F

.field private mFlingable:Z

.field private mIsBeingDragged:Z

.field private mLastPosition:[F

.field private final mLimits:[I

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOnScrollChangeListener:Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;

.field private mReceivedDown:Z

.field private mScrollDirection:I

.field private mScrollEnabled:Z

.field protected mScroller:Landroid/widget/Scroller;

.field private mTouchSlop:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mVertical:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/views/ScrollableViewGroup$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->setFocusable(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->setFocusable(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    iput v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->setFocusable(Z)V

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->sInterpolator:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p1, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method private clampToScrollLimits(I)I
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget v0, v0, v1

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget p1, v0, v1

    :cond_0
    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget v0, v0, v2

    if-le p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    aget p1, v0, v2

    goto :goto_0
.end method

.method private shouldStartDrag(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->startDrag()V

    move v1, v2

    goto :goto_0

    :cond_3
    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v3, v3, v1

    sub-float/2addr v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-gtz v4, :cond_4

    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_6

    :cond_4
    move v0, v2

    :goto_1
    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-gtz v4, :cond_5

    iget v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mTouchSlop:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_7

    :cond_5
    move v3, v2

    :goto_2
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v4, :cond_9

    if-eqz v3, :cond_8

    if-nez v0, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->startDrag()V

    move v1, v2

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v3, v1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    if-eqz v0, :cond_a

    if-nez v3, :cond_a

    move v0, v2

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private startDrag()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    sget v7, Lcom/google/android/apps/plus/R$id;->list_layout_parent:I

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    sget v8, Lcom/google/android/apps/plus/R$id;->list_layout_parent:I

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v1, v5, :cond_2

    invoke-virtual {p0, p1, v4}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->addView(Landroid/view/View;I)V

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1
.end method

.method public computeScroll()V
    .locals 5

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->invalidate()V

    iget-boolean v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getFinalY()I

    move-result v2

    :goto_1
    if-ne v0, v2, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    iget v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    const/4 v1, 0x1

    :goto_2
    iput v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onScrollFinished(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getFinalX()I

    move-result v2

    goto :goto_1

    :cond_4
    const/4 v1, -0x1

    goto :goto_2
.end method

.method public final getScroll()I
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollY()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollX()I

    move-result v0

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->shouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mOnScrollChangeListener:Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mOnScrollChangeListener:Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;

    invoke-interface {v0, p2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;->onVerticalScrollChanged(I)V

    :cond_0
    return-void
.end method

.method protected onScrollFinished(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    if-nez v0, :cond_4

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->shouldStartDrag(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    if-ne v9, v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->performClick()Z

    move-result v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    packed-switch v9, :pswitch_data_0

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v1, v1, v0

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->updatePosition(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    aget v0, v2, v0

    sub-float v10, v1, v0

    const/high16 v0, -0x40800000

    cmpg-float v0, v10, v0

    if-gez v0, :cond_8

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScroll()I

    move-result v0

    float-to-int v1, v10

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(I)V

    :cond_6
    :goto_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mReceivedDown:Z

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/high16 v0, 0x3f800000

    cmpl-float v0, v10, v0

    if-lez v0, :cond_5

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    goto :goto_3

    :pswitch_1
    const/4 v0, 0x3

    if-ne v9, v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mIsBeingDragged:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    if-eqz v0, :cond_e

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMaximumVelocity:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_6
    iget v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_9

    iget v1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mMinimumVelocity:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_d

    :cond_9
    neg-float v4, v0

    iput v4, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingVelocity:F

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollY()I

    move-result v2

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    const/4 v3, 0x0

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    iget-object v8, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v11, 0x1

    aget v8, v8, v11

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    :goto_7
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->invalidate()V

    :goto_8
    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    goto :goto_4

    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_6

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    float-to-int v3, v4

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    goto :goto_7

    :cond_d
    iget v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onScrollFinished(I)V

    goto :goto_8

    :cond_e
    iget v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollDirection:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->onScrollFinished(I)V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final scrollTo(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(II)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->scrollTo(II)V

    goto :goto_0
.end method

.method public setFlingable(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mFlingable:Z

    return-void
.end method

.method public setOnScrollChangedListener(Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mOnScrollChangeListener:Lcom/google/android/apps/plus/views/ScrollableViewGroup$OnScrollChangedListener;

    return-void
.end method

.method public setScrollEnabled(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScrollEnabled:Z

    return-void
.end method

.method public setScrollLimits(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLimits:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    return-void
.end method

.method public setVertical(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 1
    .param p1    # Landroid/view/View;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method public final smoothScrollTo(I)V
    .locals 12
    .param p1    # I

    const/16 v5, 0x1f4

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->clampToScrollLimits(I)I

    move-result p1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScroll()I

    move-result v0

    sub-int v4, p1, v0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mVertical:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollY()I

    move-result v2

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->invalidate()V

    return-void

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->getScrollX()I

    move-result v7

    move v8, v1

    move v9, v4

    move v10, v1

    move v11, v5

    invoke-virtual/range {v6 .. v11}, Landroid/widget/Scroller;->startScroll(IIIII)V

    goto :goto_0
.end method

.method protected final updatePosition(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->mLastPosition:[F

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    aput v2, v0, v1

    return-void
.end method
