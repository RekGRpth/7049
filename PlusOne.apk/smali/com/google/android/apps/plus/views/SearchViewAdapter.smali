.class public Lcom/google/android/apps/plus/views/SearchViewAdapter;
.super Ljava/lang/Object;
.source "SearchViewAdapter.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;
    }
.end annotation


# instance fields
.field protected final mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mRequestFocus:Z

.field private mSearchView:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mRequestFocus:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz v1, :cond_0

    sget v2, Lcom/google/android/apps/plus/R$id;->search_go_btn:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/views/SearchViewAdapter$1;

    invoke-direct {v2, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter$1;-><init>(Lcom/google/android/apps/plus/views/SearchViewAdapter;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/SearchViewAdapter;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/SearchViewAdapter;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    return-object v0
.end method

.method public static createInstance(Landroid/view/View;)Lcom/google/android/apps/plus/views/SearchViewAdapter;
    .locals 2
    .param p0    # Landroid/view/View;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xc

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;-><init>(Landroid/view/View;)V

    :goto_0
    return-object v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;-><init>(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/plus/views/SearchViewAdapter;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public addOnChangeListener(Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public hideSoftInput()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x6

    if-eq p2, v0, :cond_0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->onQueryTextSubmit(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;

    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;->onQueryTextChanged(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;

    invoke-interface {v1, p1}, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;->onQueryTextSubmitted(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    return v2
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->onQueryTextChange(Ljava/lang/String;)Z

    return-void
.end method

.method public requestFocus(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mRequestFocus:Z

    return-void
.end method

.method public setQueryHint(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHint(I)V

    return-void
.end method

.method public setQueryText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mRequestFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public setVisible(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->setVisible(ZLandroid/view/View;)V

    return-void
.end method

.method protected final setVisible(ZLandroid/view/View;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/view/View;

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eq v1, v0, :cond_0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->showSoftInput()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    :cond_2
    invoke-static {p2}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    invoke-virtual {p2}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    goto :goto_1
.end method

.method protected showSoftInput()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapter;->mSearchView:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$2;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapter$2;-><init>(Lcom/google/android/apps/plus/views/SearchViewAdapter;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
