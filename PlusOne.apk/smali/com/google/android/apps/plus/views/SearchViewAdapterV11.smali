.class public Lcom/google/android/apps/plus/views/SearchViewAdapterV11;
.super Lcom/google/android/apps/plus/views/SearchViewAdapter;
.source "SearchViewAdapterV11.java"

# interfaces
.implements Landroid/widget/SearchView$OnCloseListener;
.implements Landroid/widget/SearchView$OnQueryTextListener;


# instance fields
.field protected final mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapter;-><init>(Landroid/view/View;)V

    check-cast p1, Landroid/widget/SearchView;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setSubmitButtonEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    return-void
.end method


# virtual methods
.method public hideSoftInput()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    return-void
.end method

.method public onClose()Z
    .locals 3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;

    invoke-interface {v1}, Lcom/google/android/apps/plus/views/SearchViewAdapter$OnQueryChangeListener;->onQueryClose()V

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/SearchViewAdapter;->onQueryTextSubmit(Ljava/lang/String;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-static {v0}, Lcom/google/android/apps/plus/util/SoftInput;->hide(Landroid/view/View;)V

    const/4 v0, 0x0

    return v0
.end method

.method public setQueryHint(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setQueryText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mRequestFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    :cond_0
    return-void
.end method

.method public setVisible(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->setVisible(ZLandroid/view/View;)V

    return-void
.end method

.method protected final showSoftInput()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;->mSearchView:Landroid/widget/SearchView;

    new-instance v1, Lcom/google/android/apps/plus/views/SearchViewAdapterV11$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV11$1;-><init>(Lcom/google/android/apps/plus/views/SearchViewAdapterV11;)V

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
