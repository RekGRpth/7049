.class public final Lcom/google/android/apps/plus/views/SearchViewAdapterV12;
.super Lcom/google/android/apps/plus/views/SearchViewAdapterV11;
.source "SearchViewAdapterV12.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/SearchViewAdapterV11;-><init>(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    return-void
.end method


# virtual methods
.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;->mRequestFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SearchViewAdapterV12;->mSearchView:Landroid/widget/SearchView;

    new-instance v1, Lcom/google/android/apps/plus/views/SearchViewAdapterV12$1;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/SearchViewAdapterV12$1;-><init>(Lcom/google/android/apps/plus/views/SearchViewAdapterV12;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/SearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    return-void
.end method
