.class public Lcom/google/android/apps/plus/views/SlidingPanel;
.super Lcom/google/android/apps/plus/views/ScrollableViewGroup;
.source "SlidingPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;
    }
.end annotation


# static fields
.field private static sTabHeaderOverlayLeft:Landroid/graphics/drawable/Drawable;

.field private static sTabHeaderOverlayRight:Landroid/graphics/drawable/Drawable;


# instance fields
.field private mChildIndices:[I

.field private mFirstVisiblePanel:I

.field private mLastVisiblePanel:I

.field private mOnPanelSelectedListener:Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;

.field private mPanelCount:I

.field private mPanelHeight:I

.field private mPanelWidth:I

.field private mSelectedPanel:I

.field private mSelectedTabLineBounds:Landroid/graphics/Rect;

.field private mSelectedTabLineHeight:I

.field private mSelectedTabLinePaint:Landroid/graphics/Paint;

.field private mSelectedTextPaint:Landroid/text/TextPaint;

.field private mStripBackgroundPaint:Landroid/graphics/Paint;

.field private mStripHeight:I

.field private mTabLineBounds:Landroid/graphics/Rect;

.field private mTabLineHeight:I

.field private mTabLinePaint:Landroid/graphics/Paint;

.field private mText:[Ljava/lang/CharSequence;

.field private mTextHeight:I

.field private mTextPaint:Landroid/text/TextPaint;

.field private mTextX:[I

.field private mTextY:I

.field private mTitleBoldWidths:[I

.field private mTitleSpacing:I

.field private mTitleWidths:[I

.field private mTitles:[Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v6, 0x3

    const/4 v5, -0x1

    const/4 v7, 0x0

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-array v4, v6, [Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    new-array v4, v6, [I

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextX:[I

    iput v5, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mFirstVisiblePanel:I

    iput v5, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mLastVisiblePanel:I

    iput v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineBounds:Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLineBounds:Landroid/graphics/Rect;

    new-array v4, v7, [Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    new-array v4, v7, [I

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    new-array v4, v7, [I

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleBoldWidths:[I

    new-array v4, v7, [I

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mChildIndices:[I

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/views/SlidingPanel;->setVertical(Z)V

    invoke-virtual {p0, v7}, Lcom/google/android/apps/plus/views/SlidingPanel;->setFlingable(Z)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/google/android/apps/plus/R$dimen;->stream_tab_strip_height:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    sget v4, Lcom/google/android/apps/plus/R$dimen;->stream_tab_strip_spacing:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripBackgroundPaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripBackgroundPaint:Landroid/graphics/Paint;

    sget v5, Lcom/google/android/apps/plus/R$color;->tab_background_color:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget v4, Lcom/google/android/apps/plus/R$dimen;->stream_tab_line_height:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineHeight:I

    sget v4, Lcom/google/android/apps/plus/R$dimen;->stream_selected_tab_line_height:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLineHeight:I

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLinePaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLinePaint:Landroid/graphics/Paint;

    sget v5, Lcom/google/android/apps/plus/R$color;->stream_tab_line_color:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget v4, Lcom/google/android/apps/plus/R$color;->tab_text_color:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/R$dimen;->stream_tab_text_size:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v4, v2, v3}, Lcom/google/android/apps/plus/views/SlidingPanel;->createTextPaint(Landroid/graphics/Typeface;IF)Landroid/text/TextPaint;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextPaint:Landroid/text/TextPaint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v4, v2, v3}, Lcom/google/android/apps/plus/views/SlidingPanel;->createTextPaint(Landroid/graphics/Typeface;IF)Landroid/text/TextPaint;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTextPaint:Landroid/text/TextPaint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTextPaint:Landroid/text/TextPaint;

    const-string v5, "X"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v7, v6, v0}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextHeight:I

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLinePaint:Landroid/graphics/Paint;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLinePaint:Landroid/graphics/Paint;

    sget v5, Lcom/google/android/apps/plus/R$color;->stream_tab_line_color:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v4, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayLeft:Landroid/graphics/drawable/Drawable;

    if-nez v4, :cond_0

    sget v4, Lcom/google/android/apps/plus/R$drawable;->stream_tab_overlay_left:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayLeft:Landroid/graphics/drawable/Drawable;

    sget v4, Lcom/google/android/apps/plus/R$drawable;->stream_tab_overlay_right:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    sput-object v4, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayRight:Landroid/graphics/drawable/Drawable;

    :cond_0
    return-void
.end method

.method private computePanelHeaderX(II)I
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v9, 0x0

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int v5, v10, p2

    if-ltz p2, :cond_2

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-ge p2, v10, :cond_2

    iget-object v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    aget v8, v10, p2

    :goto_0
    if-gt v5, p1, :cond_3

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    add-int/2addr v10, v5

    if-ge p1, v10, :cond_3

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int/2addr v10, v8

    div-int/lit8 v7, v10, 0x2

    const/4 v0, 0x0

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    add-int/lit8 v10, v10, -0x1

    if-ge p2, v10, :cond_0

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    add-int/lit8 v12, p2, 0x1

    aget v11, v11, v12

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    iget v11, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    sub-int/2addr v10, v11

    sub-int v1, v10, v8

    invoke-static {v9, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_0
    sub-int v9, p1, v5

    invoke-direct {p0, v7, v0, v9}, Lcom/google/android/apps/plus/views/SlidingPanel;->interpolate(III)I

    move-result v9

    :cond_1
    :goto_1
    return v9

    :cond_2
    const/4 v8, 0x0

    goto :goto_0

    :cond_3
    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    add-int/2addr v10, v5

    if-gt v10, p1, :cond_6

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v10, v5

    if-ge p1, v10, :cond_6

    const/4 v7, 0x0

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    add-int/lit8 v10, v10, -0x1

    if-ge p2, v10, :cond_4

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    iget-object v11, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    add-int/lit8 v12, p2, 0x1

    aget v11, v11, v12

    sub-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    iget v11, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    sub-int/2addr v10, v11

    sub-int v2, v10, v8

    invoke-static {v9, v2}, Ljava/lang/Math;->min(II)I

    move-result v7

    :cond_4
    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    neg-int v0, v9

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    add-int/lit8 v9, v9, -0x2

    if-ge p2, v9, :cond_5

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    iget-object v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    add-int/lit8 v11, p2, 0x1

    aget v10, v10, v11

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    sub-int/2addr v9, v10

    sub-int v1, v9, v8

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_5
    sub-int v9, p1, v5

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int/2addr v9, v10

    invoke-direct {p0, v7, v0, v9}, Lcom/google/android/apps/plus/views/SlidingPanel;->interpolate(III)I

    move-result v9

    goto :goto_1

    :cond_6
    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int v10, v5, v10

    if-gt v10, p1, :cond_8

    if-ge p1, v5, :cond_8

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int/2addr v9, v8

    div-int/lit8 v7, v9, 0x2

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int v0, v9, v8

    if-lez p2, :cond_7

    iget-object v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    add-int/lit8 v10, p2, -0x1

    aget v6, v9, v10

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    add-int/2addr v9, v6

    div-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    add-int v3, v9, v10

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    :cond_7
    sub-int v9, v5, p1

    invoke-direct {p0, v7, v0, v9}, Lcom/google/android/apps/plus/views/SlidingPanel;->interpolate(III)I

    move-result v9

    goto :goto_1

    :cond_8
    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int/lit8 v10, v10, 0x2

    sub-int v10, v5, v10

    if-gt v10, p1, :cond_1

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int v10, v5, v10

    if-ge p1, v10, :cond_1

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int v7, v9, v8

    if-lez p2, :cond_9

    iget-object v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    add-int/lit8 v10, p2, -0x1

    aget v6, v9, v10

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    add-int/2addr v9, v6

    div-int/lit8 v9, v9, 0x2

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    add-int v4, v9, v10

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v7

    :cond_9
    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int/lit8 v9, v9, 0x2

    sub-int v0, v9, v8

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int v9, v5, v9

    sub-int/2addr v9, p1

    invoke-direct {p0, v7, v0, v9}, Lcom/google/android/apps/plus/views/SlidingPanel;->interpolate(III)I

    move-result v9

    goto/16 :goto_1
.end method

.method private static createTextPaint(Landroid/graphics/Typeface;IF)Landroid/text/TextPaint;
    .locals 2
    .param p0    # Landroid/graphics/Typeface;
    .param p1    # I
    .param p2    # F

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColor(I)V

    invoke-virtual {v0, p0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    invoke-virtual {v0, p2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    return-object v0
.end method

.method private interpolate(III)I
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I

    int-to-float v1, p3

    iget v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    int-to-float v2, v2

    div-float v0, v1, v2

    int-to-float v1, p1

    sub-int v2, p2, p1

    int-to-float v2, v2

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private update(I)V
    .locals 14
    .param p1    # I

    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    if-nez v7, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    div-int v1, p1, v7

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    rem-int v7, p1, v7

    if-nez v7, :cond_2

    move v7, v8

    :goto_1
    add-int v3, v1, v7

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mFirstVisiblePanel:I

    if-ne v1, v7, :cond_1

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mLastVisiblePanel:I

    if-eq v3, v7, :cond_4

    :cond_1
    iput v1, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mFirstVisiblePanel:I

    iput v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mLastVisiblePanel:I

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildCount()I

    move-result v7

    if-ge v2, v7, :cond_4

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mChildIndices:[I

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mFirstVisiblePanel:I

    aget v7, v7, v10

    if-lt v2, v7, :cond_3

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mChildIndices:[I

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mLastVisiblePanel:I

    aget v7, v7, v10

    if-gt v2, v7, :cond_3

    invoke-static {v0, v8}, Lcom/google/android/apps/plus/views/SlidingPanel;->updateVisibility(Landroid/view/View;I)V

    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    move v7, v9

    goto :goto_1

    :cond_3
    const/4 v7, 0x4

    invoke-static {v0, v7}, Lcom/google/android/apps/plus/views/SlidingPanel;->updateVisibility(Landroid/view/View;I)V

    goto :goto_3

    :cond_4
    iget v4, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    if-nez v4, :cond_8

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    aput-object v13, v7, v8

    :goto_4
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    aget-object v10, v10, v4

    aput-object v10, v7, v9

    add-int/lit8 v7, v4, 0x1

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-ge v7, v10, :cond_9

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    add-int/lit8 v11, v4, 0x1

    aget-object v10, v10, v11

    aput-object v10, v7, v12

    :goto_5
    if-lez v4, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextX:[I

    add-int/lit8 v10, v4, -0x1

    invoke-direct {p0, p1, v10}, Lcom/google/android/apps/plus/views/SlidingPanel;->computePanelHeaderX(II)I

    move-result v10

    aput v10, v7, v8

    :cond_5
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextX:[I

    invoke-direct {p0, p1, v4}, Lcom/google/android/apps/plus/views/SlidingPanel;->computePanelHeaderX(II)I

    move-result v8

    aput v8, v7, v9

    add-int/lit8 v7, v4, 0x1

    iget v8, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-ge v7, v8, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextX:[I

    add-int/lit8 v8, v4, 0x1

    invoke-direct {p0, p1, v8}, Lcom/google/android/apps/plus/views/SlidingPanel;->computePanelHeaderX(II)I

    move-result v8

    aput v8, v7, v12

    :cond_6
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleBoldWidths:[I

    aget v7, v7, v4

    iget v8, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    add-int v5, v7, v8

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-ne v7, v9, :cond_7

    iget v5, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    :cond_7
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextX:[I

    aget v7, v7, v9

    iget v8, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleSpacing:I

    div-int/lit8 v8, v8, 0x2

    sub-int v6, v7, v8

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLineBounds:Landroid/graphics/Rect;

    iget v8, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLineHeight:I

    sub-int/2addr v8, v9

    add-int v9, v6, v5

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    invoke-virtual {v7, v6, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    :cond_8
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    iget-object v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    add-int/lit8 v11, v4, -0x1

    aget-object v10, v10, v11

    aput-object v10, v7, v8

    goto :goto_4

    :cond_9
    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    aput-object v13, v7, v12

    goto :goto_5
.end method

.method private static updateVisibility(Landroid/view/View;I)V
    .locals 1
    .param p0    # Landroid/view/View;
    .param p1    # I

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getScrollX()I

    move-result v8

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    int-to-float v0, v8

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const/4 v7, 0x0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-ge v7, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mText:[Ljava/lang/CharSequence;

    aget-object v1, v0, v7

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    if-ne v7, v0, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTextPaint:Landroid/text/TextPaint;

    :goto_2
    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextX:[I

    aget v0, v0, v7

    int-to-float v4, v0

    iget v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextY:I

    int-to-float v5, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextPaint:Landroid/text/TextPaint;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineBounds:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLineBounds:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTabLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayLeft:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sget-object v0, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayRight:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-super {p0, p1}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildCount()I

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getScrollX()I

    move-result v4

    sub-int v6, p4, p2

    iput v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sub-int v6, p5, p3

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelHeight:I

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    div-int/lit8 v6, v6, 0x2

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    iput v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextY:I

    iget p3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelHeight:I

    add-int p5, p3, v6

    const/4 v2, 0x0

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    array-length v6, v6

    if-ge v2, v6, :cond_4

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mChildIndices:[I

    aget v6, v6, v2

    if-ltz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mChildIndices:[I

    aget v6, v6, v2

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int p2, v6, v2

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    add-int/2addr v6, p2

    move/from16 v0, p5

    invoke-virtual {v1, p2, p3, v6, v0}, Landroid/view/View;->layout(IIII)V

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    aget-object v5, v6, v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    array-length v6, v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTextPaint:Landroid/text/TextPaint;

    int-to-float v8, v3

    sget-object v9, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v5, v7, v8, v9}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v6, v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTextPaint:Landroid/text/TextPaint;

    const/4 v8, 0x0

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v9

    invoke-virtual {v7, v5, v8, v9}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v7

    float-to-int v7, v7

    aput v7, v6, v2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleBoldWidths:[I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedTextPaint:Landroid/text/TextPaint;

    const/4 v8, 0x0

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v9

    invoke-virtual {v7, v5, v8, v9}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v7

    float-to-int v7, v7

    aput v7, v6, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    int-to-float v6, v6

    const/high16 v7, 0x3f000000

    mul-float/2addr v6, v7

    float-to-int v3, v6

    goto :goto_2

    :cond_4
    const/4 v6, 0x0

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    iget v8, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/apps/plus/views/SlidingPanel;->setScrollLimits(II)V

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int/2addr v6, v7

    if-eq v4, v6, :cond_5

    iget v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int/2addr v6, v7

    invoke-virtual {p0, v6}, Lcom/google/android/apps/plus/views/SlidingPanel;->scrollTo(I)V

    :goto_3
    sget-object v6, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayLeft:Landroid/graphics/drawable/Drawable;

    const/4 v7, 0x0

    const/4 v8, 0x0

    sget-object v9, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayLeft:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v9}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    iget v11, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineHeight:I

    sub-int/2addr v10, v11

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v6, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayRight:Landroid/graphics/drawable/Drawable;

    iget v7, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    sget-object v8, Lcom/google/android/apps/plus/views/SlidingPanel;->sTabHeaderOverlayRight:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    sub-int/2addr v7, v8

    const/4 v8, 0x0

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    iget v11, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineHeight:I

    sub-int/2addr v10, v11

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineBounds:Landroid/graphics/Rect;

    const/4 v7, 0x0

    iget v8, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTabLineHeight:I

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    iget v10, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_0

    :cond_5
    invoke-direct {p0, v4}, Lcom/google/android/apps/plus/views/SlidingPanel;->update(I)V

    goto :goto_3
.end method

.method public onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/high16 v6, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/views/SlidingPanel;->setMeasuredDimension(II)V

    iget v5, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mStripHeight:I

    sub-int/2addr v1, v5

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/SlidingPanel;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0, v3}, Landroid/view/View;->measure(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/SlidingPanel;->update(I)V

    return-void
.end method

.method protected final onScrollFinished(I)V
    .locals 4
    .param p1    # I

    iget v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->getScrollX()I

    move-result v1

    if-gez p1, :cond_2

    iget v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    div-int v2, v1, v2

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int v0, v2, v3

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SlidingPanel;->smoothScrollTo(I)V

    iget v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    div-int v2, v0, v2

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    if-eq v3, v2, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-ge v2, v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    iput v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mOnPanelSelectedListener:Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mOnPanelSelectedListener:Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    add-int/2addr v2, v1

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    div-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelWidth:I

    mul-int v0, v2, v3

    goto :goto_1
.end method

.method public setIndices([I)V
    .locals 3
    .param p1    # [I

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    array-length v0, v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "setIndices must be called after setTitles!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    array-length v0, v0

    array-length v1, p1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mTitles.length must equal indices.length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mChildIndices:[I

    iput v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mFirstVisiblePanel:I

    iput v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mLastVisiblePanel:I

    return-void
.end method

.method public setOnPanelSelectedListener(Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mOnPanelSelectedListener:Lcom/google/android/apps/plus/views/SlidingPanel$OnPanelSelectedListener;

    return-void
.end method

.method public setSelectedPanel(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    if-ge p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    :cond_0
    return-void
.end method

.method public setTitles([Ljava/lang/String;)V
    .locals 4
    .param p1    # [Ljava/lang/String;

    array-length v0, p1

    new-array v2, v0, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleWidths:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitleBoldWidths:[I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mTitles:[Ljava/lang/CharSequence;

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iput v0, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mPanelCount:I

    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/plus/views/SlidingPanel;->mSelectedPanel:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SlidingPanel;->requestLayout()V

    return-void
.end method
