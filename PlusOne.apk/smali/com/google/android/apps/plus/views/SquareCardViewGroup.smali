.class public Lcom/google/android/apps/plus/views/SquareCardViewGroup;
.super Lcom/google/android/apps/plus/views/ImageCardViewGroup;
.source "SquareCardViewGroup.java"


# instance fields
.field private mCommunityTitleCorner:Landroid/graphics/Point;

.field private mCommunityTitleLayout:Landroid/text/StaticLayout;

.field protected mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

.field private mIconRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/SquareCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/SquareCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mIconRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    return-void
.end method


# virtual methods
.method protected final createCustomLayout(Landroid/content/Context;III)I
    .locals 12
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v0, v0, 0x2

    sub-int p4, p4, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->isInvitation()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v11, Lcom/google/android/apps/plus/R$string;->card_square_invite_invitation:I

    :goto_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    sget-object v2, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->communityIcon:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mIconRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->iconTextPadding:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    const/4 v2, 0x2

    invoke-static {p1, v2}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v9

    const/4 v10, 0x1

    move/from16 v2, p4

    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/util/TextPaintUtils;->layoutBitmapTextLabel(IIIILandroid/graphics/Bitmap;Landroid/graphics/Rect;ILjava/lang/CharSequence;Landroid/graphics/Point;Landroid/text/TextPaint;Z)Landroid/text/StaticLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleLayout:Landroid/text/StaticLayout;

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->communityIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, p3

    return v0

    :cond_0
    sget v11, Lcom/google/android/apps/plus/R$string;->communities_title:I

    goto :goto_0
.end method

.method protected final drawCustomLayout(Landroid/graphics/Canvas;III)I
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p2, v0

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/Rect;->offset(II)V

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->communityIcon:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v0, p2

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    add-int/2addr v1, p3

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    neg-int v1, p3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->communityIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, p3

    return v0
.end method

.method protected final getCenterBitmap()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getDescription()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getTitle()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getSquareName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final hasCustomLayout()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 4
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    const/16 v2, 0x23

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedSquare;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedSquare;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v3, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mSpan:I

    invoke-virtual {p0, p2, v2}, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->getAvailableWidth(Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDesiredWidth:I

    iput v2, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDesiredHeight:I

    return-void
.end method

.method public onRecycle()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/ImageCardViewGroup;->onRecycle()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mDbEmbedSquare:Lcom/google/android/apps/plus/content/DbEmbedSquare;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mIconRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareCardViewGroup;->mCommunityTitleCorner:Landroid/graphics/Point;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/Point;->set(II)V

    return-void
.end method
