.class public interface abstract Lcom/google/android/apps/plus/views/SquareLandingView$OnClickListener;
.super Ljava/lang/Object;
.source "SquareLandingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/SquareLandingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnClickListener"
.end annotation


# virtual methods
.method public abstract onBlockingHelpLinkClicked(Landroid/net/Uri;)V
.end method

.method public abstract onDeclineInvitationClicked()V
.end method

.method public abstract onExpandClicked(Z)V
.end method

.method public abstract onInviteClicked()V
.end method

.method public abstract onJoinButtonClicked(I)V
.end method

.method public abstract onMembersClicked()V
.end method

.method public abstract onSettingsClicked()V
.end method

.method public abstract onShareClicked()V
.end method
