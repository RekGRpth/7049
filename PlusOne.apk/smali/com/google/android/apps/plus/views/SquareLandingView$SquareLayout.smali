.class final Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;
.super Ljava/lang/Object;
.source "SquareLandingView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/SquareLandingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SquareLayout"
.end annotation


# instance fields
.field public blockingExplanation:Landroid/widget/TextView;

.field public buttonSection:Landroid/view/View;

.field public declineInvitationSection:Landroid/view/View;

.field public description:Landroid/widget/TextView;

.field public details:Landroid/view/View;

.field public expandArea:Landroid/widget/ImageView;

.field public header:Landroid/view/View;

.field public inviteButton:Landroid/widget/Button;

.field public joinButton:Landroid/widget/Button;

.field public memberCount:Landroid/widget/TextView;

.field public settingsLabel:Landroid/widget/TextView;

.field public settingsSection:Landroid/view/View;

.field public shareButton:Landroid/widget/Button;

.field public squareName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

.field public squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

.field public squareVisibility:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/google/android/apps/plus/R$id;->header:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->details:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->square_photo:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageResourceView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_community_avatar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceLoadingDrawable(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squarePhoto:Lcom/google/android/apps/plus/views/ImageResourceView;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_community_avatar:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/ImageResourceView;->setResourceMissingDrawable(I)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->square_name:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareName:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->square_visibility:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->squareVisibility:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->member_count:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->memberCount:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->header:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->expand:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->expandArea:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->button_section:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->buttonSection:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->description:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->description:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->join_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->joinButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->invite_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->inviteButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->share_button:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->shareButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->settings_section:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->settingsSection:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->settings_label:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->settingsLabel:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->decline_invitation_section:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->declineInvitationSection:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->details:Landroid/view/View;

    sget v1, Lcom/google/android/apps/plus/R$id;->blocking_explanation:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareLandingView$SquareLayout;->blockingExplanation:Landroid/widget/TextView;

    return-void
.end method
