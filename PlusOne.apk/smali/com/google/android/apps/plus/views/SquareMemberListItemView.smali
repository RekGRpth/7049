.class public Lcom/google/android/apps/plus/views/SquareMemberListItemView;
.super Landroid/widget/RelativeLayout;
.source "SquareMemberListItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;
    }
.end annotation


# instance fields
.field private mActionButtonView:Landroid/view/View;

.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mDivider:Landroid/view/View;

.field private mGaiaId:Ljava/lang/String;

.field private mMainView:Landroid/view/View;

.field private mMembershipStatus:I

.field private mMembershipStatusView:Landroid/widget/TextView;

.field private mName:Ljava/lang/String;

.field private mNameView:Landroid/widget/TextView;

.field private mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public final init(Landroid/database/Cursor;ZLcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;)V
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    const/16 v3, 0x8

    const/4 v2, 0x0

    iput-object p3, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mGaiaId:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mName:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mGaiaId:Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaIdAndAvatarUrl(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mNameView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x4

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatus:I

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatusView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mDivider:Landroid/view/View;

    if-eqz p2, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mActionButtonView:Landroid/view/View;

    if-eqz p2, :cond_2

    :goto_2
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMainView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mActionButtonView:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatusView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$string;->square_owner:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatusView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatusView:Landroid/widget/TextView;

    sget v4, Lcom/google/android/apps/plus/R$string;->square_moderator:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatusView:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/plus/R$id;->main:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mGaiaId:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;->onSquareMemberClick(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$id;->action_button:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mOnItemClickListener:Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mGaiaId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mName:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatus:I

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/SquareMemberListItemView$OnItemClickListener;->onSquareMemberActionClick(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMainView:Landroid/view/View;

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/plus/R$id;->main:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMainView:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->name:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mNameView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->membership_status:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mMembershipStatusView:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->divider:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mDivider:Landroid/view/View;

    sget v0, Lcom/google/android/apps/plus/R$id;->action_button:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mActionButtonView:Landroid/view/View;

    :cond_0
    return-void
.end method

.method public onRecycle()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/SquareMemberListItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
