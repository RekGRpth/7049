.class final Lcom/google/android/apps/plus/views/StreamCommentView$1;
.super Ljava/lang/Object;
.source "StreamCommentView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamCommentView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamCommentView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamCommentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamCommentView;->invalidate()V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget v9, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    add-int/lit8 v9, v9, 0x1

    iput v9, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    const/4 v8, 0x2

    if-le v9, v8, :cond_0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iput v7, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    :cond_0
    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget v8, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    packed-switch v8, :pswitch_data_0

    const/high16 v5, 0x3f800000

    const/high16 v4, 0x3f800000

    const/4 v3, 0x0

    :goto_0
    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamCommentView;->getAlpha()F

    move-result v0

    cmpl-float v8, v0, v5

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v8, v5}, Lcom/google/android/apps/plus/views/StreamCommentView;->setAlpha(F)V

    :cond_1
    cmpl-float v8, v5, v4

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamCommentView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    int-to-long v9, v3

    invoke-virtual {v8, v9, v10}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    # getter for: Lcom/google/android/apps/plus/views/StreamCommentView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;
    invoke-static {}, Lcom/google/android/apps/plus/views/StreamCommentView;->access$100()Landroid/view/animation/Interpolator;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseWithLayer()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_2
    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_3
    if-eqz v3, :cond_4

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v8

    int-to-long v9, v3

    invoke-virtual {v8, p0, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_4
    return-void

    :pswitch_0
    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget v9, v9, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentIndex:I

    add-int/lit8 v9, v9, 0x1

    iget-object v10, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v10, v10, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v10

    rem-int/2addr v9, v10

    iput v9, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentIndex:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    # invokes: Lcom/google/android/apps/plus/views/StreamCommentView;->createCommentText()V
    invoke-static {v8}, Lcom/google/android/apps/plus/views/StreamCommentView;->access$000(Lcom/google/android/apps/plus/views/StreamCommentView;)V

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v8, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mOnCommentDisplayedListener:Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v8, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mOnCommentDisplayedListener:Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;

    iget-object v9, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget v9, v9, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentIndex:I

    iget-object v10, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v10, v10, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v10}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v10

    invoke-interface {v8, v9, v10}, Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;->onCommentDisplayed(II)V

    :cond_5
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000

    const/16 v3, 0x190

    goto :goto_0

    :pswitch_1
    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v8, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentLayout:Landroid/text/StaticLayout;

    if-eqz v8, :cond_7

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamCommentView$1;->this$0:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v8, v8, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v8}, Landroid/text/StaticLayout;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    :goto_1
    mul-int/lit8 v2, v7, 0x46

    const/high16 v5, 0x3f800000

    const/high16 v4, 0x3f800000

    const/16 v8, 0xbb8

    const/16 v9, 0x1f40

    invoke-static {v9, v2}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto/16 :goto_0

    :cond_6
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v7

    goto :goto_1

    :cond_7
    const/4 v7, 0x0

    goto :goto_1

    :pswitch_2
    const/high16 v5, 0x3f800000

    const/4 v4, 0x0

    const/16 v3, 0x190

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
