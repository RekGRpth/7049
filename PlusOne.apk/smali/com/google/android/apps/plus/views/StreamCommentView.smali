.class public Lcom/google/android/apps/plus/views/StreamCommentView;
.super Landroid/view/View;
.source "StreamCommentView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;
    }
.end annotation


# static fields
.field private static sDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private static sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;


# instance fields
.field protected mAnimState:I

.field protected mAnimStateChangeRunnable:Ljava/lang/Runnable;

.field protected mCommentIndex:I

.field protected mCommentLayout:Landroid/text/StaticLayout;

.field protected mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

.field protected mOnCommentDisplayedListener:Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/StreamCommentView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    new-instance v0, Lcom/google/android/apps/plus/views/StreamCommentView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/StreamCommentView$1;-><init>(Lcom/google/android/apps/plus/views/StreamCommentView;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimStateChangeRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/views/StreamCommentView;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->createCommentText()V

    return-void
.end method

.method static synthetic access$100()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/StreamCommentView;->sDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method private createCommentText()V
    .locals 8

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    iget v5, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentIndex:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/content/DbStreamComments;->getAuthorNames(I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string v4, " "

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    iget v5, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentIndex:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/plus/content/DbStreamComments;->getContent(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_2
    new-instance v4, Landroid/text/style/StyleSpan;

    const/4 v5, 0x1

    invoke-direct {v4, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getMeasuredWidth()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/StreamCommentView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->maxCommentLines:I

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-static {v3, v0, v4, v5, v6}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;)Landroid/text/StaticLayout;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentLayout:Landroid/text/StaticLayout;

    goto :goto_0
.end method

.method private startAnimation()V
    .locals 3

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimStateChangeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private stopAnimation()V
    .locals 2

    invoke-static {}, Lcom/google/android/apps/plus/util/ThreadUtil;->getUiThreadHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimStateChangeRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final init(Lcom/google/android/apps/plus/content/DbStreamComments;ILcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;)I
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/content/DbStreamComments;
    .param p2    # I
    .param p3    # Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->stopAnimation()V

    sget-object v2, Lcom/google/android/apps/plus/views/StreamCommentView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/views/StreamCommentView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mOnCommentDisplayedListener:Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v2

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v0, v2

    mul-int/lit8 v2, v0, 0x2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamCommentView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    add-int/2addr p2, v2

    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->startAnimation()V

    return p2
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->startAnimation()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->stopAnimation()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamCommentView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int v0, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v1, v2, 0x2

    int-to-float v2, v1

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v2, v1

    int-to-float v2, v2

    invoke-virtual {p1, v4, v2}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v4

    if-lez v4, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v4

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v5

    sub-float/2addr v4, v5

    float-to-int v1, v4

    mul-int/lit8 v4, v1, 0x2

    sget-object v5, Lcom/google/android/apps/plus/views/StreamCommentView;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v0, v4, v5

    :cond_0
    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/plus/views/StreamCommentView;->setMeasuredDimension(II)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->createCommentText()V

    return-void
.end method

.method public onRecycle()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->stopAnimation()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->clearAnimation()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mComments:Lcom/google/android/apps/plus/content/DbStreamComments;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentLayout:Landroid/text/StaticLayout;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mCommentIndex:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamCommentView;->mAnimState:I

    return-void
.end method
