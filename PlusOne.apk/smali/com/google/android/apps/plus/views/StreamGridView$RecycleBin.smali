.class final Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;
.super Ljava/lang/Object;
.source "StreamGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecycleBin"
.end annotation


# instance fields
.field private mMaxScrap:I

.field private mRecyclerListener:Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

.field private mScrapViews:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mViewTypeCount:I

.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamGridView;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/plus/views/StreamGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/plus/views/StreamGridView;B)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;-><init>(Lcom/google/android/apps/plus/views/StreamGridView;)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;)Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

    return-object p1
.end method


# virtual methods
.method public final addScrap(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v0

    iget v3, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mMaxScrap:I

    if-le v0, v3, :cond_0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mMaxScrap:I

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    iget v4, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->viewType:I

    aget-object v2, v3, v4

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mMaxScrap:I

    if-ge v3, v4, :cond_1

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

    invoke-interface {v3, p1}, Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method public final clear()V
    .locals 3

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mViewTypeCount:I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final getScrapView(I)Landroid/view/View;
    .locals 4
    .param p1    # I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v3, p1

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final setViewTypeCount(I)V
    .locals 5
    .param p1    # I

    if-gtz p1, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Must have at least one view type ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " types reported)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget v2, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mViewTypeCount:I

    if-ne p1, v2, :cond_1

    :goto_0
    return-void

    :cond_1
    new-array v1, p1, [Ljava/util/ArrayList;

    const/4 v0, 0x0

    :goto_1
    if-ge v0, p1, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mViewTypeCount:I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    goto :goto_0
.end method
