.class public final Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;
.super Landroid/database/DataSetObserver;
.source "StreamGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "StreamGridDataSetObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamGridView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/views/StreamGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 0

    return-void
.end method

.method public final onChanged(ZII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamGridView;->access$400(Lcom/google/android/apps/plus/views/StreamGridView;)I

    move-result v1

    # setter for: Lcom/google/android/apps/plus/views/StreamGridView;->mOldItemCount:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->access$302(Lcom/google/android/apps/plus/views/StreamGridView;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v1}, Lcom/google/android/apps/plus/views/StreamGridView;->access$500(Lcom/google/android/apps/plus/views/StreamGridView;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    # setter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I
    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->access$402(Lcom/google/android/apps/plus/views/StreamGridView;I)I

    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$400(Lcom/google/android/apps/plus/views/StreamGridView;)I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->access$602(Lcom/google/android/apps/plus/views/StreamGridView;Z)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v0, p3, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->setFirstPositionAndOffsets(II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->requestLayout()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # |= operator for: Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/StreamGridView;->access$676(Lcom/google/android/apps/plus/views/StreamGridView;I)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$700(Lcom/google/android/apps/plus/views/StreamGridView;)Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mHasStableIds:Z
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$800(Lcom/google/android/apps/plus/views/StreamGridView;)Z

    move-result v0

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    if-gtz p2, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->access$902(Lcom/google/android/apps/plus/views/StreamGridView;I)I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$1000(Lcom/google/android/apps/plus/views/StreamGridView;)[I

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$1000(Lcom/google/android/apps/plus/views/StreamGridView;)[I

    move-result-object v0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$1100(Lcom/google/android/apps/plus/views/StreamGridView;)[I

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$1100(Lcom/google/android/apps/plus/views/StreamGridView;)[I

    move-result-object v0

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    # getter for: Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;
    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->access$1200(Lcom/google/android/apps/plus/views/StreamGridView;)Landroid/support/v4/util/SparseArrayCompat;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->requestLayout()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;->this$0:Lcom/google/android/apps/plus/views/StreamGridView;

    add-int/lit8 v1, p2, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->invalidateLayoutRecordsAfterPosition(I)V

    goto :goto_1
.end method

.method public final onInvalidated()V
    .locals 0

    return-void
.end method
