.class public Lcom/google/android/apps/plus/views/StreamGridView;
.super Landroid/view/ViewGroup;
.source "StreamGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;,
        Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;,
        Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;,
        Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;,
        Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;,
        Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;
    }
.end annotation


# instance fields
.field private mActivePointerId:I

.field private mAdapter:Landroid/widget/ListAdapter;

.field private final mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mColCount:I

.field private mColCountSetting:I

.field private mDataChanged:Z

.field private mFirstPosition:I

.field private mFlingVelocity:I

.field private mForcePopulateOnLayout:Z

.field private mHasStableIds:Z

.field private mHoleThresholdPx:I

.field private mInLayout:Z

.field private mItemBottoms:[I

.field private mItemCount:I

.field private mItemMargin:I

.field private mItemTops:[I

.field private mLastTouchY:F

.field private final mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;",
            ">;"
        }
    .end annotation
.end field

.field private mMaximumVelocity:I

.field private mMinColWidth:I

.field private final mObserver:Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

.field private mOldItemCount:I

.field private mOnScrollListener:Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

.field private mPopulating:Z

.field private final mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

.field private final mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

.field private final mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

.field private mTouchMode:I

.field private mTouchRemainderY:F

.field private mTouchSlop:I

.field private final mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/StreamGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x2

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCountSetting:I

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    iput v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mMinColWidth:I

    new-instance v1, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-direct {v1, p0, v3}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;-><init>(Lcom/google/android/apps/plus/views/StreamGridView;B)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    new-instance v1, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    invoke-direct {v1, p0}, Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;-><init>(Lcom/google/android/apps/plus/views/StreamGridView;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mObserver:Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHoleThresholdPx:I

    new-instance v1, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v1}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mMaximumVelocity:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mFlingVelocity:I

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/views/ScrollerCompat$ScrollerCompatImplIcs;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/ScrollerCompat$ScrollerCompatImplIcs;-><init>(Landroid/content/Context;)V

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    new-instance v1, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    new-instance v1, Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-direct {v1, p1}, Landroid/support/v4/widget/EdgeEffectCompat;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setWillNotDraw(Z)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->setClipToPadding(Z)V

    return-void

    :cond_0
    new-instance v1, Lcom/google/android/apps/plus/views/ScrollerCompat;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/ScrollerCompat;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic access$1000(Lcom/google/android/apps/plus/views/StreamGridView;)[I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/apps/plus/views/StreamGridView;)[I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/apps/plus/views/StreamGridView;)Landroid/support/v4/util/SparseArrayCompat;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/views/StreamGridView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOldItemCount:I

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/views/StreamGridView;)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/apps/plus/views/StreamGridView;I)I
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I

    return p1
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/views/StreamGridView;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/apps/plus/views/StreamGridView;Z)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p1    # Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$676(Lcom/google/android/apps/plus/views/StreamGridView;I)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p1    # I

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    or-int/2addr v0, p1

    int-to-byte v0, v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/views/StreamGridView;)Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/views/StreamGridView;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHasStableIds:Z

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/apps/plus/views/StreamGridView;I)I
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/StreamGridView;
    .param p1    # I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    return v0
.end method

.method private clearAllState()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->removeAllViews()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->setSelectionToTop()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    return-void
.end method

.method private fillDown(II)I
    .locals 35
    .param p1    # I
    .param p2    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingLeft()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingRight()I

    move-result v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    move/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v28

    sub-int v28, v28, v20

    sub-int v28, v28, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v29, v0

    add-int/lit8 v29, v29, -0x1

    mul-int v29, v29, v16

    sub-int v28, v28, v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v29, v0

    div-int v9, v28, v29

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v28

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingBottom()I

    move-result v29

    sub-int v11, v28, v29

    add-int v10, v11, p2

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getNextColumnDown()I

    move-result v19

    move/from16 v22, p1

    :goto_0
    if-ltz v19, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    aget v28, v28, v19

    move/from16 v0, v28

    if-ge v0, v10, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I

    move/from16 v28, v0

    move/from16 v0, v22

    move/from16 v1, v28

    if-ge v0, v1, :cond_10

    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    move/from16 v28, v0

    if-eqz v28, :cond_2

    const/16 v28, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v28

    move-object/from16 v2, v18

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v28, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    move/from16 v29, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->min(II)I

    move-result v24

    mul-int v28, v9, v24

    add-int/lit8 v29, v24, -0x1

    mul-int v29, v29, v16

    add-int v26, v28, v29

    const/high16 v28, 0x40000000

    move/from16 v0, v26

    move/from16 v1, v28

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v27

    const/16 v28, 0x1

    move/from16 v0, v24

    move/from16 v1, v28

    if-le v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    if-eqz v28, :cond_1

    move-object/from16 v0, v28

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v29, v0

    move/from16 v0, v29

    move/from16 v1, v24

    if-eq v0, v1, :cond_15

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v29, v0

    move/from16 v0, v24

    move/from16 v1, v29

    if-le v0, v1, :cond_3

    new-instance v29, Ljava/lang/IllegalStateException;

    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "Invalid LayoutRecord! Record had span="

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v28, v0

    move-object/from16 v0, v30

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " but caller requested span="

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v30, " for position="

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v29

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v29

    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/StreamGridView;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    :cond_3
    new-instance v23, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    const/16 v28, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :goto_2
    const/16 v32, -0x1

    const v30, 0x7fffffff

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v34, v0

    const/16 v31, 0x0

    :goto_3
    sub-int v28, v34, v24

    move/from16 v0, v31

    move/from16 v1, v28

    if-gt v0, v1, :cond_5

    const/high16 v29, -0x80000000

    move/from16 v33, v31

    :goto_4
    add-int v28, v31, v24

    move/from16 v0, v33

    move/from16 v1, v28

    if-ge v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    aget v28, v28, v33

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_14

    :goto_5
    add-int/lit8 v29, v33, 0x1

    move/from16 v33, v29

    move/from16 v29, v28

    goto :goto_4

    :cond_4
    move/from16 v0, v29

    move/from16 v1, v30

    if-ge v0, v1, :cond_13

    move/from16 v28, v31

    :goto_6
    add-int/lit8 v31, v31, 0x1

    move/from16 v30, v29

    move/from16 v32, v28

    goto :goto_3

    :cond_5
    move/from16 v0, v32

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    const/16 v28, 0x0

    :goto_7
    move/from16 v0, v28

    move/from16 v1, v24

    if-ge v0, v1, :cond_6

    add-int v29, v32, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v31, v0

    aget v29, v31, v29

    sub-int v29, v30, v29

    move-object/from16 v0, v23

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->setMarginAbove(II)V

    add-int/lit8 v28, v28, 0x1

    goto :goto_7

    :cond_6
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    move/from16 v19, v0

    :goto_8
    const/4 v15, 0x0

    if-nez v23, :cond_b

    new-instance v23, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    const/16 v28, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v22

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    move/from16 v0, v19

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mHasStableIds:Z

    move/from16 v28, v0

    if-eqz v28, :cond_7

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->id:J

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    move-object/from16 v2, v23

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->id:J

    :cond_7
    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    move/from16 v28, v0

    const/16 v29, -0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_d

    const/16 v28, 0x0

    const/16 v29, 0x0

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    :goto_a
    move/from16 v0, v27

    invoke-virtual {v3, v0, v12}, Landroid/view/View;->measure(II)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    if-nez v15, :cond_8

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-eq v5, v0, :cond_9

    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    move/from16 v28, v0

    if-lez v28, :cond_9

    :cond_8
    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->invalidateLayoutRecordsAfterPosition(I)V

    :cond_9
    move-object/from16 v0, v23

    iput v5, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    const/4 v13, 0x0

    :goto_b
    move/from16 v0, v24

    if-ge v13, v0, :cond_e

    add-int v14, v19, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    aget v29, v28, v14

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->getMarginAbove(I)I

    move-result v30

    add-int v29, v29, v30

    aput v29, v28, v14

    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    goto/16 :goto_8

    :cond_b
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v28, v0

    move/from16 v0, v24

    move/from16 v1, v28

    if-eq v0, v1, :cond_c

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v0, v19

    move-object/from16 v1, v23

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    const/4 v15, 0x1

    goto/16 :goto_9

    :cond_c
    move-object/from16 v0, v23

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    move/from16 v19, v0

    goto/16 :goto_9

    :cond_d
    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    move/from16 v28, v0

    const/high16 v29, 0x40000000

    invoke-static/range {v28 .. v29}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    goto/16 :goto_a

    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    aget v25, v28, v19

    add-int v8, v25, v16

    add-int v4, v8, v5

    add-int v28, v9, v16

    mul-int v28, v28, v19

    add-int v6, v20, v28

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v28

    add-int v7, v6, v28

    invoke-virtual {v3, v6, v8, v7, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v13, 0x0

    :goto_c
    move/from16 v0, v24

    if-ge v13, v0, :cond_f

    add-int v14, v19, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->getMarginBelow(I)I

    move-result v29

    add-int v29, v29, v4

    aput v29, v28, v14

    add-int/lit8 v13, v13, 0x1

    goto :goto_c

    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getNextColumnDown()I

    move-result v19

    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_0

    :cond_10
    const/16 v17, 0x0

    const/4 v13, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v13, v0, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    aget v28, v28, v13

    move/from16 v0, v28

    move/from16 v1, v17

    if-le v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v28, v0

    aget v17, v28, v13

    :cond_11
    add-int/lit8 v13, v13, 0x1

    goto :goto_d

    :cond_12
    sub-int v28, v17, v11

    return v28

    :cond_13
    move/from16 v29, v30

    move/from16 v28, v32

    goto/16 :goto_6

    :cond_14
    move/from16 v28, v29

    goto/16 :goto_5

    :cond_15
    move-object/from16 v23, v28

    goto/16 :goto_2
.end method

.method private fillUp(II)I
    .locals 35
    .param p1    # I
    .param p2    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingLeft()I

    move-result v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingRight()I

    move-result v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v29

    sub-int v29, v29, v20

    sub-int v29, v29, v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v30, v0

    add-int/lit8 v30, v30, -0x1

    mul-int v30, v30, v17

    sub-int v29, v29, v30

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v30, v0

    div-int v9, v29, v30

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingTop()I

    move-result v11

    move/from16 v0, p2

    neg-int v10, v0

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getNextColumnUp()I

    move-result v19

    move/from16 v22, p1

    move/from16 v23, v22

    :goto_0
    if-ltz v19, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    aget v29, v29, v19

    move/from16 v0, v29

    if-le v0, v10, :cond_f

    if-ltz v23, :cond_f

    const/16 v29, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v23

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    move/from16 v29, v0

    if-eqz v29, :cond_1

    const/16 v29, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v18

    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    :cond_0
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v29, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    move/from16 v30, v0

    invoke-static/range {v29 .. v30}, Ljava/lang/Math;->min(II)I

    move-result v25

    mul-int v29, v9, v25

    add-int/lit8 v30, v25, -0x1

    mul-int v30, v30, v17

    add-int v27, v29, v30

    const/high16 v29, 0x40000000

    move/from16 v0, v27

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v25

    move/from16 v1, v29

    if-le v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    if-nez v29, :cond_2

    new-instance v24, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    const/16 v29, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :goto_2
    const/16 v33, -0x1

    const/high16 v31, -0x80000000

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v29, v0

    sub-int v32, v29, v25

    :goto_3
    if-ltz v32, :cond_4

    const v30, 0x7fffffff

    move/from16 v34, v32

    :goto_4
    add-int v29, v32, v25

    move/from16 v0, v34

    move/from16 v1, v29

    if-ge v0, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    aget v29, v29, v34

    move/from16 v0, v29

    move/from16 v1, v30

    if-ge v0, v1, :cond_13

    :goto_5
    add-int/lit8 v30, v34, 0x1

    move/from16 v34, v30

    move/from16 v30, v29

    goto :goto_4

    :cond_1
    const/16 v29, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->addView(Landroid/view/View;I)V

    goto/16 :goto_1

    :cond_2
    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v30, v0

    move/from16 v0, v30

    move/from16 v1, v25

    if-eq v0, v1, :cond_14

    new-instance v30, Ljava/lang/IllegalStateException;

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "Invalid LayoutRecord! Record had span="

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v29

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v29, v0

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v31, " but caller requested span="

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v31, " for position="

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v30

    :cond_3
    move/from16 v0, v30

    move/from16 v1, v31

    if-le v0, v1, :cond_12

    move/from16 v29, v32

    :goto_6
    add-int/lit8 v32, v32, -0x1

    move/from16 v31, v30

    move/from16 v33, v29

    goto/16 :goto_3

    :cond_4
    move/from16 v0, v33

    move-object/from16 v1, v24

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    const/16 v29, 0x0

    :goto_7
    move/from16 v0, v29

    move/from16 v1, v25

    if-ge v0, v1, :cond_5

    add-int/lit8 v30, v33, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v32, v0

    aget v30, v32, v30

    sub-int v30, v30, v31

    move-object/from16 v0, v24

    move/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->setMarginBelow(II)V

    add-int/lit8 v29, v29, 0x1

    goto :goto_7

    :cond_5
    move-object/from16 v0, v24

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    move/from16 v19, v0

    :goto_8
    const/16 v16, 0x0

    if-nez v24, :cond_a

    new-instance v24, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    const/16 v29, 0x0

    move-object/from16 v0, v24

    move/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    move/from16 v0, v19

    move-object/from16 v1, v24

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mHasStableIds:Z

    move/from16 v29, v0

    if-eqz v29, :cond_6

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->id:J

    move-wide/from16 v29, v0

    move-wide/from16 v0, v29

    move-object/from16 v2, v24

    iput-wide v0, v2, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->id:J

    :cond_6
    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    move/from16 v29, v0

    const/16 v30, -0x2

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_c

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-static/range {v29 .. v30}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    :goto_a
    move/from16 v0, v28

    invoke-virtual {v3, v0, v12}, Landroid/view/View;->measure(II)V

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    if-nez v16, :cond_7

    move-object/from16 v0, v24

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-eq v5, v0, :cond_8

    move-object/from16 v0, v24

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    move/from16 v29, v0

    if-lez v29, :cond_8

    :cond_7
    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->invalidateLayoutRecordsBeforePosition(I)V

    :cond_8
    move-object/from16 v0, v24

    iput v5, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    const/4 v14, 0x0

    :goto_b
    move/from16 v0, v25

    if-ge v14, v0, :cond_d

    add-int v15, v19, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    aget v30, v29, v15

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->getMarginBelow(I)I

    move-result v31

    add-int v30, v30, v31

    aput v30, v29, v15

    add-int/lit8 v14, v14, 0x1

    goto :goto_b

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    goto/16 :goto_8

    :cond_a
    move-object/from16 v0, v24

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v29, v0

    move/from16 v0, v25

    move/from16 v1, v29

    if-eq v0, v1, :cond_b

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    move/from16 v0, v19

    move-object/from16 v1, v24

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    const/16 v16, 0x1

    goto/16 :goto_9

    :cond_b
    move-object/from16 v0, v24

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    move/from16 v19, v0

    goto/16 :goto_9

    :cond_c
    move-object/from16 v0, v18

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    move/from16 v29, v0

    const/high16 v30, 0x40000000

    invoke-static/range {v29 .. v30}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    goto/16 :goto_a

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    aget v26, v29, v19

    move/from16 v4, v26

    sub-int v8, v26, v5

    add-int v29, v9, v17

    mul-int v29, v29, v19

    add-int v6, v20, v29

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v29

    add-int v7, v6, v29

    invoke-virtual {v3, v6, v8, v7, v4}, Landroid/view/View;->layout(IIII)V

    const/4 v14, 0x0

    :goto_c
    move/from16 v0, v25

    if-ge v14, v0, :cond_e

    add-int v15, v19, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->getMarginAbove(I)I

    move-result v30

    sub-int v30, v8, v30

    sub-int v30, v30, v17

    aput v30, v29, v15

    add-int/lit8 v14, v14, 0x1

    goto :goto_c

    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getNextColumnUp()I

    move-result v19

    add-int/lit8 v22, v23, -0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    move/from16 v23, v22

    goto/16 :goto_0

    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v13

    const/4 v14, 0x0

    :goto_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move/from16 v29, v0

    move/from16 v0, v29

    if-ge v14, v0, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    aget v29, v29, v14

    move/from16 v0, v29

    if-ge v0, v13, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v29, v0

    aget v13, v29, v14

    :cond_10
    add-int/lit8 v14, v14, 0x1

    goto :goto_d

    :cond_11
    sub-int v29, v11, v13

    return v29

    :cond_12
    move/from16 v30, v31

    move/from16 v29, v33

    goto/16 :goto_6

    :cond_13
    move/from16 v29, v30

    goto/16 :goto_5

    :cond_14
    move-object/from16 v24, v29

    goto/16 :goto_2
.end method

.method private static generateDefaultLayoutParams()Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;
    .locals 2

    new-instance v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(I)V

    return-object v0
.end method

.method private static generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;
    .locals 1
    .param p0    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method private getNextColumnDown()I
    .locals 6

    const v4, 0x7fffffff

    const/4 v3, 0x0

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v0, v5, v2

    if-ge v0, v4, :cond_0

    move v4, v0

    move v3, v2

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method private getNextColumnUp()I
    .locals 6

    const/4 v3, -0x1

    const/high16 v0, -0x80000000

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-ltz v2, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aget v4, v5, v2

    if-le v4, v0, :cond_0

    move v0, v4

    move v3, v2

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    return v3
.end method

.method private invalidateLayoutRecordsBeforePosition(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v1

    if-ge v1, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAtRange(II)V

    return-void
.end method

.method private invokeOnItemScrollListener(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    invoke-interface {v0, p0, v1, p1}, Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;->onScroll(Lcom/google/android/apps/plus/views/StreamGridView;II)V

    :cond_0
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->onScrollChanged(IIII)V

    return-void
.end method

.method private obtainView(ILandroid/view/View;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    const/4 v5, 0x0

    if-eqz v5, :cond_0

    iget-boolean v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    if-eqz v6, :cond_3

    :cond_0
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    iget v1, v6, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->viewType:I

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v2

    if-ne v1, v2, :cond_5

    move-object v3, p2

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, p1, v3, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    if-eq v5, v3, :cond_1

    if-eqz v3, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-virtual {v6, v3}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    :cond_1
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    if-eq v6, p0, :cond_3

    if-nez v0, :cond_6

    invoke-static {}, Lcom/google/android/apps/plus/views/StreamGridView;->generateDefaultLayoutParams()Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    move-result-object v0

    :cond_2
    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    iput p1, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->position:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v6

    iput v6, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->viewType:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v6, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v6

    iput-wide v6, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->id:J

    return-object v5

    :cond_4
    const/4 v1, -0x1

    goto :goto_0

    :cond_5
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {v0}, Lcom/google/android/apps/plus/views/StreamGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    move-result-object v0

    goto :goto_2
.end method

.method private populate()V
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mMinColWidth:I

    div-int v2, v4, v5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-eq v2, v4, :cond_2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    :cond_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    array-length v4, v4

    if-ne v4, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    array-length v4, v4

    if-eq v4, v2, :cond_6

    :cond_3
    new-array v4, v2, [I

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    new-array v4, v2, [I

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v4}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    if-eqz v4, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->removeAllViewsInLayout()V

    :cond_4
    :goto_1
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mPopulating:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    if-eqz v4, :cond_a

    const/4 v4, 0x0

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v5

    if-ge v4, v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->removeAllViews()V

    goto :goto_1

    :cond_6
    const/4 v3, 0x0

    :goto_3
    if-ge v3, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aget v5, v5, v3

    aput v5, v4, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    if-eqz v4, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->removeAllViewsInLayout()V

    :cond_8
    :goto_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v5

    add-int/2addr v4, v5

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/plus/views/StreamGridView;->fillDown(II)I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    add-int/lit8 v4, v4, -0x1

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingTop()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/plus/views/StreamGridView;->fillUp(II)I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mPopulating:Z

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/plus/views/StreamGridView;->invokeOnItemScrollListener(I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->removeAllViews()V

    goto :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingLeft()I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingRight()I

    move-result v4

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v5

    sub-int/2addr v5, v12

    sub-int v4, v5, v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    add-int/lit8 v5, v5, -0x1

    mul-int/2addr v5, v13

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    div-int v14, v4, v5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v15

    const/4 v4, 0x0

    move v10, v4

    :goto_5
    if-ge v10, v15, :cond_8

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    const/4 v5, 0x0

    const/4 v4, -0x1

    if-eqz v6, :cond_b

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    iget v5, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    move/from16 v20, v5

    move-object v5, v4

    move/from16 v4, v20

    :cond_b
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    add-int v16, v7, v10

    if-nez v11, :cond_c

    if-eqz v6, :cond_c

    invoke-virtual {v6}, Landroid/view/View;->isLayoutRequested()Z

    move-result v7

    if-eqz v7, :cond_12

    :cond_c
    const/4 v7, 0x1

    :goto_6
    if-eqz v11, :cond_e

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/plus/views/StreamGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v4

    if-eq v4, v6, :cond_18

    if-eqz v6, :cond_d

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->removeViewAt(I)V

    :cond_d
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->addView(Landroid/view/View;I)V

    move-object v5, v4

    :goto_7
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getNextColumnDown()I

    move-result v6

    iput v6, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    iget v6, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    move/from16 v20, v6

    move-object v6, v5

    move-object v5, v4

    move/from16 v4, v20

    :cond_e
    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    iget v9, v5, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v17

    mul-int v8, v14, v17

    add-int/lit8 v9, v17, -0x1

    mul-int/2addr v9, v13

    add-int/2addr v8, v9

    if-eqz v7, :cond_f

    const/high16 v7, 0x40000000

    invoke-static {v8, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    iget v7, v5, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    const/4 v9, -0x2

    if-ne v7, v9, :cond_13

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    :goto_8
    invoke-virtual {v6, v8, v7}, Landroid/view/View;->measure(II)V

    :cond_f
    const/4 v7, 0x1

    move/from16 v0, v17

    if-le v0, v7, :cond_11

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    sub-int/2addr v7, v4

    move/from16 v0, v17

    if-ge v7, v0, :cond_10

    const/4 v4, 0x0

    :cond_10
    iput v4, v5, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    :cond_11
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v7, v7, v4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    add-int/2addr v8, v7

    const/4 v7, 0x1

    move/from16 v0, v17

    if-le v0, v7, :cond_14

    const/4 v7, 0x0

    move v9, v7

    :goto_9
    move/from16 v0, v17

    if-ge v9, v0, :cond_14

    add-int v7, v4, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v18, v0

    aget v7, v18, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    move/from16 v18, v0

    add-int v7, v7, v18

    if-le v7, v8, :cond_17

    :goto_a
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    move v8, v7

    goto :goto_9

    :cond_12
    const/4 v7, 0x0

    goto/16 :goto_6

    :cond_13
    iget v7, v5, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    const/high16 v9, 0x40000000

    invoke-static {v7, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    goto :goto_8

    :cond_14
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int v9, v8, v7

    add-int v18, v14, v13

    mul-int v18, v18, v4

    add-int v18, v18, v12

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v19

    add-int v19, v19, v18

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v8, v1, v9}, Landroid/view/View;->layout(IIII)V

    const/4 v6, 0x0

    :goto_b
    move/from16 v0, v17

    if-ge v6, v0, :cond_15

    add-int v8, v4, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v18, v0

    aput v9, v18, v8

    add-int/lit8 v6, v6, 0x1

    goto :goto_b

    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    if-nez v4, :cond_16

    new-instance v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move/from16 v0, v16

    invoke-virtual {v6, v0, v4}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :cond_16
    iget v6, v5, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    iput v6, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    iput v7, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    iget-wide v5, v5, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->id:J

    iput-wide v5, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->id:J

    move/from16 v0, v17

    iput v0, v4, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    add-int/lit8 v4, v10, 0x1

    move v10, v4

    goto/16 :goto_5

    :cond_17
    move v7, v8

    goto :goto_a

    :cond_18
    move-object v5, v6

    goto/16 :goto_7
.end method

.method private setTouchMode(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchMode:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchMode:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;->onTouchModeChanged(Lcom/google/android/apps/plus/views/StreamGridView;I)V

    :cond_0
    return-void
.end method

.method private trackMotionScroll(IZ)Z
    .locals 22
    .param p1    # I
    .param p2    # Z

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    if-nez v10, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I

    if-eq v10, v11, :cond_1

    :cond_0
    const/4 v3, 0x0

    :goto_0
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-nez v3, :cond_1b

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mPopulating:Z

    if-lez p1, :cond_6

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->fillUp(II)I

    move-result v8

    const/4 v9, 0x1

    :goto_1
    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-eqz v9, :cond_8

    move v10, v5

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v12

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v12, :cond_9

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLeft()I

    move-result v14

    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    move-result v15

    add-int/2addr v15, v10

    invoke-virtual {v13}, Landroid/view/View;->getRight()I

    move-result v16

    invoke-virtual {v13}, Landroid/view/View;->getBottom()I

    move-result v17

    add-int v17, v17, v10

    invoke-virtual/range {v13 .. v17}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    :cond_1
    const v12, 0x7fffffff

    const/high16 v11, -0x80000000

    const/4 v10, 0x0

    :goto_4
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-ge v10, v13, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aget v13, v13, v10

    if-ge v13, v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aget v12, v12, v10

    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v13, v13, v10

    if-le v13, v11, :cond_3

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v11, v11, v10

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingTop()I

    move-result v10

    if-lt v12, v10, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingBottom()I

    move-result v12

    sub-int/2addr v10, v12

    if-gt v11, v10, :cond_5

    const/4 v3, 0x1

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v11

    add-int/2addr v10, v11

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/google/android/apps/plus/views/StreamGridView;->fillDown(II)I

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    add-int v8, v10, v11

    if-gez v8, :cond_7

    const/4 v8, 0x0

    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_8
    neg-int v10, v5

    goto/16 :goto_2

    :cond_9
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    const/4 v11, 0x0

    :goto_5
    if-ge v11, v12, :cond_a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aget v14, v13, v11

    add-int/2addr v14, v10

    aput v14, v13, v11

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v14, v13, v11

    add-int/2addr v14, v10

    aput v14, v13, v11

    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v10

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    neg-int v11, v11

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    add-int/2addr v12, v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    :goto_6
    if-ltz v10, :cond_c

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    move-result v14

    if-le v14, v12, :cond_c

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    if-eqz v14, :cond_b

    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v14}, Lcom/google/android/apps/plus/views/StreamGridView;->removeViewsInLayout(II)V

    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-virtual {v14, v13}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    add-int/lit8 v10, v10, -0x1

    goto :goto_6

    :cond_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->removeViewAt(I)V

    goto :goto_7

    :cond_c
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v10

    if-lez v10, :cond_e

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v12

    if-ge v12, v11, :cond_e

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    if-eqz v12, :cond_d

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/google/android/apps/plus/views/StreamGridView;->removeViewsInLayout(II)V

    :goto_9
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-virtual {v12, v10}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v0, p0

    iput v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    goto :goto_8

    :cond_d
    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/google/android/apps/plus/views/StreamGridView;->removeViewAt(I)V

    goto :goto_9

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildCount()I

    move-result v14

    if-lez v14, :cond_15

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    const v11, 0x7fffffff

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    const/high16 v11, -0x80000000

    invoke-static {v10, v11}, Ljava/util/Arrays;->fill([II)V

    const/4 v10, 0x0

    move v13, v10

    :goto_a
    if-ge v13, v14, :cond_13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/apps/plus/views/StreamGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-virtual {v12}, Landroid/view/View;->getTop()I

    move-result v11

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    sub-int v15, v11, v15

    invoke-virtual {v12}, Landroid/view/View;->getBottom()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    move/from16 v17, v0

    add-int v17, v17, v13

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    if-nez v11, :cond_f

    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    add-int v17, v11, v13

    new-instance v11, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-direct {v11, v0}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    iget v0, v10, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    move/from16 v17, v0

    move/from16 v0, v17

    iput v0, v11, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v12

    iput v12, v11, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    iget-wide v0, v10, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->id:J

    move-wide/from16 v17, v0

    move-wide/from16 v0, v17

    iput-wide v0, v11, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->id:J

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    iget v0, v10, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    move-result v12

    iput v12, v11, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    :cond_f
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    iget v0, v10, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    move-result v17

    const/4 v12, 0x0

    :goto_b
    move/from16 v0, v17

    if-ge v12, v0, :cond_12

    iget v0, v10, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->column:I

    move/from16 v18, v0

    add-int v18, v18, v12

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->getMarginAbove(I)I

    move-result v19

    sub-int v19, v15, v19

    invoke-virtual {v11, v12}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->getMarginBelow(I)I

    move-result v20

    add-int v20, v20, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v21, v0

    aget v21, v21, v18

    move/from16 v0, v19

    move/from16 v1, v21

    if-ge v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    move-object/from16 v21, v0

    aput v19, v21, v18

    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v19, v0

    aget v19, v19, v18

    move/from16 v0, v20

    move/from16 v1, v19

    if-le v0, v1, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    move-object/from16 v19, v0

    aput v20, v19, v18

    :cond_11
    add-int/lit8 v12, v12, 0x1

    goto :goto_b

    :cond_12
    add-int/lit8 v10, v13, 0x1

    move v13, v10

    goto/16 :goto_a

    :cond_13
    const/4 v10, 0x0

    :goto_c
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-ge v10, v11, :cond_15

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aget v11, v11, v10

    const v12, 0x7fffffff

    if-ne v11, v12, :cond_14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingTop()I

    move-result v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    aput v11, v12, v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aput v11, v12, v10

    :cond_14
    add-int/lit8 v10, v10, 0x1

    goto :goto_c

    :cond_15
    const/4 v10, 0x0

    move-object/from16 v0, p0

    iput-boolean v10, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mPopulating:Z

    sub-int v7, v2, v8

    :goto_d
    if-eqz p2, :cond_17

    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v6

    if-eqz v6, :cond_16

    const/4 v10, 0x1

    if-ne v6, v10, :cond_17

    if-nez v3, :cond_17

    :cond_16
    if-lez v7, :cond_17

    if-lez p1, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    :goto_e
    invoke-static/range {p1 .. p1}, Ljava/lang/Math;->abs(I)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v11

    int-to-float v11, v11

    div-float/2addr v10, v11

    invoke-virtual {v4, v10}, Landroid/support/v4/widget/EdgeEffectCompat;->onPull(F)Z

    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_17
    if-eqz p1, :cond_18

    invoke-direct/range {p0 .. p1}, Lcom/google/android/apps/plus/views/StreamGridView;->invokeOnItemScrollListener(I)V

    :cond_18
    if-nez v5, :cond_19

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamGridView;->requestLayout()V

    :cond_19
    if-eqz p1, :cond_1a

    if-eqz v5, :cond_1d

    :cond_1a
    const/4 v10, 0x1

    :goto_f
    return v10

    :cond_1b
    move v7, v2

    const/4 v5, 0x0

    goto :goto_d

    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_e

    :cond_1d
    const/4 v10, 0x0

    goto :goto_f
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    return v0
.end method

.method public computeScroll()V
    .locals 8

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v4

    int-to-float v6, v4

    iget v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    sub-float/2addr v6, v7

    float-to-int v0, v6

    int-to-float v6, v4

    iput v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/plus/views/StreamGridView;->trackMotionScroll(IZ)Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v3, v5

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_4

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->getOverScrollMode(Landroid/view/View;)I

    move-result v2

    const/4 v6, 0x2

    if-eq v2, v6, :cond_3

    if-lez v0, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    :goto_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ScrollerCompat;->getCurrVelocity()F

    move-result v6

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/support/v4/widget/EdgeEffectCompat;->onAbsorb(I)Z

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/apps/plus/views/StreamGridView;->setTouchMode(I)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    goto :goto_2
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    if-eqz v3, :cond_2

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    const/4 v0, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3}, Landroid/support/v4/widget/EdgeEffectCompat;->isFinished()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v2

    neg-int v3, v2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v3, 0x43340000

    int-to-float v4, v2

    const/4 v5, 0x0

    invoke-virtual {p1, v3, v4, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/EdgeEffectCompat;->draw(Landroid/graphics/Canvas;)Z

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    const/4 v0, 0x1

    :cond_1
    if-eqz v0, :cond_2

    invoke-static {p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    :cond_2
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-static {}, Lcom/google/android/apps/plus/views/StreamGridView;->generateDefaultLayoutParams()Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    invoke-static {p1}, Lcom/google/android/apps/plus/views/StreamGridView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final getColumnCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    return v0
.end method

.method public final getFirstPosition()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    return v0
.end method

.method public final getItemMargin()I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    return v0
.end method

.method public final getMaximumAcceptableSpan()I
    .locals 9

    const/4 v6, 0x1

    iget v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHoleThresholdPx:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    if-nez v7, :cond_2

    :cond_0
    iget v5, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    :cond_1
    :goto_0
    return v5

    :cond_2
    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-ne v1, v6, :cond_3

    move v5, v6

    goto :goto_0

    :cond_3
    const v4, 0x7fffffff

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_5

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v7, v7, v2

    if-ge v7, v4, :cond_4

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v4, v7, v2

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v5, v1

    :goto_2
    const/4 v7, 0x2

    if-lt v5, v7, :cond_9

    const/4 v0, 0x0

    :goto_3
    sub-int v7, v1, v5

    if-gt v0, v7, :cond_8

    const/high16 v3, -0x80000000

    move v2, v0

    :goto_4
    add-int v7, v0, v5

    if-ge v2, v7, :cond_7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v7, v7, v2

    if-le v7, v3, :cond_6

    iget-object v7, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v3, v7, v2

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    sub-int v7, v3, v4

    iget v8, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHoleThresholdPx:I

    if-le v7, v8, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    add-int/lit8 v5, v5, -0x1

    goto :goto_2

    :cond_9
    move v5, v6

    goto :goto_0
.end method

.method public final invalidateLayoutRecordsAfterPosition(I)V
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v1, v0}, Landroid/support/v4/util/SparseArrayCompat;->keyAt(I)I

    move-result v1

    if-le v1, p1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/util/SparseArrayCompat;->removeAtRange(II)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v10, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    and-int/lit16 v0, v6, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v6, v8

    :goto_0
    return v6

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v6}, Landroid/view/VelocityTracker;->clear()V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    iget-object v6, v6, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->abortAnimation()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    invoke-static {p1, v8}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v6

    iput v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    iput v10, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchRemainderY:F

    iget v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchMode:I

    const/4 v9, 0x2

    if-ne v6, v9, :cond_0

    invoke-direct {p0, v7}, Lcom/google/android/apps/plus/views/StreamGridView;->setTouchMode(I)V

    move v6, v7

    goto :goto_0

    :pswitch_2
    iget v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    invoke-static {p1, v6}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v4

    if-gez v4, :cond_1

    const-string v6, "StaggeredGridView"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "onInterceptTouchEvent could not find pointer with id "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " - did StaggeredGridView receive an inconsistent event stream?"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v8

    goto :goto_0

    :cond_1
    invoke-static {p1, v4}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v5

    iget v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    sub-float v6, v5, v6

    iget v9, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchRemainderY:F

    add-float v3, v6, v9

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v9, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    int-to-float v9, v9

    cmpl-float v6, v6, v9

    if-lez v6, :cond_3

    move v1, v7

    :goto_1
    if-eqz v1, :cond_2

    cmpl-float v6, v3, v10

    if-lez v6, :cond_4

    iget v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    neg-int v6, v6

    int-to-float v6, v6

    :goto_2
    add-float/2addr v3, v6

    :cond_2
    float-to-int v2, v3

    int-to-float v6, v2

    sub-float v6, v3, v6

    iput v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchRemainderY:F

    if-eqz v1, :cond_0

    move v6, v7

    goto :goto_0

    :cond_3
    move v1, v8

    goto :goto_1

    :cond_4
    iget v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    int-to-float v6, v6

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v3, 0x0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->populate()V

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mInLayout:Z

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mForcePopulateOnLayout:Z

    sub-int v1, p4, p2

    sub-int v0, p5, p3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mTopEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mBottomEdge:Landroid/support/v4/widget/EdgeEffectCompat;

    invoke-virtual {v2, v1, v0}, Landroid/support/v4/widget/EdgeEffectCompat;->setSize(II)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setMeasuredDimension(II)V

    iget v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCountSetting:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mMinColWidth:I

    div-int v0, v2, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-eq v0, v3, :cond_0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mForcePopulateOnLayout:Z

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1    # Landroid/view/MotionEvent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v10, v1, 0xff

    packed-switch v10, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->abortAnimation()V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    const/4 v1, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->getPointerId(Landroid/view/MotionEvent;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchRemainderY:F

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Landroid/support/v4/view/MotionEventCompat;->findPointerIndex(Landroid/view/MotionEvent;I)I

    move-result v13

    if-gez v13, :cond_1

    const-string v1, "StaggeredGridView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onInterceptTouchEvent could not find pointer with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - did StaggeredGridView receive an inconsistent event stream?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Landroid/support/v4/view/MotionEventCompat;->getY(Landroid/view/MotionEvent;I)F

    move-result v15

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    sub-float v1, v15, v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchRemainderY:F

    add-float v12, v1, v2

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchMode:I

    if-nez v1, :cond_2

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    const/4 v1, 0x0

    cmpl-float v1, v12, v1

    if-lez v1, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    neg-int v1, v1

    int-to-float v1, v1

    :goto_2
    add-float/2addr v12, v1

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setTouchMode(I)V

    :cond_2
    float-to-int v11, v12

    int-to-float v1, v11

    sub-float v1, v12, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchRemainderY:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->trackMotionScroll(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mTouchSlop:I

    int-to-float v1, v1

    goto :goto_2

    :pswitch_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setTouchMode(I)V

    goto/16 :goto_0

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v2, 0x3e8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mMaximumVelocity:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mActivePointerId:I

    invoke-static {v1, v2}, Landroid/support/v4/view/VelocityTrackerCompat;->getYVelocity(Landroid/view/VelocityTracker;I)F

    move-result v14

    invoke-static {v14}, Ljava/lang/Math;->abs(F)F

    move-result v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mFlingVelocity:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    const/4 v1, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setTouchMode(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mScroller:Lcom/google/android/apps/plus/views/ScrollerCompat;

    float-to-int v5, v14

    iget-object v1, v1, Lcom/google/android/apps/plus/views/ScrollerCompat;->mScroller:Landroid/widget/Scroller;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, -0x80000000

    const v9, 0x7fffffff

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView;->mLastTouchY:F

    invoke-static/range {p0 .. p0}, Landroid/support/v4/view/ViewCompat;->postInvalidateOnAnimation(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->setTouchMode(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mPopulating:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 3
    .param p1    # Landroid/widget/ListAdapter;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mObserver:Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    invoke-interface {v0, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->clearAllState()V

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mAdapter:Landroid/widget/ListAdapter;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemCount:I

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOldItemCount:I

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mObserver:Lcom/google/android/apps/plus/views/StreamGridView$StreamGridDataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-interface {p1}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->setViewTypeCount(I)V

    invoke-interface {p1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHasStableIds:Z

    :goto_1
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iput-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHasStableIds:Z

    goto :goto_1
.end method

.method public setColumnCount(I)V
    .locals 4
    .param p1    # I

    if-gtz p1, :cond_0

    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Column count must be at least 1 - received "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-eq p1, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCountSetting:I

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->clearAllState()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->populate()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFirstPositionAndOffsets(II)V
    .locals 11
    .param p1    # I
    .param p2    # I

    const/high16 v10, 0x40000000

    const/4 v3, 0x0

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    iget v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    div-int v7, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingTop()I

    move-result v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getPaddingTop()I

    move-result v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    :cond_2
    move v2, v3

    :goto_0
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mFirstPosition:I

    if-ge v2, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0, v2}, Landroid/support/v4/util/SparseArrayCompat;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mDataChanged:Z

    if-nez v1, :cond_3

    if-nez v0, :cond_5

    :cond_3
    const/4 v1, 0x0

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/views/StreamGridView;->obtainView(ILandroid/view/View;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;

    iget v4, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    const/4 v6, -0x2

    if-ne v4, v6, :cond_8

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    :goto_1
    iget v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    iget v8, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    mul-int v8, v7, v6

    iget v9, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v6, v9

    add-int/2addr v6, v8

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;

    invoke-direct {v0, v3}, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;-><init>(B)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mLayoutRecords:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    :cond_4
    iput v4, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    iget-wide v8, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->id:J

    iput-wide v8, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->id:J

    iget v4, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    iget v1, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->span:I

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->addScrap(Landroid/view/View;)V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->getNextColumnDown()I

    move-result v1

    iget v4, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    sub-int/2addr v4, v1

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    if-ge v4, v5, :cond_6

    move v1, v3

    :cond_6
    iput v1, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->column:I

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v4, v4, v1

    iget v5, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    add-int/2addr v4, v5

    iget v5, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    const/4 v6, 0x1

    if-le v5, v6, :cond_9

    move v5, v4

    move v4, v3

    :goto_2
    iget v6, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    if-ge v4, v6, :cond_a

    add-int v6, v1, v4

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v6, v8, v6

    iget v8, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    add-int/2addr v6, v8

    if-le v6, v5, :cond_7

    move v5, v6

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_8
    iget v4, v1, Lcom/google/android/apps/plus/views/StreamGridView$LayoutParams;->height:I

    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    goto :goto_1

    :cond_9
    move v5, v4

    :cond_a
    move v4, v3

    :goto_3
    iget v6, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->span:I

    if-ge v4, v6, :cond_b

    add-int v6, v1, v4

    iget-object v8, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    iget v9, v0, Lcom/google/android/apps/plus/views/StreamGridView$LayoutRecord;->height:I

    add-int/2addr v9, v5

    aput v9, v8, v6

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_c
    const v0, 0x7fffffff

    move v1, v0

    move v0, v3

    :goto_4
    iget v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-ge v0, v2, :cond_e

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v2, v2, v0

    if-ge v2, v1, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v1, v1, v0

    :cond_d
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_e
    :goto_5
    iget v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mColCount:I

    if-ge v3, v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v2, v2, v3

    sub-int/2addr v2, v1

    add-int/2addr v2, p2

    aput v2, v0, v3

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemTops:[I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemBottoms:[I

    aget v2, v2, v3

    aput v2, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_f
    return-void
.end method

.method public setHoleThresholdPx(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mHoleThresholdPx:I

    return-void
.end method

.method public setItemMargin(I)V
    .locals 2
    .param p1    # I

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    if-eq p1, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mItemMargin:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/plus/views/StreamGridView;->populate()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMinColumnWidth(I)V
    .locals 1
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mMinColWidth:I

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->setColumnCount(I)V

    return-void
.end method

.method public setOnScrollListener(Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mOnScrollListener:Lcom/google/android/apps/plus/views/StreamGridView$OnScrollListener;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->invokeOnItemScrollListener(I)V

    return-void
.end method

.method public setRecyclerListener(Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamGridView;->mRecycler:Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;

    # setter for: Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->mRecyclerListener:Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;
    invoke-static {v0, p1}, Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;->access$202(Lcom/google/android/apps/plus/views/StreamGridView$RecycleBin;Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;)Lcom/google/android/apps/plus/views/StreamGridView$RecyclerListener;

    return-void
.end method

.method public setSelectionToTop()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/plus/views/StreamGridView;->setFirstPositionAndOffsets(II)V

    return-void
.end method
