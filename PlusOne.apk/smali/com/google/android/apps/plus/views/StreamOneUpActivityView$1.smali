.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;
.super Ljava/lang/Object;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 6
    .param p1    # Landroid/text/style/URLSpan;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v2, "square"

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareId()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mDbSquareUpdate:Lcom/google/android/apps/plus/content/DbSquareUpdate;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$100(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareStreamId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/apps/plus/views/OneUpListener;->onSquareClick(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    new-instance v3, Landroid/text/style/URLSpan;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "acl:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$1;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mActivityId:Ljava/lang/String;
    invoke-static {v5}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$200(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/plus/views/OneUpListener;->onSpanClick(Landroid/text/style/URLSpan;)V

    goto :goto_0
.end method
