.class final Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;
.super Ljava/lang/Object;
.source "StreamOneUpActivityView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableStaticLayout$SpanClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/StreamOneUpActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSpanClick(Landroid/text/style/URLSpan;)V
    .locals 3
    .param p1    # Landroid/text/style/URLSpan;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    const-string v2, "skyjam:buy:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/apps/plus/views/OneUpListener;->onSkyjamBuyClick(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "skyjam:listen:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpActivityView$5;->this$0:Lcom/google/android/apps/plus/views/StreamOneUpActivityView;

    # getter for: Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->mOneUpListener:Lcom/google/android/apps/plus/views/OneUpListener;
    invoke-static {v2}, Lcom/google/android/apps/plus/views/StreamOneUpActivityView;->access$000(Lcom/google/android/apps/plus/views/StreamOneUpActivityView;)Lcom/google/android/apps/plus/views/OneUpListener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/apps/plus/views/OneUpListener;->onSkyjamListenClick(Ljava/lang/String;)V

    goto :goto_0
.end method
