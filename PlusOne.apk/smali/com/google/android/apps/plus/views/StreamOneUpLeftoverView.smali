.class public Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;
.super Landroid/view/View;
.source "StreamOneUpLeftoverView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sBackgroundPaint:Landroid/graphics/Paint;


# instance fields
.field private mFixedHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->sBackgroundPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_list_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->sBackgroundPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final bind(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->mFixedHeight:I

    if-gez p1, :cond_0

    move p1, v0

    :cond_0
    iput p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->mFixedHeight:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->requestLayout()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->getHeight()I

    move-result v6

    int-to-float v3, v7

    int-to-float v4, v6

    sget-object v5, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->sBackgroundPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->mFixedHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRecycle()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpLeftoverView;->mFixedHeight:I

    return-void
.end method
