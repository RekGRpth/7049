.class public Lcom/google/android/apps/plus/views/StreamOneUpListView;
.super Landroid/widget/ListView;
.source "StreamOneUpListView.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;


# instance fields
.field private mMaxWidth:I

.field private mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    new-instance v0, Lcom/google/android/apps/plus/views/StreamOneUpListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpListView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    new-instance v0, Lcom/google/android/apps/plus/views/StreamOneUpListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpListView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    new-instance v0, Lcom/google/android/apps/plus/views/StreamOneUpListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView$1;-><init>(Lcom/google/android/apps/plus/views/StreamOneUpListView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    return-void
.end method


# virtual methods
.method protected layoutChildren()V
    .locals 5

    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/SettableItemAdapter;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    :goto_0
    if-ltz v2, :cond_0

    add-int v3, v1, v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/SettableItemAdapter;->setItemHeight(II)V

    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->onMeasure(II)V

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    const/high16 v1, 0x40000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/ListView;->onMeasure(II)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;->onMeasured(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public final onMeasured(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildCount()I

    move-result v4

    add-int/lit8 v1, v4, -0x1

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v2, v1

    :cond_0
    if-gez v2, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getFirstVisiblePosition()I

    move-result v4

    add-int v3, v4, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/SettableItemAdapter;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-interface {v0, v3, v4}, Lcom/google/android/apps/plus/fragments/SettableItemAdapter;->setItemHeight(II)V

    goto :goto_1
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mMaxWidth:I

    return-void
.end method

.method public setOnMeasureListener(Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/StreamOneUpListView;->mOnMeasuredListener:Lcom/google/android/apps/plus/views/OneUpBaseView$OnMeasuredListener;

    return-void
.end method
