.class public Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;
.super Landroid/view/View;
.source "StreamOneUpSkyjamView.java"

# interfaces
.implements Lcom/google/android/apps/plus/service/ResourceConsumer;
.implements Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;
.implements Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;


# static fields
.field private static sActionBarHeight:I

.field protected static sDefaultTextPaint:Landroid/text/TextPaint;

.field private static sEmptyImage:Landroid/graphics/Bitmap;

.field private static sGoogleMusic:Landroid/graphics/Bitmap;

.field protected static sMaxWidth:I

.field protected static sNameMargin:I

.field private static sPlayIcon:Landroid/graphics/Bitmap;

.field protected static sPlayStopButtonPadding:I

.field private static sPreviewPaint:Landroid/graphics/Paint;

.field private static sResizePaint:Landroid/graphics/Paint;

.field private static sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

.field private static sStopIcon:Landroid/graphics/Bitmap;

.field private static sStreamOneUpSkyjamViewInitialized:Z

.field private static sTagIcon:Landroid/graphics/Bitmap;

.field private static sTagPaint:Landroid/graphics/Paint;

.field protected static sTagTextPaint:Landroid/text/TextPaint;


# instance fields
.field private mActionIcon:Landroid/graphics/Bitmap;

.field private mActionIconPoint:Landroid/graphics/PointF;

.field private mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field private mActivityId:Ljava/lang/String;

.field private final mClickableItems:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

.field private mGoogleMusicRect:Landroid/graphics/Rect;

.field private mImageRect:Landroid/graphics/Rect;

.field private mImageResource:Lcom/google/android/apps/plus/service/Resource;

.field private mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private mIsAlbum:Z

.field private mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

.field private mMusicUrl:Ljava/lang/String;

.field private mPreviewBackground:Landroid/graphics/RectF;

.field private mPreviewStatus:Ljava/lang/String;

.field private mPreviewStatusPoint:Landroid/graphics/PointF;

.field private mTagBackground:Landroid/graphics/RectF;

.field private mTagIconRect:Landroid/graphics/Rect;

.field private mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

.field private mTagTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStreamOneUpSkyjamViewInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStreamOneUpSkyjamViewInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_metadata_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->empty_thumbnail:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->google_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_play:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_stop:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_action_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sActionBarHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_hangout_name_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sNameMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_skyjam_play_stop_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayStopButtonPadding:I

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_tag_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_tag:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$string;->skyjam_content_play_button_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStreamOneUpSkyjamViewInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStreamOneUpSkyjamViewInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_metadata_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->empty_thumbnail:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->google_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_play:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_stop:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_action_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sActionBarHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_hangout_name_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sNameMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_skyjam_play_stop_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayStopButtonPadding:I

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_tag_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_tag:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$string;->skyjam_content_play_button_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    sget-boolean v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStreamOneUpSkyjamViewInitialized:Z

    if-nez v1, :cond_0

    sput-boolean v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStreamOneUpSkyjamViewInitialized:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_metadata_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->empty_thumbnail:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->google_music:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_play:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$drawable;->ic_stop:I

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/ImageUtils;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeToSquareBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    sget v1, Lcom/google/android/apps/plus/R$dimen;->host_action_bar_height:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sActionBarHeight:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_list_max_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_hangout_name_margin:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sNameMargin:I

    sget v1, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_skyjam_play_stop_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayStopButtonPadding:I

    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_tag_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview_background:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_skyjam_preview:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$color;->stream_one_up_stage_tag:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget v2, Lcom/google/android/apps/plus/R$dimen;->stream_one_up_stage_default_text_size:I

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/util/TextPaintUtils;->registerTextPaint(Landroid/text/TextPaint;I)V

    sget v1, Lcom/google/android/apps/plus/R$string;->skyjam_content_play_button_description:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    return-void
.end method


# virtual methods
.method public final bind(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->unbindResources()V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    iput-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_4

    const-string v4, "https://"

    invoke-virtual {p5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string v4, "https://"

    invoke-virtual {p5, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    :cond_0
    :goto_0
    if-ltz v2, :cond_3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_5

    invoke-virtual {p5, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->getPlaybackStatus(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_7

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget-boolean v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-eqz v4, :cond_8

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_9

    new-instance v4, Lcom/google/android/apps/plus/api/MediaRef;

    sget-object v5, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    invoke-direct {v4, p3, v5}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    :goto_4
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x100

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput-object p6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActivityId:Ljava/lang/String;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->bindResources()V

    return-void

    :cond_4
    const-string v4, "https://"

    invoke-virtual {p4, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_0

    const-string v4, "https://"

    invoke-virtual {p4, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    :cond_5
    invoke-virtual {p4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    const-string v5, "mode=inline"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    const-string v5, "mode=inline"

    const-string v6, "mode=streaming"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&mode=streaming"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    goto/16 :goto_1

    :cond_7
    sget v4, Lcom/google/android/apps/plus/R$string;->skyjam_from_the_album:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StringUtils;->unescape(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_8
    move-object v0, p2

    goto/16 :goto_3

    :cond_9
    iput-object v8, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    goto/16 :goto_4
.end method

.method public bindResources()V
    .locals 3

    const/16 v2, 0x12c

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v0, v1, v2, v2, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;IILcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v2, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v4, v5

    :goto_0
    return v4

    :pswitch_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v5}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    move-result v6

    if-eqz v6, :cond_0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    goto :goto_0

    :cond_1
    move v4, v5

    goto :goto_0

    :pswitch_1
    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    move v4, v5

    goto :goto_0

    :pswitch_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    if-eqz v6, :cond_3

    iget-object v5, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    const/4 v6, 0x3

    invoke-interface {v5, v2, v3, v6}, Lcom/google/android/apps/plus/views/ClickableItem;->handleEvent(III)Z

    iput-object v7, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mCurrentClickableItem:Lcom/google/android/apps/plus/views/ClickableItem;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    goto :goto_0

    :cond_3
    move v4, v5

    goto :goto_0

    :pswitch_3
    move v4, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->registerListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->bindResources()V

    return-void
.end method

.method public final onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz v2, :cond_1

    const-string v3, "com.google.android.apps.plus.service.SkyjamPlaybackService.PLAY"

    :goto_1
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "music_account"

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v3, "music_url"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "song"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "activity_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActivityId:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    const-string v3, "com.google.android.apps.plus.service.SkyjamPlaybackService.STOP"

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->unregisterListener(Lcom/google/android/apps/plus/service/SkyjamPlaybackService$SkyjamPlaybackListener;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->unbindResources()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v2, 0x40a00000

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagIconRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getTop()I

    move-result v1

    int-to-float v2, v0

    int-to-float v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sEmptyImage:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageRect:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sResizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    sget-object v1, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPreviewPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIconPoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIconPoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mGoogleMusicRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 22
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v15

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    sget v14, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    :goto_0
    const/high16 v2, 0x40000000

    invoke-static {v14, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-super {v0, v2, v1}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingLeft()I

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingTop()I

    move-result v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getMeasuredWidth()I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getMeasuredHeight()I

    move-result v12

    sub-int v2, v13, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingRight()I

    move-result v3

    sub-int v11, v2, v3

    sub-int v2, v12, v17

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingBottom()I

    move-result v3

    sub-int v10, v2, v3

    add-int/lit8 v18, v16, 0xd

    add-int/lit8 v19, v17, 0xd

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v2

    float-to-int v5, v2

    add-int/lit8 v2, v20, 0xf

    add-int/2addr v2, v5

    new-instance v3, Landroid/graphics/RectF;

    move/from16 v0, v18

    int-to-float v4, v0

    move/from16 v0, v19

    int-to-float v6, v0

    add-int v2, v2, v18

    int-to-float v2, v2

    add-int/lit8 v7, v19, 0x27

    int-to-float v7, v7

    invoke-direct {v3, v4, v6, v2, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    new-instance v2, Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagTitle:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sTagTextPaint:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->getHeight()I

    move-result v2

    add-int/lit8 v3, v18, 0x5

    rsub-int/lit8 v4, v21, 0x27

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, v19

    new-instance v5, Landroid/graphics/Rect;

    add-int v6, v3, v20

    add-int v7, v4, v21

    invoke-direct {v5, v3, v4, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagIconRect:Landroid/graphics/Rect;

    add-int/lit8 v4, v20, 0x5

    add-int/2addr v3, v4

    add-int/lit8 v4, v17, 0xd

    rsub-int/lit8 v2, v2, 0x27

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagLayout:Lcom/google/android/apps/plus/views/PositionedStaticLayout;

    invoke-virtual {v4, v3, v2}, Lcom/google/android/apps/plus/views/PositionedStaticLayout;->setPosition(II)V

    add-int/lit8 v2, v17, 0xd

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v6, v2, 0xd

    add-int/lit8 v3, v11, -0x34

    sub-int v2, v10, v6

    add-int v2, v2, v17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v4, :cond_2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    sub-int/2addr v3, v2

    div-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1a

    add-int v3, v3, v16

    new-instance v4, Landroid/graphics/Rect;

    add-int v5, v3, v2

    add-int v7, v6, v2

    invoke-direct {v4, v3, v6, v5, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageRect:Landroid/graphics/Rect;

    move v9, v2

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mIsAlbum:Z

    if-nez v2, :cond_0

    new-instance v2, Landroid/graphics/RectF;

    int-to-float v4, v3

    int-to-float v5, v6

    add-int v7, v3, v9

    int-to-float v7, v7

    add-int/lit8 v8, v6, 0x42

    int-to-float v8, v8

    invoke-direct {v2, v4, v5, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableRect;

    sget v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sActionBarHeight:I

    add-int/2addr v4, v6

    add-int v5, v3, v9

    add-int/lit8 v6, v6, 0x42

    sget-object v8, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sSkyjamPlayButtonDesc:Ljava/lang/CharSequence;

    move-object/from16 v7, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mClickableItems:Ljava/util/Set;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/apps/plus/service/SkyjamPlaybackService;->isPlaying(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    :goto_2
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, 0xd

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    rsub-int/lit8 v2, v2, 0x42

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    new-instance v4, Landroid/graphics/PointF;

    int-to-float v5, v3

    int-to-float v2, v2

    invoke-direct {v4, v5, v2}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIconPoint:Landroid/graphics/PointF;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    add-int/2addr v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    rsub-int/lit8 v2, v2, 0x42

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    sget-object v4, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sDefaultTextPaint:Landroid/text/TextPaint;

    invoke-virtual {v4}, Landroid/text/TextPaint;->ascent()F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v2, v4

    new-instance v4, Landroid/graphics/PointF;

    int-to-float v3, v3

    int-to-float v2, v2

    invoke-direct {v4, v3, v2}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatusPoint:Landroid/graphics/PointF;

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sGoogleMusic:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    sub-int/2addr v4, v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    rsub-int/lit8 v6, v3, 0x42

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    new-instance v6, Landroid/graphics/Rect;

    add-int/2addr v2, v4

    add-int/2addr v3, v5

    invoke-direct {v6, v4, v5, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mGoogleMusicRect:Landroid/graphics/Rect;

    :cond_0
    add-int/lit8 v2, v17, 0xd

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mTagBackground:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v17, v2, 0xd

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v2, :cond_4

    add-int v17, v17, v9

    :cond_1
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->getPaddingBottom()I

    move-result v2

    add-int v2, v2, v17

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v2}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->setMeasuredDimension(II)V

    return-void

    :sswitch_0
    sget v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sMaxWidth:I

    invoke-static {v15, v2}, Ljava/lang/Math;->min(II)I

    move-result v14

    goto/16 :goto_0

    :sswitch_1
    move v14, v15

    goto/16 :goto_0

    :cond_2
    move v9, v3

    move/from16 v3, v16

    goto/16 :goto_1

    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewBackground:Landroid/graphics/RectF;

    if-eqz v2, :cond_1

    add-int/lit8 v17, v17, 0x42

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onPlaybackStatusUpdate(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sStopIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mMusicUrl:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    iput-object p3, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mPreviewStatus:Ljava/lang/String;

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    if-ne v0, v2, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    :cond_2
    return-void

    :cond_3
    sget-object v2, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->sPlayIcon:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mActionIcon:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onResourceStatusChange(Lcom/google/android/apps/plus/service/Resource;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/service/Resource;
    .param p2    # Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->invalidate()V

    return-void
.end method

.method public unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/StreamOneUpSkyjamView;->mImageResource:Lcom/google/android/apps/plus/service/Resource;

    :cond_0
    return-void
.end method
