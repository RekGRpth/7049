.class public Lcom/google/android/apps/plus/views/SuggestedParticipantView;
.super Landroid/widget/RelativeLayout;
.source "SuggestedParticipantView.java"


# instance fields
.field private mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

.field private mHeaderView:Landroid/view/View;

.field private mParticipantName:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public final clear()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mParticipantName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    return-void
.end method

.method public onFinishInflate()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$id;->avatar:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/AvatarView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    sget v0, Lcom/google/android/apps/plus/R$id;->participantName:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mParticipantName:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->sectionHeader:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mHeaderView:Landroid/view/View;

    return-void
.end method

.method public setHeaderVisible(Z)V
    .locals 2
    .param p1    # Z

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mHeaderView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setParticipantId(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mAvatarView:Lcom/google/android/apps/plus/views/AvatarView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/AvatarView;->setGaiaId(Ljava/lang/String;)V

    return-void
.end method

.method public setParticipantName(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedParticipantView;->mParticipantName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
