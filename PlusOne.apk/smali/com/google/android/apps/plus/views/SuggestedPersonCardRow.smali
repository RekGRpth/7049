.class public final Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;
.super Ljava/lang/Object;
.source "SuggestedPersonCardRow.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;
.implements Lcom/google/android/apps/plus/views/Recyclable;


# static fields
.field private static sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;


# instance fields
.field private mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mActivityId:Ljava/lang/String;

.field private mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

.field private mCardRowRect:Landroid/graphics/Rect;

.field private mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field private mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

.field private mMaxHeight:I

.field private mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

.field private mPromoType:I

.field private mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field private mSubtitleLayout:Landroid/text/StaticLayout;

.field private mTextY:I

.field private mTitleLayout:Landroid/text/StaticLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardRowRect:Landroid/graphics/Rect;

    sget-object v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    :cond_0
    return-void
.end method


# virtual methods
.method public final bindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    :cond_0
    return-void
.end method

.method public final create(Landroid/content/Context;IIIII)I
    .locals 24
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    move/from16 v20, p5

    sget-object v4, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p5, p5, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget-object v5, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v5, v5, p2

    sget-object v7, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v7, v7, p5

    move/from16 v0, p2

    move/from16 v1, p5

    invoke-virtual {v4, v0, v1, v5, v7}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getCircles()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    const/16 v16, 0x1

    :goto_0
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_3

    const/4 v4, 0x0

    invoke-interface {v13, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/plus/content/CircleData;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v6

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v5}, Lcom/google/android/apps/plus/views/ClickableHost;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    if-eqz v16, :cond_5

    sget-object v4, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->circlesBitmap:Landroid/graphics/Bitmap;

    :goto_2
    if-eqz v16, :cond_6

    const/4 v9, 0x3

    :goto_3
    if-eqz v16, :cond_7

    const/4 v10, 0x0

    :goto_4
    sget-object v4, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v12, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->buttonBitmapTextSpacing:I

    move-object/from16 v4, p1

    move/from16 v7, p2

    move/from16 v8, p5

    move-object v11, v6

    invoke-static/range {v4 .. v12}, Lcom/google/android/apps/plus/util/ButtonUtils;->getButton(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;IIILcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;Ljava/lang/CharSequence;I)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v4, v5}, Lcom/google/android/apps/plus/views/ClickableHost;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    sget-object v4, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    sub-int v4, p6, v4

    sget-object v5, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v5, v5, 0x2

    sub-int/2addr v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int v22, v4, v5

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v21

    if-eqz v21, :cond_0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-static {v4, v0, v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/lit8 v23, v4, 0x0

    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getDescription()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-static {v4, v0, v1, v5}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSubtitleLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSubtitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    sget-object v5, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int v23, v4, v5

    :cond_1
    :goto_5
    sget-object v4, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v0, v23

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v4, v5

    div-int/lit8 v15, v4, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int v5, p6, v5

    invoke-virtual {v4, v5, v15}, Lcom/google/android/apps/plus/views/ClickableButton;->offset(II)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    sget-object v5, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    sub-int/2addr v4, v5

    div-int/lit8 v14, v4, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    add-int v5, p5, v14

    sget-object v7, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v7, v7, p2

    sget-object v8, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v8, v8, p5

    add-int/2addr v8, v14

    move/from16 v0, p2

    invoke-virtual {v4, v0, v5, v7, v8}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    sub-int v4, v4, v23

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, p5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTextY:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    add-int v4, v4, p5

    sget-object v5, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v18, v4, v5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    move/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, p4

    move/from16 v3, v18

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/apps/plus/views/ClickableRect;->setBounds(IIII)V

    return v18

    :cond_2
    const/16 v16, 0x0

    goto/16 :goto_0

    :cond_3
    sget v4, Lcom/google/android/apps/plus/R$string;->add:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPromoType:I

    const/4 v7, 0x3

    if-ne v5, v7, :cond_4

    sget v4, Lcom/google/android/apps/plus/R$string;->follow:I

    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    :cond_5
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v9, 0x2

    goto/16 :goto_3

    :cond_7
    move-object/from16 v10, p0

    goto/16 :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSubtitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v23

    goto/16 :goto_5
.end method

.method public final draw(Landroid/graphics/Canvas;IIIII)I
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    move v1, p5

    sget-object v6, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p5, v6

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_0
    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v7}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v7

    sget-object v8, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v8, v8, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v6, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int/2addr v6, p2

    sget-object v7, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v3, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_1

    iget v4, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTextY:I

    int-to-float v6, v3

    int-to-float v7, v4

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v6, v3

    int-to-float v6, v6

    neg-int v7, v4

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSubtitleLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v6, :cond_6

    iget v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTextY:I

    sget-object v7, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v7

    add-int v2, v6, v7

    :goto_1
    int-to-float v6, v3

    int-to-float v7, v2

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSubtitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v6, v3

    int-to-float v6, v6

    neg-int v7, v2

    int-to-float v7, v7

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v6, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    iget v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    add-int/2addr v6, p5

    sget-object v7, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v0, v6, v7

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v6}, Lcom/google/android/apps/plus/views/ClickableRect;->isClicked()Z

    move-result v6

    if-eqz v6, :cond_4

    sget-object v6, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    add-int v7, p3, p4

    invoke-virtual {v6, p3, v1, v7, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    sget-object v6, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->pressedStateBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardRowRect:Landroid/graphics/Rect;

    iput p2, v6, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardRowRect:Landroid/graphics/Rect;

    add-int v7, p2, p6

    iput v7, v6, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardRowRect:Landroid/graphics/Rect;

    iput p5, v6, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardRowRect:Landroid/graphics/Rect;

    iput v0, v6, Landroid/graphics/Rect;->bottom:I

    return v0

    :cond_5
    sget-object v6, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->squareMediumAvatarBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_6
    iget v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTextY:I

    goto :goto_1
.end method

.method public final getPersonId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPersonId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getPersonName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getRect(Landroid/graphics/Rect;)V
    .locals 1
    .param p1    # Landroid/graphics/Rect;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardRowRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method public final getSuggestionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getSuggestionId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final init(Lcom/google/android/apps/plus/views/PromoCardViewGroup;Lcom/google/android/apps/plus/views/ClickableHost;Lcom/google/android/apps/plus/content/DbSuggestedPerson;I)V
    .locals 10
    .param p1    # Lcom/google/android/apps/plus/views/PromoCardViewGroup;
    .param p2    # Lcom/google/android/apps/plus/views/ClickableHost;
    .param p3    # Lcom/google/android/apps/plus/content/DbSuggestedPerson;
    .param p4    # I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PromoCardViewGroup;->getActivityId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActivityId:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iput-object p3, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    iput p4, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPromoType:I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/ClickableHost;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/ClickableHost;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v9

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/PersonData;->getCompressedPhotoUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getSuggestionId()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;IZ)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setSuggestionClickListener(Lcom/google/android/apps/plus/views/ClickableAvatar$SuggestionAvatarClickListener;)V

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableRect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/ClickableHost;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/ClickableHost;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    return-void
.end method

.method public final onAvatarClick$14e1ec6d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;->onPersonClicked(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 7
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPersonId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPromoType:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getSuggestionId()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActivityId:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;->onPersonAdded(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getPerson()Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/PersonData;->getObfuscatedId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbSuggestedPerson;->getSuggestionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;->onPersonClicked(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onRecycle()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->unbindResources()V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSelectionRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/ClickableHost;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActionButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-interface {v0, v1}, Lcom/google/android/apps/plus/views/ClickableHost;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableHost:Lcom/google/android/apps/plus/views/ClickableHost;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTitleLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mSubtitleLayout:Landroid/text/StaticLayout;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mPerson:Lcom/google/android/apps/plus/content/DbSuggestedPerson;

    iput v3, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mTextY:I

    iput v3, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mMaxHeight:I

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mActivityId:Ljava/lang/String;

    return-void
.end method

.method public final setCardActionListener(Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mCardActionListener:Lcom/google/android/apps/plus/views/PromoCardViewGroup$PromoCardActionListener;

    return-void
.end method

.method public final unbindResources()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/SuggestedPersonCardRow;->mClickableAvatar:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    :cond_0
    return-void
.end method
