.class public Lcom/google/android/apps/plus/views/TabContainer;
.super Lcom/google/android/apps/plus/views/ScrollableViewGroup;
.source "TabContainer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;
    }
.end annotation


# instance fields
.field private mFirstVisiblePanel:I

.field private mLastVisiblePanel:I

.field private mListener:Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;

.field private mPanelWidth:I

.field private mSelectedPanel:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, -0x1

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/views/ScrollableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/google/android/apps/plus/views/TabContainer;->mFirstVisiblePanel:I

    iput v0, p0, Lcom/google/android/apps/plus/views/TabContainer;->mLastVisiblePanel:I

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TabContainer;->setVertical(Z)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    if-nez v5, :cond_0

    const/4 v1, 0x1

    :goto_0
    sub-int v5, p4, p2

    iput v5, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    sub-int v2, p5, p3

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getChildCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/TabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v5, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    mul-int p2, v5, v3

    iget v5, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    add-int/2addr v5, p2

    invoke-virtual {v0, p2, v4, v5, v2}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    move v1, v4

    goto :goto_0

    :cond_1
    iget v5, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    mul-int/2addr v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/apps/plus/views/TabContainer;->setScrollLimits(II)V

    if-eqz v1, :cond_2

    iget v4, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    iget v5, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    mul-int/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/views/TabContainer;->scrollTo(I)V

    :cond_2
    return-void
.end method

.method public onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/high16 v5, 0x40000000

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v4, v1}, Lcom/google/android/apps/plus/views/TabContainer;->setMeasuredDimension(II)V

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getChildCount()I

    move-result v5

    if-ge v2, v5, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/TabContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v0, v3}, Landroid/view/View;->measure(II)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onScrollChanged(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    div-int v0, p1, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    rem-int v3, p1, v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_0
    add-int v2, v0, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mFirstVisiblePanel:I

    if-ne v0, v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mLastVisiblePanel:I

    if-eq v2, v3, :cond_3

    :cond_0
    iput v0, p0, Lcom/google/android/apps/plus/views/TabContainer;->mFirstVisiblePanel:I

    iput v2, p0, Lcom/google/android/apps/plus/views/TabContainer;->mLastVisiblePanel:I

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_3

    iget-object v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mListener:Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;

    iget v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mFirstVisiblePanel:I

    if-lt v1, v3, :cond_1

    iget v3, p0, Lcom/google/android/apps/plus/views/TabContainer;->mLastVisiblePanel:I

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 v3, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method protected final onScrollFinished(I)V
    .locals 3
    .param p1    # I

    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getScrollX()I

    move-result v0

    if-gez p1, :cond_2

    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    div-int v1, v0, v1

    iput v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    :goto_1
    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TabContainer;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    :cond_1
    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    iget v2, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    mul-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/TabContainer;->smoothScrollTo(I)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mListener:Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;

    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    div-int v1, v0, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    goto :goto_1
.end method

.method public setOnTabChangeListener(Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mListener:Lcom/google/android/apps/plus/views/TabContainer$OnTabChangeListener;

    return-void
.end method

.method public setSelectedPanel(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    iget v0, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/plus/views/TabContainer;->mPanelWidth:I

    iget v1, p0, Lcom/google/android/apps/plus/views/TabContainer;->mSelectedPanel:I

    mul-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TabContainer;->smoothScrollTo(I)V

    :cond_0
    return-void
.end method
