.class public final enum Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;
.super Ljava/lang/Enum;
.source "TextOnlyAudienceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/TextOnlyAudienceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ChevronDirection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

.field public static final enum POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

.field public static final enum POINT_LEFT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

.field public static final enum POINT_RIGHT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

.field public static final enum POINT_UP:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    const-string v1, "POINT_RIGHT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_RIGHT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    new-instance v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    const-string v1, "POINT_LEFT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_LEFT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    new-instance v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    const-string v1, "POINT_UP"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_UP:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    new-instance v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    const-string v1, "POINT_DOWN"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_RIGHT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_LEFT:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_UP:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->$VALUES:[Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->$VALUES:[Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    invoke-virtual {v0}, [Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    return-object v0
.end method
