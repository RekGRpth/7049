.class public Lcom/google/android/apps/plus/views/TextOnlyAudienceView;
.super Lcom/google/android/apps/plus/views/AudienceView;
.source "TextOnlyAudienceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/TextOnlyAudienceView$1;,
        Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;
    }
.end annotation


# instance fields
.field private mAudienceHint:Landroid/widget/TextView;

.field private mAudienceIcon:Landroid/widget/ImageView;

.field private mAudienceNames:Lcom/google/android/apps/plus/views/ConstrainedTextView;

.field private mChevronDirection:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

.field private mChevronIcon:Landroid/widget/ImageView;

.field private mSeparator:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I
    .param p4    # Z

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/plus/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IZ)V

    sget-object v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronDirection:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    return-void
.end method


# virtual methods
.method protected final addChip(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method protected final getChipCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final hideSeparator()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mSeparator:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method protected final init()V
    .locals 1

    sget v0, Lcom/google/android/apps/plus/R$layout;->audience_view_text_only:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->inflate(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->addView(Landroid/view/View;)V

    sget v0, Lcom/google/android/apps/plus/R$id;->audience_names_container:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ConstrainedTextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceNames:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->audience_to_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceIcon:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/apps/plus/R$id;->chevron_icon:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronIcon:Landroid/widget/ImageView;

    sget v0, Lcom/google/android/apps/plus/R$id;->audience_to_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceHint:Landroid/widget/TextView;

    sget v0, Lcom/google/android/apps/plus/R$id;->separator:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mSeparator:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->update()V

    return-void
.end method

.method protected final removeLastChip()V
    .locals 0

    return-void
.end method

.method public setChevronContentDescription(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setChevronDirection(Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    sget-object v1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$1;->$SwitchMap$com$google$android$apps$plus$views$TextOnlyAudienceView$ChevronDirection:[I

    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    sget-object p1, Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;->POINT_DOWN:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_down:I

    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronDirection:Lcom/google/android/apps/plus/views/TextOnlyAudienceView$ChevronDirection;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronIcon:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-void

    :pswitch_0
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_left:I

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_right:I

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/apps/plus/R$drawable;->ic_up:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setChevronVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronIcon:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChevronIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public setHint(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceHint:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method protected final update()V
    .locals 35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChips:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    sget v32, Lcom/google/android/apps/plus/R$string;->compose_acl_separator:I

    move-object/from16 v0, v27

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    const v32, 0x104000e

    move-object/from16 v0, v27

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    sget v32, Lcom/google/android/apps/plus/R$string;->loading:I

    move-object/from16 v0, v27

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v21

    sget v32, Lcom/google/android/apps/plus/R$string;->square_unknown:I

    move-object/from16 v0, v27

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v22

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    if-nez v8, :cond_1

    const/16 v16, 0x0

    const/16 v19, 0x8

    :goto_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceNames:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceNames:Lcom/google/android/apps/plus/views/ConstrainedTextView;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/views/ConstrainedTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceHint:Landroid/widget/TextView;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    move-object/from16 v32, v0

    if-eqz v32, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceChangedCallback:Ljava/lang/Runnable;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Ljava/lang/Runnable;->run()V

    :cond_0
    return-void

    :cond_1
    const/4 v13, 0x0

    const/4 v10, 0x0

    const/4 v14, 0x0

    const/4 v11, 0x0

    const/4 v15, 0x0

    const/4 v12, 0x0

    const/16 v17, 0x0

    :goto_1
    move/from16 v0, v17

    if-ge v0, v8, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mChips:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/google/android/apps/plus/content/AudienceData;

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getCircleCount()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_9

    const/16 v32, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/AudienceData;->getCircle(I)Lcom/google/android/apps/plus/content/CircleData;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getType()I

    move-result v7

    const/16 v32, 0x9

    move/from16 v0, v32

    if-ne v7, v0, :cond_4

    const/4 v13, 0x1

    :cond_2
    :goto_2
    invoke-virtual {v5}, Lcom/google/android/apps/plus/content/CircleData;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_8

    move-object/from16 v24, v6

    :goto_3
    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v32, v8, -0x1

    move/from16 v0, v17

    move/from16 v1, v32

    if-ge v0, v1, :cond_3

    move-object/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    :cond_4
    const/16 v32, 0x8

    move/from16 v0, v32

    if-ne v7, v0, :cond_5

    const/4 v10, 0x1

    goto :goto_2

    :cond_5
    const/16 v32, 0x7

    move/from16 v0, v32

    if-ne v7, v0, :cond_6

    const/4 v11, 0x1

    goto :goto_2

    :cond_6
    const/16 v32, 0x5

    move/from16 v0, v32

    if-ne v7, v0, :cond_7

    const/4 v15, 0x1

    goto :goto_2

    :cond_7
    const/16 v32, 0x65

    move/from16 v0, v32

    if-ne v7, v0, :cond_2

    const/4 v12, 0x1

    goto :goto_2

    :cond_8
    move-object/from16 v24, v21

    goto :goto_3

    :cond_9
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getUserCount()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_c

    const/16 v32, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/AudienceData;->getUser(I)Lcom/google/android/apps/plus/content/PersonData;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/content/PersonData;->getName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/plus/content/PersonData;->getEmail()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_a

    move-object/from16 v24, v26

    :goto_4
    goto :goto_3

    :cond_a
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-nez v32, :cond_b

    move-object/from16 v24, v9

    goto :goto_4

    :cond_b
    move-object/from16 v24, v23

    goto :goto_4

    :cond_c
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTargetCount()I

    move-result v32

    const/16 v33, 0x1

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_f

    const/16 v32, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/content/AudienceData;->getSquareTarget(I)Lcom/google/android/apps/plus/content/SquareTargetData;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareName()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/apps/plus/content/SquareTargetData;->getSquareStreamName()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-eqz v32, :cond_d

    move-object/from16 v30, v22

    :cond_d
    const/4 v14, 0x1

    invoke-static/range {v31 .. v31}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v32

    if-eqz v32, :cond_e

    move-object/from16 v24, v30

    goto/16 :goto_3

    :cond_e
    sget v32, Lcom/google/android/apps/plus/R$string;->square_name_and_topic:I

    const/16 v33, 0x2

    move/from16 v0, v33

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v33, v0

    const/16 v34, 0x0

    aput-object v30, v33, v34

    const/16 v34, 0x1

    aput-object v31, v33, v34

    move-object/from16 v0, v27

    move/from16 v1, v32

    move-object/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    goto/16 :goto_3

    :cond_f
    new-instance v32, Ljava/lang/IllegalArgumentException;

    invoke-direct/range {v32 .. v32}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v32

    :cond_10
    sget v18, Lcom/google/android/apps/plus/R$drawable;->ic_person_active:I

    if-eqz v14, :cond_12

    sget v18, Lcom/google/android/apps/plus/R$drawable;->ic_nav_communities:I

    :cond_11
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/plus/views/TextOnlyAudienceView;->mAudienceIcon:Landroid/widget/ImageView;

    move-object/from16 v32, v0

    move-object/from16 v0, v32

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    const/16 v16, 0x8

    const/16 v19, 0x0

    goto/16 :goto_0

    :cond_12
    const/16 v32, 0x1

    move/from16 v0, v32

    if-ne v8, v0, :cond_11

    if-eqz v13, :cond_13

    sget v18, Lcom/google/android/apps/plus/R$drawable;->ic_public_active:I

    goto :goto_5

    :cond_13
    if-eqz v10, :cond_14

    sget v18, Lcom/google/android/apps/plus/R$drawable;->list_domain:I

    goto :goto_5

    :cond_14
    if-eqz v11, :cond_15

    sget v18, Lcom/google/android/apps/plus/R$drawable;->list_extended:I

    goto :goto_5

    :cond_15
    if-eqz v15, :cond_16

    sget v18, Lcom/google/android/apps/plus/R$drawable;->ic_circles_active:I

    goto :goto_5

    :cond_16
    if-eqz v12, :cond_11

    sget v18, Lcom/google/android/apps/plus/R$drawable;->ic_private:I

    goto :goto_5
.end method

.method protected final updateChip(IIILjava/lang/String;Ljava/lang/Object;Z)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/Object;
    .param p6    # Z

    return-void
.end method
