.class public Lcom/google/android/apps/plus/views/Thermometer;
.super Landroid/view/View;
.source "Thermometer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/Thermometer$Orientation;
    }
.end annotation


# static fields
.field private static sBounds:Landroid/graphics/Rect;


# instance fields
.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mFillLevel:D

.field private mForeground:Landroid/graphics/drawable/Drawable;

.field private mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/Thermometer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/Thermometer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer$Orientation;->HORIZONTAL:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/R$styleable;->Thermometer:[I

    invoke-virtual {p1, p2, v2, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    :cond_2
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    if-eqz v1, :cond_4

    if-ne v1, v4, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer$Orientation;->VERTICAL:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    :goto_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer$Orientation;->HORIZONTAL:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    iput-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    goto :goto_1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1    # Landroid/graphics/Canvas;

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/Thermometer;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/Thermometer;->getHeight()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    sget-object v3, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    sget-object v3, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    sget-object v3, Lcom/google/android/apps/plus/views/Thermometer$Orientation;->HORIZONTAL:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    if-ne v2, v3, :cond_5

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/Thermometer;->mFillLevel:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->right:I

    :cond_3
    :goto_1
    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_4
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/plus/views/Thermometer;->mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    sget-object v3, Lcom/google/android/apps/plus/views/Thermometer$Orientation;->VERTICAL:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    if-ne v2, v3, :cond_3

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-double v2, v2

    iget-wide v4, p0, Lcom/google/android/apps/plus/views/Thermometer;->mFillLevel:D

    mul-double/2addr v2, v4

    double-to-int v0, v2

    sget-object v2, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/views/Thermometer;->sBounds:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    if-lez v4, :cond_0

    move v5, v4

    :cond_0
    iget-object v6, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-lez v0, :cond_1

    move v1, v0

    :cond_1
    iget-object v6, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    if-le v4, v5, :cond_2

    move v5, v4

    :cond_2
    iget-object v6, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-le v0, v1, :cond_3

    move v1, v0

    :cond_3
    if-lez v5, :cond_4

    if-lez v1, :cond_4

    invoke-static {v5, p1}, Lcom/google/android/apps/plus/views/Thermometer;->resolveSize(II)I

    move-result v3

    invoke-static {v1, p2}, Lcom/google/android/apps/plus/views/Thermometer;->resolveSize(II)I

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/google/android/apps/plus/views/Thermometer;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_4
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public setBackgroundImage(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/Thermometer;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/Thermometer;->invalidate()V

    return-void
.end method

.method public setFillLevel(D)V
    .locals 2
    .param p1    # D

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    const-wide/16 p1, 0x0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    const-wide/high16 p1, 0x3ff0000000000000L

    :cond_1
    iput-wide p1, p0, Lcom/google/android/apps/plus/views/Thermometer;->mFillLevel:D

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/Thermometer;->invalidate()V

    return-void
.end method

.method public setForegroundImage(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/Thermometer;->mForeground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/Thermometer;->invalidate()V

    return-void
.end method

.method public setOrientation(Lcom/google/android/apps/plus/views/Thermometer$Orientation;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/Thermometer;->mOrientation:Lcom/google/android/apps/plus/views/Thermometer$Orientation;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/Thermometer;->invalidate()V

    return-void
.end method
