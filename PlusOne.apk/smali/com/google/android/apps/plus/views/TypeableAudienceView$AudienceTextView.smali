.class public Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;
.super Landroid/widget/AutoCompleteTextView;
.source "TypeableAudienceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/apps/plus/views/TypeableAudienceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AudienceTextView"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->mListener:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;

    return-object v0
.end method


# virtual methods
.method public onCheckIsTextEditor()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3
    .param p1    # Landroid/view/inputmethod/EditorInfo;

    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;-><init>(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;Landroid/view/inputmethod/InputConnection;Z)V

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView$AudienceInputConnection;->setAudienceTextView(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;)V

    return-object v0
.end method

.method public setAudienceTextViewListener(Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;)V
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextView;->mListener:Lcom/google/android/apps/plus/views/TypeableAudienceView$AudienceTextViewListener;

    return-void
.end method
