.class public Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
.super Lcom/google/android/apps/plus/views/StreamCardViewGroup;
.source "UpdateCardViewGroup.java"

# interfaces
.implements Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;
.implements Lcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;
.implements Lcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;,
        Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;,
        Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;,
        Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;,
        Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;
    }
.end annotation


# static fields
.field private static sAccelerateDecelerateInterpolator:Landroid/view/animation/Interpolator;

.field private static final sDampingInterpolator:Landroid/view/animation/Interpolator;


# instance fields
.field protected mAnnotation:Ljava/lang/String;

.field protected mAnnotationLayout:Landroid/text/StaticLayout;

.field protected mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

.field protected mAttribution:Landroid/text/SpannableStringBuilder;

.field protected mAttributionLayout:Landroid/text/StaticLayout;

.field protected mAuthorAvatarUrl:Ljava/lang/String;

.field protected mAuthorGaiaId:Ljava/lang/String;

.field protected mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

.field protected mAuthorName:Ljava/lang/String;

.field protected mAuthorNameLayout:Landroid/text/StaticLayout;

.field protected mBackgrounds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;",
            ">;"
        }
    .end annotation
.end field

.field protected mBottomBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

.field protected mCanComment:Z

.field protected mCanPlusOne:Z

.field protected mCanReshare:Z

.field protected mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

.field protected mCommentTop:I

.field protected mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

.field protected mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

.field protected mCommenterIndicatorView:Landroid/view/View;

.field protected mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mCornerIcon:Landroid/graphics/Bitmap;

.field protected mCreationSourceId:Ljava/lang/String;

.field protected mEventId:Ljava/lang/String;

.field protected mEventOwnerId:Ljava/lang/String;

.field protected mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

.field protected mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

.field protected mGraySpamLayout:Landroid/text/StaticLayout;

.field protected mInvisiblePlusOneButton:Z

.field protected mIsApprovedGraySpam:Z

.field protected mIsGraySpam:Z

.field protected mIsLimited:Z

.field protected mIsSquarePost:Z

.field protected mLocation:Ljava/lang/CharSequence;

.field protected mLocationIcon:Landroid/graphics/Bitmap;

.field protected mLocationLayout:Landroid/text/StaticLayout;

.field protected mOriginalSquareName:Ljava/lang/String;

.field protected mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mOverridePlusOnedButtonDisplay:Z

.field protected mPlusOneBarTop:I

.field protected mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field protected mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

.field protected mRelativeTime:Ljava/lang/String;

.field protected mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

.field private mShakeAnimation:Ljava/lang/Runnable;

.field protected mSquareId:Ljava/lang/String;

.field protected mSquareMode:Z

.field protected mSquareName:Ljava/lang/String;

.field protected mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

.field protected mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

.field protected mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

.field protected mTitle:Ljava/lang/String;

.field protected mTitleLayout:Landroid/text/StaticLayout;

.field protected mTopBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

.field protected mTotalComments:I

.field protected mViewedListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;

.field protected mViewerHasRead:Z

.field protected mViewerIsSquareAdmin:Z

.field protected mWhatsHotHeader:Ljava/lang/String;

.field private mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

.field protected mWhatsHotLayout:Landroid/text/StaticLayout;

.field protected mWhatsHotSubHeader:Ljava/lang/String;

.field protected mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$1;

    invoke-direct {v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$1;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sDampingInterpolator:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sAccelerateDecelerateInterpolator:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/views/StreamCommentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$color;->riviera_commenter_indicator:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mBackgrounds:Ljava/util/List;

    new-instance v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;-><init>(Lcom/google/android/apps/plus/views/StreamCardViewGroup;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTopBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    new-instance v0, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->bottomBackground:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;-><init>(Lcom/google/android/apps/plus/views/StreamCardViewGroup;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mBottomBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mBackgrounds:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTopBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mBackgrounds:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mBottomBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic access$000()Landroid/view/animation/Interpolator;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sDampingInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .param p1    # Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    return-object v0
.end method

.method private createGraySpamBar(III)I
    .locals 14
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsGraySpam:Z

    if-eqz v1, :cond_0

    if-lez p3, :cond_0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamWarningBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamXPadding:I

    mul-int/lit8 v5, v5, 0x3

    add-int/2addr v1, v5

    sub-int v3, p3, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v5, 0x7

    invoke-static {v1, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    new-instance v0, Landroid/text/StaticLayout;

    sget v1, Lcom/google/android/apps/plus/R$string;->card_square_gray_spam_for_moderator:I

    invoke-virtual {v13, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    new-instance v4, Landroid/text/StaticLayout;

    sget v1, Lcom/google/android/apps/plus/R$string;->card_square_gray_spam_for_moderator_approved:I

    invoke-virtual {v13, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v9, 0x3f800000

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v6, v2

    move v7, v3

    invoke-direct/range {v4 .. v11}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v12

    iget-boolean v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsApprovedGraySpam:Z

    if-nez v1, :cond_1

    sget v1, Lcom/google/android/apps/plus/R$string;->card_square_gray_spam_for_moderator:I

    invoke-virtual {v13, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamLayout:Landroid/text/StaticLayout;

    :goto_0
    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamMinHeight:I

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamWarningBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v5, v12}, Ljava/lang/Math;->max(II)I

    move-result v5

    sget-object v6, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamYPadding:I

    mul-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v9

    new-instance v5, Lcom/google/android/apps/plus/views/ClickableRect;

    move v6, p1

    move/from16 v7, p2

    move/from16 v8, p3

    move-object v10, p0

    invoke-direct/range {v5 .. v11}, Lcom/google/android/apps/plus/views/ClickableRect;-><init>(IIIILcom/google/android/apps/plus/views/ClickableRect$ClickableRectListener;Ljava/lang/CharSequence;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v1, v5

    add-int p2, p2, v1

    :cond_0
    return p2

    :cond_1
    sget v1, Lcom/google/android/apps/plus/R$string;->card_square_gray_spam_for_moderator_approved:I

    invoke-virtual {v13, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    iput-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamLayout:Landroid/text/StaticLayout;

    goto :goto_0
.end method

.method private createPlusOneBar(III)I
    .locals 21
    .param p1    # I
    .param p2    # I
    .param p3    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    add-int v20, p1, p3

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p2, p2, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v18, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v2, :cond_5

    const/16 v19, 0x1

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x1

    move/from16 v0, v19

    invoke-static {v0, v9}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v2, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanPlusOne:Z

    if-eqz v2, :cond_9

    new-instance v2, Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v18, :cond_6

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    :goto_2
    if-eqz v18, :cond_7

    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_3
    if-eqz v18, :cond_8

    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_4
    move-object/from16 v8, p0

    move/from16 v9, p1

    move/from16 v10, p2

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    add-int/2addr v2, v5

    add-int p1, p1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanReshare:Z

    if-eqz v2, :cond_a

    new-instance v5, Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v7, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBitmap:Landroid/graphics/Bitmap;

    sget-object v8, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v9, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v6, Lcom/google/android/apps/plus/R$string;->reshare_button_content_description:I

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object v6, v3

    move-object/from16 v10, p0

    move/from16 v11, p1

    move/from16 v12, p2

    invoke-direct/range {v5 .. v13}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentXPadding:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :goto_6
    move/from16 p1, v20

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    if-nez v2, :cond_b

    sget v2, Lcom/google/android/apps/plus/R$string;->riviera_add_comment_button:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    :goto_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    if-gtz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanComment:Z

    if-eqz v2, :cond_1

    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    if-lez v2, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$plurals;->stream_one_up_comment_count:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    invoke-virtual {v2, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    :goto_8
    new-instance v5, Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    if-lez v2, :cond_d

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v7, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commentsBitmap:Landroid/graphics/Bitmap;

    :goto_9
    sget-object v9, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v10, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v11, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanComment:Z

    if-eqz v2, :cond_e

    move-object/from16 v12, p0

    :goto_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    const/16 v16, 0x0

    move-object v6, v3

    move/from16 v13, p1

    move/from16 v14, p2

    invoke-direct/range {v5 .. v16}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;IILjava/lang/CharSequence;B)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int p1, p1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanComment:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v17, v2, v5

    :goto_b
    if-nez v17, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v17, v2, v5

    :cond_2
    if-nez v17, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v17, v2, v5

    :cond_3
    add-int v2, p2, v17

    return v2

    :cond_4
    const/16 v18, 0x0

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v19

    goto/16 :goto_1

    :cond_6
    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sNotPlusOnedTextPaint:Landroid/text/TextPaint;

    goto/16 :goto_2

    :cond_7
    sget-object v6, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_3

    :cond_8
    sget-object v7, Lcom/google/android/apps/plus/util/PlusBarUtils;->sButtonPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    goto/16 :goto_4

    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    goto/16 :goto_5

    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    goto/16 :goto_6

    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_7

    :cond_c
    move-object v15, v8

    goto/16 :goto_8

    :cond_d
    const/4 v7, 0x0

    goto/16 :goto_9

    :cond_e
    const/4 v12, 0x0

    goto/16 :goto_a

    :cond_f
    const/16 v17, 0x0

    goto :goto_b
.end method


# virtual methods
.method public bindResources()V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->bindResources()V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/views/ClickableAvatar;->bindResources()V

    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v3

    if-lez v3, :cond_3

    new-array v4, v3, [Lcom/google/android/apps/plus/service/Resource;

    iput-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/content/DbStreamComments;->getAuthorPhotoUrls(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/plus/content/DbStreamComments;->getAuthorIds(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {v5, v1, v6, v6, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatar(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v5

    aput-object v5, v4, v2

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->imageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    invoke-virtual {v5, v0, v6, v6, p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getAvatarByGaiaId(Ljava/lang/String;IZLcom/google/android/apps/plus/service/ResourceConsumer;)Lcom/google/android/apps/plus/service/Resource;

    move-result-object v5

    aput-object v5, v4, v2

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final createCard$3fd9ad2e(IIIII)I
    .locals 13
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    move/from16 v0, p3

    invoke-direct {p0, p2, p1, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->createGraySpamBar(III)I

    move-result p1

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p1, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    if-nez v2, :cond_15

    const/4 v2, 0x0

    :goto_0
    sub-int v5, p5, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeader:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_16

    const/4 v2, 0x1

    move v10, v2

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeader:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_17

    const/4 v2, 0x1

    move v11, v2

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v12

    if-nez v10, :cond_0

    if-eqz v11, :cond_7

    :cond_0
    if-nez v10, :cond_1

    if-eqz v11, :cond_2

    :cond_1
    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p1, v2

    :cond_2
    if-eqz v10, :cond_3

    const/16 v2, 0x12

    invoke-static {v12, v2}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeader:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr p1, v2

    :cond_3
    if-eqz v10, :cond_4

    if-eqz v11, :cond_4

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    add-int/2addr p1, v2

    :cond_4
    if-eqz v11, :cond_5

    const/16 v2, 0xf

    invoke-static {v12, v2}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeader:Ljava/lang/String;

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr p1, v2

    :cond_5
    if-nez v10, :cond_6

    if-eqz v11, :cond_7

    :cond_6
    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v2, v2, 0x4

    add-int/2addr p1, v2

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int v3, v3, p4

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    add-int/2addr v4, p1

    move/from16 v0, p4

    invoke-virtual {v2, v0, p1, v3, v4}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setRect(IIII)V

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    sub-int v4, p5, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->cornerIconPadding:I

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    sub-int/2addr v2, v3

    sub-int/2addr v4, v2

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    if-nez v3, :cond_18

    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mRelativeTime:Ljava/lang/String;

    :goto_3
    const/16 v5, 0xd

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    const/4 v7, 0x1

    invoke-static {v5, v6, v4, v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorNameLayout:Landroid/text/StaticLayout;

    const/16 v5, 0xf

    invoke-static {v2, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    const/4 v5, 0x1

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    iget-object v7, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareName:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_19

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    :goto_4
    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;IILandroid/text/Layout$Alignment;Landroid/text/TextUtils$TruncateAt;)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorNameLayout:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/2addr v2, p1

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p1, v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v10

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotation:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotation:Ljava/lang/String;

    const/16 v4, 0xa

    invoke-static {v10, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v5, p5

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr p1, v2

    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_b

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p1, v2

    :cond_b
    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v11, p1, v2

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    const/16 v4, 0xa

    invoke-static {v10, v4}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v5, p5

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int p1, v11, v2

    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitle:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_f

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_e

    :cond_d
    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr p1, v2

    :cond_e
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->hasHero()Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitle:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->cardWithHeroTitleMaxLines:I

    move/from16 v0, p5

    invoke-static {v2, v3, v0, v4}, Lcom/google/android/apps/plus/util/TextPaintUtils;->createConstrainedStaticLayout(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    :goto_5
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr p1, v2

    :cond_f
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v2, :cond_11

    :cond_10
    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p1, v2

    :cond_11
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocation:Ljava/lang/CharSequence;

    if-eqz v2, :cond_12

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v10, p1, v2

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocation:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xf

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int v5, p5, v5

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationLayout:Landroid/text/StaticLayout;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v2, v3

    add-int p1, v10, v2

    :cond_12
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->hasHero()Z

    move-result v2

    if-eqz v2, :cond_13

    move/from16 v0, p3

    invoke-virtual {p0, p2, p1, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->createHero(III)I

    move-result p1

    :cond_13
    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTopBackground:Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;

    iput p1, v2, Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;->height:I

    iput p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneBarTop:I

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->createPlusOneBar(III)I

    move-result p1

    iput p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentTop:I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v2

    if-eqz v2, :cond_14

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v3, v4, p1, p0}, Lcom/google/android/apps/plus/views/StreamCommentView;->init(Lcom/google/android/apps/plus/content/DbStreamComments;ILcom/google/android/apps/plus/views/StreamCommentView$OnCommentDisplayedListener;)I

    move-result p1

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeView(Landroid/view/View;)V

    const/4 v3, 0x1

    if-le v2, v3, :cond_14

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addView(Landroid/view/View;)V

    :cond_14
    return p1

    :cond_15
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto/16 :goto_0

    :cond_16
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_1

    :cond_17
    const/4 v2, 0x0

    move v11, v2

    goto/16 :goto_2

    :cond_18
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/google/android/apps/plus/R$string;->stream_square_and_time_template:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareName:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mRelativeTime:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_3

    :cond_19
    sget-object v7, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    goto/16 :goto_4

    :cond_1a
    new-instance v2, Landroid/text/StaticLayout;

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitle:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const/16 v5, 0xa

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/TextFactory;->getTextPaint(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v7, 0x3f800000

    const/4 v8, 0x0

    const/4 v9, 0x0

    move/from16 v5, p5

    invoke-direct/range {v2 .. v9}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    goto/16 :goto_5
.end method

.method protected createHero(III)I
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I

    return p2
.end method

.method protected final drawCard(Landroid/graphics/Canvas;IIIII)I
    .locals 9
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, p3, v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->cornerIconPadding:I

    sub-int/2addr v0, v1

    add-int/2addr v0, p2

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->cornerIconPadding:I

    add-int/2addr v1, p6

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamWarningBitmap:Landroid/graphics/Bitmap;

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamXPadding:I

    add-int/2addr v2, p2

    int-to-float v2, v2

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamWarningBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int v3, v0, v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p6

    int-to-float v3, v3

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamWarningBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    add-int/2addr v1, p2

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->graySpamXPadding:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p6

    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableRect;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v0, v1

    add-int/2addr p6, v0

    :cond_1
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p6, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    move v6, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    move v8, v0

    :goto_1
    if-nez v6, :cond_2

    if-eqz v8, :cond_9

    :cond_2
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, p6

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_3

    int-to-float v1, p4

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p4

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, p5, v1

    add-int/2addr v1, p4

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    invoke-static {p6, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    int-to-float v1, v1

    int-to-float v2, v2

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_4
    if-eqz v6, :cond_5

    if-eqz v8, :cond_5

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->mediaMetaDataPadding:I

    add-int/2addr v0, v1

    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    if-eqz v1, :cond_6

    int-to-float v1, p4

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p4

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    if-nez v6, :cond_7

    if-eqz v8, :cond_29

    :cond_7
    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    move v7, v0

    :goto_2
    int-to-float v1, p4

    int-to-float v2, v7

    add-int v0, p4, p5

    int-to-float v3, v0

    int-to-float v4, v7

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    if-nez v6, :cond_8

    if-eqz v8, :cond_28

    :cond_8
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v0, v0, 0x2

    add-int p6, v7, v0

    :cond_9
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableAvatar;->isClicked()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->drawSelectionRect(Landroid/graphics/Canvas;)V

    :cond_a
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, v2

    add-int/2addr v0, p4

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorNameLayout:Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int v2, p6, v1

    int-to-float v3, v0

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorNameLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v3, v0

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorNameLayout:Landroid/text/StaticLayout;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/text/StaticLayout;->getLineBaseline(I)I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr v3, v4

    int-to-float v4, v0

    add-int v5, v2, v3

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, v0

    int-to-float v0, v0

    neg-int v4, v2

    sub-int/2addr v4, v3

    int-to-float v4, v4

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr v0, v3

    sub-int v1, v2, v1

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->avatarSize:I

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v1

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int p6, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_b

    int-to-float v0, p4

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p4

    int-to-float v0, v0

    neg-int v1, p6

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p6, v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p6, v0

    :cond_c
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v6, p6, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    neg-int v1, v1

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    neg-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getWidth()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v3}, Landroid/text/StaticLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_d
    int-to-float v0, p4

    int-to-float v1, v6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->reshareBackgroundPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->hasHero()Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v0

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_e
    neg-int v0, p4

    int-to-float v0, v0

    neg-int v1, v6

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int p6, v6, v0

    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_11

    :cond_10
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->contentYPadding:I

    add-int/2addr p6, v0

    :cond_11
    int-to-float v0, p4

    int-to-float v1, p6

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v0, p4

    int-to-float v0, v0

    neg-int v1, p6

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    move-result v0

    add-int/2addr p6, v0

    :cond_12
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_14

    :cond_13
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr p6, v0

    :cond_14
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationLayout:Landroid/text/StaticLayout;

    if-eqz v0, :cond_17

    int-to-float v1, p4

    int-to-float v2, p6

    add-int v0, p4, p5

    int-to-float v3, v0

    int-to-float v4, p6

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->separatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, p6

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_15

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconYPaddingLocation:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    int-to-float v3, p4

    int-to-float v1, v1

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconXPadding:I

    add-int/2addr v1, v2

    add-int/2addr p4, v1

    :cond_15
    int-to-float v1, p4

    int-to-float v2, v0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    neg-int v1, p4

    int-to-float v1, v1

    neg-int v2, v0

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationIconXPadding:I

    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v1, v2

    add-int p6, v0, v1

    :cond_17
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->hasHero()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p0, p1, p2, p6, p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->drawHero(Landroid/graphics/Canvas;III)I

    move-result p6

    :cond_18
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int v2, p6, v0

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mInvisiblePlusOneButton:Z

    if-nez v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_19
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_1a
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    :cond_1b
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Rect;->left:I

    iget v5, v1, Landroid/graphics/Rect;->top:I

    iget v6, v1, Landroid/graphics/Rect;->left:I

    sget-object v7, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    add-int/2addr v6, v7

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sget-object v7, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    add-int/2addr v1, v7

    invoke-virtual {v3, v4, v5, v6, v1}, Landroid/graphics/Rect;->set(IIII)V

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_6
    if-ltz v1, :cond_22

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    if-nez v0, :cond_21

    const/4 v0, 0x0

    :goto_7
    if-nez v0, :cond_1c

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commentAuthorBitmap:Landroid/graphics/Bitmap;

    :cond_1c
    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    neg-int v4, v4

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSpacing:I

    sub-int/2addr v4, v5

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->drawRect:Landroid/graphics/Rect;

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->resizePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_6

    :cond_1d
    const/4 v0, 0x0

    move v6, v0

    goto/16 :goto_0

    :cond_1e
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_1

    :cond_1f
    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->authorBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_4

    :cond_20
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableButton;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_5

    :cond_21
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/google/android/apps/plus/service/Resource;->getResource()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_7

    :cond_22
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, v1

    :goto_8
    if-nez v0, :cond_23

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v1, :cond_23

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, v1

    :cond_23
    if-nez v0, :cond_24

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v1, :cond_24

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v0, v1

    :cond_24
    add-int p6, v2, v0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v0

    if-nez v0, :cond_27

    :cond_25
    :goto_9
    return p6

    :cond_26
    const/4 v0, 0x0

    goto :goto_8

    :cond_27
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamCommentView;->getHeight()I

    move-result v0

    add-int/2addr p6, v0

    goto :goto_9

    :cond_28
    move p6, v7

    goto/16 :goto_3

    :cond_29
    move v7, v0

    goto/16 :goto_2
.end method

.method protected drawHero(Landroid/graphics/Canvas;III)I
    .locals 0
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return p3
.end method

.method protected final getBackgroundPadding()Landroid/graphics/Rect;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->streamCardBorderPadding:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected final getBackgrounds()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/plus/views/StreamCardViewGroup$Background;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mBackgrounds:Ljava/util/List;

    return-object v0
.end method

.method public getOneUpIntent(Z)Landroid/content/Intent;
    .locals 5
    .param p1    # Z

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/service/EsService;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventOwnerId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventOwnerId:Ljava/lang/String;

    invoke-static {v1, v0, v3, v4, v2}, Lcom/google/android/apps/plus/phone/Intents;->getHostedEventIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    invoke-static {v1, v0, v3}, Lcom/google/android/apps/plus/phone/Intents;->getStreamOneUpActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "square_id"

    iget-object v4, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareId:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string v3, "show_keyboard"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public final getPlusOneButtonAnimationCopies()Landroid/util/Pair;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/apps/plus/views/ClickableButton;",
            "Lcom/google/android/apps/plus/views/ClickableButton;",
            ">;"
        }
    .end annotation

    const/4 v14, 0x0

    const/4 v13, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    invoke-virtual {v0, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    aget v0, v0, v13

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/plus/R$dimen;->host_action_bar_height:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    add-int v12, v0, v1

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getLocationOnScreen([I)V

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    aget v1, v0, v13

    sub-int/2addr v1, v12

    aput v1, v0, v13

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    aget v1, v1, v14

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    aget v2, v2, v13

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ClickableButton;->createAbsoluteCoordinatesCopy(II)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v0, :cond_1

    move v9, v13

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$string;->stream_plus_one_count_with_plus:I

    new-array v2, v13, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v14

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v3, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedTextPaint:Landroid/text/TextPaint;

    sget-object v4, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v5, Lcom/google/android/apps/plus/util/PlusBarUtils;->sPlusOnedPressedDrawable:Landroid/graphics/drawable/NinePatchDrawable;

    iget v7, v6, Landroid/graphics/Rect;->left:I

    iget v8, v6, Landroid/graphics/Rect;->top:I

    move-object v6, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/plus/views/ClickableButton;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lcom/google/android/apps/plus/views/ClickableButton$ClickableButtonListener;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    aget v1, v1, v14

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->screenCoordinates:[I

    aget v2, v2, v13

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/plus/views/ClickableButton;->createAbsoluteCoordinatesCopy(II)Lcom/google/android/apps/plus/views/ClickableButton;

    move-result-object v11

    new-instance v0, Landroid/util/Pair;

    invoke-direct {v0, v10, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v0

    add-int/lit8 v9, v0, 0x1

    goto :goto_0
.end method

.method public final hasComments()Z
    .locals 1

    iget v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hasHero()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final initCard(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 32
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v25

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    const/16 v2, 0x25

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorGaiaId:Ljava/lang/String;

    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    if-nez v2, :cond_0

    const-string v2, ""

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    :cond_0
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/EsAvatarData;->uncompressAvatarUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorAvatarUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorGaiaId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorAvatarUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x2

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/plus/views/ClickableAvatar;-><init>(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->addClickableItem(Lcom/google/android/apps/plus/views/ClickableItem;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/plus/util/Dates;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mRelativeTime:Ljava/lang/String;

    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v11

    const-wide/16 v2, 0x2

    and-long/2addr v2, v11

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotation:Ljava/lang/String;

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocation:Ljava/lang/CharSequence;

    if-nez v2, :cond_3

    const-wide/16 v2, 0x8

    and-long/2addr v2, v11

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v19

    if-eqz v19, :cond_3

    invoke-static/range {v19 .. v19}, Lcom/google/android/apps/plus/content/DbLocation;->deserialize([B)Lcom/google/android/apps/plus/content/DbLocation;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/content/DbLocation;->getLocationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocation:Ljava/lang/CharSequence;

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->locationBitmaps:[Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    :cond_3
    const/16 v2, 0x1c

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v27

    if-eqz v27, :cond_c

    invoke-static/range {v27 .. v27}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->deserialize([B)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOriginalSquareName:Ljava/lang/String;

    :goto_0
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v21}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOriginalSquareName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_d

    new-instance v2, Landroid/text/SpannableStringBuilder;

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_original_author:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v22, v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v29

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    move/from16 v0, v29

    if-ne v0, v2, :cond_4

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->length()I

    move-result v2

    add-int v15, v29, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->boldSpan:Landroid/text/style/StyleSpan;

    const/16 v4, 0x21

    move/from16 v0, v29

    invoke-virtual {v2, v3, v0, v15, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    :goto_2
    const-wide/16 v2, 0x1

    and-long/2addr v2, v11

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitle:Ljava/lang/String;

    :cond_5
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    const/16 v2, 0x17

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    if-eqz v10, :cond_f

    invoke-static {v10}, Lcom/google/android/apps/plus/content/DbStreamComments;->deserialize([B)Lcom/google/android/apps/plus/content/DbStreamComments;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    :goto_3
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v23

    if-eqz v23, :cond_10

    invoke-static/range {v23 .. v23}, Lcom/google/android/apps/plus/content/DbPlusOneData;->deserialize([B)Lcom/google/android/apps/plus/content/DbPlusOneData;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    :goto_4
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_11

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanReshare:Z

    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    :goto_6
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanPlusOne:Z

    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanComment:Z

    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_14

    const/4 v2, 0x1

    :goto_8
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsLimited:Z

    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_15

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerHasRead:Z

    const-wide/32 v2, 0x8000

    and-long/2addr v2, v11

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    :goto_a
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsSquarePost:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerIsSquareAdmin:Z

    if-eqz v2, :cond_17

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsSquarePost:Z

    if-eqz v2, :cond_17

    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_17

    const/4 v2, 0x1

    :goto_b
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsGraySpam:Z

    const/16 v2, 0x1b

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v28

    if-eqz v28, :cond_6

    invoke-static/range {v28 .. v28}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->deserialize([B)Lcom/google/android/apps/plus/content/DbSquareUpdate;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareId:Ljava/lang/String;

    invoke-virtual {v14}, Lcom/google/android/apps/plus/content/DbSquareUpdate;->getSquareName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareName:Ljava/lang/String;

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerHasRead:Z

    if-eqz v2, :cond_7

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewedListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;

    :cond_7
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    if-eqz v17, :cond_8

    invoke-static {}, Lcom/google/api/services/plusi/model/PlusEventJson;->getInstance()Lcom/google/api/services/plusi/model/PlusEventJson;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/api/services/plusi/model/PlusEventJson;->fromByteArray([B)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/api/services/plusi/model/PlusEvent;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventId:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/api/services/plusi/model/PlusEvent;->creatorObfuscatedId:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventOwnerId:Ljava/lang/String;

    :cond_8
    invoke-virtual/range {p0 .. p3}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V

    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_18

    const/16 v31, 0x1

    :goto_c
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareMode:Z

    if-nez v2, :cond_19

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsSquarePost:Z

    if-eqz v2, :cond_19

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->squaresBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    :goto_d
    if-eqz v31, :cond_9

    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/plus/content/DbWhatsHot;->deserialize([B)Lcom/google/android/apps/plus/content/DbWhatsHot;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/plus/content/DbWhatsHot;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    sget v2, Lcom/google/android/apps/plus/R$string;->stream_whats_hot:I

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeader:Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeader:Ljava/lang/String;

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->whatsHotHeaderBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    :cond_9
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeader:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeader:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mRelativeTime:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotation:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitle:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/AccessibilityUtils;->appendAndSeparateIfNotEmpty(Ljava/lang/StringBuilder;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    if-lez v2, :cond_a

    sget v2, Lcom/google/android/apps/plus/R$plurals;->comments:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v2, :cond_1b

    const/16 v24, 0x0

    :goto_e
    if-lez v24, :cond_b

    sget v2, Lcom/google/android/apps/plus/R$plurals;->plus_one_accessibility_description:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, v25

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v26

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_c
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOriginalSquareName:Ljava/lang/String;

    goto/16 :goto_0

    :cond_d
    new-instance v2, Landroid/text/SpannableStringBuilder;

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_original_author_community:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v22, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOriginalSquareName:Ljava/lang/String;

    aput-object v6, v4, v5

    move-object/from16 v0, v25

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    goto/16 :goto_1

    :cond_e
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    goto/16 :goto_2

    :cond_f
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    goto/16 :goto_3

    :cond_10
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    goto/16 :goto_4

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_6

    :cond_13
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_a

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_18
    const/16 v31, 0x0

    goto/16 :goto_c

    :cond_19
    if-eqz v31, :cond_1a

    sget-object v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget-object v2, v2, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->whatsHotBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_d

    :cond_1a
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    goto/16 :goto_d

    :cond_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbPlusOneData;->getCount()I

    move-result v24

    goto/16 :goto_e
.end method

.method protected initHero(Landroid/database/Cursor;Lcom/google/android/apps/plus/util/StreamLayoutInfo;I)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/google/android/apps/plus/util/StreamLayoutInfo;
    .param p3    # I

    return-void
.end method

.method protected final isVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final layoutCard$54d8973f()V
    .locals 10

    const/high16 v9, 0x40000000

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v3

    if-lez v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWidthDimension:I

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sub-int/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->rightBorderPadding:I

    sub-int/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v2, v3, v4

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mHeightDimension:I

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/plus/views/StreamCommentView;->measure(II)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentTop:I

    sget-object v6, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v6, v6, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sget-object v7, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v7, v7, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v6, v7

    add-int/2addr v6, v2

    iget v7, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentTop:I

    iget-object v8, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v8}, Lcom/google/android/apps/plus/views/StreamCommentView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/google/android/apps/plus/views/StreamCommentView;->layout(IIII)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/content/DbStreamComments;->getNumComments()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterIndicatorHeight:I

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v0

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    sub-int/2addr v3, v4

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSpacing:I

    sub-int v1, v3, v4

    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v7, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    :cond_0
    return-void
.end method

.method public onClickableButtonListenerClick(Lcom/google/android/apps/plus/views/ClickableButton;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/ClickableButton;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-interface {v0, v1, v2, p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;->onPlusOneClicked(Ljava/lang/String;Lcom/google/android/apps/plus/content/DbPlusOneData;Lcom/google/android/apps/plus/views/UpdateCardViewGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsLimited:Z

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;->onReshareClicked(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    invoke-interface {v0, p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;->onCommentsClicked$40e28b52(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;)V

    goto :goto_0
.end method

.method public onClickableRectClick(Lcom/google/android/apps/plus/views/ClickableRect;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/plus/views/ClickableRect;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;->onGraySpamBarClicked(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final onCommentDisplayed(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-static {v2}, Lcom/google/android/apps/plus/util/ViewUtils;->isViewAttached(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/views/ClickableButton;->getRect()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v3, p2, p1

    sget-object v4, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v4, v4, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSize:I

    sget-object v5, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v5, v5, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->commenterAvatarSpacing:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    sub-int v1, v2, v3

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    int-to-float v3, v1

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0xfa

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sAccelerateDecelerateInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseWithLayer()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->clearAnimation()V

    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->onDetachedFromWindow()V

    return-void
.end method

.method public onRecycle()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->onRecycle()V

    iput v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneBarTop:I

    iput v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentTop:I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mActivityId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCreationSourceId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorGaiaId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorAvatarUrl:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorNameLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mRelativeTime:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOriginalSquareName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareNameAndRelativeTimeLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotation:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAnnotationLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttribution:Landroid/text/SpannableStringBuilder;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAttributionLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitle:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTitleLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeader:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotSubHeaderLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeader:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWhatsHotHeaderIcon:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCornerIcon:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocation:Ljava/lang/CharSequence;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationIcon:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mLocationLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mEventOwnerId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentsButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mReshareButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButton:Lcom/google/android/apps/plus/views/ClickableButton;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mInvisiblePlusOneButton:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOverridePlusOnedButtonDisplay:Z

    iput v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mTotalComments:I

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentData:Lcom/google/android/apps/plus/content/DbStreamComments;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewedListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mOnClickListener:Landroid/view/View$OnClickListener;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanReshare:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanPlusOne:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCanComment:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsLimited:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerHasRead:Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamLayout:Landroid/text/StaticLayout;

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamClickableRect:Lcom/google/android/apps/plus/views/ClickableRect;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsSquarePost:Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareId:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsGraySpam:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareMode:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerIsSquareAdmin:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommentView:Lcom/google/android/apps/plus/views/StreamCommentView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/StreamCommentView;->onRecycle()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterIndicatorView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->clearAnimation()V

    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setTranslationY(F)V

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->setRotationX(F)V

    :cond_3
    return-void
.end method

.method public final pokeGraySpamHeader()V
    .locals 4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsApprovedGraySpam:Z

    sget-object v0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v0, v0, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->topBorderPadding:I

    iget v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mWidthDimension:I

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sub-int/2addr v2, v3

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->rightBorderPadding:I

    sub-int/2addr v2, v3

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->createGraySpamBar(III)I

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->invalidate()V

    return-void
.end method

.method public final setAppLinkClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAppLinkClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$LinkClickListener;

    return-object p0
.end method

.method public final setAvatarClickListener(Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->setClickListener(Lcom/google/android/apps/plus/views/ClickableAvatar$AvatarClickListener;)V

    :cond_0
    return-object p0
.end method

.method public setGraySpamApproved(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mIsApprovedGraySpam:Z

    return-void
.end method

.method public final setGraySpamBarClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mGraySpamBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$GraySpamBarClickListener;

    return-object p0
.end method

.method public final setMediaClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamMediaClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamMediaClickListener;

    return-object p0
.end method

.method public final setPlusBarClickListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .locals 0
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    iput-object p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mStreamPlusBarClickListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$StreamPlusBarClickListener;

    return-object p0
.end method

.method public setSquareMode(ZZ)V
    .locals 0
    .param p1    # Z
    .param p2    # Z

    iput-boolean p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSquareMode:Z

    iput-boolean p2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerIsSquareAdmin:Z

    return-void
.end method

.method public final setViewedListener(Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;)Lcom/google/android/apps/plus/views/UpdateCardViewGroup;
    .locals 1
    .param p1    # Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewerHasRead:Z

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mViewedListener:Lcom/google/android/apps/plus/views/UpdateCardViewGroup$ViewedListener;

    :cond_0
    return-object p0
.end method

.method public final startDelayedShakeAnimation()V
    .locals 8

    const-wide/16 v6, 0x267

    const v5, 0x3f733333

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mInvisiblePlusOneButton:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->invalidate()V

    iget v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mSpan:I

    const/4 v3, 0x2

    if-lt v2, v3, :cond_0

    const/high16 v0, -0x40400000

    const/high16 v1, -0x40000000

    :goto_0
    invoke-static {}, Lcom/google/android/apps/plus/util/AnimationUtils;->canUseViewPropertyAnimator()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v3, 0x12c

    invoke-virtual {v2, v3, v4}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->rotationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/ViewPropertyAnimator;->rotationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sDampingInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    :goto_1
    return-void

    :cond_0
    const/high16 v0, -0x40000000

    const/high16 v1, -0x3fe00000

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup$2;-><init>(Lcom/google/android/apps/plus/views/UpdateCardViewGroup;FF)V

    iput-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mShakeAnimation:Ljava/lang/Runnable;

    invoke-virtual {p0, v2, v6, v7}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method public final togglePlusOne()V
    .locals 4

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-direct {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneData:Lcom/google/android/apps/plus/content/DbPlusOneData;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->isPlusOnedByMe()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Lcom/google/android/apps/plus/content/DbPlusOneData;->updatePlusOnedByMe(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->getAvailableWidth()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v1, v1, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->leftBorderPadding:I

    sget-object v3, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->sStaticData:Lcom/google/android/apps/plus/util/StreamCardViewGroupData;

    iget v3, v3, Lcom/google/android/apps/plus/util/StreamCardViewGroupData;->defaultPadding:I

    add-int/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mPlusOneBarTop:I

    invoke-direct {p0, v1, v3, v0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->createPlusOneBar(III)I

    iput-boolean v2, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mInvisiblePlusOneButton:Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->invalidate()V

    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public unbindResources()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mAuthorImage:Lcom/google/android/apps/plus/views/ClickableAvatar;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/ClickableAvatar;->unbindResources()V

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    :goto_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    aget-object v1, v1, v0

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/service/Resource;->unregister(Lcom/google/android/apps/plus/service/ResourceConsumer;)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/views/UpdateCardViewGroup;->mCommenterAvatars:[Lcom/google/android/apps/plus/service/Resource;

    :cond_2
    invoke-super {p0}, Lcom/google/android/apps/plus/views/StreamCardViewGroup;->unbindResources()V

    return-void
.end method
