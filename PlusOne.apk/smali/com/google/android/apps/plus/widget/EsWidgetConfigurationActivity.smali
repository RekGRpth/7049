.class public Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "EsWidgetConfigurationActivity.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;,
        Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$WidgetCircleQuery;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/FragmentActivity;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

.field private final mAdapterLock:Ljava/lang/Object;

.field private mAppWidgetId:I

.field private mDisplayingEmptyView:Z

.field private mListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapterLock:Ljava/lang/Object;

    return-void
.end method

.method private updateDisplay()V
    .locals 5

    const v4, 0x1020004

    const/4 v3, 0x0

    const/16 v2, 0x8

    iget-boolean v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mDisplayingEmptyView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_text:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/apps/plus/R$id;->list_empty_progress:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v5, "appWidgetId"

    invoke-virtual {v1, v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    :goto_0
    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    if-nez v5, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    iput v8, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    goto :goto_0

    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    if-nez v5, :cond_2

    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v5, v7, v7}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->saveCircleInfo(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v5

    iget v6, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v5, v6}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureWidget(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "appWidgetId"

    iget v6, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v5, -0x1

    invoke-virtual {p0, v5, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    :cond_2
    sget v5, Lcom/google/android/apps/plus/R$layout;->widget_configuration_activity:I

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setContentView(I)V

    iput-boolean v9, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mDisplayingEmptyView:Z

    const v5, 0x102000a

    invoke-virtual {p0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ListView;

    iput-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    sget v6, Lcom/google/android/apps/plus/R$layout;->widget_configuration_entry:I

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v5, Lcom/google/android/apps/plus/R$id;->circle_icon:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    sget v5, Lcom/google/android/apps/plus/R$id;->circle_name:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v5, Lcom/google/android/apps/plus/R$string;->widget_all_circles:I

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(I)V

    sget v5, Lcom/google/android/apps/plus/R$id;->circle_member_count:I

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5, v2, v7, v9}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    new-instance v5, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-direct {v5, p0, p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;-><init>(Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;Landroid/content/Context;)V

    iput-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    iget-object v6, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->updateDisplay()V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v5

    invoke-virtual {v5, v8, v7, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto/16 :goto_1
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/fragments/CircleListLoader;

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    const/4 v2, 0x4

    sget-object v3, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$WidgetCircleQuery;->PROJECTION:[Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/plus/fragments/CircleListLoader;-><init>(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v4

    sub-int/2addr p3, v4

    if-gez p3, :cond_0

    const-string v0, "v.all.circles"

    sget v4, Lcom/google/android/apps/plus/R$string;->stream_circles:I

    invoke-virtual {p0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    iget v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v4, v0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->saveCircleInfo(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-static {p0, v4, v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureWidget(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v4, "appWidgetId"

    iget v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAppWidgetId:I

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v4, -0x1

    invoke-virtual {p0, v4, v3}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapterLock:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v4}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v2}, Landroid/database/Cursor;->isClosed()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-gt v4, p3, :cond_2

    :cond_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    :cond_2
    :try_start_1
    invoke-interface {v2, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x2

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    monitor-exit v5

    goto :goto_0

    :cond_3
    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Landroid/support/v4/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapterLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mAdapter:Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity$CirclesCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->mDisplayingEmptyView:Z

    invoke-direct {p0}, Lcom/google/android/apps/plus/widget/EsWidgetConfigurationActivity;->updateDisplay()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method
