.class public Lcom/google/android/apps/plus/widget/EsWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "EsWidgetProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V
    .locals 13
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Landroid/widget/RemoteViews;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    invoke-static {p0, p1}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v2

    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p0, v11, v2, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    sget v11, Lcom/google/android/apps/plus/R$id;->home_icon:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    if-eqz p1, :cond_4

    const/4 v10, 0x1

    :goto_0
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_5

    const/4 v9, 0x1

    :goto_1
    sget v11, Lcom/google/android/apps/plus/R$id;->refresh_progress:I

    const/16 v12, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v11, Lcom/google/android/apps/plus/R$id;->next_progress:I

    const/16 v12, 0x8

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v12}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v12, Lcom/google/android/apps/plus/R$id;->post_icon:I

    if-eqz v10, :cond_6

    const/4 v11, 0x0

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v12, Lcom/google/android/apps/plus/R$id;->refresh_icon:I

    if-eqz p5, :cond_7

    const/4 v11, 0x0

    :goto_3
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v12, Lcom/google/android/apps/plus/R$id;->next_icon:I

    if-eqz v9, :cond_8

    const/4 v11, 0x0

    :goto_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v12, Lcom/google/android/apps/plus/R$id;->divider_1:I

    if-eqz v10, :cond_9

    if-nez p5, :cond_0

    if-eqz v9, :cond_9

    :cond_0
    const/4 v11, 0x0

    :goto_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v12, Lcom/google/android/apps/plus/R$id;->divider_2:I

    if-eqz p5, :cond_a

    if-eqz v9, :cond_a

    const/4 v11, 0x0

    :goto_6
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    if-eqz v10, :cond_1

    const/4 v11, 0x0

    invoke-static {p0, p1, v11}, Lcom/google/android/apps/plus/phone/Intents;->getPostActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Lcom/google/android/apps/plus/api/MediaRef;)Landroid/content/Intent;

    move-result-object v5

    const-string v11, "com.google.android.apps.plus.widget.POST_ACTION"

    invoke-virtual {v5, v11}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p0, v11, v5, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    sget v11, Lcom/google/android/apps/plus/R$id;->post_icon:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_1
    if-eqz p5, :cond_2

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-static {p0, p2, v11, v12}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p0, v11, v8, v12}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v7

    sget v11, Lcom/google/android/apps/plus/R$id;->refresh_icon:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_2
    if-eqz v9, :cond_3

    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-static {p0, p2, v0, v11}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const/4 v11, 0x0

    const/high16 v12, 0x8000000

    invoke-static {p0, v11, v3, v12}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    sget v11, Lcom/google/android/apps/plus/R$id;->next_icon:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v11, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :cond_3
    return-void

    :cond_4
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_6
    const/16 v11, 0x8

    goto/16 :goto_2

    :cond_7
    const/16 v11, 0x8

    goto :goto_3

    :cond_8
    const/16 v11, 0x8

    goto :goto_4

    :cond_9
    const/16 v11, 0x8

    goto :goto_5

    :cond_a
    const/16 v11, 0x8

    goto :goto_6
.end method

.method public static configureWidget(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I

    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] configureWidget"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p1, :cond_1

    invoke-static {p0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showTapToConfigure(Landroid/content/Context;I)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showLoadingView(Landroid/content/Context;I)V

    const/4 v0, 0x0

    invoke-static {p0, p2, v0}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0
.end method

.method private static getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v2, 0x1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/apps/plus/widget/EsWidgetService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "activity_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    if-eqz p3, :cond_1

    const-string v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_1
    invoke-virtual {v0, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    return-object v0
.end method

.method public static showLoadingView(Landroid/content/Context;I)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/4 v1, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x0

    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$layout;->widget_layout:I

    invoke-direct {v3, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    move-object v0, p0

    move v2, p1

    move-object v4, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->next_progress:I

    invoke-virtual {v3, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_image_layout:I

    invoke-virtual {v3, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_text_layout:I

    invoke-virtual {v3, v0, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_empty_layout:I

    invoke-virtual {v3, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->empty_view:I

    sget v1, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method public static showNoPostsFound(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    .locals 11
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I

    const/16 v10, 0x8

    const/4 v9, 0x0

    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] showNoPostsFound"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/apps/plus/R$layout;->widget_layout:I

    invoke-direct {v3, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_image_layout:I

    invoke-virtual {v3, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_text_layout:I

    invoke-virtual {v3, v0, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_empty_layout:I

    invoke-virtual {v3, v0, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->empty_view:I

    sget v1, Lcom/google/android/apps/plus/R$string;->no_posts:I

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {p0, p2}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v6, "v.all.circles"

    :cond_1
    invoke-static {p0, p1, v6}, Lcom/google/android/apps/plus/phone/Intents;->getCirclePostsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "com.google.android.apps.plus.widget.CIRCLE_ACTION"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x8000000

    invoke-static {p0, v9, v7, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_main:I

    invoke-virtual {v3, v0, v8}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p2, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method public static showProgressIndicator(Landroid/content/Context;IZ)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Z

    const/16 v8, 0x8

    const/4 v5, 0x0

    const-string v4, "EsWidget"

    const/4 v6, 0x3

    invoke-static {v4, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] showProgressIndicator"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/plus/R$layout;->widget_layout:I

    invoke-direct {v3, v4, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sget v4, Lcom/google/android/apps/plus/R$id;->empty_view:I

    sget v6, Lcom/google/android/apps/plus/R$string;->loading:I

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    if-eqz p2, :cond_1

    sget v4, Lcom/google/android/apps/plus/R$id;->refresh_icon:I

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v4, Lcom/google/android/apps/plus/R$id;->refresh_progress:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/google/android/apps/plus/widget/EsWidgetService;

    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    sget v4, Lcom/google/android/apps/plus/R$id;->next_icon:I

    invoke-virtual {v3, v4, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    :goto_0
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v4, v6, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->partiallyUpdateAppWidget(ILandroid/widget/RemoteViews;)V

    :goto_2
    return-void

    :cond_1
    sget v4, Lcom/google/android/apps/plus/R$id;->next_icon:I

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v4, Lcom/google/android/apps/plus/R$id;->next_progress:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    sget v4, Lcom/google/android/apps/plus/R$id;->widget_empty_layout:I

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v4, Lcom/google/android/apps/plus/R$id;->widget_image_layout:I

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v4, Lcom/google/android/apps/plus/R$id;->widget_text_layout:I

    invoke-virtual {v3, v4, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_2
.end method

.method public static showTapToConfigure(Landroid/content/Context;I)V
    .locals 9
    .param p0    # Landroid/content/Context;
    .param p1    # I

    const/16 v8, 0x8

    const/4 v1, 0x0

    const/4 v5, 0x0

    const-string v0, "EsWidget"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "["

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "] showTapToConfigure"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v3, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$layout;->widget_layout:I

    invoke-direct {v3, v0, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    move-object v0, p0

    move v2, p1

    move-object v4, v1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_image_layout:I

    invoke-virtual {v3, v0, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_text_layout:I

    invoke-virtual {v3, v0, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_empty_layout:I

    invoke-virtual {v3, v0, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v0, Lcom/google/android/apps/plus/R$id;->empty_view:I

    sget v2, Lcom/google/android/apps/plus/R$string;->widget_tap_to_configure:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v0, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/phone/Intents;->getRootIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v7

    const/high16 v0, 0x8000000

    invoke-static {p0, v5, v7, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    sget v0, Lcom/google/android/apps/plus/R$id;->widget_main:I

    invoke-virtual {v3, v0, v6}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    return-void
.end method

.method public static updateWidget(Landroid/content/Context;ILjava/lang/String;)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, p1, v1, v2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->getWidgetUpdateIntent(Landroid/content/Context;ILjava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # [I

    move-object v1, p2

    array-length v3, p2

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget v0, v1, v2

    const-string v4, "EsWidget"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] onDeleted"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v4, "com.google.android.apps.plus.widget.EsWidgetUtils"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "circleId_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "circleName_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x9

    if-ge v5, v6, :cond_1

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;
    .param p3    # [I

    move-object v1, p3

    array-length v3, p3

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget v0, v1, v2

    const-string v4, "EsWidget"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "EsWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] onUpdate"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, v0}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showLoadingView(Landroid/content/Context;I)V

    const/4 v4, 0x0

    invoke-static {p1, v0, v4}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->updateWidget(Landroid/content/Context;ILjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
