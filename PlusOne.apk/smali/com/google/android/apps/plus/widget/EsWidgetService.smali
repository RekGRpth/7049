.class public Lcom/google/android/apps/plus/widget/EsWidgetService;
.super Landroid/app/IntentService;
.source "EsWidgetService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;
    }
.end annotation


# static fields
.field private static final TEXT_ONLY_VIEW_IDS:[I

.field private static mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

.field private static sAuthorBitmap:Landroid/graphics/Bitmap;

.field private static sAutoTextColor:I

.field private static sContentColor:I

.field private static sInitialized:Z

.field public static sWidgetImageFetchSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/plus/R$id;->text_only_content_1:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/plus/R$id;->text_only_content_2:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/plus/R$id;->text_only_content_3:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const-string v0, "EsWidgetService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100()Lcom/google/android/apps/plus/service/ImageResourceManager;
    .locals 1

    sget-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    return-object v0
.end method

.method private createRemoteViews(Lcom/google/android/apps/plus/content/EsAccount;ILandroid/database/Cursor;Z)Landroid/widget/RemoteViews;
    .locals 32
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Landroid/database/Cursor;
    .param p4    # Z

    const-string v3, "EsWidget"

    const/4 v10, 0x3

    invoke-static {v3, v10}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "EsWidget"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "["

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] createRemoteViews: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getPackageName()Ljava/lang/String;

    move-result-object v3

    sget v10, Lcom/google/android/apps/plus/R$layout;->widget_layout:I

    invoke-direct {v4, v3, v10}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->widget_empty_layout:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->image:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->video_overlay:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->link_overlay:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->link_background:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->link_title:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_2

    :cond_1
    :goto_0
    return-object v4

    :cond_2
    const/16 v3, 0x11

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->readMediaContent(Landroid/database/Cursor;)Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->linkTitle:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    const/16 v23, 0x1

    :goto_1
    if-nez v23, :cond_c

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-nez v3, :cond_c

    const/16 v29, 0x1

    :goto_2
    if-eqz v29, :cond_d

    sget v30, Lcom/google/android/apps/plus/R$id;->text_only_user_image:I

    :goto_3
    if-eqz v29, :cond_e

    sget v31, Lcom/google/android/apps/plus/R$id;->text_only_user_name:I

    :goto_4
    if-eqz v29, :cond_f

    sget v28, Lcom/google/android/apps/plus/R$id;->text_only_stream_name:I

    :goto_5
    sget v10, Lcom/google/android/apps/plus/R$id;->widget_image_layout:I

    if-eqz v29, :cond_10

    const/16 v3, 0x8

    :goto_6
    invoke-virtual {v4, v10, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v10, Lcom/google/android/apps/plus/R$id;->widget_text_layout:I

    if-eqz v29, :cond_11

    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v4, v10, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget-object v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAuthorBitmap:Landroid/graphics/Bitmap;

    move/from16 v0, v30

    invoke-virtual {v4, v0, v3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    const/4 v3, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x0

    :try_start_0
    sget-object v3, Lcom/google/android/apps/plus/widget/EsWidgetService;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    const/4 v10, 0x2

    const/4 v11, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v10, v11}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingAvatarByGaiaId(Ljava/lang/String;IZ)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/graphics/Bitmap;

    move-object/from16 v17, v0

    if-eqz v17, :cond_3

    move/from16 v0, v30

    move-object/from16 v1, v17

    invoke-virtual {v4, v0, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_8
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move/from16 v2, v23

    invoke-static {v0, v4, v1, v14, v2}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadImage$1749d7eb(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;Ljava/lang/String;Z)Z

    move-result v22

    if-eqz v23, :cond_5

    if-nez p4, :cond_4

    sget v3, Lcom/google/android/apps/plus/R$id;->link_title:I

    const/4 v10, 0x0

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->link_title:I

    move-object/from16 v0, v24

    iget-object v10, v0, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->linkTitle:Ljava/lang/String;

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    :cond_4
    if-eqz v22, :cond_12

    sget v3, Lcom/google/android/apps/plus/R$id;->link_overlay:I

    const/4 v10, 0x0

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_5
    :goto_9
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v31

    invoke-virtual {v4, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const-string v3, "com.google.android.apps.plus.widget.EsWidgetUtils"

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "circleName_"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-interface {v3, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v3, 0x0

    :cond_6
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_7

    sget v3, Lcom/google/android/apps/plus/R$string;->widget_default_circle_name:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I)Ljava/lang/String;

    move-result-object v3

    :cond_7
    sget v10, Lcom/google/android/apps/plus/R$string;->from_circle:I

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v28

    invoke-virtual {v4, v0, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const/16 v3, 0x12

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v3, 0x13

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v6, 0x0

    const/16 v3, 0x14

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v26

    const/16 v3, 0x15

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static/range {v27 .. v27}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    sget v3, Lcom/google/android/apps/plus/R$string;->stream_original_author:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v27, v10, v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    :cond_8
    if-eqz v29, :cond_13

    move-object/from16 v3, p0

    invoke-static/range {v3 .. v9}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showTextLayoutContent(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    :cond_9
    :goto_a
    const/4 v15, 0x1

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move/from16 v12, p2

    move-object v13, v4

    invoke-static/range {v10 .. v15}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->configureHeaderButtons(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILandroid/widget/RemoteViews;Ljava/lang/String;Z)V

    if-eqz p1, :cond_1

    if-eqz v14, :cond_1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14}, Lcom/google/android/apps/plus/phone/Intents;->getPostCommentsActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v25

    const-string v3, "com.google.android.apps.plus.widget.ACTIVITY_ACTION"

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static/range {p0 .. p1}, Lcom/google/android/apps/plus/phone/Intents;->getHostNavigationActivityIntent(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;)Landroid/content/Intent;

    move-result-object v21

    const/high16 v3, 0x14000000

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x10

    if-ge v3, v10, :cond_a

    move-object/from16 v0, v21

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    :cond_a
    invoke-static/range {p0 .. p0}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    move-object/from16 v0, v18

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    const/4 v3, 0x0

    const/high16 v10, 0x8000000

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v10}, Landroid/support/v4/app/TaskStackBuilder;->getPendingIntent(II)Landroid/app/PendingIntent;

    move-result-object v19

    sget v3, Lcom/google/android/apps/plus/R$id;->widget_main:I

    move-object/from16 v0, v19

    invoke-virtual {v4, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_0

    :cond_b
    const/16 v23, 0x0

    goto/16 :goto_1

    :cond_c
    const/16 v29, 0x0

    goto/16 :goto_2

    :cond_d
    sget v30, Lcom/google/android/apps/plus/R$id;->user_image:I

    goto/16 :goto_3

    :cond_e
    sget v31, Lcom/google/android/apps/plus/R$id;->user_name:I

    goto/16 :goto_4

    :cond_f
    sget v28, Lcom/google/android/apps/plus/R$id;->stream_name:I

    goto/16 :goto_5

    :cond_10
    const/4 v3, 0x0

    goto/16 :goto_6

    :cond_11
    const/16 v3, 0x8

    goto/16 :goto_7

    :catch_0
    move-exception v20

    const-string v3, "EsWidget"

    const-string v10, "Cannot download avatar"

    move-object/from16 v0, v20

    invoke-static {v3, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_8

    :cond_12
    sget v3, Lcom/google/android/apps/plus/R$id;->link_background:I

    const/4 v10, 0x0

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_9

    :cond_13
    sget v3, Lcom/google/android/apps/plus/R$id;->content_line_1:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->content_line_2:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v3, Lcom/google/android/apps/plus/R$id;->content_multiline:I

    const/16 v10, 0x8

    invoke-virtual {v4, v3, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_14

    sget v3, Lcom/google/android/apps/plus/R$id;->content_line_1:I

    sget v10, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {v4, v3, v5, v10}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    sget v3, Lcom/google/android/apps/plus/R$id;->content_line_2:I

    sget v10, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {v4, v3, v6, v10}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_a

    :cond_14
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_15

    sget v3, Lcom/google/android/apps/plus/R$id;->content_line_1:I

    sget v10, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {v4, v3, v6, v10}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    sget v3, Lcom/google/android/apps/plus/R$id;->content_line_2:I

    sget v10, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {v4, v3, v7, v10}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_a

    :cond_15
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_16

    sget v3, Lcom/google/android/apps/plus/R$id;->content_multiline:I

    sget v10, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {v4, v3, v7, v10}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_a

    :cond_16
    move-object/from16 v0, p0

    invoke-static {v0, v8, v9}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getAutoText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_9

    sget v10, Lcom/google/android/apps/plus/R$id;->content_multiline:I

    sget v11, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAutoTextColor:I

    invoke-static {v4, v10, v3, v11}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    goto/16 :goto_a
.end method

.method private static fetchActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)V
    .locals 12
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v4, 0x1

    const-string v0, "EsWidget"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] loadActivities"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v2, 0x0

    const-string v0, "v.whatshot"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    const/4 p3, 0x0

    :cond_1
    :goto_0
    new-instance v10, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;

    invoke-direct {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;-><init>()V

    invoke-virtual {v10, v4}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->setFullSync(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Get activities for widget circleId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " view: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncStart(Ljava/lang/String;)V

    const-string v0, "Activities:SyncStream"

    invoke-virtual {v10, v0}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onStart(Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/16 v8, 0x14

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    :try_start_0
    invoke-static/range {v0 .. v10}, Lcom/google/android/apps/plus/content/EsPostsData;->doActivityStreamSync(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ILcom/google/android/apps/plus/network/HttpOperation$OperationListener;Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;)Lcom/google/android/apps/plus/service/ServiceResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    :goto_1
    return-void

    :cond_2
    const-string v0, "v.all.circles"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p3, 0x0

    goto :goto_0

    :catch_0
    move-exception v11

    :try_start_1
    const-string v0, "EsWidget"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "EsWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "] loadActivities failed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_3
    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onFinish()V

    invoke-virtual {v10}, Lcom/google/android/apps/plus/service/EsSyncAdapterService$SyncState;->onSyncFinish()V

    throw v0
.end method

.method private static getAutoText(Landroid/content/Context;J)Ljava/lang/String;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J

    invoke-static {p1, p2}, Lcom/google/android/apps/plus/content/EsPostsData;->getDefaultText(J)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadCursor(Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v0, "EsWidget"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EsWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] loadCursor"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "v.all.circles"

    invoke-static {p3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-static {v4, v4, v4, v5, v7}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/plus/phone/StreamAdapter$StreamQuery;->PROJECTION_STREAM:[Ljava/lang/String;

    const-string v3, "content_flags&2213!=0 AND content_flags&16=0"

    const-string v5, "sort_index ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    return-object v6

    :cond_2
    const-string v0, "v.whatshot"

    invoke-static {v0, p3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v4, v4, v4, v5, v5}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :cond_3
    invoke-static {v4, p3, v4, v5, v7}, Lcom/google/android/apps/plus/content/EsPostsData;->buildActivitiesStreamKey(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/plus/content/DbLocation;ZI)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/apps/plus/content/EsProvider;->buildStreamUri(Lcom/google/android/apps/plus/content/EsAccount;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private static loadImage$1749d7eb(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;Ljava/lang/String;Z)Z
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;
    .param p3    # Ljava/lang/String;
    .param p4    # Z

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v2, p2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    if-eqz v2, :cond_3

    :try_start_0
    sget-object v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    iget-object v4, p2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    iget v5, p2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->width:I

    iget v6, p2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->height:I

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getBlockingMediaWithCustomSize(Lcom/google/android/apps/plus/api/MediaRef;III)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v1, v0
    :try_end_0
    .catch Lcom/google/android/apps/plus/service/ResourceUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-nez v1, :cond_0

    if-nez p4, :cond_0

    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-static {p0}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/util/BackgroundPatternUtils;

    invoke-static {p3}, Lcom/google/android/apps/plus/util/BackgroundPatternUtils;->getBackgroundPattern(Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v4

    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v6, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    invoke-virtual {v4, v3, v3, v5, v6}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    invoke-virtual {v4, v2}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    if-gt v2, v4, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    if-le v2, v4, :cond_2

    :cond_1
    sget v2, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    sget v4, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    invoke-static {v1, v2, v4}, Lcom/google/android/apps/plus/util/ImageUtils;->resizeAndCropBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_2
    sget v2, Lcom/google/android/apps/plus/R$id;->image:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    sget v2, Lcom/google/android/apps/plus/R$id;->image:I

    invoke-virtual {p1, v2, v1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    iget-object v2, p2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/api/MediaRef;->getType()Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    if-ne v2, v4, :cond_3

    sget v2, Lcom/google/android/apps/plus/R$id;->video_overlay:I

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    :cond_3
    if-eqz v1, :cond_4

    const/4 v2, 0x1

    :goto_1
    return v2

    :catch_0
    move-exception v2

    const-string v2, "EsWidget"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not download image: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p2, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method private static readMediaContent(Landroid/database/Cursor;)Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;
    .locals 10
    .param p0    # Landroid/database/Cursor;

    const/16 v9, 0x1e

    const/4 v8, 0x0

    const/4 v2, 0x0

    invoke-interface {p0, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-interface {p0, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedAlbum;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getReportedMediaCount()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {v0, v8}, Lcom/google/android/apps/plus/content/DbEmbedAlbum;->getPhoto(I)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v2

    :cond_0
    :goto_0
    new-instance v6, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    invoke-direct {v6, v8}, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;-><init>(B)V

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getImageUrl()Ljava/lang/String;

    move-result-object v3

    new-instance v8, Lcom/google/android/apps/plus/api/MediaRef;

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->isVideo()Z

    move-result v7

    if-eqz v7, :cond_3

    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->VIDEO:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    :goto_1
    invoke-direct {v8, v3, v7}, Lcom/google/android/apps/plus/api/MediaRef;-><init>(Ljava/lang/String;Lcom/google/android/apps/plus/api/MediaRef$MediaType;)V

    iput-object v8, v6, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->mediaRef:Lcom/google/android/apps/plus/api/MediaRef;

    sget v7, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    iput v7, v6, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->width:I

    sget v7, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    iput v7, v6, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->height:I

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->getTitle()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;->linkTitle:Ljava/lang/String;

    :cond_1
    return-object v6

    :cond_2
    const/16 v7, 0x1d

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {v5}, Lcom/google/android/apps/plus/content/DbEmbedMedia;->deserialize([B)Lcom/google/android/apps/plus/content/DbEmbedMedia;

    move-result-object v2

    goto :goto_0

    :cond_3
    sget-object v7, Lcom/google/android/apps/plus/api/MediaRef$MediaType;->IMAGE:Lcom/google/android/apps/plus/api/MediaRef$MediaType;

    goto :goto_1
.end method

.method private static showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V
    .locals 1
    .param p0    # Landroid/widget/RemoteViews;
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    invoke-virtual {p0, p1, p3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    return-void
.end method

.method private static showTextLayoutContent(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/widget/RemoteViews;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # J

    const/4 v1, 0x0

    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    array-length v3, v5

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-lez v3, :cond_0

    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    const/4 v6, 0x0

    add-int/lit8 v1, v1, 0x1

    aget v4, v5, v6

    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {p1, v4, p2, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    if-ge v1, v3, :cond_1

    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v2, v1, 0x1

    aget v4, v5, v1

    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {p1, v4, p3, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    move v1, v2

    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    if-ge v1, v3, :cond_4

    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v2, v1, 0x1

    aget v4, v5, v1

    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    invoke-static {p1, v4, p4, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    :goto_0
    if-nez v2, :cond_2

    invoke-static {p0, p5, p6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getAutoText(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v1, v2, 0x1

    aget v4, v5, v2

    sget v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAutoTextColor:I

    invoke-static {p1, v4, v0, v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->showText(Landroid/widget/RemoteViews;ILjava/lang/String;I)V

    move v2, v1

    :cond_2
    :goto_1
    if-ge v2, v3, :cond_3

    sget-object v5, Lcom/google/android/apps/plus/widget/EsWidgetService;->TEXT_ONLY_VIEW_IDS:[I

    add-int/lit8 v1, v2, 0x1

    aget v4, v5, v2

    const/16 v5, 0x8

    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    move v2, v1

    goto :goto_1

    :cond_3
    return-void

    :cond_4
    move v2, v1

    goto :goto_0
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    invoke-static {p0}, Lcom/google/android/apps/plus/service/ImageResourceManager;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/service/ImageResourceManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/widget/EsWidgetService;->mImageResourceManager:Lcom/google/android/apps/plus/service/ImageResourceManager;

    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 16
    .param p1    # Landroid/content/Intent;

    sget-boolean v12, Lcom/google/android/apps/plus/widget/EsWidgetService;->sInitialized:Z

    if-nez v12, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/plus/widget/EsWidgetService;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const/4 v12, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/google/android/apps/plus/content/EsAvatarData;->getSmallDefaultAvatar(Landroid/content/Context;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    sput-object v12, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAuthorBitmap:Landroid/graphics/Bitmap;

    sget v12, Lcom/google/android/apps/plus/R$color;->stream_content_color:I

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    sput v12, Lcom/google/android/apps/plus/widget/EsWidgetService;->sContentColor:I

    sget v12, Lcom/google/android/apps/plus/R$color;->card_auto_text:I

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v12

    sput v12, Lcom/google/android/apps/plus/widget/EsWidgetService;->sAutoTextColor:I

    sget v12, Lcom/google/android/apps/plus/R$integer;->max_widget_image_size:I

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/phone/ScreenMetrics;->getInstance(Landroid/content/Context;)Lcom/google/android/apps/plus/phone/ScreenMetrics;

    move-result-object v13

    iget v13, v13, Lcom/google/android/apps/plus/phone/ScreenMetrics;->shortDimension:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    sput v12, Lcom/google/android/apps/plus/widget/EsWidgetService;->sWidgetImageFetchSize:I

    const/4 v12, 0x1

    sput-boolean v12, Lcom/google/android/apps/plus/widget/EsWidgetService;->sInitialized:Z

    :cond_0
    const-string v12, "appWidgetId"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    const-string v12, "refresh"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v12, "activity_id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v1

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showTapToConfigure(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showLoadingView(Landroid/content/Context;I)V

    goto :goto_0

    :cond_4
    if-eqz v7, :cond_5

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->fetchActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadCursor(Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_6

    :try_start_0
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v12

    if-nez v12, :cond_6

    if-nez v7, :cond_6

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->fetchActivities(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/plus/widget/EsWidgetService;->loadCursor(Lcom/google/android/apps/plus/content/EsAccount;ILjava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    :cond_6
    if-eqz v5, :cond_7

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v12

    if-nez v12, :cond_8

    :cond_7
    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showNoPostsFound(Landroid/content/Context;Lcom/google/android/apps/plus/content/EsAccount;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    if-eqz v5, :cond_1

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_8
    :try_start_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_f

    const/4 v12, -0x1

    invoke-interface {v5, v12}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_9
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-eqz v12, :cond_f

    const/4 v12, 0x1

    invoke-interface {v5, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_9

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v12

    if-nez v12, :cond_a

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_a
    :goto_2
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v12

    const/4 v13, 0x1

    if-le v12, v13, :cond_c

    invoke-interface {v5}, Landroid/database/Cursor;->getPosition()I

    move-result v12

    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-nez v13, :cond_b

    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    :cond_b
    invoke-static {v5}, Lcom/google/android/apps/plus/widget/EsWidgetService;->readMediaContent(Landroid/database/Cursor;)Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;

    move-result-object v13

    const/4 v14, 0x2

    invoke-interface {v5, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    new-instance v15, Lcom/google/android/apps/plus/widget/EsWidgetService$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v13, v14}, Lcom/google/android/apps/plus/widget/EsWidgetService$1;-><init>(Lcom/google/android/apps/plus/widget/EsWidgetService;Lcom/google/android/apps/plus/widget/EsWidgetService$MediaContent;Ljava/lang/String;)V

    invoke-static {v15}, Lcom/google/android/apps/plus/util/ThreadUtil;->postOnUiThread(Ljava/lang/Runnable;)V

    invoke-interface {v5, v12}, Landroid/database/Cursor;->moveToPosition(I)Z

    :cond_c
    invoke-static/range {p0 .. p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v8

    const/4 v6, 0x0

    sget v12, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v13, 0x11

    if-lt v12, v13, :cond_d

    invoke-virtual {v8, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v9

    const-string v12, "appWidgetCategory"

    const/4 v13, -0x1

    invoke-virtual {v9, v12, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_10

    const/4 v6, 0x1

    :cond_d
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v5, v6}, Lcom/google/android/apps/plus/widget/EsWidgetService;->createRemoteViews(Lcom/google/android/apps/plus/content/EsAccount;ILandroid/database/Cursor;Z)Landroid/widget/RemoteViews;

    move-result-object v11

    invoke-virtual {v8, v2, v11}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v12

    if-eqz v5, :cond_e

    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_e
    throw v12

    :cond_f
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    :cond_10
    const/4 v6, 0x0

    goto :goto_3
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 6
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    const/4 v5, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/IntentService;->onStart(Landroid/content/Intent;I)V

    const-string v4, "appWidgetId"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetUtils;->loadCircleId(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/apps/plus/content/EsAccountsData;->getActiveAccount(Landroid/content/Context;)Lcom/google/android/apps/plus/content/EsAccount;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showTapToConfigure(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0, v1}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showLoadingView(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    const-string v4, "refresh"

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {p0, v1, v3}, Lcom/google/android/apps/plus/widget/EsWidgetProvider;->showProgressIndicator(Landroid/content/Context;IZ)V

    goto :goto_0
.end method
