.class public abstract Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
.super Ljava/lang/Object;
.source "GoogleTalkClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$1;,
        Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;
    }
.end annotation


# instance fields
.field private mActive:Z

.field private final mAddress:Ljava/lang/String;

.field private final mBackendAddress:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDebugModeEnabled:Z

.field private final mEsAccount:Lcom/google/android/apps/plus/content/EsAccount;

.field private mGoogleToken:Ljava/lang/String;

.field private mJabberId:Ljava/lang/String;

.field private final mResource:Ljava/lang/String;

.field private mSocket:Ljava/net/Socket;

.field private mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

.field private mWriter:Ljava/io/BufferedWriter;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/content/EsAccount;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/plus/content/EsAccount;
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mEsAccount:Lcom/google/android/apps/plus/content/EsAccount;

    iput-object p2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mAddress:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mBackendAddress:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mResource:Ljava/lang/String;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mActive:Z

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    sget v2, Lcom/google/android/apps/plus/R$string;->realtimechat_notify_setting_key:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/plus/R$bool;->realtimechat_notify_setting_default_value:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mDebugModeEnabled:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mGoogleToken:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mEsAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/net/Socket;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/net/Socket;)Ljava/net/Socket;
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
    .param p1    # Ljava/net/Socket;

    iput-object p1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V
    .locals 0
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->resetWriter()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    const-string v0, "<stream:stream xmlns:stream=\'http://etherx.jabber.org/streams\' version=\'1.0\' xmlns=\'jabber:client\' to=\'gmail.com\' xml:lang=\'en\' xmlns:xml=\'http://www.w3.org/XML/1998/namespace\'>"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->write(Ljava/lang/String;)Z

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Z
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-boolean v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mDebugModeEnabled:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    :try_start_0
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    iget-object v2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getPort()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setKeepAlive(Z)V

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    const v2, 0xea60

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    invoke-direct {p0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->resetWriter()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GoogleTalkClient"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleTalkClient"

    const-string v1, "Exception while starting TLS"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    goto :goto_0
.end method

.method static synthetic access$800(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mResource:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;Ljava/lang/String;)V
    .locals 5
    .param p0    # Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mJabberId:Ljava/lang/String;

    const-string v0, "GoogleTalkClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleTalkClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "jid "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mJabberId:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mJabberId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mBackendAddress:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "<iq to=\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\' from=\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' type=\'set\'><subscribe xmlns=\'google:push\'><item from=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' channel=\'realtime-chat\'/></subscribe></iq>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->write(Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->onConnected()V

    return-void
.end method

.method private resetWriter()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    iget-object v2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mWriter:Ljava/io/BufferedWriter;

    return-void
.end method


# virtual methods
.method public final active()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mActive:Z

    return v0
.end method

.method public final declared-synchronized connect()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mActive:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    invoke-direct {v0, p0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;-><init>(Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public disconnect()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V

    return-void
.end method

.method public final declared-synchronized disconnect(I)V
    .locals 3
    .param p1    # I

    monitor-enter p0

    :try_start_0
    const-string v0, "GoogleTalkClient"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleTalkClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "disconnect "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mActive:Z

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;->setDisconnected()V

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mSocket:Ljava/net/Socket;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mThread:Lcom/google/android/apps/plus/xmpp/GoogleTalkClient$GoogleTalkThread;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->onDisconnected(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final getAccount()Lcom/google/android/apps/plus/content/EsAccount;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mEsAccount:Lcom/google/android/apps/plus/content/EsAccount;

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected abstract onConnected()V
.end method

.method protected abstract onDisconnected(I)V
.end method

.method protected abstract onMessageReceived([B)V
.end method

.method public final sendMessage([B)Z
    .locals 6
    .param p1    # [B

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mJabberId:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mJabberId:Ljava/lang/String;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mJabberId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mAddress:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "<message to=\'"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\' from=\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' type=\'headline\'><push xmlns=\'google:push\' channel=\'realtime-chat\'><recipient to=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' data=\'\'/><data>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</data></push></message>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->write(Ljava/lang/String;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public final declared-synchronized write(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mWriter:Ljava/io/BufferedWriter;

    invoke-virtual {v0, p1}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->mWriter:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "GoogleTalkClient"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "GoogleTalkClient"

    const-string v1, "IOException while writing message"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/xmpp/GoogleTalkClient;->disconnect(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
