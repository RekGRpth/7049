.class public final Lcom/google/android/apps/plus/xmpp/MessageReader;
.super Ljava/lang/Object;
.source "MessageReader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAuthenticationRequired:Z

.field private mBindAvailable:Z

.field private mEventData:Ljava/lang/String;

.field private final mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

.field private final mParser:Lorg/xmlpull/v1/XmlPullParser;

.field private mStringBuilder:Ljava/lang/StringBuilder;

.field private mTlsRequired:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "MessageReader"

    sput-object v0, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 4
    .param p1    # Ljava/io/InputStream;
    .param p2    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v1

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    if-eqz p2, :cond_0

    new-instance v1, Lcom/google/android/apps/plus/xmpp/LogInputStream;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/xmpp/LogInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    iget-object v2, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    iget-object v1, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to create XML parser"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private updateEventData()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mEventData:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    return-void
.end method


# virtual methods
.method public final getEventData()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mEventData:Ljava/lang/String;

    return-object v0
.end method

.method public final read()Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    .locals 10

    const/4 v5, 0x0

    const/4 v9, 0x5

    const/4 v8, 0x1

    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    if-eq v6, v8, :cond_17

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_1
    :goto_0
    if-eqz v1, :cond_0

    :goto_1
    return-object v1

    :pswitch_0
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "push:data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "jid"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    :cond_3
    const-string v7, "stream"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "starttls"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mTlsRequired:Z

    :cond_4
    const-string v7, "stream:features"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mTlsRequired:Z

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mAuthenticationRequired:Z

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mBindAvailable:Z

    :cond_5
    const-string v7, "bind"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mBindAvailable:Z

    :cond_6
    const-string v7, "mechanisms"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mAuthenticationRequired:Z

    :cond_7
    const-string v7, "proceed"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->PROCEED_WITH_TLS:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    :goto_2
    goto :goto_0

    :cond_8
    const-string v7, "success"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->AUTHENTICATION_SUCCEEDED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_2

    :cond_9
    const-string v7, "failure"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->AUTHENTICATION_FAILED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_2

    :cond_a
    move-object v1, v5

    goto :goto_2

    :pswitch_1
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "push:data"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-direct {p0}, Lcom/google/android/apps/plus/xmpp/MessageReader;->updateEventData()V

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->DATA_RECEIVED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    :goto_3
    goto/16 :goto_0

    :cond_b
    const-string v7, "stream:features"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_13

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_c

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "Processing stream features"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_c
    iget-boolean v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mTlsRequired:Z

    if-eqz v6, :cond_e

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_d

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "TLS required"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->TLS_REQUIRED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_3

    :cond_e
    iget-boolean v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mAuthenticationRequired:Z

    if-eqz v6, :cond_10

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_f

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "Authentication required"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->AUTHENTICATION_REQUIRED:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_3

    :cond_10
    iget-boolean v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mBindAvailable:Z

    if-eqz v6, :cond_12

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v7, 0x4

    invoke-static {v6, v7}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_11

    sget-object v6, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v7, "Stream is ready"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_11
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->STREAM_READY:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_3

    :cond_12
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->UNEXPECTED_FEATURES:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_3

    :cond_13
    const-string v7, "jid"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    invoke-direct {p0}, Lcom/google/android/apps/plus/xmpp/MessageReader;->updateEventData()V

    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->JID_AVAILABLE:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto :goto_3

    :cond_14
    move-object v1, v5

    goto :goto_3

    :pswitch_2
    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mStringBuilder:Ljava/lang/StringBuilder;

    iget-object v7, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mParser:Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v2

    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_15

    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v6, "XML parser exception"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_15
    iget-object v5, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    if-eqz v5, :cond_16

    iget-object v5, p0, Lcom/google/android/apps/plus/xmpp/MessageReader;->mInputStream:Lcom/google/android/apps/plus/xmpp/LogInputStream;

    invoke-static {}, Lcom/google/android/apps/plus/xmpp/LogInputStream;->getLog()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_16

    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "XML Data ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "): "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_16
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->END_OF_STREAM:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto/16 :goto_1

    :cond_17
    :try_start_1
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->END_OF_STREAM:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v0

    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const/4 v6, 0x6

    invoke-static {v5, v6}, Lcom/google/android/apps/plus/util/EsLog;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_18

    sget-object v5, Lcom/google/android/apps/plus/xmpp/MessageReader;->TAG:Ljava/lang/String;

    const-string v6, "Error "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_18
    sget-object v1, Lcom/google/android/apps/plus/xmpp/MessageReader$Event;->END_OF_STREAM:Lcom/google/android/apps/plus/xmpp/MessageReader$Event;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
