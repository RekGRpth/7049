.class public final Lcom/google/android/gms/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final emotishare_category:I = 0x7f0a0017

.field public static final emotishare_description:I = 0x7f0a0019

.field public static final emotishare_icon_uri:I = 0x7f0a001a

.field public static final emotishare_id:I = 0x7f0a0013

.field public static final emotishare_image_uri:I = 0x7f0a001b

.field public static final emotishare_name:I = 0x7f0a0015

.field public static final emotishare_release_generation:I = 0x7f0a0014

.field public static final emotishare_share_text:I = 0x7f0a0018

.field public static final emotishare_type:I = 0x7f0a0016

.field public static final event_activity_checked_in_strings:I = 0x7f0a000e

.field public static final event_activity_invite_strings:I = 0x7f0a000b

.field public static final event_activity_rsvp_no_strings:I = 0x7f0a000d

.field public static final event_activity_rsvp_yes_strings:I = 0x7f0a000c

.field public static final incoming_hangout_widget_2way_direction_descriptions:I = 0x7f0a0011

.field public static final incoming_hangout_widget_2way_target_descriptions:I = 0x7f0a0010

.field public static final incoming_hangout_widget_2way_targets:I = 0x7f0a000f

.field public static final network_statistics_types:I = 0x7f0a0009

.field public static final network_statistics_types_units:I = 0x7f0a000a

.field public static final photo_connection_preference_entry_labels:I = 0x7f0a0003

.field public static final photo_connection_preference_entry_values:I = 0x7f0a0002

.field public static final photo_upload_size_preference_entry_labels:I = 0x7f0a0007

.field public static final photo_upload_size_preference_entry_summaries:I = 0x7f0a0008

.field public static final photo_upload_size_preference_entry_values:I = 0x7f0a0006

.field public static final realtimechat_acl_options_keys:I = 0x7f0a0000

.field public static final realtimechat_acl_options_labels:I = 0x7f0a0001

.field public static final realtimechat_backends:I = 0x7f0a0012

.field public static final video_connection_preference_entry_labels:I = 0x7f0a0005

.field public static final video_connection_preference_entry_values:I = 0x7f0a0004


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
