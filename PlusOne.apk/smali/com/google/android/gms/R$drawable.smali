.class public final Lcom/google/android/gms/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final ab_solid_light_holo:I = 0x7f020000

.field public static final actionmode_background:I = 0x7f020001

.field public static final actiontabs_bar:I = 0x7f020002

.field public static final add:I = 0x7f020003

.field public static final album_count_background:I = 0x7f020004

.field public static final album_item_placeholder:I = 0x7f020005

.field public static final album_view_selector:I = 0x7f020006

.field public static final appwidget_button_left:I = 0x7f020007

.field public static final appwidget_button_right:I = 0x7f020008

.field public static final appwidget_dark_bg:I = 0x7f020009

.field public static final appwidget_dark_bg_focus:I = 0x7f02000a

.field public static final appwidget_dark_bg_press:I = 0x7f02000b

.field public static final appwidget_dark_bg_selector:I = 0x7f02000c

.field public static final appwidget_inner_focus_l:I = 0x7f02000d

.field public static final appwidget_inner_focus_r:I = 0x7f02000e

.field public static final appwidget_inner_press_l:I = 0x7f02000f

.field public static final appwidget_inner_press_r:I = 0x7f020010

.field public static final attach_camera:I = 0x7f020011

.field public static final audience_view_chip_background_blue:I = 0x7f020012

.field public static final badge_grey:I = 0x7f020013

.field public static final badge_red:I = 0x7f020014

.field public static final bg_blue_tile:I = 0x7f020015

.field public static final bg_card_white:I = 0x7f020016

.field public static final bg_cardbtm_gray:I = 0x7f020017

.field public static final bg_cardtop_white:I = 0x7f020018

.field public static final bg_green_tile:I = 0x7f020019

.field public static final bg_red_tile:I = 0x7f02001a

.field public static final bg_taco_avatar:I = 0x7f02001b

.field public static final bg_taco_mediapattern:I = 0x7f02001c

.field public static final bg_tacos:I = 0x7f02001d

.field public static final bg_tacos_body:I = 0x7f02001e

.field public static final bg_tacos_footer:I = 0x7f02001f

.field public static final bg_tacos_header:I = 0x7f020020

.field public static final bg_yellow_tile:I = 0x7f020021

.field public static final blue_button:I = 0x7f020022

.field public static final blue_checkmark:I = 0x7f020023

.field public static final btn_bg_pressed:I = 0x7f020024

.field public static final btn_bg_selected:I = 0x7f020025

.field public static final btn_blue:I = 0x7f020026

.field public static final btn_blue_pressed:I = 0x7f020027

.field public static final btn_default_gray:I = 0x7f020028

.field public static final btn_default_gray_pressed:I = 0x7f020029

.field public static final btn_events_on_air:I = 0x7f02002a

.field public static final btn_froyo_spinner:I = 0x7f02002b

.field public static final btn_froyo_spinner_default:I = 0x7f02002c

.field public static final btn_froyo_spinner_focused:I = 0x7f02002d

.field public static final btn_froyo_spinner_pressed:I = 0x7f02002e

.field public static final btn_plusone_red:I = 0x7f02002f

.field public static final btn_plusone_red_pressed:I = 0x7f020030

.field public static final btn_search_dialog:I = 0x7f020031

.field public static final btn_search_dialog_default:I = 0x7f020032

.field public static final btn_search_dialog_disabled:I = 0x7f020033

.field public static final btn_search_dialog_pressed:I = 0x7f020034

.field public static final btn_search_dialog_selected:I = 0x7f020035

.field public static final btn_white:I = 0x7f020036

.field public static final btn_white_pressed:I = 0x7f020037

.field public static final button_plusone:I = 0x7f020038

.field public static final button_plusone_pressed:I = 0x7f020039

.field public static final button_plusone_red:I = 0x7f02003a

.field public static final button_plusone_red_pressed:I = 0x7f02003b

.field public static final chip_blue:I = 0x7f02003c

.field public static final chip_extended:I = 0x7f02003d

.field public static final chip_green:I = 0x7f02003e

.field public static final chip_red:I = 0x7f02003f

.field public static final circle:I = 0x7f020040

.field public static final circle_member_count_background:I = 0x7f020041

.field public static final circle_settings_background:I = 0x7f020042

.field public static final compose_button_background:I = 0x7f020043

.field public static final compose_button_background_default:I = 0x7f020044

.field public static final compose_button_background_pressed:I = 0x7f020045

.field public static final compose_empty_background:I = 0x7f020046

.field public static final compose_item_background:I = 0x7f020047

.field public static final compose_photo_background:I = 0x7f020048

.field public static final count_plusone:I = 0x7f020049

.field public static final count_plusone_pressed:I = 0x7f02004a

.field public static final default_cover_photo:I = 0x7f02004b

.field public static final dialog_full_holo_light:I = 0x7f02004c

.field public static final disabled_button:I = 0x7f02004d

.field public static final divider:I = 0x7f02004e

.field public static final divider_dark:I = 0x7f02004f

.field public static final empty_recent_image:I = 0x7f020050

.field public static final empty_thumbnail:I = 0x7f020051

.field public static final fade_right:I = 0x7f020052

.field public static final footer_bar_background:I = 0x7f020053

.field public static final generic_selector_background:I = 0x7f020054

.field public static final google_music:I = 0x7f020055

.field public static final green_button:I = 0x7f020056

.field public static final green_button_default:I = 0x7f020057

.field public static final green_button_focused:I = 0x7f020058

.field public static final green_button_pressed:I = 0x7f020059

.field public static final grey_circle:I = 0x7f02005a

.field public static final hangout_background_logo:I = 0x7f02005b

.field public static final hangout_ic_menu_audio_mute:I = 0x7f02005c

.field public static final hangout_ic_menu_audio_unmute:I = 0x7f02005d

.field public static final hangout_ic_menu_exit:I = 0x7f02005e

.field public static final hangout_ic_menu_exit_unpadded:I = 0x7f02005f

.field public static final hangout_ic_menu_invite_people:I = 0x7f020060

.field public static final hangout_ic_menu_switch_camera:I = 0x7f020061

.field public static final hangout_ic_menu_video_mute:I = 0x7f020062

.field public static final hangout_ic_menu_video_unmute:I = 0x7f020063

.field public static final hangout_inset_video_border:I = 0x7f020064

.field public static final hangout_notification_icon:I = 0x7f020065

.field public static final hangout_pin:I = 0x7f020066

.field public static final hangout_pinned_border:I = 0x7f020067

.field public static final hangout_ringing_mute_menu_background:I = 0x7f020068

.field public static final hangout_toast_background:I = 0x7f020069

.field public static final hangout_toast_border:I = 0x7f02006a

.field public static final hangout_video_dropshadow:I = 0x7f02006b

.field public static final hangout_video_pause:I = 0x7f02006c

.field public static final huddle_start_arrow:I = 0x7f02006d

.field public static final huddle_watermark:I = 0x7f02006e

.field public static final ic_ab_back_holo_dark:I = 0x7f02006f

.field public static final ic_ab_back_holo_light:I = 0x7f020070

.field public static final ic_acl_circles:I = 0x7f020071

.field public static final ic_acl_custom_inactive:I = 0x7f020072

.field public static final ic_acl_domain:I = 0x7f020073

.field public static final ic_acl_domain_active:I = 0x7f020074

.field public static final ic_acl_domain_inactive:I = 0x7f020075

.field public static final ic_acl_extended:I = 0x7f020076

.field public static final ic_acl_public:I = 0x7f020077

.field public static final ic_acl_x:I = 0x7f020078

.field public static final ic_actionbar_gplus:I = 0x7f020079

.field public static final ic_actionbar_plusonegray:I = 0x7f02007a

.field public static final ic_actionbar_plusonered:I = 0x7f02007b

.field public static final ic_actionbar_reshare:I = 0x7f02007c

.field public static final ic_add_circles:I = 0x7f02007d

.field public static final ic_add_new_circle:I = 0x7f02007e

.field public static final ic_avatar:I = 0x7f02007f

.field public static final ic_block:I = 0x7f020080

.field public static final ic_btn_add_member:I = 0x7f020081

.field public static final ic_btn_dismiss_person:I = 0x7f020082

.field public static final ic_btn_search:I = 0x7f020083

.field public static final ic_cab_done_holo_dark:I = 0x7f020084

.field public static final ic_cab_done_holo_light:I = 0x7f020085

.field public static final ic_camera:I = 0x7f020086

.field public static final ic_camera_active:I = 0x7f020087

.field public static final ic_camera_grey_24:I = 0x7f020088

.field public static final ic_camera_white:I = 0x7f020089

.field public static final ic_checkin_small:I = 0x7f02008a

.field public static final ic_circles:I = 0x7f02008b

.field public static final ic_circles_active:I = 0x7f02008c

.field public static final ic_circles_light:I = 0x7f02008d

.field public static final ic_circles_white_16:I = 0x7f02008e

.field public static final ic_circles_white_20:I = 0x7f02008f

.field public static final ic_clear_normal:I = 0x7f020090

.field public static final ic_clipboard:I = 0x7f020091

.field public static final ic_close_cancel:I = 0x7f020092

.field public static final ic_close_cancel_grey:I = 0x7f020093

.field public static final ic_close_cancel_white:I = 0x7f020094

.field public static final ic_comment_16:I = 0x7f020095

.field public static final ic_comment_blue:I = 0x7f020096

.field public static final ic_communities_green_12:I = 0x7f020097

.field public static final ic_communities_green_16:I = 0x7f020098

.field public static final ic_communities_grey:I = 0x7f020099

.field public static final ic_community_avatar:I = 0x7f02009a

.field public static final ic_community_notify:I = 0x7f02009b

.field public static final ic_community_share:I = 0x7f02009c

.field public static final ic_compose:I = 0x7f02009d

.field public static final ic_compose_white:I = 0x7f02009e

.field public static final ic_delete_event:I = 0x7f02009f

.field public static final ic_done_save_ok_blue:I = 0x7f0200a0

.field public static final ic_done_save_ok_green:I = 0x7f0200a1

.field public static final ic_down:I = 0x7f0200a2

.field public static final ic_edit_event:I = 0x7f0200a3

.field public static final ic_edit_profile:I = 0x7f0200a4

.field public static final ic_error_gold_40:I = 0x7f0200a5

.field public static final ic_error_white:I = 0x7f0200a6

.field public static final ic_exit_to_app_20:I = 0x7f0200a7

.field public static final ic_flash_off_holo_light:I = 0x7f0200a8

.field public static final ic_flash_on_holo_light:I = 0x7f0200a9

.field public static final ic_friend_dismiss:I = 0x7f0200aa

.field public static final ic_gplus_red_32:I = 0x7f0200ab

.field public static final ic_hangout_black:I = 0x7f0200ac

.field public static final ic_hangout_ringing_accept:I = 0x7f0200ad

.field public static final ic_hangout_ringing_accept_active:I = 0x7f0200ae

.field public static final ic_hangout_ringing_accept_chevron:I = 0x7f0200af

.field public static final ic_hangout_ringing_accept_icon:I = 0x7f0200b0

.field public static final ic_hangout_ringing_ignore:I = 0x7f0200b1

.field public static final ic_hangout_ringing_ignore_active:I = 0x7f0200b2

.field public static final ic_hangout_ringing_ignore_chevron:I = 0x7f0200b3

.field public static final ic_hangout_ringing_ignore_icon:I = 0x7f0200b4

.field public static final ic_hangout_ringing_outerring:I = 0x7f0200b5

.field public static final ic_hangout_ringing_touch_handle:I = 0x7f0200b6

.field public static final ic_hangout_ringing_touch_handle_normal:I = 0x7f0200b7

.field public static final ic_hangout_ringing_touch_handle_pressed:I = 0x7f0200b8

.field public static final ic_hangout_title:I = 0x7f0200b9

.field public static final ic_hangouts_grey_12:I = 0x7f0200ba

.field public static final ic_hangouts_yellow_24:I = 0x7f0200bb

.field public static final ic_help_20:I = 0x7f0200bc

.field public static final ic_huddle_read:I = 0x7f0200bd

.field public static final ic_huddle_sending:I = 0x7f0200be

.field public static final ic_huddle_sent:I = 0x7f0200bf

.field public static final ic_left:I = 0x7f0200c0

.field public static final ic_left_arrow:I = 0x7f0200c1

.field public static final ic_link_blue:I = 0x7f0200c2

.field public static final ic_link_grey_24:I = 0x7f0200c3

.field public static final ic_link_yellow_12:I = 0x7f0200c4

.field public static final ic_loading_video:I = 0x7f0200c5

.field public static final ic_location_active:I = 0x7f0200c6

.field public static final ic_location_city:I = 0x7f0200c7

.field public static final ic_location_grey:I = 0x7f0200c8

.field public static final ic_location_grey_12:I = 0x7f0200c9

.field public static final ic_location_none:I = 0x7f0200ca

.field public static final ic_location_red_12:I = 0x7f0200cb

.field public static final ic_menu_account_list:I = 0x7f0200cc

.field public static final ic_menu_add_people:I = 0x7f0200cd

.field public static final ic_menu_addphotos:I = 0x7f0200ce

.field public static final ic_menu_blocked_user:I = 0x7f0200cf

.field public static final ic_menu_camera:I = 0x7f0200d0

.field public static final ic_menu_checkin:I = 0x7f0200d1

.field public static final ic_menu_compose:I = 0x7f0200d2

.field public static final ic_menu_copy_holo_dark:I = 0x7f0200d3

.field public static final ic_menu_cut_holo_dark:I = 0x7f0200d4

.field public static final ic_menu_delete:I = 0x7f0200d5

.field public static final ic_menu_done_holo_light_grey:I = 0x7f0200d6

.field public static final ic_menu_download:I = 0x7f0200d7

.field public static final ic_menu_edit:I = 0x7f0200d8

.field public static final ic_menu_events:I = 0x7f0200d9

.field public static final ic_menu_feedback:I = 0x7f0200da

.field public static final ic_menu_flag_system:I = 0x7f0200db

.field public static final ic_menu_hangout:I = 0x7f0200dc

.field public static final ic_menu_help:I = 0x7f0200dd

.field public static final ic_menu_leave:I = 0x7f0200de

.field public static final ic_menu_map:I = 0x7f0200df

.field public static final ic_menu_mute:I = 0x7f0200e0

.field public static final ic_menu_mute_conversation:I = 0x7f0200e1

.field public static final ic_menu_paste_holo_dark:I = 0x7f0200e2

.field public static final ic_menu_photos:I = 0x7f0200e3

.field public static final ic_menu_plus1:I = 0x7f0200e4

.field public static final ic_menu_preferences:I = 0x7f0200e5

.field public static final ic_menu_profile:I = 0x7f0200e6

.field public static final ic_menu_refresh:I = 0x7f0200e7

.field public static final ic_menu_remove_plus1:I = 0x7f0200e8

.field public static final ic_menu_remove_tag:I = 0x7f0200e9

.field public static final ic_menu_reshare:I = 0x7f0200ea

.field public static final ic_menu_search:I = 0x7f0200eb

.field public static final ic_menu_search_holo_light:I = 0x7f0200ec

.field public static final ic_menu_selectall_holo_dark:I = 0x7f0200ed

.field public static final ic_menu_send_holo_light:I = 0x7f0200ee

.field public static final ic_menu_send_holo_light_grey:I = 0x7f0200ef

.field public static final ic_menu_set_as_profile:I = 0x7f0200f0

.field public static final ic_menu_start_new_huddle:I = 0x7f0200f1

.field public static final ic_menu_stream:I = 0x7f0200f2

.field public static final ic_menu_terms_and_conditions:I = 0x7f0200f3

.field public static final ic_menu_unmute_conversation:I = 0x7f0200f4

.field public static final ic_menu_wallpaper:I = 0x7f0200f5

.field public static final ic_messenger_count:I = 0x7f0200f6

.field public static final ic_messenger_mute:I = 0x7f0200f7

.field public static final ic_metadata_link:I = 0x7f0200f8

.field public static final ic_metadata_location:I = 0x7f0200f9

.field public static final ic_metadata_music:I = 0x7f0200fa

.field public static final ic_missing_photo:I = 0x7f0200fb

.field public static final ic_mood:I = 0x7f0200fc

.field public static final ic_mood_gold:I = 0x7f0200fd

.field public static final ic_mood_grey_24:I = 0x7f0200fe

.field public static final ic_music_blue:I = 0x7f0200ff

.field public static final ic_mute:I = 0x7f020100

.field public static final ic_nav_apps:I = 0x7f020101

.field public static final ic_nav_circles:I = 0x7f020102

.field public static final ic_nav_communities:I = 0x7f020103

.field public static final ic_nav_events:I = 0x7f020104

.field public static final ic_nav_hangouts:I = 0x7f020105

.field public static final ic_nav_home:I = 0x7f020106

.field public static final ic_nav_local:I = 0x7f020107

.field public static final ic_nav_messenger:I = 0x7f020108

.field public static final ic_nav_myphotos:I = 0x7f020109

.field public static final ic_nav_profile:I = 0x7f02010a

.field public static final ic_next_white:I = 0x7f02010b

.field public static final ic_notification_alert:I = 0x7f02010c

.field public static final ic_notification_alert_read:I = 0x7f02010d

.field public static final ic_notification_alert_unread:I = 0x7f02010e

.field public static final ic_notification_coffeemug:I = 0x7f02010f

.field public static final ic_notification_comment:I = 0x7f020110

.field public static final ic_notification_comment_read:I = 0x7f020111

.field public static final ic_notification_comment_unread:I = 0x7f020112

.field public static final ic_notification_event:I = 0x7f020113

.field public static final ic_notification_event_read:I = 0x7f020114

.field public static final ic_notification_event_unread:I = 0x7f020115

.field public static final ic_notification_games:I = 0x7f020116

.field public static final ic_notification_games_read:I = 0x7f020117

.field public static final ic_notification_games_unread:I = 0x7f020118

.field public static final ic_notification_photo:I = 0x7f020119

.field public static final ic_notification_photo_read:I = 0x7f02011a

.field public static final ic_notification_photo_unread:I = 0x7f02011b

.field public static final ic_notification_post:I = 0x7f02011c

.field public static final ic_notification_post_read:I = 0x7f02011d

.field public static final ic_notification_post_unread:I = 0x7f02011e

.field public static final ic_overlay_album:I = 0x7f02011f

.field public static final ic_person_active:I = 0x7f020120

.field public static final ic_photodetail_comment:I = 0x7f020121

.field public static final ic_photodetail_plus:I = 0x7f020122

.field public static final ic_pin:I = 0x7f020123

.field public static final ic_play:I = 0x7f020124

.field public static final ic_plus_one:I = 0x7f020125

.field public static final ic_plusoned:I = 0x7f020126

.field public static final ic_private:I = 0x7f020127

.field public static final ic_private_small:I = 0x7f020128

.field public static final ic_profile:I = 0x7f020129

.field public static final ic_profile_attended:I = 0x7f02012a

.field public static final ic_profile_invited:I = 0x7f02012b

.field public static final ic_profile_lives:I = 0x7f02012c

.field public static final ic_profile_sms:I = 0x7f02012d

.field public static final ic_profile_works:I = 0x7f02012e

.field public static final ic_public:I = 0x7f02012f

.field public static final ic_public_active:I = 0x7f020130

.field public static final ic_public_small:I = 0x7f020131

.field public static final ic_refresh:I = 0x7f020132

.field public static final ic_refresh_blue:I = 0x7f020133

.field public static final ic_refresh_white:I = 0x7f020134

.field public static final ic_reshare_16:I = 0x7f020135

.field public static final ic_right:I = 0x7f020136

.field public static final ic_right_arrow:I = 0x7f020137

.field public static final ic_search_24_white:I = 0x7f020138

.field public static final ic_send_holo_light:I = 0x7f020139

.field public static final ic_send_photo_holo_light:I = 0x7f02013a

.field public static final ic_sliding_tab_jog_left:I = 0x7f02013b

.field public static final ic_sliding_tab_jog_right:I = 0x7f02013c

.field public static final ic_speech_bubble:I = 0x7f02013d

.field public static final ic_star_gray_16:I = 0x7f02013e

.field public static final ic_star_red_16:I = 0x7f02013f

.field public static final ic_stat_circle:I = 0x7f020140

.field public static final ic_stat_gplus:I = 0x7f020141

.field public static final ic_stat_instant_share:I = 0x7f020142

.field public static final ic_stat_instant_upload:I = 0x7f020143

.field public static final ic_stat_messenger:I = 0x7f020144

.field public static final ic_stop:I = 0x7f020145

.field public static final ic_text_active:I = 0x7f020146

.field public static final ic_text_grey:I = 0x7f020147

.field public static final ic_text_holo_light:I = 0x7f020148

.field public static final ic_time_grey_12:I = 0x7f020149

.field public static final ic_time_grey_16:I = 0x7f02014a

.field public static final ic_time_red_16:I = 0x7f02014b

.field public static final ic_up:I = 0x7f02014c

.field public static final ic_whats_hot_color_24:I = 0x7f02014d

.field public static final ic_whats_hot_red_16:I = 0x7f02014e

.field public static final icn_add_video:I = 0x7f02014f

.field public static final icn_drop_block:I = 0x7f020150

.field public static final icn_drop_block_unpadded:I = 0x7f020151

.field public static final icn_drop_blocked:I = 0x7f020152

.field public static final icn_drop_mute:I = 0x7f020153

.field public static final icn_drop_muted:I = 0x7f020154

.field public static final icn_drop_pin:I = 0x7f020155

.field public static final icn_drop_profile:I = 0x7f020156

.field public static final icn_drop_unpin:I = 0x7f020157

.field public static final icn_events_activity_checkin:I = 0x7f020158

.field public static final icn_events_activity_invited:I = 0x7f020159

.field public static final icn_events_add_comment:I = 0x7f02015a

.field public static final icn_events_add_photo:I = 0x7f02015b

.field public static final icn_events_arrow_down:I = 0x7f02015c

.field public static final icn_events_arrow_right:I = 0x7f02015d

.field public static final icn_events_arrow_up:I = 0x7f02015e

.field public static final icn_events_check:I = 0x7f02015f

.field public static final icn_events_create_event:I = 0x7f020160

.field public static final icn_events_details_location:I = 0x7f020161

.field public static final icn_events_details_time:I = 0x7f020162

.field public static final icn_events_directions:I = 0x7f020163

.field public static final icn_events_hangout_1up:I = 0x7f020164

.field public static final icn_events_hangout_taco:I = 0x7f020165

.field public static final icn_events_maybe:I = 0x7f020166

.field public static final icn_events_not_going:I = 0x7f020167

.field public static final icn_events_party_mode_1up:I = 0x7f020168

.field public static final icn_events_ribbon_blue:I = 0x7f020169

.field public static final icn_events_ribbon_green:I = 0x7f02016a

.field public static final icn_events_ribbon_grey:I = 0x7f02016b

.field public static final icn_events_rsvp_add_photo:I = 0x7f02016c

.field public static final icn_events_rsvp_invite_more:I = 0x7f02016d

.field public static final icn_hangouts_16dp:I = 0x7f02016e

.field public static final icn_location_card:I = 0x7f02016f

.field public static final icn_mute_overlay:I = 0x7f020170

.field public static final icn_notification_disabled:I = 0x7f020171

.field public static final icn_notification_enabled:I = 0x7f020172

.field public static final icn_ring_off:I = 0x7f020173

.field public static final icn_ring_on:I = 0x7f020174

.field public static final icn_starthangout:I = 0x7f020175

.field public static final icn_startmessenger:I = 0x7f020176

.field public static final image_button_background:I = 0x7f020177

.field public static final indicator_green:I = 0x7f020178

.field public static final indicator_red:I = 0x7f020179

.field public static final list_circle:I = 0x7f02017a

.field public static final list_circle_blocked:I = 0x7f02017b

.field public static final list_current:I = 0x7f02017c

.field public static final list_domain:I = 0x7f02017d

.field public static final list_extended:I = 0x7f02017e

.field public static final list_extended_red:I = 0x7f02017f

.field public static final list_focused_holo:I = 0x7f020180

.field public static final list_public:I = 0x7f020181

.field public static final list_public_red:I = 0x7f020182

.field public static final list_selected_holo:I = 0x7f020183

.field public static final list_unblock:I = 0x7f020184

.field public static final list_whats_hot:I = 0x7f020185

.field public static final local_review_icon:I = 0x7f020186

.field public static final menu_dropdown_panel_holo_light:I = 0x7f020187

.field public static final messenger_conversation_notify:I = 0x7f020188

.field public static final messenger_hangout_notify:I = 0x7f020189

.field public static final more:I = 0x7f02018a

.field public static final navigation_shadow:I = 0x7f02018b

.field public static final nearby_indicator_small:I = 0x7f02018c

.field public static final new_conversation_row:I = 0x7f02018d

.field public static final oob_blue_button_background:I = 0x7f02018e

.field public static final oob_blue_button_background_default:I = 0x7f02018f

.field public static final oob_bubbles:I = 0x7f020190

.field public static final oob_white_bottom_radio_button_background:I = 0x7f020191

.field public static final oob_white_bottom_radio_button_background_pressed:I = 0x7f020192

.field public static final oob_white_button_background:I = 0x7f020193

.field public static final oob_white_button_background_default:I = 0x7f020194

.field public static final oob_white_button_background_pressed:I = 0x7f020195

.field public static final oob_white_radio_button_background:I = 0x7f020196

.field public static final oob_white_radio_button_background_pressed:I = 0x7f020197

.field public static final oob_white_top_radio_button_background:I = 0x7f020198

.field public static final oob_white_top_radio_button_background_pressed:I = 0x7f020199

.field public static final ov_play_video_48:I = 0x7f02019a

.field public static final overlay_lightcycle:I = 0x7f02019b

.field public static final padded_list_divider:I = 0x7f02019c

.field public static final photo_comment:I = 0x7f02019d

.field public static final photo_home_item_divider_color:I = 0x7f020207

.field public static final photo_plusone:I = 0x7f02019e

.field public static final photo_tag_pressed:I = 0x7f02019f

.field public static final photo_tag_scroller_background:I = 0x7f0201a0

.field public static final photo_tag_selected:I = 0x7f0201a1

.field public static final photo_tag_selector:I = 0x7f0201a2

.field public static final photo_view_background:I = 0x7f0201a3

.field public static final photobackground:I = 0x7f0201a4

.field public static final photos_date_v:I = 0x7f0201a5

.field public static final plusone_button:I = 0x7f0201a6

.field public static final plusone_by_me_button:I = 0x7f0201a7

.field public static final profile_address:I = 0x7f0201a8

.field public static final profile_avatar_background:I = 0x7f0201a9

.field public static final profile_avatar_loading:I = 0x7f0201aa

.field public static final profile_email:I = 0x7f0201ab

.field public static final profile_local_call:I = 0x7f0201ac

.field public static final profile_local_directions:I = 0x7f0201ad

.field public static final profile_local_map:I = 0x7f0201ae

.field public static final profile_local_score_bg:I = 0x7f0201af

.field public static final profile_local_user_rated_logo:I = 0x7f0201b0

.field public static final profile_local_zagat_logo:I = 0x7f0201b1

.field public static final profile_phone:I = 0x7f0201b2

.field public static final profile_scrapbook_loading:I = 0x7f0201b3

.field public static final profile_selectable_item_background:I = 0x7f0201b4

.field public static final profile_sms:I = 0x7f0201b5

.field public static final quick_actions_divider:I = 0x7f0201b6

.field public static final quick_actions_item_background:I = 0x7f0201b7

.field public static final quick_actions_item_selector:I = 0x7f0201b8

.field public static final realtimechat_system_information_background:I = 0x7f0201b9

.field public static final realtimechat_system_information_background_default:I = 0x7f0201ba

.field public static final recent_images_border:I = 0x7f0201bb

.field public static final round_mask:I = 0x7f0201bc

.field public static final search_plate_global:I = 0x7f0201bd

.field public static final signup_picasa:I = 0x7f0201be

.field public static final sliding_tab_jog_tab_bar_left:I = 0x7f0201bf

.field public static final sliding_tab_jog_tab_bar_left_end_confirm_green:I = 0x7f0201c0

.field public static final sliding_tab_jog_tab_bar_left_end_normal:I = 0x7f0201c1

.field public static final sliding_tab_jog_tab_bar_left_end_pressed:I = 0x7f0201c2

.field public static final sliding_tab_jog_tab_bar_right:I = 0x7f0201c3

.field public static final sliding_tab_jog_tab_bar_right_end_confirm_red:I = 0x7f0201c4

.field public static final sliding_tab_jog_tab_bar_right_end_normal:I = 0x7f0201c5

.field public static final sliding_tab_jog_tab_bar_right_end_pressed:I = 0x7f0201c6

.field public static final sliding_tab_jog_tab_left:I = 0x7f0201c7

.field public static final sliding_tab_jog_tab_left_confirm_green:I = 0x7f0201c8

.field public static final sliding_tab_jog_tab_left_normal:I = 0x7f0201c9

.field public static final sliding_tab_jog_tab_left_pressed:I = 0x7f0201ca

.field public static final sliding_tab_jog_tab_right:I = 0x7f0201cb

.field public static final sliding_tab_jog_tab_right_confirm_red:I = 0x7f0201cc

.field public static final sliding_tab_jog_tab_right_normal:I = 0x7f0201cd

.field public static final sliding_tab_jog_tab_right_pressed:I = 0x7f0201ce

.field public static final sliding_tab_jog_tab_target_green:I = 0x7f0201cf

.field public static final sliding_tab_jog_tab_target_red:I = 0x7f0201d0

.field public static final stat_notify_addeduser:I = 0x7f0201d1

.field public static final stat_notify_adduser:I = 0x7f0201d2

.field public static final stat_notify_circle:I = 0x7f0201d3

.field public static final stat_notify_comment:I = 0x7f0201d4

.field public static final stat_notify_gplus:I = 0x7f0201d5

.field public static final stat_notify_instant_upload:I = 0x7f0201d6

.field public static final stat_notify_multiple_gplus:I = 0x7f0201d7

.field public static final stat_notify_plusone:I = 0x7f0201d8

.field public static final stat_notify_quota:I = 0x7f0201d9

.field public static final stat_notify_reshare:I = 0x7f0201da

.field public static final stream_list_selector:I = 0x7f0201db

.field public static final stream_tab_overlay_left:I = 0x7f0201dc

.field public static final stream_tab_overlay_right:I = 0x7f0201dd

.field public static final tab_indicator:I = 0x7f0201de

.field public static final tab_selected:I = 0x7f0201df

.field public static final tab_selected_focused_holo:I = 0x7f0201e0

.field public static final tab_selected_pressed_focused_holo:I = 0x7f0201e1

.field public static final tab_selected_pressed_holo:I = 0x7f0201e2

.field public static final tab_unselected:I = 0x7f0201e3

.field public static final tab_unselected_focused_holo:I = 0x7f0201e4

.field public static final tab_unselected_pressed_focused_holo:I = 0x7f0201e5

.field public static final tab_unselected_pressed_holo:I = 0x7f0201e6

.field public static final tag:I = 0x7f0201e7

.field public static final textfield:I = 0x7f0201e8

.field public static final textfield_default:I = 0x7f0201e9

.field public static final textfield_pressed:I = 0x7f0201ea

.field public static final textfield_search:I = 0x7f0201eb

.field public static final textfield_search_default:I = 0x7f0201ec

.field public static final textfield_search_disabled:I = 0x7f0201ed

.field public static final textfield_search_pressed:I = 0x7f0201ee

.field public static final textfield_search_selected:I = 0x7f0201ef

.field public static final textfield_selected:I = 0x7f0201f0

.field public static final title_bar_with_tabs_shadow:I = 0x7f0201f1

.field public static final title_button_background:I = 0x7f0201f2

.field public static final title_colored_button_text:I = 0x7f0201f3

.field public static final tooltip_bottom_left_background:I = 0x7f0201f4

.field public static final tooltip_bottom_right_background:I = 0x7f0201f5

.field public static final tooltip_top_left_background:I = 0x7f0201f6

.field public static final tooltip_top_right_background:I = 0x7f0201f7

.field public static final upload_photo:I = 0x7f0201f8

.field public static final video_overlay:I = 0x7f0201f9

.field public static final volume_bar:I = 0x7f0201fa

.field public static final white_arrow:I = 0x7f0201fb

.field public static final widget_divider:I = 0x7f0201fc

.field public static final widget_stream_icon:I = 0x7f0201fd

.field public static final widgets_gray_button_background:I = 0x7f0201fe

.field public static final widgets_kennedy_button_disabled:I = 0x7f0201ff

.field public static final widgets_kennedy_gray_button_default:I = 0x7f020200

.field public static final widgets_kennedy_gray_button_pressed:I = 0x7f020201

.field public static final widgets_kennedy_gray_button_selected:I = 0x7f020202

.field public static final zagat_explanation_background:I = 0x7f020203

.field public static final zagat_explanation_ok_button:I = 0x7f020204

.field public static final zagat_explanation_ok_button_default:I = 0x7f020205

.field public static final zagat_explanation_ok_button_pressed:I = 0x7f020206


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
