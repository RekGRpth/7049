.class public final Lcom/google/android/gms/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final card_max_hangout_avatars:I = 0x7f0c000e

.field public static final compose_text_min_lines_big:I = 0x7f0c0006

.field public static final config_image_cache_max_bytes_decoded_large:I = 0x7f0c0001

.field public static final config_image_cache_max_bytes_decoded_small:I = 0x7f0c0000

.field public static final device_location_map_aspect_ratio:I = 0x7f0c0004

.field public static final emotishare_icon_columns:I = 0x7f0c0002

.field public static final host_navigation_bar_width_percent:I = 0x7f0c0005

.field public static final interactive_post_title_max_lines:I = 0x7f0c000a

.field public static final max_widget_image_size:I = 0x7f0c0010

.field public static final places_map_aspect_ratio:I = 0x7f0c0003

.field public static final riviera_card_with_hero_title_max_lines:I = 0x7f0c0007

.field public static final riviera_link_description_max_lines:I = 0x7f0c0009

.field public static final riviera_link_title_max_lines:I = 0x7f0c0008

.field public static final riviera_max_comment_lines:I = 0x7f0c0012

.field public static final riviera_media_metadata_max_lines:I = 0x7f0c000c

.field public static final riviera_media_title_max_lines:I = 0x7f0c000b

.field public static final riviera_place_review_max_lines:I = 0x7f0c0011

.field public static final stream_post_max_length:I = 0x7f0c000d

.field public static final typeable_audience_view_max_lines:I = 0x7f0c000f


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
