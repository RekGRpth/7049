.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final about_preference_category:I = 0x7f090325

.field public static final account_preference_category:I = 0x7f090326

.field public static final accounts_title:I = 0x7f09003f

.field public static final acl_description_domain:I = 0x7f09048b

.field public static final acl_description_extended_network:I = 0x7f09048a

.field public static final acl_description_private_contacts:I = 0x7f090489

.field public static final acl_description_public:I = 0x7f090488

.field public static final acl_extended_network:I = 0x7f090481

.field public static final acl_limited:I = 0x7f090483

.field public static final acl_limited_album:I = 0x7f090487

.field public static final acl_private:I = 0x7f090484

.field public static final acl_private_album:I = 0x7f090485

.field public static final acl_private_contacts:I = 0x7f090480

.field public static final acl_public:I = 0x7f09047f

.field public static final acl_public_album:I = 0x7f090486

.field public static final acl_your_circles:I = 0x7f090482

.field public static final add:I = 0x7f090226

.field public static final add_email_dialog_hint:I = 0x7f09031c

.field public static final add_email_dialog_title:I = 0x7f09031b

.field public static final add_plus_one_description:I = 0x7f090463

.field public static final add_to_circle_action_label:I = 0x7f090321

.field public static final add_to_circle_confirmation_toast:I = 0x7f090331

.field public static final add_to_circle_email_picker_title:I = 0x7f090330

.field public static final add_to_circle_operation_pending:I = 0x7f0904a2

.field public static final add_to_circles:I = 0x7f09026d

.field public static final add_to_circles_dialog_new_circle:I = 0x7f090285

.field public static final add_to_circles_dialog_title:I = 0x7f090284

.field public static final added_to_circle_title:I = 0x7f090212

.field public static final album_photo_count_zero:I = 0x7f090097

.field public static final android_contact_group:I = 0x7f090440

.field public static final app_invite_default_action:I = 0x7f0904f9

.field public static final app_name:I = 0x7f090040

.field public static final apply:I = 0x7f090222

.field public static final apps_preference_category:I = 0x7f0900dd

.field public static final audience_display_circles:I = 0x7f090305

.field public static final audience_display_extended:I = 0x7f090304

.field public static final audience_selection_count_zero:I = 0x7f090306

.field public static final ban_user_also_remove_post:I = 0x7f090198

.field public static final ban_user_also_report_spam:I = 0x7f090199

.field public static final ban_user_dialog_title:I = 0x7f09019b

.field public static final ban_user_question:I = 0x7f09019a

.field public static final block_page_dialog_message:I = 0x7f0902db

.field public static final block_page_dialog_title:I = 0x7f0902d9

.field public static final block_page_operation_pending:I = 0x7f0902dd

.field public static final block_person_dialog_message:I = 0x7f0902da

.field public static final block_person_dialog_title:I = 0x7f0902d8

.field public static final block_person_operation_pending:I = 0x7f0902dc

.field public static final camera_photo_error:I = 0x7f0901da

.field public static final cancel:I = 0x7f090220

.field public static final cancel_send:I = 0x7f09010b

.field public static final cannot_add_to_circle_error_message:I = 0x7f0904a6

.field public static final cannot_add_to_circle_error_title:I = 0x7f0904a5

.field public static final card_auto_text_album:I = 0x7f090441

.field public static final card_auto_text_emotishare:I = 0x7f090449

.field public static final card_auto_text_event:I = 0x7f090444

.field public static final card_auto_text_image:I = 0x7f090442

.field public static final card_auto_text_link:I = 0x7f090447

.field public static final card_auto_text_location:I = 0x7f090446

.field public static final card_auto_text_review:I = 0x7f090448

.field public static final card_auto_text_skyjam:I = 0x7f090445

.field public static final card_auto_text_video:I = 0x7f090443

.field public static final card_event_declined_prompt:I = 0x7f090142

.field public static final card_event_going_prompt:I = 0x7f090140

.field public static final card_event_invited_prompt:I = 0x7f09013f

.field public static final card_event_maybe_prompt:I = 0x7f090141

.field public static final card_event_photo_missing_video:I = 0x7f090144

.field public static final card_hangout_state_active:I = 0x7f09044a

.field public static final card_hangout_state_inactive:I = 0x7f09044b

.field public static final card_square_gray_spam_for_moderator:I = 0x7f09044d

.field public static final card_square_gray_spam_for_moderator_approved:I = 0x7f09044e

.field public static final card_square_invite_invitation:I = 0x7f09044c

.field public static final change_cover_photo_dialog_title:I = 0x7f090353

.field public static final change_photo_crop_title:I = 0x7f09035d

.field public static final change_photo_dialog_title:I = 0x7f09034f

.field public static final change_photo_no_camera:I = 0x7f09035c

.field public static final change_photo_option_instant_upload:I = 0x7f090356

.field public static final change_photo_option_reposition:I = 0x7f090358

.field public static final change_photo_option_select_cover_photo:I = 0x7f090359

.field public static final change_photo_option_select_gallery:I = 0x7f090357

.field public static final change_photo_option_take_photo:I = 0x7f090354

.field public static final change_photo_option_your_photos:I = 0x7f090355

.field public static final checkin_places_error:I = 0x7f0901db

.field public static final circle_button_circles:I = 0x7f09031d

.field public static final circle_does_not_exist:I = 0x7f090324

.field public static final circle_properties_dialog_title:I = 0x7f090286

.field public static final circle_properties_operation_pending:I = 0x7f09028c

.field public static final circle_settings_amount:I = 0x7f09012b

.field public static final circle_settings_amount_all:I = 0x7f09012c

.field public static final circle_settings_amount_fewer:I = 0x7f09012f

.field public static final circle_settings_amount_more:I = 0x7f09012d

.field public static final circle_settings_amount_none:I = 0x7f090130

.field public static final circle_settings_amount_of_posts:I = 0x7f090131

.field public static final circle_settings_amount_standard:I = 0x7f09012e

.field public static final circle_settings_circle_subscription_section:I = 0x7f090132

.field public static final circle_settings_community_membership_section:I = 0x7f090134

.field public static final circle_settings_community_subscription_section:I = 0x7f090133

.field public static final circle_settings_from_this_circle:I = 0x7f090129

.field public static final circle_settings_from_this_community:I = 0x7f09012a

.field public static final circle_settings_get_notified:I = 0x7f090136

.field public static final circle_settings_notifications_disabled:I = 0x7f090137

.field public static final circle_settings_notifications_enabled:I = 0x7f090138

.field public static final circle_settings_posts_section:I = 0x7f090127

.field public static final circle_settings_saving:I = 0x7f090139

.field public static final circle_settings_show_posts:I = 0x7f090128

.field public static final circle_settings_subscribe:I = 0x7f090135

.field public static final clear:I = 0x7f090223

.field public static final collapse_more_info_content_description:I = 0x7f090475

.field public static final coming_soon:I = 0x7f090041

.field public static final comment_delete_error:I = 0x7f0901c3

.field public static final comment_delete_question:I = 0x7f090192

.field public static final comment_edit_error:I = 0x7f0901c2

.field public static final comment_moderate_error:I = 0x7f0901c4

.field public static final comment_post_error:I = 0x7f0901c1

.field public static final comment_quit_question:I = 0x7f090191

.field public static final comment_title:I = 0x7f090190

.field public static final comments_activity_not_found:I = 0x7f090202

.field public static final common_google_play_services_enable_button:I = 0x7f090006

.field public static final common_google_play_services_enable_text:I = 0x7f090005

.field public static final common_google_play_services_enable_title:I = 0x7f090004

.field public static final common_google_play_services_install_button:I = 0x7f090003

.field public static final common_google_play_services_install_text_phone:I = 0x7f090001

.field public static final common_google_play_services_install_text_tablet:I = 0x7f090002

.field public static final common_google_play_services_install_title:I = 0x7f090000

.field public static final common_google_play_services_unknown_issue:I = 0x7f090009

.field public static final common_google_play_services_update_button:I = 0x7f09000a

.field public static final common_google_play_services_update_text:I = 0x7f090008

.field public static final common_google_play_services_update_title:I = 0x7f090007

.field public static final communication_preference_device_location_button:I = 0x7f0900b8

.field public static final communication_preference_device_location_key:I = 0x7f090013

.field public static final communication_preference_hangout_button:I = 0x7f0900b7

.field public static final communication_preference_hangout_key:I = 0x7f090012

.field public static final communication_preference_messenger_button:I = 0x7f0900b6

.field public static final communication_preference_messenger_key:I = 0x7f090011

.field public static final communication_preference_notifications_button:I = 0x7f0900b5

.field public static final communication_preference_notifications_key:I = 0x7f090010

.field public static final communities_title:I = 0x7f090577

.field public static final compose_acl_separator:I = 0x7f0901a8

.field public static final compose_bar_share:I = 0x7f0901a5

.field public static final compose_checkin:I = 0x7f0901a2

.field public static final compose_comment_hint:I = 0x7f0901a6

.field public static final compose_comment_not_allowed_hint:I = 0x7f0901a7

.field public static final compose_emotishare2:I = 0x7f0901a3

.field public static final compose_link:I = 0x7f0901a1

.field public static final compose_photo:I = 0x7f0901a0

.field public static final compose_post:I = 0x7f0901a4

.field public static final contacts_stats_sync_preference_enabled_learn_more:I = 0x7f09033a

.field public static final contacts_stats_sync_preference_enabled_phone_summary:I = 0x7f090338

.field public static final contacts_stats_sync_preference_enabled_short_title:I = 0x7f090336

.field public static final contacts_stats_sync_preference_enabled_tablet_summary:I = 0x7f090339

.field public static final contacts_stats_sync_preference_enabled_title:I = 0x7f090337

.field public static final contacts_stats_sync_preference_key:I = 0x7f090036

.field public static final contacts_sync_preference_category:I = 0x7f090332

.field public static final contacts_sync_preference_enabled_short_title:I = 0x7f090333

.field public static final contacts_sync_preference_enabled_summary:I = 0x7f090335

.field public static final contacts_sync_preference_enabled_title:I = 0x7f090334

.field public static final contacts_sync_preference_key:I = 0x7f090035

.field public static final conversation_too_large:I = 0x7f09010e

.field public static final copied_to_clipboard:I = 0x7f09034d

.field public static final create_account_prompt:I = 0x7f090042

.field public static final create_event_activity_title:I = 0x7f0903ff

.field public static final create_event_server_error:I = 0x7f090218

.field public static final create_new_circle:I = 0x7f090316

.field public static final debug_realtimechat_default_backend:I = 0x7f09000d

.field public static final decline_call:I = 0x7f0903c2

.field public static final delete_circle_operation_pending:I = 0x7f090318

.field public static final delete_event_dialog_message:I = 0x7f090417

.field public static final delete_plusone_error:I = 0x7f0901ce

.field public static final device_location_acl_no_one:I = 0x7f090341

.field public static final device_location_acl_picker_title:I = 0x7f09033b

.field public static final device_location_assist:I = 0x7f0904fc

.field public static final device_location_best_available_manage_acl_key:I = 0x7f090025

.field public static final device_location_best_available_manage_acl_title:I = 0x7f090340

.field public static final device_location_enable_sharing_dialog_title:I = 0x7f09033f

.field public static final device_location_enable_sharing_key:I = 0x7f090024

.field public static final device_location_enable_sharing_title:I = 0x7f09033e

.field public static final device_location_header:I = 0x7f0904fb

.field public static final device_location_learn_more_preference_key:I = 0x7f090023

.field public static final device_location_learn_more_preference_title:I = 0x7f090343

.field public static final device_location_legal_summary:I = 0x7f090342

.field public static final device_location_more_than_three_no_count:I = 0x7f090348

.field public static final device_location_more_than_three_with_count:I = 0x7f090347

.field public static final device_location_no_network_key:I = 0x7f090027

.field public static final device_location_preference_no_network_alert:I = 0x7f090344

.field public static final device_location_reporting_settings_key:I = 0x7f090026

.field public static final device_location_three_acl_entries:I = 0x7f090346

.field public static final device_location_two_acl_entries:I = 0x7f090345

.field public static final dialog_inserting_camera_photo:I = 0x7f0903cc

.field public static final dialog_public_or_extended_circle_for_minor:I = 0x7f0903cd

.field public static final download_file_name_format:I = 0x7f090047

.field public static final download_photo_error:I = 0x7f0900a2

.field public static final download_photo_pending:I = 0x7f0900a0

.field public static final download_photo_retry:I = 0x7f0900a1

.field public static final download_photo_success:I = 0x7f0900a3

.field public static final edit_audience_content_description_remove:I = 0x7f090300

.field public static final edit_audience_content_description_remove_circle:I = 0x7f0902ff

.field public static final edit_audience_header_added:I = 0x7f090301

.field public static final edit_audience_header_circles:I = 0x7f0902fe

.field public static final edit_comment:I = 0x7f09021d

.field public static final edit_comment_cancel_prompt:I = 0x7f09021e

.field public static final edit_event_activity_title:I = 0x7f090419

.field public static final edit_event_quit_question:I = 0x7f090422

.field public static final edit_event_quit_title:I = 0x7f090421

.field public static final edit_location_content_description:I = 0x7f09046a

.field public static final edit_photo_content_description:I = 0x7f090471

.field public static final edit_post:I = 0x7f09021b

.field public static final edit_post_cancel_prompt:I = 0x7f09021c

.field public static final edit_post_error:I = 0x7f0901c7

.field public static final edit_text_content_description:I = 0x7f090470

.field public static final emotishare_description_anger:I = 0x7f09054f

.field public static final emotishare_description_astonished:I = 0x7f090553

.field public static final emotishare_description_crying:I = 0x7f090550

.field public static final emotishare_description_frustration:I = 0x7f090551

.field public static final emotishare_description_happy:I = 0x7f090552

.field public static final emotishare_description_joy:I = 0x7f090554

.field public static final emotishare_description_kiss:I = 0x7f090555

.field public static final emotishare_description_lol:I = 0x7f090556

.field public static final emotishare_description_new_year:I = 0x7f09055f

.field public static final emotishare_description_sad:I = 0x7f090558

.field public static final emotishare_description_sick:I = 0x7f090559

.field public static final emotishare_description_silly:I = 0x7f09055a

.field public static final emotishare_description_smirk:I = 0x7f09055b

.field public static final emotishare_description_spring:I = 0x7f090562

.field public static final emotishare_description_st_paddy:I = 0x7f090561

.field public static final emotishare_description_unamused:I = 0x7f090557

.field public static final emotishare_description_valentine:I = 0x7f090560

.field public static final emotishare_description_wink:I = 0x7f09055c

.field public static final emotishare_description_winter:I = 0x7f09055e

.field public static final emotishare_description_worried:I = 0x7f09055d

.field public static final emotishare_in_list_count:I = 0x7f0904fe

.field public static final emotishare_name_anger:I = 0x7f090563

.field public static final emotishare_name_astonished:I = 0x7f090567

.field public static final emotishare_name_crying:I = 0x7f090564

.field public static final emotishare_name_frustration:I = 0x7f090565

.field public static final emotishare_name_happy:I = 0x7f090566

.field public static final emotishare_name_joy:I = 0x7f090568

.field public static final emotishare_name_kiss:I = 0x7f090569

.field public static final emotishare_name_lol:I = 0x7f09056a

.field public static final emotishare_name_new_year:I = 0x7f090573

.field public static final emotishare_name_sad:I = 0x7f09056c

.field public static final emotishare_name_sick:I = 0x7f09056d

.field public static final emotishare_name_silly:I = 0x7f09056e

.field public static final emotishare_name_smirk:I = 0x7f09056f

.field public static final emotishare_name_spring:I = 0x7f090576

.field public static final emotishare_name_st_paddy:I = 0x7f090575

.field public static final emotishare_name_unamused:I = 0x7f09056b

.field public static final emotishare_name_valentine:I = 0x7f090574

.field public static final emotishare_name_wink:I = 0x7f090570

.field public static final emotishare_name_winter:I = 0x7f090572

.field public static final emotishare_name_worried:I = 0x7f090571

.field public static final emotishare_text_anger:I = 0x7f09053b

.field public static final emotishare_text_astonished:I = 0x7f09053f

.field public static final emotishare_text_crying:I = 0x7f09053c

.field public static final emotishare_text_frustration:I = 0x7f09053d

.field public static final emotishare_text_happy:I = 0x7f09053e

.field public static final emotishare_text_joy:I = 0x7f090540

.field public static final emotishare_text_kiss:I = 0x7f090541

.field public static final emotishare_text_lol:I = 0x7f090542

.field public static final emotishare_text_new_year:I = 0x7f09054b

.field public static final emotishare_text_sad:I = 0x7f090544

.field public static final emotishare_text_sick:I = 0x7f090545

.field public static final emotishare_text_silly:I = 0x7f090546

.field public static final emotishare_text_smirk:I = 0x7f090547

.field public static final emotishare_text_spring:I = 0x7f09054e

.field public static final emotishare_text_st_paddy:I = 0x7f09054d

.field public static final emotishare_text_unamused:I = 0x7f090543

.field public static final emotishare_text_valentine:I = 0x7f09054c

.field public static final emotishare_text_wink:I = 0x7f090548

.field public static final emotishare_text_winter:I = 0x7f09054a

.field public static final emotishare_text_worried:I = 0x7f090549

.field public static final empty_circle:I = 0x7f090216

.field public static final empty_conversation_list_text:I = 0x7f09034a

.field public static final enter_location_name:I = 0x7f0901e1

.field public static final error_creating_conversation:I = 0x7f09010f

.field public static final es_data:I = 0x7f090043

.field public static final es_google_iu_provider:I = 0x7f090045

.field public static final es_google_photo_provider:I = 0x7f090044

.field public static final event_absolute_date_format:I = 0x7f0903f1

.field public static final event_activity_checked_in:I = 0x7f09015a

.field public static final event_activity_checked_in_1:I = 0x7f090156

.field public static final event_activity_checked_in_2:I = 0x7f090157

.field public static final event_activity_checked_in_3:I = 0x7f090158

.field public static final event_activity_checked_in_4:I = 0x7f090159

.field public static final event_activity_invite:I = 0x7f09014b

.field public static final event_activity_invite_1:I = 0x7f090147

.field public static final event_activity_invite_2:I = 0x7f090148

.field public static final event_activity_invite_3:I = 0x7f090149

.field public static final event_activity_invite_4:I = 0x7f09014a

.field public static final event_activity_rsvp_no:I = 0x7f090155

.field public static final event_activity_rsvp_no_1:I = 0x7f090151

.field public static final event_activity_rsvp_no_2:I = 0x7f090152

.field public static final event_activity_rsvp_no_3:I = 0x7f090153

.field public static final event_activity_rsvp_no_4:I = 0x7f090154

.field public static final event_activity_rsvp_yes:I = 0x7f090150

.field public static final event_activity_rsvp_yes_1:I = 0x7f09014c

.field public static final event_activity_rsvp_yes_2:I = 0x7f09014d

.field public static final event_activity_rsvp_yes_3:I = 0x7f09014e

.field public static final event_activity_rsvp_yes_4:I = 0x7f09014f

.field public static final event_activity_title:I = 0x7f0903dd

.field public static final event_button_add_comment_label:I = 0x7f09016a

.field public static final event_button_add_event_label:I = 0x7f090168

.field public static final event_button_add_photo_label:I = 0x7f090169

.field public static final event_button_add_photos_label:I = 0x7f09016d

.field public static final event_button_create_event_label:I = 0x7f09016b

.field public static final event_button_invite_more_label:I = 0x7f09016c

.field public static final event_button_view_all_invitees:I = 0x7f090167

.field public static final event_change_theme:I = 0x7f090409

.field public static final event_comment_dialog_title:I = 0x7f090178

.field public static final event_comment_sending:I = 0x7f0903f6

.field public static final event_create_successful:I = 0x7f090170

.field public static final event_create_text:I = 0x7f09016e

.field public static final event_deleting:I = 0x7f090418

.field public static final event_description_hint:I = 0x7f090407

.field public static final event_detail_hangout_before:I = 0x7f090165

.field public static final event_detail_hangout_during:I = 0x7f090166

.field public static final event_detail_hangout_title:I = 0x7f090164

.field public static final event_detail_instantshare_title:I = 0x7f09016f

.field public static final event_detail_on_air:I = 0x7f09015b

.field public static final event_detail_private:I = 0x7f09015c

.field public static final event_detail_public:I = 0x7f09015d

.field public static final event_detail_rsvp_invited_count:I = 0x7f090162

.field public static final event_detail_rsvp_invited_count_past:I = 0x7f090163

.field public static final event_detail_rsvp_maybe_count:I = 0x7f09015f

.field public static final event_detail_rsvp_yes_count:I = 0x7f090160

.field public static final event_detail_rsvp_yes_count_past:I = 0x7f090161

.field public static final event_details_error:I = 0x7f0903df

.field public static final event_details_rsvp_guests_count:I = 0x7f09015e

.field public static final event_does_not_exist:I = 0x7f0903e0

.field public static final event_enable_sync_dialog_message:I = 0x7f0903f0

.field public static final event_end_date_format:I = 0x7f0903f5

.field public static final event_end_date_hint:I = 0x7f090406

.field public static final event_end_time_hint:I = 0x7f090405

.field public static final event_hangout_checkbox_label:I = 0x7f090408

.field public static final event_hangout_text:I = 0x7f0903de

.field public static final event_instant_share_dialog_checkin:I = 0x7f0903eb

.field public static final event_instant_share_dialog_content:I = 0x7f0903e7

.field public static final event_instant_share_dialog_learn_more:I = 0x7f0903ea

.field public static final event_instant_share_dialog_positive:I = 0x7f0903ed

.field public static final event_instant_share_dialog_positive_first:I = 0x7f0903ec

.field public static final event_instant_share_dialog_title:I = 0x7f0903e6

.field public static final event_instant_share_enabled_notification_subtitle:I = 0x7f0903ee

.field public static final event_instant_share_on_air_dialog_content:I = 0x7f0903e8

.field public static final event_instant_share_public_dialog_content:I = 0x7f0903e9

.field public static final event_invite_activity_title:I = 0x7f09040e

.field public static final event_invitee_list_section_attended:I = 0x7f0903f8

.field public static final event_invitee_list_section_attending:I = 0x7f0903f7

.field public static final event_invitee_list_section_did_not_respond:I = 0x7f0903fd

.field public static final event_invitee_list_section_didnt_go:I = 0x7f0903fb

.field public static final event_invitee_list_section_maybe:I = 0x7f0903f9

.field public static final event_invitee_list_section_not_attending:I = 0x7f0903fa

.field public static final event_invitee_list_section_not_responded:I = 0x7f0903fc

.field public static final event_invitee_list_section_removed:I = 0x7f0903fe

.field public static final event_invitee_read_status_read:I = 0x7f090143

.field public static final event_invitees_hint:I = 0x7f090402

.field public static final event_inviting_more:I = 0x7f090414

.field public static final event_location_accessibility_description:I = 0x7f09047a

.field public static final event_location_activity_title:I = 0x7f09040f

.field public static final event_location_add:I = 0x7f090412

.field public static final event_location_hint:I = 0x7f090403

.field public static final event_location_none_description:I = 0x7f090411

.field public static final event_location_none_title:I = 0x7f090410

.field public static final event_master_sync_dialog_message:I = 0x7f0903ef

.field public static final event_name_hint:I = 0x7f090400

.field public static final event_no_audience_hint:I = 0x7f0901b1

.field public static final event_no_time_hint:I = 0x7f0901b2

.field public static final event_no_title_hint:I = 0x7f0901b3

.field public static final event_photo_share_failed_toast:I = 0x7f0902fb

.field public static final event_picker_activity_title:I = 0x7f09040a

.field public static final event_post_photo:I = 0x7f09041a

.field public static final event_reinvite_invitee:I = 0x7f09041c

.field public static final event_reinviting_invitee:I = 0x7f09041e

.field public static final event_relative_end_date_format:I = 0x7f0903f4

.field public static final event_relative_start_date_format:I = 0x7f0903f2

.field public static final event_remove_invitee:I = 0x7f09041b

.field public static final event_removing_invitee:I = 0x7f09041d

.field public static final event_report_question:I = 0x7f09019f

.field public static final event_rsvp_accessibility_description:I = 0x7f09047b

.field public static final event_rsvp_are_you_going:I = 0x7f0903e2

.field public static final event_rsvp_are_you_going_past:I = 0x7f0903e3

.field public static final event_rsvp_attending:I = 0x7f0903e1

.field public static final event_rsvp_maybe:I = 0x7f0903e4

.field public static final event_rsvp_not_attending:I = 0x7f0903e5

.field public static final event_save_successful:I = 0x7f090171

.field public static final event_send_rsvp:I = 0x7f090172

.field public static final event_start_date_format:I = 0x7f0903f3

.field public static final event_start_time_hint:I = 0x7f090404

.field public static final event_tab_event_text:I = 0x7f0903db

.field public static final event_tab_photos_text:I = 0x7f0903dc

.field public static final event_theme_list_empty:I = 0x7f09040d

.field public static final event_themes_featured_tab_text:I = 0x7f09040b

.field public static final event_themes_patterns_tab_text:I = 0x7f09040c

.field public static final event_update_operation_pending:I = 0x7f090401

.field public static final events_section_past:I = 0x7f09013e

.field public static final events_section_upcoming:I = 0x7f09013d

.field public static final expand_more_info_content_description:I = 0x7f090474

.field public static final find_people_all_interesting:I = 0x7f09049d

.field public static final find_people_all_you_may_know:I = 0x7f090491

.field public static final find_people_classmates:I = 0x7f090499

.field public static final find_people_classmates_help_details:I = 0x7f09049b

.field public static final find_people_classmates_help_title:I = 0x7f09049a

.field public static final find_people_coworkers:I = 0x7f090496

.field public static final find_people_coworkers_help_details:I = 0x7f090498

.field public static final find_people_coworkers_help_title:I = 0x7f090497

.field public static final find_people_friends_add:I = 0x7f090493

.field public static final find_people_friends_add_desc:I = 0x7f090494

.field public static final find_people_get_more_suggestions:I = 0x7f090492

.field public static final find_people_interesting:I = 0x7f09049c

.field public static final find_people_nobody_blocked:I = 0x7f0904a1

.field public static final find_people_suggestions:I = 0x7f090495

.field public static final find_people_view_all:I = 0x7f09049f

.field public static final find_people_view_all_count:I = 0x7f0904a0

.field public static final find_people_your_circles:I = 0x7f09049e

.field public static final finding_your_location:I = 0x7f0901df

.field public static final first_circle_picker_alert_shown_key:I = 0x7f09003e

.field public static final follow:I = 0x7f09026e

.field public static final following_circle_name:I = 0x7f09048e

.field public static final friends_circle_name:I = 0x7f09048d

.field public static final from_circle:I = 0x7f0904a7

.field public static final from_your_phone_initiate_share:I = 0x7f0900b2

.field public static final full_size_first_time_notification_text:I = 0x7f09050a

.field public static final full_size_first_time_notification_title:I = 0x7f090509

.field public static final full_size_gigabyte:I = 0x7f090500

.field public static final full_size_low_quota_text:I = 0x7f090505

.field public static final full_size_megabyte:I = 0x7f0904ff

.field public static final full_size_no_quota_text:I = 0x7f090504

.field public static final full_size_terabyte:I = 0x7f090501

.field public static final get_acl_error:I = 0x7f0901cf

.field public static final google_plus_account_label:I = 0x7f090322

.field public static final hangout_abuse_learn_more:I = 0x7f09038c

.field public static final hangout_abuse_ok_button_text:I = 0x7f090389

.field public static final hangout_abuse_warning_header:I = 0x7f09038a

.field public static final hangout_abuse_warning_message:I = 0x7f09038b

.field public static final hangout_already_ended:I = 0x7f0903ac

.field public static final hangout_anonymous_person:I = 0x7f0903a5

.field public static final hangout_audio_video_error:I = 0x7f090395

.field public static final hangout_audiofocus_loss_warning:I = 0x7f0903ad

.field public static final hangout_authentication_error:I = 0x7f090378

.field public static final hangout_avatar_menu_pin_video:I = 0x7f0903ae

.field public static final hangout_avatar_menu_remote_mute:I = 0x7f0903b0

.field public static final hangout_avatar_menu_unpin_video:I = 0x7f0903af

.field public static final hangout_broadcast_view:I = 0x7f09036f

.field public static final hangout_camera_error:I = 0x7f0903a6

.field public static final hangout_common_menu_exit:I = 0x7f090374

.field public static final hangout_crash_report_sent_failed:I = 0x7f0903b9

.field public static final hangout_crash_report_sent_succeeded:I = 0x7f0903b8

.field public static final hangout_create:I = 0x7f090372

.field public static final hangout_default_device_name:I = 0x7f090362

.field public static final hangout_dinging_content:I = 0x7f0903c9

.field public static final hangout_dinging_content_title:I = 0x7f0903c8

.field public static final hangout_dinging_ticker:I = 0x7f0903c7

.field public static final hangout_dingtone_setting_key:I = 0x7f09001d

.field public static final hangout_enter_blocked_error:I = 0x7f09037c

.field public static final hangout_enter_blocking_error:I = 0x7f09037d

.field public static final hangout_enter_green_room_info_error:I = 0x7f090380

.field public static final hangout_enter_greenroom:I = 0x7f09036e

.field public static final hangout_enter_hangout_over:I = 0x7f090381

.field public static final hangout_enter_max_users_error:I = 0x7f09037e

.field public static final hangout_enter_outdated_client_error:I = 0x7f090382

.field public static final hangout_enter_server_error:I = 0x7f09037f

.field public static final hangout_enter_timeout_error:I = 0x7f09037b

.field public static final hangout_enter_unknown_error:I = 0x7f09037a

.field public static final hangout_enter_upgrade:I = 0x7f090384

.field public static final hangout_exit_generic:I = 0x7f090392

.field public static final hangout_exited:I = 0x7f090393

.field public static final hangout_fatal_error:I = 0x7f090376

.field public static final hangout_green_room_instructions:I = 0x7f090370

.field public static final hangout_help_url:I = 0x7f09036d

.field public static final hangout_invites_sent:I = 0x7f0903b5

.field public static final hangout_launch_already_in_hangout:I = 0x7f090375

.field public static final hangout_launch_join:I = 0x7f090371

.field public static final hangout_launch_joining:I = 0x7f090399

.field public static final hangout_launch_signing_in:I = 0x7f090398

.field public static final hangout_log_upload_message:I = 0x7f0903b3

.field public static final hangout_log_upload_title:I = 0x7f0903b2

.field public static final hangout_media_block:I = 0x7f0903a3

.field public static final hangout_media_block_by_self:I = 0x7f0903a1

.field public static final hangout_media_block_to_self:I = 0x7f0903a2

.field public static final hangout_member_entering_meeting:I = 0x7f09039b

.field public static final hangout_member_exiting_meeting:I = 0x7f09039c

.field public static final hangout_member_unable_to_join:I = 0x7f09039d

.field public static final hangout_menu_audio_mute:I = 0x7f090363

.field public static final hangout_menu_audio_unmute:I = 0x7f090364

.field public static final hangout_menu_invite:I = 0x7f0903b1

.field public static final hangout_menu_switch_camera:I = 0x7f090365

.field public static final hangout_menu_transfer:I = 0x7f0903b6

.field public static final hangout_menu_video_mute:I = 0x7f090366

.field public static final hangout_menu_video_unmute:I = 0x7f090367

.field public static final hangout_minor_dontshow_again:I = 0x7f090388

.field public static final hangout_minor_ok_button_text:I = 0x7f090385

.field public static final hangout_minor_warning_header:I = 0x7f090386

.field public static final hangout_minor_warning_message:I = 0x7f090387

.field public static final hangout_missed_notification_content:I = 0x7f0903cb

.field public static final hangout_missed_notification_title:I = 0x7f0903ca

.field public static final hangout_native_lib_error:I = 0x7f090383

.field public static final hangout_network_error:I = 0x7f090377

.field public static final hangout_no_one_joined:I = 0x7f0903ab

.field public static final hangout_no_participants:I = 0x7f09039a

.field public static final hangout_not_supported_account:I = 0x7f09036a

.field public static final hangout_not_supported_child:I = 0x7f09036b

.field public static final hangout_not_supported_device:I = 0x7f090369

.field public static final hangout_not_supported_os:I = 0x7f090368

.field public static final hangout_not_supported_type:I = 0x7f09036c

.field public static final hangout_notify_setting_key:I = 0x7f09001e

.field public static final hangout_onair_cancel_button_text:I = 0x7f09038e

.field public static final hangout_onair_checkbox_text:I = 0x7f090391

.field public static final hangout_onair_ok_button_text:I = 0x7f09038d

.field public static final hangout_onair_warning_header:I = 0x7f09038f

.field public static final hangout_onair_warning_message:I = 0x7f090390

.field public static final hangout_ongoing_notify_text:I = 0x7f090397

.field public static final hangout_ongoing_notify_title:I = 0x7f090396

.field public static final hangout_participants_title:I = 0x7f0903b4

.field public static final hangout_recording_abuse:I = 0x7f0903a4

.field public static final hangout_remote_mute:I = 0x7f0903a0

.field public static final hangout_remote_mute_by_self:I = 0x7f09039e

.field public static final hangout_remote_mute_to_self:I = 0x7f09039f

.field public static final hangout_resume:I = 0x7f090373

.field public static final hangout_ring_off_content_description:I = 0x7f090477

.field public static final hangout_ring_on_content_description:I = 0x7f090478

.field public static final hangout_ringing_accept:I = 0x7f0903c5

.field public static final hangout_ringing_ignore:I = 0x7f0903c6

.field public static final hangout_ringing_incoming:I = 0x7f0903c4

.field public static final hangout_ringtone_directory:I = 0x7f090021

.field public static final hangout_ringtone_name:I = 0x7f090022

.field public static final hangout_ringtone_setting_default_value:I = 0x7f090020

.field public static final hangout_ringtone_setting_key:I = 0x7f09001c

.field public static final hangout_settings_notify_title:I = 0x7f0903bc

.field public static final hangout_settings_ringtone_title:I = 0x7f0903be

.field public static final hangout_settings_vibrate_title:I = 0x7f0903bd

.field public static final hangout_signin_timeout_error:I = 0x7f090379

.field public static final hangout_transfer_sent:I = 0x7f0903b7

.field public static final hangout_unknown_error:I = 0x7f090394

.field public static final hangout_vibrate_setting_key:I = 0x7f09001f

.field public static final hangout_waiting_for_more_than_two_participants:I = 0x7f0903a9

.field public static final hangout_waiting_for_participant:I = 0x7f0903a7

.field public static final hangout_waiting_for_participants:I = 0x7f0903aa

.field public static final hangout_waiting_for_two_participants:I = 0x7f0903a8

.field public static final home_menu_feedback:I = 0x7f09007b

.field public static final home_menu_notifications:I = 0x7f090079

.field public static final home_menu_settings:I = 0x7f09007a

.field public static final home_screen_events_label:I = 0x7f090076

.field public static final home_screen_hangout_label:I = 0x7f090075

.field public static final home_screen_huddle_label:I = 0x7f090071

.field public static final home_screen_local_label:I = 0x7f090077

.field public static final home_screen_people_label:I = 0x7f090072

.field public static final home_screen_photos_label:I = 0x7f090074

.field public static final home_screen_profile_label:I = 0x7f090070

.field public static final home_screen_squares_label:I = 0x7f090073

.field public static final home_stream_label:I = 0x7f090078

.field public static final huddle_help_text:I = 0x7f09034e

.field public static final huddle_title:I = 0x7f09034b

.field public static final image_file_name_format:I = 0x7f090046

.field public static final instant_share_after_description:I = 0x7f090177

.field public static final instant_share_description:I = 0x7f090176

.field public static final instant_upload_notification_title:I = 0x7f090502

.field public static final invite:I = 0x7f0901b6

.field public static final invite_contact_label:I = 0x7f09032f

.field public static final key_acl_setting_anyone:I = 0x7f0903bf

.field public static final key_acl_setting_extended_circles:I = 0x7f0903c1

.field public static final key_acl_setting_my_circles:I = 0x7f0903c0

.field public static final label_person_blocked:I = 0x7f0902d7

.field public static final license_preference_key:I = 0x7f090037

.field public static final loading:I = 0x7f0900b3

.field public static final loading_friend_suggestions:I = 0x7f09048f

.field public static final loading_panorama:I = 0x7f09050c

.field public static final local_review_more_reviews:I = 0x7f09043f

.field public static final local_review_title:I = 0x7f09043e

.field public static final location_lat_long_format:I = 0x7f09045e

.field public static final location_provider_disabled:I = 0x7f0901dc

.field public static final location_provider_disabled_in_stream:I = 0x7f0901dd

.field public static final location_provider_disabled_settings:I = 0x7f0901de

.field public static final location_reporting_settings_summary:I = 0x7f09033d

.field public static final location_reporting_settings_title:I = 0x7f09033c

.field public static final locations_map_cannot_load_message:I = 0x7f09057b

.field public static final locations_map_default_title:I = 0x7f09057c

.field public static final main_menu_content_description:I = 0x7f090467

.field public static final master_sync_disabled_summary:I = 0x7f0900e0

.field public static final media_not_found_message:I = 0x7f09008f

.field public static final media_processing_message:I = 0x7f09008d

.field public static final media_processing_message_subtitle:I = 0x7f09008e

.field public static final media_progress:I = 0x7f090512

.field public static final media_reason_dont_upload:I = 0x7f09051d

.field public static final media_reason_instant_share:I = 0x7f09051e

.field public static final media_reason_instant_upload:I = 0x7f09051f

.field public static final media_reason_manual:I = 0x7f090520

.field public static final media_reason_unknown:I = 0x7f090522

.field public static final media_reason_upload_all:I = 0x7f090521

.field public static final media_state:I = 0x7f090511

.field public static final media_state_failed:I = 0x7f09051b

.field public static final media_state_not_uploaded:I = 0x7f090519

.field public static final media_state_pending:I = 0x7f090517

.field public static final media_state_queued:I = 0x7f090518

.field public static final media_state_removed:I = 0x7f090515

.field public static final media_state_unknown:I = 0x7f09051c

.field public static final media_state_uploaded:I = 0x7f09051a

.field public static final media_state_uploading:I = 0x7f090516

.field public static final media_status_background_disabled:I = 0x7f09052a

.field public static final media_status_cancelled:I = 0x7f090539

.field public static final media_status_downsync_disabled:I = 0x7f090529

.field public static final media_status_duplicate:I = 0x7f090534

.field public static final media_status_fail_no_storage:I = 0x7f090532

.field public static final media_status_fail_user_auth:I = 0x7f090531

.field public static final media_status_google_exif:I = 0x7f090537

.field public static final media_status_in_progress:I = 0x7f090524

.field public static final media_status_invalid_metadata:I = 0x7f090533

.field public static final media_status_is_disabled:I = 0x7f090536

.field public static final media_status_network_exception:I = 0x7f09052f

.field public static final media_status_no_fingerprint:I = 0x7f090535

.field public static final media_status_no_network:I = 0x7f09052e

.field public static final media_status_no_power:I = 0x7f090527

.field public static final media_status_no_quota:I = 0x7f090530

.field public static final media_status_no_storage:I = 0x7f09052d

.field public static final media_status_no_wifi:I = 0x7f090525

.field public static final media_status_ok:I = 0x7f090523

.field public static final media_status_roaming:I = 0x7f090526

.field public static final media_status_skipped:I = 0x7f090538

.field public static final media_status_unknown:I = 0x7f09053a

.field public static final media_status_upsync_disabled:I = 0x7f090528

.field public static final media_status_user_auth:I = 0x7f09052c

.field public static final media_status_yielded:I = 0x7f09052b

.field public static final menu_add_plus_one:I = 0x7f09017f

.field public static final menu_blocked_circle:I = 0x7f090462

.field public static final menu_checkin:I = 0x7f090179

.field public static final menu_choose_photo_from_gallery:I = 0x7f09017e

.field public static final menu_circle_settings:I = 0x7f090461

.field public static final menu_delete_circle:I = 0x7f090317

.field public static final menu_delete_comment:I = 0x7f09018a

.field public static final menu_delete_event:I = 0x7f090416

.field public static final menu_delete_photo:I = 0x7f090082

.field public static final menu_download_photo:I = 0x7f09018d

.field public static final menu_edit:I = 0x7f090181

.field public static final menu_edit_comment:I = 0x7f0901c0

.field public static final menu_edit_event:I = 0x7f090415

.field public static final menu_home_help:I = 0x7f09007d

.field public static final menu_home_privacy_policy:I = 0x7f09007e

.field public static final menu_home_sign_out:I = 0x7f09007c

.field public static final menu_home_terms_of_service:I = 0x7f09007f

.field public static final menu_invite_more:I = 0x7f090413

.field public static final menu_item_ban_user:I = 0x7f0902d5

.field public static final menu_item_block_person:I = 0x7f0902cf

.field public static final menu_item_block_profile:I = 0x7f0902d1

.field public static final menu_item_delete_post:I = 0x7f0902d6

.field public static final menu_item_mute_person:I = 0x7f0902cd

.field public static final menu_item_remove_post:I = 0x7f0902d4

.field public static final menu_item_report_abuse:I = 0x7f0902d3

.field public static final menu_item_unblock_person:I = 0x7f0902d0

.field public static final menu_item_unblock_profile:I = 0x7f0902d2

.field public static final menu_item_unmute_person:I = 0x7f0902ce

.field public static final menu_mute_post:I = 0x7f090185

.field public static final menu_network_clear:I = 0x7f0903ce

.field public static final menu_network_customize:I = 0x7f0903cf

.field public static final menu_new_conversation:I = 0x7f09028d

.field public static final menu_photo_chooser:I = 0x7f09017c

.field public static final menu_photo_share:I = 0x7f09017d

.field public static final menu_plus_oned_by:I = 0x7f0901fa

.field public static final menu_post:I = 0x7f09017b

.field public static final menu_postphoto:I = 0x7f09017a

.field public static final menu_refresh:I = 0x7f09013b

.field public static final menu_refresh_photo:I = 0x7f090080

.field public static final menu_remove_deleted_media:I = 0x7f09050d

.field public static final menu_remove_plus_one:I = 0x7f090180

.field public static final menu_remove_post:I = 0x7f090182

.field public static final menu_remove_tag:I = 0x7f090083

.field public static final menu_report_abuse:I = 0x7f090189

.field public static final menu_report_photo:I = 0x7f090085

.field public static final menu_rescan_media:I = 0x7f09050e

.field public static final menu_reshare_post:I = 0x7f090183

.field public static final menu_search:I = 0x7f0902fc

.field public static final menu_search_posts:I = 0x7f0902fd

.field public static final menu_select_item:I = 0x7f090081

.field public static final menu_set_profile_photo:I = 0x7f09018b

.field public static final menu_set_wallpaper_photo:I = 0x7f09018c

.field public static final menu_share_link:I = 0x7f090187

.field public static final menu_share_link_toast:I = 0x7f090188

.field public static final menu_show_post_location:I = 0x7f090184

.field public static final menu_unmute_post:I = 0x7f090186

.field public static final menu_uploadstats_option_remove:I = 0x7f090510

.field public static final menu_uploadstats_option_upload:I = 0x7f09050f

.field public static final moving_between_circles_operation_pending:I = 0x7f0904a4

.field public static final mute_activity_error:I = 0x7f0901cb

.field public static final mute_dialog_content_female:I = 0x7f0902e7

.field public static final mute_dialog_content_general:I = 0x7f0902e8

.field public static final mute_dialog_content_male:I = 0x7f0902e6

.field public static final mute_dialog_title:I = 0x7f0902e5

.field public static final mute_operation_pending:I = 0x7f0902e9

.field public static final my_city:I = 0x7f090205

.field public static final my_location:I = 0x7f090204

.field public static final nav_up_content_description:I = 0x7f090468

.field public static final network_statistics_total:I = 0x7f0903d0

.field public static final network_statistics_value:I = 0x7f0903d1

.field public static final network_stats_preference_key:I = 0x7f09003c

.field public static final network_transaction_duration:I = 0x7f0903d5

.field public static final network_transaction_many_bytes:I = 0x7f0903d4

.field public static final network_transaction_one_bytes:I = 0x7f0903d3

.field public static final network_transactions_preference_key:I = 0x7f09003b

.field public static final new_circle_dialog_hint:I = 0x7f090288

.field public static final new_circle_dialog_title:I = 0x7f090287

.field public static final new_circle_just_following_message:I = 0x7f09028b

.field public static final new_circle_just_following_title:I = 0x7f09028a

.field public static final new_circle_operation_pending:I = 0x7f090289

.field public static final new_conversation_description:I = 0x7f09034c

.field public static final new_event_quit_question:I = 0x7f090420

.field public static final new_event_quit_title:I = 0x7f09041f

.field public static final new_hangout_label:I = 0x7f090113

.field public static final new_huddle_label:I = 0x7f090112

.field public static final ninety_nine_plus:I = 0x7f090096

.field public static final no:I = 0x7f090224

.field public static final no_added_to_circle_person:I = 0x7f090213

.field public static final no_albums:I = 0x7f090090

.field public static final no_circles:I = 0x7f090215

.field public static final no_connection:I = 0x7f0900b4

.field public static final no_emotishares:I = 0x7f0904fd

.field public static final no_events:I = 0x7f09011f

.field public static final no_location_attached:I = 0x7f0901e0

.field public static final no_locations:I = 0x7f0901d9

.field public static final no_network_transactions:I = 0x7f0903d2

.field public static final no_notifications:I = 0x7f0901d8

.field public static final no_people_in_circles:I = 0x7f090214

.field public static final no_photos:I = 0x7f090091

.field public static final no_photos_left:I = 0x7f090092

.field public static final no_posts:I = 0x7f09011e

.field public static final no_square_members:I = 0x7f090121

.field public static final no_squares:I = 0x7f090120

.field public static final no_video:I = 0x7f090093

.field public static final not_signed_in:I = 0x7f09006f

.field public static final notification_actors_for_action_description:I = 0x7f0900d9

.field public static final notification_content_added_back_to_circle:I = 0x7f090241

.field public static final notification_content_added_to_circle:I = 0x7f090240

.field public static final notification_event_deleted:I = 0x7f090245

.field public static final notification_photo_deleted:I = 0x7f090246

.field public static final notification_post_deleted:I = 0x7f090244

.field public static final notification_settings_save_failed:I = 0x7f0900d8

.field public static final notification_square_invite:I = 0x7f0900d6

.field public static final notification_square_membership_approved:I = 0x7f0900d1

.field public static final notification_square_new_moderator:I = 0x7f0900d4

.field public static final notification_square_new_owner:I = 0x7f0900d5

.field public static final notification_square_promoted_to_mod:I = 0x7f0900d2

.field public static final notification_square_promoted_to_owner:I = 0x7f0900d3

.field public static final notification_square_single_post_share:I = 0x7f0900d7

.field public static final notification_title_added_back_to_circle:I = 0x7f090243

.field public static final notification_title_added_to_circle:I = 0x7f090242

.field public static final notifications_action_add_all_to_friends:I = 0x7f0900c3

.field public static final notifications_action_add_to_friends:I = 0x7f0900c2

.field public static final notifications_action_added_to_friends:I = 0x7f0900c4

.field public static final notifications_action_comment:I = 0x7f0900c0

.field public static final notifications_action_plusone_post:I = 0x7f0900c1

.field public static final notifications_action_say_hello:I = 0x7f0900c5

.field public static final notifications_instant_upload_more_than_two_taggee_names:I = 0x7f0900c7

.field public static final notifications_instant_upload_share_with_her:I = 0x7f0900c8

.field public static final notifications_instant_upload_share_with_him:I = 0x7f0900c9

.field public static final notifications_instant_upload_share_with_them:I = 0x7f0900ca

.field public static final notifications_instant_upload_two_taggee_names:I = 0x7f0900c6

.field public static final notifications_preference_enabled_key:I = 0x7f090014

.field public static final notifications_preference_enabled_summary:I = 0x7f0900ba

.field public static final notifications_preference_enabled_title:I = 0x7f0900b9

.field public static final notifications_preference_no_network_alert:I = 0x7f0900bf

.field public static final notifications_preference_no_network_category:I = 0x7f0900be

.field public static final notifications_preference_ringtone_default_value:I = 0x7f090017

.field public static final notifications_preference_ringtone_key:I = 0x7f090016

.field public static final notifications_preference_ringtone_summary:I = 0x7f0900bd

.field public static final notifications_preference_ringtone_title:I = 0x7f0900bc

.field public static final notifications_preference_vibrate_key:I = 0x7f090015

.field public static final notifications_preference_vibrate_title:I = 0x7f0900bb

.field public static final notifications_single_post_action_comment:I = 0x7f0900cb

.field public static final notifications_single_post_action_mention:I = 0x7f0900ce

.field public static final notifications_single_post_action_plus_one:I = 0x7f0900cc

.field public static final notifications_single_post_action_post:I = 0x7f0900d0

.field public static final notifications_single_post_action_reshare:I = 0x7f0900cd

.field public static final notifications_single_post_action_share:I = 0x7f0900cf

.field public static final ok:I = 0x7f09021f

.field public static final okay_got_it:I = 0x7f090225

.field public static final oob_contacts_sync_title:I = 0x7f09006c

.field public static final oob_enable_sync_dialog_message:I = 0x7f09005b

.field public static final oob_enable_sync_dialog_title:I = 0x7f09005a

.field public static final oob_first_circle_picker_alert_message:I = 0x7f09006e

.field public static final oob_first_circle_picker_alert_title:I = 0x7f09006d

.field public static final oob_instant_upload_disclaimer:I = 0x7f090062

.field public static final oob_instant_upload_info:I = 0x7f090061

.field public static final oob_instant_upload_option_disable:I = 0x7f090065

.field public static final oob_instant_upload_option_wifi_and_mobile:I = 0x7f090063

.field public static final oob_instant_upload_option_wifi_only:I = 0x7f090064

.field public static final oob_instant_upload_title:I = 0x7f090060

.field public static final oob_master_sync_dialog_message:I = 0x7f090059

.field public static final oob_master_sync_dialog_title:I = 0x7f090058

.field public static final oob_plus_page_name:I = 0x7f09005e

.field public static final oob_profile_photo_add_photo_button:I = 0x7f09006a

.field public static final oob_profile_photo_change_photo_button:I = 0x7f09006b

.field public static final oob_profile_photo_desc_after_change_tap_done:I = 0x7f090069

.field public static final oob_profile_photo_desc_after_change_tap_next:I = 0x7f090068

.field public static final oob_profile_photo_desc_before_change:I = 0x7f090067

.field public static final oob_profile_photo_title:I = 0x7f090066

.field public static final oob_select_plus_page_item_create_profile:I = 0x7f09005d

.field public static final oob_select_plus_page_title:I = 0x7f09005c

.field public static final operation_failed:I = 0x7f0901d6

.field public static final originally_shared:I = 0x7f090200

.field public static final originally_shared_post:I = 0x7f090201

.field public static final participant_count:I = 0x7f090270

.field public static final party_mode_expired_text:I = 0x7f090506

.field public static final party_mode_expired_title:I = 0x7f090503

.field public static final people_list_error:I = 0x7f090219

.field public static final people_public_circle:I = 0x7f09027e

.field public static final people_search_job:I = 0x7f09048c

.field public static final people_search_public_error:I = 0x7f09031a

.field public static final people_search_public_not_found:I = 0x7f090319

.field public static final permdesc_picasastore:I = 0x7f0904f8

.field public static final permlab_picasastore:I = 0x7f0904f7

.field public static final person_entry_content_description:I = 0x7f09023d

.field public static final person_entry_email_content_description:I = 0x7f09023f

.field public static final person_with_subtitle_entry_content_description:I = 0x7f09023e

.field public static final photo_buy_quota_preference_key:I = 0x7f09002f

.field public static final photo_buy_quota_preference_summary:I = 0x7f0900f7

.field public static final photo_buy_quota_preference_title:I = 0x7f0900f6

.field public static final photo_connection_preference_default_value:I = 0x7f090031

.field public static final photo_connection_preference_key:I = 0x7f090029

.field public static final photo_connection_preference_summary_mobile:I = 0x7f0900e7

.field public static final photo_connection_preference_summary_wifi:I = 0x7f0900e6

.field public static final photo_connection_preference_title:I = 0x7f0900e5

.field public static final photo_connection_wifi_and_mobile:I = 0x7f0900e8

.field public static final photo_connection_wifi_only:I = 0x7f0900e9

.field public static final photo_gallery_content_description:I = 0x7f0901b0

.field public static final photo_in_list_count:I = 0x7f090098

.field public static final photo_instant_upload_preference_key:I = 0x7f090028

.field public static final photo_instant_upload_preference_summary:I = 0x7f0900de

.field public static final photo_instant_upload_preference_title:I = 0x7f0900dc

.field public static final photo_learn_more_preference_key:I = 0x7f090030

.field public static final photo_learn_more_preference_summary:I = 0x7f0900f9

.field public static final photo_learn_more_preference_title:I = 0x7f0900f8

.field public static final photo_network_error:I = 0x7f09008c

.field public static final photo_on_battery_preference_key:I = 0x7f09002c

.field public static final photo_on_battery_preference_summary:I = 0x7f0900e4

.field public static final photo_on_battery_preference_title:I = 0x7f0900e3

.field public static final photo_picker_album_label_cover_photo:I = 0x7f0900aa

.field public static final photo_picker_album_label_messenger:I = 0x7f0900ac

.field public static final photo_picker_album_label_profile:I = 0x7f0900a9

.field public static final photo_picker_album_label_share:I = 0x7f0900ad

.field public static final photo_picker_album_message_cover_photo:I = 0x7f0900ab

.field public static final photo_picker_cancel:I = 0x7f0900ae

.field public static final photo_picker_photos_home_label:I = 0x7f0900a8

.field public static final photo_picker_save:I = 0x7f0900af

.field public static final photo_picker_select:I = 0x7f0900b0

.field public static final photo_picker_sublabel:I = 0x7f0900a7

.field public static final photo_preference_category:I = 0x7f0900da

.field public static final photo_preference_instant_upload_button:I = 0x7f0900db

.field public static final photo_preference_instant_upload_key:I = 0x7f090034

.field public static final photo_roaming_upload_preference_key:I = 0x7f09002b

.field public static final photo_roaming_upload_preference_summary:I = 0x7f0900e2

.field public static final photo_roaming_upload_preference_title:I = 0x7f0900e1

.field public static final photo_sync_disabled_summary:I = 0x7f0900df

.field public static final photo_sync_preference_cancel_title:I = 0x7f0900fb

.field public static final photo_sync_preference_key:I = 0x7f09002a

.field public static final photo_sync_preference_summary:I = 0x7f0900fc

.field public static final photo_sync_preference_title:I = 0x7f0900fa

.field public static final photo_tag_approve_error:I = 0x7f0900a6

.field public static final photo_tag_deny_error:I = 0x7f0900b1

.field public static final photo_upload_confirmation:I = 0x7f090108

.field public static final photo_upload_finished:I = 0x7f090109

.field public static final photo_upload_now_inprogress_summary:I = 0x7f0900fe

.field public static final photo_upload_now_paused_summary:I = 0x7f0900ff

.field public static final photo_upload_now_status_no_background_data:I = 0x7f090106

.field public static final photo_upload_now_status_no_power:I = 0x7f090102

.field public static final photo_upload_now_status_no_sdcard:I = 0x7f090105

.field public static final photo_upload_now_status_no_wifi:I = 0x7f090100

.field public static final photo_upload_now_status_quota:I = 0x7f090104

.field public static final photo_upload_now_status_roaming:I = 0x7f090101

.field public static final photo_upload_now_status_unknown:I = 0x7f090107

.field public static final photo_upload_now_status_user_auth:I = 0x7f090103

.field public static final photo_upload_size_full:I = 0x7f0900f4

.field public static final photo_upload_size_preference_default_value:I = 0x7f090033

.field public static final photo_upload_size_preference_entry_summary_full:I = 0x7f0900f2

.field public static final photo_upload_size_preference_entry_summary_standard:I = 0x7f0900f3

.field public static final photo_upload_size_preference_key:I = 0x7f09002e

.field public static final photo_upload_size_preference_summary_full:I = 0x7f0900ef

.field public static final photo_upload_size_preference_summary_full_unknown:I = 0x7f0900ee

.field public static final photo_upload_size_preference_summary_overquota:I = 0x7f0900f0

.field public static final photo_upload_size_preference_summary_standard:I = 0x7f0900f1

.field public static final photo_upload_size_preference_title:I = 0x7f0900ed

.field public static final photo_upload_size_quota_available:I = 0x7f090507

.field public static final photo_upload_size_quota_unknown:I = 0x7f090508

.field public static final photo_upload_size_standard:I = 0x7f0900f5

.field public static final photo_upload_starting_summary:I = 0x7f0900fd

.field public static final photo_view_count:I = 0x7f09009a

.field public static final photo_view_default_title:I = 0x7f09009b

.field public static final photo_view_messenger_title:I = 0x7f09009c

.field public static final photo_view_tag_suggestion_of_you:I = 0x7f09009e

.field public static final photo_view_tagged_you:I = 0x7f09009d

.field public static final photo_view_video_not_ready:I = 0x7f090099

.field public static final photos_home_instant_upload_label:I = 0x7f090088

.field public static final photos_home_local_label:I = 0x7f090089

.field public static final photos_home_of_you_label:I = 0x7f090087

.field public static final photos_home_unknown_label:I = 0x7f09008a

.field public static final photos_next_button_text:I = 0x7f0901ae

.field public static final photos_of_user_label:I = 0x7f09008b

.field public static final plus_one_added_confirmation:I = 0x7f090465

.field public static final plus_one_people_title:I = 0x7f0901f9

.field public static final plus_one_removed_confirmation:I = 0x7f090466

.field public static final plus_page_reminder:I = 0x7f09005f

.field public static final plusone_error:I = 0x7f0901cd

.field public static final plusoned_by_one_known:I = 0x7f0901fc

.field public static final plusoned_by_two_known:I = 0x7f0901fe

.field public static final plusoned_by_you:I = 0x7f0901fb

.field public static final plusoned_by_you_and_one_known:I = 0x7f0901fd

.field public static final plusoned_by_you_and_two_known:I = 0x7f0901ff

.field public static final post:I = 0x7f0901b4

.field public static final post_add_audience:I = 0x7f0901b8

.field public static final post_add_emotion:I = 0x7f0901bb

.field public static final post_add_link:I = 0x7f0901ba

.field public static final post_add_photo_from_phone:I = 0x7f0901b9

.field public static final post_body_hint:I = 0x7f090207

.field public static final post_cancel_button_text:I = 0x7f0901ab

.field public static final post_checkin_title:I = 0x7f090206

.field public static final post_close_acl_drop_down:I = 0x7f0901ac

.field public static final post_create_activity_error:I = 0x7f090208

.field public static final post_create_custom_acl:I = 0x7f0901bf

.field public static final post_delete_question:I = 0x7f090196

.field public static final post_discard:I = 0x7f09020a

.field public static final post_done_button_text:I = 0x7f0901a9

.field public static final post_edit_audience_activity_title:I = 0x7f090302

.field public static final post_invalid_photos_remote:I = 0x7f090194

.field public static final post_invalid_photos_unsupported:I = 0x7f090195

.field public static final post_link_hint:I = 0x7f0901f5

.field public static final post_loading_from_clipboard:I = 0x7f0901af

.field public static final post_location_dialog_message:I = 0x7f09020c

.field public static final post_location_dialog_title:I = 0x7f09020b

.field public static final post_max_photos:I = 0x7f090193

.field public static final post_mute_question:I = 0x7f09019c

.field public static final post_not_sent_title:I = 0x7f0901ca

.field public static final post_open_acl_drop_down:I = 0x7f0901ad

.field public static final post_operation_pending:I = 0x7f0901d7

.field public static final post_quit_question:I = 0x7f090209

.field public static final post_remove_question:I = 0x7f090197

.field public static final post_report_question:I = 0x7f09019e

.field public static final post_restricted_mention_error:I = 0x7f0901c9

.field public static final post_share_button_text:I = 0x7f0901aa

.field public static final post_take_photo_button:I = 0x7f0901bd

.field public static final post_take_video_button:I = 0x7f0901be

.field public static final post_to:I = 0x7f0901bc

.field public static final post_unmute_question:I = 0x7f09019d

.field public static final posted_just_now:I = 0x7f09013c

.field public static final preferences_about_button:I = 0x7f090327

.field public static final preferences_build_version:I = 0x7f090328

.field public static final preferences_license_summary:I = 0x7f09032a

.field public static final preferences_license_title:I = 0x7f090329

.field public static final preferences_network_bandwidth_summary:I = 0x7f09022f

.field public static final preferences_network_bandwidth_title:I = 0x7f09022e

.field public static final preferences_network_stats_title:I = 0x7f09022d

.field public static final preferences_network_transactions_summary:I = 0x7f090233

.field public static final preferences_network_transactions_title:I = 0x7f090232

.field public static final preferences_remove_account_description:I = 0x7f09032c

.field public static final preferences_remove_account_dialog_message:I = 0x7f09032d

.field public static final preferences_remove_account_title:I = 0x7f09032b

.field public static final preferences_upload_stats_summary:I = 0x7f090231

.field public static final preferences_upload_stats_title:I = 0x7f090230

.field public static final preview_content_description:I = 0x7f09046e

.field public static final privacy_policy_preference_key:I = 0x7f090038

.field public static final profile_add_a_job:I = 0x7f0902ba

.field public static final profile_add_a_place:I = 0x7f0902bc

.field public static final profile_add_a_school:I = 0x7f0902bb

.field public static final profile_cover_photo_content_description:I = 0x7f090473

.field public static final profile_cover_photos_stream_title:I = 0x7f09035b

.field public static final profile_does_not_exist:I = 0x7f090323

.field public static final profile_edit_cancel:I = 0x7f0902b8

.field public static final profile_edit_hint_end_date:I = 0x7f0902c3

.field public static final profile_edit_hint_job_name:I = 0x7f0902bd

.field public static final profile_edit_hint_job_title:I = 0x7f0902c0

.field public static final profile_edit_hint_location_name:I = 0x7f0902bf

.field public static final profile_edit_hint_school_major:I = 0x7f0902c1

.field public static final profile_edit_hint_school_name:I = 0x7f0902be

.field public static final profile_edit_hint_start_date:I = 0x7f0902c2

.field public static final profile_edit_item_acl_picker:I = 0x7f0902c8

.field public static final profile_edit_item_none:I = 0x7f0902cc

.field public static final profile_edit_item_remove:I = 0x7f0902c5

.field public static final profile_edit_item_remove_confirm:I = 0x7f0902c4

.field public static final profile_edit_item_visibility:I = 0x7f0902c7

.field public static final profile_edit_items_exit_unsaved:I = 0x7f0902cb

.field public static final profile_edit_label_current:I = 0x7f0902c6

.field public static final profile_edit_save:I = 0x7f0902b9

.field public static final profile_edit_update_error:I = 0x7f0902ca

.field public static final profile_edit_updating:I = 0x7f0902c9

.field public static final profile_end_date_for_current:I = 0x7f0902af

.field public static final profile_item_address:I = 0x7f0902b6

.field public static final profile_item_address_home:I = 0x7f0902b4

.field public static final profile_item_address_work:I = 0x7f0902b5

.field public static final profile_item_birthday:I = 0x7f0902aa

.field public static final profile_item_email:I = 0x7f09028e

.field public static final profile_item_email_home:I = 0x7f0902a4

.field public static final profile_item_email_work:I = 0x7f0902a5

.field public static final profile_item_gender:I = 0x7f0902a7

.field public static final profile_item_gender_female:I = 0x7f0902a9

.field public static final profile_item_gender_male:I = 0x7f0902a8

.field public static final profile_item_phone:I = 0x7f09028f

.field public static final profile_item_phone_assistant:I = 0x7f090299

.field public static final profile_item_phone_callback:I = 0x7f09029d

.field public static final profile_item_phone_car:I = 0x7f09029a

.field public static final profile_item_phone_company_main:I = 0x7f090298

.field public static final profile_item_phone_fax:I = 0x7f090294

.field public static final profile_item_phone_google_voice:I = 0x7f0902a2

.field public static final profile_item_phone_home:I = 0x7f090290

.field public static final profile_item_phone_home_fax:I = 0x7f090295

.field public static final profile_item_phone_isdn:I = 0x7f09029c

.field public static final profile_item_phone_main:I = 0x7f090293

.field public static final profile_item_phone_mobile:I = 0x7f090291

.field public static final profile_item_phone_other_fax:I = 0x7f090297

.field public static final profile_item_phone_pager:I = 0x7f0902a3

.field public static final profile_item_phone_radio:I = 0x7f09029b

.field public static final profile_item_phone_telex:I = 0x7f09029e

.field public static final profile_item_phone_tty_tdd:I = 0x7f09029f

.field public static final profile_item_phone_work:I = 0x7f090292

.field public static final profile_item_phone_work_fax:I = 0x7f090296

.field public static final profile_item_phone_work_mobile:I = 0x7f0902a0

.field public static final profile_item_phone_work_pager:I = 0x7f0902a1

.field public static final profile_item_website:I = 0x7f0902b7

.field public static final profile_load_error:I = 0x7f09021a

.field public static final profile_local_details_at_a_glance:I = 0x7f090435

.field public static final profile_local_details_opening_hours:I = 0x7f090433

.field public static final profile_local_details_phone:I = 0x7f090434

.field public static final profile_local_from_zagat:I = 0x7f09042e

.field public static final profile_local_page_call:I = 0x7f09042c

.field public static final profile_local_page_directions:I = 0x7f09042b

.field public static final profile_local_page_map:I = 0x7f09042a

.field public static final profile_local_page_review_source_user:I = 0x7f09042d

.field public static final profile_local_section_activity_from_your_circles:I = 0x7f090431

.field public static final profile_local_section_details:I = 0x7f090432

.field public static final profile_local_section_reviews:I = 0x7f09042f

.field public static final profile_local_section_your_activity:I = 0x7f090430

.field public static final profile_location_not_available:I = 0x7f0902b1

.field public static final profile_not_on_google_plus:I = 0x7f09023b

.field public static final profile_not_sharing_location_description:I = 0x7f0902b3

.field public static final profile_not_sharing_location_title:I = 0x7f0902b2

.field public static final profile_photo_content_description:I = 0x7f090472

.field public static final profile_photos_stream_title:I = 0x7f09035a

.field public static final profile_photos_tab_text:I = 0x7f090237

.field public static final profile_posts_tab_text:I = 0x7f090236

.field public static final profile_section_contact:I = 0x7f09023a

.field public static final profile_section_current_location:I = 0x7f0902ad

.field public static final profile_section_education:I = 0x7f0902ac

.field public static final profile_section_employment:I = 0x7f0902ab

.field public static final profile_section_introduction:I = 0x7f090239

.field public static final profile_section_links:I = 0x7f0902b0

.field public static final profile_section_personal:I = 0x7f0902a6

.field public static final profile_section_places:I = 0x7f0902ae

.field public static final profile_section_tagline:I = 0x7f090238

.field public static final profile_show_less:I = 0x7f090235

.field public static final profile_show_more:I = 0x7f090234

.field public static final profile_unknown_name:I = 0x7f09023c

.field public static final progress_remove_deleted_media:I = 0x7f090513

.field public static final progress_rescan_media:I = 0x7f090514

.field public static final ps_cache_done:I = 0x7f0903da

.field public static final ps_cache_status:I = 0x7f0903d9

.field public static final ps_cache_status_title:I = 0x7f0903d8

.field public static final realtimechat_accept_invitation_button_text:I = 0x7f09030a

.field public static final realtimechat_acl_anyone:I = 0x7f09030c

.field public static final realtimechat_acl_extended_circles:I = 0x7f090310

.field public static final realtimechat_acl_key:I = 0x7f09000c

.field public static final realtimechat_acl_my_circles:I = 0x7f09030e

.field public static final realtimechat_acl_settings_title:I = 0x7f09030b

.field public static final realtimechat_acl_subtitle_anyone:I = 0x7f09030d

.field public static final realtimechat_acl_subtitle_extended_circles:I = 0x7f090311

.field public static final realtimechat_acl_subtitle_my_circles:I = 0x7f09030f

.field public static final realtimechat_acl_update_failed:I = 0x7f090315

.field public static final realtimechat_acl_update_failed_title:I = 0x7f090314

.field public static final realtimechat_acl_update_pending:I = 0x7f090313

.field public static final realtimechat_acl_update_pending_title:I = 0x7f090312

.field public static final realtimechat_android_id_key:I = 0x7f0903d6

.field public static final realtimechat_backend_key:I = 0x7f09000f

.field public static final realtimechat_c2dm_registration_key:I = 0x7f0903d7

.field public static final realtimechat_conversation_description_message:I = 0x7f090255

.field public static final realtimechat_conversation_description_muted:I = 0x7f090256

.field public static final realtimechat_conversation_description_time_since:I = 0x7f090253

.field public static final realtimechat_conversation_description_title:I = 0x7f090252

.field public static final realtimechat_conversation_description_unread_count:I = 0x7f090254

.field public static final realtimechat_conversation_edit_name_menu_item_text:I = 0x7f090247

.field public static final realtimechat_conversation_error_dialog_general:I = 0x7f090264

.field public static final realtimechat_conversation_error_dialog_huddle_too_big:I = 0x7f090265

.field public static final realtimechat_conversation_error_dialog_leave_button:I = 0x7f090267

.field public static final realtimechat_conversation_error_dialog_some_invalid_participants:I = 0x7f090266

.field public static final realtimechat_conversation_error_dialog_title:I = 0x7f090263

.field public static final realtimechat_conversation_error_message_general:I = 0x7f09026c

.field public static final realtimechat_conversation_error_message_invalid_country:I = 0x7f09026a

.field public static final realtimechat_conversation_error_message_invalid_email:I = 0x7f090268

.field public static final realtimechat_conversation_error_message_invalid_phone_number:I = 0x7f090269

.field public static final realtimechat_conversation_error_message_sms_invites:I = 0x7f09026b

.field public static final realtimechat_conversation_invite_menu_item_text:I = 0x7f090272

.field public static final realtimechat_conversation_join_hangout_menu_item_text:I = 0x7f090273

.field public static final realtimechat_conversation_leave_menu_item_text:I = 0x7f09024a

.field public static final realtimechat_conversation_loading_older_messages:I = 0x7f090111

.field public static final realtimechat_conversation_mute_menu_item_text:I = 0x7f090248

.field public static final realtimechat_conversation_rename_cancel_button_text:I = 0x7f09024d

.field public static final realtimechat_conversation_rename_dialog_title:I = 0x7f09024b

.field public static final realtimechat_conversation_rename_hint:I = 0x7f09024e

.field public static final realtimechat_conversation_rename_save_button_text:I = 0x7f09024c

.field public static final realtimechat_conversation_system_message_joins_hangout_tile:I = 0x7f090260

.field public static final realtimechat_conversation_system_message_participant_added:I = 0x7f090261

.field public static final realtimechat_conversation_system_message_participant_left:I = 0x7f090262

.field public static final realtimechat_conversation_system_message_rename:I = 0x7f09025f

.field public static final realtimechat_conversation_unmute_menu_item_text:I = 0x7f090249

.field public static final realtimechat_default_acl_key:I = 0x7f09000b

.field public static final realtimechat_default_backend:I = 0x7f09000e

.field public static final realtimechat_edit_audience_activity_title:I = 0x7f090303

.field public static final realtimechat_failure_loading_messages:I = 0x7f090110

.field public static final realtimechat_invitation_acl_and_block_text:I = 0x7f090307

.field public static final realtimechat_invitation_message_text:I = 0x7f090308

.field public static final realtimechat_invitation_notification_content_text:I = 0x7f090278

.field public static final realtimechat_invitation_notification_summary_text:I = 0x7f090277

.field public static final realtimechat_invitation_notification_title_text:I = 0x7f090279

.field public static final realtimechat_invitation_preview_text:I = 0x7f090349

.field public static final realtimechat_launcher_title:I = 0x7f090251

.field public static final realtimechat_leave_conversation_text:I = 0x7f09010d

.field public static final realtimechat_leave_conversation_title:I = 0x7f09010c

.field public static final realtimechat_message_description_author:I = 0x7f090257

.field public static final realtimechat_message_description_image:I = 0x7f09025b

.field public static final realtimechat_message_description_message:I = 0x7f090259

.field public static final realtimechat_message_description_show_participants:I = 0x7f09025c

.field public static final realtimechat_message_description_system_message:I = 0x7f09025a

.field public static final realtimechat_message_description_time_since:I = 0x7f090258

.field public static final realtimechat_message_text_hint:I = 0x7f090250

.field public static final realtimechat_more_than_three_people_typing_text:I = 0x7f09027d

.field public static final realtimechat_name_and_message_image:I = 0x7f090276

.field public static final realtimechat_name_and_message_text:I = 0x7f090275

.field public static final realtimechat_new_conversation_hint_text:I = 0x7f09025d

.field public static final realtimechat_notification_new_messages:I = 0x7f090274

.field public static final realtimechat_notify_setting_key:I = 0x7f090019

.field public static final realtimechat_one_person_typing_text:I = 0x7f09027a

.field public static final realtimechat_participant_without_name_text:I = 0x7f090271

.field public static final realtimechat_reject_invitation_button_text:I = 0x7f090309

.field public static final realtimechat_ringtone_setting_default_value:I = 0x7f09001b

.field public static final realtimechat_ringtone_setting_key:I = 0x7f090018

.field public static final realtimechat_send_button_text:I = 0x7f09024f

.field public static final realtimechat_settings_notify_title:I = 0x7f09011a

.field public static final realtimechat_settings_ringtone_title:I = 0x7f09011c

.field public static final realtimechat_settings_silent_ringtone:I = 0x7f09011d

.field public static final realtimechat_settings_vibrate_title:I = 0x7f09011b

.field public static final realtimechat_three_people_typing_text:I = 0x7f09027c

.field public static final realtimechat_two_people_typing_text:I = 0x7f09027b

.field public static final realtimechat_users_you_may_know:I = 0x7f09025e

.field public static final realtimechat_vibrate_setting_key:I = 0x7f09001a

.field public static final refresh_photo_album_error:I = 0x7f0901d4

.field public static final remove_account_preference_key:I = 0x7f09003a

.field public static final remove_album_name_content_description:I = 0x7f09046d

.field public static final remove_from_circle_operation_pending:I = 0x7f0904a3

.field public static final remove_from_circles:I = 0x7f09026f

.field public static final remove_location_content_description:I = 0x7f09046b

.field public static final remove_media_content_description:I = 0x7f09046c

.field public static final remove_photo_error:I = 0x7f0901d3

.field public static final remove_plus_one_description:I = 0x7f090464

.field public static final remove_post_error:I = 0x7f0901c5

.field public static final remove_preview_content_description:I = 0x7f09046f

.field public static final remove_tag_question:I = 0x7f090084

.field public static final report_abuse_completed_toast:I = 0x7f0902f7

.field public static final report_abuse_event_completed_toast:I = 0x7f0902f8

.field public static final report_abuse_operation_pending:I = 0x7f0902f9

.field public static final report_abuse_reason_copyright:I = 0x7f0902f4

.field public static final report_abuse_reason_fake_profile:I = 0x7f0902f6

.field public static final report_abuse_reason_hate_speech_or_violence:I = 0x7f0902f3

.field public static final report_abuse_reason_impersonation:I = 0x7f0902f5

.field public static final report_abuse_reason_nudity:I = 0x7f0902f2

.field public static final report_abuse_reason_spam:I = 0x7f0902f1

.field public static final report_activity_error:I = 0x7f0901cc

.field public static final report_impersonation_dialog_message:I = 0x7f0902fa

.field public static final report_mute_completed_toast:I = 0x7f0902ee

.field public static final report_photo_error:I = 0x7f0901d0

.field public static final report_photo_question:I = 0x7f090086

.field public static final report_unmute_completed_toast:I = 0x7f0902ef

.field public static final report_user_dialog_title:I = 0x7f0902f0

.field public static final reshare_button_content_description:I = 0x7f090479

.field public static final reshare_dialog_message:I = 0x7f090210

.field public static final reshare_dialog_positive_button:I = 0x7f090211

.field public static final reshare_dialog_title:I = 0x7f09020f

.field public static final reshare_post_error:I = 0x7f0901c6

.field public static final reshare_title:I = 0x7f09020e

.field public static final restore_post_error:I = 0x7f0901c8

.field public static final retry_send:I = 0x7f09010a

.field public static final ring_disabled_hangout_label:I = 0x7f090116

.field public static final ring_off_hangout_label:I = 0x7f090115

.field public static final ring_off_hangout_toast:I = 0x7f090118

.field public static final ring_off_overcapacity_hangout_toast:I = 0x7f090119

.field public static final ring_on_hangout_label:I = 0x7f090114

.field public static final ring_on_hangout_toast:I = 0x7f090117

.field public static final riviera_add_comment_button:I = 0x7f09057a

.field public static final riviera_album_media_count:I = 0x7f090578

.field public static final riviera_media_content_description:I = 0x7f090579

.field public static final save:I = 0x7f0901b7

.field public static final save_settings_error:I = 0x7f0901d5

.field public static final search_activity_label:I = 0x7f090227

.field public static final search_location_hint_text:I = 0x7f09022c

.field public static final search_people_hint_text:I = 0x7f09022a

.field public static final search_people_tab_text:I = 0x7f090228

.field public static final search_posts_hint_text:I = 0x7f090229

.field public static final search_squares_hint_text:I = 0x7f09022b

.field public static final search_tab_best:I = 0x7f09057d

.field public static final search_tab_recent:I = 0x7f09057e

.field public static final send_sms_content_description:I = 0x7f090476

.field public static final set_profile_photo_error:I = 0x7f0900a5

.field public static final set_profile_photo_pending:I = 0x7f0900a4

.field public static final set_wallpaper_photo_error:I = 0x7f0901d1

.field public static final set_wallpaper_photo_pending:I = 0x7f09009f

.field public static final set_wallpaper_photo_success:I = 0x7f0901d2

.field public static final setting_cover_photo:I = 0x7f09035f

.field public static final setting_profile_photo:I = 0x7f09035e

.field public static final share:I = 0x7f0901b5

.field public static final share_account_warning:I = 0x7f0904fa

.field public static final share_add_more_photos:I = 0x7f0901f7

.field public static final share_album_name:I = 0x7f0901f8

.field public static final share_body_empty:I = 0x7f0901f6

.field public static final share_body_hint:I = 0x7f0901f4

.field public static final share_connection_error:I = 0x7f0901f1

.field public static final share_incorrect_account:I = 0x7f0901ee

.field public static final share_menu_anchor_content_description:I = 0x7f090469

.field public static final share_post_success:I = 0x7f0901f2

.field public static final share_preview_error:I = 0x7f0901ef

.field public static final share_preview_post_error:I = 0x7f0901f0

.field public static final sharebox_location_dialog_message:I = 0x7f09020d

.field public static final sign_out_pending:I = 0x7f09018e

.field public static final sign_out_title:I = 0x7f09018f

.field public static final signup_authentication_error:I = 0x7f09004f

.field public static final signup_continue:I = 0x7f09004b

.field public static final signup_create_new_account:I = 0x7f09004a

.field public static final signup_done:I = 0x7f09004c

.field public static final signup_error_network:I = 0x7f090051

.field public static final signup_profile_error:I = 0x7f090050

.field public static final signup_required_update_available:I = 0x7f090052

.field public static final signup_retry:I = 0x7f090056

.field public static final signup_select_account_desc:I = 0x7f090049

.field public static final signup_select_account_title:I = 0x7f090048

.field public static final signup_sending:I = 0x7f09004d

.field public static final signup_signing_in:I = 0x7f09004e

.field public static final signup_text_mobile_not_available:I = 0x7f090055

.field public static final signup_title_mobile_not_available:I = 0x7f090054

.field public static final signup_title_no_connection:I = 0x7f090053

.field public static final signup_upgrading:I = 0x7f090057

.field public static final skyjam_button_listen:I = 0x7f0901e3

.field public static final skyjam_content_play_button_description:I = 0x7f09047c

.field public static final skyjam_from_the_album:I = 0x7f0901e5

.field public static final skyjam_listen:I = 0x7f0901e4

.field public static final skyjam_listen_buy:I = 0x7f0901e2

.field public static final skyjam_notification_playing_song:I = 0x7f0901e6

.field public static final skyjam_notification_playing_song_subtitle:I = 0x7f0901e8

.field public static final skyjam_notification_playing_song_title:I = 0x7f0901e7

.field public static final skyjam_status_buffering:I = 0x7f0901ea

.field public static final skyjam_status_connecting:I = 0x7f0901e9

.field public static final skyjam_status_playing:I = 0x7f0901eb

.field public static final skyjam_status_stopped:I = 0x7f0901ed

.field public static final skyjam_time_formatting:I = 0x7f0901ec

.field public static final source_app_installed_notification:I = 0x7f090490

.field public static final square_accept_invitation:I = 0x7f0904b0

.field public static final square_all_categories:I = 0x7f0904aa

.field public static final square_approve_member_error:I = 0x7f0904ca

.field public static final square_approve_request:I = 0x7f0904f4

.field public static final square_ban_member_error:I = 0x7f0904c8

.field public static final square_ban_user:I = 0x7f0904f1

.field public static final square_blocking_explanation:I = 0x7f0904e3

.field public static final square_blocking_moderator_text:I = 0x7f0904dc

.field public static final square_cancel_invitation:I = 0x7f0904f6

.field public static final square_cancel_invitation_error:I = 0x7f0904cc

.field public static final square_cancel_join_request:I = 0x7f0904b2

.field public static final square_cancel_join_request_error:I = 0x7f0904c2

.field public static final square_canceling_join_request:I = 0x7f0904d6

.field public static final square_cant_see_posts:I = 0x7f0904ab

.field public static final square_confirm_leave_private:I = 0x7f0904e0

.field public static final square_confirm_leave_public:I = 0x7f0904e1

.field public static final square_confirm_leave_title:I = 0x7f0904df

.field public static final square_decline_invitation_error:I = 0x7f0904c4

.field public static final square_demote_member_error:I = 0x7f0904c6

.field public static final square_demote_to_member:I = 0x7f0904ed

.field public static final square_demote_to_moderator:I = 0x7f0904ee

.field public static final square_dialog_decline_button:I = 0x7f0904de

.field public static final square_dialog_leave_button:I = 0x7f0904e2

.field public static final square_dismiss_invitation_content_description:I = 0x7f09047d

.field public static final square_dismiss_invitation_text:I = 0x7f0904dd

.field public static final square_error_moderator_too_new:I = 0x7f0904ce

.field public static final square_error_moderator_too_new_title:I = 0x7f0904cd

.field public static final square_error_sole_owner_leaving:I = 0x7f0904d0

.field public static final square_error_sole_owner_stepping_down:I = 0x7f0904d1

.field public static final square_error_sole_owner_title:I = 0x7f0904cf

.field public static final square_error_too_may_invitees:I = 0x7f0904d3

.field public static final square_error_too_may_invitees_title:I = 0x7f0904d2

.field public static final square_ignore_member_error:I = 0x7f0904cb

.field public static final square_ignore_request:I = 0x7f0904f5

.field public static final square_invitation:I = 0x7f0904d8

.field public static final square_invitation_no_name:I = 0x7f0904d9

.field public static final square_invitation_required:I = 0x7f0904b3

.field public static final square_invite:I = 0x7f0904b6

.field public static final square_invite_success:I = 0x7f0901f3

.field public static final square_invited_item_text:I = 0x7f0904b9

.field public static final square_join:I = 0x7f0904af

.field public static final square_join_error:I = 0x7f0904c0

.field public static final square_joining:I = 0x7f0904d4

.field public static final square_leave:I = 0x7f0904b4

.field public static final square_leave_error:I = 0x7f0904c3

.field public static final square_leaving:I = 0x7f0904d7

.field public static final square_member_actions:I = 0x7f0904ea

.field public static final square_member_item_text:I = 0x7f0904b8

.field public static final square_member_item_text_acl:I = 0x7f0904b7

.field public static final square_members:I = 0x7f0904bb

.field public static final square_members_awaiting_approval:I = 0x7f0904bd

.field public static final square_members_banned:I = 0x7f0904be

.field public static final square_members_invited:I = 0x7f0904bf

.field public static final square_members_moderators:I = 0x7f0904bc

.field public static final square_moderation_dialog_remove:I = 0x7f0904e6

.field public static final square_moderation_dialog_restore:I = 0x7f0904e7

.field public static final square_moderation_dialog_text:I = 0x7f0904e5

.field public static final square_moderation_dialog_title:I = 0x7f0904e4

.field public static final square_moderator:I = 0x7f0904e9

.field public static final square_name_and_topic:I = 0x7f0904db

.field public static final square_oneup_acl_name_and_stream:I = 0x7f09047e

.field public static final square_owner:I = 0x7f0904e8

.field public static final square_private:I = 0x7f0904ae

.field public static final square_promote_member_error:I = 0x7f0904c5

.field public static final square_promote_to_moderator:I = 0x7f0904eb

.field public static final square_promote_to_owner:I = 0x7f0904ec

.field public static final square_public:I = 0x7f0904ad

.field public static final square_remove_member:I = 0x7f0904f3

.field public static final square_remove_member_error:I = 0x7f0904c7

.field public static final square_request_to_join:I = 0x7f0904b1

.field public static final square_request_to_join_error:I = 0x7f0904c1

.field public static final square_search_error:I = 0x7f0904a9

.field public static final square_search_not_found:I = 0x7f0904a8

.field public static final square_sending_join_request:I = 0x7f0904d5

.field public static final square_share:I = 0x7f0904b5

.field public static final square_step_down_to_member:I = 0x7f0904ef

.field public static final square_step_down_to_moderator:I = 0x7f0904f0

.field public static final square_subscriptions_disabled:I = 0x7f09013a

.field public static final square_suggested_item_text:I = 0x7f0904ba

.field public static final square_unban_member_error:I = 0x7f0904c9

.field public static final square_unban_user:I = 0x7f0904f2

.field public static final square_unknown:I = 0x7f0904da

.field public static final squares_load_error:I = 0x7f0904ac

.field public static final start_conversation_action_label:I = 0x7f09031f

.field public static final start_hangout_action_label:I = 0x7f090320

.field public static final stream_circle_settings:I = 0x7f090125

.field public static final stream_circles:I = 0x7f090123

.field public static final stream_nearby:I = 0x7f090122

.field public static final stream_one_up_comment_option_ban_comment_author:I = 0x7f09045b

.field public static final stream_one_up_comment_option_delete:I = 0x7f09045a

.field public static final stream_one_up_comment_option_edit:I = 0x7f09045c

.field public static final stream_one_up_comment_option_plus_ones:I = 0x7f09045d

.field public static final stream_one_up_comment_option_plusminus:I = 0x7f090453

.field public static final stream_one_up_comment_option_plusone:I = 0x7f090452

.field public static final stream_one_up_comment_option_report:I = 0x7f090454

.field public static final stream_one_up_comment_option_undo_report:I = 0x7f090457

.field public static final stream_one_up_comment_options_title:I = 0x7f090451

.field public static final stream_one_up_comment_report_dialog_question:I = 0x7f090456

.field public static final stream_one_up_comment_report_dialog_title:I = 0x7f090455

.field public static final stream_one_up_comment_undo_report_dialog_question:I = 0x7f090459

.field public static final stream_one_up_comment_undo_report_dialog_title:I = 0x7f090458

.field public static final stream_one_up_is_edited:I = 0x7f090450

.field public static final stream_one_up_is_muted:I = 0x7f09044f

.field public static final stream_original_author:I = 0x7f090145

.field public static final stream_original_author_community:I = 0x7f090146

.field public static final stream_plus_one_count:I = 0x7f090424

.field public static final stream_plus_one_count_with_plus:I = 0x7f090425

.field public static final stream_square_and_time_template:I = 0x7f090126

.field public static final stream_truncated_info:I = 0x7f090423

.field public static final stream_whats_hot:I = 0x7f090124

.field public static final take_call:I = 0x7f0903c3

.field public static final terms_of_service_preference_key:I = 0x7f090039

.field public static final tile_plus_one_count_with_plus:I = 0x7f090426

.field public static final time_zone_format:I = 0x7f090174

.field public static final time_zone_generic_system_prefix:I = 0x7f090173

.field public static final time_zone_utc_format:I = 0x7f090175

.field public static final toast_circle_already_exists:I = 0x7f09032e

.field public static final toast_circle_deleted:I = 0x7f090460

.field public static final toast_new_circle_created:I = 0x7f09045f

.field public static final toast_panorama_viewer_failure:I = 0x7f09050b

.field public static final today:I = 0x7f090427

.field public static final tomorrow:I = 0x7f090428

.field public static final transient_server_error:I = 0x7f090217

.field public static final truncated_info:I = 0x7f0903ba

.field public static final truncated_info_see_more:I = 0x7f0903bb

.field public static final unblock_page_dialog_message:I = 0x7f0902e1

.field public static final unblock_page_dialog_title:I = 0x7f0902df

.field public static final unblock_page_operation_pending:I = 0x7f0902e3

.field public static final unblock_person_dialog_message:I = 0x7f0902e0

.field public static final unblock_person_dialog_title:I = 0x7f0902de

.field public static final unblock_person_operation_pending:I = 0x7f0902e2

.field public static final unknown_address:I = 0x7f090203

.field public static final unmute_dialog_content_female:I = 0x7f0902ec

.field public static final unmute_dialog_content_general:I = 0x7f0902ed

.field public static final unmute_dialog_content_male:I = 0x7f0902eb

.field public static final unmute_dialog_title:I = 0x7f0902ea

.field public static final unsupported_link_dialog_message:I = 0x7f090361

.field public static final unsupported_link_dialog_title:I = 0x7f090360

.field public static final upgrade_to_cover_photo_dialog_confirm:I = 0x7f090352

.field public static final upgrade_to_cover_photo_dialog_content:I = 0x7f090351

.field public static final upgrade_to_cover_photo_dialog_title:I = 0x7f090350

.field public static final upload_stats_preference_key:I = 0x7f09003d

.field public static final url_param_help_circles:I = 0x7f09057f

.field public static final url_param_help_events:I = 0x7f090586

.field public static final url_param_help_hangouts:I = 0x7f090582

.field public static final url_param_help_location:I = 0x7f090589

.field public static final url_param_help_messenger:I = 0x7f090584

.field public static final url_param_help_privacy_block:I = 0x7f090588

.field public static final url_param_help_profile:I = 0x7f090581

.field public static final url_param_help_settings:I = 0x7f090585

.field public static final url_param_help_squares:I = 0x7f090587

.field public static final url_param_help_stats_sync:I = 0x7f090583

.field public static final url_param_help_stream:I = 0x7f090580

.field public static final video_connection_preference_default_value:I = 0x7f090032

.field public static final video_connection_preference_key:I = 0x7f09002d

.field public static final video_connection_preference_summary_mobile:I = 0x7f0900ec

.field public static final video_connection_preference_summary_wifi:I = 0x7f0900eb

.field public static final video_connection_preference_title:I = 0x7f0900ea

.field public static final video_no_stream:I = 0x7f090095

.field public static final video_not_ready:I = 0x7f090094

.field public static final view_profile_action_label:I = 0x7f09031e

.field public static final what_does_this_mean_link:I = 0x7f0902e4

.field public static final widget_all_circles:I = 0x7f090282

.field public static final widget_choose_view:I = 0x7f090280

.field public static final widget_default_circle_name:I = 0x7f090281

.field public static final widget_posts_title:I = 0x7f090283

.field public static final widget_tap_to_configure:I = 0x7f09027f

.field public static final yes:I = 0x7f090221

.field public static final yesterday:I = 0x7f090429

.field public static final zagat_explanation_description:I = 0x7f090437

.field public static final zagat_explanation_ok_button:I = 0x7f09043d

.field public static final zagat_explanation_score_description_0_10:I = 0x7f09043c

.field public static final zagat_explanation_score_description_11_15:I = 0x7f09043b

.field public static final zagat_explanation_score_description_16_20:I = 0x7f09043a

.field public static final zagat_explanation_score_description_21_25:I = 0x7f090439

.field public static final zagat_explanation_score_description_26_30:I = 0x7f090438

.field public static final zagat_explanation_title:I = 0x7f090436


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
