.class public final Lcom/google/android/gms/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final AddToCirclesButton:I = 0x7f10006c

.field public static final AlbumView:I = 0x7f100030

.field public static final AlbumView_BottomActionBar:I = 0x7f100034

.field public static final AlbumView_BottomActionBar_ActionButton:I = 0x7f100035

.field public static final AlbumView_BottomActionBar_ActionButton_Disabled:I = 0x7f100036

.field public static final AlbumView_GridView:I = 0x7f100031

.field public static final AlbumView_Instructions:I = 0x7f100033

.field public static final AlbumView_Photo:I = 0x7f100032

.field public static final BottomActionBar:I = 0x7f100073

.field public static final BottomActionBar_ActionButton:I = 0x7f100076

.field public static final BottomActionBar_Button:I = 0x7f100074

.field public static final BottomActionBar_Label:I = 0x7f100075

.field public static final CircleBrowserTheme:I = 0x7f100046

.field public static final CircleBrowserTheme_Dark:I = 0x7f100043

.field public static final CircleBrowserTheme_DarkActionBar:I = 0x7f100044

.field public static final CircleBrowserTheme_Default:I = 0x7f100042

.field public static final CircleBrowserTheme_Default_Tabs:I = 0x7f100045

.field public static final CircleBrowserTheme_FastScroller:I = 0x7f10004b

.field public static final CircleBrowserTheme_Tabs:I = 0x7f10004a

.field public static final CircleSelectorList:I = 0x7f10004e

.field public static final CircleSelectorTheme:I = 0x7f10004d

.field public static final CircleSettingsItemCheckbox:I = 0x7f100055

.field public static final CircleSettingsItemDescription:I = 0x7f100054

.field public static final CircleSettingsItemLabel:I = 0x7f100053

.field public static final CircleSettingsItemText:I = 0x7f100052

.field public static final CircleSettingsSectionHeader:I = 0x7f100050

.field public static final CircleSettingsSectionHeaderDivider:I = 0x7f100051

.field public static final CircleSubscriptionList:I = 0x7f10004f

.field public static final CirclesButton:I = 0x7f10006a

.field public static final ComposeMediaButton:I = 0x7f10006b

.field public static final ComposeUpdate:I = 0x7f100063

.field public static final ComposeUpdate_AutoComplete:I = 0x7f100065

.field public static final ComposeUpdate_Editor:I = 0x7f100064

.field public static final DarkCircleBrowserTheme:I = 0x7f100047

.field public static final DateTimePickerButton:I = 0x7f1000a0

.field public static final DeviceLocation:I = 0x7f1000a3

.field public static final DeviceLocation_Link:I = 0x7f1000a4

.field public static final Divider:I = 0x7f10007a

.field public static final EditAudienceTheme:I = 0x7f10004c

.field public static final EsActionBar:I = 0x7f1000a5

.field public static final EsActionBar_Dark:I = 0x7f1000aa

.field public static final EsActionBar_SearchView:I = 0x7f1000a8

.field public static final EsActionBar_Spinner:I = 0x7f1000a9

.field public static final EsActionBar_Subtitle:I = 0x7f1000a7

.field public static final EsActionBar_Title:I = 0x7f1000a6

.field public static final Events:I = 0x7f100028

.field public static final EventsRsvpActionButton:I = 0x7f10002b

.field public static final Events_RsvpActionButton:I = 0x7f10002a

.field public static final Events_RsvpDivider:I = 0x7f10002c

.field public static final Events_RsvpLabel:I = 0x7f100029

.field public static final Events_RsvpLabel_Going:I = 0x7f10002d

.field public static final Events_RsvpLabel_Maybe:I = 0x7f10002e

.field public static final Events_RsvpLabel_NotGoing:I = 0x7f10002f

.field public static final FlatButton:I = 0x7f10006e

.field public static final FlatButtonBar:I = 0x7f10006d

.field public static final FlatButtonBorder:I = 0x7f100071

.field public static final FlatButtonSeparator:I = 0x7f100070

.field public static final FlatButtonShadow:I = 0x7f100072

.field public static final FlatButton_Text:I = 0x7f10006f

.field public static final HangoutActionBar:I = 0x7f1000ab

.field public static final LabelPreference:I = 0x7f10009b

.field public static final LabelPreference_Label:I = 0x7f10009e

.field public static final LabelPreference_Summary:I = 0x7f10009d

.field public static final LabelPreference_Title:I = 0x7f10009c

.field public static final ListItemAction:I = 0x7f100066

.field public static final MediaUploadSeparator:I = 0x7f10009f

.field public static final People:I = 0x7f10008d

.field public static final PeopleSearchTheme:I = 0x7f100056

.field public static final People_Avatar:I = 0x7f10008e

.field public static final People_Avatar_TrayAvatar:I = 0x7f10008f

.field public static final PhotoView:I = 0x7f10003b

.field public static final PhotoViewTheme:I = 0x7f100038

.field public static final PhotoViewTheme_Default:I = 0x7f100037

.field public static final PhotoView_Photo:I = 0x7f10003c

.field public static final PhotoView_PhotoPager:I = 0x7f10003d

.field public static final PhotoView_Tag:I = 0x7f10003e

.field public static final PhotoView_Tag_Action:I = 0x7f100040

.field public static final PhotoView_Tag_Avatar:I = 0x7f10003f

.field public static final PhotosHome:I = 0x7f100024

.field public static final PhotosHome_ListView:I = 0x7f100025

.field public static final ProfileItemLabel:I = 0x7f10007c

.field public static final ProfileLocalDetails:I = 0x7f10008a

.field public static final ProfileLocalDetails_Label:I = 0x7f10008b

.field public static final ProfileLocalEditorialRating:I = 0x7f100080

.field public static final ProfileLocalEditorialRating_FromZagat:I = 0x7f100085

.field public static final ProfileLocalEditorialRating_Label:I = 0x7f100081

.field public static final ProfileLocalEditorialRating_Label_Main:I = 0x7f100082

.field public static final ProfileLocalEditorialRating_Value:I = 0x7f100083

.field public static final ProfileLocalEditorialRating_Value_Main:I = 0x7f100084

.field public static final ProfileLocalUserRating:I = 0x7f100086

.field public static final ProfileLocalUserRating_AspectExplanation:I = 0x7f100089

.field public static final ProfileLocalUserRating_AspectLabel:I = 0x7f100087

.field public static final ProfileLocalUserRating_AspectValue:I = 0x7f100088

.field public static final ProfileLocalZagatExplanationDialog:I = 0x7f10008c

.field public static final ProfileNoItems:I = 0x7f10007d

.field public static final QuickActions:I = 0x7f10007e

.field public static final QuickActionsItem:I = 0x7f10007f

.field public static final RealTimeChat_ConversationHeader_ConversationName:I = 0x7f10005c

.field public static final RealTimeChat_ConversationList_ConversationName:I = 0x7f100057

.field public static final RealTimeChat_ConversationList_LastMessage:I = 0x7f100059

.field public static final RealTimeChat_ConversationList_MutedIcon:I = 0x7f10005b

.field public static final RealTimeChat_ConversationList_TimeSince:I = 0x7f100058

.field public static final RealTimeChat_ConversationList_UnreadCount:I = 0x7f10005a

.field public static final RealTimeChat_Message_Author:I = 0x7f10005e

.field public static final RealTimeChat_Message_Text:I = 0x7f100060

.field public static final RealTimeChat_Message_TimeSince:I = 0x7f10005f

.field public static final RealTimeChat_ParticipantHeader_ParticipantCount:I = 0x7f10005d

.field public static final RealTimeChat_Participant_Name:I = 0x7f100061

.field public static final RealTimeChat_SuggestedParticipant_Name:I = 0x7f100062

.field public static final SdkActivity:I = 0x7f10009a

.field public static final SdkActivityDefault:I = 0x7f100099

.field public static final SdkTheme:I = 0x7f100098

.field public static final Signup:I = 0x7f100011

.field public static final SignupErrorAppearance:I = 0x7f100023

.field public static final Signup_Body:I = 0x7f100014

.field public static final Signup_Entry:I = 0x7f100017

.field public static final Signup_Entry_Birthday:I = 0x7f100022

.field public static final Signup_Entry_Button:I = 0x7f10001b

.field public static final Signup_Entry_Button_Action:I = 0x7f10001d

.field public static final Signup_Entry_Button_Action_Highlight:I = 0x7f10001e

.field public static final Signup_Entry_Button_Highlight:I = 0x7f10001c

.field public static final Signup_Entry_Checkbox:I = 0x7f100020

.field public static final Signup_Entry_Checkbox_Title:I = 0x7f100021

.field public static final Signup_Entry_Input:I = 0x7f10001a

.field public static final Signup_Entry_Label:I = 0x7f100019

.field public static final Signup_Entry_Layout:I = 0x7f100018

.field public static final Signup_Entry_RadioButton:I = 0x7f10001f

.field public static final Signup_H1:I = 0x7f100012

.field public static final Signup_H3:I = 0x7f100013

.field public static final Signup_Info:I = 0x7f100016

.field public static final Signup_Legal:I = 0x7f100015

.field public static final Squares:I = 0x7f100026

.field public static final Squares_LandingViewButton:I = 0x7f100027

.field public static final StyledButton:I = 0x7f100078

.field public static final StyledButtonBar:I = 0x7f100077

.field public static final StyledButtonSeparator:I = 0x7f100079

.field public static final Tab:I = 0x7f100068

.field public static final TabBar:I = 0x7f100067

.field public static final TabSeparator:I = 0x7f100069

.field public static final TabletCircleBrowserTheme:I = 0x7f100049

.field public static final TabletCircleBrowserTheme_Dark:I = 0x7f100048

.field public static final TextAppearance_SlidingTabActive:I = 0x7f100097

.field public static final TextAppearance_SlidingTabNormal:I = 0x7f100096

.field public static final Theme:I = 0x7f100000

.field public static final Theme_DarkActionBar:I = 0x7f100009

.field public static final Theme_EmeraldSea:I = 0x7f100008

.field public static final Theme_EmeraldSea_Dark:I = 0x7f10000e

.field public static final Theme_EmeraldSea_Dark_Dialog:I = 0x7f10000f

.field public static final Theme_EmeraldSea_Dialog:I = 0x7f10000c

.field public static final Theme_EmeraldSea_NoTitle:I = 0x7f10000b

.field public static final Theme_EmeraldSea_Preference_Dialog:I = 0x7f10000d

.field public static final Theme_Hangout:I = 0x7f100091

.field public static final Theme_HangoutLaunch:I = 0x7f100095

.field public static final Theme_Hangout_InHangout:I = 0x7f100092

.field public static final Theme_Hangout_NoActionBar:I = 0x7f100093

.field public static final Theme_Host:I = 0x7f100003

.field public static final Theme_Host_ActionBar:I = 0x7f100004

.field public static final Theme_Host_ActionBar_Button:I = 0x7f100005

.field public static final Theme_Host_ActionBar_Up:I = 0x7f100006

.field public static final Theme_Host_Navigation:I = 0x7f100007

.field public static final Theme_Host_SearchBar:I = 0x7f1000a2

.field public static final Theme_Messenger:I = 0x7f100090

.field public static final Theme_Platform:I = 0x7f100001

.field public static final Theme_Screen:I = 0x7f100002

.field public static final Theme_SignIn:I = 0x7f10000a

.field public static final Theme_Streams:I = 0x7f100010

.field public static final Theme_TabletHangout:I = 0x7f100094

.field public static final TimeZonePickerButton:I = 0x7f1000a1

.field public static final VerticalDivider:I = 0x7f10007b

.field public static final VideoViewTheme:I = 0x7f10003a

.field public static final VideoViewTheme_Default:I = 0x7f100039

.field public static final Widgets:I = 0x7f100041


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
