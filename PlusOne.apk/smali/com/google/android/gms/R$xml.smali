.class public final Lcom/google/android/gms/R$xml;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "xml"
.end annotation


# static fields
.field public static final about_preferences:I = 0x7f060000

.field public static final contacts_sync_preferences:I = 0x7f060001

.field public static final device_location_preferences:I = 0x7f060002

.field public static final device_location_preferences_no_network:I = 0x7f060003

.field public static final es_contacts:I = 0x7f060004

.field public static final es_froyo_widget:I = 0x7f060005

.field public static final es_iu_sync_adapter:I = 0x7f060006

.field public static final es_picasa_sync_adapter:I = 0x7f060007

.field public static final es_sync_adapter:I = 0x7f060008

.field public static final es_widget:I = 0x7f060009

.field public static final hangout_preferences:I = 0x7f06000a

.field public static final main_preferences:I = 0x7f06000b

.field public static final main_preferences_plus_page:I = 0x7f06000c

.field public static final network_stats_preferences:I = 0x7f06000d

.field public static final notifications_preferences:I = 0x7f06000e

.field public static final photo_preferences:I = 0x7f06000f

.field public static final realtimechat_development_preferences:I = 0x7f060010

.field public static final realtimechat_preferences:I = 0x7f060011


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
