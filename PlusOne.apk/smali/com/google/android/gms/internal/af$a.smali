.class final Lcom/google/android/gms/internal/af$a;
.super Lcom/google/android/gms/internal/c$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "a"
.end annotation


# instance fields
.field private final aA:Landroid/net/Uri;

.field final synthetic aB:Lcom/google/android/gms/internal/af;

.field private final ay:Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

.field private final az:Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Landroid/net/Uri;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/internal/af$a;->aB:Lcom/google/android/gms/internal/af;

    invoke-direct {p0}, Lcom/google/android/gms/internal/c$a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/af$a;->ay:Lcom/google/android/gms/panorama/PanoramaClient$OnFullPanoramaInfoLoadedListener;

    iput-object p2, p0, Lcom/google/android/gms/internal/af$a;->az:Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;

    iput-object p3, p0, Lcom/google/android/gms/internal/af$a;->aA:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;ILandroid/content/Intent;)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/internal/af$a;->aA:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/af$a;->aB:Lcom/google/android/gms/internal/af;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/af;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/af$a;->aA:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    :cond_0
    const/4 v0, 0x0

    if-eqz p2, :cond_1

    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_1
    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/af$a;->aB:Lcom/google/android/gms/internal/af;

    new-instance v2, Lcom/google/android/gms/internal/af$c;

    iget-object v3, p0, Lcom/google/android/gms/internal/af$a;->aB:Lcom/google/android/gms/internal/af;

    iget-object v4, p0, Lcom/google/android/gms/internal/af$a;->az:Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;

    invoke-direct {v2, v3, v4, v1, p4}, Lcom/google/android/gms/internal/af$c;-><init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/af;->a(Lcom/google/android/gms/internal/aa$c;)V

    return-void
.end method
