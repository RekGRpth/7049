.class final Lcom/google/android/gms/internal/af$c;
.super Lcom/google/android/gms/internal/aa$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/aa",
        "<",
        "Lcom/google/android/gms/internal/bd;",
        ">.c<",
        "Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic aB:Lcom/google/android/gms/internal/af;

.field private final at:Lcom/google/android/gms/common/ConnectionResult;

.field private final bf:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Lcom/google/android/gms/common/ConnectionResult;Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/af$c;->aB:Lcom/google/android/gms/internal/af;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/aa$c;-><init>(Lcom/google/android/gms/internal/aa;Ljava/lang/Object;)V

    iput-object p3, p0, Lcom/google/android/gms/internal/af$c;->at:Lcom/google/android/gms/common/ConnectionResult;

    iput-object p4, p0, Lcom/google/android/gms/internal/af$c;->bf:Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1

    check-cast p1, Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/af$c;->at:Lcom/google/android/gms/common/ConnectionResult;

    iget-object v0, p0, Lcom/google/android/gms/internal/af$c;->bf:Landroid/content/Intent;

    invoke-interface {p1, v0}, Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;->onPanoramaInfoLoaded$680664b4(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
