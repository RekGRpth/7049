.class public final Lcom/google/android/gms/internal/af;
.super Lcom/google/android/gms/internal/aa;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/af$a;,
        Lcom/google/android/gms/internal/af$c;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/aa",
        "<",
        "Lcom/google/android/gms/internal/bd;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/internal/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/internal/p;Lcom/google/android/gms/internal/aa$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/p;",
            "Lcom/google/android/gms/internal/aa",
            "<",
            "Lcom/google/android/gms/internal/bd;",
            ">.a;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/af;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/internal/p;->a(Lcom/google/android/gms/internal/ar;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Landroid/net/Uri;Z)V
    .locals 5

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/internal/af$a;

    if-eqz p3, :cond_1

    move-object v0, p2

    :goto_0
    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/gms/internal/af$a;-><init>(Lcom/google/android/gms/internal/af;Lcom/google/android/gms/panorama/PanoramaClient$OnPanoramaInfoLoadedListener;Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/af;->m()V

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/af;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "com.google.android.gms"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, p2, v4}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/af;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bd;

    const/4 v3, 0x0

    invoke-interface {v0, v2, p2, v3, p3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/c;Landroid/net/Uri;Landroid/os/Bundle;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3, v1}, Lcom/google/android/gms/internal/af$a;->a(ILandroid/os/Bundle;ILandroid/content/Intent;)V

    goto :goto_1
.end method

.method protected final h()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.panorama.service.START"

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.panorama.internal.IPanoramaService"

    return-object v0
.end method

.method public final bridge synthetic j(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/bd$a;->M(Landroid/os/IBinder;)Lcom/google/android/gms/internal/bd;

    move-result-object v0

    return-object v0
.end method
