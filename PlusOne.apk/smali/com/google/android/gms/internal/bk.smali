.class public final Lcom/google/android/gms/internal/bk;
.super Ljava/lang/Object;


# instance fields
.field bM:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/bk;->bM:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public final ah()Z
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/bk;->bM:Landroid/os/Bundle;

    const-string v1, "has_plus_one"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final am()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/bk;->bM:Landroid/os/Bundle;

    const-string v1, "confirmation_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
