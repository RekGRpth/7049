.class public Lcom/google/android/gms/internal/g;
.super Lcom/google/android/gms/internal/ad;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/g$b;,
        Lcom/google/android/gms/internal/g$a;
    }
.end annotation


# static fields
.field static final dm:Landroid/net/Uri;

.field static final dn:Landroid/net/Uri;

.field static final do:Landroid/net/Uri;


# instance fields
.field private final dA:Ljava/lang/Runnable;

.field private final dB:Lcom/google/android/gms/plus/PlusClient$b;

.field private final dC:Lcom/google/android/gms/plus/PlusClient$b;

.field private final dD:Lcom/google/android/gms/plus/PlusClient$c;

.field private final dp:Landroid/view/Display;

.field private dq:Landroid/widget/PopupWindow;

.field private dr:Z

.field private final ds:Landroid/widget/ImageView;

.field private final dt:Landroid/widget/ImageView;

.field private du:Lcom/google/android/gms/internal/av;

.field private dv:Landroid/view/View$OnClickListener;

.field private dw:Z

.field private dx:Z

.field protected dy:Z

.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "plus_one_button_popup_beak_up"

    invoke-static {v0}, Lcom/google/android/gms/internal/e;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/g;->dm:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_beak_down"

    invoke-static {v0}, Lcom/google/android/gms/internal/e;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/g;->dn:Landroid/net/Uri;

    const-string v0, "plus_one_button_popup_bg"

    invoke-static {v0}, Lcom/google/android/gms/internal/e;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/g;->do:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/ad;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/google/android/gms/internal/v;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/v;-><init>(Lcom/google/android/gms/internal/g;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dA:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/gms/internal/g$a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/g$a;-><init>(Lcom/google/android/gms/internal/g;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dB:Lcom/google/android/gms/plus/PlusClient$b;

    new-instance v0, Lcom/google/android/gms/internal/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/w;-><init>(Lcom/google/android/gms/internal/g;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dC:Lcom/google/android/gms/plus/PlusClient$b;

    new-instance v0, Lcom/google/android/gms/internal/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/y;-><init>(Lcom/google/android/gms/internal/g;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dD:Lcom/google/android/gms/plus/PlusClient$c;

    iput-object p1, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dp:Landroid/view/Display;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->ds:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->ds:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/internal/g;->dm:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dt:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dt:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/internal/g;->do:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cD:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cF:Lcom/google/android/gms/internal/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/internal/n;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/g;Landroid/widget/PopupWindow;)Landroid/widget/PopupWindow;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/g;Lcom/google/android/gms/internal/av;)Lcom/google/android/gms/internal/av;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/g;->du:Lcom/google/android/gms/internal/av;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/g;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/g;Z)Z
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/g;->dx:Z

    return v0
.end method

.method private ac()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method private ae()V
    .locals 3

    const/4 v2, 0x3

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dw:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/g;->dw:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/bk;->am()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v0, "plus_popup_text"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/g;->m(Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    const-string v0, "text"

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/internal/g;->b(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/g;->b(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "Text confirmation popup requested but text is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/g;->setType(I)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;)Landroid/widget/FrameLayout;
    .locals 4

    const/4 v3, -0x1

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/g;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method private b(Landroid/widget/FrameLayout;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, -0x2

    new-instance v1, Lcom/google/android/gms/internal/g$b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/internal/g$b;-><init>(Lcom/google/android/gms/internal/g;Landroid/widget/FrameLayout;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-boolean v0, v1, Lcom/google/android/gms/internal/g$b;->cq:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/internal/g;->dn:Landroid/net/Uri;

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v1, Lcom/google/android/gms/internal/g$b;->cv:I

    invoke-direct {v0, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget v3, v1, Lcom/google/android/gms/internal/g$b;->cr:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v4, v1, Lcom/google/android/gms/internal/g$b;->ct:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v3, v5, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {p1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v0, p1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    const/16 v2, 0x33

    iget v3, v1, Lcom/google/android/gms/internal/g$b;->cw:I

    iget v1, v1, Lcom/google/android/gms/internal/g$b;->cx:I

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dA:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/g;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dA:Ljava/lang/Runnable;

    const-wide/16 v1, 0xbb8

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/internal/g;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lcom/google/android/gms/internal/g;->dm:Landroid/net/Uri;

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/g;)Lcom/google/android/gms/plus/PlusClient$b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dC:Lcom/google/android/gms/plus/PlusClient$b;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/g;)Landroid/view/Display;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dp:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/internal/g;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->ds:Landroid/widget/ImageView;

    return-object v0
.end method

.method private m(Ljava/lang/String;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cP:Landroid/content/res/Resources;

    const-string v1, "layout"

    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/internal/g;->cQ:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/internal/g;->cP:Landroid/content/res/Resources;

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getLayout(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(Lorg/xmlpull/v1/XmlPullParser;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final R()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/internal/ad;->R()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/g;->ac()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/g;->ae()V

    return-void
.end method

.method public final S()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/gms/internal/ad;->S()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/g;->ac()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/g;->ae()V

    return-void
.end method

.method public final T()V
    .locals 8

    const/4 v4, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/gms/internal/ad;->T()V

    invoke-direct {p0}, Lcom/google/android/gms/internal/g;->ac()V

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dw:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/gms/internal/g;->dw:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->du:Lcom/google/android/gms/internal/av;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->du:Lcom/google/android/gms/internal/av;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/av;->F()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/bk;->am()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/g;->du:Lcom/google/android/gms/internal/av;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/av;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    iget-object v0, v0, Lcom/google/android/gms/internal/bk;->bM:Landroid/os/Bundle;

    const-string v3, "visibility"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v0, "plus_popup_confirmation"

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/g;->m(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/internal/au;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->mContext:Landroid/content/Context;

    invoke-direct {v4, v0}, Lcom/google/android/gms/internal/au;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/au;->a(Lcom/google/android/gms/plus/PlusClient;)V

    const/high16 v0, 0x42000000

    invoke-virtual {p0}, Lcom/google/android/gms/internal/g;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v0, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/internal/bh;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v5, "content"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "com.google.android.gms.plus"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "images"

    invoke-virtual {v2, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "url"

    invoke-virtual {v2, v5, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/internal/au;->b(Landroid/net/Uri;)V

    const-string v0, "profile_image"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    const-string v0, "text"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/internal/g;->b(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/g;->b(Landroid/widget/FrameLayout;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "Confirmation popup requested but content view cannot be created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/internal/g;->setType(I)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/internal/ad;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x2

    iget-boolean v0, p0, Lcom/google/android/gms/internal/g;->dx:Z

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: result pending, ignoring +1 button click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/g;->dw:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/g;->dw:Z

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dv:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dv:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/bk;->ah()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onClick: undo +1"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final onConnected()V
    .locals 2

    const-string v0, "PlusOneButtonWithPopup"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/g;->cO:Lcom/google/android/gms/internal/bk;

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->du:Lcom/google/android/gms/internal/av;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/internal/g;->dD:Lcom/google/android/gms/plus/PlusClient$c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/PlusClient;->a(Lcom/google/android/gms/plus/PlusClient$c;)V

    :cond_1
    return-void
.end method

.method public final onConnectionFailed$5d4cef71()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/g;->R()V

    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/gms/internal/ad;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/g;->dr:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/g;->dq:Landroid/widget/PopupWindow;

    :cond_0
    return-void
.end method

.method public final onDisconnected()V
    .locals 0

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    if-ne p1, p0, :cond_0

    invoke-super {p0, p0}, Lcom/google/android/gms/internal/ad;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/internal/g;->dv:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method
