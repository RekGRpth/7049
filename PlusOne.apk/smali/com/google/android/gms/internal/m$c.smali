.class final Lcom/google/android/gms/internal/m$c;
.super Lcom/google/android/gms/internal/aj;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/m;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "c"
.end annotation


# instance fields
.field private final br:Lcom/google/android/gms/plus/PlusClient$a;

.field final synthetic x:Lcom/google/android/gms/internal/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/m;Lcom/google/android/gms/plus/PlusClient$a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/m$c;->x:Lcom/google/android/gms/internal/m;

    invoke-direct {p0}, Lcom/google/android/gms/internal/aj;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/internal/m$c;->br:Lcom/google/android/gms/plus/PlusClient$a;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :cond_0
    new-instance v1, Lcom/google/android/gms/common/ConnectionResult;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/m$c;->x:Lcom/google/android/gms/internal/m;

    new-instance v2, Lcom/google/android/gms/internal/m$d;

    iget-object v3, p0, Lcom/google/android/gms/internal/m$c;->x:Lcom/google/android/gms/internal/m;

    iget-object v4, p0, Lcom/google/android/gms/internal/m$c;->br:Lcom/google/android/gms/plus/PlusClient$a;

    invoke-direct {v2, v3, v4, v1, p3}, Lcom/google/android/gms/internal/m$d;-><init>(Lcom/google/android/gms/internal/m;Lcom/google/android/gms/plus/PlusClient$a;Lcom/google/android/gms/common/ConnectionResult;Landroid/os/ParcelFileDescriptor;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/m;->a(Lcom/google/android/gms/internal/aa$c;)V

    return-void
.end method
