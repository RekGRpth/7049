.class public final Lcom/google/android/gms/internal/m;
.super Lcom/google/android/gms/internal/aa;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/gms/internal/m$c;,
        Lcom/google/android/gms/internal/m$d;,
        Lcom/google/android/gms/internal/m$a;,
        Lcom/google/android/gms/internal/m$e;,
        Lcom/google/android/gms/internal/m$b;,
        Lcom/google/android/gms/internal/m$f;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/internal/aa",
        "<",
        "Lcom/google/android/gms/internal/at;",
        ">;"
    }
.end annotation


# instance fields
.field private final dF:Ljava/lang/String;

.field private final dG:Ljava/lang/String;

.field private final dH:Ljava/lang/String;


# virtual methods
.method protected final a(Lcom/google/android/gms/internal/p;Lcom/google/android/gms/internal/aa$a;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/p;",
            "Lcom/google/android/gms/internal/aa",
            "<",
            "Lcom/google/android/gms/internal/at;",
            ">.a;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v0, "skip_oob"

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/internal/m;->dF:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/internal/m;->dG:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->j()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/internal/m;->dH:Ljava/lang/String;

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/internal/p;->a(Lcom/google/android/gms/internal/ar;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/PlusClient$a;Landroid/net/Uri;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->m()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v0, "bounding_box"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v2, Lcom/google/android/gms/internal/m$c;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/internal/m$c;-><init>(Lcom/google/android/gms/internal/m;Lcom/google/android/gms/plus/PlusClient$a;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/at;

    invoke-interface {v0, v2, p2, v1}, Lcom/google/android/gms/internal/at;->a(Lcom/google/android/gms/internal/x;Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v3, v3}, Lcom/google/android/gms/internal/m$c;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/PlusClient$b;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->m()V

    new-instance v1, Lcom/google/android/gms/internal/m$b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/internal/m$b;-><init>(Lcom/google/android/gms/internal/m;Lcom/google/android/gms/plus/PlusClient$b;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/at;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/internal/at;->a(Lcom/google/android/gms/internal/x;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/internal/m$b;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/PlusClient$c;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->m()V

    new-instance v1, Lcom/google/android/gms/internal/m$a;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/internal/m$a;-><init>(Lcom/google/android/gms/internal/m;Lcom/google/android/gms/plus/PlusClient$c;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/m;->n()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/at;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/at;->a(Lcom/google/android/gms/internal/x;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/internal/m$a;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final h()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method protected final i()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method

.method protected final bridge synthetic j(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/at$a;->x(Landroid/os/IBinder;)Lcom/google/android/gms/internal/at;

    move-result-object v0

    return-object v0
.end method
