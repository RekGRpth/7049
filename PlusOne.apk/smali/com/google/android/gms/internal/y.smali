.class final Lcom/google/android/gms/internal/y;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/plus/PlusClient$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gms/internal/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic k:Lcom/google/android/gms/internal/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/g;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/internal/av;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    invoke-static {v0, p2}, Lcom/google/android/gms/internal/g;->a(Lcom/google/android/gms/internal/g;Lcom/google/android/gms/internal/av;)Lcom/google/android/gms/internal/av;

    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    invoke-static {v0}, Lcom/google/android/gms/internal/g;->c(Lcom/google/android/gms/internal/g;)Lcom/google/android/gms/plus/PlusClient$b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    invoke-virtual {v3, v0, v3}, Lcom/google/android/gms/plus/PlusClient;->a(Lcom/google/android/gms/plus/PlusClient$b;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/g;->dy:Z

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonWithPopup"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onSignUpStateLoaded: performing pending click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    iput-boolean v2, v0, Lcom/google/android/gms/internal/g;->cz:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/g;->performClick()Z

    iget-object v0, p0, Lcom/google/android/gms/internal/y;->k:Lcom/google/android/gms/internal/g;

    iput-boolean v2, v0, Lcom/google/android/gms/internal/g;->dy:Z

    :cond_1
    return-void
.end method
