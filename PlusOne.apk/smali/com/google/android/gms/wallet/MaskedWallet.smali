.class public final Lcom/google/android/gms/wallet/MaskedWallet;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/internal/al;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ah;

.field public static final DEFAULT_INSTANCE:Lcom/google/android/gms/wallet/MaskedWallet;


# instance fields
.field public aud:Ljava/lang/String;

.field public billingAddress:Lcom/google/android/gms/wallet/Address;

.field public email:Ljava/lang/String;

.field public exp:J

.field public googleTransactionId:Ljava/lang/String;

.field public iat:J

.field public iss:Ljava/lang/String;

.field public mVersionCode:I

.field public merchantTransactionId:Ljava/lang/String;

.field public paymentDescriptions:[Ljava/lang/String;

.field public shippingAddress:Lcom/google/android/gms/wallet/Address;

.field public typ:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ah;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ah;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/MaskedWallet;->CREATOR:Lcom/google/android/gms/internal/ah;

    new-instance v0, Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/MaskedWallet;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/MaskedWallet;->DEFAULT_INSTANCE:Lcom/google/android/gms/wallet/MaskedWallet;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/MaskedWallet;->mVersionCode:I

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/MaskedWallet;->CREATOR:Lcom/google/android/gms/internal/ah;

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/wallet/MaskedWallet;->CREATOR:Lcom/google/android/gms/internal/ah;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ah;->a(Lcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Parcel;I)V

    return-void
.end method
