.class final Lcom/google/android/gsf/EsGservices$1$1;
.super Landroid/database/ContentObserver;
.source "EsGservices.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/gsf/EsGservices$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/gsf/EsGservices$1;


# direct methods
.method constructor <init>(Lcom/google/android/gsf/EsGservices$1;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/gsf/EsGservices$1$1;->this$0:Lcom/google/android/gsf/EsGservices$1;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public final onChange(Z)V
    .locals 3
    .param p1    # Z

    const-class v1, Lcom/google/android/gsf/EsGservices;

    monitor-enter v1

    :try_start_0
    # getter for: Lcom/google/android/gsf/EsGservices;->sCache:Ljava/util/HashMap;
    invoke-static {}, Lcom/google/android/gsf/EsGservices;->access$000()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    # setter for: Lcom/google/android/gsf/EsGservices;->sVersionToken:Ljava/lang/Object;
    invoke-static {v0}, Lcom/google/android/gsf/EsGservices;->access$102(Ljava/lang/Object;)Ljava/lang/Object;

    # getter for: Lcom/google/android/gsf/EsGservices;->sPreloadedPrefixes:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gsf/EsGservices;->access$200()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    # getter for: Lcom/google/android/gsf/EsGservices;->sResolver:Landroid/content/ContentResolver;
    invoke-static {}, Lcom/google/android/gsf/EsGservices;->access$300()Landroid/content/ContentResolver;

    move-result-object v0

    # getter for: Lcom/google/android/gsf/EsGservices;->sPreloadedPrefixes:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/gsf/EsGservices;->access$200()[Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gsf/EsGservices;->bulkCacheByPrefix(Landroid/content/ContentResolver;[Ljava/lang/String;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
