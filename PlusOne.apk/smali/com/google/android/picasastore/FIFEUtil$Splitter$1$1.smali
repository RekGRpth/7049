.class final Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;
.super Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/FIFEUtil$Splitter$1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/picasastore/FIFEUtil$Splitter$1;


# direct methods
.method constructor <init>(Lcom/google/android/picasastore/FIFEUtil$Splitter$1;Lcom/google/android/picasastore/FIFEUtil$Splitter;Ljava/lang/CharSequence;)V
    .locals 0
    .param p2    # Lcom/google/android/picasastore/FIFEUtil$Splitter;
    .param p3    # Ljava/lang/CharSequence;

    iput-object p1, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;->this$0:Lcom/google/android/picasastore/FIFEUtil$Splitter$1;

    invoke-direct {p0, p2, p3}, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;-><init>(Lcom/google/android/picasastore/FIFEUtil$Splitter;Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public final separatorEnd(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;->this$0:Lcom/google/android/picasastore/FIFEUtil$Splitter$1;

    iget-object v0, v0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1;->val$separator:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, p1

    return v0
.end method

.method public final separatorStart(I)I
    .locals 6
    .param p1    # I

    iget-object v4, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;->this$0:Lcom/google/android/picasastore/FIFEUtil$Splitter$1;

    iget-object v4, v4, Lcom/google/android/picasastore/FIFEUtil$Splitter$1;->val$separator:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    move v3, p1

    iget-object v4, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    sub-int v2, v4, v0

    :goto_0
    if-gt v3, v2, :cond_1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_2

    iget-object v4, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;->toSplit:Ljava/lang/CharSequence;

    add-int v5, v1, v3

    invoke-interface {v4, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    iget-object v5, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$1$1;->this$0:Lcom/google/android/picasastore/FIFEUtil$Splitter$1;

    iget-object v5, v5, Lcom/google/android/picasastore/FIFEUtil$Splitter$1;->val$separator:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, -0x1

    :cond_2
    return v3
.end method
