.class abstract Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;
.super Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;
.source "FIFEUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/FIFEUtil$Splitter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "SplittingIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field offset:I

.field final omitEmptyStrings:Z

.field final toSplit:Ljava/lang/CharSequence;


# direct methods
.method protected constructor <init>(Lcom/google/android/picasastore/FIFEUtil$Splitter;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Lcom/google/android/picasastore/FIFEUtil$Splitter;
    .param p2    # Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;-><init>(B)V

    iput v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->offset:I

    # getter for: Lcom/google/android/picasastore/FIFEUtil$Splitter;->omitEmptyStrings:Z
    invoke-static {p1}, Lcom/google/android/picasastore/FIFEUtil$Splitter;->access$200(Lcom/google/android/picasastore/FIFEUtil$Splitter;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->omitEmptyStrings:Z

    iput-object p2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->toSplit:Ljava/lang/CharSequence;

    return-void
.end method


# virtual methods
.method protected final bridge synthetic computeNext()Ljava/lang/Object;
    .locals 4

    const/4 v3, -0x1

    :cond_0
    iget v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->offset:I

    if-eq v0, v3, :cond_3

    iget v1, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->offset:I

    iget v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->offset:I

    invoke-virtual {p0, v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->separatorStart(I)I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iput v3, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->offset:I

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->omitEmptyStrings:Z

    if-eqz v2, :cond_1

    if-eq v1, v0, :cond_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->toSplit:Ljava/lang/CharSequence;

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->separatorEnd(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$SplittingIterator;->offset:I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;->DONE:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    iput-object v0, p0, Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator;->state:Lcom/google/android/picasastore/FIFEUtil$Splitter$AbstractIterator$State;

    const/4 v0, 0x0

    goto :goto_1
.end method

.method abstract separatorEnd(I)I
.end method

.method abstract separatorStart(I)I
.end method
