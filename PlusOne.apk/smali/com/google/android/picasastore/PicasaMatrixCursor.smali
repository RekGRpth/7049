.class public final Lcom/google/android/picasastore/PicasaMatrixCursor;
.super Landroid/database/AbstractCursor;
.source "PicasaMatrixCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;
    }
.end annotation


# instance fields
.field private final columnCount:I

.field private final columnNames:[Ljava/lang/String;

.field private data:[Ljava/lang/Object;

.field private rowCount:I


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1
    .param p1    # [Ljava/lang/String;

    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lcom/google/android/picasastore/PicasaMatrixCursor;-><init>([Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>([Ljava/lang/String;I)V
    .locals 1
    .param p1    # [Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    iput-object p1, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnNames:[Ljava/lang/String;

    array-length v0, p1

    iput v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    iget v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    mul-int/lit8 v0, v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/picasastore/PicasaMatrixCursor;)[Ljava/lang/Object;
    .locals 1
    .param p0    # Lcom/google/android/picasastore/PicasaMatrixCursor;

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    return-object v0
.end method

.method private ensureCapacity(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    array-length v2, v2

    if-le p1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    array-length v2, v2

    mul-int/lit8 v0, v2, 0x2

    if-ge v0, p1, :cond_0

    move v0, p1

    :cond_0
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    return-void
.end method

.method private get(I)Ljava/lang/Object;
    .locals 3
    .param p1    # I

    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    if-lt p1, v0, :cond_1

    :cond_0
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested column: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", # of columns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->mPos:I

    if-gez v0, :cond_2

    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "Before first row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->mPos:I

    iget v1, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    if-lt v0, v1, :cond_3

    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "After last row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->mPos:I

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    mul-int/2addr v1, v2

    add-int/2addr v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public final addRow([Ljava/lang/Object;)V
    .locals 4
    .param p1    # [Ljava/lang/Object;

    array-length v1, p1

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "columnNames.length = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", columnValues.length = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget v1, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    mul-int v0, v1, v2

    iget v1, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    add-int/2addr v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->ensureCapacity(I)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->data:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    invoke-static {p1, v1, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public final getBlob(I)[B
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public final getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnNames:[Ljava/lang/String;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    return v0
.end method

.method public final getDouble(I)D
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    goto :goto_0
.end method

.method public final getFloat(I)F
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public final getInt(I)I
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public final getLong(I)J
    .locals 3
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_0
.end method

.method public final getShort(I)S
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v1

    goto :goto_0
.end method

.method public final getString(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final getType(I)I
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    instance-of v1, v0, [B

    if-eqz v1, :cond_1

    const/4 v1, 0x4

    goto :goto_0

    :cond_1
    instance-of v1, v0, Ljava/lang/Float;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_3

    :cond_2
    const/4 v1, 0x2

    goto :goto_0

    :cond_3
    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_4

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    const/4 v1, 0x3

    goto :goto_0
.end method

.method public final isNull(I)Z
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final newRow()Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;
    .locals 4

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->rowCount:I

    iget v3, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    mul-int v0, v2, v3

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaMatrixCursor;->ensureCapacity(I)V

    iget v2, p0, Lcom/google/android/picasastore/PicasaMatrixCursor;->columnCount:I

    sub-int v1, v0, v2

    new-instance v2, Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;

    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/picasastore/PicasaMatrixCursor$RowBuilder;-><init>(Lcom/google/android/picasastore/PicasaMatrixCursor;II)V

    return-object v2
.end method
