.class public Lcom/google/android/picasastore/PicasaPhotoContentProvider;
.super Landroid/content/ContentProvider;
.source "PicasaPhotoContentProvider.java"


# static fields
.field private static final EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;


# instance fields
.field private mAuthority:Ljava/lang/String;

.field private mExternalStorageFsId:I

.field private mFingerprintManager:Lcom/google/android/picasastore/FingerprintManager;

.field private mIsExternalStorageFsIdReady:Z

.field private mPrefs:Landroid/content/SharedPreferences;

.field private final mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "content://media/external/fs_id"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mIsExternalStorageFsIdReady:Z

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/picasastore/PicasaPhotoContentProvider;)V
    .locals 0
    .param p0    # Lcom/google/android/picasastore/PicasaPhotoContentProvider;

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->onFsIdChanged()V

    return-void
.end method

.method private static getFsId(Landroid/content/Context;)I
    .locals 7
    .param p0    # Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_0
    return v1

    :cond_0
    :try_start_1
    const-string v1, "gp.PicasaPhotoCP"

    const-string v2, "No FSID on this device!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    const/4 v1, -0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private declared-synchronized onFsIdChanged()V
    .locals 5

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "mounted_ro"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    :goto_0
    if-nez v2, :cond_3

    :cond_1
    :goto_1
    monitor-exit p0

    return-void

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->getFsId(Landroid/content/Context;)I

    move-result v1

    iget-boolean v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mIsExternalStorageFsIdReady:Z

    if-nez v2, :cond_4

    const-string v2, "gp.PicasaPhotoCP"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "set fsid first time:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mIsExternalStorageFsIdReady:Z

    iput v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mExternalStorageFsId:I

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "external_storage_fsid"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :cond_4
    :try_start_2
    iget v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mExternalStorageFsId:I

    if-eq v2, v1, :cond_1

    const-string v2, "gp.PicasaPhotoCP"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fsid changed: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mExternalStorageFsId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mExternalStorageFsId:I

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mPrefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "external_storage_fsid"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->isMaster()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mFingerprintManager:Lcom/google/android/picasastore/FingerprintManager;

    invoke-virtual {v2}, Lcom/google/android/picasastore/FingerprintManager;->reset()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private queryFingerprint(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;

    const/4 v9, 0x0

    const-string v8, "1"

    const-string v10, "force_recalculate"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v8, "1"

    const-string v10, "cache_only"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Lcom/google/android/picasastore/PicasaMatrixCursor;

    invoke-direct {v2, p2}, Lcom/google/android/picasastore/PicasaMatrixCursor;-><init>([Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mFingerprintManager:Lcom/google/android/picasastore/FingerprintManager;

    array-length v8, p2

    new-array v1, v8, [Ljava/lang/Object;

    if-eqz v0, :cond_2

    const/4 v6, 0x0

    array-length v7, p2

    :goto_1
    if-ge v6, v7, :cond_4

    aget-object v8, p2, v6

    invoke-virtual {v4, v8}, Lcom/google/android/picasastore/FingerprintManager;->getCachedFingerprint(Ljava/lang/String;)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v8, v9

    :goto_2
    aput-object v8, v1, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v8

    goto :goto_2

    :cond_2
    const/4 v6, 0x0

    array-length v7, p2

    :goto_3
    if-ge v6, v7, :cond_4

    aget-object v8, p2, v6

    invoke-virtual {v4, v8, v5}, Lcom/google/android/picasastore/FingerprintManager;->getFingerprint(Ljava/lang/String;Z)Lcom/android/gallery3d/common/Fingerprint;

    move-result-object v3

    if-nez v3, :cond_3

    move-object v8, v9

    :goto_4
    aput-object v8, v1, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Lcom/android/gallery3d/common/Fingerprint;->getBytes()[B

    move-result-object v8

    goto :goto_4

    :cond_4
    invoke-virtual {v2, v1}, Lcom/google/android/picasastore/PicasaMatrixCursor;->addRow([Ljava/lang/Object;)V

    return-object v2
.end method


# virtual methods
.method public attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/pm/ProviderInfo;

    invoke-super {p0, p1, p2}, Landroid/content/ContentProvider;->attachInfo(Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V

    iget-object v0, p2, Landroid/content/pm/ProviderInfo;->authority:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mAuthority:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "photos"

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "fingerprint"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "photos/#"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mAuthority:Ljava/lang/String;

    const-string v2, "albumcovers/#"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported uri:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mFingerprintManager:Lcom/google/android/picasastore/FingerprintManager;

    invoke-virtual {v0, p3}, Lcom/google/android/picasastore/FingerprintManager;->invalidate([Ljava/lang/String;)I

    move-result v0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.picasasync.item"

    :goto_0
    return-object v0

    :pswitch_2
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.item"

    goto :goto_0

    :pswitch_3
    const-string v0, "vnd.android.cursor.item/vnd.google.android.picasasync.album_cover"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "INSERT "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unsupported uri:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v1
.end method

.method public onCreate()Z
    .locals 8

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/picasastore/Config;->init(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/google/android/picasastore/FingerprintManager;->get(Landroid/content/Context;)Lcom/google/android/picasastore/FingerprintManager;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mFingerprintManager:Lcom/google/android/picasastore/FingerprintManager;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mPrefs:Landroid/content/SharedPreferences;

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "picasa-photo-provider"

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/google/android/picasastore/PicasaPhotoContentProvider$2;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v1, p0, v3}, Lcom/google/android/picasastore/PicasaPhotoContentProvider$2;-><init>(Lcom/google/android/picasastore/PicasaPhotoContentProvider;Landroid/os/Looper;)V

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->EXTERNAL_STORAGE_FSID_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/picasastore/PicasaPhotoContentProvider$1;

    invoke-direct {v6, p0, v1}, Lcom/google/android/picasastore/PicasaPhotoContentProvider$1;-><init>(Lcom/google/android/picasastore/PicasaPhotoContentProvider;Landroid/os/Handler;)V

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    invoke-static {v1, v7}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    return v7
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OPEN "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "unsupported uri: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v2

    :pswitch_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPicasaStore()Lcom/google/android/picasastore/PicasaStore;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/android/picasastore/PicasaStore;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    :goto_0
    return-object v2

    :pswitch_2
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPicasaStore()Lcom/google/android/picasastore/PicasaStore;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/google/android/picasastore/PicasaStore;->openAlbumCover(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 5
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "QUERY "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid URI: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    throw v2

    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->queryFingerprint(Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/picasastore/MetricsUtils;->incrementQueryResultCount(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaPhotoContentProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported uri:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
