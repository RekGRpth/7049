.class final Lcom/google/android/picasastore/PicasaStore;
.super Ljava/lang/Object;
.source "PicasaStore.java"

# interfaces
.implements Lcom/google/android/picasastore/UrlDownloader$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;,
        Lcom/google/android/picasastore/PicasaStore$ImagePack;,
        Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;,
        Lcom/google/android/picasastore/PicasaStore$DownloadWriter;,
        Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;,
        Lcom/google/android/picasastore/PicasaStore$DownloadListener;
    }
.end annotation


# instance fields
.field private mBlobCache:Lcom/android/gallery3d/common/BlobCache;

.field private mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
            "<[B>;"
        }
    .end annotation
.end field

.field private mCacheDir:Ljava/io/File;

.field private final mContext:Landroid/content/Context;

.field private final mCreatePipe:Ljava/lang/reflect/Method;

.field private mFileCache:Lcom/android/gallery3d/common/FileCache;

.field private mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
            "<",
            "Lcom/google/android/picasastore/PicasaStore$ImagePack;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeepAlive:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/os/ParcelFileDescriptor;",
            "Ljava/net/Socket;",
            ">;"
        }
    .end annotation
.end field

.field private final mMountListener:Landroid/content/BroadcastReceiver;

.field private final mServerSocket:Ljava/net/ServerSocket;

.field private final mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

.field private final mUrlDownloader:Lcom/google/android/picasastore/UrlDownloader;

.field private mUsingInternalStorage:Z

.field private mVersionInfo:Lcom/google/android/picasastore/VersionInfo;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    new-instance v3, Ljava/util/WeakHashMap;

    invoke-direct {v3}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mKeepAlive:Ljava/util/WeakHashMap;

    new-instance v3, Lcom/android/gallery3d/util/ThreadPool;

    invoke-direct {v3}, Lcom/android/gallery3d/util/ThreadPool;-><init>()V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    new-instance v3, Lcom/google/android/picasastore/PicasaStore$2;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/PicasaStore$2;-><init>(Lcom/google/android/picasastore/PicasaStore;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    new-instance v3, Lcom/google/android/picasastore/PicasaStore$3;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/PicasaStore$3;-><init>(Lcom/google/android/picasastore/PicasaStore;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/google/android/picasastore/UrlDownloader;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/UrlDownloader;-><init>(Lcom/google/android/picasastore/UrlDownloader$Controller;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mUrlDownloader:Lcom/google/android/picasastore/UrlDownloader;

    :try_start_0
    new-instance v3, Ljava/net/ServerSocket;

    invoke-direct {v3}, Ljava/net/ServerSocket;-><init>()V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-virtual {v3, v4, v5}, Ljava/net/ServerSocket;->bind(Ljava/net/SocketAddress;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x0

    :try_start_1
    const-class v3, Landroid/os/ParcelFileDescriptor;

    const-string v4, "createPipe"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mCreatePipe:Ljava/lang/reflect/Method;

    new-instance v3, Lcom/google/android/picasastore/PicasaStore$1;

    invoke-direct {v3, p0}, Lcom/google/android/picasastore/PicasaStore$1;-><init>(Lcom/google/android/picasastore/PicasaStore;)V

    iput-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mMountListener:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "file"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore;->mMountListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "cannot create server socket"

    invoke-direct {v3, v4, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3

    :catch_1
    move-exception v3

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/picasastore/PicasaStore;)V
    .locals 0
    .param p0    # Lcom/google/android/picasastore/PicasaStore;

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->onMediaMountOrUnmount()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/picasastore/PicasaStore;JI)[B
    .locals 1
    .param p0    # Lcom/google/android/picasastore/PicasaStore;
    .param p1    # J
    .param p3    # I

    invoke-static {p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/picasastore/PicasaStore;Ljava/lang/String;)[B
    .locals 1
    .param p0    # Lcom/google/android/picasastore/PicasaStore;
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/picasastore/PicasaStore;[B[BI[B)V
    .locals 0
    .param p0    # Lcom/google/android/picasastore/PicasaStore;
    .param p1    # [B
    .param p2    # [B
    .param p3    # I
    .param p4    # [B

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache([B[BI[B)V

    return-void
.end method

.method private checkCacheVersion(Ljava/lang/String;I)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasastore/VersionInfo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getCacheDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/cache_versions.info"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/picasastore/VersionInfo;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    invoke-virtual {v0, p1}, Lcom/google/android/picasastore/VersionInfo;->getVersion(Ljava/lang/String;)I

    move-result v0

    if-eq v0, p2, :cond_1

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/picasastore/VersionInfo;->setVersion(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;

    invoke-virtual {v0}, Lcom/google/android/picasastore/VersionInfo;->sync()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 6
    .param p0    # Ljava/lang/String;
    .param p1    # I
    .param p2    # Z

    invoke-static {p0}, Lcom/google/android/picasastore/FIFEUtil;->isFifeHostedUrl(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {p0}, Lcom/google/android/picasastore/FIFEUtil;->getImageUrlOptions(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "I"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const-string v4, "k"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x73

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, "-no"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    const-string v4, "-c"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    if-eqz v1, :cond_1

    const-string v4, "-I"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    if-eqz v2, :cond_2

    const-string v4, "-k"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p0}, Lcom/google/android/picasastore/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_3
    if-eqz p2, :cond_4

    const-string v4, "gp.PicasaStore"

    const-string v5, "not a FIFE url, ignore the crop option"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {p1, p0}, Lcom/google/android/picasastore/ImageProxyUtil;->setImageUrlSize(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private createPipe()[Landroid/os/ParcelFileDescriptor;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mCreatePipe:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->createSocketPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mCreatePipe:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "gp.PicasaStore"

    const-string v2, "fail to create pipe"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/io/IOException;

    throw v0

    :cond_1
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private createSocketPipe()[Landroid/os/ParcelFileDescriptor;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-array v1, v8, [Ljava/net/Socket;

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/net/Socket;

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v5}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v5

    invoke-direct {v3, v4, v5}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStore;->mServerSocket:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v3

    aput-object v3, v1, v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-array v0, v8, [Landroid/os/ParcelFileDescriptor;

    aget-object v2, v1, v6

    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    aput-object v2, v0, v6

    aget-object v2, v1, v7

    invoke-static {v2}, Landroid/os/ParcelFileDescriptor;->fromSocket(Ljava/net/Socket;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    aput-object v2, v0, v7

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mKeepAlive:Ljava/util/WeakHashMap;

    aget-object v3, v0, v6

    aget-object v4, v1, v6

    invoke-virtual {v2, v3, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mKeepAlive:Ljava/util/WeakHashMap;

    aget-object v3, v0, v7

    aget-object v4, v1, v7

    invoke-virtual {v2, v3, v4}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized findInCacheOrDownload(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v3, 0x0

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getDownloadCache()Lcom/android/gallery3d/common/FileCache;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Lcom/android/gallery3d/common/FileCache;->lookup(Ljava/lang/String;)Lcom/android/gallery3d/common/FileCache$CacheEntry;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v4, v1, Lcom/android/gallery3d/common/FileCache$CacheEntry;->cacheFile:Ljava/io/File;

    const/high16 v5, 0x10000000

    invoke-static {v4, v5}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :cond_0
    :goto_0
    monitor-exit p0

    return-object v3

    :catch_0
    move-exception v2

    :try_start_1
    const-string v4, "gp.PicasaStore"

    const-string v5, "open image from download cache"

    invoke-static {v4, v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    if-nez p4, :cond_0

    const/4 v3, 0x0

    :try_start_2
    new-instance v4, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;

    iget-object v5, p0, Lcom/google/android/picasastore/PicasaStore;->mUrlDownloader:Lcom/google/android/picasastore/UrlDownloader;

    invoke-virtual {v5, p3}, Lcom/google/android/picasastore/UrlDownloader;->openInputStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, p0, p1, p2, v5}, Lcom/google/android/picasastore/PicasaStore$InputStreamWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/io/InputStream;)V

    const/4 v5, 0x1

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_3
    const-string v3, "gp.PicasaStore"

    const-string v4, "download fail"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v3, Ljava/io/FileNotFoundException;

    invoke-direct {v3}, Ljava/io/FileNotFoundException;-><init>()V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private declared-synchronized getBlobCache()Lcom/android/gallery3d/common/BlobCache;
    .locals 8

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getCacheDirectory()Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/picasa-cache"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "picasa-image-cache-version"

    const/4 v2, 0x5

    invoke-direct {p0, v0, v2}, Lcom/google/android/picasastore/PicasaStore;->checkCacheVersion(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/android/gallery3d/common/BlobCache;->deleteFiles(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    if-eqz v0, :cond_2

    new-instance v0, Lcom/android/gallery3d/common/BlobCache;

    const/16 v2, 0x4e2

    const/high16 v3, 0x3200000

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/BlobCache;-><init>(Ljava/lang/String;IIZI)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_3
    new-instance v0, Lcom/android/gallery3d/common/BlobCache;

    const/16 v2, 0x1388

    const/high16 v3, 0xc800000

    const/4 v4, 0x0

    const/4 v5, 0x5

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/BlobCache;-><init>(Ljava/lang/String;IIZI)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v7

    :try_start_4
    const-string v0, "gp.PicasaStore"

    const-string v2, "fail to create blob cache"

    invoke-static {v0, v2, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized getCacheDirectory()Ljava/io/File;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v2

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    if-nez v2, :cond_2

    const-string v2, "gp.PicasaStore"

    const-string v3, "switch to internal storage for picasastore cache"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    const-string v3, ".nomedia"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :goto_1
    :try_start_3
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    invoke-static {v2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "gp.PicasaStore"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fail to create \'.nomedia\' in "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized getDownloadCache()Lcom/android/gallery3d/common/FileCache;
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :try_start_1
    new-instance v2, Ljava/io/File;

    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    const-string v1, "download-cache"

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v0, "picasa-download-cache-version"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/picasastore/PicasaStore;->checkCacheVersion(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    const-string v1, "picasa-downloads"

    invoke-static {v0, v2, v1}, Lcom/android/gallery3d/common/FileCache;->deleteFiles(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/picasastore/PicasaStore;->mUsingInternalStorage:Z

    if-nez v0, :cond_2

    new-instance v0, Lcom/android/gallery3d/common/FileCache;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    const-string v3, "picasa-downloads"

    const-wide/32 v4, 0x6400000

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/FileCache;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_3
    new-instance v0, Lcom/android/gallery3d/common/FileCache;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mContext:Landroid/content/Context;

    const-string v3, "picasa-downloads"

    const-wide/32 v4, 0x1400000

    invoke-direct/range {v0 .. v5}, Lcom/android/gallery3d/common/FileCache;-><init>(Landroid/content/Context;Ljava/io/File;Ljava/lang/String;J)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v6

    :try_start_4
    const-string v0, "gp.PicasaStore"

    const-string v1, "fail to create file cache"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static getItemIdFromUri(Landroid/net/Uri;)J
    .locals 3
    .param p0    # Landroid/net/Uri;

    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :goto_0
    return-wide v0

    :catch_0
    move-exception v0

    const-string v0, "gp.PicasaStore"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cannot get id from: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method private static isKeyTooLong(I)Z
    .locals 1
    .param p0    # I

    const/16 v0, 0x7fff

    if-le p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;
    .locals 3
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;

    invoke-static {p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B

    move-result-object v1

    invoke-static {p4}, Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache([B[B)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v2

    return-object v2
.end method

.method private lookupBlobCache([B[B)Lcom/google/android/picasastore/PicasaStore$ImagePack;
    .locals 12
    .param p1    # [B
    .param p2    # [B

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getBlobCache()Lcom/android/gallery3d/common/BlobCache;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v7, 0x0

    :goto_0
    return-object v7

    :cond_0
    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v3

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v0, v3, v4}, Lcom/android/gallery3d/common/BlobCache;->lookup(J)[B

    move-result-object v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_1

    if-nez p2, :cond_4

    :try_start_2
    array-length v7, v1

    array-length v8, p1

    if-ge v7, v8, :cond_2

    const/4 v7, 0x0

    :goto_1
    if-nez v7, :cond_d

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v0

    throw v7
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v6

    const-string v7, "gp.PicasaStore"

    const-string v8, "cache lookup fail"

    invoke-static {v7, v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v7, 0x0

    goto :goto_0

    :cond_2
    const/4 v7, 0x0

    :try_start_3
    array-length v8, p1

    :goto_2
    if-ge v7, v8, :cond_c

    aget-byte v9, p1, v7

    aget-byte v10, v1, v7

    if-eq v9, v10, :cond_3

    const/4 v7, 0x0

    goto :goto_1

    :cond_3
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_4
    array-length v7, p1

    array-length v8, p2

    add-int/2addr v7, v8

    add-int/lit8 v8, v7, 0x3

    array-length v7, v1

    if-lt v7, v8, :cond_5

    invoke-static {v8}, Lcom/google/android/picasastore/PicasaStore;->isKeyTooLong(I)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_5
    const/4 v7, 0x0

    goto :goto_1

    :cond_6
    const/4 v7, 0x0

    array-length v9, p1

    :goto_3
    if-ge v7, v9, :cond_8

    aget-byte v10, p1, v7

    aget-byte v11, v1, v7

    if-eq v10, v11, :cond_7

    const/4 v7, 0x0

    goto :goto_1

    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_8
    array-length v7, p1

    add-int/lit8 v9, v7, 0x1

    aget-byte v7, v1, v7

    int-to-byte v10, v8

    if-eq v7, v10, :cond_9

    const/4 v7, 0x0

    goto :goto_1

    :cond_9
    add-int/lit8 v7, v9, 0x1

    aget-byte v9, v1, v9

    ushr-int/lit8 v8, v8, 0x8

    int-to-byte v8, v8

    if-eq v9, v8, :cond_a

    const/4 v7, 0x0

    goto :goto_1

    :cond_a
    add-int/lit8 v8, v7, 0x1

    const/4 v7, 0x0

    array-length v9, p2

    :goto_4
    if-ge v7, v9, :cond_c

    aget-byte v10, p2, v7

    add-int v11, v7, v8

    aget-byte v11, v1, v11

    if-eq v10, v11, :cond_b

    const/4 v7, 0x0

    goto :goto_1

    :cond_b
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    :cond_c
    const/4 v7, 0x1

    goto :goto_1

    :cond_d
    array-length v7, p1

    aget-byte v7, v1, v7

    and-int/lit16 v7, v7, 0xff

    array-length v8, p1

    add-int/lit8 v8, v8, 0x1

    aget-byte v8, v1, v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    add-int v5, v7, v8

    array-length v7, p1

    add-int/lit8 v7, v7, 0x2

    aget-byte v7, v1, v7

    and-int/lit16 v2, v7, 0xff

    new-instance v7, Lcom/google/android/picasastore/PicasaStore$ImagePack;

    invoke-direct {v7, v5, v2, v1}, Lcom/google/android/picasastore/PicasaStore$ImagePack;-><init>(II[B)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method

.method private static makeAuxKey(Ljava/lang/String;)[B
    .locals 2
    .param p0    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    const-string v1, "https://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    goto :goto_0

    :cond_1
    const-string v1, "http://"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x7

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, p0

    goto :goto_1
.end method

.method private static makeKey(JI)[B
    .locals 5
    .param p0    # J
    .param p2    # I

    const/16 v4, 0x8

    const/16 v2, 0x9

    new-array v0, v2, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    mul-int/lit8 v2, v1, 0x8

    ushr-long v2, p0, v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    int-to-byte v2, p2

    aput-byte v2, v0, v4

    return-object v0
.end method

.method private declared-synchronized onMediaMountOrUnmount()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mCacheDir:Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mBlobCache:Lcom/android/gallery3d/common/BlobCache;

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mFileCache:Lcom/android/gallery3d/common/FileCache;

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStore;->mVersionInfo:Lcom/google/android/picasastore/VersionInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private openFullImage(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    :try_start_0
    const-string v2, ".full"

    invoke-static {p1, p2, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    const/high16 v2, 0x10000000

    invoke-static {v0, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v1

    const-string v2, "gp.PicasaStore"

    const-string v3, "openFullImage from cache"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    if-nez p3, :cond_1

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/picasastore/PicasaStore;->findInCacheOrDownload(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    goto :goto_0
.end method

.method private openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;
    .locals 6
    .param p3    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/google/android/picasastore/PicasaStore$PipeDataWriter",
            "<TT;>;Z)",
            "Landroid/os/ParcelFileDescriptor;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    new-instance v0, Lcom/google/android/picasastore/PicasaStore$4;

    move-object v1, p0

    move v2, p3

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/picasastore/PicasaStore$4;-><init>(Lcom/google/android/picasastore/PicasaStore;ZLcom/google/android/picasastore/PicasaStore$PipeDataWriter;[Landroid/os/ParcelFileDescriptor;Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mThreadPool:Lcom/android/gallery3d/util/ThreadPool;

    invoke-virtual {v1, v0}, Lcom/android/gallery3d/util/ThreadPool;->submit(Lcom/android/gallery3d/util/ThreadPool$Job;)Lcom/android/gallery3d/util/Future;

    const/4 v1, 0x0

    aget-object v1, v4, v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v1

    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v2, "failure making pipe"

    invoke-direct {v1, v2}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private openScreenNail(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 11
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, v2, p3}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    invoke-direct {p0, v8, v1, v2}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    :try_start_0
    const-string v2, ".screen"

    invoke-static {p1, p2, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v7

    if-eqz v7, :cond_2

    const/high16 v2, 0x10000000

    invoke-static {v7, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v9

    const-string v2, "gp.PicasaStore"

    const-string v3, "openScreenNail from cache"

    invoke-static {v2, v3, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    if-nez p4, :cond_0

    if-nez p3, :cond_3

    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    throw v1

    :cond_3
    :try_start_1
    new-instance v0, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;-><init>(Lcom/google/android/picasastore/PicasaStore;JIILjava/lang/String;)V

    const/4 v10, 0x0

    new-instance v1, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;

    move-object v2, p0

    move-wide v3, p1

    move-object v5, p3

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V

    const/4 v2, 0x1

    invoke-direct {p0, v10, v1, v2}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_0

    :catch_1
    move-exception v9

    const-string v1, "gp.PicasaStore"

    const-string v2, "download fail"

    invoke-static {v1, v2, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v1, Ljava/io/FileNotFoundException;

    invoke-direct {v1}, Ljava/io/FileNotFoundException;-><init>()V

    throw v1
.end method

.method private openThumbNail(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;
    .locals 25
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v22

    if-eqz v22, :cond_0

    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->flags:I

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    :try_start_0
    const-string v4, ".screen"

    move-wide/from16 v0, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheFile(JLjava/lang/String;)Ljava/io/File;

    move-result-object v19

    if-eqz v19, :cond_2

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v20

    new-instance v21, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    sget v4, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    int-to-float v4, v4

    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v21

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(F)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static/range {v20 .. v21}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v18

    if-eqz v18, :cond_1

    sget v4, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-static {v0, v4, v5}, Lcom/android/gallery3d/common/BitmapUtils;->resizeAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v18

    const/16 v4, 0x5f

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/android/gallery3d/common/BitmapUtils;->compressToBytes(Landroid/graphics/Bitmap;I)[B

    move-result-object v10

    const/4 v7, 0x1

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move-object/from16 v8, p3

    invoke-direct/range {v4 .. v10}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache(JILjava/lang/String;I[B)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto :goto_0

    :cond_1
    const-string v4, "gp.PicasaStore"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "invalid prefetch file: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", length: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-object/from16 v3, p3

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v23

    if-eqz v23, :cond_3

    new-instance v21, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v4, 0x1

    move-object/from16 v0, v21

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->data:[B

    move-object/from16 v0, v23

    iget v5, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->offset:I

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->data:[B

    array-length v6, v6

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->offset:I

    sub-int/2addr v6, v7

    invoke-static {v4, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    sget v4, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    int-to-float v4, v4

    move-object/from16 v0, v21

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move-object/from16 v0, v21

    iget v6, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    invoke-static {v4}, Lcom/android/gallery3d/common/BitmapUtils;->computeSampleSizeLarger(F)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v4, 0x0

    move-object/from16 v0, v21

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->data:[B

    move-object/from16 v0, v23

    iget v5, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->offset:I

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->data:[B

    array-length v6, v6

    move-object/from16 v0, v23

    iget v7, v0, Lcom/google/android/picasastore/PicasaStore$ImagePack;->offset:I

    sub-int/2addr v6, v7

    invoke-static {v4, v5, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v18

    if-eqz v18, :cond_3

    sget v4, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-static {v0, v4, v5}, Lcom/android/gallery3d/common/BitmapUtils;->resizeAndCropCenter(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v18

    const/16 v4, 0x5f

    move-object/from16 v0, v18

    invoke-static {v0, v4}, Lcom/android/gallery3d/common/BitmapUtils;->compressToBytes(Landroid/graphics/Bitmap;I)[B

    move-result-object v10

    const/4 v7, 0x1

    const/4 v9, 0x0

    move-object/from16 v4, p0

    move-wide/from16 v5, p1

    move-object/from16 v8, p3

    invoke-direct/range {v4 .. v10}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache(JILjava/lang/String;I[B)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v4, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    :catch_0
    move-exception v24

    const-string v4, "gp.PicasaStore"

    const-string v5, "openThumbNail from screennail"

    move-object/from16 v0, v24

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_3
    if-eqz v22, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v4, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    goto/16 :goto_0

    :cond_4
    if-eqz p4, :cond_5

    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_5
    if-nez p3, :cond_6

    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4

    :cond_6
    :try_start_1
    new-instance v11, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;

    const/4 v15, 0x1

    const/16 v16, 0x1

    move-object/from16 v12, p0

    move-wide/from16 v13, p1

    move-object/from16 v17, p3

    invoke-direct/range {v11 .. v17}, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;-><init>(Lcom/google/android/picasastore/PicasaStore;JIILjava/lang/String;)V

    const/4 v12, 0x0

    new-instance v4, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v8, p3

    move-object v9, v11

    invoke-direct/range {v4 .. v9}, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v4, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    goto/16 :goto_0

    :catch_1
    move-exception v24

    const-string v4, "gp.PicasaStore"

    const-string v5, "download fail"

    move-object/from16 v0, v24

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v4, Ljava/io/FileNotFoundException;

    invoke-direct {v4}, Ljava/io/FileNotFoundException;-><init>()V

    throw v4
.end method

.method private putBlobCache(JILjava/lang/String;I[B)V
    .locals 3
    .param p1    # J
    .param p3    # I
    .param p4    # Ljava/lang/String;
    .param p5    # I
    .param p6    # [B

    invoke-static {p1, p2, p3}, Lcom/google/android/picasastore/PicasaStore;->makeKey(JI)[B

    move-result-object v1

    invoke-static {p4}, Lcom/google/android/picasastore/PicasaStore;->makeAuxKey(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2, p6}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache([B[BI[B)V

    return-void
.end method

.method private putBlobCache([B[BI[B)V
    .locals 10
    .param p1    # [B
    .param p2    # [B
    .param p3    # I
    .param p4    # [B

    const/4 v0, 0x0

    if-nez p2, :cond_1

    :goto_0
    array-length v7, p1

    add-int/2addr v7, v0

    add-int/lit8 v4, v7, 0x3

    invoke-static {v4}, Lcom/google/android/picasastore/PicasaStore;->isKeyTooLong(I)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    array-length v0, p2

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getBlobCache()Lcom/android/gallery3d/common/BlobCache;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v7, p4

    add-int/2addr v7, v4

    new-array v5, v7, [B

    const/4 v7, 0x0

    const/4 v8, 0x0

    array-length v9, p1

    invoke-static {p1, v7, v5, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v7, p1

    int-to-byte v8, v4

    aput-byte v8, v5, v7

    array-length v7, p1

    add-int/lit8 v7, v7, 0x1

    ushr-int/lit8 v8, v4, 0x8

    int-to-byte v8, v8

    aput-byte v8, v5, v7

    array-length v7, p1

    add-int/lit8 v7, v7, 0x2

    int-to-byte v8, p3

    aput-byte v8, v5, v7

    if-lez v0, :cond_3

    const/4 v7, 0x0

    array-length v8, p1

    add-int/lit8 v8, v8, 0x3

    invoke-static {p2, v7, v5, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    const/4 v7, 0x0

    array-length v8, p4

    invoke-static {p4, v7, v5, v4, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->crc64Long([B)J

    move-result-wide v2

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v1, v2, v3, v5}, Lcom/android/gallery3d/common/BlobCache;->insert(J[B)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v7

    :try_start_2
    monitor-exit v1

    throw v7
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v6

    const-string v7, "gp.PicasaStore"

    const-string v8, "cache insert fail"

    invoke-static {v7, v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final createTempFile()Ljava/io/File;
    .locals 3

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getDownloadCache()Lcom/android/gallery3d/common/FileCache;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v0}, Lcom/android/gallery3d/common/FileCache;->createFile()Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public final declared-synchronized onDownloadComplete(Ljava/lang/String;Ljava/io/File;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/io/File;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/picasastore/PicasaStore;->getDownloadCache()Lcom/android/gallery3d/common/FileCache;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/android/gallery3d/common/FileCache;->store(Ljava/lang/String;Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p2}, Ljava/io/File;->delete()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final openAlbumCover(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 24
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const-string v2, "w"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "invalid mode: "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/picasastore/PicasaStore;->getItemIdFromUri(Landroid/net/Uri;)J

    move-result-wide v3

    const-string v2, "content_url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v2, v6}, Lcom/google/android/picasastore/PicasaStore;->lookupBlobCache(JILjava/lang/String;)Lcom/google/android/picasastore/PicasaStore$ImagePack;

    move-result-object v21

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasastore/PicasaStore;->mImagePackWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_1
    if-nez v6, :cond_2

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    const-string v2, ".thumb"

    invoke-static {v3, v4, v6, v2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    if-eqz v18, :cond_6

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v10

    const-wide/32 v12, 0x80000

    cmp-long v2, v10, v12

    if-gez v2, :cond_5

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->length()J

    move-result-wide v10

    long-to-int v2, v10

    new-array v8, v2, [B

    new-instance v19, Ljava/io/FileInputStream;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/16 v20, 0x0

    const/4 v2, 0x0

    :try_start_0
    array-length v5, v8

    add-int/lit8 v5, v5, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v8, v2, v5}, Ljava/io/FileInputStream;->read([BII)I

    move-result v22

    :goto_2
    if-ltz v22, :cond_4

    array-length v2, v8

    move/from16 v0, v20

    if-ge v0, v2, :cond_4

    add-int v20, v20, v22

    array-length v2, v8

    sub-int v2, v2, v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v8, v1, v2}, Ljava/io/FileInputStream;->read([BII)I

    move-result v22

    goto :goto_2

    :cond_3
    const/16 v18, 0x0

    goto :goto_1

    :cond_4
    const/4 v5, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/picasastore/PicasaStore;->putBlobCache(JILjava/lang/String;I[B)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/picasastore/PicasaStore;->mBytesWriter:Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    invoke-static/range {v19 .. v19}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_0

    :catch_0
    move-exception v17

    :try_start_1
    new-instance v2, Ljava/io/FileNotFoundException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v2

    invoke-static/range {v19 .. v19}, Lcom/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v2

    :cond_5
    const/high16 v2, 0x10000000

    move-object/from16 v0, v18

    invoke-static {v0, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    goto/16 :goto_0

    :cond_6
    :try_start_2
    sget v2, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    const/4 v5, 0x1

    invoke-static {v6, v2, v5}, Lcom/google/android/picasastore/PicasaStore;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v16

    new-instance v9, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;

    const/4 v13, 0x2

    const/4 v14, 0x0

    move-object/from16 v10, p0

    move-wide v11, v3

    move-object v15, v6

    invoke-direct/range {v9 .. v15}, Lcom/google/android/picasastore/PicasaStore$BlobCacheRegister;-><init>(Lcom/google/android/picasastore/PicasaStore;JIILjava/lang/String;)V

    const/4 v2, 0x0

    new-instance v10, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;

    move-object/from16 v11, p0

    move-wide v12, v3

    move-object/from16 v14, v16

    move-object v15, v9

    invoke-direct/range {v10 .. v15}, Lcom/google/android/picasastore/PicasaStore$DownloadWriter;-><init>(Lcom/google/android/picasastore/PicasaStore;JLjava/lang/String;Lcom/google/android/picasastore/PicasaStore$DownloadListener;)V

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v10, v5}, Lcom/google/android/picasastore/PicasaStore;->openPipeHelper(Ljava/lang/Object;Lcom/google/android/picasastore/PicasaStore$PipeDataWriter;Z)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    goto/16 :goto_0

    :catch_1
    move-exception v23

    const-string v2, "gp.PicasaStore"

    const-string v5, "download fail"

    move-object/from16 v0, v23

    invoke-static {v2, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v2, Ljava/io/FileNotFoundException;

    invoke-direct {v2}, Ljava/io/FileNotFoundException;-><init>()V

    throw v2
.end method

.method public final openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 10
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    const-string v6, "w"

    invoke-virtual {p2, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v6, Ljava/io/FileNotFoundException;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "invalid mode: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    const-string v6, "type"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    const-string v7, "cache_only"

    invoke-virtual {p1, v7}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {p1}, Lcom/google/android/picasastore/PicasaStore;->getItemIdFromUri(Landroid/net/Uri;)J

    move-result-wide v3

    const-string v6, "content_url"

    invoke-virtual {p1, v6}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v6, 0x0

    cmp-long v6, v3, v6

    if-eqz v6, :cond_3

    const-string v6, "screennail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    sget v6, Lcom/google/android/picasastore/Config;->sScreenNailSize:I

    invoke-static {v1, v6, v8}, Lcom/google/android/picasastore/PicasaStore;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v3, v4, v2, v0}, Lcom/google/android/picasastore/PicasaStore;->openScreenNail(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    :goto_0
    return-object v6

    :cond_1
    const-string v6, "thumbnail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    sget v6, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    invoke-static {v1, v6, v9}, Lcom/google/android/picasastore/PicasaStore;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v3, v4, v2, v0}, Lcom/google/android/picasastore/PicasaStore;->openThumbNail(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_0

    :cond_2
    invoke-direct {p0, v3, v4, v1, v0}, Lcom/google/android/picasastore/PicasaStore;->openFullImage(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_6

    const-string v6, "thumbnail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    sget v6, Lcom/google/android/picasastore/Config;->sThumbNailSize:I

    invoke-static {v1, v6, v9}, Lcom/google/android/picasastore/PicasaStore;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    :cond_4
    :goto_1
    const-wide/16 v6, -0x1

    invoke-direct {p0, v6, v7, v1, v0}, Lcom/google/android/picasastore/PicasaStore;->findInCacheOrDownload(JLjava/lang/String;Z)Landroid/os/ParcelFileDescriptor;

    move-result-object v6

    goto :goto_0

    :cond_5
    const-string v6, "screennail"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    sget v6, Lcom/google/android/picasastore/Config;->sScreenNailSize:I

    invoke-static {v1, v6, v8}, Lcom/google/android/picasastore/PicasaStore;->convertImageUrl(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_6
    new-instance v6, Ljava/io/FileNotFoundException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v6
.end method
