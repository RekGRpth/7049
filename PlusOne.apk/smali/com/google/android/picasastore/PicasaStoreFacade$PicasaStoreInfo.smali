.class final Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
.super Ljava/lang/Object;
.source "PicasaStoreFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasastore/PicasaStoreFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PicasaStoreInfo"
.end annotation


# instance fields
.field public final authority:Ljava/lang/String;

.field public final packageName:Ljava/lang/String;

.field public final priority:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->packageName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->authority:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->priority:I

    return-void
.end method
