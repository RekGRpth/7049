.class public final Lcom/google/android/picasastore/PicasaStoreFacade;
.super Ljava/lang/Object;
.source "PicasaStoreFacade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasastore/PicasaStoreFacade$DummyService;,
        Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    }
.end annotation


# static fields
.field private static sCacheDir:Ljava/io/File;

.field private static sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

.field private static sNetworkReceiver:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private mAlbumCoversUri:Landroid/net/Uri;

.field private mAuthority:Ljava/lang/String;

.field private mCachedFingerprintUri:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private mFingerprintUri:Landroid/net/Uri;

.field private mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

.field private mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

.field private mPhotosUri:Landroid/net/Uri;

.field private mPicasaStore:Lcom/google/android/picasastore/PicasaStore;

.field private mPicasaStoreLock:Ljava/lang/Object;

.field private mRecalculateFingerprintUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPicasaStoreLock:Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->updatePicasaSyncInfo(Z)V

    return-void
.end method

.method public static broadcastOperationReport(Ljava/lang/String;JJIJJ)V
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # J
    .param p8    # J

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sNetworkReceiver:Ljava/lang/Class;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    iget-object v0, v2, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sNetworkReceiver:Ljava/lang/Class;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.google.android.picasastore.op_report"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "op_name"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "total_time"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "net_duration"

    invoke-virtual {v1, v2, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "transaction_count"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "sent_bytes"

    invoke-virtual {v1, v2, p6, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string v2, "received_bytes"

    invoke-virtual {v1, v2, p8, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static createCacheFile(JLjava/lang/String;)Ljava/io/File;
    .locals 12
    .param p0    # J
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v7

    if-nez v7, :cond_1

    move-object v0, v8

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v9, 0xa

    rem-long v9, p0, v9

    long-to-int v2, v9

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "picasa--"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    :goto_1
    const/4 v9, 0x5

    if-ge v6, v9, :cond_4

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v7, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v9

    if-nez v9, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    move-result v9

    if-eqz v9, :cond_3

    :cond_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    invoke-virtual {v0}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-nez v9, :cond_0

    :cond_3
    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "e"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :catch_0
    move-exception v3

    const-string v9, "gp.PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is full: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    move-object v0, v8

    goto :goto_0
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasastore/PicasaStoreFacade;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/picasastore/PicasaStoreFacade;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasastore/PicasaStoreFacade;

    invoke-direct {v0, p0}, Lcom/google/android/picasastore/PicasaStoreFacade;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;

    :cond_0
    sget-object v0, Lcom/google/android/picasastore/PicasaStoreFacade;->sInstance:Lcom/google/android/picasastore/PicasaStoreFacade;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getAlbumCoverCacheFile(JLjava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .param p0    # J
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "picasa_covers/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, p2}, Lcom/google/android/picasastore/PicasaStoreFacade;->getAlbumCoverKey(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getAlbumCoverKey(JLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0    # J
    .param p2    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->crc64Long(Ljava/lang/String;)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getCacheDirectory()Ljava/io/File;
    .locals 6

    const-class v3, Lcom/google/android/picasastore/PicasaStoreFacade;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    if-nez v2, :cond_1

    sget-object v2, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    const-string v4, "cache/com.google.android.googlephotos"

    invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/File;

    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    const-string v4, ".nomedia"

    invoke-direct {v0, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    sget-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit v3

    return-object v2

    :catch_0
    move-exception v2

    :try_start_3
    const-string v2, "gp.PicasaStore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fail to create \'.nomedia\' in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    sput-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_2
    :try_start_4
    const-string v2, "gp.PicasaStore"

    const-string v4, "fail to create cache dir in external storage"

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    sput-object v2, Lcom/google/android/picasastore/PicasaStoreFacade;->sCacheDir:Ljava/io/File;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public static getCacheFile(JLjava/lang/String;)Ljava/io/File;
    .locals 10
    .param p0    # J
    .param p2    # Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/android/picasastore/PicasaStoreFacade;->getCacheDirectory()Ljava/io/File;

    move-result-object v6

    if-nez v6, :cond_1

    move-object v0, v7

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v8, 0xa

    rem-long v8, p0, v8

    long-to-int v2, v8

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "picasa--"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    :goto_1
    const/4 v8, 0x5

    if-ge v5, v8, :cond_4

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    move-object v0, v7

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v8

    if-eqz v8, :cond_3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_0

    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "e"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_4
    move-object v0, v7

    goto :goto_0
.end method

.method private static getRetryUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 3
    .param p0    # Landroid/net/Uri;
    .param p1    # Ljava/lang/String;

    const-string v1, "content_url"

    invoke-virtual {p0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "content_url"

    invoke-virtual {v1, v2, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method private redirectOpen(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, p2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-static {v1, p3}, Lcom/google/android/picasastore/PicasaStoreFacade;->getRetryUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "w"

    invoke-virtual {p2, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    throw v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, p2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    goto :goto_0
.end method

.method public static setNetworkReceiver(Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    sput-object p0, Lcom/google/android/picasastore/PicasaStoreFacade;->sNetworkReceiver:Ljava/lang/Class;

    return-void
.end method

.method private declared-synchronized updatePicasaSyncInfo(Z)V
    .locals 13
    .param p1    # Z

    const/4 v12, -0x1

    const/4 v7, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v8, Landroid/content/Intent;

    const-string v9, "com.google.android.picasastore.PACKAGE_METADATA_LOOKUP"

    invoke-direct {v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/16 v9, 0x84

    invoke-virtual {v2, v8, v9}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v5, 0x0

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v8, v3, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-boolean v9, v8, Landroid/content/pm/ServiceInfo;->enabled:Z

    if-eqz v9, :cond_1

    iget-object v9, v8, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v9, v9, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-nez v9, :cond_4

    :cond_1
    const-string v9, "gp.PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "ignore disabled picasa sync adapter: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    :goto_1
    if-eqz v6, :cond_0

    if-eqz v5, :cond_2

    iget v8, v5, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->priority:I

    iget v9, v6, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->priority:I

    if-ge v8, v9, :cond_3

    :cond_2
    move-object v5, v6

    :cond_3
    iget-object v8, v6, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    iput-object v6, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    :cond_4
    :try_start_1
    iget-object v9, v8, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-nez v9, :cond_5

    const-string v9, "gp.PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "missing metadata: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    goto :goto_1

    :cond_5
    const-string v10, "com.google.android.picasastore.priority"

    const/4 v11, -0x1

    invoke-virtual {v9, v10, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v10

    const-string v11, "com.google.android.picasastore.authority"

    invoke-virtual {v9, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eq v10, v12, :cond_6

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_7

    :cond_6
    const-string v9, "gp.PicasaStore"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "missing required metadata info: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v7

    goto :goto_1

    :cond_7
    new-instance v6, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v8, v8, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-direct {v6, v8, v9, v10}, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    :cond_8
    iput-object v5, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    invoke-static {v7}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v7, v7, Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;->authority:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    iput-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "content://"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAuthority:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    const-string v8, "photos"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPhotosUri:Landroid/net/Uri;

    const-string v8, "fingerprint"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "force_recalculate"

    const-string v10, "1"

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mRecalculateFingerprintUri:Landroid/net/Uri;

    iget-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "cache_only"

    const-string v10, "1"

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mCachedFingerprintUri:Landroid/net/Uri;

    const-string v8, "albumcovers"

    invoke-static {v7, v8}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mAlbumCoversUri:Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_9
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final getFingerprintUri()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    return-object v0
.end method

.method public final getFingerprintUri(ZZ)Landroid/net/Uri;
    .locals 1
    .param p1    # Z
    .param p2    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mRecalculateFingerprintUri:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mCachedFingerprintUri:Landroid/net/Uri;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mFingerprintUri:Landroid/net/Uri;

    goto :goto_0
.end method

.method final getPicasaStore()Lcom/google/android/picasastore/PicasaStore;
    .locals 3

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPicasaStoreLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPicasaStore:Lcom/google/android/picasastore/PicasaStore;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasastore/PicasaStore;

    iget-object v2, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/picasastore/PicasaStore;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPicasaStore:Lcom/google/android/picasastore/PicasaStore;

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mPicasaStore:Lcom/google/android/picasastore/PicasaStore;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final isMaster()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mMasterInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    iget-object v1, p0, Lcom/google/android/picasastore/PicasaStoreFacade;->mLocalInfo:Lcom/google/android/picasastore/PicasaStoreFacade$PicasaStoreInfo;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPackageAdded$552c4e01()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->updatePicasaSyncInfo(Z)V

    return-void
.end method

.method public final onPackageRemoved$552c4e01()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/picasastore/PicasaStoreFacade;->updatePicasaSyncInfo(Z)V

    return-void
.end method

.method public final openAlbumCover(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    if-nez p3, :cond_2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPicasaStore()Lcom/google/android/picasastore/PicasaStore;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/picasastore/PicasaStore;->openAlbumCover(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-static {p1, p4}, Lcom/google/android/picasastore/PicasaStoreFacade;->getRetryUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v1, "w"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPicasaStore()Lcom/google/android/picasastore/PicasaStore;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/picasastore/PicasaStore;->openAlbumCover(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/picasastore/PicasaStoreFacade;->redirectOpen(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0
.end method

.method public final openFile(Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 2
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # Z
    .param p4    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    if-nez p3, :cond_2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPicasaStore()Lcom/google/android/picasastore/PicasaStore;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/picasastore/PicasaStore;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-static {p1, p4}, Lcom/google/android/picasastore/PicasaStoreFacade;->getRetryUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    const-string v1, "w"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/picasastore/PicasaStoreFacade;->getPicasaStore()Lcom/google/android/picasastore/PicasaStore;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/google/android/picasastore/PicasaStore;->openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/picasastore/PicasaStoreFacade;->redirectOpen(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    goto :goto_0
.end method
