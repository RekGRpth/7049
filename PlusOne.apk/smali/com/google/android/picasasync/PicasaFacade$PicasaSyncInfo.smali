.class final Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;
.super Ljava/lang/Object;
.source "PicasaFacade.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PicasaFacade;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PicasaSyncInfo"
.end annotation


# instance fields
.field public final authority:Ljava/lang/String;

.field public enableDownSync:Z

.field public final packageName:Ljava/lang/String;

.field public final priority:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->packageName:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->authority:Ljava/lang/String;

    iput p3, p0, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->priority:I

    iput-boolean p4, p0, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->enableDownSync:Z

    return-void
.end method
