.class final Lcom/google/android/picasasync/PicasaSyncManager;
.super Ljava/lang/Object;
.source "PicasaSyncManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;,
        Lcom/google/android/picasasync/PicasaSyncManager$GetNextSyncTask;,
        Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/google/android/picasasync/PicasaSyncManager;


# instance fields
.field private mBackgroundData:Z

.field private final mContext:Landroid/content/Context;

.field private volatile mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

.field private final mFacade:Lcom/google/android/picasasync/PicasaFacade;

.field private mHasWifiConnectivity:Z

.field private final mInvalidAccounts:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsPlugged:Z

.field private mIsRoaming:Z

.field private final mProviders:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/picasasync/SyncTaskProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final mSyncHandler:Landroid/os/Handler;

.field private final mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

.field private mSyncRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    iput-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncRequests:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mFacade:Lcom/google/android/picasasync/PicasaFacade;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getInstance(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "picasa-sync-manager"

    const/16 v3, 0xa

    invoke-direct {v1, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lcom/google/android/picasasync/PicasaSyncManager$2;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/picasasync/PicasaSyncManager$2;-><init>(Lcom/google/android/picasasync/PicasaSyncManager;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    new-instance v0, Lcom/google/android/picasasync/PicasaSyncManager$1;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/PicasaSyncManager$1;-><init>(Lcom/google/android/picasasync/PicasaSyncManager;)V

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    return-void
.end method

.method private acceptSyncTask(Lcom/google/android/picasasync/SyncTask;)Z
    .locals 6
    .param p1    # Lcom/google/android/picasasync/SyncTask;

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isBackgroundSync()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because master auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    new-instance v3, Landroid/accounts/Account;

    iget-object v4, p1, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v5, "com.google"

    invoke-direct {v3, v4, v5}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mFacade:Lcom/google/android/picasasync/PicasaFacade;

    invoke-virtual {v4}, Lcom/google/android/picasasync/PicasaFacade;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because auto sync is off"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isBackgroundSync()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for disabled background data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mFacade:Lcom/google/android/picasasync/PicasaFacade;

    invoke-virtual {v3}, Lcom/google/android/picasasync/PicasaFacade;->getMasterInfo()Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/picasasync/PicasaFacade$PicasaSyncInfo;->enableDownSync:Z

    if-nez v3, :cond_3

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " because downsync is disabled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    if-nez v3, :cond_4

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnBattery()Z

    move-result v3

    if-nez v3, :cond_4

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on battery"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    iget-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    if-nez v3, :cond_6

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnWifiOnly()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for non-wifi connection"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    iget-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnRoaming()Z

    move-result v3

    if-nez v3, :cond_6

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for roaming"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mounted"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "mounted_ro"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    move v3, v2

    :goto_1
    if-nez v3, :cond_9

    invoke-virtual {p1}, Lcom/google/android/picasasync/SyncTask;->isSyncOnExternalStorageOnly()Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v2, "gp.PicasaSyncManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "reject "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on external storage"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    move v3, v1

    goto :goto_1

    :cond_9
    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    iget-object v5, p1, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v2, "gp.PicasaSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "reject "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for invalid account: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v3

    throw v1

    :cond_a
    monitor-exit v3

    move v1, v2

    goto/16 :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/picasasync/PicasaSyncManager;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 4
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/picasasync/MetadataSync;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/picasasync/MetadataSync;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/picasasync/MetadataSync;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/picasasync/MetadataSync;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, Lcom/google/android/picasasync/PhotoPrefetch;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x3

    invoke-direct {v1, v2, v3}, Lcom/google/android/picasasync/PhotoPrefetch;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/picasasync/PhotoPrefetch;

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/picasasync/PhotoPrefetch;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic access$200(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 8
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncRequests:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncRequests:Ljava/util/ArrayList;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;

    iget-object v0, v1, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;->account:Ljava/lang/String;

    if-nez v0, :cond_2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PicasaSyncHelper;->getUsers()Ljava/util/ArrayList;

    move-result-object v0

    move-object v2, v0

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/UserEntry;

    iget-object v7, v1, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;->state:Lcom/google/android/picasasync/SyncState;

    iget-object v0, v0, Lcom/google/android/picasasync/UserEntry;->account:Ljava/lang/String;

    invoke-virtual {v7, v4, v0}, Lcom/google/android/picasasync/SyncState;->onSyncRequested(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    or-int/2addr v0, v3

    move v3, v0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    iget-object v0, v1, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;->state:Lcom/google/android/picasasync/SyncState;

    iget-object v1, v1, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;->account:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Lcom/google/android/picasasync/SyncState;->onSyncRequested(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    move-result v0

    or-int/2addr v3, v0

    goto :goto_0

    :cond_3
    if-eqz v3, :cond_4

    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V

    :cond_4
    return-void
.end method

.method static synthetic access$300(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 7
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    const-string v1, "gp.PicasaSyncManager"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "active network: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    iget-boolean v5, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    if-eq v1, v5, :cond_5

    iput-boolean v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mHasWifiConnectivity:Z

    move v1, v2

    :goto_2
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    :cond_0
    iget-boolean v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    if-eq v3, v4, :cond_1

    iput-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsRoaming:Z

    move v1, v2

    :cond_1
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    const-string v3, "gp.PicasaSyncManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "background data: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    if-eq v3, v0, :cond_4

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mBackgroundData:Z

    :goto_3
    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasksInternal()V

    :cond_2
    return-void

    :pswitch_1
    move v1, v3

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic access$400(Lcom/google/android/picasasync/PicasaSyncManager;Ljava/lang/Boolean;)V
    .locals 4
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;
    .param p1    # Ljava/lang/Boolean;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    if-nez p1, :cond_3

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v0, "gp.PicasaSyncManager"

    const-string v1, "there is no battery info yet"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "plugged"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v0, :cond_2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_4

    :cond_2
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    :cond_3
    const-string v0, "gp.PicasaSyncManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "battery info: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mIsPlugged:Z

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasksInternal()V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic access$500(Lcom/google/android/picasasync/PicasaSyncManager;)V
    .locals 0
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    invoke-direct {p0}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasksInternal()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/picasasync/PicasaSyncManager;)Lcom/google/android/picasasync/PicasaFacade;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mFacade:Lcom/google/android/picasasync/PicasaFacade;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/picasasync/PicasaSyncManager;)Lcom/google/android/picasasync/PicasaSyncHelper;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/picasasync/PicasaSyncManager;Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/PicasaSyncManager;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-class v1, Lcom/google/android/picasasync/PicasaSyncManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/picasasync/PicasaSyncManager;->sInstance:Lcom/google/android/picasasync/PicasaSyncManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/picasasync/PicasaSyncManager;

    invoke-direct {v0, p0}, Lcom/google/android/picasasync/PicasaSyncManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/picasasync/PicasaSyncManager;->sInstance:Lcom/google/android/picasasync/PicasaSyncManager;

    :cond_0
    sget-object v0, Lcom/google/android/picasasync/PicasaSyncManager;->sInstance:Lcom/google/android/picasasync/PicasaSyncManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isValidAccount(Landroid/accounts/Account;)Z
    .locals 7
    .param p1    # Landroid/accounts/Account;

    iget-object v5, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v5, "com.google"

    invoke-virtual {v3, v5}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v4, v0, v1

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_1
    return v5

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    iget-object v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_4

    iget-object v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/picasasync/SyncTaskProvider;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v6}, Lcom/google/android/picasasync/SyncTaskProvider;->collectTasks(Ljava/util/Collection;)V

    const/4 v4, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/picasasync/SyncTask;

    iput v0, v5, Lcom/google/android/picasasync/SyncTask;->mPriority:I

    invoke-direct {p0, v5}, Lcom/google/android/picasasync/PicasaSyncManager;->acceptSyncTask(Lcom/google/android/picasasync/SyncTask;)Z

    move-result v7

    if-eqz v7, :cond_0

    if-eqz v4, :cond_1

    iget-object v7, v5, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :cond_1
    move-object v4, v5

    goto :goto_1

    :cond_2
    if-eqz v4, :cond_3

    :goto_2
    return-object v4

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_2
.end method

.method private declared-synchronized requestSync(Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/google/android/picasasync/SyncState;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncRequests:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncRequests:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p2}, Lcom/google/android/picasasync/PicasaSyncManager$SyncRequest;-><init>(Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private updateTasksInternal()V
    .locals 11

    const/4 v10, 0x1

    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    :goto_0
    if-nez v4, :cond_3

    iget-object v6, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/google/android/picasasync/PicasaSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;

    move-result-object v5

    if-nez v5, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    new-instance v0, Landroid/accounts/Account;

    iget-object v6, v5, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    const-string v7, "com.google"

    invoke-direct {v0, v6, v7}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->isValidAccount(Landroid/accounts/Account;)Z

    move-result v6

    if-eqz v6, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v6, "picasa-sync-manager-requested"

    invoke-virtual {v2, v6, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "ignore_settings"

    invoke-virtual {v2, v6, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v6, "gp.PicasaSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "request sync for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mFacade:Lcom/google/android/picasasync/PicasaFacade;

    invoke-virtual {v6}, Lcom/google/android/picasasync/PicasaFacade;->getAuthority()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_2
    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v7, "gp.PicasaSyncManager"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "account: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has been removed ?!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v7

    :try_start_0
    iget-object v8, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v8, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v7

    throw v6

    :cond_3
    iget-object v1, v4, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/picasasync/PicasaSyncManager;->acceptSyncTask(Lcom/google/android/picasasync/SyncTask;)Z

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "gp.PicasaSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "stop task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " due to environment change"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/picasasync/SyncTask;->cancelSync()V

    goto/16 :goto_1

    :cond_4
    iget-object v6, v4, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->account:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/google/android/picasasync/PicasaSyncManager;->nextSyncTaskInternal(Ljava/lang/String;)Lcom/google/android/picasasync/SyncTask;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v6, v3, Lcom/google/android/picasasync/SyncTask;->mPriority:I

    iget v7, v1, Lcom/google/android/picasasync/SyncTask;->mPriority:I

    if-ge v6, v7, :cond_0

    const-string v6, "gp.PicasaSyncManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cancel task: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/picasasync/SyncTask;->cancelSync()V

    goto/16 :goto_1
.end method


# virtual methods
.method public final declared-synchronized onAccountInitialized(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    invoke-virtual {v2, p1}, Lcom/google/android/picasasync/PicasaSyncHelper;->findUser(Ljava/lang/String;)Lcom/google/android/picasasync/UserEntry;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHelper:Lcom/google/android/picasasync/PicasaSyncHelper;

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    new-instance v1, Lcom/google/android/picasasync/UserEntry;

    invoke-direct {v1, p1}, Lcom/google/android/picasasync/UserEntry;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/picasasync/UserEntry;->SCHEMA:Lcom/android/gallery3d/common/EntrySchema;

    invoke-virtual {v2, v0, v1}, Lcom/android/gallery3d/common/EntrySchema;->insertOrReplace(Landroid/database/sqlite/SQLiteDatabase;Lcom/android/gallery3d/common/Entry;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public final onBatteryStateChanged(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v3, 0x5

    if-eqz p1, :cond_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-static {v2, v3, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public final onEnvironmentChanged()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final performSync(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V
    .locals 9
    .param p1    # Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    invoke-static {p1}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    :try_start_0
    iget-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    :cond_0
    const-string v0, "PicasaSyncManager.getNextSyncTask"

    invoke-static {v0}, Lcom/google/android/picasastore/MetricsUtils;->begin(Ljava/lang/String;)I

    move-result v0

    new-instance v2, Ljava/util/concurrent/FutureTask;

    new-instance v3, Lcom/google/android/picasasync/PicasaSyncManager$GetNextSyncTask;

    invoke-direct {v3, p0, p1}, Lcom/google/android/picasasync/PicasaSyncManager$GetNextSyncTask;-><init>(Lcom/google/android/picasasync/PicasaSyncManager;Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V

    invoke-direct {v2, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v2}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_0
    :try_start_2
    invoke-static {v0}, Lcom/google/android/picasastore/MetricsUtils;->end(I)V

    iget-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->account:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    iget-object v2, v2, Lcom/google/android/picasasync/SyncTask;->syncAccount:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    monitor-enter p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-boolean v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mSyncCancelled:Z

    if-nez v0, :cond_2

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/picasasync/PicasaSyncManager;->updateTasks(J)V

    :cond_2
    monitor-exit p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    iput-object v6, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    return-void

    :catch_0
    move-exception v2

    :try_start_4
    const-string v3, "gp.PicasaSyncManager"

    const-string v4, "fail to get next task"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v6, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mCurrentSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    throw v0

    :cond_3
    :try_start_5
    const-string v0, "gp.PicasaSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "perform sync task: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    iget-object v2, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v0, v2}, Lcom/google/android/picasasync/SyncTask;->performSync(Landroid/content/SyncResult;)V

    iget-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v0}, Landroid/content/SyncResult;->hasError()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "gp.PicasaSyncManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sync complete w error:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->result:Landroid/content/SyncResult;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_4
    const/4 v0, 0x0

    :try_start_6
    iput-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    :goto_2
    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    cmp-long v0, v2, v7

    if-lez v0, :cond_0

    const-string v0, "gp.PicasaSyncManager"

    const-string v1, "stop sync session due to io error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->cancelSync()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_7
    const-string v2, "gp.PicasaSyncManager"

    const-string v3, "perform sync fail"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/4 v0, 0x0

    :try_start_8
    iput-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_2

    :catch_2
    move-exception v0

    :try_start_9
    const-string v2, "gp.PicasaSyncManager"

    const-string v3, "perform sync fail"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    const/4 v0, 0x0

    :try_start_a
    iput-object v0, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    goto :goto_2

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p1, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->mCurrentTask:Lcom/google/android/picasasync/SyncTask;

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit p1

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method public final requestAccountSync()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public final requestMetadataSync(Z)V
    .locals 2
    .param p1    # Z

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/picasasync/SyncState;->METADATA_MANUAL:Lcom/google/android/picasasync/SyncState;

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->requestSync(Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/picasasync/SyncState;->METADATA:Lcom/google/android/picasasync/SyncState;

    goto :goto_0
.end method

.method public final requestPrefetchSync()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/picasasync/PhotoPrefetch;->onRequestSync(Landroid/content/Context;)V

    sget-object v0, Lcom/google/android/picasasync/SyncState;->PREFETCH_FULL_IMAGE:Lcom/google/android/picasasync/SyncState;

    invoke-direct {p0, v1, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->requestSync(Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    sget-object v0, Lcom/google/android/picasasync/SyncState;->PREFETCH_SCREEN_NAIL:Lcom/google/android/picasasync/SyncState;

    invoke-direct {p0, v1, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->requestSync(Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    sget-object v0, Lcom/google/android/picasasync/SyncState;->PREFETCH_ALBUM_COVER:Lcom/google/android/picasasync/SyncState;

    invoke-direct {p0, v1, v0}, Lcom/google/android/picasasync/PicasaSyncManager;->requestSync(Ljava/lang/String;Lcom/google/android/picasasync/SyncState;)V

    return-void
.end method

.method public final resetSyncStates()V
    .locals 4

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    monitor-enter v3

    :try_start_0
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mInvalidAccounts:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    monitor-enter p0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mProviders:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/picasasync/SyncTaskProvider;

    invoke-interface {v1}, Lcom/google/android/picasasync/SyncTaskProvider;->resetSyncStates()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2

    :cond_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method

.method public final updateTasks(J)V
    .locals 2
    .param p1    # J

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncManager;->mSyncHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method
