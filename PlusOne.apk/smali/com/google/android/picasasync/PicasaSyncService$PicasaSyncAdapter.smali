.class final Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "PicasaSyncService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PicasaSyncService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PicasaSyncAdapter"
.end annotation


# instance fields
.field private mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    return-void
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 8
    .param p1    # Landroid/accounts/Account;
    .param p2    # Landroid/os/Bundle;
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/ContentProviderClient;
    .param p5    # Landroid/content/SyncResult;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/PicasaSyncManager;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaSyncManager;

    move-result-object v2

    const-string v3, "initialize"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "gp.PicasaSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "initialize account: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/picasasync/PicasaFacade;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaFacade;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/picasasync/PicasaFacade;->isMaster()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "com.google"

    iget-object v4, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    invoke-static {p1, p3, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    # invokes: Lcom/google/android/picasasync/PicasaSyncService;->carryOverSyncAutomatically$5e8deb68(Landroid/accounts/Account;Ljava/lang/String;)V
    invoke-static {v3, p1, p3}, Lcom/google/android/picasasync/PicasaSyncService;->access$000(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;)V

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/picasasync/PicasaSyncManager;->onAccountInitialized(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v3, 0x0

    invoke-static {p1, p3, v3}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "gp.PicasaSyncService"

    const-string v4, "cannot do sync"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    monitor-enter p0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "gp.PicasaSyncService"

    const-string v4, "sync is cancelled"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    :cond_2
    :try_start_2
    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v4, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-direct {v4, v3, p5}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;-><init>(Ljava/lang/String;Landroid/content/SyncResult;)V

    iput-object v4, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    const-string v3, "upload"

    invoke-virtual {p2, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "picasa-sync-manager-requested"

    invoke-virtual {p2, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/picasasync/PicasaSyncManager;->resetSyncStates()V

    sget-object v3, Lcom/google/android/picasasync/SyncState;->METADATA:Lcom/google/android/picasasync/SyncState;

    invoke-virtual {p0}, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->get(Landroid/content/Context;)Lcom/google/android/picasasync/PicasaDatabaseHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/picasasync/PicasaDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/picasasync/SyncState;->onSyncRequested(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)Z

    :cond_3
    const-string v3, "gp.PicasaSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "start sync on "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/gallery3d/common/Utils;->maskDebugInfo(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_3
    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-virtual {v2, v3}, Lcom/google/android/picasasync/PicasaSyncManager;->performSync(Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-virtual {v3}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->isSyncCancelled()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "gp.PicasaSyncService"

    const-string v4, "sync cancelled"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_4
    const-string v3, "gp.PicasaSyncService"

    const-string v4, "sync finished"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_1
    move-exception v0

    :try_start_4
    const-string v3, "gp.PicasaSyncService"

    const-string v4, "performSync error"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v3, p5, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    iget-object v3, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-virtual {v3}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->isSyncCancelled()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "gp.PicasaSyncService"

    const-string v4, "sync cancelled"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    const-string v3, "gp.PicasaSyncService"

    const-string v4, "sync finished"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_1
    move-exception v3

    iget-object v4, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-virtual {v4}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->isSyncCancelled()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "gp.PicasaSyncService"

    const-string v5, "sync cancelled"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    throw v3

    :cond_6
    const-string v4, "gp.PicasaSyncService"

    const-string v5, "sync finished"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final declared-synchronized onSyncCanceled()V
    .locals 2

    monitor-enter p0

    :try_start_0
    const-string v0, "gp.PicasaSyncService"

    const-string v1, "receive cancel request"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0}, Landroid/content/AbstractThreadedSyncAdapter;->onSyncCanceled()V

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/PicasaSyncService$PicasaSyncAdapter;->mSession:Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;

    invoke-virtual {v0}, Lcom/google/android/picasasync/PicasaSyncManager$SyncSession;->cancelSync()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
