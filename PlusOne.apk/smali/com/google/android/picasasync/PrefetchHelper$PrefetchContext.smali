.class public final Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;
.super Ljava/lang/Object;
.source "PrefetchHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/PrefetchHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "PrefetchContext"
.end annotation


# instance fields
.field private mCacheListener:Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;

.field private mDownloadFailCount:I

.field private mLastVersion:I

.field private volatile mStopSync:Z

.field private mThread:Ljava/lang/Thread;

.field public result:Landroid/content/SyncResult;

.field final synthetic this$0:Lcom/google/android/picasasync/PrefetchHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/picasasync/PrefetchHelper;Landroid/content/SyncResult;Ljava/lang/Thread;)V
    .locals 1
    .param p2    # Landroid/content/SyncResult;
    .param p3    # Ljava/lang/Thread;

    iput-object p1, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->this$0:Lcom/google/android/picasasync/PrefetchHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SyncResult;

    iput-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->result:Landroid/content/SyncResult;

    iput-object p3, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mThread:Ljava/lang/Thread;

    return-void
.end method


# virtual methods
.method public final checkCacheConfigVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mLastVersion:I

    iget-object v1, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->this$0:Lcom/google/android/picasasync/PrefetchHelper;

    # getter for: Lcom/google/android/picasasync/PrefetchHelper;->mCacheConfigVersion:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v1}, Lcom/google/android/picasasync/PrefetchHelper;->access$000(Lcom/google/android/picasasync/PrefetchHelper;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getDownloadFailCount()I
    .locals 1

    iget v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mDownloadFailCount:I

    return v0
.end method

.method public final onDownloadFinish(JZ)V
    .locals 1
    .param p1    # J
    .param p3    # Z

    if-eqz p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mDownloadFailCount:I

    iget-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mCacheListener:Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mCacheListener:Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;

    invoke-interface {v0}, Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;->onDownloadFinish$256a6c5()V

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mDownloadFailCount:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final setCacheDownloadListener(Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;)V
    .locals 0
    .param p1    # Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;

    iput-object p1, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mCacheListener:Lcom/google/android/picasasync/PrefetchHelper$PrefetchListener;

    return-void
.end method

.method public final stopSync()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mStopSync:Z

    iget-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method

.method public final syncInterrupted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mStopSync:Z

    return v0
.end method

.method public final updateCacheConfigVersion()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->this$0:Lcom/google/android/picasasync/PrefetchHelper;

    # getter for: Lcom/google/android/picasasync/PrefetchHelper;->mCacheConfigVersion:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/google/android/picasasync/PrefetchHelper;->access$000(Lcom/google/android/picasasync/PrefetchHelper;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    iput v0, p0, Lcom/google/android/picasasync/PrefetchHelper$PrefetchContext;->mLastVersion:I

    return-void
.end method
