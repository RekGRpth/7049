.class public final Lcom/google/android/picasasync/SyncLockManager$SyncLock;
.super Ljava/lang/Object;
.source "SyncLockManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/picasasync/SyncLockManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SyncLock"
.end annotation


# instance fields
.field private mKey:Ljava/lang/Object;

.field private mType:I

.field final synthetic this$0:Lcom/google/android/picasasync/SyncLockManager;


# direct methods
.method private constructor <init>(Lcom/google/android/picasasync/SyncLockManager;ILjava/lang/Object;)V
    .locals 0
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->this$0:Lcom/google/android/picasasync/SyncLockManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mType:I

    iput-object p3, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mKey:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/picasasync/SyncLockManager;ILjava/lang/Object;B)V
    .locals 0
    .param p1    # Lcom/google/android/picasasync/SyncLockManager;
    .param p2    # I
    .param p3    # Ljava/lang/Object;

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/picasasync/SyncLockManager$SyncLock;-><init>(Lcom/google/android/picasasync/SyncLockManager;ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1    # Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/google/android/picasasync/SyncLockManager$SyncLock;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    check-cast v0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;

    iget v2, v0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mType:I

    iget v3, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mType:I

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mKey:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mKey:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    iget v0, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mType:I

    iget-object v1, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->mKey:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public final unlock()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->this$0:Lcom/google/android/picasasync/SyncLockManager;

    # getter for: Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/picasasync/SyncLockManager;->access$100(Lcom/google/android/picasasync/SyncLockManager;)Ljava/util/HashSet;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->this$0:Lcom/google/android/picasasync/SyncLockManager;

    # getter for: Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/picasasync/SyncLockManager;->access$100(Lcom/google/android/picasasync/SyncLockManager;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/gallery3d/common/Utils;->assertTrue(Z)V

    iget-object v0, p0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;->this$0:Lcom/google/android/picasasync/SyncLockManager;

    # getter for: Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/google/android/picasasync/SyncLockManager;->access$100(Lcom/google/android/picasasync/SyncLockManager;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
