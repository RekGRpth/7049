.class final Lcom/google/android/picasasync/SyncLockManager;
.super Ljava/lang/Object;
.source "SyncLockManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/picasasync/SyncLockManager$SyncLock;
    }
.end annotation


# instance fields
.field private final mLocks:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/picasasync/SyncLockManager$SyncLock;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/picasasync/SyncLockManager;)Ljava/util/HashSet;
    .locals 1
    .param p0    # Lcom/google/android/picasasync/SyncLockManager;

    iget-object v0, p0, Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;

    return-object v0
.end method


# virtual methods
.method public final acquireLock(ILjava/lang/Object;)Lcom/google/android/picasasync/SyncLockManager$SyncLock;
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_0
    new-instance v0, Lcom/google/android/picasasync/SyncLockManager$SyncLock;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/picasasync/SyncLockManager$SyncLock;-><init>(Lcom/google/android/picasasync/SyncLockManager;ILjava/lang/Object;B)V

    :goto_0
    iget-object v1, p0, Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/picasasync/SyncLockManager;->mLocks:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    :cond_0
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
