.class public final Lcom/google/api/services/plusi/model/AllPhotosViewResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "AllPhotosViewResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public resumeToken:Ljava/lang/String;

.field public success:Ljava/lang/Boolean;

.field public tile:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Tile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
