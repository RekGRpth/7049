.class public final Lcom/google/api/services/plusi/model/ApiaryFieldsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ApiaryFieldsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ApiaryFields;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ApiaryFieldsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;->INSTANCE:Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ApiaryFields;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "appVersion"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "effectiveUser"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "noLog"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "sourceInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ApiaryFieldsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;->INSTANCE:Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ApiaryFields;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ApiaryFields;->appVersion:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ApiaryFields;->effectiveUser:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ApiaryFields;->noLog:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ApiaryFields;->sourceInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ApiaryFields;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ApiaryFields;-><init>()V

    return-object v0
.end method
