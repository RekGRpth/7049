.class public final Lcom/google/api/services/plusi/model/CenterFocusFilterParams;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "CenterFocusFilterParams.java"


# instance fields
.field public blurStyle:Ljava/lang/Float;

.field public center:Lcom/google/api/services/plusi/model/VectorParamsVector2;

.field public centerBrightness:Ljava/lang/Float;

.field public filterStrength:Ljava/lang/Float;

.field public reverseVignette:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
