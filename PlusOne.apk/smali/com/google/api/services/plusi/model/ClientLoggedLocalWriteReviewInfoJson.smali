.class public final Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ClientLoggedLocalWriteReviewInfoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "alsoShared"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "clusterId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "newReview"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "source"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "textCommentLength"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;->INSTANCE:Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;->alsoShared:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;->clusterId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;->newReview:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;->source:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;->textCommentLength:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ClientLoggedLocalWriteReviewInfo;-><init>()V

    return-object v0
.end method
