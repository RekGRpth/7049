.class public final Lcom/google/api/services/plusi/model/ClientLoggedSuggestionInfo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ClientLoggedSuggestionInfo.java"


# instance fields
.field public celebrityCategoryId:Ljava/lang/Integer;

.field public deprecatedFriendSuggestionSummarizedInfoBitmask:Ljava/math/BigInteger;

.field public explanationType:Ljava/lang/Integer;

.field public explanationsTypesBitmask:Ljava/lang/Integer;

.field public friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

.field public friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

.field public numberOfCircleMembersAdded:Ljava/lang/Integer;

.field public numberOfCircleMembersRemoved:Ljava/lang/Integer;

.field public placement:Ljava/lang/Integer;

.field public score:Ljava/lang/Double;

.field public scoringExperiments:Ljava/lang/String;

.field public seed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public suggestedCircle:Lcom/google/api/services/plusi/model/ClientLoggedCircle;

.field public suggestedCircleMember:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ClientLoggedCircleMember;",
            ">;"
        }
    .end annotation
.end field

.field public suggestionId:Ljava/lang/String;

.field public suggestionType:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
