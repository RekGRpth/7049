.class public final Lcom/google/api/services/plusi/model/CommonSegmentJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "CommonSegmentJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/CommonSegment;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/CommonSegmentJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/CommonSegmentJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CommonSegmentJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/CommonSegmentJson;->INSTANCE:Lcom/google/api/services/plusi/model/CommonSegmentJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/CommonSegment;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/CommonFormattingJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "formatting"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/HashtagDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "hashtagData"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/LinkDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "linkData"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "text"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/UserMentionDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "userMentionData"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/CommonSegmentJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/CommonSegmentJson;->INSTANCE:Lcom/google/api/services/plusi/model/CommonSegmentJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/CommonSegment;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CommonSegment;->formatting:Lcom/google/api/services/plusi/model/CommonFormatting;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CommonSegment;->hashtagData:Lcom/google/api/services/plusi/model/HashtagData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CommonSegment;->linkData:Lcom/google/api/services/plusi/model/LinkData;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CommonSegment;->text:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CommonSegment;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/CommonSegment;->userMentionData:Lcom/google/api/services/plusi/model/UserMentionData;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/CommonSegment;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/CommonSegment;-><init>()V

    return-object v0
.end method
