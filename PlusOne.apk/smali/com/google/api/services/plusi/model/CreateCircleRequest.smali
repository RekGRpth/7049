.class public final Lcom/google/api/services/plusi/model/CreateCircleRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "CreateCircleRequest.java"


# instance fields
.field public circleMembershipModificationParams:Lcom/google/api/services/plusi/model/DataCircleMembershipModificationParams;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public description:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public justFollowingStatus:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
