.class public final Lcom/google/api/services/plusi/model/DataDiscoverySettingsDiscoveryEmail;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataDiscoverySettingsDiscoveryEmail.java"


# instance fields
.field public alwaysDiscoverable:Ljava/lang/Boolean;

.field public discoverable:Ljava/lang/Boolean;

.field public removable:Ljava/lang/Boolean;

.field public source:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public value:Ljava/lang/String;

.field public verified:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
