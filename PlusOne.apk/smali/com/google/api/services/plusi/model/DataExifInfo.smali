.class public final Lcom/google/api/services/plusi/model/DataExifInfo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataExifInfo.java"


# instance fields
.field public apertureFnumber:Ljava/lang/Float;

.field public cameraMake:Ljava/lang/String;

.field public cameraModel:Ljava/lang/String;

.field public distance:Ljava/lang/Float;

.field public exifTimeSec:Ljava/lang/Long;

.field public exposureBias:Ljava/lang/Float;

.field public exposureTimeSec:Ljava/lang/Float;

.field public flashUsed:Ljava/lang/Boolean;

.field public focalLengthIn35mmFilm:Ljava/lang/Integer;

.field public focalLengthMm:Ljava/lang/Float;

.field public isoEquivalent:Ljava/lang/Integer;

.field public orientation:Ljava/lang/Integer;

.field public originHash:Ljava/lang/String;

.field public photoTimeSec:Ljava/lang/Long;

.field public subjectDistance:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
