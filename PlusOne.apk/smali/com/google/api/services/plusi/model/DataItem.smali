.class public final Lcom/google/api/services/plusi/model/DataItem;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataItem.java"


# instance fields
.field public actor:Lcom/google/api/services/plusi/model/DataActor;

.field public actorOid:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public isRead:Ljava/lang/Boolean;

.field public notificationType:Ljava/lang/String;

.field public opaqueClientFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataKvPair;",
            ">;"
        }
    .end annotation
.end field

.field public timestamp:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
