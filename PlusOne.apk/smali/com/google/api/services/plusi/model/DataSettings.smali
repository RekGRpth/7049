.class public final Lcom/google/api/services/plusi/model/DataSettings;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSettings.java"


# instance fields
.field public acknowledgement:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataAcknowledgement;",
            ">;"
        }
    .end annotation
.end field

.field public defaultGeoVisibility:Ljava/lang/Boolean;

.field public enableDownloadPhoto:Ljava/lang/Boolean;

.field public facialRecognitionEnabled:Ljava/lang/Boolean;

.field public nameSuggestionLightboxPromoEnabled:Ljava/lang/Boolean;

.field public originalResolutionUploadsEnabled:Ljava/lang/Boolean;

.field public showTab:Ljava/lang/Boolean;

.field public storageQuota:Lcom/google/api/services/plusi/model/DataStorageQuota;

.field public tagAcl:Ljava/lang/String;

.field public viewAcl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
