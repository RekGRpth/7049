.class public final Lcom/google/api/services/plusi/model/DataSettingsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DataSettingsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DataSettings;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DataSettingsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataSettingsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataSettingsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DataSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataSettingsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DataSettings;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataAcknowledgementJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "acknowledgement"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "defaultGeoVisibility"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "enableDownloadPhoto"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "facialRecognitionEnabled"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "nameSuggestionLightboxPromoEnabled"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "originalResolutionUploadsEnabled"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "showTab"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/DataStorageQuotaJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "storageQuota"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "tagAcl"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "viewAcl"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DataSettingsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DataSettingsJson;->INSTANCE:Lcom/google/api/services/plusi/model/DataSettingsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DataSettings;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->acknowledgement:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->defaultGeoVisibility:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->enableDownloadPhoto:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->facialRecognitionEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->nameSuggestionLightboxPromoEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->originalResolutionUploadsEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->showTab:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->storageQuota:Lcom/google/api/services/plusi/model/DataStorageQuota;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->tagAcl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DataSettings;->viewAcl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DataSettings;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DataSettings;-><init>()V

    return-object v0
.end method
