.class public final Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataSuggestedCelebrityCategory.java"


# instance fields
.field public category:Ljava/lang/String;

.field public categoryName:Ljava/lang/String;

.field public celebrity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field

.field public totalCelebrityCount:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
