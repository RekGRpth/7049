.class public final Lcom/google/api/services/plusi/model/DataVideo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "DataVideo.java"


# instance fields
.field public downloadUrl:Ljava/lang/String;

.field public durationMillis:Ljava/lang/Long;

.field public id:Ljava/lang/String;

.field public status:Ljava/lang/String;

.field public stream:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataVideoStream;",
            ">;"
        }
    .end annotation
.end field

.field public timedText:Lcom/google/api/services/plusi/model/DataTimedTextMetaData;

.field public validThumbnail:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
