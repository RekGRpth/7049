.class public final Lcom/google/api/services/plusi/model/DeviceLocationAclJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DeviceLocationAclJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DeviceLocationAcl;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DeviceLocationAclJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DeviceLocationAclJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DeviceLocationAclJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DeviceLocationAclJson;->INSTANCE:Lcom/google/api/services/plusi/model/DeviceLocationAclJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DeviceLocationAcl;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/SharingRosterJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "lastSavedSharingRoster"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "renderedSharingRoster"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "sharingEnabled"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/SharingRosterJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "sharingRoster"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "type"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DeviceLocationAclJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DeviceLocationAclJson;->INSTANCE:Lcom/google/api/services/plusi/model/DeviceLocationAclJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->lastSavedSharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->renderedSharingRoster:Lcom/google/api/services/plusi/model/RenderedSharingRosters;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->sharingEnabled:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->sharingRoster:Lcom/google/api/services/plusi/model/SharingRoster;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocationAcl;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DeviceLocationAcl;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DeviceLocationAcl;-><init>()V

    return-object v0
.end method
