.class public final Lcom/google/api/services/plusi/model/DeviceLocationJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "DeviceLocationJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/DeviceLocation;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/DeviceLocationJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DeviceLocationJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DeviceLocationJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/DeviceLocationJson;->INSTANCE:Lcom/google/api/services/plusi/model/DeviceLocationJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/DeviceLocation;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "accuracyMeters"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/api/services/plusi/model/DeviceLocationJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "altitudeMeters"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "displayName"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "lat"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "lng"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "mapImageUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "mapImageUrlFullBleed"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "mapImageUrlMobile"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/api/services/plusi/model/DeviceLocationJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "timestampUsec"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "type"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/DeviceLocationJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/DeviceLocationJson;->INSTANCE:Lcom/google/api/services/plusi/model/DeviceLocationJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/DeviceLocation;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->accuracyMeters:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->altitudeMeters:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->displayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->lat:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->lng:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->mapImageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->mapImageUrlFullBleed:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->mapImageUrlMobile:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->timestampUsec:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/DeviceLocation;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/DeviceLocation;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/DeviceLocation;-><init>()V

    return-object v0
.end method
