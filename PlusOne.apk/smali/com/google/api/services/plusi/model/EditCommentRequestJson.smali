.class public final Lcom/google/api/services/plusi/model/EditCommentRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EditCommentRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EditCommentRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EditCommentRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EditCommentRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EditCommentRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EditCommentRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/EditCommentRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/EditCommentRequest;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "commentId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/EditSegmentsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "commentSegments"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "commentText"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "contentFormat"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "renderContextLocation"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EditCommentRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/EditCommentRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/EditCommentRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/EditCommentRequest;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->activityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentSegments:Lcom/google/api/services/plusi/model/EditSegments;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commentText:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->contentFormat:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EditCommentRequest;->renderContextLocation:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EditCommentRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EditCommentRequest;-><init>()V

    return-object v0
.end method
