.class public final Lcom/google/api/services/plusi/model/EmbedsSquareJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EmbedsSquareJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EmbedsSquare;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EmbedsSquareJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EmbedsSquareJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedsSquareJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EmbedsSquareJson;->INSTANCE:Lcom/google/api/services/plusi/model/EmbedsSquareJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/EmbedsSquare;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "communityId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "deprecated4"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "imageUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/ThumbnailJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "proxiedImage"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "relativeUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "url"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EmbedsSquareJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/EmbedsSquareJson;->INSTANCE:Lcom/google/api/services/plusi/model/EmbedsSquareJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/EmbedsSquare;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->communityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->deprecated4:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->imageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->relativeUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EmbedsSquare;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EmbedsSquare;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EmbedsSquare;-><init>()V

    return-object v0
.end method
