.class public final Lcom/google/api/services/plusi/model/EntitySquaresDataJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EntitySquaresDataJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EntitySquaresData;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EntitySquaresDataJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/EntitySquaresData;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareInviteJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "invite"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipApprovedJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "membershipApproved"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareMembershipRequestJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "membershipRequest"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataNewModeratorJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "newModerator"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "renderSquaresData"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareNameChangeJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "squareNameChange"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/EntitySquaresDataSquareSubscriptionJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "subscription"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EntitySquaresDataJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/EntitySquaresDataJson;->INSTANCE:Lcom/google/api/services/plusi/model/EntitySquaresDataJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/EntitySquaresData;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->invite:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipApproved:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->membershipRequest:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->newModerator:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->renderSquaresData:Lcom/google/api/services/plusi/model/EntitySquaresDataRenderSquaresData;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->squareNameChange:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EntitySquaresData;->subscription:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EntitySquaresData;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EntitySquaresData;-><init>()V

    return-object v0
.end method
