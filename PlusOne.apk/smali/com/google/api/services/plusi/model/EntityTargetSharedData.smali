.class public final Lcom/google/api/services/plusi/model/EntityTargetSharedData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "EntityTargetSharedData.java"


# instance fields
.field public actorOid:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public post:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EntityTargetSharedDataPost;",
            ">;"
        }
    .end annotation
.end field

.field public targetId:Ljava/lang/String;

.field public titleSanitizedHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
