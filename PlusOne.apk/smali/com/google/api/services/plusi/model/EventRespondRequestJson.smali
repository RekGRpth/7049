.class public final Lcom/google/api/services/plusi/model/EventRespondRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "EventRespondRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/EventRespondRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/EventRespondRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EventRespondRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventRespondRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/EventRespondRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/EventRespondRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/EventRespondRequest;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "deprecated1"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "eventId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/EventSelectorJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "eventSelector"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "invitationToken"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "location"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "numAdditionalGuests"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "offNetworkDisplayName"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "response"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "rsvpToken"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "squareId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/EventRespondRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/EventRespondRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/EventRespondRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/EventRespondRequest;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->deprecated1:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->eventId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->eventSelector:Lcom/google/api/services/plusi/model/EventSelector;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->invitationToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->location:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->numAdditionalGuests:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->offNetworkDisplayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->response:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->rsvpToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/EventRespondRequest;->squareId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/EventRespondRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/EventRespondRequest;-><init>()V

    return-object v0
.end method
