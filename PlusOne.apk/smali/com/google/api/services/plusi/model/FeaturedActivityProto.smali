.class public final Lcom/google/api/services/plusi/model/FeaturedActivityProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "FeaturedActivityProto.java"


# instance fields
.field public activity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProto;",
            ">;"
        }
    .end annotation
.end field

.field public checkedInUser:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AuthorProto;",
            ">;"
        }
    .end annotation
.end field

.field public reviewTemplate:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GoogleReviewProto;",
            ">;"
        }
    .end annotation
.end field

.field public reviewedUser:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AuthorProto;",
            ">;"
        }
    .end annotation
.end field

.field public totalCheckins:Ljava/lang/Integer;

.field public totalMedia:Ljava/lang/Integer;

.field public totalReviews:Ljava/lang/Integer;

.field public wishlistedUser:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/AuthorProto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
