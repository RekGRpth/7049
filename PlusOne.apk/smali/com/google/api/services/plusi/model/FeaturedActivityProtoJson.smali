.class public final Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "FeaturedActivityProtoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/FeaturedActivityProto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/PlaceActivityStreamEntryProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "activity"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/AuthorProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "checkedInUser"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/GoogleReviewProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "reviewTemplate"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/AuthorProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "reviewedUser"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "totalCheckins"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "totalMedia"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "totalReviews"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/AuthorProtoJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "wishlistedUser"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/FeaturedActivityProtoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->activity:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->checkedInUser:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->reviewTemplate:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->reviewedUser:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->totalCheckins:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->totalMedia:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->totalReviews:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/FeaturedActivityProto;->wishlistedUser:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/FeaturedActivityProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FeaturedActivityProto;-><init>()V

    return-object v0
.end method
