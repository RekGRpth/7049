.class public final Lcom/google/api/services/plusi/model/FinancialQuote;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "FinancialQuote.java"


# instance fields
.field public afterHoursPrice:Ljava/lang/String;

.field public afterHoursPriceChange:Ljava/lang/String;

.field public afterHoursPriceChangePercent:Ljava/lang/String;

.field public afterHoursQuoteTime:Ljava/lang/String;

.field public dataSource:Ljava/lang/String;

.field public dataSourceDisclaimerUrl:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public exchange:Ljava/lang/String;

.field public exchangeTimezone:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public isAfterHours:Ljava/lang/Boolean;

.field public isPreMarket:Ljava/lang/Boolean;

.field public localizedAfterHoursQuoteTimestamp:Ljava/lang/String;

.field public localizedQuoteTimestamp:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public priceChange:Ljava/lang/String;

.field public priceChangeNumber:Ljava/lang/Float;

.field public priceChangePercent:Ljava/lang/String;

.field public priceCurrency:Ljava/lang/String;

.field public quoteTime:Ljava/lang/String;

.field public tickerSymbol:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
