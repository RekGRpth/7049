.class public final Lcom/google/api/services/plusi/model/FrameJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "FrameJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/Frame;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/FrameJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/FrameJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/FrameJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/FrameJson;->INSTANCE:Lcom/google/api/services/plusi/model/FrameJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/Frame;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "domain"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "embed"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/FrameEmbedDuplicateFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "embedDupes"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "iconUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "isPrivate"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/SourceJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "source"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/api/services/plusi/model/FrameJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "timestampMsec"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/VerbJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "verb"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "visibility"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/FrameJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/FrameJson;->INSTANCE:Lcom/google/api/services/plusi/model/FrameJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/Frame;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->domain:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->embed:Lcom/google/api/services/plusi/model/EmbedClientItem;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->embedDupes:Lcom/google/api/services/plusi/model/FrameEmbedDuplicateFields;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->iconUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->isPrivate:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->source:Lcom/google/api/services/plusi/model/Source;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->timestampMsec:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->verb:Lcom/google/api/services/plusi/model/Verb;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Frame;->visibility:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/Frame;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Frame;-><init>()V

    return-object v0
.end method
