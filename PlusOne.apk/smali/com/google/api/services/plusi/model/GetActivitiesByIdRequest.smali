.class public final Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetActivitiesByIdRequest.java"


# instance fields
.field public activityId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public activityMaxResharers:Ljava/lang/Long;

.field public collapseActivities:Ljava/lang/Boolean;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public commonParams:Lcom/google/api/services/plusi/model/GetActivitiesCommonParams;

.field public contentFormat:Ljava/lang/String;

.field public embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public renderContext:Lcom/google/api/services/plusi/model/RenderContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
