.class public final Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GetActivitiesByIdRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "activityMaxResharers"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "collapseActivities"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/GetActivitiesCommonParamsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "commonParams"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "contentFormat"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/ClientEmbedOptionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "embedOptions"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/RenderContextJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "renderContext"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetActivitiesByIdRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->activityId:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->activityMaxResharers:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->collapseActivities:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->commonParams:Lcom/google/api/services/plusi/model/GetActivitiesCommonParams;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->contentFormat:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;->renderContext:Lcom/google/api/services/plusi/model/RenderContext;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetActivitiesByIdRequest;-><init>()V

    return-object v0
.end method
