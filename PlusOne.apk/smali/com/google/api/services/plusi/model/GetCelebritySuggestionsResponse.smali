.class public final Lcom/google/api/services/plusi/model/GetCelebritySuggestionsResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetCelebritySuggestionsResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public category:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedCelebrityCategory;",
            ">;"
        }
    .end annotation
.end field

.field public experimentNames:Ljava/lang/String;

.field public fbsVersionInfo:Ljava/lang/String;

.field public personalized:Ljava/lang/Boolean;

.field public previewCeleb:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
