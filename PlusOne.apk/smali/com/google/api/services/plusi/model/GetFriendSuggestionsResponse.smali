.class public final Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetFriendSuggestionsResponse.java"


# instance fields
.field public experimentNames:Ljava/lang/String;

.field public experimentThreshold:Ljava/lang/Double;

.field public imported:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataImportedPerson;",
            ">;"
        }
    .end annotation
.end field

.field public portraitVersion:Ljava/lang/String;

.field public promotedSuggestion:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field

.field public queryId:Ljava/lang/String;

.field public suggestion:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataSuggestedPerson;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
