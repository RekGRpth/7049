.class public final Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GetFriendSuggestionsResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "experimentNames"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "experimentThreshold"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataImportedPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "imported"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "portraitVersion"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/DataSuggestedPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "promotedSuggestion"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "queryId"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/DataSuggestedPersonJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "suggestion"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->experimentNames:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->experimentThreshold:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->imported:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->portraitVersion:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->promotedSuggestion:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->queryId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;->suggestion:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;-><init>()V

    return-object v0
.end method
