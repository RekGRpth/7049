.class public final Lcom/google/api/services/plusi/model/GetMobileExperimentsResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GetMobileExperimentsResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public experiment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/GetMobileExperimentsResponseExperiment;",
            ">;"
        }
    .end annotation
.end field

.field public fbsVersionInfo:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
