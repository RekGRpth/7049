.class public final Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GetMobileSettingsResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/MobileSettingsApplicationJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "application"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/MobilePreferenceJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "preference"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "requestError"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "shareboxSettings"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "speedRacerDefaultSetting"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/MobileSettingsUserJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "user"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/GetMobileSettingsResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->application:Lcom/google/api/services/plusi/model/MobileSettingsApplication;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->preference:Lcom/google/api/services/plusi/model/MobilePreference;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->requestError:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->speedRacerDefaultSetting:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;->user:Lcom/google/api/services/plusi/model/MobileSettingsUser;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GetMobileSettingsResponse;-><init>()V

    return-object v0
.end method
