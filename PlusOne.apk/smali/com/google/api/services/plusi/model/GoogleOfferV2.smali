.class public final Lcom/google/api/services/plusi/model/GoogleOfferV2;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "GoogleOfferV2.java"


# instance fields
.field public about:Lcom/google/api/services/plusi/model/EmbedClientItem;

.field public author:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedClientItem;",
            ">;"
        }
    .end annotation
.end field

.field public availabilityEnds:Ljava/lang/String;

.field public availabilityStarts:Ljava/lang/String;

.field public availableAtOrFrom:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedClientItem;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public descriptionTruncated:Ljava/lang/String;

.field public faviconUrl:Ljava/lang/String;

.field public finePrint:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public localizedValidThrough:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public offerState:Ljava/lang/String;

.field public offerType:Ljava/lang/String;

.field public ownerObfuscatedId:Ljava/lang/String;

.field public previewUrl:Ljava/lang/String;

.field public price:Ljava/lang/String;

.field public proxiedFaviconUrl:Ljava/lang/String;

.field public proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

.field public saved:Ljava/lang/Boolean;

.field public seller:Lcom/google/api/services/plusi/model/EmbedClientItem;

.field public shortTitle:Ljava/lang/String;

.field public totalSaves:Ljava/lang/Integer;

.field public url:Ljava/lang/String;

.field public validFrom:Ljava/lang/String;

.field public validThrough:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
