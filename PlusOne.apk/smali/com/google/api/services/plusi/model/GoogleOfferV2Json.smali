.class public final Lcom/google/api/services/plusi/model/GoogleOfferV2Json;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GoogleOfferV2Json.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GoogleOfferV2;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GoogleOfferV2Json;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GoogleOfferV2Json;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GoogleOfferV2Json;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GoogleOfferV2Json;->INSTANCE:Lcom/google/api/services/plusi/model/GoogleOfferV2Json;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/GoogleOfferV2;

    const/16 v1, 0x20

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "about"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "author"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "availabilityEnds"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "availabilityStarts"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "availableAtOrFrom"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "description"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "descriptionTruncated"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "faviconUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "finePrint"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "id"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "imageUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "localizedValidThrough"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "name"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "offerState"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "offerType"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "ownerObfuscatedId"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "previewUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "price"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "proxiedFaviconUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-class v3, Lcom/google/api/services/plusi/model/ThumbnailJson;

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "proxiedImage"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "saved"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "seller"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "shortTitle"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "totalSaves"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "url"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "validFrom"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "validThrough"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GoogleOfferV2Json;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/GoogleOfferV2Json;->INSTANCE:Lcom/google/api/services/plusi/model/GoogleOfferV2Json;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;

    const/16 v0, 0x1b

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->about:Lcom/google/api/services/plusi/model/EmbedClientItem;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->author:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->availabilityEnds:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->availabilityStarts:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->availableAtOrFrom:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->description:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->descriptionTruncated:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->faviconUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->finePrint:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->imageUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->localizedValidThrough:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->offerState:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->offerType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->ownerObfuscatedId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->previewUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->price:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->proxiedFaviconUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->proxiedImage:Lcom/google/api/services/plusi/model/Thumbnail;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->saved:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->seller:Lcom/google/api/services/plusi/model/EmbedClientItem;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->shortTitle:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->totalSaves:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->url:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->validFrom:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleOfferV2;->validThrough:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GoogleOfferV2;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GoogleOfferV2;-><init>()V

    return-object v0
.end method
