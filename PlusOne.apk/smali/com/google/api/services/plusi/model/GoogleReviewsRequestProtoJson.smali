.class public final Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "GoogleReviewsRequestProtoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/CommonReviewOptionsProtoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "commonOptions"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "qualityScoreThreshold"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "requestOwnerResponses"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "requestZagatReviews"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "sortBy"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "suppressRatingOnlyReviews"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;->INSTANCE:Lcom/google/api/services/plusi/model/GoogleReviewsRequestProtoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;->commonOptions:Lcom/google/api/services/plusi/model/CommonReviewOptionsProto;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;->qualityScoreThreshold:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;->requestOwnerResponses:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;->requestZagatReviews:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;->sortBy:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;->suppressRatingOnlyReviews:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;-><init>()V

    return-object v0
.end method
