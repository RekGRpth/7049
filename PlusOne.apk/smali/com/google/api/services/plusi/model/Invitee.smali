.class public final Lcom/google/api/services/plusi/model/Invitee;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Invitee.java"


# instance fields
.field public album:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusEventAlbum;",
            ">;"
        }
    .end annotation
.end field

.field public canUploadPhotos:Ljava/lang/Boolean;

.field public deprecated10:Lcom/google/api/services/plusi/model/EventTime;

.field public invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

.field public inviter:Lcom/google/api/services/plusi/model/EmbedsPerson;

.field public inviterSquare:Lcom/google/api/services/plusi/model/EmbedsSquare;

.field public isAdminBlacklisted:Ljava/lang/Boolean;

.field public numAdditionalGuests:Ljava/lang/Integer;

.field public readState:Ljava/lang/String;

.field public rsvpToken:Ljava/lang/String;

.field public rsvpType:Ljava/lang/String;

.field public square:Lcom/google/api/services/plusi/model/EmbedsSquare;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
