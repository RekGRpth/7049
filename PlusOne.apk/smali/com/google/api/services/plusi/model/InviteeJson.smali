.class public final Lcom/google/api/services/plusi/model/InviteeJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "InviteeJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/Invitee;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/InviteeJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/InviteeJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/InviteeJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/InviteeJson;->INSTANCE:Lcom/google/api/services/plusi/model/InviteeJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/Invitee;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/PlusEventAlbumJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "album"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "canUploadPhotos"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/EventTimeJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "deprecated10"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/EmbedsPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "invitee"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/EmbedsPersonJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "inviter"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/EmbedsSquareJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "inviterSquare"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "isAdminBlacklisted"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "numAdditionalGuests"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "readState"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "rsvpToken"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "rsvpType"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-class v3, Lcom/google/api/services/plusi/model/EmbedsSquareJson;

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "square"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/InviteeJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/InviteeJson;->INSTANCE:Lcom/google/api/services/plusi/model/InviteeJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/Invitee;

    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->album:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->canUploadPhotos:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->deprecated10:Lcom/google/api/services/plusi/model/EventTime;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->invitee:Lcom/google/api/services/plusi/model/EmbedsPerson;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->inviter:Lcom/google/api/services/plusi/model/EmbedsPerson;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->inviterSquare:Lcom/google/api/services/plusi/model/EmbedsSquare;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->isAdminBlacklisted:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->numAdditionalGuests:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->readState:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->rsvpToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->rsvpType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Invitee;->square:Lcom/google/api/services/plusi/model/EmbedsSquare;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/Invitee;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Invitee;-><init>()V

    return-object v0
.end method
