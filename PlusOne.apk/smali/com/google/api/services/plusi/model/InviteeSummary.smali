.class public final Lcom/google/api/services/plusi/model/InviteeSummary;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "InviteeSummary.java"


# instance fields
.field public count:Ljava/lang/Integer;

.field public invitee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;"
        }
    .end annotation
.end field

.field public resumeToken:Ljava/lang/String;

.field public rsvpIsByInvitee:Ljava/lang/Boolean;

.field public rsvpType:Ljava/lang/String;

.field public setByViewer:Ljava/lang/Boolean;

.field public viewerNumAdditionalGuests:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
