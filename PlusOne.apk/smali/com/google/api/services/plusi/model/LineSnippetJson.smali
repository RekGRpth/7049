.class public final Lcom/google/api/services/plusi/model/LineSnippetJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LineSnippetJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LineSnippet;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LineSnippetJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LineSnippetJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LineSnippetJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LineSnippetJson;->INSTANCE:Lcom/google/api/services/plusi/model/LineSnippetJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LineSnippet;

    const/16 v1, 0xa

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "backgroundColor"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "iconAlt"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "iconSrc"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "longName"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "nameInColorBox"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "nameNonBold"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "textColor"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "tooltip"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "vehicleTypeName"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LineSnippetJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LineSnippetJson;->INSTANCE:Lcom/google/api/services/plusi/model/LineSnippetJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LineSnippet;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->backgroundColor:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->iconAlt:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->iconSrc:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->longName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->nameInColorBox:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->nameNonBold:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->textColor:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->tooltip:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LineSnippet;->vehicleTypeName:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LineSnippet;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LineSnippet;-><init>()V

    return-object v0
.end method
