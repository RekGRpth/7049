.class public final Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LinkPreviewRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LinkPreviewRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LinkPreviewRequest;

    const/16 v1, 0x12

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "callToActionDeepLinkId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "callToActionLabel"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "callToActionLabelDeprecated"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "callToActionUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "clientId"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "content"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "deepLinkId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/ClientEmbedOptionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "embedOptions"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "fallbackToUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "isInteractivePost"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "useBlackboxPreviewData"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "useEmbedV2"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "useSmallPreviews"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/LinkPreviewRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;

    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionDeepLinkId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionLabel:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionLabelDeprecated:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->callToActionUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->clientId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->content:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->deepLinkId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->embedOptions:Lcom/google/api/services/plusi/model/ClientEmbedOptions;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->fallbackToUrl:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->isInteractivePost:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->useBlackboxPreviewData:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->useEmbedV2:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LinkPreviewRequest;->useSmallPreviews:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LinkPreviewRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LinkPreviewRequest;-><init>()V

    return-object v0
.end method
