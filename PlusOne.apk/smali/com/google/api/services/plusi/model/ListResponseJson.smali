.class public final Lcom/google/api/services/plusi/model/ListResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "ListResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/ListResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/ListResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ListResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ListResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/ListResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/ListResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/ListResponse;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataAlmaMaterPropertiesJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "almaMater"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "errorCode"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "experimentNames"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "listType"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/PeopleViewPersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "people"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "retrySeconds"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/ListResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/ListResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/ListResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/ListResponse;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ListResponse;->almaMater:Lcom/google/api/services/plusi/model/DataAlmaMaterProperties;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ListResponse;->errorCode:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ListResponse;->experimentNames:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ListResponse;->listType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ListResponse;->people:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/ListResponse;->retrySeconds:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/ListResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/ListResponse;-><init>()V

    return-object v0
.end method
