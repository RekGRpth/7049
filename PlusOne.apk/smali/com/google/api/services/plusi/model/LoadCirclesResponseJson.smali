.class public final Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoadCirclesResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoadCirclesResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LoadCirclesResponse;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "changesSinceMillis"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DataCircleDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "circleData"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataCirclePersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "circlePerson"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/DataContinuationTokenJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "continuationToken"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "totalPeople"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoadCirclesResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LoadCirclesResponse;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadCirclesResponse;->changesSinceMillis:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadCirclesResponse;->circleData:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadCirclesResponse;->circlePerson:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadCirclesResponse;->continuationToken:Lcom/google/api/services/plusi/model/DataContinuationToken;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoadCirclesResponse;->totalPeople:Ljava/lang/Integer;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoadCirclesResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoadCirclesResponse;-><init>()V

    return-object v0
.end method
