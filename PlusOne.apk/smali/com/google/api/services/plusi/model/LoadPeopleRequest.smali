.class public final Lcom/google/api/services/plusi/model/LoadPeopleRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoadPeopleRequest.java"


# instance fields
.field public circleMemberId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleMemberId;",
            ">;"
        }
    .end annotation
.end field

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public includeIsFollowing:Ljava/lang/Boolean;

.field public includeMemberships:Ljava/lang/Boolean;

.field public versionInfo:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
