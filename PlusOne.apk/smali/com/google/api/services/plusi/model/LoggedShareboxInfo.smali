.class public final Lcom/google/api/services/plusi/model/LoggedShareboxInfo;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "LoggedShareboxInfo.java"


# instance fields
.field public alsoSendEmail:Ljava/lang/Boolean;

.field public autocheckedAlsoSendEmail:Ljava/lang/Boolean;

.field public emailRecipients:Ljava/lang/Integer;

.field public postSize:Ljava/lang/Integer;

.field public postingMode:Ljava/lang/String;

.field public shareTargetType:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public shareType:Ljava/lang/Integer;

.field public sharedToEmptyCircles:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
