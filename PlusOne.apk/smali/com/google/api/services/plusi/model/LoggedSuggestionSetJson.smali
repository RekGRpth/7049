.class public final Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "LoggedSuggestionSetJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/LoggedSuggestionSet;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/LoggedSuggestionSet;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "info"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfoJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "summaryInfo"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;->INSTANCE:Lcom/google/api/services/plusi/model/LoggedSuggestionSetJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/LoggedSuggestionSet;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionSet;->info:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/LoggedSuggestionSet;->summaryInfo:Lcom/google/api/services/plusi/model/LoggedSuggestionSummaryInfo;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/LoggedSuggestionSet;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/LoggedSuggestionSet;-><init>()V

    return-object v0
.end method
