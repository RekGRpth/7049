.class public final Lcom/google/api/services/plusi/model/MediaProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MediaProto.java"


# instance fields
.field public authorName:Ljava/lang/String;

.field public latLng:Lcom/google/api/services/plusi/model/LatLngProto;

.field public originalIndex:Ljava/lang/Integer;

.field public reviewId:Ljava/lang/String;

.field public sourceId:Ljava/lang/Integer;

.field public sourceName:Ljava/lang/String;

.field public thumbnail:Lcom/google/api/services/plusi/model/MediaProtoThumbnail;

.field public thumbnails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MediaProtoThumbnail;",
            ">;"
        }
    .end annotation
.end field

.field public title:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public userMedia:Lcom/google/api/services/plusi/model/UserMediaProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
