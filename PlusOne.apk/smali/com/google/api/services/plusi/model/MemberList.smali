.class public final Lcom/google/api/services/plusi/model/MemberList;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MemberList.java"


# instance fields
.field public continuationToken:Ljava/lang/String;

.field public member:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SquareMember;",
            ">;"
        }
    .end annotation
.end field

.field public membershipStatus:Ljava/lang/String;

.field public totalMembers:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
