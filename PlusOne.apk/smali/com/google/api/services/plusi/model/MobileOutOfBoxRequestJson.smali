.class public final Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "MobileOutOfBoxRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    const/16 v1, 0x11

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxActionJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "action"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "allowNonGooglePlusUsers"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "clientType"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "continueUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxInputFieldJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "input"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "integrated"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "invitationToken"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "partnerId"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "postMessageTargetOrigin"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "upgradeOrigin"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "webClientPathAndQuery"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/MobileOutOfBoxRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->action:Lcom/google/api/services/plusi/model/OutOfBoxAction;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->allowNonGooglePlusUsers:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->clientType:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->continueUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->input:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->integrated:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->invitationToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->partnerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->postMessageTargetOrigin:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->upgradeOrigin:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;->webClientPathAndQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxRequest;-><init>()V

    return-object v0
.end method
