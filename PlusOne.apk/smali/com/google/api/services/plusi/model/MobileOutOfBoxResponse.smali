.class public final Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MobileOutOfBoxResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public continueUrl:Ljava/lang/String;

.field public failureView:Lcom/google/api/services/plusi/model/OutOfBoxView;

.field public fbsVersionInfo:Ljava/lang/String;

.field public postMessageTargetOrigin:Ljava/lang/String;

.field public redirectUrl:Ljava/lang/String;

.field public signupComplete:Ljava/lang/Boolean;

.field public smsSent:Ljava/lang/Boolean;

.field public view:Lcom/google/api/services/plusi/model/OutOfBoxView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
