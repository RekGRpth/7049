.class public final Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "MobileOutOfBoxResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "continueUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxViewJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "failureView"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "postMessageTargetOrigin"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "redirectUrl"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "signupComplete"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "smsSent"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxViewJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "view"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/MobileOutOfBoxResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->continueUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->failureView:Lcom/google/api/services/plusi/model/OutOfBoxView;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->postMessageTargetOrigin:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->redirectUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->signupComplete:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->smsSent:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;->view:Lcom/google/api/services/plusi/model/OutOfBoxView;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/MobileOutOfBoxResponse;-><init>()V

    return-object v0
.end method
