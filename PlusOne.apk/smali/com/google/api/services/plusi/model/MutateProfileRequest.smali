.class public final Lcom/google/api/services/plusi/model/MutateProfileRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MutateProfileRequest.java"


# instance fields
.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public includeAclData:Ljava/lang/Boolean;

.field public includeIncomingMembership:Ljava/lang/Boolean;

.field public includeViewerCircles:Ljava/lang/Boolean;

.field public profile:Lcom/google/api/services/plusi/model/SimpleProfile;

.field public useCachedDataForCircles:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
