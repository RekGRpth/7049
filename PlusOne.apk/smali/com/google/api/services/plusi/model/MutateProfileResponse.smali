.class public final Lcom/google/api/services/plusi/model/MutateProfileResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "MutateProfileResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public errorCode:Ljava/lang/String;

.field public errorMessage:Ljava/lang/String;

.field public fbsVersionInfo:Ljava/lang/String;

.field public updatedProfile:Lcom/google/api/services/plusi/model/SimpleProfile;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
