.class public final Lcom/google/api/services/plusi/model/NearbyTransitProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "NearbyTransitProto.java"


# instance fields
.field public hasMoreStations:Ljava/lang/Boolean;

.field public isStation:Ljava/lang/Boolean;

.field public startIndex:Ljava/lang/Integer;

.field public station:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/NearbyTransitProtoStation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
