.class public final Lcom/google/api/services/plusi/model/NotificationsResponseOptions;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "NotificationsResponseOptions.java"


# instance fields
.field public includeActorMap:Ljava/lang/Boolean;

.field public includeDeletedEntities:Ljava/lang/Boolean;

.field public includeFullActorDetails:Ljava/lang/Boolean;

.field public includeFullEntityDetails:Ljava/lang/Boolean;

.field public includeFullRootDetails:Ljava/lang/Boolean;

.field public numPhotoEntities:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
