.class public final Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "OutOfBoxImageFieldJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/OutOfBoxImageField;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;->INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "centered"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxTextFieldJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "coverText"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/OutOfBoxTextFieldJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "descriptiveText"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "type"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;->INSTANCE:Lcom/google/api/services/plusi/model/OutOfBoxImageFieldJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxImageField;->centered:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxImageField;->coverText:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxImageField;->descriptiveText:Lcom/google/api/services/plusi/model/OutOfBoxTextField;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/OutOfBoxImageField;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/OutOfBoxImageField;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/OutOfBoxImageField;-><init>()V

    return-object v0
.end method
