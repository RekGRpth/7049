.class public final Lcom/google/api/services/plusi/model/OutOfBoxView;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "OutOfBoxView.java"


# instance fields
.field public action:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxAction;",
            ">;"
        }
    .end annotation
.end field

.field public dialog:Lcom/google/api/services/plusi/model/OutOfBoxDialog;

.field public field:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/OutOfBoxField;",
            ">;"
        }
    .end annotation
.end field

.field public header:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
