.class public final Lcom/google/api/services/plusi/model/PeopleViewPersonJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PeopleViewPersonJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PeopleViewPerson;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PeopleViewPersonJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PeopleViewPersonJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PeopleViewPersonJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PeopleViewPersonJson;->INSTANCE:Lcom/google/api/services/plusi/model/PeopleViewPersonJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/DataCirclePersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "commonFriend"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "friendSuggestionSummarizedAdditionalInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "friendSuggestionSummarizedInfoBitmask"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/DataCirclePersonJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "member"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "numberOfCommonFriends"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "score"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "suggestionId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PeopleViewPersonJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PeopleViewPersonJson;->INSTANCE:Lcom/google/api/services/plusi/model/PeopleViewPersonJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->commonFriend:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->friendSuggestionSummarizedAdditionalInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->friendSuggestionSummarizedInfoBitmask:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->member:Lcom/google/api/services/plusi/model/DataCirclePerson;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->numberOfCommonFriends:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->score:Ljava/lang/Double;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PeopleViewPerson;->suggestionId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PeopleViewPerson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PeopleViewPerson;-><init>()V

    return-object v0
.end method
