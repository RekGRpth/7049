.class public final Lcom/google/api/services/plusi/model/PhotoServiceShareActionData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotoServiceShareActionData.java"


# instance fields
.field public albumTitle:Ljava/lang/String;

.field public isAlbumReshare:Ljava/lang/Boolean;

.field public isFullAlbumShare:Ljava/lang/Boolean;

.field public mediaRef:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PhotoServiceMediaReference;",
            ">;"
        }
    .end annotation
.end field

.field public targetAlbum:Lcom/google/api/services/plusi/model/PhotoServiceShareActionDataAlbum;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
