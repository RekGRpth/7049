.class public final Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PhotosInAlbumRequest.java"


# instance fields
.field public authkey:Ljava/lang/String;

.field public collectionId:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public maxResults:Ljava/lang/Integer;

.field public offset:Ljava/lang/Integer;

.field public ownerId:Ljava/lang/String;

.field public photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

.field public photosSortOrder:Ljava/lang/String;

.field public returnAlbumShapeClusters:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
