.class public final Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotosInAlbumRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "authkey"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "collectionId"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "maxResults"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "offset"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/RequestsPhotoOptionsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "photoOptions"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "photosSortOrder"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "returnAlbumShapeClusters"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosInAlbumRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->authkey:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->collectionId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->maxResults:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->offset:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->photoOptions:Lcom/google/api/services/plusi/model/RequestsPhotoOptions;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->photosSortOrder:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;->returnAlbumShapeClusters:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosInAlbumRequest;-><init>()V

    return-object v0
.end method
