.class public final Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PhotosPlusOneRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;

    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "activityId"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "albumId"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "authkey"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "deprecatedPhotoId"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "isPlusOne"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "ownerId"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "photoId"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "returnPlusOneResult"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "squareId"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "squareStreamId"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/PhotosPlusOneRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;

    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->activityId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->albumId:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->authkey:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->deprecatedPhotoId:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->isPlusOne:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->ownerId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->photoId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->returnPlusOneResult:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->squareId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;->squareStreamId:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PhotosPlusOneRequest;-><init>()V

    return-object v0
.end method
