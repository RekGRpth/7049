.class public final Lcom/google/api/services/plusi/model/PlayMusicTrack;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlayMusicTrack.java"


# instance fields
.field public audio:Lcom/google/api/services/plusi/model/AudioObject;

.field public audioEmbedUrlWithSessionIndex:Ljava/lang/String;

.field public audioUrlWithSessionIndex:Ljava/lang/String;

.field public byArtist:Lcom/google/api/services/plusi/model/MusicGroup;

.field public description:Ljava/lang/String;

.field public explicitType:Ljava/lang/String;

.field public imageUrl:Ljava/lang/String;

.field public inAlbum:Lcom/google/api/services/plusi/model/PlayMusicAlbum;

.field public name:Ljava/lang/String;

.field public offerUrlWithSessionIndex:Ljava/lang/String;

.field public offers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Offer;",
            ">;"
        }
    .end annotation
.end field

.field public previewToken:Ljava/lang/String;

.field public purchaseStatus:Ljava/lang/String;

.field public storeId:Ljava/lang/String;

.field public url:Ljava/lang/String;

.field public urlWithSessionIndex:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
