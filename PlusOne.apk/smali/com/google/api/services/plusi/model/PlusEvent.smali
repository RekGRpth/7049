.class public final Lcom/google/api/services/plusi/model/PlusEvent;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlusEvent.java"


# instance fields
.field public abuseStatus:Ljava/lang/String;

.field public authKey:Ljava/lang/String;

.field public creationLocation:Ljava/lang/String;

.field public creator:Lcom/google/api/services/plusi/model/EmbedsPerson;

.field public creatorObfuscatedId:Ljava/lang/String;

.field public deprecated11:Lcom/google/api/services/plusi/model/EventCategory;

.field public deprecated29:Ljava/lang/Boolean;

.field public deprecated6:Ljava/lang/String;

.field public deprecated9:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;"
        }
    .end annotation
.end field

.field public description:Ljava/lang/String;

.field public displayContent:Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

.field public endDate:Ljava/lang/String;

.field public endTime:Lcom/google/api/services/plusi/model/EventTime;

.field public eventOptions:Lcom/google/api/services/plusi/model/EventOptions;

.field public featureFlags:Lcom/google/api/services/plusi/model/PlusEventFeatureFlags;

.field public featuredPlusPhoto:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public hangoutInfo:Lcom/google/api/services/plusi/model/HangoutInfo;

.field public id:Ljava/lang/String;

.field public inviteeSummary:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/InviteeSummary;",
            ">;"
        }
    .end annotation
.end field

.field public isBroadcastView:Ljava/lang/Boolean;

.field public isPublic:Ljava/lang/Boolean;

.field public lastSignificantUpdateTimestampUsec:Ljava/lang/Long;

.field public location:Lcom/google/api/services/plusi/model/Place;

.field public name:Ljava/lang/String;

.field public photoContributor:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Invitee;",
            ">;"
        }
    .end annotation
.end field

.field public photoCount:Ljava/lang/Integer;

.field public photosDownloadUrl:Ljava/lang/String;

.field public startDate:Ljava/lang/String;

.field public startTime:Lcom/google/api/services/plusi/model/EventTime;

.field public theme:Lcom/google/api/services/plusi/model/Theme;

.field public themeSpecification:Lcom/google/api/services/plusi/model/ThemeSpecification;

.field public thirdPartyInfo:Lcom/google/api/services/plusi/model/EventThirdPartyInfo;

.field public url:Ljava/lang/String;

.field public viewerInfo:Lcom/google/api/services/plusi/model/Invitee;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
