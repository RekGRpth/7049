.class public final Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PlusEventDisplayContentJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PlusEventDisplayContent;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;->INSTANCE:Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/PlusEventAudienceJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "audience"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "descriptionHtml"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/ViewSegmentsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "descriptionSegments"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "eventTimeRange"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "eventTimeRangeShort"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "eventTimeStart"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "isEventOver"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "iso8601EventTimeStart"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/PlusEventSocialSummaryJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "socialSummary"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "videoEmbedUrl"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;->INSTANCE:Lcom/google/api/services/plusi/model/PlusEventDisplayContentJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->audience:Lcom/google/api/services/plusi/model/PlusEventAudience;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->descriptionSegments:Lcom/google/api/services/plusi/model/ViewSegments;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->eventTimeRange:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->eventTimeRangeShort:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->eventTimeStart:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->isEventOver:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->iso8601EventTimeStart:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->socialSummary:Lcom/google/api/services/plusi/model/PlusEventSocialSummary;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;->videoEmbedUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PlusEventDisplayContent;-><init>()V

    return-object v0
.end method
