.class public final Lcom/google/api/services/plusi/model/PlusEventSocialSummary;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlusEventSocialSummary.java"


# instance fields
.field public invitee:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EmbedsPerson;",
            ">;"
        }
    .end annotation
.end field

.field public inviteeSummary:Lcom/google/api/services/plusi/model/InviteeSummary;

.field public inviter:Lcom/google/api/services/plusi/model/EmbedsPerson;

.field public socialSummaryType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
