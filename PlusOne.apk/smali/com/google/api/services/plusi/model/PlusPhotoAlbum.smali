.class public final Lcom/google/api/services/plusi/model/PlusPhotoAlbum;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PlusPhotoAlbum.java"


# instance fields
.field public albumId:Ljava/lang/String;

.field public albumSummaryType:Ljava/lang/String;

.field public associatedMedia:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/PlusPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public authkey:Ljava/lang/String;

.field public contentLocation:Lcom/google/api/services/plusi/model/Place;

.field public description:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public ownerObfuscatedId:Ljava/lang/String;

.field public photoCount:Ljava/lang/Integer;

.field public relativeUrl:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
