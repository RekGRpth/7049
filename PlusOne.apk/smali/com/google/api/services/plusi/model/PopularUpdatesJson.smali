.class public final Lcom/google/api/services/plusi/model/PopularUpdatesJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PopularUpdatesJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PopularUpdates;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PopularUpdatesJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PopularUpdatesJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PopularUpdatesJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PopularUpdatesJson;->INSTANCE:Lcom/google/api/services/plusi/model/PopularUpdatesJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PopularUpdates;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "isExpanded"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "position"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/UpdateJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "update"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PopularUpdatesJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PopularUpdatesJson;->INSTANCE:Lcom/google/api/services/plusi/model/PopularUpdatesJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PopularUpdates;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PopularUpdates;->isExpanded:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PopularUpdates;->position:Ljava/lang/Integer;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PopularUpdates;->update:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PopularUpdates;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PopularUpdates;-><init>()V

    return-object v0
.end method
