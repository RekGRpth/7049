.class public final Lcom/google/api/services/plusi/model/PostActivityResponseJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PostActivityResponseJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/PostActivityResponse;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PostActivityResponseJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PostActivityResponseJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PostActivityResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/PostActivityResponse;

    const/16 v1, 0xb

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/TraceRecordsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "backendTrace"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/EmbedClientItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "deleted4"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/LayoutJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "layout"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ShareboxSettingsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "shareboxSettings"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/StreamJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "stream"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PostActivityResponseJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PostActivityResponseJson;->INSTANCE:Lcom/google/api/services/plusi/model/PostActivityResponseJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/PostActivityResponse;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->deleted4:Lcom/google/api/services/plusi/model/EmbedClientItem;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->layout:Lcom/google/api/services/plusi/model/Layout;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->shareboxSettings:Lcom/google/api/services/plusi/model/ShareboxSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/PostActivityResponse;->stream:Lcom/google/api/services/plusi/model/Stream;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PostActivityResponse;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PostActivityResponse;-><init>()V

    return-object v0
.end method
