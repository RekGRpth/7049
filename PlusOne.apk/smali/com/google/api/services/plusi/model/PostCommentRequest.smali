.class public final Lcom/google/api/services/plusi/model/PostCommentRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "PostCommentRequest.java"


# instance fields
.field public activityId:Ljava/lang/String;

.field public botGuardResponse:Ljava/lang/String;

.field public clientId:Ljava/lang/String;

.field public commentSegments:Lcom/google/api/services/plusi/model/EditSegments;

.field public commentText:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public contentFormat:Ljava/lang/String;

.field public creationTimeMs:Ljava/lang/Long;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public renderContextLocation:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
