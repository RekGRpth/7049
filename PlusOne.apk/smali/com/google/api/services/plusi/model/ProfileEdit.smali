.class public final Lcom/google/api/services/plusi/model/ProfileEdit;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ProfileEdit.java"


# instance fields
.field public coverPhotoCoordinate:Ljava/lang/String;

.field public coverPhotoOffset:Ljava/lang/String;

.field public coverPhotoOwnerType:Ljava/lang/String;

.field public coverPhotoRotation:Ljava/lang/String;

.field public currentLocation:Ljava/lang/String;

.field public education:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public employment:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public familyName:Ljava/lang/String;

.field public fullBleedPhotoId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public givenName:Ljava/lang/String;

.field public scrapbookLayout:Ljava/lang/String;

.field public visibility:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ProfileVisibilityEdit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
