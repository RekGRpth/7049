.class public final Lcom/google/api/services/plusi/model/PromoJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "PromoJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/Promo;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/PromoJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/PromoJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/PromoJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/PromoJson;->INSTANCE:Lcom/google/api/services/plusi/model/PromoJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/Promo;

    const/16 v1, 0x18

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "allowDismiss"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/LoadBulkCircleDataResponseJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "bulkCircleData"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/CelebritySuggestionsResponseJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "celebritySuggestions"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "disposition"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/FindFriendsPromoMessageJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "findFriendsPromoMessage"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "isOptional"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-class v3, Lcom/google/api/services/plusi/model/QualitySignalsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "qualitySignals"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponseJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "response"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/TrendResultsJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "results"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-class v3, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "suggestedCelebritiesPromoData"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-class v3, Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessageJson;

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "suggestedCelebritiesPromoMessage"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "suggestedPeoplePromoData"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-class v3, Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessageJson;

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "suggestedPeoplePromoMessage"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "type"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/PromoJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/PromoJson;->INSTANCE:Lcom/google/api/services/plusi/model/PromoJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/Promo;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->allowDismiss:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->bulkCircleData:Lcom/google/api/services/plusi/model/LoadBulkCircleDataResponse;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->celebritySuggestions:Lcom/google/api/services/plusi/model/CelebritySuggestionsResponse;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->disposition:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->findFriendsPromoMessage:Lcom/google/api/services/plusi/model/FindFriendsPromoMessage;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->isOptional:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->qualitySignals:Lcom/google/api/services/plusi/model/QualitySignals;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->response:Lcom/google/api/services/plusi/model/GetFriendSuggestionsResponse;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->results:Lcom/google/api/services/plusi/model/TrendResults;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->suggestedCelebritiesPromoData:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoData;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->suggestedCelebritiesPromoMessage:Lcom/google/api/services/plusi/model/SuggestedCelebritiesPromoMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->suggestedPeoplePromoData:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoData;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->suggestedPeoplePromoMessage:Lcom/google/api/services/plusi/model/SuggestedPeoplePromoMessage;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Promo;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/Promo;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Promo;-><init>()V

    return-object v0
.end method
