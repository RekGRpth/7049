.class public final Lcom/google/api/services/plusi/model/ReadResponsePhotosData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ReadResponsePhotosData.java"


# instance fields
.field public person:Lcom/google/api/services/plusi/model/EmbedsPerson;

.field public photos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataPhoto;",
            ">;"
        }
    .end annotation
.end field

.field public sortType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
