.class public final Lcom/google/api/services/plusi/model/ReadSquareMembersOzResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ReadSquareMembersOzResponse.java"


# instance fields
.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public fbsVersionInfo:Ljava/lang/String;

.field public memberList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/MemberList;",
            ">;"
        }
    .end annotation
.end field

.field public viewerSquare:Lcom/google/api/services/plusi/model/ViewerSquare;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
