.class public final Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "RegisterDeviceRequestJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/RegisterDeviceRequest;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ApiaryFieldsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "commonFields"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettingsJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "destinationSettings"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "enableTracing"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "fbsVersionInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/PushAddressJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "pushAddress"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;->INSTANCE:Lcom/google/api/services/plusi/model/RegisterDeviceRequestJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->destinationSettings:Lcom/google/api/services/plusi/model/RegisterDeviceParamsDestinationSettings;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->enableTracing:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->fbsVersionInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;->pushAddress:Lcom/google/api/services/plusi/model/PushAddress;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RegisterDeviceRequest;-><init>()V

    return-object v0
.end method
