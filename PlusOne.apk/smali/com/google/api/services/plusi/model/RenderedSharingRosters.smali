.class public final Lcom/google/api/services/plusi/model/RenderedSharingRosters;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "RenderedSharingRosters.java"


# instance fields
.field public applicationPolicies:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ApplicationSharingPolicy;",
            ">;"
        }
    .end annotation
.end field

.field public domain:Lcom/google/api/services/plusi/model/DasherDomain;

.field public resourceSharingRosters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ResourceSharingRoster;",
            ">;"
        }
    .end annotation
.end field

.field public targets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SharingTarget;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
