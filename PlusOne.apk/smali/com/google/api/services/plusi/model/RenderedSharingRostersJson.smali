.class public final Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "RenderedSharingRostersJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/RenderedSharingRosters;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;->INSTANCE:Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/RenderedSharingRosters;

    const/16 v1, 0x8

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ApplicationSharingPolicyJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "applicationPolicies"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/DasherDomainJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "domain"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/ResourceSharingRosterJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "resourceSharingRosters"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-class v3, Lcom/google/api/services/plusi/model/SharingTargetJson;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "targets"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;->INSTANCE:Lcom/google/api/services/plusi/model/RenderedSharingRostersJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/RenderedSharingRosters;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderedSharingRosters;->applicationPolicies:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderedSharingRosters;->domain:Lcom/google/api/services/plusi/model/DasherDomain;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderedSharingRosters;->resourceSharingRosters:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/RenderedSharingRosters;->targets:Ljava/util/List;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/RenderedSharingRosters;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/RenderedSharingRosters;-><init>()V

    return-object v0
.end method
