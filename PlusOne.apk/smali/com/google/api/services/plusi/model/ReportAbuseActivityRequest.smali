.class public final Lcom/google/api/services/plusi/model/ReportAbuseActivityRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ReportAbuseActivityRequest.java"


# instance fields
.field public abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public isUndo:Ljava/lang/Boolean;

.field public itemId:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public renderContext:Lcom/google/api/services/plusi/model/RenderContext;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
