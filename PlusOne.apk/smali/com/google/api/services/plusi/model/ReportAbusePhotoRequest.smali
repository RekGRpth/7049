.class public final Lcom/google/api/services/plusi/model/ReportAbusePhotoRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ReportAbusePhotoRequest.java"


# instance fields
.field public abuseReport:Lcom/google/api/services/plusi/model/DataAbuseReport;

.field public authkey:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public enableTracing:Ljava/lang/Boolean;

.field public fbsVersionInfo:Ljava/lang/String;

.field public ownerId:Ljava/lang/String;

.field public photoId:Ljava/lang/Long;

.field public reportToken:Ljava/lang/String;

.field public signedClusterId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
