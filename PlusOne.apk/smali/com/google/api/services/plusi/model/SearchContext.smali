.class public final Lcom/google/api/services/plusi/model/SearchContext;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SearchContext.java"


# instance fields
.field public clientOverrides:Lcom/google/api/services/plusi/model/ClientOverrides;

.field public location:Lcom/google/api/services/plusi/model/LocationData;

.field public param:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/SearchContextParam;",
            ">;"
        }
    .end annotation
.end field

.field public placesInCommonQuery:Ljava/lang/Boolean;

.field public whatChip:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ChipData;",
            ">;"
        }
    .end annotation
.end field

.field public whatQuery:Ljava/lang/String;

.field public whereChip:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ChipData;",
            ">;"
        }
    .end annotation
.end field

.field public whereQuery:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
