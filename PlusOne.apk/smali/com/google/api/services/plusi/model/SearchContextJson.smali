.class public final Lcom/google/api/services/plusi/model/SearchContextJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "SearchContextJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/SearchContext;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/SearchContextJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SearchContextJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchContextJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/SearchContextJson;->INSTANCE:Lcom/google/api/services/plusi/model/SearchContextJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/SearchContext;

    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/ClientOverridesJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "clientOverrides"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-class v3, Lcom/google/api/services/plusi/model/LocationDataJson;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "location"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/SearchContextParamJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "param"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "placesInCommonQuery"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-class v3, Lcom/google/api/services/plusi/model/ChipDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "whatChip"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "whatQuery"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/ChipDataJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "whereChip"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "whereQuery"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/SearchContextJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/SearchContextJson;->INSTANCE:Lcom/google/api/services/plusi/model/SearchContextJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/SearchContext;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->clientOverrides:Lcom/google/api/services/plusi/model/ClientOverrides;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->location:Lcom/google/api/services/plusi/model/LocationData;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->param:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->placesInCommonQuery:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->whatChip:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->whatQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->whereChip:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SearchContext;->whereQuery:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SearchContext;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SearchContext;-><init>()V

    return-object v0
.end method
