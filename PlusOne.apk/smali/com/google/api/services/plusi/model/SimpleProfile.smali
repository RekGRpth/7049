.class public final Lcom/google/api/services/plusi/model/SimpleProfile;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SimpleProfile.java"


# instance fields
.field public config:Lcom/google/api/services/plusi/model/CommonConfig;

.field public content:Lcom/google/api/services/plusi/model/CommonContent;

.field public displayName:Ljava/lang/String;

.field public obfuscatedGaiaId:Ljava/lang/String;

.field public page:Lcom/google/api/services/plusi/model/Page;

.field public profileType:Ljava/lang/String;

.field public rosterData:Lcom/google/api/services/plusi/model/SharingRosterData;

.field public user:Lcom/google/api/services/plusi/model/User;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
