.class public final Lcom/google/api/services/plusi/model/SquareProfile;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SquareProfile.java"


# instance fields
.field public aboutText:Ljava/lang/String;

.field public abuseAppealState:Ljava/lang/String;

.field public abuseState:Ljava/lang/String;

.field public abuseType:Ljava/lang/String;

.field public bannerUrl:Ljava/lang/String;

.field public isDomainRestricted:Ljava/lang/Boolean;

.field public location:Lcom/google/api/services/plusi/model/Place;

.field public name:Ljava/lang/String;

.field public photoUrl:Ljava/lang/String;

.field public relatedLinkList:Lcom/google/api/services/plusi/model/SquareRelatedLinks;

.field public relatedLinks:Lcom/google/api/services/plusi/model/SquareRelatedLinks;

.field public tagline:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
