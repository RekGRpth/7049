.class public final Lcom/google/api/services/plusi/model/SquareResultJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "SquareResultJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/SquareResult;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/SquareResultJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SquareResultJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SquareResultJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/SquareResultJson;->INSTANCE:Lcom/google/api/services/plusi/model/SquareResultJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/SquareResult;

    const/16 v1, 0x10

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "displayName"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "dominant"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/google/api/services/plusi/model/SquareResultJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "memberCount"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-class v3, Lcom/google/api/services/plusi/model/SquareResultSquareMemberJson;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "peopleInCommon"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "photoUrl"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/api/services/plusi/model/SquareResultJson;->JSON_STRING:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "postCount"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "privatePosts"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "snippetHtml"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-class v3, Lcom/google/api/services/plusi/model/SquareIdJson;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "squareId"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/SquareResultSquareMemberJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "squareOwner"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "tagline"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/SquareResultJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/SquareResultJson;->INSTANCE:Lcom/google/api/services/plusi/model/SquareResultJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/SquareResult;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->displayName:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->dominant:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->memberCount:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->peopleInCommon:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->photoUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->postCount:Ljava/lang/Long;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->privatePosts:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->snippetHtml:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->squareId:Lcom/google/api/services/plusi/model/SquareId;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->squareOwner:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/SquareResult;->tagline:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/SquareResult;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/SquareResult;-><init>()V

    return-object v0
.end method
