.class public final Lcom/google/api/services/plusi/model/StaticMapProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "StaticMapProto.java"


# instance fields
.field public additionalMaps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/StaticMapProtoImage;",
            ">;"
        }
    .end annotation
.end field

.field public directionsLink:Lcom/google/api/services/plusi/model/PlacePageLink;

.field public image:Lcom/google/api/services/plusi/model/StaticMapProtoImage;

.field public latitudeE6:Ljava/lang/Long;

.field public link:Lcom/google/api/services/plusi/model/PlacePageLink;

.field public longitudeE6:Ljava/lang/Long;

.field public zoomLevel:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
