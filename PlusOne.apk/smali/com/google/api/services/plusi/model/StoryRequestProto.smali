.class public final Lcom/google/api/services/plusi/model/StoryRequestProto;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "StoryRequestProto.java"


# instance fields
.field public compositeRequest:Lcom/google/api/services/plusi/model/CompositeStoryRequestProto;

.field public googleReviewsRequest:Lcom/google/api/services/plusi/model/GoogleReviewsRequestProto;

.field public maxNum:Ljava/lang/Integer;

.field public snippetTag:Ljava/lang/String;

.field public start:Ljava/lang/Integer;

.field public staticMapRequest:Lcom/google/api/services/plusi/model/StaticMapRequestProto;

.field public type:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
