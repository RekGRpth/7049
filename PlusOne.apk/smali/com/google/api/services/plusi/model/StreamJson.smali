.class public final Lcom/google/api/services/plusi/model/StreamJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "StreamJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/Stream;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/StreamJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/StreamJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/StreamJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/StreamJson;->INSTANCE:Lcom/google/api/services/plusi/model/StreamJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/Stream;

    const/16 v1, 0xf

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "continuationToken"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/DebugJson;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "debugInfo"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/StreamItemJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "item"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/LayoutJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "layout"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "mixerDebugInfo"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/StreamParamsJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "params"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/PopularUpdatesJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "popularUpdates"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-class v3, Lcom/google/api/services/plusi/model/UpdateJson;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "update"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "volume"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/StreamJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/StreamJson;->INSTANCE:Lcom/google/api/services/plusi/model/StreamJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/Stream;

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->continuationToken:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->debugInfo:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->item:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->layout:Lcom/google/api/services/plusi/model/Layout;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->mixerDebugInfo:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->params:Lcom/google/api/services/plusi/model/StreamParams;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->popularUpdates:Lcom/google/api/services/plusi/model/PopularUpdates;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->update:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Stream;->volume:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/Stream;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Stream;-><init>()V

    return-object v0
.end method
