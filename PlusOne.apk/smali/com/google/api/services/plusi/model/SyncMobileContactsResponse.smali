.class public final Lcom/google/api/services/plusi/model/SyncMobileContactsResponse;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "SyncMobileContactsResponse.java"


# instance fields
.field public aggregationSyncRequired:Ljava/lang/Boolean;

.field public backendTrace:Lcom/google/api/services/plusi/model/TraceRecords;

.field public failedContact:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/DataCircleMemberId;",
            ">;"
        }
    .end annotation
.end field

.field public fbsVersionInfo:Ljava/lang/String;

.field public status:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
