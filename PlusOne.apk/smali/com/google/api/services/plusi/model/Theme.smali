.class public final Lcom/google/api/services/plusi/model/Theme;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Theme.java"


# instance fields
.field public category:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/EventCategory;",
            ">;"
        }
    .end annotation
.end field

.field public image:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/ThemeImage;",
            ">;"
        }
    .end annotation
.end field

.field public restrictions:Lcom/google/api/services/plusi/model/ThemeRestrictions;

.field public themeId:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
