.class public final Lcom/google/api/services/plusi/model/Thumbnail;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Thumbnail.java"


# instance fields
.field public boxHeightPx:Ljava/lang/Integer;

.field public boxWidthPx:Ljava/lang/Integer;

.field public exactHeight:Ljava/lang/Boolean;

.field public exactWidth:Ljava/lang/Boolean;

.field public imageHeightPx:Ljava/lang/Integer;

.field public imageUrl:Ljava/lang/String;

.field public recenterVertically:Ljava/lang/Boolean;

.field public safeMobileUrl:Lcom/google/api/services/plusi/model/SafeMobileUrl;

.field public uncroppedImageUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
