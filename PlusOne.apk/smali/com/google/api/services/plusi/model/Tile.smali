.class public final Lcom/google/api/services/plusi/model/Tile;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "Tile.java"


# instance fields
.field public albumTile:Lcom/google/api/services/plusi/model/AlbumTile;

.field public backgroundColor:Ljava/lang/String;

.field public children:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/api/services/plusi/model/Tile;",
            ">;"
        }
    .end annotation
.end field

.field public collectionTile:Lcom/google/api/services/plusi/model/CollectionTile;

.field public defaultRenderingSupported:Ljava/lang/Boolean;

.field public eventTile:Lcom/google/api/services/plusi/model/EventTile;

.field public image:Lcom/google/api/services/plusi/model/DataImage;

.field public linkUrl:Ljava/lang/String;

.field public photoTile:Lcom/google/api/services/plusi/model/PhotoTile;

.field public subtitle:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public tileId:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public userTile:Lcom/google/api/services/plusi/model/UserTile;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
