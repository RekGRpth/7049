.class public final Lcom/google/api/services/plusi/model/TileJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "TileJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/Tile;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/TileJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/TileJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TileJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/TileJson;->INSTANCE:Lcom/google/api/services/plusi/model/TileJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/Tile;

    const/16 v1, 0x15

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-class v3, Lcom/google/api/services/plusi/model/AlbumTileJson;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "albumTile"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "backgroundColor"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-class v3, Lcom/google/api/services/plusi/model/TileJson;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "children"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-class v3, Lcom/google/api/services/plusi/model/CollectionTileJson;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "collectionTile"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "defaultRenderingSupported"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-class v3, Lcom/google/api/services/plusi/model/EventTileJson;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "eventTile"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-class v3, Lcom/google/api/services/plusi/model/DataImageJson;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "image"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "linkUrl"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-class v3, Lcom/google/api/services/plusi/model/PhotoTileJson;

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "photoTile"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "subtitle"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "tileId"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "title"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-class v3, Lcom/google/api/services/plusi/model/UserTileJson;

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "userTile"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/TileJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/TileJson;->INSTANCE:Lcom/google/api/services/plusi/model/TileJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/Tile;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->albumTile:Lcom/google/api/services/plusi/model/AlbumTile;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->backgroundColor:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->children:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->collectionTile:Lcom/google/api/services/plusi/model/CollectionTile;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->defaultRenderingSupported:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->eventTile:Lcom/google/api/services/plusi/model/EventTile;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->image:Lcom/google/api/services/plusi/model/DataImage;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->linkUrl:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->photoTile:Lcom/google/api/services/plusi/model/PhotoTile;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->subtitle:Ljava/util/List;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->tileId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->title:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->type:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    iget-object v2, p1, Lcom/google/api/services/plusi/model/Tile;->userTile:Lcom/google/api/services/plusi/model/UserTile;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/Tile;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/Tile;-><init>()V

    return-object v0
.end method
