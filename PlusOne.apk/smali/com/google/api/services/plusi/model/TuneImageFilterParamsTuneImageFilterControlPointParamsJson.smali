.class public final Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;
.super Lcom/google/android/apps/plus/json/EsJson;
.source "TuneImageFilterParamsTuneImageFilterControlPointParamsJson.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/plus/json/EsJson",
        "<",
        "Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;-><init>()V

    sput-object v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    const-class v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "brightness"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Lcom/google/api/services/plusi/model/VectorParamsVector2Json;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "center"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "contrast"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "diameter"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "inking"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "saturation"

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/json/EsJson;-><init>(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method public static getInstance()Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;
    .locals 1

    sget-object v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;->INSTANCE:Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParamsJson;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getValues(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;->brightness:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;->center:Lcom/google/api/services/plusi/model/VectorParamsVector2;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;->contrast:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;->diameter:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;->inking:Ljava/lang/Float;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p1, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;->saturation:Ljava/lang/Float;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final bridge synthetic newInstance()Ljava/lang/Object;
    .locals 1

    new-instance v0, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;

    invoke-direct {v0}, Lcom/google/api/services/plusi/model/TuneImageFilterParamsTuneImageFilterControlPointParams;-><init>()V

    return-object v0
.end method
