.class public final Lcom/google/api/services/plusi/model/UploadMediaRequest;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "UploadMediaRequest.java"


# instance fields
.field public albumId:Ljava/lang/String;

.field public albumLabel:Ljava/lang/String;

.field public albumTitle:Ljava/lang/String;

.field public autoResize:Ljava/lang/Boolean;

.field public clientAssignedId:Ljava/lang/String;

.field public commonFields:Lcom/google/api/services/plusi/model/ApiaryFields;

.field public coordinate:Lcom/google/api/services/plusi/model/ScrapbookInfoCoverLayoutCoordinate;

.field public description:Ljava/lang/String;

.field public displayName:Ljava/lang/String;

.field public enableTracing:Ljava/lang/Boolean;

.field public eventId:Ljava/lang/String;

.field public fbsVersionInfo:Ljava/lang/String;

.field public localData:Lcom/google/api/services/plusi/model/LocalData;

.field public offset:Ljava/lang/Integer;

.field public ownerId:Ljava/lang/String;

.field public rotation:Ljava/lang/Integer;

.field public scottyMedia:Lcom/google/api/services/plusi/model/ScottyMedia;

.field public setProfilePhoto:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
