.class public final Lcom/google/api/services/plusi/model/ViewerData;
.super Lcom/google/android/apps/plus/json/GenericJson;
.source "ViewerData.java"


# instance fields
.field public actualUserObfuscatedId:Ljava/lang/String;

.field public ageInYears:Ljava/lang/Integer;

.field public allFocusGroups:Lcom/google/api/services/plusi/model/AllFocusGroups;

.field public birthdayHasYear:Ljava/lang/Boolean;

.field public contactMatchingOptIn:Ljava/lang/Boolean;

.field public emailAddress:Ljava/lang/String;

.field public externalLinks:Lcom/google/api/services/plusi/model/Links;

.field public fromAddress:Ljava/lang/String;

.field public geographicLocation:Ljava/lang/String;

.field public hasEsMobile:Ljava/lang/Boolean;

.field public hasVoiceAccountDeprecated:Ljava/lang/Boolean;

.field public isBuzzUser:Ljava/lang/Boolean;

.field public isDefaultRestricted:Ljava/lang/Boolean;

.field public isDefaultSortLatest:Ljava/lang/Boolean;

.field public isFirstTimeResharePromoDismissed:Ljava/lang/Boolean;

.field public isGmailUser:Ljava/lang/Boolean;

.field public isGoogleMeUser:Ljava/lang/Boolean;

.field public isOrkutUser:Ljava/lang/Boolean;

.field public isPlusOneUser:Ljava/lang/Boolean;

.field public isProfilePublic:Ljava/lang/Boolean;

.field public locale:Ljava/lang/String;

.field public nameFormatData:Lcom/google/api/services/plusi/model/NameFormatData;

.field public navSelectedPath:Ljava/lang/String;

.field public navShowChatPromo:Ljava/lang/Boolean;

.field public obfuscatedGaiaId:Ljava/lang/String;

.field public outgoingAnyCircleCount:Ljava/lang/Integer;

.field public primaryUsername:Ljava/lang/String;

.field public profile:Lcom/google/api/services/plusi/model/Profile;

.field public profileHasAcls:Ljava/lang/Boolean;

.field public segmentationInfo:Lcom/google/api/services/plusi/model/SegmentationInfo;

.field public selectedViewFilter:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;

.field public siloPolicy:Ljava/lang/String;

.field public visibleCirclesInfo:Lcom/google/api/services/plusi/model/VisibleCirclesInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/apps/plus/json/GenericJson;-><init>()V

    return-void
.end method
