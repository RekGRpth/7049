.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HangoutInviteNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

.field private context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

.field private dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

.field private hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

.field private status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;-><init>()V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    move-result-object v1

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;->RINGING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->RING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->NOTIFICATION_RING:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;->UNKNOWN:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;)V
    .locals 2
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    invoke-direct {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    return-object p1
.end method

.method static synthetic access$402(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    return-object p1
.end method

.method static synthetic access$802(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;I)I
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .param p1    # I

    iput p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .locals 1

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    return-object v0
.end method

.method public static parseFrom([B)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;
    .locals 1
    .param p0    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->access$100()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;

    invoke-static {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;->access$000(Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Builder;)Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getCommand()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    return-object v0
.end method

.method public final getContext()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    return-object v0
.end method

.method public final getDismissReason()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    return-object v0
.end method

.method public final getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    return-object v0
.end method

.method public final getNotificationType()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;->getNumber()I

    move-result v2

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    invoke-virtual {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    invoke-virtual {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    invoke-virtual {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    invoke-virtual {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getStatus()Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    return-object v0
.end method

.method public final hasCommand()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasContext()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDismissReason()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutType()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNotificationType()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStatus()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedIsInitialized:B

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hasContext()Z

    move-result v3

    if-nez v3, :cond_2

    iput-byte v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-virtual {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_3

    iput-byte v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_3
    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->getSerializedSize()I

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->context_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->status_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;

    invoke-virtual {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Status;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->command_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$Command;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->notificationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$NotificationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_3
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$HangoutType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_4
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification;->dismissReason_:Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutInviteNotification$DismissReason;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_5
    return-void
.end method
