.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HangoutStartContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Invitation"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private invitationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

.field private inviterGaiaId_:Ljava/lang/Object;

.field private inviterProfileName_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private phoneNumber_:Ljava/lang/Object;

.field private shouldAutoAccept_:Z

.field private timestamp_:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;-><init>()V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->timestamp_:J

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterGaiaId_:Ljava/lang/Object;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->invitationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterProfileName_:Ljava/lang/Object;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->shouldAutoAccept_:Z

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->phoneNumber_:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;)V
    .locals 2
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    invoke-direct {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;J)J
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->timestamp_:J

    return-wide p1
.end method

.method static synthetic access$402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterGaiaId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->invitationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    return-object p1
.end method

.method static synthetic access$602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterProfileName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;Z)Z
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->shouldAutoAccept_:Z

    return p1
.end method

.method static synthetic access$802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->phoneNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;I)I
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .param p1    # I

    iput p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .locals 1

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    return-object v0
.end method

.method private getInviterGaiaIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterGaiaId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterGaiaId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getInviterProfileNameBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterProfileName_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterProfileName_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getPhoneNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->phoneNumber_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->phoneNumber_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->access$100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;
    .locals 1
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->access$100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getInvitationType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->invitationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    return-object v0
.end method

.method public final getInviterGaiaId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterGaiaId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterGaiaId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getInviterProfileName()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterProfileName_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->inviterProfileName_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getPhoneNumber()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->phoneNumber_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->phoneNumber_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->timestamp_:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterGaiaIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_3

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->invitationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    invoke-virtual {v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->getNumber()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterProfileNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->shouldAutoAccept_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    const/4 v2, 0x6

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getShouldAutoAccept()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->shouldAutoAccept_:Z

    return v0
.end method

.method public final getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->timestamp_:J

    return-wide v0
.end method

.method public final hasInvitationType()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviterGaiaId()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviterProfileName()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhoneNumber()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasShouldAutoAccept()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTimestamp()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedIsInitialized:B

    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->hasTimestamp()Z

    move-result v3

    if-nez v3, :cond_2

    iput-byte v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->hasInviterGaiaId()Z

    move-result v3

    if-nez v3, :cond_3

    iput-byte v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedIsInitialized:B

    move v1, v2

    goto :goto_0

    :cond_3
    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getSerializedSize()I

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->timestamp_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterGaiaIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->invitationType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getInviterProfileNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->shouldAutoAccept_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_5
    return-void
.end method
