.class public final enum Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
.super Ljava/lang/Enum;
.source "HangoutStartContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InvitationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

.field public static final enum HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

.field public static final enum HANGOUT_SYNC:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

.field public static final enum TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    const-string v1, "HANGOUT"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    const-string v1, "HANGOUT_SYNC"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT_SYNC:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    const-string v1, "TRANSFER"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT_SYNC:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->$VALUES:[Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType$1;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType$1;-><init>()V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->HANGOUT_SYNC:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->TRANSFER:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    return-object v0
.end method

.method public static values()[Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;
    .locals 1

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->$VALUES:[Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    invoke-virtual {v0}, [Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;->value:I

    return v0
.end method
