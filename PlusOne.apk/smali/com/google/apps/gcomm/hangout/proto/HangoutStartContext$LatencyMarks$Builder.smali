.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "HangoutStartContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;",
        "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private clientLaunch_:J

.field private serverCreateRedirectEnd_:J

.field private serverCreateRoomEnd_:J

.field private serverCreateRoomStart_:J


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 1

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;-><init>()V

    return-object v0
.end method

.method private clear()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 3

    const-wide/16 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clientLaunch_:J

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomStart_:J

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomEnd_:J

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRedirectEnd_:J

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    return-object p0
.end method

.method private clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 2

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clientLaunch_:J

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomStart_:J

    goto :goto_0

    :sswitch_3
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomEnd_:J

    goto :goto_0

    :sswitch_4
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRedirectEnd_:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .locals 5

    new-instance v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;B)V

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-wide v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clientLaunch_:J

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J
    invoke-static {v1, v3, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->access$1302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-wide v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomStart_:J

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J
    invoke-static {v1, v3, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->access$1402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-wide v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomEnd_:J

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J
    invoke-static {v1, v3, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->access$1502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-wide v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRedirectEnd_:J

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J
    invoke-static {v1, v3, v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->access$1602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J

    # setter for: Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->access$1702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;I)I

    return-object v1
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->buildPartial()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clear()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clone()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 3
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->hasClientLaunch()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getClientLaunch()J

    move-result-wide v0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->clientLaunch_:J

    :cond_2
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->hasServerCreateRoomStart()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getServerCreateRoomStart()J

    move-result-wide v0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomStart_:J

    :cond_3
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->hasServerCreateRoomEnd()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getServerCreateRoomEnd()J

    move-result-wide v0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRoomEnd_:J

    :cond_4
    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->hasServerCreateRedirectEnd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getServerCreateRedirectEnd()J

    move-result-wide v0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->bitField0_:I

    iput-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->serverCreateRedirectEnd_:J

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method
