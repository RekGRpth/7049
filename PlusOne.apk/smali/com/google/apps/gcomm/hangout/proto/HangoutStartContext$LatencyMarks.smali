.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HangoutStartContext.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LatencyMarks"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private clientLaunch_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private serverCreateRedirectEnd_:J

.field private serverCreateRoomEnd_:J

.field private serverCreateRoomStart_:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const-wide/16 v1, 0x0

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;-><init>()V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    iput-wide v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;)V
    .locals 2
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    invoke-direct {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;)V

    return-void
.end method

.method static synthetic access$1302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    return-wide p1
.end method

.method static synthetic access$1402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    return-wide p1
.end method

.method static synthetic access$1502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    return-wide p1
.end method

.method static synthetic access$1602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;J)J
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    return-wide p1
.end method

.method static synthetic access$1702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;I)I
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .param p1    # I

    iput p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .locals 1

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;
    .locals 1
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->access$1100()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getClientLaunch()J
    .locals 2

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    return-wide v0
.end method

.method public final getSerializedSize()I
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v4, :cond_1

    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    invoke-static {v5, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_3

    const/4 v2, 0x3

    iget-wide v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    iget-wide v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    invoke-static {v6, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getServerCreateRedirectEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    return-wide v0
.end method

.method public final getServerCreateRoomEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    return-wide v0
.end method

.method public final getServerCreateRoomStart()J
    .locals 2

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    return-wide v0
.end method

.method public final hasClientLaunch()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasServerCreateRedirectEnd()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasServerCreateRoomEnd()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasServerCreateRoomStart()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getSerializedSize()I

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->clientLaunch_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_0
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomStart_:J

    invoke-virtual {p1, v3, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    iget-wide v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRoomEnd_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_2
    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-wide v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->serverCreateRedirectEnd_:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_3
    return-void
.end method
