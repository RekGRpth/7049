.class public final Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "HangoutStartContext.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$InvitationType;,
        Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

.field private static final serialVersionUID:J


# instance fields
.field private activityId_:Ljava/lang/Object;

.field private appData_:Ljava/lang/Object;

.field private appId_:Ljava/lang/Object;

.field private bitField0_:I

.field private callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

.field private circleId_:Ljava/lang/Object;

.field private contextId_:Ljava/lang/Object;

.field private create_:Z

.field private dEPRECATEDCallback_:Z

.field private externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

.field private flippy_:Z

.field private hangoutId_:Ljava/lang/Object;

.field private hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

.field private invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private nick_:Ljava/lang/Object;

.field private profileId_:Lcom/google/protobuf/LazyStringList;

.field private referringUrl_:Ljava/lang/Object;

.field private shouldAutoInvite_:Z

.field private shouldMuteVideo_:Z

.field private source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

.field private topic_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-direct {v0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;-><init>()V

    sput-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->REGULAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;

    iput-boolean v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->flippy_:Z

    iput-boolean v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->dEPRECATEDCallback_:Z

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;->SANDBAR:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    move-result-object v1

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    iput-boolean v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->create_:Z

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    move-result-object v1

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    sget-object v1, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;->NONE:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    move-result-object v1

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    iput-boolean v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldAutoInvite_:Z

    const-string v1, ""

    iput-object v1, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;

    iput-boolean v2, v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldMuteVideo_:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;)V
    .locals 2
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    invoke-direct {p0, p1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;-><init>(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;)V

    return-void
.end method

.method static synthetic access$2702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    return-object p1
.end method

.method static synthetic access$2902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3002(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3102(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$3302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->flippy_:Z

    return p1
.end method

.method static synthetic access$3702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->dEPRECATEDCallback_:Z

    return p1
.end method

.method static synthetic access$3802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    return-object p1
.end method

.method static synthetic access$3902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    return-object p1
.end method

.method static synthetic access$4002(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->create_:Z

    return p1
.end method

.method static synthetic access$4102(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4202(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    return-object p1
.end method

.method static synthetic access$4302(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    return-object p1
.end method

.method static synthetic access$4402(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;)Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$4602(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldAutoInvite_:Z

    return p1
.end method

.method static synthetic access$4702(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$4802(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;Z)Z
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldMuteVideo_:Z

    return p1
.end method

.method static synthetic access$4902(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;I)I
    .locals 0
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .param p1    # I

    iput p1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    return p1
.end method

.method private getActivityIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getAppDataBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getAppIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getCircleIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getContextIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;
    .locals 1

    sget-object v0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->defaultInstance:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    return-object v0
.end method

.method private getHangoutIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getNickBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getReferringUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method private getTopicBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 1

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->access$2500()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;
    .locals 1
    .param p0    # Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;

    invoke-static {}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->access$2500()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;->mergeFrom(Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;)Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getActivityId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->activityId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getAppData()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appData_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getAppId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->appId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getCallback()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    return-object v0
.end method

.method public final getCircleId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->circleId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getContextId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->contextId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getCreate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->create_:Z

    return v0
.end method

.method public final getDEPRECATEDCallback()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->dEPRECATEDCallback_:Z

    return v0
.end method

.method public final getExternalKey()Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    return-object v0
.end method

.method public final getFlippy()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->flippy_:Z

    return v0
.end method

.method public final getHangoutId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getHangoutType()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    return-object v0
.end method

.method public final getInvitation()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    return-object v0
.end method

.method public final getLatencyMarks()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    return-object v0
.end method

.method public final getNick()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->nick_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getReferringUrl()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->referringUrl_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 10

    const/16 v9, 0x10

    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    iget v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/lit8 v2, v4, 0x0

    :cond_1
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    if-ne v4, v6, :cond_2

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    invoke-virtual {v4}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->getNumber()I

    move-result v4

    invoke-static {v6, v4}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_2
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x4

    if-ne v4, v7, :cond_3

    const/4 v4, 0x3

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getTopicBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x8

    if-ne v4, v8, :cond_4

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getReferringUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v7, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_4
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x10

    if-ne v4, v9, :cond_5

    const/4 v4, 0x5

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getCircleIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_5
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_6

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_6
    add-int/2addr v2, v0

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_7

    const/4 v4, 0x7

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getActivityIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_7
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v4, v4, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_8

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getAppIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v8, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_8
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_9

    const/16 v4, 0x9

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getAppDataBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_9
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_a

    const/16 v4, 0xa

    iget-boolean v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->flippy_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_a
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_b

    const/16 v4, 0xb

    iget-boolean v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->dEPRECATEDCallback_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_b
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x400

    const/16 v5, 0x400

    if-ne v4, v5, :cond_c

    const/16 v4, 0xc

    iget-object v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    invoke-virtual {v5}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_c
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x800

    const/16 v5, 0x800

    if-ne v4, v5, :cond_d

    const/16 v4, 0xd

    iget-object v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_d
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x1000

    const/16 v5, 0x1000

    if-ne v4, v5, :cond_e

    const/16 v4, 0xe

    iget-boolean v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->create_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_e
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x2000

    const/16 v5, 0x2000

    if-ne v4, v5, :cond_f

    const/16 v4, 0xf

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNickBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_f
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v4, v4, 0x4000

    const/16 v5, 0x4000

    if-ne v4, v5, :cond_10

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-static {v9, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_10
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const v5, 0x8000

    and-int/2addr v4, v5

    const v5, 0x8000

    if-ne v4, v5, :cond_11

    const/16 v4, 0x11

    iget-object v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    invoke-virtual {v5}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;->getNumber()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v4

    add-int/2addr v2, v4

    :cond_11
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    const/high16 v5, 0x10000

    if-ne v4, v5, :cond_12

    const/16 v4, 0x12

    iget-object v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_12
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_13

    const/16 v5, 0x13

    iget-object v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_13
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v5, 0x20000

    and-int/2addr v4, v5

    const/high16 v5, 0x20000

    if-ne v4, v5, :cond_14

    const/16 v4, 0x14

    iget-boolean v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldAutoInvite_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_14
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v5, 0x40000

    and-int/2addr v4, v5

    const/high16 v5, 0x40000

    if-ne v4, v5, :cond_15

    const/16 v4, 0x15

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getContextIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_15
    iget v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v5, 0x80000

    and-int/2addr v4, v5

    const/high16 v5, 0x80000

    if-ne v4, v5, :cond_16

    const/16 v4, 0x16

    iget-boolean v5, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldMuteVideo_:Z

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v4

    add-int/2addr v2, v4

    :cond_16
    iput v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedSerializedSize:I

    move v3, v2

    goto/16 :goto_0
.end method

.method public final getShouldAutoInvite()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldAutoInvite_:Z

    return v0
.end method

.method public final getShouldMuteVideo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldMuteVideo_:Z

    return v0
.end method

.method public final getSource()Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;
    .locals 1

    iget-object v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    return-object v0
.end method

.method public final getTopic()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->topic_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final hasActivityId()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAppData()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAppId()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCallback()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCircleId()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasContextId()Z
    .locals 2

    const/high16 v1, 0x40000

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCreate()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDEPRECATEDCallback()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasExternalKey()Z
    .locals 2

    const/high16 v1, 0x10000

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFlippy()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutType()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInvitation()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLatencyMarks()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNick()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReferringUrl()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasShouldAutoInvite()Z
    .locals 2

    const/high16 v1, 0x20000

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasShouldMuteVideo()Z
    .locals 2

    const/high16 v1, 0x80000

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSource()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTopic()Z
    .locals 2

    iget v0, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-byte v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    if-ne v1, v3, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    move v2, v4

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasHangoutId()Z

    move-result v2

    if-nez v2, :cond_2

    iput-byte v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    move v2, v4

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasHangoutType()Z

    move-result v2

    if-nez v2, :cond_3

    iput-byte v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    move v2, v4

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasInvitation()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_4

    iput-byte v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    move v2, v4

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hasExternalKey()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_5

    iput-byte v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    move v2, v4

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_7

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitee;->isInitialized()Z

    move-result v2

    if-nez v2, :cond_6

    iput-byte v4, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    move v2, v4

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iput-byte v3, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->memoizedIsInitialized:B

    move v2, v3

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 7
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getSerializedSize()I

    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getHangoutIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->hangoutType_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;

    invoke-virtual {v1}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_2

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getTopicBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_3

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getReferringUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v6, :cond_4

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getCircleIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->profileId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getActivityIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_6
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getAppIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v5, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_7
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getAppDataBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_8
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->flippy_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_9
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->dEPRECATEDCallback_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_a
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->source_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Source;->getNumber()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_b
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitation_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$Invitation;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_c
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->create_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_d
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_e

    const/16 v1, 0xf

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getNickBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_e
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_f

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->latencyMarks_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$LatencyMarks;

    invoke-virtual {p1, v6, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_f
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_10

    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->callback_:Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;

    invoke-virtual {v2}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext$CallbackType;->getNumber()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_10
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v2, 0x10000

    and-int/2addr v1, v2

    const/high16 v2, 0x10000

    if-ne v1, v2, :cond_11

    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->externalKey_:Lcom/google/apps/gcomm/hangout/proto/ExternalEntityKey;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_11
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_12

    const/16 v2, 0x13

    iget-object v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->invitee_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_12
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v2, 0x20000

    and-int/2addr v1, v2

    const/high16 v2, 0x20000

    if-ne v1, v2, :cond_13

    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldAutoInvite_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_13
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v2, 0x40000

    and-int/2addr v1, v2

    const/high16 v2, 0x40000

    if-ne v1, v2, :cond_14

    const/16 v1, 0x15

    invoke-direct {p0}, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->getContextIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_14
    iget v1, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->bitField0_:I

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    const/high16 v2, 0x80000

    if-ne v1, v2, :cond_15

    const/16 v1, 0x16

    iget-boolean v2, p0, Lcom/google/apps/gcomm/hangout/proto/HangoutStartContext;->shouldMuteVideo_:Z

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_15
    return-void
.end method
