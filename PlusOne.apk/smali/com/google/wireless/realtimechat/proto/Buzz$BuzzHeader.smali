.class public final Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeaderOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuzzHeader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

.field private static final serialVersionUID:J


# instance fields
.field private actAsPrimary_:Z

.field private alreadySentToPrimary_:Z

.field private bitField0_:I

.field private countForReliabilityTest_:Z

.field private destinationPayloadsSetSender_:Z

.field private dropIfNoEndpoint_:Z

.field private dropIfNoResource_:Z

.field private individuallyRoutedPayload_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private secondaryPayload_:Ljava/lang/Object;

.field private stateUpdate_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->stateUpdate_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->alreadySentToPrimary_:Z

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoEndpoint_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->actAsPrimary_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoResource_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->individuallyRoutedPayload_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->countForReliabilityTest_:Z

    iput-boolean v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->destinationPayloadsSetSender_:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;)V

    return-void
.end method

.method static synthetic access$2302(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->stateUpdate_:Z

    return p1
.end method

.method static synthetic access$2402(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->alreadySentToPrimary_:Z

    return p1
.end method

.method static synthetic access$2502(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$2602(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoEndpoint_:Z

    return p1
.end method

.method static synthetic access$2702(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->actAsPrimary_:Z

    return p1
.end method

.method static synthetic access$2802(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoResource_:Z

    return p1
.end method

.method static synthetic access$2902(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->individuallyRoutedPayload_:Z

    return p1
.end method

.method static synthetic access$3002(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->countForReliabilityTest_:Z

    return p1
.end method

.method static synthetic access$3102(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->destinationPayloadsSetSender_:Z

    return p1
.end method

.method static synthetic access$3202(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-object v0
.end method

.method private getSecondaryPayloadBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->access$2100()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->access$2100()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getActAsPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->actAsPrimary_:Z

    return v0
.end method

.method public final getAlreadySentToPrimary()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->alreadySentToPrimary_:Z

    return v0
.end method

.method public final getCountForReliabilityTest()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->countForReliabilityTest_:Z

    return v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-object v0
.end method

.method public final getDestinationPayloadsSetSender()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->destinationPayloadsSetSender_:Z

    return v0
.end method

.method public final getDropIfNoEndpoint()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoEndpoint_:Z

    return v0
.end method

.method public final getDropIfNoResource()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoResource_:Z

    return v0
.end method

.method public final getIndividuallyRoutedPayload()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->individuallyRoutedPayload_:Z

    return v0
.end method

.method public final getSecondaryPayload()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->secondaryPayload_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->stateUpdate_:Z

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->alreadySentToPrimary_:Z

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_3

    const/4 v2, 0x6

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getSecondaryPayloadBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v5, :cond_4

    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoEndpoint_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->actAsPrimary_:Z

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    const/16 v2, 0x9

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoResource_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->individuallyRoutedPayload_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->countForReliabilityTest_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_9

    const/16 v2, 0xc

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->destinationPayloadsSetSender_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedSerializedSize:I

    move v1, v0

    goto/16 :goto_0
.end method

.method public final getStateUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->stateUpdate_:Z

    return v0
.end method

.method public final hasActAsPrimary()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAlreadySentToPrimary()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCountForReliabilityTest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDestinationPayloadsSetSender()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDropIfNoEndpoint()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDropIfNoResource()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasIndividuallyRoutedPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSecondaryPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStateUpdate()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v3, 0x8

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->stateUpdate_:Z

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->alreadySentToPrimary_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getSecondaryPayloadBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v3, :cond_3

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoEndpoint_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_3
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->actAsPrimary_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->dropIfNoResource_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_5
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->individuallyRoutedPayload_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->countForReliabilityTest_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->destinationPayloadsSetSender_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_8
    return-void
.end method
