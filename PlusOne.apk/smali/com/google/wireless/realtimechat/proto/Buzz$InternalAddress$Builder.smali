.class public final Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddressOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddressOrBuilder;"
    }
.end annotation


# instance fields
.field private addressable_:Z

.field private bitField0_:I

.field private gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

.field private jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    return-void
.end method

.method static synthetic access$1400()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->hasJID()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->getJID()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;

    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readGroup(ILcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->setJID(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->hasGateway()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->getGateway()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;

    :cond_2
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readGroup(ILcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->setGateway(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    goto :goto_0

    :sswitch_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x2b -> :sswitch_2
        0x40 -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->access$1602(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->access$1702(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->addressable_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->access$1802(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;Z)Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->access$1902(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearAddressable()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    return-object p0
.end method

.method public final clearGateway()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearJID()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAddressable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    return v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    return-object v0
.end method

.method public final getGateway()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    return-object v0
.end method

.method public final getJID()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    return-object v0
.end method

.method public final hasAddressable()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGateway()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasJID()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 3
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->hasJID()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getJID()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v2

    if-eq v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;->newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    :goto_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->hasGateway()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getGateway()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;->newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    :goto_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->hasAddressable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getAddressable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->setAddressable(Z)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    goto :goto_0

    :cond_4
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    goto :goto_1

    :cond_5
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    goto :goto_2
.end method

.method public final setAddressable(Z)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->addressable_:Z

    return-object p0
.end method

.method public final setGateway(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway$Builder;->build()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setGateway(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->gateway_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Gateway;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setJID(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID$Builder;->build()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setJID(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->jID_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$JID;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->bitField0_:I

    return-object p0
.end method
