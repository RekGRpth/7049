.class public final Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientDataOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientDataOrBuilder;"
    }
.end annotation


# instance fields
.field private address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

.field private bitField0_:I

.field private gaiaId_:J

.field private payload_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$4600()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 5

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v2

    if-eq v1, v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->hasAddress()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getAddress()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v4

    if-eq v3, v4, :cond_3

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    :goto_0
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->hasGaiaId()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getGaiaId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->setGaiaId(J)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    :cond_1
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->hasPayload()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getPayload()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->setPayload(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    :cond_2
    return-object v0

    :cond_3
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    goto :goto_0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->hasAddress()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->getAddress()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->setAddress(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    goto :goto_0

    :sswitch_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->gaiaId_:J

    goto :goto_0

    :sswitch_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->access$4802(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->gaiaId_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->gaiaId_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->access$4902(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;J)J

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->payload_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->access$5002(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;Ljava/lang/Object;)Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->access$5102(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->gaiaId_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearAddress()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearGaiaId()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->gaiaId_:J

    return-object p0
.end method

.method public final clearPayload()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getPayload()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAddress()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData;

    move-result-object v0

    return-object v0
.end method

.method public final getGaiaId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->gaiaId_:J

    return-wide v0
.end method

.method public final getPayload()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final hasAddress()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGaiaId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPayload()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setAddress(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->build()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setAddress(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->address_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setGaiaId(J)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->gaiaId_:J

    return-object p0
.end method

.method public final setPayload(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingRecipientData$Builder;->payload_:Ljava/lang/Object;

    return-object p0
.end method
