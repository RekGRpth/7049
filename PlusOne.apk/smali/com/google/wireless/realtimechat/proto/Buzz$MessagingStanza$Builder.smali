.class public final Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Buzz.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanzaOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanzaOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

.field private compressionType_:I

.field private recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

.field private sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

.field private xmppStanza_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-void
.end method

.method private buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;-><init>(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->access$3602(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->access$3702(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->xmppStanza_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->access$3802(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->compressionType_:I

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->compressionType_:I
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->access$3902(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;I)I

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->access$4002(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    # setter for: Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->access$4102(Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;I)I

    return-object v1
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 5

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v2

    if-eq v1, v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->hasRecipient()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getRecipient()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v4

    if-eq v3, v4, :cond_5

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    :goto_0
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    :cond_0
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->hasSender()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getSender()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v4

    if-eq v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    invoke-static {v3}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    :goto_1
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    :cond_1
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->hasXmppStanza()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getXmppStanza()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->setXmppStanza(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    :cond_2
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->hasCompressionType()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getCompressionType()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->setCompressionType(I)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    :cond_3
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->hasBuzzHeader()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getBuzzHeader()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v1

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_7

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v3

    if-eq v2, v3, :cond_7

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    invoke-static {v2}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->newBuilder(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    :goto_2
    iget v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    :cond_4
    return-object v0

    :cond_5
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    goto/16 :goto_0

    :cond_6
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    goto :goto_1

    :cond_7
    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    goto :goto_2
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->hasRecipient()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->getRecipient()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->setRecipient(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->hasSender()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->getSender()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->setSender(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    goto :goto_0

    :sswitch_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->newBuilder()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->hasBuzzHeader()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->getBuzzHeader()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->setBuzzHeader(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    goto :goto_0

    :sswitch_5
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->compressionType_:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->compressionType_:I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearBuzzHeader()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearCompressionType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->compressionType_:I

    return-object p0
.end method

.method public final clearRecipient()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearSender()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearXmppStanza()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getXmppStanza()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getBuzzHeader()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    return-object v0
.end method

.method public final getCompressionType()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->compressionType_:I

    return v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza;

    move-result-object v0

    return-object v0
.end method

.method public final getRecipient()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object v0
.end method

.method public final getSender()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    return-object v0
.end method

.method public final getXmppStanza()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final hasBuzzHeader()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCompressionType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasRecipient()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSender()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasXmppStanza()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setBuzzHeader(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader$Builder;->build()Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setBuzzHeader(Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->buzzHeader_:Lcom/google/wireless/realtimechat/proto/Buzz$BuzzHeader;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setCompressionType(I)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->compressionType_:I

    return-object p0
.end method

.method public final setRecipient(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->build()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setRecipient(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->recipient_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setSender(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress$Builder;->build()Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setSender(Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->sender_:Lcom/google/wireless/realtimechat/proto/Buzz$InternalAddress;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setXmppStanza(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Buzz$MessagingStanza$Builder;->xmppStanza_:Ljava/lang/Object;

    return-object p0
.end method
