.class public final Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BatchCommandOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;",
        "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$BatchCommandOrBuilder;"
    }
.end annotation


# instance fields
.field private androidNewUrl_:Ljava/lang/Object;

.field private bitField0_:I

.field private clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private clientVersion_:Ljava/lang/Object;

.field private command_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;"
        }
    .end annotation
.end field

.field private iphoneNewUrl_:Ljava/lang/Object;

.field private isAndroidDeprecated_:Z

.field private isIphoneDeprecated_:Z

.field private request_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation
.end field

.field private response_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;"
        }
    .end annotation
.end field

.field private stateUpdate_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$80300(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .locals 2
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v0}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method static synthetic access$80400()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;-><init>()V

    return-object v0
.end method

.method private buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80602(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80702(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80802(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80902(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    const/4 v2, 0x1

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersion_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81002(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x2

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81102(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion;

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x4

    :cond_6
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isIphoneDeprecated_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81202(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Z)Z

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit8 v2, v2, 0x8

    :cond_7
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isAndroidDeprecated_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81302(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Z)Z

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit8 v2, v2, 0x10

    :cond_8
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->iphoneNewUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81402(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit8 v2, v2, 0x20

    :cond_9
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->androidNewUrl_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81502(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;Ljava/lang/Object;)Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$81602(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;I)I

    return-object v1
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 5

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;-><init>()V

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v2

    if-eq v1, v2, :cond_9

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_a

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x2

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_0
    :goto_0
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_1
    :goto_1
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_c

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_2
    :goto_2
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_d

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x9

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_3
    :goto_3
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasClientVersion()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setClientVersion(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    :cond_4
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasClientVersionMessage()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    iget v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_e

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v4

    if-eq v3, v4, :cond_e

    iget-object v3, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {v3}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    :goto_4
    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_5
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasIsIphoneDeprecated()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIsIphoneDeprecated()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setIsIphoneDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    :cond_6
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasIsAndroidDeprecated()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIsAndroidDeprecated()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setIsAndroidDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    :cond_7
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasIphoneNewUrl()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIphoneNewUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setIphoneNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    :cond_8
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->hasAndroidNewUrl()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getAndroidNewUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setAndroidNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    :cond_9
    return-object v0

    :cond_a
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->command_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80600(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    :cond_b
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->request_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80700(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_c
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->response_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80800(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_d
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->stateUpdate_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->access$80900(Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_e
    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    goto :goto_4
.end method

.method private ensureCommandIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureRequestIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureResponseIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureStateUpdateIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v2

    if-nez v2, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    :sswitch_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->newBuilder()Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->hasClientVersionMessage()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->mergeFrom(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->buildPartial()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->addStateUpdate(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    goto :goto_0

    :sswitch_7
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    goto :goto_0

    :sswitch_8
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x80

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    goto/16 :goto_0

    :sswitch_9
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x100

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v2, v2, 0x200

    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch
.end method


# virtual methods
.method public final addAllCommand(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllRequest(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllResponse(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllStateUpdate(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addCommand(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addRequest(Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addResponse(Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addStateUpdate(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addStateUpdate(Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .locals 2

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearAndroidNewUrl()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getAndroidNewUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearClientVersion()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getClientVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearClientVersionMessage()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearCommand()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearIphoneNewUrl()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getIphoneNewUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearIsAndroidDeprecated()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    return-object p0
.end method

.method public final clearIsIphoneDeprecated()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    return-object p0
.end method

.method public final clearRequest()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearResponse()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearStateUpdate()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAndroidNewUrl()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getClientVersion()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getClientVersionMessage()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method public final getCommand(I)Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    return-object v0
.end method

.method public final getCommandCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getCommandList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BatchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final getIphoneNewUrl()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getIsAndroidDeprecated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    return v0
.end method

.method public final getIsIphoneDeprecated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    return v0
.end method

.method public final getRequest(I)Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    return-object v0
.end method

.method public final getRequestCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getRequestList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getResponse(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    return-object v0
.end method

.method public final getResponseCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getResponseList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getStateUpdate(I)Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    return-object v0
.end method

.method public final getStateUpdateCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getStateUpdateList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final hasAndroidNewUrl()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasClientVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasClientVersionMessage()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasIphoneNewUrl()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasIsAndroidDeprecated()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasIsIphoneDeprecated()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setAndroidNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->androidNewUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setClientVersion(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersion_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/webapps/Version$ClientVersion$Builder;->build()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setClientVersionMessage(Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->clientVersionMessage_:Lcom/google/wireless/webapps/Version$ClientVersion;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setCommand(ILcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureCommandIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->command_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setIphoneNewUrl(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->iphoneNewUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setIsAndroidDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isAndroidDeprecated_:Z

    return-object p0
.end method

.method public final setIsIphoneDeprecated(Z)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->isIphoneDeprecated_:Z

    return-object p0
.end method

.method public final setRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setRequest(ILcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchClientRequest;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureRequestIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->request_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setResponse(ILcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerResponse;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureResponseIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->response_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setStateUpdate(ILcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;)Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$BunchServerStateUpdate;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->ensureStateUpdateIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BatchCommand$Builder;->stateUpdate_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
