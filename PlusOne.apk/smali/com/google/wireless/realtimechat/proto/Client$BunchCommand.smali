.class public final Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$BunchCommandOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BunchCommand"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private bitField1_:I

.field private bitField2_:I

.field private chatMessageRequest_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

.field private chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

.field private chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

.field private conversationJoinRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

.field private conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

.field private conversationListRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

.field private conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

.field private conversationPreferenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

.field private conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

.field private conversationRenameRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

.field private conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

.field private conversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

.field private conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

.field private conversationSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

.field private conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

.field private deviceRegistrationRequest_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

.field private deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

.field private error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

.field private eventSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

.field private eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

.field private eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

.field private eventStreamRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

.field private globalConversationPreferencesRequest_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

.field private globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

.field private groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

.field private hangoutInviteFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

.field private hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

.field private hangoutInviteKeepAliveRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

.field private hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

.field private hangoutInviteReplyRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

.field private hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

.field private hangoutInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

.field private hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

.field private hangoutRingFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

.field private hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

.field private invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

.field private inviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

.field private inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

.field private leaveConversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

.field private leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

.field private membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

.field private pingRequest_:Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

.field private pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

.field private presenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

.field private presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

.field private presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

.field private receiptRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

.field private receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

.field private receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

.field private replyToInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

.field private replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

.field private requestClientId_:Ljava/lang/Object;

.field private setAclsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

.field private setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

.field private suggestionsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

.field private suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

.field private tileEventRequest_:Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

.field private tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

.field private tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

.field private typingRequest_:Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

.field private typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

.field private typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

.field private userCreationRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

.field private userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

.field private userInfoRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

.field private userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->requestClientId_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageRequest_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Presence;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Presence;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingRequest_:Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Typing;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Typing;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Receipt;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventStreamRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationRequest_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingRequest_:Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$PingResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Error;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Error;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventRequest_:Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$TileEvent;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$Migration;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$Migration;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesRequest_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;-><init>(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;)V

    return-void
.end method

.method static synthetic access$64502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->requestClientId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$64602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object p1
.end method

.method static synthetic access$64702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageRequest_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    return-object p1
.end method

.method static synthetic access$64802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    return-object p1
.end method

.method static synthetic access$64902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;)Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    return-object p1
.end method

.method static synthetic access$65002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    return-object p1
.end method

.method static synthetic access$65102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$Presence;)Lcom/google/wireless/realtimechat/proto/Client$Presence;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Presence;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    return-object p1
.end method

.method static synthetic access$65202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;)Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingRequest_:Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    return-object p1
.end method

.method static synthetic access$65302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;)Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    return-object p1
.end method

.method static synthetic access$65402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$Typing;)Lcom/google/wireless/realtimechat/proto/Client$Typing;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Typing;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    return-object p1
.end method

.method static synthetic access$65502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$Receipt;)Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    return-object p1
.end method

.method static synthetic access$65602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;)Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    return-object p1
.end method

.method static synthetic access$65702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    return-object p1
.end method

.method static synthetic access$65802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;)Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    return-object p1
.end method

.method static synthetic access$65902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    return-object p1
.end method

.method static synthetic access$66002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    return-object p1
.end method

.method static synthetic access$66102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventStreamRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    return-object p1
.end method

.method static synthetic access$66202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    return-object p1
.end method

.method static synthetic access$66302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationRequest_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    return-object p1
.end method

.method static synthetic access$66402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    return-object p1
.end method

.method static synthetic access$66502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$PingRequest;)Lcom/google/wireless/realtimechat/proto/Client$PingRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingRequest_:Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    return-object p1
.end method

.method static synthetic access$66602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$PingResponse;)Lcom/google/wireless/realtimechat/proto/Client$PingResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    return-object p1
.end method

.method static synthetic access$66702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;)Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    return-object p1
.end method

.method static synthetic access$66802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    return-object p1
.end method

.method static synthetic access$66902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;)Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    return-object p1
.end method

.method static synthetic access$67002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    return-object p1
.end method

.method static synthetic access$67102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$Error;)Lcom/google/wireless/realtimechat/proto/Client$Error;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Error;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    return-object p1
.end method

.method static synthetic access$67202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;)Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    return-object p1
.end method

.method static synthetic access$67302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;)Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    return-object p1
.end method

.method static synthetic access$67402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;)Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    return-object p1
.end method

.method static synthetic access$67502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;)Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    return-object p1
.end method

.method static synthetic access$67602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    return-object p1
.end method

.method static synthetic access$67702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;)Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    return-object p1
.end method

.method static synthetic access$67802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;)Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventRequest_:Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    return-object p1
.end method

.method static synthetic access$67902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;)Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    return-object p1
.end method

.method static synthetic access$68002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$TileEvent;)Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    return-object p1
.end method

.method static synthetic access$68102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$Migration;)Lcom/google/wireless/realtimechat/proto/Client$Migration;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Migration;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    return-object p1
.end method

.method static synthetic access$68202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;)Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    return-object p1
.end method

.method static synthetic access$68302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    return-object p1
.end method

.method static synthetic access$68402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;)Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    return-object p1
.end method

.method static synthetic access$68502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    return-object p1
.end method

.method static synthetic access$68602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    return-object p1
.end method

.method static synthetic access$68702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    return-object p1
.end method

.method static synthetic access$68802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    return-object p1
.end method

.method static synthetic access$68902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    return-object p1
.end method

.method static synthetic access$69002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    return-object p1
.end method

.method static synthetic access$69102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;)Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    return-object p1
.end method

.method static synthetic access$69202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    return-object p1
.end method

.method static synthetic access$69302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    return-object p1
.end method

.method static synthetic access$69402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;)Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    return-object p1
.end method

.method static synthetic access$69502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;)Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    return-object p1
.end method

.method static synthetic access$69602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    return-object p1
.end method

.method static synthetic access$69702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;)Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    return-object p1
.end method

.method static synthetic access$69802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;)Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesRequest_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    return-object p1
.end method

.method static synthetic access$69902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;)Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    return-object p1
.end method

.method static synthetic access$70002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;)Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    return-object p1
.end method

.method static synthetic access$70102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;)Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    return-object p1
.end method

.method static synthetic access$70202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    return-object p1
.end method

.method static synthetic access$70302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    return-object p1
.end method

.method static synthetic access$70402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    return-object p1
.end method

.method static synthetic access$70502(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    return-object p1
.end method

.method static synthetic access$70602(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    return-object p1
.end method

.method static synthetic access$70702(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    return-object p1
.end method

.method static synthetic access$70802(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    return-object p1
.end method

.method static synthetic access$70902(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    return-object p1
.end method

.method static synthetic access$71002(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;)Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    return-object p1
.end method

.method static synthetic access$71102(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;)Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    return-object p1
.end method

.method static synthetic access$71202(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    return p1
.end method

.method static synthetic access$71302(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    return p1
.end method

.method static synthetic access$71402(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    return-object v0
.end method

.method private getRequestClientIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->requestClientId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->requestClientId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->access$64300()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->access$64300()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;)Lcom/google/wireless/realtimechat/proto/Client$BunchCommand$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getChatMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object v0
.end method

.method public final getChatMessageRequest()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageRequest_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    return-object v0
.end method

.method public final getChatMessageResponse()Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    return-object v0
.end method

.method public final getConversationJoinRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    return-object v0
.end method

.method public final getConversationJoinResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    return-object v0
.end method

.method public final getConversationListRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    return-object v0
.end method

.method public final getConversationListResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    return-object v0
.end method

.method public final getConversationPreferenceRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    return-object v0
.end method

.method public final getConversationPreferenceResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    return-object v0
.end method

.method public final getConversationRenameRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    return-object v0
.end method

.method public final getConversationRenameResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    return-object v0
.end method

.method public final getConversationRequest()Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    return-object v0
.end method

.method public final getConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    return-object v0
.end method

.method public final getConversationSearchRequest()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    return-object v0
.end method

.method public final getConversationSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;

    return-object v0
.end method

.method public final getDeviceRegistrationRequest()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationRequest_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    return-object v0
.end method

.method public final getDeviceRegistrationResponse()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    return-object v0
.end method

.method public final getError()Lcom/google/wireless/realtimechat/proto/Client$Error;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    return-object v0
.end method

.method public final getEventSearchRequest()Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    return-object v0
.end method

.method public final getEventSearchResponse()Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    return-object v0
.end method

.method public final getEventSteamResponse()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    return-object v0
.end method

.method public final getEventStreamRequest()Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventStreamRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    return-object v0
.end method

.method public final getGlobalConversationPreferencesRequest()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesRequest_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    return-object v0
.end method

.method public final getGlobalConversationPreferencesResponse()Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    return-object v0
.end method

.method public final getGroupConversationRename()Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    return-object v0
.end method

.method public final getHangoutInviteFinishRequest()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    return-object v0
.end method

.method public final getHangoutInviteFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    return-object v0
.end method

.method public final getHangoutInviteKeepAliveRequest()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    return-object v0
.end method

.method public final getHangoutInviteKeepAliveResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    return-object v0
.end method

.method public final getHangoutInviteReplyRequest()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    return-object v0
.end method

.method public final getHangoutInviteReplyResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    return-object v0
.end method

.method public final getHangoutInviteRequest()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    return-object v0
.end method

.method public final getHangoutInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    return-object v0
.end method

.method public final getHangoutRingFinishRequest()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    return-object v0
.end method

.method public final getHangoutRingFinishResponse()Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    return-object v0
.end method

.method public final getInvalidateLocalCache()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    return-object v0
.end method

.method public final getInviteRequest()Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    return-object v0
.end method

.method public final getInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    return-object v0
.end method

.method public final getLeaveConversationRequest()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    return-object v0
.end method

.method public final getLeaveConversationResponse()Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    return-object v0
.end method

.method public final getMembershipChange()Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    return-object v0
.end method

.method public final getMigration()Lcom/google/wireless/realtimechat/proto/Client$Migration;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    return-object v0
.end method

.method public final getPingRequest()Lcom/google/wireless/realtimechat/proto/Client$PingRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingRequest_:Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    return-object v0
.end method

.method public final getPingResponse()Lcom/google/wireless/realtimechat/proto/Client$PingResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    return-object v0
.end method

.method public final getPresence()Lcom/google/wireless/realtimechat/proto/Client$Presence;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    return-object v0
.end method

.method public final getPresenceRequest()Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    return-object v0
.end method

.method public final getPresenceResponse()Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    return-object v0
.end method

.method public final getReceipt()Lcom/google/wireless/realtimechat/proto/Client$Receipt;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    return-object v0
.end method

.method public final getReceiptRequest()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    return-object v0
.end method

.method public final getReceiptResponse()Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    return-object v0
.end method

.method public final getReplyToInviteRequest()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    return-object v0
.end method

.method public final getReplyToInviteResponse()Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    return-object v0
.end method

.method public final getRequestClientId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->requestClientId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->requestClientId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 9

    const/high16 v8, 0x10000

    const v7, 0x8000

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/high16 v4, -0x80000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->getRequestClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    invoke-static {v6, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_4

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingRequest_:Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_5

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_6

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_7

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_8

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v2, v8

    if-ne v2, v8, :cond_9

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventStreamRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_a

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_b

    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationRequest_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_c

    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_c
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_d

    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingRequest_:Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_d
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_e

    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_e
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_f

    const/16 v2, 0x10

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_f
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    const/high16 v3, 0x800000

    if-ne v2, v3, :cond_10

    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_10
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    const/high16 v3, 0x1000000

    if-ne v2, v3, :cond_11

    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_11
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    const/high16 v3, 0x2000000

    if-ne v2, v3, :cond_12

    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_12
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    const/high16 v3, 0x4000000

    if-ne v2, v3, :cond_13

    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_13
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    const/high16 v3, 0x8000000

    if-ne v2, v3, :cond_14

    const/16 v2, 0x15

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_14
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_15

    const/16 v2, 0x16

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_15
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_16

    const/16 v2, 0x17

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_16
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_17

    const/16 v2, 0x18

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_17
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_18

    const/16 v2, 0x19

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_18
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_19

    const/16 v2, 0x1a

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_19
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_1a

    const/16 v2, 0x1b

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_1b

    const/16 v2, 0x1c

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1b
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_1c

    const/16 v2, 0x1d

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1c
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_1d

    const/16 v2, 0x1e

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1d
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_1e

    const/16 v2, 0x1f

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1e
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_1f

    const/16 v2, 0x21

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_1f
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_20

    const/16 v2, 0x22

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_20
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x2000

    const/16 v3, 0x2000

    if-ne v2, v3, :cond_21

    const/16 v2, 0x23

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_21
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v2, v2, 0x4000

    const/16 v3, 0x4000

    if-ne v2, v3, :cond_22

    const/16 v2, 0x24

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_22
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v2, v7

    if-ne v2, v7, :cond_23

    const/16 v2, 0x25

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_23
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v2, v8

    if-ne v2, v8, :cond_24

    const/16 v2, 0x26

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_24
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_25

    const/16 v2, 0x27

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_25
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_26

    const/16 v2, 0x28

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_26
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_27

    const/16 v2, 0x29

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_27
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_28

    const/16 v2, 0x2a

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_28
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x20000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000

    if-ne v2, v3, :cond_29

    const/16 v2, 0x2b

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_29
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x40000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000

    if-ne v2, v3, :cond_2a

    const/16 v2, 0x2c

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v3, 0x40000000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000000

    if-ne v2, v3, :cond_2b

    const/16 v2, 0x2d

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2b
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_2c

    const/16 v2, 0x2e

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2c
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_2d

    const/16 v2, 0x2f

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageRequest_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2d
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2e

    const/16 v2, 0x30

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2e
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x800

    const/16 v3, 0x800

    if-ne v2, v3, :cond_2f

    const/16 v2, 0x31

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2f
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v2, v2, 0x1000

    const/16 v3, 0x1000

    if-ne v2, v3, :cond_30

    const/16 v2, 0x32

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_30
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x80000

    and-int/2addr v2, v3

    const/high16 v3, 0x80000

    if-ne v2, v3, :cond_31

    const/16 v2, 0x33

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_31
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x100000

    and-int/2addr v2, v3

    const/high16 v3, 0x100000

    if-ne v2, v3, :cond_32

    const/16 v2, 0x34

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_32
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_33

    const/16 v2, 0x35

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventRequest_:Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_33
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_34

    const/16 v2, 0x36

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_34
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_35

    const/16 v2, 0x37

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_35
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x200000

    and-int/2addr v2, v3

    const/high16 v3, 0x200000

    if-ne v2, v3, :cond_36

    const/16 v2, 0x38

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesRequest_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_36
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x400000

    and-int/2addr v2, v3

    const/high16 v3, 0x400000

    if-ne v2, v3, :cond_37

    const/16 v2, 0x39

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_37
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x800000

    and-int/2addr v2, v3

    const/high16 v3, 0x800000

    if-ne v2, v3, :cond_38

    const/16 v2, 0x3a

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_38
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x1000000

    and-int/2addr v2, v3

    const/high16 v3, 0x1000000

    if-ne v2, v3, :cond_39

    const/16 v2, 0x3b

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_39
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x2000000

    and-int/2addr v2, v3

    const/high16 v3, 0x2000000

    if-ne v2, v3, :cond_3a

    const/16 v2, 0x3c

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x4000000

    and-int/2addr v2, v3

    const/high16 v3, 0x4000000

    if-ne v2, v3, :cond_3b

    const/16 v2, 0x3d

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3b
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x8000000

    and-int/2addr v2, v3

    const/high16 v3, 0x8000000

    if-ne v2, v3, :cond_3c

    const/16 v2, 0x3e

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3c
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x10000000

    and-int/2addr v2, v3

    const/high16 v3, 0x10000000

    if-ne v2, v3, :cond_3d

    const/16 v2, 0x3f

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3d
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x20000000

    and-int/2addr v2, v3

    const/high16 v3, 0x20000000

    if-ne v2, v3, :cond_3e

    const/16 v2, 0x40

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3e
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v3, 0x40000000

    and-int/2addr v2, v3

    const/high16 v3, 0x40000000

    if-ne v2, v3, :cond_3f

    const/16 v2, 0x41

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3f
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v2, v4

    if-ne v2, v4, :cond_40

    const/16 v2, 0x42

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_40
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_41

    const/16 v2, 0x43

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_41
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_42

    const/16 v2, 0x44

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_42
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_43

    const/16 v2, 0x45

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_43
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedSerializedSize:I

    move v1, v0

    goto/16 :goto_0
.end method

.method public final getSetAclsRequest()Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    return-object v0
.end method

.method public final getSetAclsResponse()Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    return-object v0
.end method

.method public final getSuggestionsRequest()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    return-object v0
.end method

.method public final getSuggestionsResponse()Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    return-object v0
.end method

.method public final getTileEvent()Lcom/google/wireless/realtimechat/proto/Client$TileEvent;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    return-object v0
.end method

.method public final getTileEventRequest()Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventRequest_:Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    return-object v0
.end method

.method public final getTileEventResponse()Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    return-object v0
.end method

.method public final getTyping()Lcom/google/wireless/realtimechat/proto/Client$Typing;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    return-object v0
.end method

.method public final getTypingRequest()Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingRequest_:Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    return-object v0
.end method

.method public final getTypingResponse()Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    return-object v0
.end method

.method public final getUserCreationRequest()Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    return-object v0
.end method

.method public final getUserCreationResponse()Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    return-object v0
.end method

.method public final getUserInfoRequest()Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    return-object v0
.end method

.method public final getUserInfoResponse()Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    return-object v0
.end method

.method public final hasChatMessage()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasChatMessageRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasChatMessageResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationJoinRequest()Z
    .locals 2

    const/high16 v1, 0x800000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationJoinResponse()Z
    .locals 2

    const/high16 v1, 0x1000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationListRequest()Z
    .locals 2

    const/high16 v1, 0x400000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationListResponse()Z
    .locals 2

    const/high16 v1, 0x800000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationPreferenceRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationPreferenceResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationRenameRequest()Z
    .locals 2

    const/high16 v1, 0x40000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationRenameResponse()Z
    .locals 2

    const/high16 v1, -0x80000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationResponse()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationSearchRequest()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationSearchResponse()Z
    .locals 2

    const/high16 v1, 0x10000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDeviceRegistrationRequest()Z
    .locals 2

    const/high16 v1, 0x40000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDeviceRegistrationResponse()Z
    .locals 2

    const/high16 v1, 0x80000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasError()Z
    .locals 2

    const/high16 v1, 0x4000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEventSearchRequest()Z
    .locals 2

    const/high16 v1, 0x20000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEventSearchResponse()Z
    .locals 2

    const/high16 v1, 0x40000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEventSteamResponse()Z
    .locals 2

    const/high16 v1, 0x20000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEventStreamRequest()Z
    .locals 2

    const/high16 v1, 0x10000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGlobalConversationPreferencesRequest()Z
    .locals 2

    const/high16 v1, 0x200000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGlobalConversationPreferencesResponse()Z
    .locals 2

    const/high16 v1, 0x400000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasGroupConversationRename()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteFinishRequest()Z
    .locals 2

    const/high16 v1, -0x80000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteFinishResponse()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteKeepAliveRequest()Z
    .locals 2

    const/high16 v1, 0x8000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteKeepAliveResponse()Z
    .locals 2

    const/high16 v1, 0x10000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteReplyRequest()Z
    .locals 2

    const/high16 v1, 0x20000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteReplyResponse()Z
    .locals 2

    const/high16 v1, 0x40000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteRequest()Z
    .locals 2

    const/high16 v1, 0x2000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutInviteResponse()Z
    .locals 2

    const/high16 v1, 0x4000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutRingFinishRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHangoutRingFinishResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInvalidateLocalCache()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviteRequest()Z
    .locals 2

    const/high16 v1, 0x8000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviteResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLeaveConversationRequest()Z
    .locals 2

    const/high16 v1, 0x10000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLeaveConversationResponse()Z
    .locals 2

    const/high16 v1, 0x20000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMembershipChange()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMigration()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPingRequest()Z
    .locals 2

    const/high16 v1, 0x100000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPingResponse()Z
    .locals 2

    const/high16 v1, 0x200000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPresence()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPresenceRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPresenceResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceipt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceiptRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReceiptResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReplyToInviteRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasReplyToInviteResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasRequestClientId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSetAclsRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSetAclsResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSuggestionsRequest()Z
    .locals 2

    const/high16 v1, 0x80000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSuggestionsResponse()Z
    .locals 2

    const/high16 v1, 0x100000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTileEvent()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTileEventRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTileEventResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTyping()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTypingRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTypingResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUserCreationRequest()Z
    .locals 2

    const/high16 v1, 0x1000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUserCreationResponse()Z
    .locals 2

    const/high16 v1, 0x2000000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUserInfoRequest()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUserInfoResponse()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 7
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v6, 0x10000

    const v5, 0x8000

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/high16 v2, -0x80000000

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->getRequestClientIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$PresenceRequest;

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingRequest_:Lcom/google/wireless/realtimechat/proto/Client$TypingRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_4

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receipt_:Lcom/google/wireless/realtimechat/proto/Client$Receipt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_5

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->membershipChange_:Lcom/google/wireless/realtimechat/proto/Client$MembershipChange;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_5
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_6

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_7

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$NewConversationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_8

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventStreamRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_9

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSteamResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_9
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_a

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationRequest_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_a
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_b

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->deviceRegistrationResponse_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_b
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_c

    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingRequest_:Lcom/google/wireless/realtimechat/proto/Client$PingRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_c
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_d

    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->pingResponse_:Lcom/google/wireless/realtimechat/proto/Client$PingResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_d
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_e

    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_e
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_f

    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationListResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationListResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_f
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_10

    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_10
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_11

    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userCreationResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserCreationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_11
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_12

    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->error_:Lcom/google/wireless/realtimechat/proto/Client$Error;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_12
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_13

    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$InviteRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_13
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_14

    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationRequest_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_14
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_15

    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->leaveConversationResponse_:Lcom/google/wireless/realtimechat/proto/Client$LeaveConversationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_15
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_16

    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->groupConversationRename_:Lcom/google/wireless/realtimechat/proto/Client$GroupConversationRename;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_16
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_17

    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->migration_:Lcom/google/wireless/realtimechat/proto/Client$Migration;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_17
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_18

    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_18
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_19

    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationPreferenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationPreferenceResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_19
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_1a

    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1a
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_1b

    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1b
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_1c

    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->invalidateLocalCache_:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1c
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_1d

    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->inviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1d
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_1e

    const/16 v0, 0x21

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->replyToInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReplyToInviteResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1e
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_1f

    const/16 v0, 0x22

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->setAclsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SetAclsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1f
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_20

    const/16 v0, 0x23

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoRequest_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_20
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_21

    const/16 v0, 0x24

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->userInfoResponse_:Lcom/google/wireless/realtimechat/proto/Client$UserInfoResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_21
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v5

    if-ne v0, v5, :cond_22

    const/16 v0, 0x25

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_22
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_23

    const/16 v0, 0x26

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationSearchResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_23
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_24

    const/16 v0, 0x27

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presenceResponse_:Lcom/google/wireless/realtimechat/proto/Client$PresenceResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_24
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_25

    const/16 v0, 0x28

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->presence_:Lcom/google/wireless/realtimechat/proto/Client$Presence;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_25
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_26

    const/16 v0, 0x29

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typingResponse_:Lcom/google/wireless/realtimechat/proto/Client$TypingResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_26
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_27

    const/16 v0, 0x2a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->typing_:Lcom/google/wireless/realtimechat/proto/Client$Typing;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_27
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000

    if-ne v0, v1, :cond_28

    const/16 v0, 0x2b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchRequest_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_28
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x40000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000

    if-ne v0, v1, :cond_29

    const/16 v0, 0x2c

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->eventSearchResponse_:Lcom/google/wireless/realtimechat/proto/Client$EventSearchResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_29
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    const/high16 v1, 0x40000000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000000

    if-ne v0, v1, :cond_2a

    const/16 v0, 0x2d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2a
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_2b

    const/16 v0, 0x2e

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationRenameResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationRenameResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2b
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2c

    const/16 v0, 0x2f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageRequest_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2c
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2d

    const/16 v0, 0x30

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->chatMessageResponse_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessageResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2d
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_2e

    const/16 v0, 0x31

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptRequest_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2e
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_2f

    const/16 v0, 0x32

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->receiptResponse_:Lcom/google/wireless/realtimechat/proto/Client$ReceiptResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2f
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x80000

    and-int/2addr v0, v1

    const/high16 v1, 0x80000

    if-ne v0, v1, :cond_30

    const/16 v0, 0x33

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsRequest_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_30
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x100000

    and-int/2addr v0, v1

    const/high16 v1, 0x100000

    if-ne v0, v1, :cond_31

    const/16 v0, 0x34

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->suggestionsResponse_:Lcom/google/wireless/realtimechat/proto/Client$SuggestionsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_31
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_32

    const/16 v0, 0x35

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventRequest_:Lcom/google/wireless/realtimechat/proto/Client$TileEventRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_32
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_33

    const/16 v0, 0x36

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEventResponse_:Lcom/google/wireless/realtimechat/proto/Client$TileEventResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_33
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_34

    const/16 v0, 0x37

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->tileEvent_:Lcom/google/wireless/realtimechat/proto/Client$TileEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_34
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x200000

    and-int/2addr v0, v1

    const/high16 v1, 0x200000

    if-ne v0, v1, :cond_35

    const/16 v0, 0x38

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesRequest_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_35
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x400000

    and-int/2addr v0, v1

    const/high16 v1, 0x400000

    if-ne v0, v1, :cond_36

    const/16 v0, 0x39

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->globalConversationPreferencesResponse_:Lcom/google/wireless/realtimechat/proto/Client$GlobalConversationPreferencesResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_36
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x800000

    and-int/2addr v0, v1

    const/high16 v1, 0x800000

    if-ne v0, v1, :cond_37

    const/16 v0, 0x3a

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinRequest_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_37
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x1000000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000000

    if-ne v0, v1, :cond_38

    const/16 v0, 0x3b

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->conversationJoinResponse_:Lcom/google/wireless/realtimechat/proto/Client$ConversationJoinResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_38
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x2000000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000000

    if-ne v0, v1, :cond_39

    const/16 v0, 0x3c

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_39
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x4000000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000000

    if-ne v0, v1, :cond_3a

    const/16 v0, 0x3d

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3a
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x8000000

    and-int/2addr v0, v1

    const/high16 v1, 0x8000000

    if-ne v0, v1, :cond_3b

    const/16 v0, 0x3e

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3b
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x10000000

    and-int/2addr v0, v1

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_3c

    const/16 v0, 0x3f

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteKeepAliveResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteKeepAliveResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3c
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x20000000

    and-int/2addr v0, v1

    const/high16 v1, 0x20000000

    if-ne v0, v1, :cond_3d

    const/16 v0, 0x40

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3d
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    const/high16 v1, 0x40000000

    and-int/2addr v0, v1

    const/high16 v1, 0x40000000

    if-ne v0, v1, :cond_3e

    const/16 v0, 0x41

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteReplyResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteReplyResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3e
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField1_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_3f

    const/16 v0, 0x42

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3f
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_40

    const/16 v0, 0x43

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutInviteFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutInviteFinishResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_40
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_41

    const/16 v0, 0x44

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishRequest_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishRequest;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_41
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->bitField2_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_42

    const/16 v0, 0x45

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$BunchCommand;->hangoutRingFinishResponse_:Lcom/google/wireless/realtimechat/proto/Client$HangoutRingFinishResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_42
    return-void
.end method
