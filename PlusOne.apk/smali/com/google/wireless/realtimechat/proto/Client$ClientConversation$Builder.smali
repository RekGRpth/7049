.class public final Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$ClientConversationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;",
        "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$ClientConversationOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private conversationClientId_:Ljava/lang/Object;

.field private conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

.field private createdAt_:J

.field private firstEventTimestamp_:J

.field private hidden_:Z

.field private id_:Ljava/lang/Object;

.field private inactiveParticipant_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

.field private lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

.field private lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

.field private muted_:Z

.field private name_:Ljava/lang/Object;

.field private needsAccept_:Z

.field private offTheRecord_:Z

.field private participantId_:Lcom/google/protobuf/LazyStringList;

.field private participant_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

.field private unreadCount_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-void
.end method

.method static synthetic access$3300()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureInactiveParticipantIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureParticipantIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureParticipantIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    goto :goto_0

    :sswitch_3
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->offTheRecord_:Z

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIdIsMutable()V

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_5
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x10

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hasLastMessage()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->getLastMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setLastMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto :goto_0

    :sswitch_7
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x20

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->muted_:Z

    goto :goto_0

    :sswitch_8
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x40

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->unreadCount_:J

    goto/16 :goto_0

    :sswitch_9
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->addParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto/16 :goto_0

    :sswitch_a
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x100

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->needsAccept_:Z

    goto/16 :goto_0

    :sswitch_b
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hasInviter()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->getInviter()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    :cond_2
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setInviter(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto/16 :goto_0

    :sswitch_c
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->addInactiveParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto/16 :goto_0

    :sswitch_d
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x400

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->createdAt_:J

    goto/16 :goto_0

    :sswitch_e
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x800

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hidden_:Z

    goto/16 :goto_0

    :sswitch_f
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v4, v4, 0x2000

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_10
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hasConversationMetadata()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->getConversationMetadata()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    :cond_3
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setConversationMetadata(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto/16 :goto_0

    :sswitch_11
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hasLastPreviewEvent()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    :cond_4
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setLastPreviewEvent(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    goto/16 :goto_0

    :sswitch_12
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/high16 v5, 0x10000

    or-int/2addr v4, v5

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->firstEventTimestamp_:J

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x92 -> :sswitch_11
        0x98 -> :sswitch_12
    .end sparse-switch
.end method


# virtual methods
.method public final addAllInactiveParticipant(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllParticipant(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllParticipantId(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addInactiveParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addInactiveParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addInactiveParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addInactiveParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addParticipant(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addParticipantId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 8

    const/high16 v7, 0x20000

    const/high16 v6, 0x10000

    const v5, 0x8000

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;-><init>(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->id_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3502(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3602(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->offTheRecord_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->offTheRecord_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3702(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3802(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x8

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->name_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3902(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x10

    :cond_5
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->muted_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->muted_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4002(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x20

    :cond_6
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->unreadCount_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->unreadCount_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4102(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x81

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_7
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4202(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/util/List;)Ljava/util/List;

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit8 v2, v2, 0x40

    :cond_8
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->needsAccept_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->needsAccept_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4302(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x201

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_9
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4402(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/util/List;)Ljava/util/List;

    and-int/lit16 v3, v0, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x80

    :cond_a
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->createdAt_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->createdAt_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4502(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J

    and-int/lit16 v3, v0, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    or-int/lit16 v2, v2, 0x100

    :cond_b
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hidden_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hidden_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4602(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Z)Z

    and-int/lit16 v3, v0, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    or-int/lit16 v2, v2, 0x200

    :cond_c
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4702(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant;

    and-int/lit16 v3, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    or-int/lit16 v2, v2, 0x400

    :cond_d
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationClientId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4802(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    or-int/lit16 v2, v2, 0x800

    :cond_e
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4902(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    and-int v3, v0, v5

    if-ne v3, v5, :cond_f

    or-int/lit16 v2, v2, 0x1000

    :cond_f
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$5002(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    and-int v3, v0, v6

    if-ne v3, v6, :cond_10

    or-int/lit16 v2, v2, 0x2000

    :cond_10
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->firstEventTimestamp_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->firstEventTimestamp_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$5102(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;J)J

    and-int v3, v0, v7

    if-ne v3, v7, :cond_11

    or-int/lit16 v2, v2, 0x4000

    :cond_11
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$5202(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$5302(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->offTheRecord_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->muted_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->unreadCount_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->needsAccept_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->createdAt_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hidden_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->firstEventTimestamp_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearConversationClientId()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationClientId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearConversationMetadata()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearCreatedAt()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->createdAt_:J

    return-object p0
.end method

.method public final clearFirstEventTimestamp()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->firstEventTimestamp_:J

    return-object p0
.end method

.method public final clearHidden()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hidden_:Z

    return-object p0
.end method

.method public final clearId()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearInactiveParticipant()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearInviter()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearLastMessage()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearMuted()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->muted_:Z

    return-object p0
.end method

.method public final clearName()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearNeedsAccept()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->needsAccept_:Z

    return-object p0
.end method

.method public final clearOffTheRecord()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->offTheRecord_:Z

    return-object p0
.end method

.method public final clearParticipant()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearParticipantId()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearType()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationType;->ONE_TO_ONE:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    return-object p0
.end method

.method public final clearUnreadCount()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->unreadCount_:J

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getConversationClientId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getConversationMetadata()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    return-object v0
.end method

.method public final getCreatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->createdAt_:J

    return-wide v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    return-object v0
.end method

.method public final getFirstEventTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->firstEventTimestamp_:J

    return-wide v0
.end method

.method public final getHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hidden_:Z

    return v0
.end method

.method public final getId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getInactiveParticipant(I)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getInactiveParticipantCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getInactiveParticipantList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getInviter()Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getLastMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    return-object v0
.end method

.method public final getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    return-object v0
.end method

.method public final getMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->muted_:Z

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getNeedsAccept()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->needsAccept_:Z

    return v0
.end method

.method public final getOffTheRecord()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->offTheRecord_:Z

    return v0
.end method

.method public final getParticipant(I)Lcom/google/wireless/realtimechat/proto/Data$Participant;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant;

    return-object v0
.end method

.method public final getParticipantCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getParticipantIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getParticipantList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Data$ConversationType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    return-object v0
.end method

.method public final getUnreadCount()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->unreadCount_:J

    return-wide v0
.end method

.method public final hasConversationClientId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasConversationMetadata()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCreatedAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasFirstEventTimestamp()Z
    .locals 2

    const/high16 v1, 0x10000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasHidden()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasInviter()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLastMessage()Z
    .locals 2

    const/high16 v1, 0x20000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLastPreviewEvent()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMuted()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasName()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasNeedsAccept()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasOffTheRecord()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUnreadCount()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 5
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    const/high16 v4, 0x20000

    const v3, 0x8000

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasType()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getType()Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setType(Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasOffTheRecord()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getOffTheRecord()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setOffTheRecord(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_4
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3800(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3800(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_5
    :goto_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasName()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasMuted()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getMuted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setMuted(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasUnreadCount()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getUnreadCount()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setUnreadCount(J)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_8
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4200(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4200(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_9
    :goto_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasNeedsAccept()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getNeedsAccept()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setNeedsAccept(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_a
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4400(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_15

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4400(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_b
    :goto_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasCreatedAt()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getCreatedAt()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setCreatedAt(J)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_c
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasHidden()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getHidden()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setHidden(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_d
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasInviter()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getInviter()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_16

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v2

    if-eq v1, v2, :cond_16

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Data$Participant;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    :goto_4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_e
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasConversationClientId()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setConversationClientId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_f
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasConversationMetadata()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getConversationMetadata()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_17

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v2

    if-eq v1, v2, :cond_17

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    :goto_5
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_10
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasLastPreviewEvent()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastPreviewEvent()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/2addr v1, v3

    if-ne v1, v3, :cond_18

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v2

    if-eq v1, v2, :cond_18

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    :goto_6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/2addr v0, v3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    :cond_11
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasFirstEventTimestamp()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getFirstEventTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->setFirstEventTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;

    :cond_12
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->hasLastMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->getLastMessage()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    and-int/2addr v1, v4

    if-ne v1, v4, :cond_19

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v2

    if-eq v1, v2, :cond_19

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;->newBuilder(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    :goto_7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/2addr v0, v4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    goto/16 :goto_0

    :cond_13
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participantId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$3800(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_14
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->participant_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4200(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_15
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->inactiveParticipant_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;->access$4400(Lcom/google/wireless/realtimechat/proto/Client$ClientConversation;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_16
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    goto/16 :goto_4

    :cond_17
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    goto/16 :goto_5

    :cond_18
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    goto :goto_6

    :cond_19
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    goto :goto_7
.end method

.method public final setConversationClientId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationClientId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setConversationMetadata(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setConversationMetadata(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->conversationMetadata_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setCreatedAt(J)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->createdAt_:J

    return-object p0
.end method

.method public final setFirstEventTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/high16 v1, 0x10000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->firstEventTimestamp_:J

    return-object p0
.end method

.method public final setHidden(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->hidden_:Z

    return-object p0
.end method

.method public final setId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->id_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setInactiveParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setInactiveParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureInactiveParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inactiveParticipant_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setInviter(Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setInviter(Lcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->inviter_:Lcom/google/wireless/realtimechat/proto/Data$Participant;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setLastMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ChatMessage$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setLastMessage(Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastMessage_:Lcom/google/wireless/realtimechat/proto/Client$ChatMessage;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const/high16 v1, 0x20000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setLastPreviewEvent(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setLastPreviewEvent(Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->lastPreviewEvent_:Lcom/google/wireless/realtimechat/proto/Client$EventStreamResponse$Event;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setMuted(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->muted_:Z

    return-object p0
.end method

.method public final setName(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->name_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setNeedsAccept(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->needsAccept_:Z

    return-object p0
.end method

.method public final setOffTheRecord(Z)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->offTheRecord_:Z

    return-object p0
.end method

.method public final setParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant$Builder;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Participant;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setParticipant(ILcom/google/wireless/realtimechat/proto/Data$Participant;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Data$Participant;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participant_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setParticipantId(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->ensureParticipantIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->participantId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setType(Lcom/google/wireless/realtimechat/proto/Data$ConversationType;)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->type_:Lcom/google/wireless/realtimechat/proto/Data$ConversationType;

    return-object p0
.end method

.method public final setUnreadCount(J)Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ClientConversation$Builder;->unreadCount_:J

    return-object p0
.end method
