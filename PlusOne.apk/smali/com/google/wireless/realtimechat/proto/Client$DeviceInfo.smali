.class public final Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$DeviceInfoOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;,
        Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

.field private static final serialVersionUID:J


# instance fields
.field private androidId_:J

.field private appId_:Ljava/lang/Object;

.field private bitField0_:I

.field private clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private createdAt_:J

.field private enabled_:Z

.field private markedForDeletionAt_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private phoneNumber_:Ljava/lang/Object;

.field private pushEnabled_:Z

.field private token_:Lcom/google/protobuf/ByteString;

.field private type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

.field private updatedAt_:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iput-boolean v4, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->enabled_:Z

    iput-boolean v4, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->pushEnabled_:Z

    sget-object v1, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->token_:Lcom/google/protobuf/ByteString;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;

    iput-wide v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->createdAt_:J

    iput-wide v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->updatedAt_:J

    iput-wide v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->markedForDeletionAt_:J

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;

    iput-wide v2, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->androidId_:J

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;-><init>(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;)V

    return-void
.end method

.method static synthetic access$52602(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    return-object p1
.end method

.method static synthetic access$52702(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->enabled_:Z

    return p1
.end method

.method static synthetic access$52802(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Z)Z
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->pushEnabled_:Z

    return p1
.end method

.method static synthetic access$52902(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->token_:Lcom/google/protobuf/ByteString;

    return-object p1
.end method

.method static synthetic access$53002(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53102(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->createdAt_:J

    return-wide p1
.end method

.method static synthetic access$53202(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->updatedAt_:J

    return-wide p1
.end method

.method static synthetic access$53302(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->markedForDeletionAt_:J

    return-wide p1
.end method

.method static synthetic access$53402(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53502(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->androidId_:J

    return-wide p1
.end method

.method static synthetic access$53602(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object p1
.end method

.method static synthetic access$53702(Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    return p1
.end method

.method private getAppIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    return-object v0
.end method

.method private getPhoneNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;->access$52400()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getAndroidId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->androidId_:J

    return-wide v0
.end method

.method public final getAppId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->appId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getClientVersion()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method public final getCreatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->createdAt_:J

    return-wide v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;

    return-object v0
.end method

.method public final getEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->enabled_:Z

    return v0
.end method

.method public final getMarkedForDeletionAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->markedForDeletionAt_:J

    return-wide v0
.end method

.method public final getPhoneNumber()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->phoneNumber_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getPushEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->pushEnabled_:Z

    return v0
.end method

.method public final getSerializedSize()I
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->getNumber()I

    move-result v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    iget-boolean v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->enabled_:Z

    invoke-static {v4, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v5, :cond_3

    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->pushEnabled_:Z

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_3
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    if-ne v2, v6, :cond_4

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->token_:Lcom/google/protobuf/ByteString;

    invoke-static {v5, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_4
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_5

    const/4 v2, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_5
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_6

    const/4 v2, 0x6

    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->createdAt_:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_6
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_7

    const/4 v2, 0x7

    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->updatedAt_:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_7
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_8

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->markedForDeletionAt_:J

    invoke-static {v6, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_8
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v2, v2, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_9

    const/16 v2, 0x9

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getAppIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_9
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v2, v2, 0x200

    const/16 v3, 0x200

    if-ne v2, v3, :cond_a

    const/16 v2, 0xa

    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->androidId_:J

    invoke-static {v2, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_a
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v2, v2, 0x400

    const/16 v3, 0x400

    if-ne v2, v3, :cond_b

    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v2

    add-int/2addr v0, v2

    :cond_b
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedSerializedSize:I

    move v1, v0

    goto/16 :goto_0
.end method

.method public final getToken()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->token_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    return-object v0
.end method

.method public final getUpdatedAt()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->updatedAt_:J

    return-wide v0
.end method

.method public final hasAndroidId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAppId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasClientVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasCreatedAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEnabled()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasMarkedForDeletionAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhoneNumber()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPushEnabled()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasToken()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasUpdatedAt()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo$DeviceType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->enabled_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->pushEnabled_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->token_:Lcom/google/protobuf/ByteString;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->createdAt_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_5
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->updatedAt_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_6
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->markedForDeletionAt_:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_7
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->getAppIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_8
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->androidId_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_9
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_a
    return-void
.end method
