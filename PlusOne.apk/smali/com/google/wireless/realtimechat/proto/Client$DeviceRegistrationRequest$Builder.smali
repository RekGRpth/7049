.class public final Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequestOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;",
        "Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequestOrBuilder;"
    }
.end annotation


# instance fields
.field private androidId_:J

.field private androidRegistrationId_:Ljava/lang/Object;

.field private appId_:Ljava/lang/Object;

.field private bitField0_:I

.field private deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

.field private locale_:Ljava/lang/Object;

.field private oldToken_:Lcom/google/protobuf/ByteString;

.field private ownerId_:Ljava/lang/Object;

.field private phoneNumber_:Ljava/lang/Object;

.field private registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

.field private token_:Lcom/google/protobuf/ByteString;

.field private verification_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;->REGISTER:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    return-void
.end method

.method static synthetic access$37500()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    goto :goto_0

    :sswitch_2
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v3, v3, 0x80

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_3
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x10

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x20

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_7
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x40

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_8
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v3, v3, 0x400

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    goto :goto_0

    :sswitch_9
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v3, v3, 0x200

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v3, v3, 0x100

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidId_:J

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v3, v3, 0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;-><init>(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$37702(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$37802(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->ownerId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$37902(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->verification_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38002(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->locale_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38102(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->phoneNumber_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38202(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v0, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->appId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38302(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->token_:Lcom/google/protobuf/ByteString;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38402(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    and-int/lit16 v3, v0, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidId_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->androidId_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38502(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;J)J

    and-int/lit16 v3, v0, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x200

    :cond_9
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->androidRegistrationId_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38602(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v3, v0, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x400

    :cond_a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->oldToken_:Lcom/google/protobuf/ByteString;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38702(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/ByteString;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->access$38802(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;->REGISTER:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidId_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/ByteString;->EMPTY:Lcom/google/protobuf/ByteString;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearAndroidId()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidId_:J

    return-object p0
.end method

.method public final clearAndroidRegistrationId()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getAndroidRegistrationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearAppId()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getAppId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearDeviceType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;->ANDROID:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    return-object p0
.end method

.method public final clearLocale()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getLocale()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearOldToken()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getOldToken()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final clearOwnerId()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getOwnerId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearPhoneNumber()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearRegistrationType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;->REGISTER:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    return-object p0
.end method

.method public final clearToken()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getToken()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final clearVerification()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getVerification()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final getAndroidId()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidId_:J

    return-wide v0
.end method

.method public final getAndroidRegistrationId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getAppId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    return-object v0
.end method

.method public final getDeviceType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    return-object v0
.end method

.method public final getLocale()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getOldToken()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getOwnerId()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getPhoneNumber()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getRegistrationType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    return-object v0
.end method

.method public final getToken()Lcom/google/protobuf/ByteString;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public final getVerification()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final hasAndroidId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAndroidRegistrationId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasAppId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasDeviceType()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLocale()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasOldToken()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasOwnerId()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasPhoneNumber()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasRegistrationType()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasToken()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasVerification()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasDeviceType()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getDeviceType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setDeviceType(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasRegistrationType()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getRegistrationType()Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setRegistrationType(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasOwnerId()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getOwnerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setOwnerId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasVerification()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getVerification()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setVerification(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasLocale()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getLocale()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setLocale(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setPhoneNumber(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasAppId()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getAppId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setAppId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasToken()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getToken()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setToken(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasAndroidId()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getAndroidId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setAndroidId(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasAndroidRegistrationId()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getAndroidRegistrationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setAndroidRegistrationId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->hasOldToken()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest;->getOldToken()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->setOldToken(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;

    goto/16 :goto_0
.end method

.method public final setAndroidId(J)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidId_:J

    return-object p0
.end method

.method public final setAndroidRegistrationId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->androidRegistrationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setAppId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->appId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setDeviceType(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->deviceType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$DeviceType;

    return-object p0
.end method

.method public final setLocale(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->locale_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setOldToken(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->oldToken_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final setOwnerId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->ownerId_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setPhoneNumber(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->phoneNumber_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setRegistrationType(Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->registrationType_:Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$RegistrationType;

    return-object p0
.end method

.method public final setToken(Lcom/google/protobuf/ByteString;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/ByteString;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->token_:Lcom/google/protobuf/ByteString;

    return-object p0
.end method

.method public final setVerification(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$DeviceRegistrationRequest$Builder;->verification_:Ljava/lang/Object;

    return-object p0
.end method
