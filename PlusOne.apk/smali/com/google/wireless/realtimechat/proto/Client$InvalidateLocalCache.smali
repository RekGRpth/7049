.class public final Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCacheOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InvalidateLocalCache"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private version_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->version_:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;-><init>(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;)V

    return-void
.end method

.method static synthetic access$47502(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->version_:I

    return p1
.end method

.method static synthetic access$47602(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->access$47300()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->access$47300()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;)Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->version_:I

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getVersion()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->version_:I

    return v0
.end method

.method public final hasVersion()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 2
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InvalidateLocalCache;->version_:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_0
    return-void
.end method
