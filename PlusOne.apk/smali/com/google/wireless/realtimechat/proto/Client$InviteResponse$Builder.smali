.class public final Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$InviteResponseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;",
        "Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Client$InviteResponseOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private participantError_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;",
            ">;"
        }
    .end annotation
.end field

.field private requestError_:Ljava/lang/Object;

.field private status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

.field private timestamp_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    return-void
.end method

.method static synthetic access$26300()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureParticipantErrorIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    goto :goto_0

    :sswitch_2
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->timestamp_:J

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;->newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;

    move-result-object v1

    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->addParticipantError(Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    goto :goto_0

    :sswitch_4
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final addAllParticipantError(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addParticipantError(ILcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addParticipantError(ILcom/google/wireless/realtimechat/proto/Client$ParticipantError;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public final addParticipantError(Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addParticipantError(Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;-><init>(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26502(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->timestamp_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->timestamp_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26602(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;J)J

    and-int/lit8 v3, v0, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->requestError_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26702(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;Ljava/lang/Object;)Ljava/lang/Object;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->participantError_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26802(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;Ljava/util/List;)Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26902(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->timestamp_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearParticipantError()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearRequestError()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getRequestError()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearStatus()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;->OK:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final clearTimestamp()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->timestamp_:J

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    return-object v0
.end method

.method public final getParticipantError(I)Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    return-object v0
.end method

.method public final getParticipantErrorCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getParticipantErrorList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestError()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object v0
.end method

.method public final getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->timestamp_:J

    return-wide v0
.end method

.method public final hasRequestError()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStatus()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasTimestamp()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->hasStatus()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getStatus()Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->setTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->hasRequestError()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->getRequestError()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->setRequestError(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;

    :cond_4
    # getter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->participantError_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26800(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->participantError_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26800(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->participantError_:Ljava/util/List;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;->access$26800(Lcom/google/wireless/realtimechat/proto/Client$InviteResponse;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final setParticipantError(ILcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 2
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/wireless/realtimechat/proto/Client$ParticipantError$Builder;->build()Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setParticipantError(ILcom/google/wireless/realtimechat/proto/Client$ParticipantError;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Lcom/google/wireless/realtimechat/proto/Client$ParticipantError;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->ensureParticipantErrorIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->participantError_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setRequestError(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->requestError_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setStatus(Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->status_:Lcom/google/wireless/realtimechat/proto/Data$ResponseStatus;

    return-object p0
.end method

.method public final setTimestamp(J)Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$InviteResponse$Builder;->timestamp_:J

    return-object p0
.end method
