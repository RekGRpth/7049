.class public final Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfoOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceiptInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private eventTimestamp_:J

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->eventTimestamp_:J

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;-><init>(Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;)V

    return-void
.end method

.method static synthetic access$13202(Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;)Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    return-object p1
.end method

.method static synthetic access$13302(Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;J)J
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;
    .param p1    # J

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->eventTimestamp_:J

    return-wide p1
.end method

.method static synthetic access$13402(Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;->access$13000()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;

    return-object v0
.end method

.method public final getEventTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->eventTimestamp_:J

    return-wide v0
.end method

.method public final getSerializedSize()I
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    move v1, v0

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    invoke-virtual {v2}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->getNumber()I

    move-result v2

    invoke-static {v3, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v2

    add-int/lit8 v0, v2, 0x0

    :cond_1
    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v4, :cond_2

    iget-wide v2, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->eventTimestamp_:J

    invoke-static {v4, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v2

    add-int/2addr v0, v2

    :cond_2
    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedSerializedSize:I

    move v1, v0

    goto :goto_0
.end method

.method public final getType()Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    return-object v0
.end method

.method public final hasEventTimestamp()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasType()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->getSerializedSize()I

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->type_:Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Client$Receipt$Type;->getNumber()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Client$ReceiptRequest$ReceiptInfo;->eventTimestamp_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1
    return-void
.end method
