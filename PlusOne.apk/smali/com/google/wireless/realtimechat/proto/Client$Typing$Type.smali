.class public final enum Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;
.super Ljava/lang/Enum;
.source "Client.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client$Typing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

.field public static final enum CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

.field public static final enum PAUSE:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

.field public static final enum START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

.field public static final enum UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    const-string v1, "START"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->PAUSE:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    const-string v1, "CLEAR"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->PAUSE:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->$VALUES:[Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type$1;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type$1;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->UNKNOWN:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->START:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->PAUSE:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->CLEAR:Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->$VALUES:[Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    invoke-virtual {v0}, [Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$Typing$Type;->value:I

    return v0
.end method
