.class public final Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Client.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Client$TypingNotificationOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Client;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TypingNotification"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Builder;,
        Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Type;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;

.field private static final serialVersionUID:J


# instance fields
.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;-><init>(Lcom/google/wireless/realtimechat/proto/Client$TypingNotification$Builder;)V

    return-void
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->memoizedSerializedSize:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->memoizedSerializedSize:I

    move v0, v1

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 0
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Client$TypingNotification;->getSerializedSize()I

    return-void
.end method
