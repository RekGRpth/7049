.class public final Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadataOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;",
        "Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadataOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private joinCode_:Ljava/lang/Object;

.field private location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

.field private tag_:Lcom/google/protobuf/LazyStringList;

.field private visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;->PUBLIC:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$1100()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method private ensureTagIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 6
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v4

    if-nez v4, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;->valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->newBuilder()Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->hasLocation()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->getLocation()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    :cond_1
    invoke-virtual {p1, v1, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->setLocation(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->ensureTagIsMutable()V

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_4
    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    iput-object v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final addAllTag(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->ensureTagIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addTag(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->ensureTagIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;-><init>(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1302(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1402(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->tag_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1502(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    and-int/lit8 v3, v0, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x4

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->joinCode_:Ljava/lang/Object;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1602(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;Ljava/lang/Object;)Ljava/lang/Object;

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1702(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;->PUBLIC:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearJoinCode()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getJoinCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    return-object p0
.end method

.method public final clearLocation()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearTag()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearVisibility()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;->PUBLIC:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    return-object v0
.end method

.method public final getJoinCode()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-nez v2, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    :goto_0
    return-object v1

    :cond_0
    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getLocation()Lcom/google/wireless/realtimechat/proto/Data$Location;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    return-object v0
.end method

.method public final getTag(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getTagCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getTagList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getVisibility()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    return-object v0
.end method

.method public final hasJoinCode()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasLocation()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasVisibility()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 3
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->hasVisibility()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getVisibility()Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->setVisibility(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->hasLocation()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getLocation()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$Location;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Data$Location;->newBuilder(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    :goto_1
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    :cond_3
    # getter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->tag_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1500(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    # getter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->tag_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1500(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    :cond_4
    :goto_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->hasJoinCode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->getJoinCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->setJoinCode(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;

    goto :goto_0

    :cond_5
    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->ensureTagIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->tag_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;->access$1500(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public final setJoinCode(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->joinCode_:Ljava/lang/Object;

    return-object p0
.end method

.method public final setLocation(Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$Location$Builder;->build()Lcom/google/wireless/realtimechat/proto/Data$Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setLocation(Lcom/google/wireless/realtimechat/proto/Data$Location;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$Location;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->location_:Lcom/google/wireless/realtimechat/proto/Data$Location;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    return-object p0
.end method

.method public final setTag(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->ensureTagIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->tag_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setVisibility(Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;)Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;
    .locals 1
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$Builder;->visibility_:Lcom/google/wireless/realtimechat/proto/Data$ConversationMetadata$ConversationVisibility;

    return-object p0
.end method
