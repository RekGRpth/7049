.class public final enum Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
.super Ljava/lang/Enum;
.source "Data.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data$Participant;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

.field public static final enum ANDROID:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

.field public static final enum INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

.field public static final enum IPHONE:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

.field public static final enum SMS:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

.field public static final enum WEB:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v7, v3}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->SMS:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const-string v1, "ANDROID"

    invoke-direct {v0, v1, v4, v5}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->ANDROID:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const-string v1, "IPHONE"

    invoke-direct {v0, v1, v5, v6}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->IPHONE:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const-string v1, "WEB"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->WEB:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->SMS:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->ANDROID:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->IPHONE:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->WEB:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->$VALUES:[Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type$1;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type$1;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->value:I

    return-void
.end method

.method public static valueOf(I)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
    .locals 1
    .param p0    # I

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->INVITED:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->SMS:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->ANDROID:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->IPHONE:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->WEB:Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    return-object v0
.end method

.method public static values()[Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->$VALUES:[Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    invoke-virtual {v0}, [Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$Participant$Type;->value:I

    return v0
.end method
