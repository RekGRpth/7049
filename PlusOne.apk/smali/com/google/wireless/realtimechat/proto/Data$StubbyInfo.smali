.class public final Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Data$StubbyInfoOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StubbyInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

.field private static final serialVersionUID:J


# instance fields
.field private bitField0_:I

.field private clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private recipientId_:Lcom/google/protobuf/LazyStringList;

.field private senderId_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->senderId_:Ljava/lang/Object;

    sget-object v1, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {}, Lcom/google/wireless/webapps/Version$ClientVersion;->getDefaultInstance()Lcom/google/wireless/webapps/Version$ClientVersion;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;-><init>(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;)V

    return-void
.end method

.method static synthetic access$4902(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .param p1    # Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->senderId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$5000(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .param p1    # Lcom/google/protobuf/LazyStringList;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$5102(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;Lcom/google/wireless/webapps/Version$ClientVersion;)Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .param p1    # Lcom/google/wireless/webapps/Version$ClientVersion;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object p1
.end method

.method static synthetic access$5202(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;I)I
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .param p1    # I

    iput p1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object v0
.end method

.method private getSenderIdBytes()Lcom/google/protobuf/ByteString;
    .locals 3

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->senderId_:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->senderId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v1, Lcom/google/protobuf/ByteString;

    move-object v0, v1

    goto :goto_0
.end method

.method public static newBuilder()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->access$4700()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->access$4700()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;)Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo$Builder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getClientVersion()Lcom/google/wireless/webapps/Version$ClientVersion;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;

    return-object v0
.end method

.method public final getRecipientId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getRecipientIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getRecipientIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public final getSenderId()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->senderId_:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    check-cast v1, Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    move-object v0, v1

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v2, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->senderId_:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final getSerializedSize()I
    .locals 6

    const/4 v5, 0x1

    iget v2, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedSerializedSize:I

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    move v3, v2

    :goto_0
    return v3

    :cond_0
    const/4 v2, 0x0

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v5, :cond_1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getSenderIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/lit8 v2, v4, 0x0

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v4, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-static {v4}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v4

    add-int/2addr v0, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    add-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getRecipientIdList()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v2, v4

    iget v4, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-static {v4, v5}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v4

    add-int/2addr v2, v4

    :cond_3
    iput v2, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedSerializedSize:I

    move v3, v2

    goto :goto_0
.end method

.method public final hasClientVersion()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasSenderId()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v1, 0x1

    iget-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedIsInitialized:B

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->memoizedIsInitialized:B

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getSerializedSize()I

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->getSenderIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->recipientId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/wireless/realtimechat/proto/Data$StubbyInfo;->clientVersion_:Lcom/google/wireless/webapps/Version$ClientVersion;

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    return-void
.end method
