.class public final Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Data.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Data$TimeRangeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Data$TimeRange;",
        "Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Data$TimeRangeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private end_:J

.field private start_:J


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    return-void
.end method

.method static synthetic access$4100()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v1

    if-nez v1, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->start_:J

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->end_:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public final build()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;-><init>(Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v0, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->start_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->start_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->access$4302(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;J)J

    and-int/lit8 v3, v0, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-wide v3, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->end_:J

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->end_:J
    invoke-static {v1, v3, v4}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->access$4402(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;J)J

    # setter for: Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->access$4502(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 3

    const-wide/16 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->start_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    iput-wide v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->end_:J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnd()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->end_:J

    return-object p0
.end method

.method public final clearStart()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->start_:J

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    return-object v0
.end method

.method public final getEnd()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->end_:J

    return-wide v0
.end method

.method public final getStart()J
    .locals 2

    iget-wide v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->start_:J

    return-wide v0
.end method

.method public final hasEnd()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasStart()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/wireless/realtimechat/proto/Data$TimeRange;)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Data$TimeRange;

    move-result-object v0

    if-ne p1, v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->hasStart()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->getStart()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->setStart(J)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->hasEnd()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange;->getEnd()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->setEnd(J)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;

    goto :goto_0
.end method

.method public final setEnd(J)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->end_:J

    return-object p0
.end method

.method public final setStart(J)Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;
    .locals 1
    .param p1    # J

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/google/wireless/realtimechat/proto/Data$TimeRange$Builder;->start_:J

    return-object p0
.end method
