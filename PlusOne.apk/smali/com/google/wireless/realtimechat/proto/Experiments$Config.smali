.class public final Lcom/google/wireless/realtimechat/proto/Experiments$Config;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "Experiments.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Experiments$ConfigOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Experiments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Config"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;
    }
.end annotation


# static fields
.field private static final defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$Config;

.field private static final serialVersionUID:J


# instance fields
.field private experimentMapping_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;-><init>()V

    sput-object v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->memoizedIsInitialized:B

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->memoizedSerializedSize:I

    return-void
.end method

.method private constructor <init>(Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;)V
    .locals 2
    .param p1    # Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(B)V

    iput-byte v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->memoizedIsInitialized:B

    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;B)V
    .locals 0
    .param p1    # Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;

    invoke-direct {p0, p1}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;-><init>(Lcom/google/wireless/realtimechat/proto/Experiments$Config$Builder;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/google/wireless/realtimechat/proto/Experiments$Config;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/google/wireless/realtimechat/proto/Experiments$Config;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0    # Lcom/google/wireless/realtimechat/proto/Experiments$Config;
    .param p1    # Ljava/util/List;

    iput-object p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Experiments$Config;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$Config;
    .locals 1

    sget-object v0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->defaultInstance:Lcom/google/wireless/realtimechat/proto/Experiments$Config;

    return-object v0
.end method

.method public final getExperimentMapping(I)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    return-object v0
.end method

.method public final getExperimentMappingCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getExperimentMappingList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    return-object v0
.end method

.method public final getExperimentMappingOrBuilder(I)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMappingOrBuilder;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMappingOrBuilder;

    return-object v0
.end method

.method public final getExperimentMappingOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMappingOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    return-object v0
.end method

.method public final getSerializedSize()I
    .locals 5

    iget v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->memoizedSerializedSize:I

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    move v2, v1

    :goto_0
    return v2

    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    const/4 v4, 0x1

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v3}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v3

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iput v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->memoizedSerializedSize:I

    move v2, v1

    goto :goto_0
.end method

.method protected final writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .param p1    # Lcom/google/protobuf/CodedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->getSerializedSize()I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$Config;->experimentMapping_:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
