.class public final Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "Experiments.java"

# interfaces
.implements Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMappingOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;",
        "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;",
        ">;",
        "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMappingOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private disabledAllUser_:Z

.field private disabledDomain_:Lcom/google/protobuf/LazyStringList;

.field private disabledEmail_:Lcom/google/protobuf/LazyStringList;

.field private disabledId_:Lcom/google/protobuf/LazyStringList;

.field private disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

.field private disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

.field private disabledRegexId_:Lcom/google/protobuf/LazyStringList;

.field private enabledAllUser_:Z

.field private enabledDomain_:Lcom/google/protobuf/LazyStringList;

.field private enabledEmail_:Lcom/google/protobuf/LazyStringList;

.field private enabledId_:Lcom/google/protobuf/LazyStringList;

.field private enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

.field private enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

.field private enabledRegexId_:Lcom/google/protobuf/LazyStringList;

.field private experiment_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    return-void
.end method

.method static synthetic access$100()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;-><init>()V

    return-object v0
.end method

.method private clone()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 4

    new-instance v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;-><init>()V

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v2

    if-eq v1, v2, :cond_e

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x2

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    :goto_0
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_10

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_1
    :goto_1
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_11

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_2
    :goto_2
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$600(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$600(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x9

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_3
    :goto_3
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$700(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_13

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$700(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x11

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_4
    :goto_4
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$800(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_14

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$800(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x21

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_5
    :goto_5
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$900(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_15

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$900(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x41

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_6
    :goto_6
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1000(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_16

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1000(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x81

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_7
    :goto_7
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1100(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_17

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1100(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x101

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_8
    :goto_8
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1200(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_18

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1200(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x201

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_9
    :goto_9
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_19

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x401

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_a
    :goto_a
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1a

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x801

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_b
    :goto_b
    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1b

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v2, v2, -0x1001

    iput v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_c
    :goto_c
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->hasDisabledAllUser()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDisabledAllUser()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->setDisabledAllUser(Z)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    :cond_d
    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->hasEnabledAllUser()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getEnabledAllUser()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->setEnabledAllUser(Z)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    :cond_e
    return-object v0

    :cond_f
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureExperimentIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    :cond_10
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledIdIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_11
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledIdIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2

    :cond_12
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexIdIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$600(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    :cond_13
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexIdIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$700(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_4

    :cond_14
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledEmailIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$800(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_5

    :cond_15
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledEmailIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$900(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_6

    :cond_16
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexEmailIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1000(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_7

    :cond_17
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexEmailIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1100(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_8

    :cond_18
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledDomainIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1200(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_9

    :cond_19
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledDomainIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1300(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_a

    :cond_1a
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexDomainIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1400(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_b

    :cond_1b
    invoke-direct {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexDomainIsMutable()V

    iget-object v2, v0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    # getter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1500(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;)Lcom/google/protobuf/LazyStringList;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_c
.end method

.method private ensureDisabledDomainIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureDisabledEmailIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureDisabledIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureDisabledRegexDomainIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureDisabledRegexEmailIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureDisabledRegexIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEnabledDomainIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEnabledEmailIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEnabledIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEnabledRegexDomainIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEnabledRegexEmailIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureEnabledRegexIdIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureExperimentIsMutable()V
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 5
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/CodedInputStream;->skipField(I)Z

    move-result v3

    if-nez v3, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureExperimentIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readRawVarint32()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/CodedInputStream;->pushLimit(I)I

    move-result v1

    :goto_1
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->getBytesUntilLimit()I

    move-result v3

    if-lez v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->addExperiment(I)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    goto :goto_1

    :cond_1
    invoke-virtual {p1, v1}, Lcom/google/protobuf/CodedInputStream;->popLimit(I)V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledIdIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_4
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledIdIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexIdIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_6
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexIdIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_7
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledEmailIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_8
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledEmailIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexEmailIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_a
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexEmailIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledDomainIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledDomainIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexDomainIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexDomainIsMutable()V

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto/16 :goto_0

    :sswitch_f
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v3, v3, 0x2000

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledAllUser_:Z

    goto/16 :goto_0

    :sswitch_10
    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v3, v3, 0x4000

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledAllUser_:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x5a -> :sswitch_c
        0x62 -> :sswitch_d
        0x6a -> :sswitch_e
        0x70 -> :sswitch_f
        0x78 -> :sswitch_10
    .end sparse-switch
.end method


# virtual methods
.method public final addAllDisabledDomain(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllDisabledEmail(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllDisabledId(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllDisabledRegexDomain(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllDisabledRegexEmail(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllDisabledRegexId(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllEnabledDomain(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllEnabledEmail(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllEnabledId(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllEnabledRegexDomain(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllEnabledRegexEmail(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllEnabledRegexId(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addAllExperiment(Ljava/lang/Iterable;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureExperimentIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public final addDisabledDomain(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addDisabledEmail(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addDisabledId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addDisabledRegexDomain(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addDisabledRegexEmail(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addDisabledRegexId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEnabledDomain(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEnabledEmail(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEnabledId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEnabledRegexDomain(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEnabledRegexEmail(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addEnabledRegexId(Ljava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final addExperiment(I)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 2
    .param p1    # I

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureExperimentIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final build()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 2

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/protobuf/UninitializedMessageException;

    invoke-direct {v1}, Lcom/google/protobuf/UninitializedMessageException;-><init>()V

    throw v1

    :cond_0
    return-object v0
.end method

.method public final bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v0

    return-object v0
.end method

.method public final buildPartial()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 5

    new-instance v1, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;-><init>(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;B)V

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x2

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_0
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->experiment_:Ljava/util/List;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$302(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Ljava/util/List;)Ljava/util/List;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_1
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$402(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_2
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$502(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_3
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$602(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_4
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$702(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x21

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_5
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$802(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x41

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_6
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$902(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x81

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_7
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1002(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x101

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_8
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1102(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x201

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_9
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1202(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x401

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_a
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1302(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x801

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_b
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1402(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x1001

    iput v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    :cond_c
    iget-object v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1502(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    and-int/lit16 v3, v0, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    const/4 v2, 0x1

    :cond_d
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledAllUser_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->disabledAllUser_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1602(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Z)Z

    and-int/lit16 v3, v0, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    or-int/lit8 v2, v2, 0x2

    :cond_e
    iget-boolean v3, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledAllUser_:Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->enabledAllUser_:Z
    invoke-static {v1, v3}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1702(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;Z)Z

    # setter for: Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->bitField0_:I
    invoke-static {v1, v2}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->access$1802(Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;I)I

    return-object v1
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->clear()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final clear()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledAllUser_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledAllUser_:Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearDisabledAllUser()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledAllUser_:Z

    return-object p0
.end method

.method public final clearDisabledDomain()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearDisabledEmail()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearDisabledId()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearDisabledRegexDomain()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearDisabledRegexEmail()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearDisabledRegexId()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnabledAllUser()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledAllUser_:Z

    return-object p0
.end method

.method public final clearEnabledDomain()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnabledEmail()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnabledId()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnabledRegexDomain()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnabledRegexEmail()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearEnabledRegexId()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final clearExperiment()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    return-object p0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->clone()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v0

    return-object v0
.end method

.method public final getDefaultInstanceForType()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;
    .locals 1

    invoke-static {}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;->getDefaultInstance()Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping;

    move-result-object v0

    return-object v0
.end method

.method public final getDisabledAllUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledAllUser_:Z

    return v0
.end method

.method public final getDisabledDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getDisabledEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getDisabledId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getDisabledRegexDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledRegexDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledRegexDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getDisabledRegexEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledRegexEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledRegexEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getDisabledRegexId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getDisabledRegexIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getDisabledRegexIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabledAllUser()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledAllUser_:Z

    return v0
.end method

.method public final getEnabledDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabledEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabledId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabledRegexDomain(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledRegexDomainCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledRegexDomainList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabledRegexEmail(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledRegexEmailCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledRegexEmailList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getEnabledRegexId(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final getEnabledRegexIdCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public final getEnabledRegexIdList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final getExperiment(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final getExperimentCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getExperimentList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final hasDisabledAllUser()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasEnabledAllUser()Z
    .locals 2

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .param p1    # Lcom/google/protobuf/CodedInputStream;
    .param p2    # Lcom/google/protobuf/ExtensionRegistryLite;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;

    move-result-object v0

    return-object v0
.end method

.method public final setDisabledAllUser(Z)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledAllUser_:Z

    return-object p0
.end method

.method public final setDisabledDomain(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setDisabledEmail(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setDisabledId(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setDisabledRegexDomain(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setDisabledRegexEmail(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setDisabledRegexId(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureDisabledRegexIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->disabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEnabledAllUser(Z)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # Z

    iget v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledAllUser_:Z

    return-object p0
.end method

.method public final setEnabledDomain(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEnabledEmail(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEnabledId(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEnabledRegexDomain(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexDomainIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexDomain_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEnabledRegexEmail(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexEmailIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexEmail_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setEnabledRegexId(ILjava/lang/String;)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureEnabledRegexIdIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->enabledRegexId_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public final setExperiment(II)Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-direct {p0}, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->ensureExperimentIsMutable()V

    iget-object v0, p0, Lcom/google/wireless/realtimechat/proto/Experiments$ExperimentMapping$Builder;->experiment_:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
