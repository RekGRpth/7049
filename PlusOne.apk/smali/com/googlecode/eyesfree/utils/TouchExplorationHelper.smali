.class public abstract Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
.super Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;
.source "TouchExplorationHelper.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;"
    }
.end annotation


# instance fields
.field private mCurrentItem:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

.field private mFocusedItemId:I

.field private final mManager:Landroid/view/accessibility/AccessibilityManager;

.field private final mNodeCache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ">;"
        }
    .end annotation
.end field

.field private final mOnHoverListener:Landroid/view/View$OnHoverListener;

.field private mParentView:Landroid/view/View;

.field private final mTempGlobalRect:Landroid/graphics/Rect;

.field private final mTempParentRect:Landroid/graphics/Rect;

.field private final mTempScreenRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    new-instance v0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$1;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$1;-><init>(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    new-instance v0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;

    invoke-direct {v0, p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper$2;-><init>(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)V

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mOnHoverListener:Landroid/view/View$OnHoverListener;

    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;

    return-void
.end method

.method static synthetic access$000(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1
    .param p0    # Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mManager:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;Ljava/lang/Object;)V
    .locals 2
    .param p0    # Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    const/16 v1, 0x100

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    :cond_0
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mCurrentItem:Ljava/lang/Object;

    const/16 v1, 0x80

    invoke-direct {p0, v0, v1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    :cond_1
    return-void
.end method

.method private clearCache()V
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    invoke-virtual {v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->recycle()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    return-void
.end method

.method private sendEventForItem(Ljava/lang/Object;I)V
    .locals 5
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    new-instance v2, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    invoke-direct {v2, v0}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getIdForItem(Ljava/lang/Object;)I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    invoke-virtual {p0, p1, v0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "You must add text or a content description in populateEventForItem()"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v4, v3}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;I)V

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    return-void
.end method


# virtual methods
.method public final createAccessibilityNodeInfo(I)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;
    .locals 7
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-static {v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-static {v3, v2}, Landroid/support/v4/view/ViewCompat;->onInitializeAccessibilityNodeInfo(Landroid/view/View;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p0, v3}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getVisibleItems(Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getIdForItem(Ljava/lang/Object;)I

    move-result v4

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v5, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addChild(Landroid/view/View;I)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    if-eqz v0, :cond_2

    invoke-static {v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    :cond_1
    :goto_1
    return-object v2

    :cond_2
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getItemForId(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_3

    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-static {}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain()Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getIdForItem(Ljava/lang/Object;)I

    move-result v3

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setEnabled(Z)V

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setVisibleToUser(Z)V

    invoke-virtual {p0, v1, v2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "TouchExplHelper"

    const-string v5, "You should add text or a content description in populateNodeForItem()"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setPackageName(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setClassName(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setParent(Landroid/view/View;)V

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v4, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setSource(Landroid/view/View;I)V

    iget v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    if-ne v4, v3, :cond_5

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    :goto_2
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInParent(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->getBoundsInScreen(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_6

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "You must set parent or screen bounds in populateNodeForItem()"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_5
    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_8

    :cond_7
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempGlobalRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInScreen(Landroid/graphics/Rect;)V

    :cond_8
    :goto_3
    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mNodeCache:Landroid/util/SparseArray;

    invoke-static {v2}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->obtain(Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_1

    :cond_9
    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempScreenRect:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v5, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    neg-int v3, v3

    neg-int v4, v4

    invoke-virtual {v5, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mTempParentRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    goto :goto_3
.end method

.method protected abstract getIdForItem(Ljava/lang/Object;)I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation
.end method

.method protected abstract getItemAt(FF)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)TT;"
        }
    .end annotation
.end method

.method protected abstract getItemForId(I)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation
.end method

.method protected abstract getVisibleItems(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public final install(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-static {p1}, Landroid/support/v4/view/ViewCompat;->getAccessibilityNodeProvider(Landroid/view/View;)Landroid/support/v4/view/accessibility/AccessibilityNodeProviderCompat;

    move-result-object v0

    instance-of v0, v0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot install TouchExplorationHelper on a View that already has a helper installed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mOnHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    invoke-virtual {p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->invalidateParent()V

    return-void
.end method

.method public final invalidateParent()V
    .locals 2

    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->clearCache()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    return-void
.end method

.method public final performAction(IILandroid/os/Bundle;)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/os/Bundle;

    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mDelegate:Landroid/support/v4/view/AccessibilityDelegateCompat;

    iget-object v3, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v2, v3, p2, p3}, Landroid/support/v4/view/AccessibilityDelegateCompat;->performAccessibilityAction(Landroid/view/View;ILandroid/os/Bundle;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->getItemForId(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    sparse-switch p2, :sswitch_data_0

    :cond_2
    :goto_1
    invoke-virtual {p0, v1, p2, p3}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z

    move-result v2

    or-int/2addr v0, v2

    goto :goto_0

    :sswitch_0
    iget v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    if-eq v2, p1, :cond_2

    iput p1, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    const v2, 0x8000

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    iget v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    if-ne v2, p1, :cond_2

    const/high16 v2, -0x80000000

    iput v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mFocusedItemId:I

    const/high16 v2, 0x10000

    invoke-direct {p0, v1, v2}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->sendEventForItem(Ljava/lang/Object;I)V

    const/4 v0, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x40 -> :sswitch_0
        0x80 -> :sswitch_1
    .end sparse-switch
.end method

.method protected abstract performActionForItem(Ljava/lang/Object;ILandroid/os/Bundle;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I",
            "Landroid/os/Bundle;",
            ")Z"
        }
    .end annotation
.end method

.method protected abstract populateEventForItem(Ljava/lang/Object;Landroid/view/accessibility/AccessibilityEvent;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/view/accessibility/AccessibilityEvent;",
            ")V"
        }
    .end annotation
.end method

.method protected abstract populateNodeForItem(Ljava/lang/Object;Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;",
            ")V"
        }
    .end annotation
.end method

.method public final uninstall()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot uninstall TouchExplorationHelper on a View that does not have a helper installed."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    new-instance v1, Landroid/support/v4/view/AccessibilityDelegateCompat;

    invoke-direct {v1}, Landroid/support/v4/view/AccessibilityDelegateCompat;-><init>()V

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setAccessibilityDelegate(Landroid/view/View;Landroid/support/v4/view/AccessibilityDelegateCompat;)V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setImportantForAccessibility(Landroid/view/View;I)V

    invoke-direct {p0}, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->clearCache()V

    iget-object v0, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iput-object v2, p0, Lcom/googlecode/eyesfree/utils/TouchExplorationHelper;->mParentView:Landroid/view/View;

    return-void
.end method
