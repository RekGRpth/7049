.class public Lcom/android/settings/HDMISettings;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "HDMISettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# static fields
.field private static final KEY_TOGGLE_AUDIO:Ljava/lang/String; = "audio_toggler"

.field private static final KEY_TOGGLE_HDMI:Ljava/lang/String; = "hdmi_toggler"

.field private static final KEY_TOGGLE_VIDEO:Ljava/lang/String; = "video_toggler"

.field private static final KEY_VIDEO_RESOLUTION:Ljava/lang/String; = "video_resolution"

.field private static final TAG:Ljava/lang/String; = "hdmi"


# instance fields
.field private mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

.field private mHDMIServiceConn:Landroid/content/ServiceConnection;

.field private mIsHDMIEnabled:Z

.field mLocalServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mToggleAudioPref:Landroid/preference/CheckBoxPreference;

.field private mToggleHDMIPref:Landroid/preference/CheckBoxPreference;

.field private mToggleVideoPref:Landroid/preference/CheckBoxPreference;

.field private mVideoResolutionPref:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/HDMISettings;->mIsHDMIEnabled:Z

    new-instance v0, Lcom/android/settings/HDMISettings$1;

    invoke-direct {v0, p0}, Lcom/android/settings/HDMISettings$1;-><init>(Lcom/android/settings/HDMISettings;)V

    iput-object v0, p0, Lcom/android/settings/HDMISettings;->mHDMIServiceConn:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/android/settings/HDMISettings$2;

    invoke-direct {v0, p0}, Lcom/android/settings/HDMISettings$2;-><init>(Lcom/android/settings/HDMISettings;)V

    iput-object v0, p0, Lcom/android/settings/HDMISettings;->mLocalServiceReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$002(Lcom/android/settings/HDMISettings;Lcom/mediatek/hdmi/HDMILocalService;)Lcom/mediatek/hdmi/HDMILocalService;
    .locals 0
    .param p0    # Lcom/android/settings/HDMISettings;
    .param p1    # Lcom/mediatek/hdmi/HDMILocalService;

    iput-object p1, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/android/settings/HDMISettings;)V
    .locals 0
    .param p0    # Lcom/android/settings/HDMISettings;

    invoke-direct {p0}, Lcom/android/settings/HDMISettings;->updateSettingsItemEnableStatus()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/settings/HDMISettings;)V
    .locals 0
    .param p0    # Lcom/android/settings/HDMISettings;

    invoke-direct {p0}, Lcom/android/settings/HDMISettings;->updateSelectedResolution()V

    return-void
.end method

.method private updatePref()V
    .locals 6

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hdmi_enable_status"

    invoke-static {v2, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/android/settings/HDMISettings;->mIsHDMIEnabled:Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hdmi_audio_status"

    invoke-static {v2, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_2

    move v0, v3

    :goto_1
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hdmi_video_status"

    invoke-static {v2, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_3

    move v1, v3

    :goto_2
    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleHDMIPref:Landroid/preference/CheckBoxPreference;

    iget-boolean v3, p0, Lcom/android/settings/HDMISettings;->mIsHDMIEnabled:Z

    invoke-virtual {v2, v3}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleAudioPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v0}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/HDMISettings;->updateSelectedResolution()V

    return-void

    :cond_1
    move v2, v4

    goto :goto_0

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_2
.end method

.method private updateSelectedResolution()V
    .locals 7

    const/4 v6, 0x0

    const-string v4, "hdmi"

    const-string v5, "HDMISettings>>updateSelectedResolution()"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    if-nez v4, :cond_1

    const-string v4, "hdmi"

    const-string v5, "HDMISettings>>updateSelectedResolution(), service have not been connected, wait"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "hdmi_video_resolution"

    invoke-static {v4, v5}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    invoke-virtual {v4}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, -0x1

    const/4 v0, 0x0

    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v0

    :cond_2
    const/4 v4, -0x1

    if-eq v2, v4, :cond_4

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    invoke-virtual {v4, v2}, Landroid/preference/ListPreference;->setValueIndex(I)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    const-string v4, "hdmi"

    const-string v5, " set HDMI video resolution to default value, the first one"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    invoke-virtual {v4, v6}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    aget-object v5, v1, v6

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/mediatek/hdmi/HDMILocalService;->setVideoResolution(I)Z

    goto :goto_0
.end method

.method private updateSettingsItemEnableStatus()V
    .locals 6

    const/4 v3, 0x1

    const/4 v4, 0x0

    const-string v2, "hdmi"

    const-string v5, "HDMISettings>>updateSettingsItemEnableStatus()"

    invoke-static {v2, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    if-nez v2, :cond_0

    const-string v2, "hdmi"

    const-string v3, "HDMI service has not connected, wait"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-virtual {v2}, Lcom/mediatek/hdmi/HDMILocalService;->isCablePluged()Z

    move-result v0

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v5, "hdmi_enable_status"

    invoke-static {v2, v5, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v3, :cond_2

    move v2, v3

    :goto_1
    iput-boolean v2, p0, Lcom/android/settings/HDMISettings;->mIsHDMIEnabled:Z

    if-eqz v0, :cond_3

    iget-boolean v2, p0, Lcom/android/settings/HDMISettings;->mIsHDMIEnabled:Z

    if-eqz v2, :cond_3

    move v1, v3

    :goto_2
    const-string v2, "hdmi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Is cable pluged?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isHDMIEnabled?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/settings/HDMISettings;->mIsHDMIEnabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleAudioPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_1
    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_2
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const-string v2, "hdmi"

    const-string v3, ">>HDMISettings.onCreate()"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f05001d

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    const-string v2, "hdmi_toggler"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleHDMIPref:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleHDMIPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v2, "audio_toggler"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleAudioPref:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleAudioPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v2, "video_toggler"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/CheckBoxPreference;

    iput-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string v2, "video_resolution"

    invoke-virtual {p0, v2}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/ListPreference;

    iput-object v2, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/mediatek/hdmi/HDMILocalService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v4, p0, Lcom/android/settings/HDMISettings;->mHDMIServiceConn:Landroid/content/ServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContextWrapper;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v2, "hdmi"

    const-string v3, "HDMISettings fail to bind HDMI service"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleHDMIPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleAudioPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mVideoResolutionPref:Landroid/preference/ListPreference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    iget-object v2, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/HDMISettings;->mToggleVideoPref:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mediatek.hdmi.localservice.action.CABLE_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/settings/HDMISettings;->mLocalServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/HDMISettings;->mHDMIServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unbindService(Landroid/content/ServiceConnection;)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/android/settings/HDMISettings;->mLocalServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    iget-object v5, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    if-nez v5, :cond_0

    const-string v4, "hdmi"

    const-string v5, "HDMISettings  -- Connection to HDMI local service have not been established."

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v3

    :cond_0
    const-string v5, "hdmi_toggler"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v5, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-virtual {v5, v0}, Lcom/mediatek/hdmi/HDMILocalService;->enableHDMI(Z)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hdmi_enable_status"

    if-eqz v0, :cond_1

    move v3, v4

    :cond_1
    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-direct {p0}, Lcom/android/settings/HDMISettings;->updateSettingsItemEnableStatus()V

    :cond_2
    :goto_1
    move v3, v4

    goto :goto_0

    :cond_3
    const-string v5, "audio_toggler"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v5, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-virtual {v5, v0}, Lcom/mediatek/hdmi/HDMILocalService;->enableAudio(Z)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hdmi_audio_status"

    if-eqz v0, :cond_4

    move v3, v4

    :cond_4
    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_5
    const-string v5, "video_toggler"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v5, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-virtual {v5, v0}, Lcom/mediatek/hdmi/HDMILocalService;->enableVideo(Z)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "hdmi_video_status"

    if-eqz v0, :cond_6

    move v3, v4

    :cond_6
    invoke-static {v5, v6, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1

    :cond_7
    const-string v3, "video_resolution"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v2, p2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/android/settings/HDMISettings;->mHDMIService:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/mediatek/hdmi/HDMILocalService;->setVideoResolution(I)Z

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v5, "hdmi_video_resolution"

    invoke-static {v3, v5, v2}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_1
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/HDMISettings;->updatePref()V

    invoke-direct {p0}, Lcom/android/settings/HDMISettings;->updateSettingsItemEnableStatus()V

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    return-void
.end method
