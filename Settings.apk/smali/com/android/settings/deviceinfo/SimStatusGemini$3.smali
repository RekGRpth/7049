.class Lcom/android/settings/deviceinfo/SimStatusGemini$3;
.super Landroid/telephony/PhoneStateListener;
.source "SimStatusGemini.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/settings/deviceinfo/SimStatusGemini;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;


# direct methods
.method constructor <init>(Lcom/android/settings/deviceinfo/SimStatusGemini;)V
    .locals 0

    iput-object p1, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataConnectionStateChanged(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$400(Lcom/android/settings/deviceinfo/SimStatusGemini;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-static {v0}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$500(Lcom/android/settings/deviceinfo/SimStatusGemini;)V

    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 2
    .param p1    # Landroid/telephony/ServiceState;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getMySimId()I

    move-result v0

    invoke-static {}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$600()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$802(Lcom/android/settings/deviceinfo/SimStatusGemini;I)I

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-static {v0, p1}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$900(Lcom/android/settings/deviceinfo/SimStatusGemini;Landroid/telephony/ServiceState;)V

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-static {}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$600()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/SimStatusGemini;->updateSignalStrength(I)V

    :cond_0
    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 3
    .param p1    # Landroid/telephony/SignalStrength;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getMySimId()I

    move-result v0

    invoke-static {}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$600()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-static {v0, p1}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$1002(Lcom/android/settings/deviceinfo/SimStatusGemini;Landroid/telephony/SignalStrength;)Landroid/telephony/SignalStrength;

    iget-object v0, p0, Lcom/android/settings/deviceinfo/SimStatusGemini$3;->this$0:Lcom/android/settings/deviceinfo/SimStatusGemini;

    invoke-static {}, Lcom/android/settings/deviceinfo/SimStatusGemini;->access$600()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/settings/deviceinfo/SimStatusGemini;->updateSignalStrength(I)V

    const-string v0, "Gemini_SimStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "message mGeminiPhone number is : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MySimId is"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getMySimId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
