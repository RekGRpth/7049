.class public Lcom/android/settings/ext/DefaultAudioProfileExt;
.super Landroid/content/ContextWrapper;
.source "DefaultAudioProfileExt.java"

# interfaces
.implements Lcom/android/settings/ext/IAudioProfileExt;


# instance fields
.field private mCheckboxButton:Landroid/widget/RadioButton;

.field private mContext:Landroid/app/Fragment;

.field private mHasMoreRingtone:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayout:Landroid/view/View;

.field private mSummary:Landroid/widget/TextView;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mTextView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mCheckboxButton:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mSummary:Landroid/widget/TextView;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mHasMoreRingtone:Z

    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public createView(I)Landroid/view/View;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mInflater:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mLayout:Landroid/view/View;

    return-object v0
.end method

.method public getPrefRadioButton(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mLayout:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mCheckboxButton:Landroid/widget/RadioButton;

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mCheckboxButton:Landroid/widget/RadioButton;

    return-object v0
.end method

.method public getPreferenceSummary(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mLayout:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mSummary:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mSummary:Landroid/widget/TextView;

    return-object v0
.end method

.method public getPreferenceTitle(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mLayout:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method public onPreferenceTreeClick(Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;Landroid/app/Fragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;
    .param p2    # Landroid/app/Fragment;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;->GENERAL:Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-virtual {v0, p1}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;->CUSTOM:Lcom/mediatek/audioprofile/AudioProfileManager$Scenario;

    invoke-virtual {v0, p1}, Ljava/lang/Enum;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object p2, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mContext:Landroid/app/Fragment;

    const-string v0, "profileKey"

    invoke-virtual {v2, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mContext:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceActivity;

    move-object v1, p4

    move-object v5, v4

    move v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/preference/PreferenceActivity;->startPreferencePanel(Ljava/lang/String;Landroid/os/Bundle;ILjava/lang/CharSequence;Landroid/app/Fragment;I)V

    :cond_1
    return-void
.end method

.method public setRingerVolume(Landroid/media/AudioManager;I)V
    .locals 2
    .param p1    # Landroid/media/AudioManager;
    .param p2    # I

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-virtual {p1, v0, p2, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    const/4 v0, 0x5

    invoke-virtual {p1, v0, p2, v1}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method

.method public setRingtonePickerParams(Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Intent;

    const-string v0, "android.intent.extra.ringtone.SHOW_MORE_RINGTONES"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/settings/ext/DefaultAudioProfileExt;->mHasMoreRingtone:Z

    return-void
.end method

.method public setVolume(Landroid/media/AudioManager;II)V
    .locals 1
    .param p1    # Landroid/media/AudioManager;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    return-void
.end method
