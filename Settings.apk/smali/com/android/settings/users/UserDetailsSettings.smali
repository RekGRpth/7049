.class public Lcom/android/settings/users/UserDetailsSettings;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "UserDetailsSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/android/settings/DialogCreatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/settings/users/UserDetailsSettings$AppState;
    }
.end annotation


# static fields
.field private static final DIALOG_CONFIRM_REMOVE:I = 0x1

.field public static final EXTRA_USER_ID:Ljava/lang/String; = "user_id"

.field private static final KEY_INSTALLED_APPS:Ljava/lang/String; = "market_apps_category"

.field private static final KEY_SYSTEM_APPS:Ljava/lang/String; = "system_apps_category"

.field private static final KEY_USER_NAME:Ljava/lang/String; = "user_name"

.field private static final MENU_REMOVE_USER:I = 0x1

.field private static final SYSTEM_APPS:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "UserDetailsSettings"


# instance fields
.field private mAppStates:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/settings/users/UserDetailsSettings$AppState;",
            ">;"
        }
    .end annotation
.end field

.field private mIPm:Landroid/content/pm/IPackageManager;

.field private mInstalledAppGroup:Landroid/preference/PreferenceGroup;

.field private mNamePref:Landroid/preference/EditTextPreference;

.field private mNewUser:Z

.field private mPm:Landroid/content/pm/PackageManager;

.field private mSystemAppGroup:Landroid/preference/PreferenceGroup;

.field private mUserId:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.google.android.browser"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.google.android.gm"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.google.android.youtube"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/settings/users/UserDetailsSettings;->SYSTEM_APPS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/settings/users/UserDetailsSettings;->mAppStates:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/android/settings/users/UserDetailsSettings;)V
    .locals 0
    .param p0    # Lcom/android/settings/users/UserDetailsSettings;

    invoke-direct {p0}, Lcom/android/settings/users/UserDetailsSettings;->removeUserNow()V

    return-void
.end method

.method private initExistingUser()V
    .locals 6

    iget-object v4, p0, Lcom/android/settings/users/UserDetailsSettings;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v4}, Landroid/content/pm/PackageManager;->getUsers()Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    iget v4, v2, Landroid/content/pm/UserInfo;->id:I

    iget v5, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    if-ne v4, v5, :cond_0

    move-object v0, v2

    :cond_1
    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    iget-object v5, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    iget-object v5, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method private initNewUser()V
    .locals 3

    const v2, 0x7f0b0848

    iget-object v0, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/EditTextPreference;->setText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private insertAppInfo(Landroid/preference/PreferenceGroup;Ljava/util/HashMap;Landroid/content/pm/PackageInfo;Z)V
    .locals 8
    .param p1    # Landroid/preference/PreferenceGroup;
    .param p3    # Landroid/content/pm/PackageInfo;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/preference/PreferenceGroup;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/settings/users/UserDetailsSettings$AppState;",
            ">;",
            "Landroid/content/pm/PackageInfo;",
            "Z)V"
        }
    .end annotation

    if-eqz p3, :cond_0

    iget-object v5, p3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget-object v6, p3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageItemInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v6, p3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/settings/users/UserDetailsSettings$AppState;

    if-nez v1, :cond_1

    move v2, p4

    :goto_0
    new-instance v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v0, v6}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    if-eqz v4, :cond_2

    :goto_1
    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setIcon(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v0, v2}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Landroid/preference/Preference;->setPersistent(Z)V

    invoke-virtual {v0, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :cond_0
    return-void

    :cond_1
    iget-boolean v2, v1, Lcom/android/settings/users/UserDetailsSettings$AppState;->enabled:Z

    goto :goto_0

    :cond_2
    move-object v4, v5

    goto :goto_1
.end method

.method private onRemoveUserClicked()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/settings/users/UserDetailsSettings;->mNewUser:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/users/UserDetailsSettings;->removeUserNow()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/settings/SettingsPreferenceFragment;->showDialog(I)V

    goto :goto_0
.end method

.method private refreshApps()V
    .locals 12

    const/4 v8, 0x0

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mSystemAppGroup:Landroid/preference/PreferenceGroup;

    invoke-virtual {v7}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mInstalledAppGroup:Landroid/preference/PreferenceGroup;

    invoke-virtual {v7}, Landroid/preference/PreferenceGroup;->removeAll()V

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mAppStates:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    new-instance v4, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    const/4 v9, 0x0

    invoke-direct {v4, v7, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v7, "android.intent.category.LAUNCHER"

    invoke-virtual {v4, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mPm:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v4, v8}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    :try_start_0
    iget-object v9, p0, Lcom/android/settings/users/UserDetailsSettings;->mIPm:Landroid/content/pm/IPackageManager;

    iget-object v7, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v7, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    const/4 v11, 0x0

    iget v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    if-gez v7, :cond_2

    move v7, v8

    :goto_1
    invoke-interface {v9, v10, v11, v7}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-eqz v1, :cond_1

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mAppStates:Ljava/util/HashMap;

    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    new-instance v10, Lcom/android/settings/users/UserDetailsSettings$AppState;

    iget-object v11, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v11, v11, Landroid/content/pm/ApplicationInfo;->enabled:Z

    invoke-direct {v10, v11}, Lcom/android/settings/users/UserDetailsSettings$AppState;-><init>(Z)V

    invoke-virtual {v7, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v7, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v7, v7, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mSystemAppGroup:Landroid/preference/PreferenceGroup;

    iget-object v9, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mSystemAppGroup:Landroid/preference/PreferenceGroup;

    iget-object v9, p0, Lcom/android/settings/users/UserDetailsSettings;->mAppStates:Ljava/util/HashMap;

    invoke-direct {p0, v7, v9, v3, v8}, Lcom/android/settings/users/UserDetailsSettings;->insertAppInfo(Landroid/preference/PreferenceGroup;Ljava/util/HashMap;Landroid/content/pm/PackageInfo;Z)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v5

    goto :goto_0

    :cond_3
    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mInstalledAppGroup:Landroid/preference/PreferenceGroup;

    iget-object v9, v3, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v9}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/android/settings/users/UserDetailsSettings;->mInstalledAppGroup:Landroid/preference/PreferenceGroup;

    iget-object v9, p0, Lcom/android/settings/users/UserDetailsSettings;->mAppStates:Ljava/util/HashMap;

    invoke-direct {p0, v7, v9, v3, v8}, Lcom/android/settings/users/UserDetailsSettings;->insertAppInfo(Landroid/preference/PreferenceGroup;Ljava/util/HashMap;Landroid/content/pm/PackageInfo;Z)V

    goto :goto_0

    :cond_4
    return-void
.end method

.method private removeUserNow()V
    .locals 4

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mIPm:Landroid/content/pm/IPackageManager;

    iget v2, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    invoke-interface {v1, v2}, Landroid/content/pm/IPackageManager;->removeUser(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finish()V

    return-void

    :catch_0
    move-exception v0

    const-string v1, "UserDetailsSettings"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t remove user "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f050045

    invoke-virtual {p0, v4}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v4, "user_id"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v3, :cond_1

    :cond_0
    move v1, v2

    :cond_1
    iput-boolean v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mNewUser:Z

    iget-boolean v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mNewUser:Z

    if-eqz v1, :cond_3

    move v1, v3

    :goto_0
    iput v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    const-string v1, "package"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mIPm:Landroid/content/pm/IPackageManager;

    iget v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    if-ne v1, v3, :cond_2

    :try_start_0
    iget-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mIPm:Landroid/content/pm/IPackageManager;

    const v3, 0x7f0b0848

    invoke-virtual {p0, v3}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/pm/IPackageManager;->createUser(Ljava/lang/String;I)Landroid/content/pm/UserInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/UserInfo;->id:I

    iput v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const-string v1, "system_apps_category"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceGroup;

    iput-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mSystemAppGroup:Landroid/preference/PreferenceGroup;

    const-string v1, "market_apps_category"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceGroup;

    iput-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mInstalledAppGroup:Landroid/preference/PreferenceGroup;

    const-string v1, "user_name"

    invoke-virtual {p0, v1}, Landroid/preference/PreferenceFragment;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/EditTextPreference;

    iput-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    iget-object v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    invoke-virtual {v1, p0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p0, v2}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    return-void

    :cond_3
    const-string v1, "user_id"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .param p1    # I

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b0849

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b084a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lcom/android/settings/users/UserDetailsSettings$1;

    invoke-direct {v3, p0}, Lcom/android/settings/users/UserDetailsSettings$1;-><init>(Lcom/android/settings/users/UserDetailsSettings;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/android/settings/users/UserDetailsSettings;->mNewUser:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0b0846

    :goto_0
    invoke-interface {p1, v3, v2, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    return-void

    :cond_0
    const v1, 0x7f0b0847

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/android/settings/users/UserDetailsSettings;->onRemoveUserClicked()V

    :goto_0
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 10
    .param p1    # Landroid/preference/Preference;
    .param p2    # Ljava/lang/Object;

    const/4 v6, 0x1

    const/4 v7, 0x0

    instance-of v5, p1, Landroid/preference/CheckBoxPreference;

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_1

    move v2, v6

    :goto_0
    :try_start_0
    iget-object v5, p0, Lcom/android/settings/users/UserDetailsSettings;->mIPm:Landroid/content/pm/IPackageManager;

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    invoke-interface {v5, v3, v2, v7, v8}, Landroid/content/pm/IPackageManager;->setApplicationEnabledSetting(Ljava/lang/String;III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return v6

    :cond_1
    const/4 v2, 0x3

    goto :goto_0

    :catch_0
    move-exception v4

    const-string v5, "UserDetailsSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unable to change enabled state of package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for user "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    iget-object v5, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    if-ne p1, v5, :cond_0

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v6, v7

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/android/settings/users/UserDetailsSettings;->mIPm:Landroid/content/pm/IPackageManager;

    iget v9, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    move-object v5, v0

    invoke-interface {v8, v9, v5}, Landroid/content/pm/IPackageManager;->updateUserName(ILjava/lang/String;)V

    iget-object v5, p0, Lcom/android/settings/users/UserDetailsSettings;->mNamePref:Landroid/preference/EditTextPreference;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v5, p2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v4

    move v6, v7

    goto :goto_1
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/settings/users/UserDetailsSettings;->mPm:Landroid/content/pm/PackageManager;

    iget v0, p0, Lcom/android/settings/users/UserDetailsSettings;->mUserId:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/android/settings/users/UserDetailsSettings;->initExistingUser()V

    :goto_0
    invoke-direct {p0}, Lcom/android/settings/users/UserDetailsSettings;->refreshApps()V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/android/settings/users/UserDetailsSettings;->initNewUser()V

    goto :goto_0
.end method
