.class Lcom/mediatek/audioprofile/Editprofile$1;
.super Ljava/lang/Object;
.source "Editprofile.java"

# interfaces
.implements Ljava/util/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/audioprofile/Editprofile;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/audioprofile/Editprofile;

.field final synthetic val$name:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/mediatek/audioprofile/Editprofile;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/audioprofile/Editprofile$1;->this$0:Lcom/mediatek/audioprofile/Editprofile;

    iput-object p2, p0, Lcom/mediatek/audioprofile/Editprofile$1;->val$name:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/util/Observable;
    .param p2    # Ljava/lang/Object;

    const-string v1, "Settings/EditProfile"

    const-string v2, "update"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/audioprofile/Editprofile$1;->this$0:Lcom/mediatek/audioprofile/Editprofile;

    invoke-static {v1}, Lcom/mediatek/audioprofile/Editprofile;->access$000(Lcom/mediatek/audioprofile/Editprofile;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/audioprofile/Editprofile$1;->this$0:Lcom/mediatek/audioprofile/Editprofile;

    invoke-static {v1}, Lcom/mediatek/audioprofile/Editprofile;->access$100(Lcom/mediatek/audioprofile/Editprofile;)Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/audioprofile/Editprofile$1;->val$name:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/mediatek/audioprofile/Editprofile$1;->this$0:Lcom/mediatek/audioprofile/Editprofile;

    invoke-static {v1}, Lcom/mediatek/audioprofile/Editprofile;->access$000(Lcom/mediatek/audioprofile/Editprofile;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    const-string v2, "true"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/preference/TwoStatePreference;->setChecked(Z)V

    const-string v1, "Settings/EditProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "vibrate setting is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "true"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
