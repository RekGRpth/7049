.class public Lcom/mediatek/batterywarning/BatteryNotifyCodes;
.super Ljava/lang/Object;
.source "BatteryNotifyCodes.java"


# static fields
.field public static final ACTION_BATTER_OVER_TEMPERATURE:Ljava/lang/String; = "mediatek.intent.action.BATTER_OVER_TEMPERATURE"

.field public static final ACTION_BATTER_OVER_VOLTAGE:Ljava/lang/String; = "mediatek.intent.action.BATTER_OVER_VOLTAGE"

.field public static final ACTION_CHARGER_OVER_VOLTAGE:Ljava/lang/String; = "mediatek.intent.action.CHARGER_OVER_VOLTAGE"

.field public static final ACTION_OVER_CURRENT_PROTECTION:Ljava/lang/String; = "mediatek.intent.action.OVER_CURRENT_PROTECTION"

.field public static final ACTION_SAFETY_TIMER_TIMEOUT:Ljava/lang/String; = "mediatek.intent.action.SAFETY_TIMER_TIMEOUT"

.field public static final BATTERY_OK:I = 0x0

.field public static final BATTER_OVER_TEMPERATURE:I = 0x2

.field public static final BATTER_OVER_VOLTAGE:I = 0x8

.field public static final CHARGER_OVER_VOLTAGE:I = 0x1

.field public static final OVER_CURRENT_PROTECTION:I = 0x4

.field public static final SAFETY_TIMER_TIMEOUT:I = 0x10


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
