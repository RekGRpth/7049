.class public Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BatteryWarningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/batterywarning/BatteryWarningService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BatteryWarningMessageReceiver"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mediatek.intent.action.CHARGER_OVER_VOLTAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "Settings/BW"

    const-string v2, "WarningMessage:receiver: over charger voltage, please disconnect charger"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$000()Z

    move-result v1

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$100()Landroid/app/AlertDialog;

    move-result-object v2

    sget-object v3, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->CHARGER_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-static {p1, v1, v2, v3}, Lcom/mediatek/batterywarning/BatteryWarningService;->showWarningDialog(Landroid/content/Context;ZLandroid/app/AlertDialog;Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "mediatek.intent.action.BATTER_OVER_TEMPERATURE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "Settings/BW"

    const-string v2, "WarningMessage:receiver: over battery temperature, please remove battery"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$200()Z

    move-result v1

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$300()Landroid/app/AlertDialog;

    move-result-object v2

    sget-object v3, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->BATTERY_OVER_TEMP:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-static {p1, v1, v2, v3}, Lcom/mediatek/batterywarning/BatteryWarningService;->showWarningDialog(Landroid/content/Context;ZLandroid/app/AlertDialog;Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;)V

    goto :goto_0

    :cond_2
    const-string v1, "mediatek.intent.action.OVER_CURRENT_PROTECTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "Settings/BW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WarningMessage:receiver: over current-protection, please disconnect charger"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$400()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$400()Z

    move-result v1

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$500()Landroid/app/AlertDialog;

    move-result-object v2

    sget-object v3, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->OVER_CUR_PROTENCTION:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-static {p1, v1, v2, v3}, Lcom/mediatek/batterywarning/BatteryWarningService;->showWarningDialog(Landroid/content/Context;ZLandroid/app/AlertDialog;Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;)V

    goto :goto_0

    :cond_3
    const-string v1, "mediatek.intent.action.BATTER_OVER_VOLTAGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "Settings/BW"

    const-string v2, "WarningMessage:receiver: over battery voltage, please remove battery"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$600()Z

    move-result v1

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$700()Landroid/app/AlertDialog;

    move-result-object v2

    sget-object v3, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->BATTERY_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-static {p1, v1, v2, v3}, Lcom/mediatek/batterywarning/BatteryWarningService;->showWarningDialog(Landroid/content/Context;ZLandroid/app/AlertDialog;Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;)V

    goto :goto_0

    :cond_4
    const-string v1, "mediatek.intent.action.SAFETY_TIMER_TIMEOUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "Settings/BW"

    const-string v2, "WarningMessage:receiver: over 12 hours, battery does not charge full, please disconnect charger"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$800()Z

    move-result v1

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$900()Landroid/app/AlertDialog;

    move-result-object v2

    sget-object v3, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->SAFETY_TIMEOUT:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-static {p1, v1, v2, v3}, Lcom/mediatek/batterywarning/BatteryWarningService;->showWarningDialog(Landroid/content/Context;ZLandroid/app/AlertDialog;Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;)V

    goto/16 :goto_0

    :cond_5
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$100()Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$100()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$100()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_6
    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$900()Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$900()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$900()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_7
    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$700()Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$700()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mediatek/batterywarning/BatteryWarningService;->access$700()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    goto/16 :goto_0
.end method
