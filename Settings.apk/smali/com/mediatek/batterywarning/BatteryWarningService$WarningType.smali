.class final enum Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;
.super Ljava/lang/Enum;
.source "BatteryWarningService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/batterywarning/BatteryWarningService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "WarningType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

.field public static final enum BATTERY_OVER_TEMP:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

.field public static final enum BATTERY_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

.field public static final enum CHARGER_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

.field public static final enum OVER_CUR_PROTENCTION:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

.field public static final enum SAFETY_TIMEOUT:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const-string v1, "CHARGER_OVER_VOL"

    invoke-direct {v0, v1, v2}, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->CHARGER_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const-string v1, "BATTERY_OVER_TEMP"

    invoke-direct {v0, v1, v3}, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->BATTERY_OVER_TEMP:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const-string v1, "OVER_CUR_PROTENCTION"

    invoke-direct {v0, v1, v4}, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->OVER_CUR_PROTENCTION:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const-string v1, "BATTERY_OVER_VOL"

    invoke-direct {v0, v1, v5}, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->BATTERY_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const-string v1, "SAFETY_TIMEOUT"

    invoke-direct {v0, v1, v6}, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->SAFETY_TIMEOUT:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->CHARGER_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->BATTERY_OVER_TEMP:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->OVER_CUR_PROTENCTION:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->BATTERY_OVER_VOL:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->SAFETY_TIMEOUT:Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->$VALUES:[Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    return-object v0
.end method

.method public static values()[Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;->$VALUES:[Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    invoke-virtual {v0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    return-object v0
.end method
