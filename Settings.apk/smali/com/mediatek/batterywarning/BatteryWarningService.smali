.class public Lcom/mediatek/batterywarning/BatteryWarningService;
.super Landroid/app/Service;
.source "BatteryWarningService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mediatek/batterywarning/BatteryWarningService$3;,
        Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;,
        Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;
    }
.end annotation


# static fields
.field private static final FILE_BATTERY_NOTIFY_CODE:Ljava/lang/String; = "/sys/devices/platform/mt-battery/BatteryNotify"

.field private static final TAG:Ljava/lang/String; = "WarningMessage:"

.field private static final WARNING_SOUND_URI:Landroid/net/Uri;

.field private static final XLOGTAG:Ljava/lang/String; = "Settings/BW"

.field private static sBatteryOverTemperatureDialog:Landroid/app/AlertDialog;

.field private static sBatteryrOverVoltageDialog:Landroid/app/AlertDialog;

.field private static sChargerOverVoltageDialog:Landroid/app/AlertDialog;

.field private static sOnNegativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private static sOnPositiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private static sOverCurrentProtectionDialog:Landroid/app/AlertDialog;

.field private static sRingtone:Landroid/media/Ringtone;

.field private static sSatetyTimerTimeoutDialog:Landroid/app/AlertDialog;

.field private static sShowBatteryOverTemperatureDialog:Z

.field private static sShowBatteryrOverVoltageDialog:Z

.field private static sShowChargerOverVoltageDialog:Z

.field private static sShowOverCurrentProtectionDialog:Z

.field private static sShowSatetyTimerTimeoutDialog:Z

.field private static sWarningMsg:[I

.field private static sWarningTitle:[I


# instance fields
.field private mReceiver:Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x5

    const/4 v1, 0x1

    const-string v0, "file:///system/media/audio/ui/VideoRecord.ogg"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->WARNING_SOUND_URI:Landroid/net/Uri;

    sput-boolean v1, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowChargerOverVoltageDialog:Z

    sput-boolean v1, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowBatteryOverTemperatureDialog:Z

    sput-boolean v1, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowOverCurrentProtectionDialog:Z

    sput-boolean v1, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowBatteryrOverVoltageDialog:Z

    sput-boolean v1, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowSatetyTimerTimeoutDialog:Z

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sWarningTitle:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sWarningMsg:[I

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$1;

    invoke-direct {v0}, Lcom/mediatek/batterywarning/BatteryWarningService$1;-><init>()V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sOnNegativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$2;

    invoke-direct {v0}, Lcom/mediatek/batterywarning/BatteryWarningService$2;-><init>()V

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sOnPositiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0b0163
        0x7f0b0164
        0x7f0b0165
        0x7f0b0166
        0x7f0b0167
    .end array-data

    :array_1
    .array-data 4
        0x7f0b0168
        0x7f0b0169
        0x7f0b016a
        0x7f0b016b
        0x7f0b016c
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowChargerOverVoltageDialog:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowChargerOverVoltageDialog:Z

    return p0
.end method

.method static synthetic access$100()Landroid/app/AlertDialog;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sChargerOverVoltageDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1000()Landroid/media/Ringtone;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sRingtone:Landroid/media/Ringtone;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowBatteryOverTemperatureDialog:Z

    return v0
.end method

.method static synthetic access$202(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowBatteryOverTemperatureDialog:Z

    return p0
.end method

.method static synthetic access$300()Landroid/app/AlertDialog;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sBatteryOverTemperatureDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$400()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowOverCurrentProtectionDialog:Z

    return v0
.end method

.method static synthetic access$402(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowOverCurrentProtectionDialog:Z

    return p0
.end method

.method static synthetic access$500()Landroid/app/AlertDialog;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sOverCurrentProtectionDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$600()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowBatteryrOverVoltageDialog:Z

    return v0
.end method

.method static synthetic access$602(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowBatteryrOverVoltageDialog:Z

    return p0
.end method

.method static synthetic access$700()Landroid/app/AlertDialog;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sBatteryrOverVoltageDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$800()Z
    .locals 1

    sget-boolean v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowSatetyTimerTimeoutDialog:Z

    return v0
.end method

.method static synthetic access$802(Z)Z
    .locals 0
    .param p0    # Z

    sput-boolean p0, Lcom/mediatek/batterywarning/BatteryWarningService;->sShowSatetyTimerTimeoutDialog:Z

    return p0
.end method

.method static synthetic access$900()Landroid/app/AlertDialog;
    .locals 1

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sSatetyTimerTimeoutDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static playAlertSound(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Landroid/net/Uri;

    if-eqz p1, :cond_0

    invoke-static {p0, p1}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    move-result-object v0

    sput-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sRingtone:Landroid/media/Ringtone;

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sRingtone:Landroid/media/Ringtone;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sRingtone:Landroid/media/Ringtone;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/Ringtone;->setStreamType(I)V

    sget-object v0, Lcom/mediatek/batterywarning/BatteryWarningService;->sRingtone:Landroid/media/Ringtone;

    invoke-virtual {v0}, Landroid/media/Ringtone;->play()V

    :cond_0
    return-void
.end method

.method static showWarningDialog(Landroid/content/Context;ZLandroid/app/AlertDialog;Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Z
    .param p2    # Landroid/app/AlertDialog;
    .param p3    # Lcom/mediatek/batterywarning/BatteryWarningService$WarningType;

    const-string v1, "Settings/BW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WarningMessage:showDialog:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, p3

    if-eqz p1, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService;->sWarningTitle:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    sget-object v2, Lcom/mediatek/batterywarning/BatteryWarningService;->sWarningMsg:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const v3, 0x7f020014

    invoke-static {p0, v1, v2, v3}, Lcom/mediatek/batterywarning/BatteryWarningService;->warningMessageDialog(Landroid/content/Context;III)Landroid/app/AlertDialog;

    move-result-object p2

    invoke-virtual {p2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    invoke-virtual {p2}, Landroid/app/Dialog;->show()V

    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService;->WARNING_SOUND_URI:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/mediatek/batterywarning/BatteryWarningService;->playAlertSound(Landroid/content/Context;Landroid/net/Uri;)V

    const-string v1, "Settings/BW"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WarningMessage:create & show dialog:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    sget-object v1, Lcom/mediatek/batterywarning/BatteryWarningService$3;->$SwitchMap$com$mediatek$batterywarning$BatteryWarningService$WarningType:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sput-object p2, Lcom/mediatek/batterywarning/BatteryWarningService;->sChargerOverVoltageDialog:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    sput-object p2, Lcom/mediatek/batterywarning/BatteryWarningService;->sBatteryOverTemperatureDialog:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_2
    sput-object p2, Lcom/mediatek/batterywarning/BatteryWarningService;->sOverCurrentProtectionDialog:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_3
    sput-object p2, Lcom/mediatek/batterywarning/BatteryWarningService;->sBatteryrOverVoltageDialog:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_4
    sput-object p2, Lcom/mediatek/batterywarning/BatteryWarningService;->sSatetyTimerTimeoutDialog:Landroid/app/AlertDialog;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method static warningMessageDialog(Landroid/content/Context;III)Landroid/app/AlertDialog;
    .locals 6
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const v4, 0x7f04000c

    const/4 v5, 0x0

    invoke-static {p0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f080016

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(I)V

    const v4, 0x7f080017

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v4, 0x1010355

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b016d

    sget-object v5, Lcom/mediatek/batterywarning/BatteryWarningService;->sOnPositiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v4, 0x7f0b016e

    sget-object v5, Lcom/mediatek/batterywarning/BatteryWarningService;->sOnNegativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 9

    const-wide/16 v2, 0x2710

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "Settings/BW"

    const-string v1, "WarningMessage:Battery Warning Service: onCreate()"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v6, 0x2710

    const/16 v8, 0x2710

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/mediatek/batterywarning/ReadCodeTask;

    const-string v4, "/sys/devices/platform/mt-battery/BatteryNotify"

    invoke-direct {v1, p0, v4}, Lcom/mediatek/batterywarning/ReadCodeTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    const-string v0, "Settings/BW"

    const-string v1, "WarningMessage:Task schedule started: delay--10000 period--10000"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;

    invoke-direct {v0}, Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;-><init>()V

    iput-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mReceiver:Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;

    new-instance v7, Landroid/content/IntentFilter;

    const-string v0, "mediatek.intent.action.CHARGER_OVER_VOLTAGE"

    invoke-direct {v7, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v0, "mediatek.intent.action.BATTER_OVER_TEMPERATURE"

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "mediatek.intent.action.OVER_CURRENT_PROTECTION"

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "mediatek.intent.action.BATTER_OVER_VOLTAGE"

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "mediatek.intent.action.SAFETY_TIMER_TIMEOUT"

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v7, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mReceiver:Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;

    invoke-virtual {p0, v0, v7}, Landroid/content/ContextWrapper;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/mediatek/batterywarning/BatteryWarningService;->mReceiver:Lcom/mediatek/batterywarning/BatteryWarningService$BatteryWarningMessageReceiver;

    invoke-virtual {p0, v0}, Landroid/content/ContextWrapper;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
