.class Lcom/mediatek/gemini/SimDataRoamingSettings$2;
.super Ljava/lang/Object;
.source "SimDataRoamingSettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mediatek/gemini/SimDataRoamingSettings;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

.field final synthetic val$simInfoPref:Lcom/mediatek/gemini/SimInfoPreference;


# direct methods
.method constructor <init>(Lcom/mediatek/gemini/SimDataRoamingSettings;Lcom/mediatek/gemini/SimInfoPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

    iput-object p2, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->val$simInfoPref:Lcom/mediatek/gemini/SimInfoPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v4, 0x1

    :try_start_0
    iget-object v1, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

    invoke-static {v1}, Lcom/mediatek/gemini/SimDataRoamingSettings;->access$000(Lcom/mediatek/gemini/SimDataRoamingSettings;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

    invoke-static {v1}, Lcom/mediatek/gemini/SimDataRoamingSettings;->access$000(Lcom/mediatek/gemini/SimDataRoamingSettings;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

    invoke-static {v3}, Lcom/mediatek/gemini/SimDataRoamingSettings;->access$100(Lcom/mediatek/gemini/SimDataRoamingSettings;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/android/internal/telephony/ITelephony;->setDataRoamingEnabledGemini(ZI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    iget-object v1, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->this$0:Lcom/mediatek/gemini/SimDataRoamingSettings;

    invoke-static {v2}, Lcom/mediatek/gemini/SimDataRoamingSettings;->access$200(Lcom/mediatek/gemini/SimDataRoamingSettings;)J

    move-result-wide v2

    invoke-static {v1, v4, v2, v3}, Landroid/provider/Telephony$SIMInfo;->setDataRoaming(Landroid/content/Context;IJ)I

    iget-object v1, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->val$simInfoPref:Lcom/mediatek/gemini/SimInfoPreference;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/mediatek/gemini/SimDataRoamingSettings$2;->val$simInfoPref:Lcom/mediatek/gemini/SimInfoPreference;

    invoke-virtual {v1, v4}, Lcom/mediatek/gemini/SimInfoPreference;->setCheck(Z)V

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "SimDataRoamingSettings"

    const-string v2, "mTelephony exception"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
