.class Lcom/mediatek/hdmi/HDMILocalService$1;
.super Landroid/telephony/PhoneStateListener;
.source "HDMILocalService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/hdmi/HDMILocalService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/hdmi/HDMILocalService;


# direct methods
.method constructor <init>(Lcom/mediatek/hdmi/HDMILocalService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-string v2, "hdmi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " >> HDMILocalService. Phone state changed, new state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    invoke-static {v5}, Lcom/mediatek/hdmi/HDMILocalService;->access$302(Z)Z

    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$400(Lcom/mediatek/hdmi/HDMILocalService;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$500(Lcom/mediatek/hdmi/HDMILocalService;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    const-string v4, "phone"

    invoke-virtual {v2, v4}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/telephony/TelephonyManager;

    invoke-static {v3, v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$502(Lcom/mediatek/hdmi/HDMILocalService;Landroid/telephony/TelephonyManager;)Landroid/telephony/TelephonyManager;

    :cond_1
    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$500(Lcom/mediatek/hdmi/HDMILocalService;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/telephony/TelephonyManager;->getCallStateGemini(I)I

    move-result v0

    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$500(Lcom/mediatek/hdmi/HDMILocalService;)Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/telephony/TelephonyManager;->getCallStateGemini(I)I

    move-result v1

    const-string v2, "hdmi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " >> HDMILocalService.phone state change, sim1="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", sim2="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    const-string v2, "hdmi"

    const-string v3, " >> HDMILocalService. phone is not idle for gemini phone"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v5}, Lcom/mediatek/hdmi/HDMILocalService;->access$302(Z)Z

    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$400(Lcom/mediatek/hdmi/HDMILocalService;)V

    goto :goto_0

    :cond_3
    invoke-static {v6}, Lcom/mediatek/hdmi/HDMILocalService;->access$302(Z)Z

    iget-object v2, p0, Lcom/mediatek/hdmi/HDMILocalService$1;->this$0:Lcom/mediatek/hdmi/HDMILocalService;

    invoke-static {v2}, Lcom/mediatek/hdmi/HDMILocalService;->access$400(Lcom/mediatek/hdmi/HDMILocalService;)V

    goto :goto_0
.end method
