.class public Lcom/mediatek/lbs/AgpsNotifyDialog;
.super Landroid/app/Activity;
.source "AgpsNotifyDialog.java"


# static fields
.field private static final AGPS_ENABLE_KEY:Ljava/lang/String; = "location_agps_enable"

.field private static final DISABLE_KEY:Ljava/lang/String; = "disable_agps_on_reboot"

.field private static final EM_ENABLE_KEY:Ljava/lang/String; = "EM_Indication"

.field private static final EM_STRING_LIST:[I

.field private static final ERROR_STRING_LIST:[I

.field private static final IND_EM_DIALOG_ID:I = 0x0

.field private static final IND_ERROR_DIALOG_ID:I = 0x3

.field private static final INFO_STRING_LIST:[I

.field private static final NOTIFY_ALLOW_NO_ANSWER_DIALOG_ID:I = 0x1

.field private static final NOTIFY_ALLOW_NO_DENY_DIALOG_ID:I = 0x2

.field private static final NOTIFY_ONLY_DIALOG_ID:I = 0x4

.field private static final NOTIFY_STRING_LIST:[I

.field private static final PREFERENCE_FILE:Ljava/lang/String; = "com.android.settings_preferences"

.field private static final SEC_TO_MILLSEC:I = 0x3e8

.field private static final UNKNOWN_VALUE:Ljava/lang/String; = "UNKNOWN_VALUE"

.field private static final XLOGTAG:Ljava/lang/String; = "Settings/Agps"

.field protected static sAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;


# instance fields
.field private mCliecntName:Ljava/lang/String;

.field private mDialog:Landroid/app/Dialog;

.field private mGetOtherNotify:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsUserResponse:Z

.field private mMessage:Ljava/lang/String;

.field private mRequestId:Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/lbs/AgpsNotifyDialog;->ERROR_STRING_LIST:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/mediatek/lbs/AgpsNotifyDialog;->EM_STRING_LIST:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/mediatek/lbs/AgpsNotifyDialog;->NOTIFY_STRING_LIST:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f0b0023

    aput v2, v0, v1

    sput-object v0, Lcom/mediatek/lbs/AgpsNotifyDialog;->INFO_STRING_LIST:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0b0023
        0x7f0b0024
        0x7f0b0025
        0x7f0b0026
        0x7f0b0027
        0x7f0b0028
        0x7f0b0029
        0x7f0b002a
        0x7f0b002b
        0x7f0b002c
        0x7f0b002d
        0x7f0b002e
        0x7f0b002f
        0x7f0b0030
        0x7f0b0031
        0x7f0b0032
    .end array-data

    :array_1
    .array-data 4
        0x7f0b0023
        0x7f0b0033
        0x7f0b0034
    .end array-data

    :array_2
    .array-data 4
        0x7f0b0023
        0x7f0b0035
        0x7f0b0036
        0x7f0b0037
        0x7f0b0038
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTimer:Ljava/util/Timer;

    iput-boolean v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mIsUserResponse:Z

    iput-boolean v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mGetOtherNotify:Z

    iput-object v2, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    iput-object v2, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTitle:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/mediatek/lbs/AgpsNotifyDialog;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/mediatek/lbs/AgpsNotifyDialog;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/AgpsNotifyDialog;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$102(Lcom/mediatek/lbs/AgpsNotifyDialog;Z)Z
    .locals 0
    .param p0    # Lcom/mediatek/lbs/AgpsNotifyDialog;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mIsUserResponse:Z

    return p1
.end method

.method private getStringFromIndex(II[I)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    const/4 v0, 0x1

    :goto_0
    if-ge v0, p2, :cond_1

    if-ne v0, p1, :cond_0

    aget v1, p3, v0

    :goto_1
    return v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const v1, 0x7f0b0023

    goto :goto_1
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const-string v0, "Settings/Agps"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[AgpsNotify] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setTimerIfNeed(I)V
    .locals 9
    .param p1    # I

    const/4 v4, 0x1

    new-instance v6, Ljava/util/Timer;

    invoke-direct {v6}, Ljava/util/Timer;-><init>()V

    iput-object v6, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTimer:Ljava/util/Timer;

    new-instance v2, Lcom/mediatek/lbs/AgpsNotifyDialog$1;

    invoke-direct {v2, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$1;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    sget-object v6, Lcom/mediatek/lbs/AgpsNotifyDialog;->sAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    invoke-interface {v6}, Lcom/mediatek/common/agps/MtkAgpsManager;->getConfig()Lcom/mediatek/common/agps/MtkAgpsConfig;

    move-result-object v0

    iget v1, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->notifyTimeout:I

    iget v5, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->verifyTimeout:I

    iget v6, v0, Lcom/mediatek/common/agps/MtkAgpsConfig;->niTimer:I

    if-ne v6, v4, :cond_1

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notifyTimeout="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " verifyTimeout="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " timerEnabled="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/mediatek/lbs/AgpsNotifyDialog;->log(Ljava/lang/String;)V

    if-eqz v4, :cond_0

    const/4 v3, 0x0

    if-nez p1, :cond_2

    mul-int/lit16 v3, v1, 0x3e8

    :goto_1
    iget-object v6, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTimer:Ljava/util/Timer;

    int-to-long v7, v3

    invoke-virtual {v6, v2, v7, v8}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    :cond_0
    return-void

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    mul-int/lit16 v3, v5, 0x3e8

    goto :goto_1
.end method

.method private setup(Landroid/content/Intent;)V
    .locals 12
    .param p1    # Landroid/content/Intent;

    const v11, 0x7f0b001a

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    sget-object v5, Lcom/mediatek/lbs/AgpsNotifyDialog;->sAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    if-nez v5, :cond_0

    const-string v5, "mtk-agps"

    invoke-virtual {p0, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/mediatek/common/agps/MtkAgpsManager;

    sput-object v5, Lcom/mediatek/lbs/AgpsNotifyDialog;->sAgpsMgr:Lcom/mediatek/common/agps/MtkAgpsManager;

    :cond_0
    iget-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mHandler:Landroid/os/Handler;

    if-nez v5, :cond_1

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mHandler:Landroid/os/Handler;

    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const/4 v3, -0x1

    const/4 v2, -0x1

    if-eqz v0, :cond_4

    const-string v5, "msg_type"

    const/16 v8, 0x115c

    invoke-virtual {v0, v5, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string v5, "msg_id"

    invoke-virtual {v0, v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {p0, v3, v2}, Lcom/mediatek/lbs/AgpsNotifyDialog;->getStringID(II)I

    move-result v5

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    const-string v5, "request_id"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mRequestId:Ljava/lang/String;

    const-string v5, "client_name"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mCliecntName:Ljava/lang/String;

    :goto_0
    iget-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mRequestId:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mRequestId:Ljava/lang/String;

    const-string v8, "UNKNOWN_VALUE"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    move v4, v6

    :goto_1
    iget-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mCliecntName:Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mCliecntName:Ljava/lang/String;

    const-string v8, "UNKNOWN_VALUE"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    move v1, v6

    :goto_2
    if-eqz v4, :cond_7

    if-eqz v1, :cond_7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mRequestId:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v8, 0x7f0b001b

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mCliecntName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    :cond_2
    :goto_3
    sparse-switch v3, :sswitch_data_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unrecongnized type is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/mediatek/lbs/AgpsNotifyDialog;->log(Ljava/lang/String;)V

    :cond_3
    :goto_4
    return-void

    :cond_4
    const-string v5, "Error: Bundle is null"

    invoke-direct {p0, v5}, Lcom/mediatek/lbs/AgpsNotifyDialog;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v4, v7

    goto/16 :goto_1

    :cond_6
    move v1, v7

    goto :goto_2

    :cond_7
    if-eqz v4, :cond_8

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mRequestId:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    goto :goto_3

    :cond_8
    if-eqz v1, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "\n"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v8, 0x7f0b001b

    invoke-virtual {p0, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ": "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mCliecntName:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    goto/16 :goto_3

    :sswitch_0
    invoke-virtual {p0, v7}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_4

    :sswitch_1
    if-ne v2, v9, :cond_9

    const v5, 0x7f0b001c

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTitle:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/mediatek/lbs/AgpsNotifyDialog;->setTimerIfNeed(I)V

    invoke-virtual {p0, v6}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_4

    :cond_9
    if-ne v2, v10, :cond_a

    const v5, 0x7f0b001c

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTitle:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/mediatek/lbs/AgpsNotifyDialog;->setTimerIfNeed(I)V

    invoke-virtual {p0, v9}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_4

    :cond_a
    if-ne v2, v6, :cond_3

    const v5, 0x7f0b001d

    invoke-virtual {p0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTitle:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/mediatek/lbs/AgpsNotifyDialog;->setTimerIfNeed(I)V

    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_4

    :sswitch_2
    invoke-virtual {p0, v10}, Landroid/app/Activity;->showDialog(I)V

    goto/16 :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x457 -> :sswitch_0
        0xd05 -> :sswitch_1
        0x115c -> :sswitch_2
    .end sparse-switch
.end method


# virtual methods
.method public finishActivity()V
    .locals 8

    iget-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/16 v7, 0x2710

    iget-boolean v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mIsUserResponse:Z

    if-nez v0, :cond_0

    const v2, 0x7f02004d

    iget-object v3, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTitle:Ljava/lang/String;

    iget-object v4, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    move-object v0, p0

    move-object v1, p0

    invoke-virtual/range {v0 .. v6}, Lcom/mediatek/lbs/AgpsNotifyDialog;->sendNotification(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    iget-boolean v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mGetOtherNotify:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mGetOtherNotify:Z

    goto :goto_0
.end method

.method public getStringID(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    const v0, 0x7f0b0023

    sparse-switch p1, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const/4 v1, 0x1

    sget-object v2, Lcom/mediatek/lbs/AgpsNotifyDialog;->INFO_STRING_LIST:[I

    invoke-direct {p0, p2, v1, v2}, Lcom/mediatek/lbs/AgpsNotifyDialog;->getStringFromIndex(II[I)I

    move-result v0

    goto :goto_0

    :sswitch_1
    const/4 v1, 0x5

    sget-object v2, Lcom/mediatek/lbs/AgpsNotifyDialog;->NOTIFY_STRING_LIST:[I

    invoke-direct {p0, p2, v1, v2}, Lcom/mediatek/lbs/AgpsNotifyDialog;->getStringFromIndex(II[I)I

    move-result v0

    goto :goto_0

    :sswitch_2
    const/16 v1, 0x10

    sget-object v2, Lcom/mediatek/lbs/AgpsNotifyDialog;->ERROR_STRING_LIST:[I

    invoke-direct {p0, p2, v1, v2}, Lcom/mediatek/lbs/AgpsNotifyDialog;->getStringFromIndex(II[I)I

    move-result v0

    goto :goto_0

    :sswitch_3
    const/4 v1, 0x3

    sget-object v2, Lcom/mediatek/lbs/AgpsNotifyDialog;->EM_STRING_LIST:[I

    invoke-direct {p0, p2, v1, v2}, Lcom/mediatek/lbs/AgpsNotifyDialog;->getStringFromIndex(II[I)I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x457 -> :sswitch_3
        0x8ae -> :sswitch_0
        0xd05 -> :sswitch_1
        0x115c -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mIsUserResponse:Z

    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/AgpsNotifyDialog;->setup(Landroid/content/Intent;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1    # I

    const v4, 0x7f0b0021

    const v3, 0x7f0b0020

    const v1, 0x7f0b001c

    const v2, 0x7f0b001f

    packed-switch p1, :pswitch_data_0

    const-string v0, "WARNING: No such dialog"

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/AgpsNotifyDialog;->log(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b001e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$3;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$3;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$2;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$2;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$6;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$6;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$5;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$5;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$4;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$4;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$9;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$9;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$8;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$8;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$7;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$7;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b0022

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$11;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$11;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$10;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$10;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0b001d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$13;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$13;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/mediatek/lbs/AgpsNotifyDialog$12;

    invoke-direct {v1, p0}, Lcom/mediatek/lbs/AgpsNotifyDialog$12;-><init>(Lcom/mediatek/lbs/AgpsNotifyDialog;)V

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mDialog:Landroid/app/Dialog;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const-string v0, "onNewIntent is called "

    invoke-direct {p0, v0}, Lcom/mediatek/lbs/AgpsNotifyDialog;->log(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/lbs/AgpsNotifyDialog;->mGetOtherNotify:Z

    invoke-virtual {p0}, Lcom/mediatek/lbs/AgpsNotifyDialog;->finishActivity()V

    invoke-direct {p0, p1}, Lcom/mediatek/lbs/AgpsNotifyDialog;->setup(Landroid/content/Intent;)V

    return-void
.end method

.method public sendNotification(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # I

    const/4 v5, 0x0

    new-instance v1, Landroid/content/Intent;

    const-string v4, ""

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    iput p2, v2, Landroid/app/Notification;->icon:I

    iput-object p3, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    iput v5, v2, Landroid/app/Notification;->defaults:I

    const/16 v4, 0x10

    iput v4, v2, Landroid/app/Notification;->flags:I

    invoke-virtual {v2, p1, p4, p5, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-virtual {v3, p6, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method
