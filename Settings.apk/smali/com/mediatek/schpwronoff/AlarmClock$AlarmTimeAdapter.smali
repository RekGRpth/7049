.class Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;
.super Landroid/widget/CursorAdapter;
.source "AlarmClock.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/schpwronoff/AlarmClock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlarmTimeAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/schpwronoff/AlarmClock;


# direct methods
.method public constructor <init>(Lcom/mediatek/schpwronoff/AlarmClock;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    iput-object p1, p0, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;->this$0:Lcom/mediatek/schpwronoff/AlarmClock;

    invoke-direct {p0, p2, p3}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/4 v11, 0x0

    new-instance v0, Lcom/mediatek/schpwronoff/Alarm;

    invoke-direct {v0, p3}, Lcom/mediatek/schpwronoff/Alarm;-><init>(Landroid/database/Cursor;)V

    move-object v2, p2

    const v8, 0x7f080151

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    if-eqz v6, :cond_0

    iget-boolean v8, v0, Lcom/mediatek/schpwronoff/Alarm;->mEnabled:Z

    invoke-virtual {v6, v8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    new-instance v8, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter$1;

    invoke-direct {v8, p0, v2, v0}, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter$1;-><init>(Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;Landroid/content/Context;Lcom/mediatek/schpwronoff/Alarm;)V

    invoke-virtual {v6, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v8, 0x7f08014a

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    if-eqz v7, :cond_1

    iget-object v8, p0, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;->this$0:Lcom/mediatek/schpwronoff/AlarmClock;

    invoke-virtual {v8}, Landroid/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget v8, v0, Lcom/mediatek/schpwronoff/Alarm;->mId:I

    const/4 v10, 0x1

    if-ne v8, v10, :cond_4

    const v8, 0x7f020077

    :goto_0
    invoke-virtual {v9, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    const v8, 0x7f08014b

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/mediatek/schpwronoff/DigitalClock;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    const/16 v8, 0xb

    iget v9, v0, Lcom/mediatek/schpwronoff/Alarm;->mHour:I

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->set(II)V

    const/16 v8, 0xc

    iget v9, v0, Lcom/mediatek/schpwronoff/Alarm;->mMinutes:I

    invoke-virtual {v1, v8, v9}, Ljava/util/Calendar;->set(II)V

    if-eqz v5, :cond_2

    invoke-virtual {v5, v1}, Lcom/mediatek/schpwronoff/DigitalClock;->updateTime(Ljava/util/Calendar;)V

    :cond_2
    const v8, 0x7f080150

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v8, v0, Lcom/mediatek/schpwronoff/Alarm;->mDaysOfWeek:Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;

    invoke-virtual {v8, p2, v11}, Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;->toString(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v11}, Landroid/view/View;->setVisibility(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    const v8, 0x7f020076

    goto :goto_0

    :cond_5
    const/16 v8, 0x8

    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;->this$0:Lcom/mediatek/schpwronoff/AlarmClock;

    invoke-static {v2}, Lcom/mediatek/schpwronoff/AlarmClock;->access$000(Lcom/mediatek/schpwronoff/AlarmClock;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040075

    invoke-virtual {v2, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f08014e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;->this$0:Lcom/mediatek/schpwronoff/AlarmClock;

    invoke-static {v3}, Lcom/mediatek/schpwronoff/AlarmClock;->access$100(Lcom/mediatek/schpwronoff/AlarmClock;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f08014f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/mediatek/schpwronoff/AlarmClock$AlarmTimeAdapter;->this$0:Lcom/mediatek/schpwronoff/AlarmClock;

    invoke-static {v3}, Lcom/mediatek/schpwronoff/AlarmClock;->access$200(Lcom/mediatek/schpwronoff/AlarmClock;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f08014b

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/mediatek/schpwronoff/DigitalClock;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Lcom/mediatek/schpwronoff/DigitalClock;->setLive(Z)V

    :cond_0
    const-string v2, "AlarmClock"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "newView "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1
.end method
