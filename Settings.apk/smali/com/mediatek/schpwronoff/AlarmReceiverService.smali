.class public Lcom/mediatek/schpwronoff/AlarmReceiverService;
.super Landroid/app/Service;
.source "AlarmReceiverService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AlarmReceiverService"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "AlarmReceiverService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1    # Landroid/content/Intent;
    .param p2    # I
    .param p3    # I

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "AlarmReceiverService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "action="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "AlarmReceiverService"

    const-string v3, "onACTION_BOOT_COMPLETEDStartCommand----Intent.ACTION_BOOT_COMPLETED"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, -0x1

    const-wide/16 v3, -0x1

    invoke-static {p0, v2, v3, v4}, Lcom/mediatek/schpwronoff/Alarms;->saveSnoozeAlert(Landroid/content/Context;IJ)V

    invoke-static {p0}, Lcom/mediatek/schpwronoff/Alarms;->disableExpiredAlarms(Landroid/content/Context;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/mediatek/schpwronoff/SchPwrOnOffService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Landroid/content/ContextWrapper;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    invoke-static {p0}, Lcom/mediatek/schpwronoff/Alarms;->setNextAlert(Landroid/content/Context;)V

    :cond_0
    :goto_0
    const/4 v2, 0x2

    return v2

    :cond_1
    const-string v2, "AlarmReceiverService"

    const-string v3, "onStartCommand---Alarms.setNextAlert"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0}, Lcom/mediatek/schpwronoff/Alarms;->setNextAlert(Landroid/content/Context;)V

    goto :goto_0
.end method
