.class Lcom/mediatek/schpwronoff/SchPwrOnOffService$1;
.super Landroid/telephony/PhoneStateListener;
.source "SchPwrOnOffService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/schpwronoff/SchPwrOnOffService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/schpwronoff/SchPwrOnOffService;


# direct methods
.method constructor <init>(Lcom/mediatek/schpwronoff/SchPwrOnOffService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/schpwronoff/SchPwrOnOffService$1;->this$0:Lcom/mediatek/schpwronoff/SchPwrOnOffService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1    # I
    .param p2    # Ljava/lang/String;

    const-string v0, "SchPwrOnOffService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SchPwrOnOffService - call_state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->sInCall:Z

    const-string v0, "SchPwrOnOffService"

    const-string v1, "SchPwrOnOffService - sInCall = false"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->sInCall:Z

    const-string v0, "SchPwrOnOffService"

    const-string v1, "SchPwrOnOffService - sInCall = true"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
