.class public Lcom/mediatek/schpwronoff/SchPwrOnOffService;
.super Landroid/app/Service;
.source "SchPwrOnOffService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SchPwrOnOffService"

.field public static sInCall:Z


# instance fields
.field private final mPhoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->sInCall:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/mediatek/schpwronoff/SchPwrOnOffService$1;

    invoke-direct {v0, p0}, Lcom/mediatek/schpwronoff/SchPwrOnOffService$1;-><init>(Lcom/mediatek/schpwronoff/SchPwrOnOffService;)V

    iput-object v0, p0, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "SchPwrOnOffService"

    const-string v3, "SchPwrOnOffService - onCreate"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/ContextWrapper;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    sput-boolean v2, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->sInCall:Z

    const-string v2, "SchPwrOnOffService"

    const-string v3, "SchPwrOnOffService - sInCall = false"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/mediatek/schpwronoff/SchPwrOnOffService;->sInCall:Z

    const-string v2, "SchPwrOnOffService"

    const-string v3, "SchPwrOnOffService - sInCall = true"

    invoke-static {v2, v3}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
