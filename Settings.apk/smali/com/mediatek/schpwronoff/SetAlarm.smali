.class public Lcom/mediatek/schpwronoff/SetAlarm;
.super Lcom/android/settings/SettingsPreferenceFragment;
.source "SetAlarm.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# static fields
.field private static final MENU_REVET:I = 0x1

.field private static final MENU_SAVE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SetAlarm"


# instance fields
.field private mEnabled:Z

.field private mHour:I

.field private mId:I

.field private mMinutes:I

.field private mPrevTitle:Ljava/lang/String;

.field private mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

.field private mTestAlarmItem:Landroid/view/MenuItem;

.field private mTimePref:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/settings/SettingsPreferenceFragment;-><init>()V

    return-void
.end method

.method static formatToast(Landroid/content/Context;IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;I)Ljava/lang/String;
    .locals 30
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;
    .param p4    # I

    invoke-static/range {p1 .. p3}, Lcom/mediatek/schpwronoff/Alarms;->calculateAlarm(IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;)Ljava/util/Calendar;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    sub-long v9, v3, v26

    const/16 v20, 0x3e8

    const/16 v25, 0x3c

    const/16 v5, 0x18

    const-wide/32 v26, 0x36ee80

    div-long v17, v9, v26

    const-wide/32 v26, 0xea60

    div-long v26, v9, v26

    const-wide/16 v28, 0x3c

    rem-long v22, v26, v28

    const-wide/16 v26, 0x18

    div-long v7, v17, v26

    const-wide/16 v26, 0x18

    rem-long v17, v17, v26

    const-wide/16 v26, 0x0

    cmp-long v26, v7, v26

    if-nez v26, :cond_1

    const-string v6, ""

    :goto_0
    const-wide/16 v26, 0x0

    cmp-long v26, v22, v26

    if-nez v26, :cond_3

    const-string v21, ""

    :goto_1
    const-wide/16 v26, 0x0

    cmp-long v26, v17, v26

    if-nez v26, :cond_5

    const-string v16, ""

    :goto_2
    const-wide/16 v26, 0x0

    cmp-long v26, v7, v26

    if-lez v26, :cond_7

    const/4 v11, 0x1

    :goto_3
    const-wide/16 v26, 0x0

    cmp-long v26, v17, v26

    if-lez v26, :cond_8

    const/4 v12, 0x1

    :goto_4
    const-wide/16 v26, 0x0

    cmp-long v26, v22, v26

    if-lez v26, :cond_9

    const/4 v13, 0x1

    :goto_5
    const/4 v14, 0x4

    const/16 v24, 0x8

    if-eqz v11, :cond_a

    const/16 v26, 0x1

    move/from16 v27, v26

    :goto_6
    if-eqz v12, :cond_b

    const/16 v26, 0x2

    :goto_7
    or-int v27, v27, v26

    if-eqz v13, :cond_c

    const/16 v26, 0x4

    :goto_8
    or-int v19, v27, v26

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f07007e

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v15

    const/16 v26, 0x2

    move/from16 v0, p4

    move/from16 v1, v26

    if-ne v0, v1, :cond_0

    add-int/lit8 v19, v19, 0x8

    :cond_0
    aget-object v26, v15, v19

    const/16 v27, 0x3

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aput-object v6, v27, v28

    const/16 v28, 0x1

    aput-object v16, v27, v28

    const/16 v28, 0x2

    aput-object v21, v27, v28

    invoke-static/range {v26 .. v27}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    return-object v26

    :cond_1
    const-wide/16 v26, 0x1

    cmp-long v26, v7, v26

    if-nez v26, :cond_2

    const v26, 0x7f0b0149

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    :cond_2
    const v26, 0x7f0b014a

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v27, v28

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_0

    :cond_3
    const-wide/16 v26, 0x1

    cmp-long v26, v22, v26

    if-nez v26, :cond_4

    const v26, 0x7f0b014d

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_1

    :cond_4
    const v26, 0x7f0b014e

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v27, v28

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_1

    :cond_5
    const-wide/16 v26, 0x1

    cmp-long v26, v17, v26

    if-nez v26, :cond_6

    const v26, 0x7f0b014b

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    :cond_6
    const v26, 0x7f0b014c

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v29

    aput-object v29, v27, v28

    move-object/from16 v0, p0

    move/from16 v1, v26

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_4

    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_5

    :cond_a
    const/16 v26, 0x0

    move/from16 v27, v26

    goto/16 :goto_6

    :cond_b
    const/16 v26, 0x0

    goto/16 :goto_7

    :cond_c
    const/16 v26, 0x0

    goto/16 :goto_8
.end method

.method static popAlarmSetToast(Landroid/content/Context;IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;I)V
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;
    .param p4    # I

    invoke-static {p0, p1, p2, p3, p4}, Lcom/mediatek/schpwronoff/SetAlarm;->formatToast(Landroid/content/Context;IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private saveAlarm()V
    .locals 10

    const-string v9, "silent"

    iget-boolean v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mEnabled:Z

    iget-object v1, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    iget-boolean v1, v1, Lcom/mediatek/schpwronoff/RepeatPreference;->mIsPressedPositive:Z

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mEnabled:Z

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    iget-boolean v2, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mEnabled:Z

    iget v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mHour:I

    iget v4, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mMinutes:I

    iget-object v5, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    invoke-virtual {v5}, Lcom/mediatek/schpwronoff/RepeatPreference;->getDaysOfWeek()Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;

    move-result-object v5

    const/4 v6, 0x1

    const-string v7, ""

    const-string v8, "silent"

    invoke-static/range {v0 .. v8}, Lcom/mediatek/schpwronoff/Alarms;->setAlarm(Landroid/content/Context;IZIILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;ZLjava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mHour:I

    iget v2, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mMinutes:I

    iget-object v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    invoke-virtual {v3}, Lcom/mediatek/schpwronoff/RepeatPreference;->getDaysOfWeek()Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/mediatek/schpwronoff/SetAlarm;->popAlarmSetToast(Landroid/content/Context;IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;I)V

    :cond_0
    return-void
.end method

.method private static saveAlarm(Landroid/content/Context;IZIILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;ZLjava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # I
    .param p2    # Z
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;
    .param p6    # Z
    .param p7    # Ljava/lang/String;
    .param p8    # Ljava/lang/String;
    .param p9    # Z

    const-string v0, "SetAlarm"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "** saveAlarm "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vibe "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static/range {p0 .. p8}, Lcom/mediatek/schpwronoff/Alarms;->setAlarm(Landroid/content/Context;IZIILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;ZLjava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    if-eqz p9, :cond_0

    const/4 v0, 0x1

    invoke-static {p0, p3, p4, p5, v0}, Lcom/mediatek/schpwronoff/SetAlarm;->popAlarmSetToast(Landroid/content/Context;IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;I)V

    :cond_0
    return-void
.end method

.method private updateTime()V
    .locals 5

    const-string v0, "SetAlarm"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateTime "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mTimePref:Landroid/preference/Preference;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mHour:I

    iget v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mMinutes:I

    iget-object v4, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    invoke-virtual {v4}, Lcom/mediatek/schpwronoff/RepeatPreference;->getDaysOfWeek()Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/mediatek/schpwronoff/Alarms;->formatTime(Landroid/content/Context;IILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    const v3, 0x7f050027

    invoke-virtual {p0, v3}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    const-string v3, "time"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mTimePref:Landroid/preference/Preference;

    const-string v3, "setRepeat"

    invoke-virtual {v2, v3}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/mediatek/schpwronoff/RepeatPreference;

    iput-object v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    invoke-virtual {p0}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v3, "alarm_id"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    :cond_0
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mPrevTitle:Ljava/lang/String;

    iget v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    if-ne v3, v6, :cond_2

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0145

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setTitle(I)V

    :goto_0
    const-string v3, "SetAlarm"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "In SetAlarm, alarm id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    iget v4, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    invoke-static {v3, v4}, Lcom/mediatek/schpwronoff/Alarms;->getAlarm(Landroid/content/ContentResolver;I)Lcom/mediatek/schpwronoff/Alarm;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v3, v0, Lcom/mediatek/schpwronoff/Alarm;->mEnabled:Z

    iput-boolean v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mEnabled:Z

    iget v3, v0, Lcom/mediatek/schpwronoff/Alarm;->mHour:I

    iput v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mHour:I

    iget v3, v0, Lcom/mediatek/schpwronoff/Alarm;->mMinutes:I

    iput v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mMinutes:I

    iget-object v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    iget-object v4, v0, Lcom/mediatek/schpwronoff/Alarm;->mDaysOfWeek:Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;

    invoke-virtual {v3, v4}, Lcom/mediatek/schpwronoff/RepeatPreference;->setDaysOfWeek(Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;)V

    :cond_1
    invoke-direct {p0}, Lcom/mediatek/schpwronoff/SetAlarm;->updateTime()V

    invoke-virtual {p0, v6}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0b0146

    invoke-virtual {v3, v4}, Landroid/app/Activity;->setTitle(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/android/settings/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v0, 0x7f0b0153

    invoke-interface {p1, v3, v2, v3, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const/4 v0, 0x2

    const v1, 0x7f0b0152

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-super {p0, p1, p2}, Lcom/android/settings/SettingsPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void
.end method

.method public onDetach()V
    .locals 2

    iget-object v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mPrevTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mPrevTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    invoke-super {p0}, Lcom/android/settings/SettingsPreferenceFragment;->onDetach()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    invoke-super {p0, p1}, Landroid/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finishFragment()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/mediatek/schpwronoff/SetAlarm;->saveAlarm()V

    invoke-virtual {p0}, Lcom/android/settings/SettingsPreferenceFragment;->finishFragment()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    iget-object v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mTimePref:Landroid/preference/Preference;

    if-ne p2, v0, :cond_0

    new-instance v0, Landroid/app/TimePickerDialog;

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mHour:I

    iget v4, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mMinutes:I

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v0

    return v0
.end method

.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 1
    .param p1    # Landroid/widget/TimePicker;
    .param p2    # I
    .param p3    # I

    iput p2, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mHour:I

    iput p3, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mMinutes:I

    invoke-direct {p0}, Lcom/mediatek/schpwronoff/SetAlarm;->updateTime()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mEnabled:Z

    return-void
.end method

.method setTestAlarm()V
    .locals 14

    const/4 v2, 0x1

    const/16 v13, 0x3c

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v0, 0xb

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v11

    const/16 v0, 0xc

    invoke-virtual {v10, v0}, Ljava/util/Calendar;->get(I)I

    move-result v12

    add-int/lit8 v0, v12, 0x1

    rem-int/lit8 v4, v0, 0x3c

    if-nez v12, :cond_0

    move v0, v2

    :goto_0
    add-int v3, v11, v0

    invoke-virtual {p0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mId:I

    iget-object v5, p0, Lcom/mediatek/schpwronoff/SetAlarm;->mRepeatPref:Lcom/mediatek/schpwronoff/RepeatPreference;

    invoke-virtual {v5}, Lcom/mediatek/schpwronoff/RepeatPreference;->getDaysOfWeek()Lcom/mediatek/schpwronoff/Alarm$DaysOfWeek;

    move-result-object v5

    const-string v7, ""

    const-string v8, "silent"

    move v6, v2

    move v9, v2

    invoke-static/range {v0 .. v9}, Lcom/mediatek/schpwronoff/SetAlarm;->saveAlarm(Landroid/content/Context;IZIILcom/mediatek/schpwronoff/Alarm$DaysOfWeek;ZLjava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
