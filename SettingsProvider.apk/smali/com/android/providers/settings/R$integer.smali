.class public final Lcom/android/providers/settings/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/providers/settings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final boot_up_select_mode:I = 0x7f05000d

.field public static final def_cro_setting:I = 0x7f050011

.field public static final def_dock_sounds_enabled:I = 0x7f050004

.field public static final def_download_manager_max_bytes_over_mobile:I = 0x7f050007

.field public static final def_download_manager_recommended_max_bytes_over_mobile:I = 0x7f050008

.field public static final def_hoo_setting:I = 0x7f050012

.field public static final def_ivsr_setting:I = 0x7f050010

.field public static final def_lockscreen_sounds_enabled:I = 0x7f050005

.field public static final def_long_press_timeout_millis:I = 0x7f050009

.field public static final def_max_dhcp_retries:I = 0x7f05000b

.field public static final def_network_preference:I = 0x7f050002

.field public static final def_pointer_speed:I = 0x7f05000a

.field public static final def_power_sounds_enabled:I = 0x7f050003

.field public static final def_screen_brightness:I = 0x7f050001

.field public static final def_screen_off_timeout:I = 0x7f050000

.field public static final def_tether_ipv6_feature:I = 0x7f050013

.field public static final def_user_rotation:I = 0x7f050006

.field public static final def_video_call_reject_mode:I = 0x7f05000f

.field public static final def_voice_call_reject_mode:I = 0x7f05000e

.field public static final wifi_select_ssid_type:I = 0x7f05000c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
