.class public Lcom/mediatek/smsreg/InfoPersistentor;
.super Ljava/lang/Object;
.source "InfoPersistentor.java"

# interfaces
.implements Lcom/mediatek/smsreg/IInfoPersistentor;


# instance fields
.field private mAgent:Lcom/mediatek/common/dm/DMAgent;

.field private mTAG:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "SmsReg/InfoPersistentor"

    iput-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v2, "get the agent..."

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "DMAgent"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v2, "get DMAgent fail! binder is null!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/mediatek/common/dm/DMAgent$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mediatek/common/dm/DMAgent;

    move-result-object v1

    iput-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    goto :goto_0
.end method

.method private setSavedCTA(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v2, "save CTA switch value failed, agent is null!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/mediatek/common/dm/DMAgent;->writeCTA([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "save CTA ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v2, "save CTA switch failed, writeCTA failed!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getSavedCTA()I
    .locals 6

    iget-object v3, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v4, "get CTA failed, agent is null!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    invoke-interface {v3}, Lcom/mediatek/common/dm/DMAgent;->readCTA()[B

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v3, 0x0

    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    :goto_2
    iget-object v3, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Get savedCTA = ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v1

    iget-object v3, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v4, "get cta cmcc switch failed, readCTA failed!"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v1

    iget-object v3, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v4, "number format exception. "

    invoke-static {v3, v4, v1}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public getSavedIMSI()Ljava/lang/String;
    .locals 7

    iget-object v4, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v5, "get IMSI failed, agent is null!"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    invoke-interface {v4}, Lcom/mediatek/common/dm/DMAgent;->readIMSI()[B

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Get savedIMSI = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v4, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v5, "get IMSI failed, readIMSI failed!"

    invoke-static {v4, v5}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method public setSavedIMSI(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v2, "save IMSI failed, agent is null!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mAgent:Lcom/mediatek/common/dm/DMAgent;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/mediatek/common/dm/DMAgent;->writeIMSI([B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "save IMSI ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/mediatek/smsreg/InfoPersistentor;->mTAG:Ljava/lang/String;

    const-string v2, "save IMSI failed, writeIMSI failed!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method
