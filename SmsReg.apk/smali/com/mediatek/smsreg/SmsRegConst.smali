.class public Lcom/mediatek/smsreg/SmsRegConst;
.super Ljava/lang/Object;
.source "SmsRegConst.java"


# static fields
.field public static final ACTION_BOOTCOMPLETED:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_SIM_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.SIM_STATE_CHANGED"

.field public static final CMCCID:I = 0x1

.field public static final CONFIG_PATH:Ljava/lang/String; = "/system/etc/dm/smsSelfRegConfig.xml"

.field public static final CUCOMID:I = 0x2

.field public static final DM_REGISTER_SMS_RECEIVED_ACTION:Ljava/lang/String; = "android.intent.action.DM_REGISTER_SMS_RECEIVED"

.field public static final GEMINI_SIM_1:I = 0x0

.field public static final GEMINI_SIM_2:I = 0x1

.field public static final GEMINI_SIM_ID_KEY:Ljava/lang/String; = "simId"

.field public static GEMSIM:[I = null

.field public static final SIM_STATE_ABSENT:I = 0x1

.field public static final SIM_STATE_NETWORK_LOCKED:I = 0x4

.field public static final SIM_STATE_PIN_REQUIRED:I = 0x2

.field public static final SIM_STATE_PUK_REQUIRED:I = 0x3

.field public static final SIM_STATE_READY:I = 0x5

.field public static final SIM_STATE_UNKNOWN:I = 0x0

.field public static final SMS_REGIST_ENABLED_PATH:Ljava/lang/String; = "/data/data/com.mediatek.engineermode/sharefile/cta_cmcc"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/mediatek/smsreg/SmsRegConst;->GEMSIM:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
