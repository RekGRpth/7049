.class public Lcom/mediatek/smsreg/SmsRegContentProvider;
.super Landroid/content/ContentProvider;
.source "SmsRegContentProvider.java"


# instance fields
.field private SmsRegColumn:[Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private smsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

.field private xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    const-string v0, "SmsReg/SmsRegContentProvider"

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->TAG:Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "enable"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "imei"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "op"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "smsNumber"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "smsPort"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "manufacturer"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "product"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "version"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->SmsRegColumn:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 2

    const-string v0, "SmsReg/SmsRegContentProvider"

    const-string v1, "SmsRegContentProvider onCreate.."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/mediatek/smsreg/SmsBuilder;

    invoke-virtual {p0}, Landroid/content/ContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/mediatek/smsreg/SmsBuilder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->smsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    const-string v0, "/system/etc/dm/smsSelfRegConfig.xml"

    invoke-static {v0}, Lcom/mediatek/smsreg/XMLGenerator;->getInstance(Ljava/lang/String;)Lcom/mediatek/smsreg/ConfigInfoGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 13
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const-string v10, "SmsReg/SmsRegContentProvider"

    const-string v11, "SmsRegContentProvider query.."

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ""

    new-instance v10, Lcom/mediatek/smsreg/SmsRegReceiver;

    invoke-direct {v10}, Lcom/mediatek/smsreg/SmsRegReceiver;-><init>()V

    invoke-virtual {v10}, Lcom/mediatek/smsreg/SmsRegReceiver;->enableSmsReg()Z

    move-result v10

    if-eqz v10, :cond_0

    const-string v1, "yes"

    :goto_0
    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "is SmsReg enable: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->smsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    iget-object v11, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    const-string v12, "getimei"

    invoke-virtual {v10, v11, v12}, Lcom/mediatek/smsreg/SmsBuilder;->getContentInfo(Lcom/mediatek/smsreg/ConfigInfoGenerator;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "imei : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->smsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    iget-object v11, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    const-string v12, "getproduct"

    invoke-virtual {v10, v11, v12}, Lcom/mediatek/smsreg/SmsBuilder;->getContentInfo(Lcom/mediatek/smsreg/ConfigInfoGenerator;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "product : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->smsBuilder:Lcom/mediatek/smsreg/SmsBuilder;

    iget-object v11, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    const-string v12, "getversion"

    invoke-virtual {v10, v11, v12}, Lcom/mediatek/smsreg/SmsBuilder;->getContentInfo(Lcom/mediatek/smsreg/ConfigInfoGenerator;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "product : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v10}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getOperatorName()Ljava/lang/String;

    move-result-object v4

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "opName : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v10}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getManufacturerName()Ljava/lang/String;

    move-result-object v3

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "manufacturer : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v10}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSmsNumber()Ljava/lang/String;

    move-result-object v7

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "smsNumber : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->xmlGenerator:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v10}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getSmsPort()Ljava/lang/Short;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Short;->shortValue()S

    move-result v10

    invoke-static {v10}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    move-result-object v8

    const-string v10, "SmsReg/SmsRegContentProvider"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "smsPort : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v10, 0x8

    new-array v6, v10, [Ljava/lang/String;

    const/4 v10, 0x0

    aput-object v1, v6, v10

    const/4 v10, 0x1

    aput-object v2, v6, v10

    const/4 v10, 0x2

    aput-object v4, v6, v10

    const/4 v10, 0x3

    aput-object v7, v6, v10

    const/4 v10, 0x4

    aput-object v8, v6, v10

    const/4 v10, 0x5

    aput-object v3, v6, v10

    const/4 v10, 0x6

    aput-object v5, v6, v10

    const/4 v10, 0x7

    aput-object v9, v6, v10

    new-instance v0, Landroid/database/MatrixCursor;

    iget-object v10, p0, Lcom/mediatek/smsreg/SmsRegContentProvider;->SmsRegColumn:[Ljava/lang/String;

    invoke-direct {v0, v10}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    return-object v0

    :cond_0
    const-string v1, "no"

    goto/16 :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
