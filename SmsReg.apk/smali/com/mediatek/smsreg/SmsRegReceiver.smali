.class public Lcom/mediatek/smsreg/SmsRegReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsRegReceiver.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private xmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const-string v0, "SmsReg/Receiver"

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->TAG:Ljava/lang/String;

    const-string v0, "/system/etc/dm/smsSelfRegConfig.xml"

    invoke-static {v0}, Lcom/mediatek/smsreg/XMLGenerator;->getInstance(Ljava/lang/String;)Lcom/mediatek/smsreg/ConfigInfoGenerator;

    move-result-object v0

    iput-object v0, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->xmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    return-void
.end method


# virtual methods
.method public enableSmsReg()Z
    .locals 3

    const/4 v1, 0x1

    new-instance v0, Lcom/mediatek/smsreg/InfoPersistentor;

    invoke-direct {v0}, Lcom/mediatek/smsreg/InfoPersistentor;-><init>()V

    invoke-virtual {v0}, Lcom/mediatek/smsreg/InfoPersistentor;->getSavedCTA()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The intent is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/mediatek/smsreg/SmsRegReceiver;->enableSmsReg()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->xmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    if-nez v3, :cond_0

    const-string v3, "/system/etc/dm/smsSelfRegConfig.xml"

    invoke-static {v3}, Lcom/mediatek/smsreg/XMLGenerator;->getInstance(Ljava/lang/String;)Lcom/mediatek/smsreg/ConfigInfoGenerator;

    move-result-object v3

    iput-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->xmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    :cond_0
    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->xmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->xmlG:Lcom/mediatek/smsreg/ConfigInfoGenerator;

    invoke-interface {v3}, Lcom/mediatek/smsreg/ConfigInfoGenerator;->getCustomizedStatus()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v3, "BOOTCOMPLETED"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-class v3, Lcom/mediatek/smsreg/SmsRegService;

    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->TAG:Ljava/lang/String;

    const-string v4, "The phone is not a customized phone "

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegReceiver;->TAG:Ljava/lang/String;

    const-string v4, "Sms register is disabled by engineer mode !"

    invoke-static {v3, v4}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
