.class Lcom/mediatek/smsreg/SmsRegService$3;
.super Landroid/telephony/PhoneStateListener;
.source "SmsRegService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/smsreg/SmsRegService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/smsreg/SmsRegService;


# direct methods
.method constructor <init>(Lcom/mediatek/smsreg/SmsRegService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 5
    .param p1    # Landroid/telephony/ServiceState;

    const/4 v4, 0x0

    const-string v1, "SmsReg/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Service state change sim:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v1}, Lcom/mediatek/smsreg/SmsRegService;->access$000(Lcom/mediatek/smsreg/SmsRegService;)V

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v1}, Lcom/mediatek/smsreg/SmsRegService;->access$100(Lcom/mediatek/smsreg/SmsRegService;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v4

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v2}, Lcom/mediatek/smsreg/SmsRegService;->access$300(Lcom/mediatek/smsreg/SmsRegService;)Lcom/mediatek/smsreg/InfoPersistentor;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mediatek/smsreg/InfoPersistentor;->getSavedIMSI()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/smsreg/SmsRegService;->access$202(Lcom/mediatek/smsreg/SmsRegService;Ljava/lang/String;)Ljava/lang/String;

    const-string v1, "SmsReg/Service"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "the mSavedIMSI = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v3}, Lcom/mediatek/smsreg/SmsRegService;->access$200(Lcom/mediatek/smsreg/SmsRegService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    iget-object v2, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v2}, Lcom/mediatek/smsreg/SmsRegService;->access$100(Lcom/mediatek/smsreg/SmsRegService;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v3}, Lcom/mediatek/smsreg/SmsRegService;->access$200(Lcom/mediatek/smsreg/SmsRegService;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/mediatek/smsreg/SmsRegService;->access$400(Lcom/mediatek/smsreg/SmsRegService;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "SmsReg/Service"

    const-string v2, "The sim card in this phone is not registered, need register"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-static {v1, v4}, Lcom/mediatek/smsreg/SmsRegService;->access$500(Lcom/mediatek/smsreg/SmsRegService;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "SmsReg/Service"

    const-string v2, "The gemini register msg has been sent already!"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mediatek/smsreg/SmsRegService$3;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-virtual {v1}, Lcom/mediatek/smsreg/SmsRegService;->stopService()V

    goto :goto_0

    :cond_2
    const-string v1, "SmsReg/Service"

    const-string v2, "Sim card 1 is not the right operator"

    invoke-static {v1, v2}, Lcom/mediatek/xlog/Xlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
