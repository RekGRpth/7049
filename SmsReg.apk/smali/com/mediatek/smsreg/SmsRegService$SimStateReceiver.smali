.class Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsRegService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/smsreg/SmsRegService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/smsreg/SmsRegService;


# direct methods
.method constructor <init>(Lcom/mediatek/smsreg/SmsRegService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/smsreg/SmsRegService$SimStateReceiver;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const-string v0, "SmsReg/Service"

    const-string v1, "sim state changed"

    invoke-static {v0, v1}, Lcom/mediatek/xlog/Xlog;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SIM_STATE_CHANGED"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-class v0, Lcom/mediatek/smsreg/SmsRegService;

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
