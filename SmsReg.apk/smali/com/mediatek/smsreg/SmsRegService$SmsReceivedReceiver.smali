.class Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsRegService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mediatek/smsreg/SmsRegService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SmsReceivedReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mediatek/smsreg/SmsRegService;


# direct methods
.method constructor <init>(Lcom/mediatek/smsreg/SmsRegService;)V
    .locals 0

    iput-object p1, p0, Lcom/mediatek/smsreg/SmsRegService$SmsReceivedReceiver;->this$0:Lcom/mediatek/smsreg/SmsRegService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/BroadcastReceiver;->getResultCode()I

    move-result v0

    const-string v1, "REGISTER_SMS_RECEIVED"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-class v1, Lcom/mediatek/smsreg/SmsRegService;

    invoke-virtual {p2, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "SMS_SENDING_RESULT"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method
