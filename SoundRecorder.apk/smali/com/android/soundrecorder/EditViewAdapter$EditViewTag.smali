.class Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;
.super Ljava/lang/Object;
.source "EditViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/EditViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "EditViewTag"
.end annotation


# instance fields
.field protected mCheckBox:Landroid/widget/CheckBox;

.field protected mDuration:Landroid/widget/TextView;

.field protected mName:Landroid/widget/TextView;

.field protected mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/widget/TextView;Landroid/widget/CheckBox;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p1    # Landroid/widget/TextView;
    .param p2    # Landroid/widget/CheckBox;
    .param p3    # Landroid/widget/TextView;
    .param p4    # Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->mName:Landroid/widget/TextView;

    iput-object p2, p0, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->mCheckBox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->mTitle:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/android/soundrecorder/EditViewAdapter$EditViewTag;->mDuration:Landroid/widget/TextView;

    return-void
.end method
