.class public final Lcom/android/soundrecorder/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final acceptButton:I = 0x7f090011

.field public static final bottomLinearLayout:I = 0x7f09001c

.field public static final buttonParent:I = 0x7f090013

.field public static final currState:I = 0x7f090008

.field public static final deleteButton:I = 0x7f09001f

.field public static final discardButton:I = 0x7f090010

.field public static final divider_img:I = 0x7f09001e

.field public static final empty_view:I = 0x7f09001d

.field public static final exitButtons:I = 0x7f09000f

.field public static final fileListButton:I = 0x7f090018

.field public static final message:I = 0x7f09001a

.field public static final pauseRecordingButton:I = 0x7f090015

.field public static final playButton:I = 0x7f090016

.field public static final recordButton:I = 0x7f090014

.field public static final record_file_checkbox:I = 0x7f090000

.field public static final record_file_duration:I = 0x7f090003

.field public static final record_file_icon:I = 0x7f090019

.field public static final record_file_name:I = 0x7f090001

.field public static final record_file_title:I = 0x7f090002

.field public static final recordfileMessage2Layout:I = 0x7f090007

.field public static final recordingFileName:I = 0x7f090009

.field public static final recording_file_list_view:I = 0x7f09001b

.field public static final stateLED:I = 0x7f09000b

.field public static final stateMessage1:I = 0x7f09000d

.field public static final stateMessage2:I = 0x7f09000c

.field public static final stateMessage2Layout:I = 0x7f09000a

.field public static final stateProgressBar:I = 0x7f09000e

.field public static final stopButton:I = 0x7f090017

.field public static final timerView:I = 0x7f090006

.field public static final timerViewLayout:I = 0x7f090005

.field public static final uvMeter:I = 0x7f090012

.field public static final whole_view:I = 0x7f090004


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
