.class public final Lcom/android/soundrecorder/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept:I = 0x7f07002b

.field public static final accessing_db_fail:I = 0x7f070023

.field public static final alert_delete_multiple:I = 0x7f070008

.field public static final alert_delete_single:I = 0x7f070009

.field public static final app_name:I = 0x7f070024

.field public static final audio_db_album_name:I = 0x7f07002e

.field public static final audio_db_artist_name:I = 0x7f070038

.field public static final audio_db_playlist_name:I = 0x7f07002f

.field public static final audio_db_title_format:I = 0x7f070033

.field public static final button_ok:I = 0x7f07002d

.field public static final cancel:I = 0x7f070003

.field public static final delete:I = 0x7f07000b

.field public static final deleting:I = 0x7f07000c

.field public static final deleting_fail:I = 0x7f070022

.field public static final discard:I = 0x7f07002c

.field public static final error_app_internal:I = 0x7f070039

.field public static final error_mediadb_new_record:I = 0x7f070031

.field public static final error_sdcard_access:I = 0x7f070030

.field public static final insert_sd_card:I = 0x7f070032

.field public static final max_length_reached:I = 0x7f070028

.field public static final message_recorded:I = 0x7f070035

.field public static final min_available:I = 0x7f070029

.field public static final no_recording_file:I = 0x7f07000d

.field public static final ok:I = 0x7f07000a

.field public static final player_occupied:I = 0x7f07001f

.field public static final playing_fail:I = 0x7f07001e

.field public static final record_your_message:I = 0x7f070034

.field public static final recorder_occupied:I = 0x7f07001d

.field public static final recording:I = 0x7f070025

.field public static final recording_doc_deleted:I = 0x7f070020

.field public static final recording_fail:I = 0x7f07001c

.field public static final recording_file_list:I = 0x7f07000e

.field public static final recording_format_high:I = 0x7f070004

.field public static final recording_format_low:I = 0x7f070006

.field public static final recording_format_mid:I = 0x7f070005

.field public static final recording_mode:I = 0x7f070011

.field public static final recording_mode_lecture:I = 0x7f070017

.field public static final recording_mode_meeting:I = 0x7f070016

.field public static final recording_mode_nomal:I = 0x7f070014

.field public static final recording_paused:I = 0x7f07000f

.field public static final recording_stopped:I = 0x7f070026

.field public static final review_message:I = 0x7f070036

.field public static final save_record:I = 0x7f070002

.field public static final saving_fail:I = 0x7f070021

.field public static final sd_no:I = 0x7f07001b

.field public static final sd_unmounted:I = 0x7f070018

.field public static final sec_available:I = 0x7f07002a

.field public static final select_recording_mode:I = 0x7f070013

.field public static final select_voice_quality:I = 0x7f070012

.field public static final storage_full:I = 0x7f07001a

.field public static final storage_is_full:I = 0x7f070027

.field public static final storage_low:I = 0x7f070019

.field public static final tell_save_record_success:I = 0x7f070001

.field public static final three_mins_avail_total:I = 0x7f070007

.field public static final time_available:I = 0x7f070015

.field public static final timer_format:I = 0x7f070037

.field public static final unknown_artist_name:I = 0x7f070000

.field public static final voice_quality:I = 0x7f070010


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
