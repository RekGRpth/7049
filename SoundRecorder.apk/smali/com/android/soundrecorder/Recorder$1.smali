.class Lcom/android/soundrecorder/Recorder$1;
.super Ljava/lang/Object;
.source "Recorder.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/soundrecorder/Recorder;-><init>(Landroid/os/storage/StorageManager;Landroid/media/AudioManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/Recorder;


# direct methods
.method constructor <init>(Lcom/android/soundrecorder/Recorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/Recorder$1;->this$0:Lcom/android/soundrecorder/Recorder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1    # I

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    const-string v0, "SR/Recorder"

    const-string v1, "<startPlayback()> audio focus changed to AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder$1;->this$0:Lcom/android/soundrecorder/Recorder;

    invoke-static {v0, v2}, Lcom/android/soundrecorder/Recorder;->access$002(Lcom/android/soundrecorder/Recorder;Z)Z

    const-string v0, "SR/Recorder"

    const-string v1, "<startPlayback> start playback"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder$1;->this$0:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->startPlayback()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    const-string v0, "SR/Recorder"

    const-string v1, "<startPlayback()> audio focus loss, pause play back"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder$1;->this$0:Lcom/android/soundrecorder/Recorder;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/soundrecorder/Recorder;->access$002(Lcom/android/soundrecorder/Recorder;Z)Z

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder$1;->this$0:Lcom/android/soundrecorder/Recorder;

    invoke-virtual {v0}, Lcom/android/soundrecorder/Recorder;->pausePlayback()V

    goto :goto_0
.end method
