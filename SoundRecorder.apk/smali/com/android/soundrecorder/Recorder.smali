.class public Lcom/android/soundrecorder/Recorder;
.super Ljava/lang/Object;
.source "Recorder.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaRecorder$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/soundrecorder/Recorder$OnStateChangedListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_SAMPLE_LENGTH:I = 0x0

.field public static final IDLE_STATE:I = 0x0

.field private static final MONO:I = 0x1

.field private static final MTK_AUDIO_HD_REC_SUPPORT:Z = true

.field private static final ONE_SECOND:I = 0x3e8

.field public static final PAUSE_PLAYING_STATE:I = 0x3

.field public static final PAUSE_RECORDING_STATE:I = 0x4

.field public static final PLAYING_STATE:I = 0x2

.field public static final RECORDING_STATE:I = 0x1

.field public static final RECORD_FOLDER:Ljava/lang/String; = "Recording"

.field private static final SAMPLE_LENGTH_KEY:Ljava/lang/String; = "sample_length"

.field private static final SAMPLE_PATH_KEY:Ljava/lang/String; = "sample_path"

.field private static final SAMPLE_PREFIX:Ljava/lang/String; = "record"

.field private static final STEREO:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SR/Recorder"

.field public static final TEMP_SUFFIX:Ljava/lang/String; = ".tmp"


# instance fields
.field private final mAudioManager:Landroid/media/AudioManager;

.field private final mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mGetFocus:Z

.field private mMode:I

.field private mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

.field private mPlayer:Landroid/media/MediaPlayer;

.field public mPreviousTime:J

.field private mRecorder:Landroid/media/MediaRecorder;

.field public mSampleFile:Ljava/io/File;

.field public mSampleLength:I

.field private mSampleStart:J

.field public mSoundRecorderDoWhat:Ljava/lang/String;

.field public mState:I

.field private final mStorageManager:Landroid/os/storage/StorageManager;


# direct methods
.method public constructor <init>(Landroid/os/storage/StorageManager;Landroid/media/AudioManager;)V
    .locals 4
    .param p1    # Landroid/os/storage/StorageManager;
    .param p2    # Landroid/media/AudioManager;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSoundRecorderDoWhat:Ljava/lang/String;

    iput v1, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    iput v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    iput v1, p0, Lcom/android/soundrecorder/Recorder;->mMode:I

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    iput-boolean v1, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    iput-object p1, p0, Lcom/android/soundrecorder/Recorder;->mStorageManager:Landroid/os/storage/StorageManager;

    iput-object p2, p0, Lcom/android/soundrecorder/Recorder;->mAudioManager:Landroid/media/AudioManager;

    new-instance v0, Lcom/android/soundrecorder/Recorder$1;

    invoke-direct {v0, p0}, Lcom/android/soundrecorder/Recorder$1;-><init>(Lcom/android/soundrecorder/Recorder;)V

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method private abandonAudioFocus()V
    .locals 2

    iget-boolean v0, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string v0, "SR/Recorder"

    const-string v1, "<abandonAudioFocus()> abandon audio focus success"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "SR/Recorder"

    const-string v1, "<abandonAudioFocus()> abandon audio focus faild"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/android/soundrecorder/Recorder;Z)Z
    .locals 0
    .param p0    # Lcom/android/soundrecorder/Recorder;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    return p1
.end method

.method private setError(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    invoke-interface {v0, p1}, Lcom/android/soundrecorder/Recorder$OnStateChangedListener;->onError(I)V

    :cond_0
    return-void
.end method

.method private setState(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    iget v0, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/Recorder;->signalStateChanged(I)V

    goto :goto_0
.end method

.method private signalStateChanged(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    invoke-interface {v0, p1}, Lcom/android/soundrecorder/Recorder$OnStateChangedListener;->onStateChanged(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    iput v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/Recorder;->signalStateChanged(I)V

    return-void
.end method

.method public createRecordingFile(Ljava/lang/String;)V
    .locals 13
    .param p1    # Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ".tmp"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    iget-object v10, p0, Lcom/android/soundrecorder/Recorder;->mStorageManager:Landroid/os/storage/StorageManager;

    if-nez v10, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v5, Ljava/io/File;

    iget-object v10, p0, Lcom/android/soundrecorder/Recorder;->mStorageManager:Landroid/os/storage/StorageManager;

    invoke-static {}, Landroid/os/storage/StorageManager;->getDefaultPath()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> sd card directory is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v11, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "Recording"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_1
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v10

    if-nez v10, :cond_1

    new-instance v5, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x28

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x29

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> make directory ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] fail"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v5, :cond_3

    :try_start_0
    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> sample directory  is:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v7, Ljava/text/SimpleDateFormat;

    const-string v10, "yyyyMMddHHmmss"

    invoke-direct {v7, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-direct {v10, v11, v12}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v7, v10}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "record"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, v5, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v10, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iget-object v10, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v10}, Ljava/io/File;->createNewFile()Z

    move-result v4

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> creat file success is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v10, "SR/Recorder"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "<createRecordingFile> mSampleFile.getAbsolutePath() is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v12}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    const/16 v10, 0xa

    invoke-direct {p0, v10}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    const-string v10, "SR/Recorder"

    const-string v11, "<createRecordingFile> io exception happens"

    invoke-static {v10, v11}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public delete()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-nez v1, :cond_0

    const-string v1, "SR/Recorder"

    const-string v2, "<delete()> mSampleFile is null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    const-string v1, "SR/Recorder"

    const-string v2, "<delete> set mSampleFile = null"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iput v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-direct {p0, v4}, Lcom/android/soundrecorder/Recorder;->signalStateChanged(I)V

    return-void

    :cond_0
    const-string v1, "SR/Recorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<delete()> mSampleFile is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v0

    const-string v1, "SR/Recorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<delete()> file delete success is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public finish()V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    const-string v0, "SR/Recorder"

    const-string v1, "<finish> set mSampleFile = null"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iput v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Recorder;->signalStateChanged(I)V

    return-void
.end method

.method public getMaxAmplitude()I
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->getMaxAmplitude()I

    move-result v0

    goto :goto_0
.end method

.method public getRecordingFilePath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public goOnRecording()V
    .locals 3

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SR/Recorder"

    const-string v2, "<goOnRecording> IllegalArgumentException"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    goto :goto_0
.end method

.method public initAndStartMediaRecorder(IILandroid/content/Context;)Z
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Context;

    const v8, 0x1f400

    const v7, 0xbb80

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Landroid/media/MediaRecorder;

    invoke-direct {v3}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget v4, p0, Lcom/android/soundrecorder/Recorder;->mMode:I

    invoke-virtual {v3, v4, v2}, Landroid/media/MediaRecorder;->setHDRecordMode(IZ)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, p1}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    const-string v3, "SR/Recorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<initAndStartMediaRecorder> SR file path is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    const-string v3, "SR/Recorder"

    const-string v4, "<initAndStartMediaRecorder> AudioEncoder is default"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->prepare()V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3}, Landroid/media/MediaRecorder;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    return v1

    :pswitch_1
    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v8}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v7}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v6}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v6}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    const/16 v4, 0x6f54

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    const/16 v4, 0x3e80

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v1}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    goto :goto_0

    :pswitch_4
    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v8}, Landroid/media/MediaRecorder;->setAudioEncodingBitRate(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v7}, Landroid/media/MediaRecorder;->setAudioSamplingRate(I)V

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v3, v6}, Landroid/media/MediaRecorder;->setAudioChannels(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SR/Recorder"

    const-string v3, "<initAndStartMediaRecorder> IO exception"

    invoke-static {v1, v3}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    move v1, v2

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v1, "SR/Recorder"

    const-string v3, "<initAndStartMediaRecorder> RuntimeException"

    invoke-static {v1, v3}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    move v1, v2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public isPlaying()Z
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    goto :goto_0
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 0
    .param p1    # Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    return-void
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 3
    .param p1    # Landroid/media/MediaRecorder;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    const-string v0, "SR/Recorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onError> Recorder Error Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 3
    .param p1    # Landroid/media/MediaPlayer;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    const-string v0, "SR/Recorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<onError> Player Error Type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public pausePlayback()V
    .locals 2

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-direct {p0}, Lcom/android/soundrecorder/Recorder;->abandonAudioFocus()V

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/android/soundrecorder/Recorder;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public pauseRecording()V
    .locals 7

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->pause()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-wide v1, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v3

    iget-wide v5, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    sub-long/2addr v3, v5

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "SR/Recorder"

    const-string v2, "<pauseRecording> IllegalArgumentException"

    invoke-static {v1, v2}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->reset()V

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1}, Landroid/media/MediaRecorder;->release()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    goto :goto_0
.end method

.method public progress()I
    .locals 8

    const-wide/16 v6, 0x3e8

    const-string v2, "SR/Recorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<progress> current state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-ne v2, v3, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-string v2, "SR/Recorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<progress> elapsedRealtime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    sub-long v2, v0, v2

    iget-wide v4, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    add-long/2addr v2, v4

    div-long/2addr v2, v6

    long-to-int v2, v2

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x2

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-eq v2, v3, :cond_1

    const/4 v2, 0x3

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-ne v2, v3, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    div-int/lit16 v2, v2, 0x3e8

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    iget v3, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    div-long/2addr v2, v6

    long-to-int v2, v2

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public restoreState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    const-string v4, "sample_path"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "not_saved"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v3, :cond_1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "sample_length"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    mul-int/lit16 v4, v2, 0x3e8

    int-to-long v4, v4

    iput-wide v4, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    const-string v4, "sample_path"

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "sample_length"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz v2, :cond_0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->delete()V

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iput v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/Recorder;->signalStateChanged(I)V

    goto :goto_0
.end method

.method public sampleFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    return-object v0
.end method

.method public sampleFileDelSuffix()V
    .locals 7

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v4, ".tmp"

    invoke-virtual {v2, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    const-string v5, ".tmp"

    invoke-virtual {v2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v4, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    const-string v4, "SR/Recorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<sampleFileDelSuffix()> rename file <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> to <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "SR/Recorder"

    const-string v5, "<sampleFileDelSuffix()> rename file fail"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v4, "SR/Recorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<sampleFileDelSuffix()> file <"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "> is not end with <.tmp>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public sampleLength()I
    .locals 1

    iget v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    return v0
.end method

.method public saveState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "sample_path"

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sample_length"

    iget v1, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public setOnStateChangedListener(Lcom/android/soundrecorder/Recorder$OnStateChangedListener;)V
    .locals 0
    .param p1    # Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    iput-object p1, p0, Lcom/android/soundrecorder/Recorder;->mOnStateChangedListener:Lcom/android/soundrecorder/Recorder$OnStateChangedListener;

    return-void
.end method

.method public setRecordMode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/soundrecorder/Recorder;->mMode:I

    return-void
.end method

.method public startPlayback()V
    .locals 8

    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x1

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_2

    const/16 v2, 0x9

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mAudioManager:Landroid/media/AudioManager;

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mFocusChangeListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-eq v1, v5, :cond_3

    const-string v2, "SR/Recorder"

    const-string v3, "<startPlayback()> request audio focus fail"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x7

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    goto :goto_0

    :cond_3
    const-string v2, "SR/Recorder"

    const-string v3, "<startPlayback()> request audio focus success"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v5, p0, Lcom/android/soundrecorder/Recorder;->mGetFocus:Z

    :cond_4
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    :try_start_0
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    iget-object v3, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->prepare()V

    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V

    const-string v2, "SR/Recorder"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<startPlayback> The length of recording file is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    :goto_1
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iput-object v7, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/android/soundrecorder/Recorder;->abandonAudioFocus()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iput-object v7, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/android/soundrecorder/Recorder;->abandonAudioFocus()V

    goto/16 :goto_0

    :cond_5
    :try_start_1
    iget-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->start()V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-direct {p0, v6}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iput-object v7, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    goto/16 :goto_0
.end method

.method public startRecording(IILjava/lang/String;Landroid/content/Context;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Landroid/content/Context;

    const-string v0, "SR/Recorder"

    const-string v1, "<startRecording> 4 params"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSoundRecorderDoWhat:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSoundRecorderDoWhat:Ljava/lang/String;

    const-string v1, "play"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SR/Recorder"

    const-string v1, "<startRecording> delete file fail"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stop()V

    const-string v0, "SR/Recorder"

    const-string v1, "<startRecording> set mSampleFile = null"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    iget v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    if-lez v0, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    :cond_2
    invoke-virtual {p0, p3}, Lcom/android/soundrecorder/Recorder;->createRecordingFile(Ljava/lang/String;)V

    invoke-virtual {p0, p1, p2, p4}, Lcom/android/soundrecorder/Recorder;->initAndStartMediaRecorder(IILandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_0
    return-void

    :cond_3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    const-string v0, "SR/Recorder"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<startRecording> mSampleStart = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto :goto_0
.end method

.method public state()I
    .locals 1

    iget v0, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    return v0
.end method

.method public stop()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stopRecording()V

    invoke-virtual {p0}, Lcom/android/soundrecorder/Recorder;->stopPlayback()V

    return-void
.end method

.method public stopPlayback()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    iput-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/android/soundrecorder/Recorder;->abandonAudioFocus()V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    iput-object v2, p0, Lcom/android/soundrecorder/Recorder;->mPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0}, Lcom/android/soundrecorder/Recorder;->abandonAudioFocus()V

    goto :goto_0
.end method

.method public stopRecording()V
    .locals 11

    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    if-nez v4, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v4, p0, Lcom/android/soundrecorder/Recorder;->mState:I

    if-ne v1, v4, :cond_3

    :goto_1
    :try_start_0
    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> mRecorder.stop() begin"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->stop()V

    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> mRecorder.stop() end"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> release mRecorder begin"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->reset()V

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v4}, Landroid/media/MediaRecorder;->release()V

    iput-object v10, p0, Lcom/android/soundrecorder/Recorder;->mRecorder:Landroid/media/MediaRecorder;

    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> release mRecorder end"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_1

    iget-wide v4, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/android/soundrecorder/Recorder;->mSampleStart:J

    sub-long/2addr v6, v8

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    :cond_1
    iget-wide v4, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    iput v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    const-string v4, "SR/Recorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<stopRecording> mSampleLength in ms is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/soundrecorder/Recorder;->mPreviousTime:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "SR/Recorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<stopRecording> mSampleLength in s is = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleLength:I

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v2

    const-string v4, "SR/Recorder"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<stopRecording> Is file delete success? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> set mSampleFile = null when mSampleLength == 0"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v10, p0, Lcom/android/soundrecorder/Recorder;->mSampleFile:Ljava/io/File;

    :cond_2
    invoke-direct {p0, v3}, Lcom/android/soundrecorder/Recorder;->setState(I)V

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const/4 v4, 0x6

    invoke-direct {p0, v4}, Lcom/android/soundrecorder/Recorder;->setError(I)V

    const-string v4, "SR/Recorder"

    const-string v5, "<stopRecording> recorder illegalstate exception in recorder.stop()"

    invoke-static {v4, v5}, Lcom/android/soundrecorder/SRLogUtils;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method
