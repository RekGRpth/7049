.class Lcom/android/soundrecorder/RecordingFileList$4;
.super Landroid/content/BroadcastReceiver;
.source "RecordingFileList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/soundrecorder/RecordingFileList;->registerExternalStorageListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/RecordingFileList;


# direct methods
.method constructor <init>(Lcom/android/soundrecorder/RecordingFileList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v1}, Lcom/android/soundrecorder/RecordingFileList;->access$100(Lcom/android/soundrecorder/RecordingFileList;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v1, v2}, Lcom/android/soundrecorder/RecordingFileList;->access$902(Lcom/android/soundrecorder/RecordingFileList;Ljava/util/List;)Ljava/util/List;

    iget-object v1, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    iget-object v1, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v1}, Lcom/android/soundrecorder/RecordingFileList;->finishSelf()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    iget-object v2, p0, Lcom/android/soundrecorder/RecordingFileList$4;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v2}, Lcom/android/soundrecorder/RecordingFileList;->access$900(Lcom/android/soundrecorder/RecordingFileList;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/soundrecorder/RecordingFileList;->access$500(Lcom/android/soundrecorder/RecordingFileList;Ljava/util/List;)V

    goto :goto_0
.end method
