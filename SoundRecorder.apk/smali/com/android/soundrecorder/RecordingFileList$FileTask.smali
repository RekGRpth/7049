.class public Lcom/android/soundrecorder/RecordingFileList$FileTask;
.super Landroid/os/AsyncTask;
.source "RecordingFileList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/RecordingFileList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FileTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/RecordingFileList;


# direct methods
.method public constructor <init>(Lcom/android/soundrecorder/RecordingFileList;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6
    .param p1    # [Ljava/lang/Void;

    const-string v3, "SR/RecordingFileList"

    const-string v4, "<FileTask.doInBackground> begin"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v3}, Lcom/android/soundrecorder/RecordingFileList;->getSelectedFiles()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "SR/RecordingFileList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<FileTask.doInBackground> the number of delete files: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v4, "SR/RecordingFileList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<FileTask.doInBackground> delete file ["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] fail"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v3, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v3}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/android/soundrecorder/SoundRecorderUtils;->deleteFileFromMediaDB(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    :goto_1
    return-object v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const-string v3, "SR/RecordingFileList"

    const-string v4, "<FileTask.doInBackground> end"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/soundrecorder/RecordingFileList$FileTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 5

    const-string v2, "SR/RecordingFileList"

    const-string v3, "<FileTask.onCancelled>"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "SR/RecordingFileList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<FileTask.onCancelled> fragmentManager = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "Progress"

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/app/DialogFragment;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/DialogFragment;->dismissAllowingStateLoss()V

    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Boolean;

    const/4 v2, 0x1

    const-string v0, "SR/RecordingFileList"

    const-string v1, "<FileTask.onPostExecute>"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    const-string v1, "Progress"

    invoke-static {v0, v1}, Lcom/android/soundrecorder/RecordingFileList;->access$300(Lcom/android/soundrecorder/RecordingFileList;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v0, v2}, Lcom/android/soundrecorder/RecordingFileList;->access$402(Lcom/android/soundrecorder/RecordingFileList;Z)Z

    iget-object v0, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v0}, Lcom/android/soundrecorder/RecordingFileList;->access$200(Lcom/android/soundrecorder/RecordingFileList;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v0, v2}, Lcom/android/soundrecorder/RecordingFileList;->access$002(Lcom/android/soundrecorder/RecordingFileList;I)I

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    const/16 v1, 0xc

    invoke-static {v0, v1}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    :cond_0
    iget-object v0, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/soundrecorder/RecordingFileList;->access$500(Lcom/android/soundrecorder/RecordingFileList;Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/android/soundrecorder/RecordingFileList$FileTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    const-string v2, "SR/RecordingFileList"

    const-string v3, "<FileTask.onPreExecute>"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-static {v2}, Lcom/android/soundrecorder/RecordingFileList;->access$200(Lcom/android/soundrecorder/RecordingFileList;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "SR/RecordingFileList"

    const-string v3, "<FileTask.onPreExecute> Activity is running in foreground"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/soundrecorder/RecordingFileList$FileTask;->this$0:Lcom/android/soundrecorder/RecordingFileList;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v2, "SR/RecordingFileList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<FileTask.onPreExecute> fragmentManager = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/android/soundrecorder/ProgressDialogFragment;->newInstance()Lcom/android/soundrecorder/ProgressDialogFragment;

    move-result-object v1

    const-string v2, "Progress"

    invoke-virtual {v1, v0, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    :goto_0
    return-void

    :cond_0
    const-string v2, "SR/RecordingFileList"

    const-string v3, "<FileTask.onPreExecute> Activity is running in background"

    invoke-static {v2, v3}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
