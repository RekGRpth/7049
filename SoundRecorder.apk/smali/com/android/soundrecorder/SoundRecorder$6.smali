.class Lcom/android/soundrecorder/SoundRecorder$6;
.super Landroid/content/BroadcastReceiver;
.source "SoundRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/soundrecorder/SoundRecorder;->registerExternalStorageListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/SoundRecorder;


# direct methods
.method constructor <init>(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onReceive> action = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_4

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$100(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$200(Lcom/android/soundrecorder/SoundRecorder;)Lcom/android/soundrecorder/Recorder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/soundrecorder/Recorder;->delete()V

    :goto_0
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$300(Lcom/android/soundrecorder/SoundRecorder;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$400(Lcom/android/soundrecorder/SoundRecorder;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SoundRecorder;->access$502(Lcom/android/soundrecorder/SoundRecorder;Ljava/lang/String;)Ljava/lang/String;

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3, v6}, Lcom/android/soundrecorder/SoundRecorder;->access$602(Lcom/android/soundrecorder/SoundRecorder;Z)Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3, v7}, Lcom/android/soundrecorder/ErrorHandle;->showErrorInfo(Landroid/app/Activity;I)V

    :cond_1
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3, v7}, Lcom/android/soundrecorder/SoundRecorder;->access$402(Lcom/android/soundrecorder/SoundRecorder;Z)Z

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onReceive> set mIsSDCardPlugOut = true"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorder;->onBackToInit()V

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_2

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onReceive> ACTION_MEDIA_MOUNTED"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    const-string v4, "error_dialog"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SoundRecorder;->access$700(Lcom/android/soundrecorder/SoundRecorder;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3, v6}, Lcom/android/soundrecorder/SoundRecorder;->access$602(Lcom/android/soundrecorder/SoundRecorder;Z)Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3, v7}, Lcom/android/soundrecorder/SoundRecorder;->access$802(Lcom/android/soundrecorder/SoundRecorder;Z)Z

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onReceive> mIsSDCardPlugOut = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v5}, Lcom/android/soundrecorder/SoundRecorder;->access$400(Lcom/android/soundrecorder/SoundRecorder;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onReceive> mRecordingFilePath = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v5}, Lcom/android/soundrecorder/SoundRecorder;->access$900(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "SR/SoundRecorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<onReceive> mDoWhat = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v5}, Lcom/android/soundrecorder/SoundRecorder;->access$100(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$900(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$900(Lcom/android/soundrecorder/SoundRecorder;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "SR/SoundRecorder"

    const-string v4, "<onReceive> deleteRecordingFile"

    invoke-static {v3, v4}, Lcom/android/soundrecorder/SRLogUtils;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$1000(Lcom/android/soundrecorder/SoundRecorder;)V

    :cond_5
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$1100(Lcom/android/soundrecorder/SoundRecorder;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$1200(Lcom/android/soundrecorder/SoundRecorder;)Lcom/android/soundrecorder/RemainingTimeCalculator;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/soundrecorder/RemainingTimeCalculator;->diskSpaceRemaining()J

    move-result-wide v1

    iget-object v5, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-ltz v3, :cond_7

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$1300(Lcom/android/soundrecorder/SoundRecorder;)J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    :goto_2
    invoke-static {v5, v3, v4}, Lcom/android/soundrecorder/SoundRecorder;->access$1302(Lcom/android/soundrecorder/SoundRecorder;J)J

    :cond_6
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3, v6}, Lcom/android/soundrecorder/SoundRecorder;->access$402(Lcom/android/soundrecorder/SoundRecorder;Z)Z

    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-virtual {v3}, Lcom/android/soundrecorder/SoundRecorder;->updateUi()V

    goto/16 :goto_1

    :cond_7
    iget-object v3, p0, Lcom/android/soundrecorder/SoundRecorder$6;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v3}, Lcom/android/soundrecorder/SoundRecorder;->access$1300(Lcom/android/soundrecorder/SoundRecorder;)J

    move-result-wide v3

    goto :goto_2
.end method
