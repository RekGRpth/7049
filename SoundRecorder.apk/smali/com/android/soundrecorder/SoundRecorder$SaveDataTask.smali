.class public Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;
.super Landroid/os/AsyncTask;
.source "SoundRecorder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/soundrecorder/SoundRecorder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SaveDataTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Object;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/soundrecorder/SoundRecorder;


# direct methods
.method public constructor <init>(Lcom/android/soundrecorder/SoundRecorder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/net/Uri;
    .locals 2
    .param p1    # [Ljava/lang/Void;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    iget-object v1, p0, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v1}, Lcom/android/soundrecorder/SoundRecorder;->access$200(Lcom/android/soundrecorder/SoundRecorder;)Lcom/android/soundrecorder/Recorder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/soundrecorder/Recorder;->sampleFile()Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/soundrecorder/SoundRecorder;->access$1400(Lcom/android/soundrecorder/SoundRecorder;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;->doInBackground([Ljava/lang/Void;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/net/Uri;)V
    .locals 1
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;->this$0:Lcom/android/soundrecorder/SoundRecorder;

    invoke-static {v0, p1}, Lcom/android/soundrecorder/SoundRecorder;->access$1500(Lcom/android/soundrecorder/SoundRecorder;Landroid/net/Uri;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/android/soundrecorder/SoundRecorder$SaveDataTask;->onPostExecute(Landroid/net/Uri;)V

    return-void
.end method
